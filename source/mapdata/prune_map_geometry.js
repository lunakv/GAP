const fs = require("fs");

const fContent = fs.readFileSync("C:\\Users\\mbrabec\\source\\repos\\GAP\\source\\frontend\\public\\topo-orig.json");

const topoJson = JSON.parse(fContent);

topoJson.arcs.forEach((polygon, idx) => {
  if (polygon.length < 50) return;

  topoJson.arcs[idx] = polygon.filter((_, i) => i % 4 == 0);
});

const output = JSON.stringify(topoJson);

fs.writeFileSync("C:\\Users\\mbrabec\\source\\repos\\GAP\\source\\frontend\\public\\topo.json", output);

console.log('done');
