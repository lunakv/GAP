#!/bin/sh

# Download data from national catalog
mkdir -p output
if ! [ -f output/1.zip ]; then
	echo Downloading map data...
	curl 'https://services.cuzk.cz/shp/stat/epsg-5514/1.zip' > output/1.zip
else
	echo Found map data ZIP in output folder, skipping download.
fi
echo Extracting map data...
unzip -q output/1.zip

# Extract layers we care about
LAYERS='ORP_P OKRESY_P VUSC_P'
for layer in $LAYERS; do
	mv 1/${layer}.* .
done
rm -r 1 

# expression passed to ndjson-map to describe the object transforation
# creates a deduplicated ID for each layer, links to correct ID for parent,
# and gets rid of unnecessary properties 
ndmapExpr() {
	case $1 in
		ORP_P)
			PRE=1
			PARENT='parseInt("2" + d.properties.OKRES_KOD, 10)'
			;;
		OKRESY_P)
			PRE=2
			PARENT='parseInt("3" + d.properties.VUSC_KOD, 10)'
			;;
		VUSC_P)
			PRE=3
			PARENT='undefined'
			;;
		*)
			exit 1
			;;
	esac
	PROPS='"name": d.properties.NAZEV, "parent_id": '"$PARENT"
	ID="parseInt(\"$PRE\" + d.properties.KOD, 10)"
	echo -n "d.id = $ID, d.properties = {$PROPS}, d"  
}

# Convert to GeoJSON, transforming encoding to UTF-8 and projection to EPSG 4326
echo Converting to GeoJSON...
TOPO_OPTS=''
for layer in $LAYERS; do
	ogr2ogr -f 'ESRI Shapefile' -t_srs EPSG:4326 -lco ENCODING=UTF-8 transformed.shp ${layer}.shp
	shp2json -n --encoding=utf-8 transformed.shp | ndjson-map "$(ndmapExpr $layer)" > $layer.ndjson
	TOPO_OPTS="$layer=$layer.ndjson $TOPO_OPTS"
done

# compile all the GeoJSON files into topojson and simplify the topology
echo Compiling into topojson...
geo2topo -n $TOPO_OPTS > topo1.json
toposimplify -P 0.05 -f < topo1.json > output/topo.json

# cleanup
for layer in $LAYERS; do
	rm $layer.*
done
rm topo1.json
rm transformed.*
echo Done.
