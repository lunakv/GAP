module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: ['airbnb', 'airbnb-typescript', 'prettier'],
  overrides: [],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: './tsconfig.json',
  },
  plugins: ['react', '@typescript-eslint'],
  ignorePatterns: ['*.css', '*.svg', '*.jpg', '*.png', '*.md'],
  rules: {
    'react/jsx-filename-extension': [2, { extensions: ['.tsx'] }],
    'react/react-in-jsx-scope': ['off', 'always'],
    '@typescript-eslint/no-unused-vars': ['warn'],
    '@typescript-eslint/no-use-before-define': 0,
    'no-plusplus': 0,
    'no-param-reassign': 0,
    'no-debugger': 1,
    'react/require-default-props': 0,
  },
};
