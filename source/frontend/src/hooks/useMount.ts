import { EffectCallback, useEffect } from 'react';

function useMount(effect: EffectCallback) {
  useEffect(effect, []);
}

export default useMount;
