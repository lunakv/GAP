import { ReactNode } from 'react';
import MainLayout from '../features/AppSkeleton/MainLayout/MainLayout';
import { baseSideMenuItems, SideMenuItem } from '../features/AppSkeleton/SideMenu/side-menu-items';

function withMainLayout(component: ReactNode, sideMenu: SideMenuItem[] = baseSideMenuItems) {
  return <MainLayout sideMenuItems={sideMenu}>{component}</MainLayout>;
}

export default withMainLayout;
