import { MapRegionDetail, Region } from '../../apiTypes/map';

export interface TopoJsonProps {
  name: string;
  parent_id?: number;
  meta?: Region;
}

export interface TooltipData {
  chunk: string;
  kraj: string;
  top: number;
  left: number;
}

export interface MapModalProps {
  regionDetail: MapRegionDetail;
  region: TopoJsonProps;
  top: number;
  left: number;
}
