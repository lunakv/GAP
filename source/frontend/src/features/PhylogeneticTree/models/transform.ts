export interface Transform {
  scale: number;
  x: number;
  y: number;
}
