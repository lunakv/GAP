export interface TooltipData {
  top: number;
  left: number;
  content: string[];
  moreUsersCount: number | null;
}

export interface TooltipCollSetters {
  setTooltipVisible: (v: boolean) => void;
  setTooltipData: (v: TooltipData) => void;
}
