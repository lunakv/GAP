export default interface UserOfHaplogroup {
  name: string;
  email: string;
  municipality: string;
}

export interface UsersOfHaploGroupDto {
  usersForNode: {
    givenName: string;
    familyName: string;
    email: string;
    residenceAddress: {
      municipality: string;
    };
  }[];
}
