export interface HaploGroupNodeDto {
  id: number;
  name: string;
  parentId: number;
  haplogroupId: number;
  peopleCount: {
    inside: number;
    inSubtree: number;
  };
  usersSample: {
    profile: {
      familyName: string;
      givenName: string;
    };
  }[];
  approximateYear: number;
  isHaplogroupRoot: boolean;
  isSpecifiedUser: boolean;
}

export interface UsersHaploGroupDto {
  user: {
    geneticData: {
      haplogroup: {
        id: number;
        name: string;
      };
    };
  };
}
