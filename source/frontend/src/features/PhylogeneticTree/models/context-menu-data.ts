import { Node } from './graph';

export interface ContextMenuData {
  position: { top: number; left: number };
  node: d3.HierarchyPointNode<Node>;
}

export interface ContextMenuSetters {
  setContextMenuData: (data: ContextMenuData) => void;
  setContextMenuVisible: (visible: boolean) => void;
}

export interface ContextMenuCallbacks {
  expandEntireSubtree: (node: d3.HierarchyPointNode<Node>) => void;
  collapseEntireSubtree: (node: d3.HierarchyPointNode<Node>) => void;
}
