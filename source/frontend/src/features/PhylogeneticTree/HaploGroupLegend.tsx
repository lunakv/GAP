import { getHaplogroupRoots, Node } from './models/graph';
import './HaploGroupLegend.css';
import getGetTextColorForBackground from './d3-tree-logic';

interface HaploGroupLegendParams {
  tree: Node;
}

export default function HaploGroupLegend({ tree }: HaploGroupLegendParams) {
  const haploGroupRoots = tree ? getHaplogroupRoots(tree) : [];

  return (
    <div className="haplogroup-legend-container">
      {haploGroupRoots.map((haploRoot) => (
        <div
          key={haploRoot.name}
          className="legend-item"
          style={{ color: getGetTextColorForBackground(haploRoot.color), backgroundColor: haploRoot.color }}
        >
          {haploRoot.name}
        </div>
      ))}
    </div>
  );
}
