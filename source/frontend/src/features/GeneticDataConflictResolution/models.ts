export interface UserReference {
  id: number;
  name: string;
}

export interface ConflictingGeneticData {
  dataOrigins: {
    name: string;
  }[];

  conflictingMarkers: MarkerValuesRow[];

  alignedMarkers: MarkerValuesRow[];
}

export interface MarkerValuesRow {
  markerName: string;
  valuesByOrigin: { [markerName: string]: number[] };
  noConflict?: boolean;
}
