import { UserType } from '../../apiTypes/user';

export default interface Staff {
  id: number;
  email: string;
  role: UserType;
}
