import { useTranslation } from 'react-i18next';
import { ChangeEvent, useState } from 'react';
import { useMutation, useQuery } from '@apollo/client';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { getUserId } from '../../utils/api-client';
import { Ancestor, User } from '../../apiTypes/user';
import { updateAncestorMutation, userQuery } from './queries';
import MainLayout from '../AppSkeleton/MainLayout/MainLayout';
import { userSideMenuItems } from '../AppSkeleton/SideMenu/side-menu-items';
import LoadingIcon from '../xShared/LoadingIcon';
import { cleanUpObject } from './utils';

function AncestorProfile() {
  const userId = getUserId();
  const { t } = useTranslation(['profile', 'skeleton']);
  const [ancestor, setAncestor] = useState<Ancestor>(null);
  useQuery<{ user: User }>(userQuery, {
    variables: { id: userId },
    onCompleted: (d) => setAncestor(d.user.ancestor),
  });
  const [update] = useMutation(updateAncestorMutation);

  const profileChange = (key: string) => (e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    const newProfile = { ...ancestor, [key]: value };
    setAncestor(newProfile);
  };

  const addressChange = (key: string) => (e: ChangeEvent<HTMLInputElement>) => {
    const newProfile = { ...ancestor };
    newProfile.address[key] = e.target.value;
    setAncestor(newProfile);
  };

  const handleUpdate = (e) => {
    e.preventDefault();
    // todo show success/failure
    update({ variables: { userId: getUserId(), ancestor: cleanUpObject(ancestor) } }).then();
  };

  return (
    <MainLayout sideMenuItems={userSideMenuItems}>
      <Container className="mt-5 user-form-container">
        {ancestor ? (
          <Form onSubmit={handleUpdate}>
            <h2 className="mb-5">{t('skeleton:userSideMenu.ancestorInfo')}</h2>
            <Form.Group as={Row} className="mb-2">
              <Form.Label column sm={2}>
                {t('firstName')}
              </Form.Label>
              <Col sm={10}>
                <Form.Control type="text" value={ancestor.givenName} onChange={profileChange('givenName')} />
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-2">
              <Form.Label column sm={2}>
                {t('lastName')}
              </Form.Label>
              <Col sm={10}>
                <Form.Control type="text" value={ancestor.familyName} onChange={profileChange('familyName')} />
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-2">
              <Form.Label column sm={2}>
                {t('birthDate')}
              </Form.Label>
              <Col sm={10}>
                <Form.Control type="date" value={ancestor.birthDate} onChange={profileChange('birthDate')} />
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-2">
              <Form.Label column sm={2}>
                {t('deathDate')}
              </Form.Label>
              <Col sm={10}>
                <Form.Control type="date" value={ancestor.deathDate} onChange={profileChange('birthDate')} />
              </Col>
            </Form.Group>
            <h4 className="mt-4 mb-3">{t('address')}</h4>
            <Form.Group as={Row} className="mb-2">
              <Form.Label column sm={2}>
                {t('street')}
              </Form.Label>
              <Col sm={10}>
                <Form.Control type="text" value={ancestor.address.street} onChange={addressChange('street')} />
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-2">
              <Form.Label column sm={2}>
                {t('town')}
              </Form.Label>
              <Col sm={10}>
                <Form.Control type="text" value={ancestor.address.town} onChange={addressChange('town')} />
              </Col>
            </Form.Group>
            <Form.Group as={Row} className="mb-2">
              <Form.Label column sm={2}>
                {t('zipCode')}
              </Form.Label>
              <Col sm={10}>
                <Form.Control type="text" value={ancestor.address.zipCode} onChange={addressChange('zipCode')} />
              </Col>
            </Form.Group>
            <Button variant="secondary" type="submit">
              {t('update')}
            </Button>
          </Form>
        ) : (
          <LoadingIcon />
        )}
      </Container>
    </MainLayout>
  );
}

export default AncestorProfile;
