import fileDownload from 'js-file-download';
import { getToken } from '../../utils/api-client';

class RestApiClientType {
  private baseUrl: string;

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  public downloadFile(fileName: string, fileAlias: string) {
    fetch(`${this.baseUrl}/file/${fileName}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    }).then(async (res) => {
      fileDownload(await res.blob(), fileAlias); // call to 'js-file-download'
    });
  }
}

const RestApiClient = new RestApiClientType('http://localhost:3000');

export default RestApiClient;
