export interface UserReference {
  id: number;
  name: string;
  email: string;
}

export interface UsersRights {
  withViewRight?: UserReference[];
  withEditRight?: UserReference[];
  canView?: UserReference[];
  canEdit?: UserReference[];
}
