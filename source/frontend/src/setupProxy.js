const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function middleware(app) {
  app.use(
    '/api',
    createProxyMiddleware({
      target: 'https://localhost:7095/',
      changeOrigin: true,
      secure: false,
    })
  );
  app.use(
    '/laboratory',
    createProxyMiddleware({
      target: 'https://localhost:7095/',
      changeOrigin: true,
      secure: false,
    })
  );
  app.use(
    '/file',
    createProxyMiddleware({
      target: 'https://localhost:7095/',
      changeOrigin: true,
      secure: false,
    })
  );
};
