interface Point {
  x: number;
  y: number;
}

function minus(p1: Point, p2: Point): Point {
  return {
    x: p1.x - p2.x,
    y: p1.y - p2.y,
  };
}

function plus(p1: Point, p2: Point): Point {
  return {
    x: p1.x + p2.x,
    y: p1.y + p2.y,
  };
}

function times(p: Point, scalar: number): Point {
  return {
    x: p.x * scalar,
    y: p.y * scalar,
  };
}

function len(p: Point): number {
  return Math.sqrt(p.x * p.x + p.y * p.y);
}

export { Point, minus, plus, times, len };
