import { PublicUser } from './user';
import { MarkerSet } from './geneticData';

export interface UserRelativeDistance {
  comparedMarkers: number;
  distance: number;
  markerSetValues: (number | null)[];
  user: PublicUser;
}

export interface RelativeSearchResult {
  markerSet: MarkerSet;
  userMarkerSetValues: (number | null)[];
  userRelativeDistances: UserRelativeDistance[];
}
