import { Haplogroup, HaplogroupCount } from './geneticData';

export interface FamilyNameCount {
  count: number;
  familyName: string;
}

export interface HaplogroupFamilyNameCount {
  familyNameCounts: FamilyNameCount[];
  haplogroup: Haplogroup;
}

export interface MapRegionDetail {
  haplogroupFamilyNameCounts: HaplogroupFamilyNameCount[];
  haplogroupUserCounts: HaplogroupCount[];
}

export interface Region {
  id: number;
  name: string;
  userCount: number;
}

export interface RegionLayer {
  layerId: number;
  regions: Region[];
}

export interface RegionUserFilter {
  familyNames?: string[];
  haplogroupIds?: number[];
}

export interface GlobalCounts {
  [haplogroupId: number]: {
    total: number;
    byName?: {
      [familyName: string]: number;
    };
  };
}
