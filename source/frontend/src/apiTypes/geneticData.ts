export interface Haplogroup {
  id: number;
  name: string;
}

export interface StrMarker {
  count: number;
  name: string;
}

export interface PublicGeneticData {
  haplogroup: string;
}

export interface GeneticData {
  haplogroup: Haplogroup;
  strMarkers: StrMarker[];
}

export interface MarkerSet {
  markerCount: number;
  markers: string[];
  maxDistance: number;
  name: string;
}

export interface HaplogroupCount {
  count: number;
  haplogroup: Haplogroup;
}
