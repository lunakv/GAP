## API Types

This directory contains TypeScript definitions of types defined in the GraphQL API.

Note that because different queries can ask for only different subsets of the type, most fields in the response were explicitly marked as optional. It is up to the programmer of the feature to ensure that all relevant fields exist on their query response.
