import { Language } from './general';

export interface NullableExpandedAddressInput {
  county?: string;
  municipality?: string;
  street?: string;
  town?: string;
  zipCode?: string;
}

export interface AncestorInput {
  address?: NullableExpandedAddressInput;
  birthDate?: string;
  deathDate?: string;
  familyName?: string;
  givenName?: string;
  placeOfBirth?: string;
  placeOfDeath?: string;
}

export interface AddressInput {
  street: string;
  town: string;
  zipCode: string;
}

export interface RegistrationResidenceAddressInput extends AddressInput {
  municipality: string;
}

export interface RegistrationProfileInput {
  birthDate: string;
  correspondenceAddress: AddressInput;
  email: string;
  familyName: string;
  givenName: string;
  phoneNumber: string;
  residenceAddress: RegistrationResidenceAddressInput;
}

export interface RegistrationUserInput {
  administrationAgreementSigned: boolean;
  ancestor: AncestorInput;
  password: string;
  profile: RegistrationProfileInput;
}

export interface GeneticTestProviderInput {
  name: string;
}

export interface StrMarkerInput {
  name: string;
  value: number;
}

export interface RegisterWithLabTestInput {
  language: Language;
  user: RegistrationUserInput;
}

export interface RegisterWithNotSupportedExternalTestProviderInput {
  labTestRequested: boolean;
  language: Language;
  strFile: File;
  strMarkers: StrMarkerInput[];
  testProvider: GeneticTestProviderInput;
  user: RegistrationUserInput;
}

export interface RegisterWithSupportedExternalTestProviderInput {
  labTestRequested: boolean;
  language: Language;
  strFile: File;
  user: RegistrationUserInput;
}

export type RegisterInput =
  | RegisterWithLabTestInput
  | RegisterWithSupportedExternalTestProviderInput
  | RegisterWithNotSupportedExternalTestProviderInput;
