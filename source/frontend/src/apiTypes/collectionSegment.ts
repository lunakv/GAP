interface CollectionSegmentInfo {
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;
}

export interface CollectionSegment<T> {
  totalCount?: number;
  items?: T[];
  pageInfo?: CollectionSegmentInfo;
}
