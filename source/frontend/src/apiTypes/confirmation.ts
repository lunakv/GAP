export interface AdministrationAgreementSignResult {
  loginToken: string;
  success: boolean;
}
