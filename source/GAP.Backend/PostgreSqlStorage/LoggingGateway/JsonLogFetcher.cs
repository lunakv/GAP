using System.Text.Json;
using System.Text.Json.Nodes;
using Logging.Storage;
using Microsoft.EntityFrameworkCore;

namespace PostgreSqlStorage.LoggingGateway; 

/// <summary>
/// Fetches logs for mutation requests. One request log fetched does not correspond to one row but multiple rows.
/// To produce request logs, it must be grouped by RequestId column. 
/// </summary>
public class JsonLogFetcher : ILogFetcher {

	private readonly LogsContext _context;
	
	public JsonLogFetcher(LogsContext context) {
		_context = context;
	}
	
	public async Task<IList<StorageRequestLog>> GetLogsAsync(StorageLogFilter logFilter, bool includeRawLogs) {
		IQueryable<MutationLog> logs = _context.MutationLogs;
		if (logFilter.From.HasValue) {
			logs = logs.Where(log => log.Date.HasValue && log.Date.Value > logFilter.From.Value);
		}
		
		if (logFilter.To.HasValue) {
			logs = logs.Where(log => log.Date.HasValue && log.Date.Value < logFilter.To.Value);
		}
		
		if (logFilter.RequestId != null) {
			logs = logs.Where(log => log.RequestId == logFilter.RequestId);
		}

		logs = FilterBySparselyPresent(logs: logs, performerId: logFilter.PerformerId, aboutUser: logFilter.AboutUser, endpointNames: logFilter.EndpointNames);
		logs = FilterByRequestResult(logs: logs, requestSuccessful: logFilter.RequestSuccessful);
		
		var filteredLogs = await logs.ToListAsync().ConfigureAwait(false);

		return filteredLogs
			.GroupBy(log => log.RequestId)
			.Select(g => CreateLog(g: g, includeRawLogs: includeRawLogs))
			.ToList();
	}

	public async Task<IList<string>> GetEndpointNamesAsync() {
		IList<string?> endpoints = await _context.MutationLogs.Select(log => log.Endpoint).Distinct().ToListAsync().ConfigureAwait(false);
		return endpoints.Where(endpoint => endpoint != null).Cast<string>().ToList();
	}

	private IQueryable<MutationLog> FilterBySparselyPresent(IQueryable<MutationLog> logs, int? performerId, int? aboutUser, IList<string> endpointNames) {
		if (performerId.HasValue || aboutUser.HasValue || endpointNames.Count != 0) {
			var filteredLogsForRequestIds = logs.Where(log => log.PerformedByUser.HasValue || log.AboutUser.HasValue);

			if (performerId.HasValue) {
				filteredLogsForRequestIds = filteredLogsForRequestIds.Where(log => log.PerformedByUser == performerId);
			}
			
			if (aboutUser.HasValue) {
				filteredLogsForRequestIds = filteredLogsForRequestIds.Where(log => log.AboutUser == aboutUser);
			}
			
			if (endpointNames.Count != 0) {
				filteredLogsForRequestIds = filteredLogsForRequestIds.Where(log => endpointNames.Contains(log.Endpoint ?? string.Empty));
			}
			var filteredRequestIds = filteredLogsForRequestIds.Select(log => log.RequestId);

			return
				from log in logs
				join requestId in filteredRequestIds on log.RequestId equals requestId
				select log;
		}

		return logs;
	}

	private IQueryable<MutationLog> FilterByRequestResult(IQueryable<MutationLog> logs, bool? requestSuccessful) {
		if (requestSuccessful.HasValue) {
			IQueryable<string?> successfulRequestIds = logs.Where(log => log.RequestSuccessful.HasValue).Select(log => log.RequestId);
			
			IQueryable<string?> requestIds;
			if (requestSuccessful.Value) {
				requestIds = successfulRequestIds;
			} else {
				requestIds = logs.Where(log => !log.RequestSuccessful.HasValue).Select(log => log.RequestId).Except(successfulRequestIds);
			}
			
			return 
				from log in logs
				join requestId in requestIds on log.RequestId equals requestId
				select log;
		} 
		
		return logs;
	}

	private StorageRequestLog CreateLog(IGrouping<string?, MutationLog> g, bool includeRawLogs) {
		int? performedByUser = null;
		int? aboutUser = null;
		string? endpoint = null;
		DateTime? date = null;
		bool? requestSuccessful = null;
		JsonObject rawLog = new JsonObject();
		
		foreach (MutationLog log in g) {
			performedByUser = log.PerformedByUser ?? performedByUser;
			aboutUser = log.AboutUser ?? aboutUser;
			endpoint = log.Endpoint ?? endpoint;
			date = log.Date ?? date;
			requestSuccessful = log.RequestSuccessful ?? requestSuccessful;
			if (includeRawLogs && log.Event != null) {
				var json = JsonNode.Parse(log.Event);
				// json is null only if log.Event is null which is checked above.
				foreach (var element in json!.AsObject()) {
					
					if (!rawLog.ContainsKey(element.Key)) {
						rawLog.Add(element.Key, element.Value.Deserialize<JsonNode>());
					}
				} 
			}
		}

		return new StorageRequestLog(
			requestId: g.Key,
			performedByUser: performedByUser,
			aboutUser: aboutUser,
			endpoint: endpoint,
			date: date,
			requestSuccessful: requestSuccessful,
			raw: rawLog.ToJsonString()
		);
	}
	
}