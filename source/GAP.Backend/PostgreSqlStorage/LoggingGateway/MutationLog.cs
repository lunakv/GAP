﻿namespace PostgreSqlStorage.LoggingGateway {
	public partial class MutationLog {
		public DateTime? Date { get; set; }
		public string? Event { get; set; }
		public int Id { get; set; }
		public string? Message { get; set; }
		public int? AboutUser { get; set; }
		public string? Endpoint { get; set; }
		public int? PerformedByUser { get; set; }
		public string? RequestId { get; set; }
		public bool? RequestSuccessful { get; set; }
	}
}