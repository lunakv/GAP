﻿using Microsoft.EntityFrameworkCore;

namespace PostgreSqlStorage.LoggingGateway {
	public partial class LogsContext : DbContext {
		public LogsContext() { }

		public LogsContext(DbContextOptions<LogsContext> options)
			: base(options) { }

		public virtual DbSet<DebugLog> DebugLogs { get; set; } = null!;
		public virtual DbSet<MutationLog> MutationLogs { get; set; } = null!;

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) { }

		protected override void OnModelCreating(ModelBuilder modelBuilder) {
			modelBuilder.Entity<DebugLog>(entity => {
				entity.Property(e => e.Event).HasColumnType("jsonb");

				entity.Property(e => e.Level).HasMaxLength(50);
			});

			modelBuilder.Entity<MutationLog>(entity => { entity.Property(e => e.Event).HasColumnType("jsonb"); });

			OnModelCreatingPartial(modelBuilder);
		}

		partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
	}
}