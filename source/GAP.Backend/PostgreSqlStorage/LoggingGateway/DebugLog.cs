﻿namespace PostgreSqlStorage.LoggingGateway {
	public partial class DebugLog {
		public DateTime? Date { get; set; }
		public string? Event { get; set; }
		public string? Exception { get; set; }
		public int Id { get; set; }
		public string? Level { get; set; }
		public string? Message { get; set; }
	}
}