namespace PostgreSqlStorage.Exceptions; 

public class InvalidStorageUseException : Exception {
	public InvalidStorageUseException(string message) : base(message) {}
}