namespace PostgreSqlStorage.Exceptions; 

public class NavigationalPropertyNullException : InvalidStorageUseException {
	public NavigationalPropertyNullException(string message) : base(message) { }
}