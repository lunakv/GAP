using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;
using Auth.Registration;
using Auth.Registration.Objects;
using PostgreSqlStorage.UsersGateway;
using Users.GenData;
using Users.GenData.Haplogroup;
using Users.GenData.OriginalStrDataSources;
using Users.GenData.Parsers;
using Users.UserAccess.Entities;

namespace PostgreSqlStorage.RegistrationGateway.Models; 

public class RegistrationData {
	[Key]
	[DatabaseGenerated(DatabaseGeneratedOption.None)]
	public int UserId { get; set; }
	
	public bool AdministrationAgreementSigned { get; set; }
	public string PublicUserId { get; set; } = null!;

	public string GivenName { get; set; } = null!;
	public string FamilyName { get; set; } = null!;
	public string Email { get; set; } = null!;
	public string? PhoneNumber { get; set; }
	public string? ResidenceTown { get; set; }
	public string? ResidenceMunicipality { get; set; }
	public string? ResidenceCounty { get; set; }
	public string? ResidenceStreet { get; set; }
	public string? ResidenceZipCode { get; set; }

	public string? CorrespondenceTown { get; set; }
	public string? CorrespondenceStreet { get; set; }
	public string? CorrespondenceZipCode { get; set; }

	public DateOnly? BirthDate { get; set; }

	public string? AncestorGivenName { get; set; }
	public string? AncestorFamilyName { get; set; }
	public DateOnly? AncestorBirthDate { get; set; }
	public string? AncestorPlaceOfBirth { get; set; }
	public DateOnly? AncestorDeathDate { get; set; }
	public string? AncestorPlaceOfDeath { get; set; }
	public string? AncestorTown { get; set; }
	public string? AncestorMunicipality { get; set; }
	public string? AncestorCounty { get; set; }
	public string? AncestorStreet { get; set; }
	public string? AncestorZipCode { get; set; }

	
	public int RegionId { get; set; }

	public bool LabTestRequested { get; set; }
	
	public int? OriginalStrDataSourceId { get; set; }
	public string? TestProvider { get; set; } = null!;
	public JsonDocument? StrMarkers { get; set; }
	public string? FileName { get; set; }

	public int? HaplogroupId { get; set; }
	public string? HaplogroupName { get; set; } = null!;
	
	public RegistrationData() {}

	public RegistrationData(Auth.Registration.Objects.RegistrationData registrationData) {
		UserId = registrationData.UserId;
		AdministrationAgreementSigned = registrationData.User.AdministrationAgreementSigned;
		PublicUserId = registrationData.User.PublicId;
		
		GivenName = registrationData.User.Profile.GivenName;
		FamilyName = registrationData.User.Profile.FamilyName;
		Email = registrationData.User.Profile.Email;
		PhoneNumber = registrationData.User.Profile.PhoneNumber;
		ResidenceTown = registrationData.User.Profile.ResidenceAddress.Town;
		ResidenceMunicipality = registrationData.User.Profile.ResidenceAddress.Municipality;
		ResidenceCounty = registrationData.User.Profile.ResidenceAddress.County;
		ResidenceStreet = registrationData.User.Profile.ResidenceAddress.Street;
		ResidenceZipCode = registrationData.User.Profile.ResidenceAddress.ZipCode;

		CorrespondenceTown = registrationData.User.Profile.CorrespondenceAddress?.Town;
		CorrespondenceStreet = registrationData.User.Profile.CorrespondenceAddress?.Street;
		CorrespondenceZipCode = registrationData.User.Profile.CorrespondenceAddress?.ZipCode;

		BirthDate = registrationData.User.Profile.BirthDate;

		AncestorGivenName = registrationData.User.Ancestor.GivenName;
		AncestorFamilyName = registrationData.User.Ancestor.FamilyName;
		AncestorBirthDate = registrationData.User.Ancestor.BirthDate;
		AncestorPlaceOfBirth = registrationData.User.Ancestor.PlaceOfBirth;
		AncestorDeathDate = registrationData.User.Ancestor.DeathDate;
		AncestorPlaceOfDeath = registrationData.User.Ancestor.PlaceOfDeath;
		AncestorTown = registrationData.User.Ancestor.Address.Town;
		AncestorMunicipality = registrationData.User.Ancestor.Address.Municipality;
		AncestorCounty = registrationData.User.Ancestor.Address.County;
		AncestorStreet = registrationData.User.Ancestor.Address.Street;
		AncestorZipCode = registrationData.User.Ancestor.Address.ZipCode;
		
		RegionId = registrationData.User.RegionId;

		LabTestRequested = registrationData.LabTestRequested;
		
		OriginalStrDataSourceId = registrationData.UploadedStrDataSource?.Id;
		TestProvider = registrationData.UploadedStrDataSource?.TestProvider.Name;
		StrMarkers = registrationData.UploadedStrDataSource != null 
			? new StrMarkerParser().StrMarkersToJson(registrationData.UploadedStrDataSource.StrMarkers.CloneAsDictionary())
			: null;
		FileName = registrationData.UploadedStrDataSource?.FileName;

		HaplogroupId = registrationData.Haplogroup?.Id;
		HaplogroupName = registrationData.Haplogroup?.Name;
	}

	public Auth.Registration.Objects.RegistrationData ToRegistrationData() {
		Address? correspondceAddress = null;
		if (CorrespondenceTown != null) {
			correspondceAddress = new Address(
					town: CorrespondenceTown,
					street: CorrespondenceStreet!,
					zipCode: CorrespondenceZipCode!
			);
		}

		OriginalStrDataSource? originalStrDataSource = null;
		if (TestProvider != null) {
			originalStrDataSource = new OriginalStrDataSource(
				id: OriginalStrDataSourceId!.Value,
				userId: UserId,
				testProvider: new GeneticTestProvider(name: TestProvider),
				strMarkers: new StrMarkerCollection<string, int>(new StrMarkerParser().ParseGeneticData(StrMarkers!)),
				fileName: FileName
			);
		}

		Haplogroup? haplogroup = null;
		if (HaplogroupId != null) {
			haplogroup = new Haplogroup(id: HaplogroupId.Value, name: HaplogroupName!);
		}

		return new Auth.Registration.Objects.RegistrationData(
			userId: UserId,
			user: new RegistrationUser(
				administrationAgreementSigned: AdministrationAgreementSigned,
				publicId: PublicUserId,
				profile: new Profile(
					givenName: GivenName,
					familyName: FamilyName,
					email: Email,
					phoneNumber: PhoneNumber,
					residenceAddress: new ExpandedAddress(
						town: ResidenceTown,
						municipality: ResidenceMunicipality,
						county: ResidenceCounty,
						street: ResidenceStreet,
						zipCode: ResidenceZipCode
					),
					correspondenceAddress: correspondceAddress,
					birthDate: BirthDate
				),
				ancestor: new Ancestor(
					address: new ExpandedAddress(
						town: AncestorTown,
						municipality: AncestorMunicipality,
						county: AncestorCounty,
						street: AncestorStreet,
						zipCode: AncestorZipCode
					),
					givenName: AncestorGivenName,
					familyName: AncestorFamilyName,
					birthDate: AncestorBirthDate,
					placeOfBirth: AncestorPlaceOfBirth,
					deathDate: AncestorDeathDate,
					placeOfDeath: AncestorPlaceOfDeath
				),
				regionId: RegionId,
				password: string.Empty
			),
			labTestRequested: LabTestRequested,
			uploadedStrDataSource: originalStrDataSource,
			haplogroup: haplogroup
		);
	}
}