using Auth.Registration;
using Auth.Registration.Objects;
using Auth.Registration.Storage;
using PostgreSqlStorage.Data;

namespace PostgreSqlStorage.RegistrationGateway.Accessors;

public class RegistrationDataSaver : IRegistrationDataSaver {
	private readonly GapContext _context;

	public RegistrationDataSaver(GapContext context) {
		_context = context;
	}

	public Task AddRegistrationDataAsync(RegistrationData registrationData) {
		_context.RegistrationData.Add(new Models.RegistrationData(registrationData));

		return _context.SaveChangesAsync();
	}

	public async Task DeleteRegistrationDataAsync(int userId) {
		Models.RegistrationData? registrationData = await _context.RegistrationData.FindAsync(userId);
		if (registrationData != null) {
			_context.RegistrationData.Remove(registrationData);
			await _context.SaveChangesAsync();
		}
	}
}