using Auth.Registration;
using Auth.Registration.Objects;
using Auth.Registration.Storage;
using PostgreSqlStorage.Data;

namespace PostgreSqlStorage.RegistrationGateway.Accessors;

public class RegistrationDataFetcher : IRegistrationDataFetcher {
	private readonly GapContext _context;

	public RegistrationDataFetcher(GapContext context) {
		_context = context;
	}

	public async Task<RegistrationData> GetRegistrationDataAsync(int userId) {
		Models.RegistrationData? registrationData = await _context.RegistrationData.FindAsync(userId).ConfigureAwait(false);
		if (registrationData != null) {
			return registrationData.ToRegistrationData();
		}

		throw new ArgumentException($"No registration data for user {userId} not found in db.");
	}
}