﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostgreSqlStorage.FamilyTreeStorageGateway; 
public class FamilyTreeHolder
{
    public int Id { get; set; }
    public int UserId { get; set; }
    public string? Current { get; private set; }
    public string? Backup { get; private set; }

    private FamilyTreeHolder(string? current, string? backup, int id, int userId)
    {
        Current = current;
        Backup = backup;
        Id = id;
        UserId = userId;
    }
    public FamilyTreeHolder(int id, int userId)
        :this(null, null, id, userId)
    {}

    public FamilyTreeHolder Update(string? newTree)
    {
        return new FamilyTreeHolder(
            current: newTree,
            backup: Current,
            Id,
            UserId
        );
    }
}
