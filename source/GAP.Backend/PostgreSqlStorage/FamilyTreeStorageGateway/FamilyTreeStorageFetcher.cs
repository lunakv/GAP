﻿using PostgreSqlStorage.Data;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostgreSqlStorage.FamilyTreeStorageGateway; 
public class FamilyTreeStorageFetcher {
    private readonly GapContext _context;

    public FamilyTreeStorageFetcher(GapContext context) {
        _context = context;
    }
    
    public async Task<FamilyTreeHolder> GetFamilyTreeAsync(int treeId) {
        var tree = await _context.FamilyTrees
            .FirstOrDefaultAsync(t => t != null && t.Id == treeId);
        if (tree != null) {
            return tree.ToFamilyTreeHolder();
        }
        throw new ArgumentException($"No family tree with id {treeId} found in the database.");
    }
    public async Task<List<FamilyTreeHolder>> GetFamilyTreesAsync() {
        return  await _context.FamilyTrees
            .Select(t => t.ToFamilyTreeHolder())
            .ToListAsync().ConfigureAwait(false);
    }
}
