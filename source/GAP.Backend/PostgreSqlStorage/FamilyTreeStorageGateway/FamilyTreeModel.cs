﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostgreSqlStorage.FamilyTreeStorageGateway; 
public class FamilyTreeModel {

    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    public int UserId { get; set; }
    public string? TreeRepresentation { get; set; } = null;
    public string? TreeBackupRepresentation { get; set; } = null;

    public FamilyTreeHolder ToFamilyTreeHolder() {
        return new FamilyTreeHolder(Id, UserId)
            .Update(TreeBackupRepresentation)
            .Update(TreeRepresentation);
    }
}
