﻿using Microsoft.EntityFrameworkCore;
using PostgreSqlStorage.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostgreSqlStorage.FamilyTreeStorageGateway; 
public class FamilyTreeStorageSaver {
    private readonly GapContext _context;

    public FamilyTreeStorageSaver(GapContext context) {
        _context = context;
    }

    public async Task<int> AddFamilyTreeAsync(FamilyTreeHolder familyTree) {
        var ee = _context.FamilyTrees.Add(FromHolder(familyTree));
        await _context.SaveChangesAsync().ConfigureAwait(false);
        return ee.Entity.Id;
    }

    public async Task<FamilyTreeHolder> UpdateFamilyTreeAsync(FamilyTreeHolder familyTree) {
        var pre = await _context.FamilyTrees.FindAsync(familyTree.Id).ConfigureAwait(false);

        if (pre == null) {
            throw new ArgumentException(
                $"Attempted update of family tree {familyTree.Id} failed because no such tree was found in the database.");
        }

        pre.TreeRepresentation = familyTree.Current;
        pre.TreeBackupRepresentation = familyTree.Backup;

        await _context.SaveChangesAsync().ConfigureAwait(false);
        return pre.ToFamilyTreeHolder();
    }
    
    public async Task RemoveFamilyTreeAsync(int treeId) {
		var fromDb = await _context.FamilyTrees.FirstOrDefaultAsync(
            t => t != null && t.Id == treeId).ConfigureAwait(false);	
		if (fromDb != null) {
			_context.FamilyTrees.Remove(fromDb);
			
			await _context.SaveChangesAsync().ConfigureAwait(false);
		} else {
			throw new ArgumentException($"Family tree with id {treeId} is not present in db to be deleted.");
		}
    }

    private FamilyTreeModel FromHolder(FamilyTreeHolder tree) {
        return new FamilyTreeModel() {
            //Id = tree.Id, <- that would set id to -1, but it will be created by the db
            TreeRepresentation = tree.Current,
            TreeBackupRepresentation = tree.Backup,
            UserId = tree.UserId
        };
    }
}
