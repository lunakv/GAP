﻿using Map.RegionDataSaver;
using Map.ValueObjects;
using PostgreSqlStorage.Data;
using PostgreSqlStorage.MapGateway.Models;

namespace PostgreSqlStorage.MapGateway.Accessors;

/// <summary>
/// Adds administrative regions to PostgreSQL database.
/// </summary>
public class AdministrativeRegionLayerStorageSaver : IAdministrativeRegionLayerStorageSaver {
	private readonly GapContext _context;

	public AdministrativeRegionLayerStorageSaver(GapContext context) {
		_context = context;
	}

	public void AddLayers(IList<AdministrativeRegionLayer> layers) {
		if (!layers.Any()) {
			throw new ArgumentException("No layers added");
		}

		List<int> layerIds = new();
		foreach (AdministrativeRegionLayer layer in layers) {
			if (layerIds.Where(id => id == layer.Id).Any()) {
				throw new ArgumentException($"Duplicate layer id: {layer.Id}.");
			}

			layerIds.Add(layer.Id);
		}

		AddUnitRegionLayer(layers.First());

		foreach (AdministrativeRegionLayer layer in layers.Skip(1)) {
			AddRegionLayer(layer);
		}

		_context.SaveChanges();
	}

	private void AddUnitRegionLayer(AdministrativeRegionLayer layer) {
		foreach (AdministrativeRegion region in layer.Regions) {
			UnitRegionMapping regionMapping = new UnitRegionMapping() {
				UnitRegionId = region.Id,
				RegionId = region.Id
			};

			Region regionToAdd = new Region() {
				Id = region.Id,
				Type = region.Type,
				Name = region.Name,
				LayerId = layer.Id,
				ParentRegionId = region.ParentRegionId,
			};

			_context.UnitRegionMappings.Add(regionMapping);
			_context.Regions.Add(regionToAdd);
		}
	}

	private void AddRegionLayer(AdministrativeRegionLayer layer) {
		var previousLayerRegions = from previousLayerRegion in _context.Regions.Local
			where previousLayerRegion.LayerId == layer.Id - 1
			join unitRegionMapping in _context.UnitRegionMappings.Local on previousLayerRegion.Id equals unitRegionMapping.RegionId
			join unitRegion in _context.Regions.Local on unitRegionMapping.UnitRegionId equals unitRegion.Id
			select new { previousLayerRegion, unitRegion };

		foreach (AdministrativeRegion region in layer.Regions) {
			List<UnitRegionMapping> specificMappings = previousLayerRegions
				.Where(m => m.previousLayerRegion.ParentRegionId == region.Id)
				.Select(m => new UnitRegionMapping() { RegionId = region.Id, UnitRegionId = m.unitRegion.Id })
				.ToList();

			Region regionToAdd = new Region() {
				Id = region.Id,
				Type = region.Type,
				Name = region.Name,
				LayerId = layer.Id,
				ParentRegionId = region.ParentRegionId,
			};

			_context.UnitRegionMappings.AddRange(specificMappings);
			_context.Regions.Add(regionToAdd);
		}
	}
}