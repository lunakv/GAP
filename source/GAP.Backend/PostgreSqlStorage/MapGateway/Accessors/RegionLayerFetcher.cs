﻿using Map.Exceptions;
using Map.MapFetcher.Storage;
using Map.ValueObjects;
using Microsoft.EntityFrameworkCore;
using PostgreSqlStorage.Data;
using PostgreSqlStorage.MapGateway.Models;

namespace PostgreSqlStorage.MapGateway.Accessors;

public class RegionLayerFetcher : IRegionLayerFetcher {

    private readonly GapContext _context;

    public RegionLayerFetcher(GapContext context) {
        _context = context;
    }

    public async Task<HierachicalRegionLayer> GetHierarchicalLayerAsync(int layerId)
    {
        int layerRegionsCount = await _context.Regions.Where(r => r.LayerId == layerId).CountAsync().ConfigureAwait(false);
        
        IQueryable<Region> regions = _context.Regions.Where(r => r.LayerId == layerId);

        var regionsWithUnitRegions = await (
            from region in regions
            join unitRegionMapping in _context.UnitRegionMappings on region.Id equals unitRegionMapping.RegionId
            join unitRegion in _context.Regions on unitRegionMapping.UnitRegionId equals unitRegion.Id
            select new { unitRegion, region }
        ).ToListAsync().ConfigureAwait(false);
        
        if (layerRegionsCount == 0) {
            throw new LayerNotFoundException($"Layer {layerId} not found.");
        }
        
        var unitRegionsPerRegions = regionsWithUnitRegions
            .GroupBy(
                keySelector: a => a.region.Id,
                resultSelector: (regionId, regionWithUnitRegions) => new { regionId, regionWithUnitRegions }
            );

        List<HierarchicalRegion> layerRegions = new();

        foreach (var unitRegionsPerRegion in unitRegionsPerRegions) {

            if (!unitRegionsPerRegion.regionWithUnitRegions.Any()) {
                throw new RegionWithoutUnitRegionsException($"Region {unitRegionsPerRegion.regionId} does not contain any unit regions.");
            }
            Region region = unitRegionsPerRegion.regionWithUnitRegions.First().region;

            layerRegions.Add(new HierarchicalRegion(
                region: new AdministrativeRegion(
                    id: region.Id,
                    type: region.Type,
                    name: region.Name,
                    parentRegionId: region.ParentRegionId
                ),
                subregions: unitRegionsPerRegion.regionWithUnitRegions.Select(rur => 
                    new AdministrativeRegion(
                        id: rur.unitRegion.Id,
                        type: rur.unitRegion.Type,
                        name: rur.unitRegion.Name,
                        parentRegionId: rur.unitRegion.ParentRegionId
                    )
                ).ToList()
            ));
        }

        if (layerRegionsCount != layerRegions.Count) {
            throw new RegionWithoutUnitRegionsException("There is a region with no unit regions mapped.");
        }

        return new HierachicalRegionLayer(layerId, layerRegions);
    }

    public async Task<IList<int>> GetLayerStructureAsync() {
        List<int> layerStructure = await _context.Regions.Select(r => r.LayerId).Distinct().ToListAsync().ConfigureAwait(false);
        layerStructure.Sort();
        return layerStructure;
    }
}
