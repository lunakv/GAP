﻿using Map.Exceptions;
using Map.MapFetcher.Storage;
using Map.ValueObjects;
using Microsoft.EntityFrameworkCore;
using PostgreSqlStorage.Data;

namespace PostgreSqlStorage.MapGateway.Accessors;

/// <summary>
/// Fetches administrative regions from PostgreSQL database.
/// </summary>
public class RegionFetcher : IRegionFetcher {
	private readonly GapContext _context;

	public RegionFetcher(GapContext context) {
		_context = context;
	}

	public async Task<HierarchicalRegion> GetHierarchicalRegionByIdAsync(int id) {
		var regions = await (
			from region in _context.Regions
			where region.Id == id
			join unitRegionMapping in _context.UnitRegionMappings on region.Id equals unitRegionMapping.RegionId
			join unitRegion in _context.Regions on unitRegionMapping.UnitRegionId equals unitRegion.Id
			select new { region, unitRegion }
		).ToListAsync().ConfigureAwait(false);

		if (!regions.Any()) {
			throw new RegionNotFoundException($"Region with id {id} not found.");
		}

		var first = regions.First();

		return new HierarchicalRegion(
			region: new AdministrativeRegion(
				id: first.region.Id,
				type: first.region.Type,
				name: first.region.Name,
				parentRegionId: first.region.ParentRegionId),
			subregions: regions.Select(r => new AdministrativeRegion(
				id: r.unitRegion.Id,
				type: r.unitRegion.Type,
				name: r.unitRegion.Name,
				parentRegionId: r.unitRegion.ParentRegionId
			)).ToList()
		);
	}

	public async Task<IList<AdministrativeRegion>> GetParentRegionsAsync(int unitRegionId) {
		var parentRegions = from unitRegionMapping in _context.UnitRegionMappings.Where(mapping => mapping.UnitRegionId == unitRegionId)
			join region in _context.Regions on unitRegionMapping.RegionId equals region.Id
			select region;
		return await parentRegions
			.OrderBy(region => region.LayerId)
			.Select(region => new AdministrativeRegion(region.Id, region.Type, region.Name, region.ParentRegionId))
			.ToListAsync().ConfigureAwait(false);
	}

	public async Task<AdministrativeRegion> GetMunicipalityRegionByName(string municipalityName) {
		Models.Region? unitRegion = await _context.Regions.FirstOrDefaultAsync(
			region => region.Type == AdministrativeRegion.RegionType.Municipality && region.Name == municipalityName
		).ConfigureAwait(false);
		if (unitRegion == null) {
			throw new RegionNotFoundException($"Municipality unit region {municipalityName} not found in db.");
		}

		return unitRegion.ToAdministrativeRegion();
	}

	public async Task<IList<AdministrativeRegion>> GetAdministrativeRegionsAsync(AdministrativeRegion.RegionType regionType) {
		IList<AdministrativeRegion> regions = await _context.Regions
			.Where(region => region.Type == regionType)
			.Select(region => region.ToAdministrativeRegion())
			.ToListAsync().ConfigureAwait(false);
		return regions;
	}
}