﻿namespace PostgreSqlStorage.MapGateway.Models;

public class UnitRegionMapping {
    public int RegionId { get; set; }
    public Region Region { get; set; } = null!;
    public int UnitRegionId { get; set; }
    public Region UnitRegion { get; set; } = null!;
}
