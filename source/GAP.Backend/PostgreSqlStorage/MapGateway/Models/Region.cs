﻿using System.ComponentModel.DataAnnotations.Schema;
using Map.ValueObjects;

namespace PostgreSqlStorage.MapGateway.Models;

public class Region {
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public int Id { get; set; }
    public AdministrativeRegion.RegionType Type { get; set; }
    public string Name { get; set; } = null!;
    public int LayerId { get; set; }
    public int? ParentRegionId { get; set; } = null!;
    public Region? ParentRegion { get; set; } = null!;

    public AdministrativeRegion ToAdministrativeRegion() {
        return new AdministrativeRegion(id: Id, type: Type, name: Name, parentRegionId: ParentRegionId);
    }
}
