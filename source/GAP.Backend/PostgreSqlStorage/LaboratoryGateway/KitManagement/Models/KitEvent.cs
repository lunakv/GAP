using System.ComponentModel.DataAnnotations.Schema;
using PostgreSqlStorage.LaboratoryGateway.KitManagement.Exceptions;

namespace PostgreSqlStorage.LaboratoryGateway.KitManagement.Models;

using DomainKitEventNamespace = Laboratory.KitManagement.Events;
using DomainKitEvent = Laboratory.KitManagement.Events.KitEvent;

public class KitEvent {
	public DomainKitEvent.Event Event { get; set; }
	public DateTime Date { get; set; }
	public string Note { get; set; } = null!;

	public KitEvent() { }

	public KitEvent(DomainKitEvent kitEvent) {
		Event = kitEvent.GetEvent();
		Date = kitEvent.Date;
		Note = kitEvent.Note;
	}

	public DomainKitEvent ToKitEvent() {
		return Event switch {
			DomainKitEvent.Event.Create => new DomainKitEventNamespace.CreateKitEvent(date: Date, note: Note),
			DomainKitEvent.Event.Send => new DomainKitEventNamespace.SendKitEvent(date: Date, note: Note),
			DomainKitEvent.Event.Receive => new DomainKitEventNamespace.ReceiveKitEvent(date: Date, note: Note),
			DomainKitEvent.Event.Analyse => new DomainKitEventNamespace.AnalyseKitEvent(date: Date, note: Note),
			_ => throw new KitEventEnumNotMatchingKitEventException($"{Event} does not match any KitEvent.")
		};
	}
}