using System.ComponentModel.DataAnnotations.Schema;
using PostgreSqlStorage.LaboratoryGateway.KitManagement.Exceptions;
using Users.UserAccess.Entities;
using Users.UserAccess.Entities.Interfaces;

namespace PostgreSqlStorage.LaboratoryGateway.KitManagement.Models;

using DomainKitEventNamespace = Laboratory.KitManagement.Events;
using DomainKitNamespace = Laboratory.KitManagement.Kits;
using DomainKit = Laboratory.KitManagement.Kits.Kit<IHasUserId>;

public class Kit {
	[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
	public int Id { get; set; }

	public DomainKit.KitStage KitStage { get; set; }

	[Column(TypeName = PostgresTypes.BinaryJson)]
	public List<KitEvent> History { get; set; } = null!;

	public int UserId { get; set; }
	public string Note { get; set; } = null!;
	public bool ActionRequired { get; set; }
	public bool Active { get; set; }

	public Kit() { }

	public Kit(DomainKit kit) {
		Set(kit);
	}

	public void Set(DomainKit kit) {
		KitStage = kit.Stage;
		History = kit.History.Select(kitEvent => new KitEvent(kitEvent)).ToList();
		UserId = kit.User.Id;
		Note = kit.Note;
		ActionRequired = kit.ActionRequired;
		Active = kit.Active;
	}

	public DomainKit ToKit() {
		DomainKit.KitStage[] stages = Enum.GetValues<DomainKit.KitStage>();
		IReadOnlyCollection<DomainKitEventNamespace.KitEvent> history = History.Select(kitEvent => kitEvent.ToKitEvent()).ToList();
		IHasUserId user = new IdUser(id: UserId);
		return KitStage switch {
			DomainKit.KitStage.Pending => new DomainKitNamespace.PendingKit<IHasUserId>(id: Id, history: history, user: user, note: Note),
			DomainKit.KitStage.Sent => new DomainKitNamespace.SentKit<IHasUserId>(id: Id, history: history, user: user, note: Note),
			DomainKit.KitStage.Received => new DomainKitNamespace.ReceivedKit<IHasUserId>(id: Id, history: history, user: user, note: Note),
			DomainKit.KitStage.Analysed => new DomainKitNamespace.AnalysedKit<IHasUserId>(id: Id, history: history, user: user, note: Note),
			_ => throw new KitStageEnumNotMatchingKitException($"Kit Stage enum no matched by any Kit type.")
		};
	}
}