using Laboratory.KitManagement.Storage;
using Microsoft.EntityFrameworkCore;
using PostgreSqlStorage.Data;
using PostgreSqlStorage.LaboratoryGateway.KitManagement.Models;
using Users.UserAccess.Entities.Interfaces;
using DomainKitEventNamespace = Laboratory.KitManagement.Events;
using DomainKitNamespace = Laboratory.KitManagement.Kits;

namespace PostgreSqlStorage.LaboratoryGateway.KitManagement.Accessors;

public class KitSaver : IKitSaver<IHasUserId> {
	private readonly GapContext _context;

	public KitSaver(GapContext context) {
		_context = context;
	}

	public Task AddKitAsync(DomainKitNamespace.Kit<IHasUserId> kit) {
		return AddKitsAsync(new[] { kit });
	}

	public async Task AddKitsAsync(IReadOnlyCollection<DomainKitNamespace.Kit<IHasUserId>> kits) {
		await _context.Kits.AddRangeAsync(kits.Select(kit => new Kit(kit)));
		await _context.SaveChangesAsync().ConfigureAwait(false);
	}

	public async Task UpdateKitsAsync(IReadOnlyCollection<DomainKitNamespace.Kit<IHasUserId>> kits) {
		var kitIds = kits.Select(kit => kit.Id).ToList();

		var dbKits = await _context.Kits
			.Where(kit => kitIds.Contains(kit.Id))
			.ToDictionaryAsync(kit => kit.Id, kit => kit)
			.ConfigureAwait(false);
		foreach (var kit in kits) {
			dbKits[kit.Id].Set(kit);
		}

		await _context.SaveChangesAsync().ConfigureAwait(false);
	}
}