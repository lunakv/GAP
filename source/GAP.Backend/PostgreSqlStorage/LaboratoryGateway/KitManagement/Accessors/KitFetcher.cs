using System.Text;
using Laboratory.KitManagement.Exceptions;
using Laboratory.KitManagement.Kits;
using Laboratory.KitManagement.Storage;
using Microsoft.EntityFrameworkCore;
using PostgreSqlStorage.Data;
using PostgreSqlStorage.LaboratoryGateway.KitManagement.Models;
using Users.Exceptions;
using Users.UserAccess.Entities.Interfaces;

namespace PostgreSqlStorage.LaboratoryGateway.KitManagement.Accessors;

public class KitFetcher : IKitFetcher<IHasUserId> {
	private readonly GapContext _context;

	public KitFetcher(GapContext context) {
		_context = context;
	}

	public async Task<IReadOnlyCollection<Kit<IHasUserId>>> GetKitsAsync(KitStorageFilter? filter) {
		IQueryable<Kit> storageKits = _context.Kits;
		if (filter != null) {
			storageKits = storageKits.Where(kit => !filter.Active.HasValue || filter.Active == kit.Active);
			storageKits = storageKits.Where(kit => !filter.ActionRequired.HasValue || filter.ActionRequired == kit.ActionRequired);
		}

		var kits = await storageKits.ToListAsync().ConfigureAwait(false);
		return kits.Select(kit => kit.ToKit()).ToList();
	}

	public async Task<IReadOnlyDictionary<int, Kit<IHasUserId>>> FindKitsById(IReadOnlyCollection<int> kitIds) {
		List<int> distinctKitIds = kitIds.Distinct().ToList();
		IList<Kit> kits = await _context.Kits.Where(kit => distinctKitIds.Contains(kit.Id)).ToListAsync().ConfigureAwait(false);

		if (distinctKitIds.Count != kits.Count) {
			string notFoundInfo = ExceptionUtils.CreateIdDiff(requestedIds: distinctKitIds, foundIds: kits.Select(kit => kit.Id));
			throw new KitNotFoundException($"Kits {notFoundInfo} not found");
		}

		return kits.Select(kit => kit.ToKit()).ToDictionary(kit => kit.Id, kit => kit);
	}
}