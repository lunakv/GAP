using PostgreSqlStorage.LaboratoryGateway.Exceptions;

namespace PostgreSqlStorage.LaboratoryGateway.KitManagement.Exceptions; 

public class KitStageEnumNotMatchingKitException : LaboratoryStorageException {
	public KitStageEnumNotMatchingKitException(string message) : base(message) { }
}