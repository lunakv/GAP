using PostgreSqlStorage.LaboratoryGateway.Exceptions;

namespace PostgreSqlStorage.LaboratoryGateway.KitManagement.Exceptions;

public class KitEventEnumNotMatchingKitEventException : LaboratoryStorageException {
	public KitEventEnumNotMatchingKitEventException(string message) : base(message) { }
}