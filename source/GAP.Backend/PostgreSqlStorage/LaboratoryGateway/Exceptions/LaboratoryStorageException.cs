using Laboratory;
using Laboratory.Exceptions;

namespace PostgreSqlStorage.LaboratoryGateway.Exceptions; 

public class LaboratoryStorageException : LaboratoryException {
	public LaboratoryStorageException(string message) : base(message) { }
}