using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PostgreSqlStorage.LaboratoryGateway.LabResults.Models; 

public class SequencerInput {
	[Key]
	[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
	public int PlateNumber { get; set; }
}