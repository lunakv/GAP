using Laboratory.SequencerInputGeneration.Input;
using Laboratory.SequencerInputGeneration.Storage;
using PostgreSqlStorage.Data;

namespace PostgreSqlStorage.LaboratoryGateway.LabResults.Accessors; 

public class SequencerInputFetcher : ISequencerInputFetcher {
	private readonly GapContext _context;

	public SequencerInputFetcher(GapContext context) {
		_context = context;
	}

	public async Task<int> CreateNewSequencerInput() {
		var savedSequencerInput = _context.SequencerInputs.Add(new Models.SequencerInput() { });
		await _context.SaveChangesAsync().ConfigureAwait(false);
		return savedSequencerInput.Entity.PlateNumber;
	}
}