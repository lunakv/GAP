using System.ComponentModel.DataAnnotations;

namespace PostgreSqlStorage.UsersGateway.GenData.Models;

public class StrConflict {
	[Key]
	public int UserId { get; set; }
	public List<string> ConflictingMarkers { get; set; } = null!;

	public Users.GenData.Conflict.StrConflict ToStrConflict() {
		return new Users.GenData.Conflict.StrConflict(
			userId: UserId,
			conflictingMarkers: ConflictingMarkers
		);
	}
}