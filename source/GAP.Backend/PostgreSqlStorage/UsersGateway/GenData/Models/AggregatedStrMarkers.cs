using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;
using PostgreSqlStorage.UsersGateway.UserAccess.Models;

namespace PostgreSqlStorage.UsersGateway.GenData.Models; 

public class AggregatedStrMarkers {
	[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
	public int Id { get; set; }
	
	[ForeignKey(nameof(User))]
	public int UserId { get; set; }
	public User User { get; set; } = null!;
	public JsonDocument StrMarkers { get; set; } = null!;

	public void Set(JsonDocument strMarkers) {
		StrMarkers = strMarkers;
	}
}