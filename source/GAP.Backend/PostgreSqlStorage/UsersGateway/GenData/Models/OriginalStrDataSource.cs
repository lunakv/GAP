using System.Text.Json;
using PostgreSqlStorage.UsersGateway.UserAccess.Models;
using Users.GenData;
using Users.GenData.Parsers;

namespace PostgreSqlStorage.UsersGateway.GenData.Models; 

public class OriginalStrDataSource {
	public int Id { get; set; }
	public JsonDocument StrMarkers { get; set; } = null!;
	public string? FileName { get; set; } = null!;
	
	public int UserId { get; set; }
	public User User { get; set; } = null!;
	
	public string GeneticTestProvider { get; set; } = null!;

	public Users.GenData.OriginalStrDataSources.OriginalStrDataSource ToOriginalStrDataSource() {
		return new Users.GenData.OriginalStrDataSources.OriginalStrDataSource(
			id: Id,
			userId: UserId,
			testProvider: new GeneticTestProvider(GeneticTestProvider),
			strMarkers: new StrMarkerCollection<string, int>(new StrMarkerParser().ParseGeneticData(StrMarkers)),
			fileName: FileName
		);
	}
	
}