﻿using System.ComponentModel.DataAnnotations.Schema;
using PostgreSqlStorage.UsersGateway.UserAccess.Models;

namespace PostgreSqlStorage.UsersGateway.GenData.Models;

public class Haplogroup {

    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    public string Name { get; set; } = null!;
    
    public ICollection<User> Users { get; set; } = null!;

    public global::Users.GenData.Haplogroup.Haplogroup ToHaplogroup() => new global::Users.GenData.Haplogroup.Haplogroup(id: Id, name: Name);
}
