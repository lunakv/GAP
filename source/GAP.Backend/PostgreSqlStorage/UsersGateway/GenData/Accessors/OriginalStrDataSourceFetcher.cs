using Microsoft.EntityFrameworkCore;
using PostgreSqlStorage.Data;
using PostgreSqlStorage.UsersGateway.UserAccess.Models;
using Users.Exceptions;
using Users.GenData.OriginalStrDataSources.Exceptions;
using Users.GenData.OriginalStrDataSources.Storage;
using OriginalStrDataSource = Users.GenData.OriginalStrDataSources.OriginalStrDataSource;

namespace PostgreSqlStorage.UsersGateway.GenData.Accessors;

public class OriginalStrDataSourceFetcher : IOriginalStrDataSourceFetcher {
	private readonly GapContext _context;

	public OriginalStrDataSourceFetcher(GapContext context) {
		_context = context;
	}

	public async Task<IList<OriginalStrDataSource>> GetOriginalStrDataSourceAsync(int userId) {
		User? user = await _context.Users
			.Include(u => u.OriginalStrDataSources)
			.FirstOrDefaultAsync<User?>(u => u != null && u.Id == userId).ConfigureAwait(false);

		if (user != null) {
			return user.OriginalStrDataSources.Select(strData => strData.ToOriginalStrDataSource()).ToList();
		}

		throw new OriginalStrDataSourceNotFoundException($"No user with {userId} found in database.");
	}

	public async Task<IDictionary<int, OriginalStrDataSource>> GetOriginalStrDataSourcesAsync(IReadOnlyCollection<int> sourceIds) {
		var distinctSourceIds = sourceIds.Distinct().ToList();
		Dictionary<int, OriginalStrDataSource> sources = await _context.OriginalStrDataSources
			.Where(source => distinctSourceIds.Contains(source.Id))
			.ToDictionaryAsync(source => source.Id, source => source.ToOriginalStrDataSource());

		if (sources.Keys.Count != distinctSourceIds.Count) {
			string notFoundInfo = ExceptionUtils.CreateIdDiff(requestedIds: distinctSourceIds, foundIds: sources.Keys);
			throw new OriginalStrDataSourceNotFoundException($"Original str data sources {notFoundInfo} not found");
		}

		return sources;
	}

	public async Task<ICollection<OriginalStrDataSource>> TryGetOriginalStrDataSourceAsync(string fileName) {
		return await _context.OriginalStrDataSources.Where(source => source.FileName == fileName)
			.Select(source => source.ToOriginalStrDataSource()).ToListAsync();
	}
}