using PostgreSqlStorage.Data;
using Users.GenData.OriginalStrDataSources;
using Users.GenData.OriginalStrDataSources.Storage;

namespace PostgreSqlStorage.UsersGateway.GenData.Accessors; 

public class OriginalStrDataSourceSaver : IOriginalStrDataSourceSaver {

	private readonly GapContext _context;

	public OriginalStrDataSourceSaver(GapContext context) {
		_context = context;
	}
	
	public async Task<OriginalStrDataSource> AddOriginalStrDataSourceAsync(OriginalStrDataSource originalStrDataSource) {
		var dataEntity = await _context.OriginalStrDataSources.AddAsync(new UsersGateway.GenData.Models.OriginalStrDataSource() {
			UserId = originalStrDataSource.UserId,
			StrMarkers = new StrMarkerParser().StrMarkersToJson(originalStrDataSource.StrMarkers.CloneAsDictionary()),
			GeneticTestProvider = originalStrDataSource.TestProvider.Name,
			FileName = originalStrDataSource.FileName
		}).ConfigureAwait(false);

		await _context.SaveChangesAsync().ConfigureAwait(false);

		return dataEntity.Entity.ToOriginalStrDataSource();
	}
}