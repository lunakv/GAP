using PostgreSqlStorage.Data;
using PostgreSqlStorage.UsersGateway.GenData.Models;
using Users.GenData.Conflict.Storage;

namespace PostgreSqlStorage.UsersGateway.GenData.Accessors;

public class StrConflictSaver : IStrConflictSaver {
	private readonly GapContext _context;

	public StrConflictSaver(GapContext context) {
		_context = context;
	}

	public async Task DeleteConflictAsync(int userId) {
		var conflictFetcher = new StrConflictFetcher(_context);
		StrConflict conflict = await conflictFetcher.GetDbStrConflictAsync(userId: userId).ConfigureAwait(false);

		_context.StrConflicts.Remove(conflict);
		await _context.SaveChangesAsync().ConfigureAwait(false);
	}

	public async Task AddConflictAsync(int userId, IReadOnlyCollection<string> conflictingMarkers) {
		StrConflict? conflict = await _context.StrConflicts.FindAsync(userId).ConfigureAwait(false);
		if (conflict != null) {
			_context.StrConflicts.Remove(conflict);
			_context.Add(new StrConflict() {
				UserId = userId,
				ConflictingMarkers = conflictingMarkers.ToList()
			});
		} else {
			await _context.AddAsync(new StrConflict() {
				UserId = userId,
				ConflictingMarkers = conflictingMarkers.ToList()
			}).ConfigureAwait(false);
		}

		await _context.SaveChangesAsync().ConfigureAwait(false);
	}
}