﻿using Microsoft.EntityFrameworkCore;
using PostgreSqlStorage.Data;
using Users.GenData.Haplogroup;
using Users.GenData.Haplogroup.Exceptions;
using Users.GenData.Haplogroup.Storage;
using Users.UserAccess.UserFilter;

namespace PostgreSqlStorage.UsersGateway.GenData.Accessors;

public class HaplogroupFetcher : IHaplogroupFetcher {

    private readonly GapContext _context;

    public HaplogroupFetcher(GapContext context) {
        _context = context;
    }

    public async Task<Haplogroup> FindHaplogroupByIdAsync(int haplogroupId) {
        UsersGateway.GenData.Models.Haplogroup? haplogroup = await _context.Haplogroups.FindAsync(haplogroupId).ConfigureAwait(false);
        if (haplogroup != null) {
            return haplogroup.ToHaplogroup();
        }

        throw new HaplogroupNotFoundException($"Haplogroup {haplogroupId} not found in database.");
    }

    public async Task<IList<Haplogroup>> GetHaplogroupsAsync() {
        return await _context.Haplogroups.Select(h => h.ToHaplogroup()).ToListAsync().ConfigureAwait(false);
    }

    public async Task<IList<Haplogroup>> GetHaplogroupsAsync(IHaplogroupFilter haplogroupFilter) {
        IList<Haplogroup> haplogroups = await GetHaplogroupsAsync().ConfigureAwait(false);
        return haplogroupFilter.Filter(haplogroups: haplogroups).ToList();
    }
}
