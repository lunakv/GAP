using Microsoft.EntityFrameworkCore;
using PostgreSqlStorage.Data;
using Users.Exceptions;
using Users.GenData.Conflict;
using Users.GenData.Conflict.Exceptions;
using Users.GenData.Conflict.Storage;

namespace PostgreSqlStorage.UsersGateway.GenData.Accessors;

public class StrConflictFetcher : IStrConflictFetcher {
	private readonly GapContext _context;

	public StrConflictFetcher(GapContext context) {
		_context = context;
	}

	public async Task<ICollection<StrConflict>> GetConflictsAsync(IReadOnlyCollection<int>? userIds = null) {
		List<int>? distinctUserIds = userIds?.Distinct().ToList();

		var foundConflicts = await TryGetConflictsAsync(userIds: distinctUserIds).ConfigureAwait(false);

		if (distinctUserIds != null && foundConflicts.Count != distinctUserIds.Count) {
			string notFoundInfo = ExceptionUtils.CreateIdDiff(requestedIds: distinctUserIds, foundIds: foundConflicts.Select(conflict => conflict.UserId));
			throw new StrConflictNotFoundException($"Conflicts {notFoundInfo} not found in database.");
		}

		return foundConflicts;
	}

	public async Task<ICollection<StrConflict>> TryGetConflictsAsync(IReadOnlyCollection<int>? userIds = null) {
		List<int>? distinctUserIds = userIds?.Distinct().ToList();
		IQueryable<UsersGateway.GenData.Models.StrConflict> conflicts = _context.StrConflicts;
		if (distinctUserIds != null) {
			conflicts = conflicts.Where(conflict => distinctUserIds.Contains(conflict.UserId));
		}

		var foundConflicts = await conflicts.Select(conflict => conflict.ToStrConflict()).ToListAsync().ConfigureAwait(false);

		return foundConflicts;
	}

	public async Task<StrConflict> GetUserStrConflictAsync(int userId) {
		UsersGateway.GenData.Models.StrConflict conflict = await GetDbStrConflictAsync(userId).ConfigureAwait(false);
		return conflict.ToStrConflict();
	}

	internal async Task<UsersGateway.GenData.Models.StrConflict> GetDbStrConflictAsync(int userId) {
		UsersGateway.GenData.Models.StrConflict? conflict = await _context.StrConflicts.FindAsync(userId);
		if (conflict != null) {
			return conflict;
		}

		throw new StrConflictNotFoundException($"Str conflict for user {userId} not found.");
	}
}