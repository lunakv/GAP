﻿using PostgreSqlStorage.Data;
using Users.GenData.Haplogroup;
using Users.GenData.Haplogroup.Storage;

namespace PostgreSqlStorage.UsersGateway.GenData.Accessors;

public class HaplogroupSaver : IHaplogroupSaver {

    private readonly GapContext _context;

    public HaplogroupSaver(GapContext context) {
        _context = context;
    }

    public async Task AddHaplogroupAsync(string haplogroupName) {
        await AddHaplogroupsAsync(new[] { haplogroupName }).ConfigureAwait(false);
    }

    public async Task AddHaplogroupsAsync(IList<string> haplogroupNames) {
        _context.Haplogroups.AddRange(haplogroupNames.Select(h => new UsersGateway.GenData.Models.Haplogroup() {
            Name = h
        }));

        await _context.SaveChangesAsync().ConfigureAwait(false);
    }
}
