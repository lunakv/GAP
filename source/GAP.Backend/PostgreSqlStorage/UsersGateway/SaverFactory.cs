using Microsoft.EntityFrameworkCore.Storage;
using PostgreSqlStorage.Data;
using PostgreSqlStorage.UsersGateway.GenData.Accessors;
using PostgreSqlStorage.UsersGateway.UserAccess.Accessors;
using Users;
using Users.GenData.Conflict.Storage;
using Users.GenData.Haplogroup.Storage;
using Users.GenData.OriginalStrDataSources.Storage;
using Users.UserAccess.Storage;

namespace PostgreSqlStorage.UsersGateway;

public class SaverFactory : ISaverFactory {
	private readonly GapContext _context;
	private IDbContextTransaction? _transaction;

	public SaverFactory(GapContext context) {
		_context = context;
		_transaction = null;
	}

	public IUserSaver GetUserSaver() {
		// CheckActiveTransaction();
		return new UserSaver(_context);
	}

	public IStrConflictSaver GetStrConflictSaver() {
		// CheckActiveTransaction();
		return new StrConflictSaver(_context);
	}

	public IHaplogroupSaver GetHaplogroupSaver() {
		// CheckActiveTransaction();
		return new HaplogroupSaver(_context);
	}

	public IOriginalStrDataSourceSaver GetOriginalStrDataSourceSaver() {
		// CheckActiveTransaction();
		return new OriginalStrDataSourceSaver(_context);
	}

	public async Task StartTransactionAsync() {
		if (_transaction == null) {
			_transaction = await _context.Database.BeginTransactionAsync().ConfigureAwait(false);
		} else {
			throw new InvalidOperationException("Transaction cannot be started twice.");
		}
	}

	public Task CommitAsync() {
		CheckActiveTransaction();
		return _transaction!.CommitAsync();
	}

	public async ValueTask DisposeAsync() {
		if (_transaction != null) {
			await _transaction.DisposeAsync().ConfigureAwait(false);
		}

		await _context.DisposeAsync().ConfigureAwait(false);
	}

	private void CheckActiveTransaction() {
		if (_transaction == null) {
			throw new InvalidOperationException(
				"Transaction in TransactionalFactorySaver must be started via StartTransactionAsync before any concrete methods are called."
			);
		}
	}
}