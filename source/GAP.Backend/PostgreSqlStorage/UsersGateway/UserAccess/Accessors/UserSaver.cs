﻿using Microsoft.EntityFrameworkCore;
using PostgreSqlStorage.Data;
using Users.GenData;
using Users.GenData.Haplogroup.Exceptions;
using Users.UserAccess.Entities;
using Users.UserAccess.Exceptions;
using Users.UserAccess.Storage;
using Haplogroup = PostgreSqlStorage.UsersGateway.GenData.Models.Haplogroup;
using User = Users.UserAccess.Entities.User;

namespace PostgreSqlStorage.UsersGateway.UserAccess.Accessors;

public class UserSaver : IUserSaver {
	private readonly GapContext _context;

	public UserSaver(GapContext context) {
		_context = context;
	}

	public async Task AddUserAsync(User user) {
		if (user.GeneticData != null) {
			Haplogroup? haplogroup = await _context.Haplogroups.FindAsync(user.GeneticData.Haplogroup.Id).ConfigureAwait(false);

			if (haplogroup == null) {
				throw new HaplogroupNotFoundException(
					$"Haplogroup {user.GeneticData.Haplogroup.Id} not found in database when adding user {user.Id}."
				);
			}
		}
		
		Models.User dbUser = new Models.User();
		dbUser.Set(user: user);
		dbUser.AddedAt = DateTime.UtcNow;
		_context.Users.Add(dbUser);
		
		await _context.SaveChangesAsync().ConfigureAwait(false);
	}

	public async Task UpdateProfileAsync(int userId, Profile profile, int? regionId) {
		Models.User currentUser = await FindUserAsync(userId: userId).ConfigureAwait(false);
		await _context.Entry(currentUser).Reference(u => u.Profile).LoadAsync().ConfigureAwait(false);
		await _context.Entry(currentUser.Profile).Reference(p => p.Address).LoadAsync().ConfigureAwait(false);
		currentUser.SetProfile(profile: profile);
		if (regionId != null) {
			currentUser.UnitRegionId = regionId.Value;
		}
		await _context.SaveChangesAsync().ConfigureAwait(false);
	}

	public async Task UpdateAncestorAsync(int userId, Ancestor ancestor) {
		Models.User currentUser = await FindUserAsync(userId: userId).ConfigureAwait(false);
		await _context.Entry(currentUser).Reference(u => u.Ancestor).LoadAsync().ConfigureAwait(false);
		currentUser.Ancestor.Set(ancestor: ancestor, userId: userId);
		await _context.SaveChangesAsync().ConfigureAwait(false);
	}

	public async Task UpdateGeneticDataAsync(int userId, GeneticData geneticData) {
		Models.User currentUser = await FindUserAsync(userId: userId).ConfigureAwait(false);
		await _context.Entry(currentUser).Reference(u => u.AggregatedStrMarkers).LoadAsync().ConfigureAwait(false);
		currentUser.SetGeneticData(geneticData: geneticData);
		await _context.SaveChangesAsync().ConfigureAwait(false);
	}

	public async Task UpdateAdministrationAgreementSignStatus(int userId, bool administrationAggreementSigned) {
		Models.User currentUser = await FindUserAsync(userId: userId).ConfigureAwait(false);
		currentUser.AdministrationAgreementSigned = administrationAggreementSigned;
		await _context.SaveChangesAsync();
	}

	public async Task UpdateEmailAsync(int userId, string email) {
		Models.User currentUser = await FindUserAsync(userId: userId).ConfigureAwait(false);
		currentUser.Email = email;
		await _context.SaveChangesAsync().ConfigureAwait(false);
	}

	private async Task<Models.User> FindUserAsync(int userId) {
		Models.User? user = await _context.Users.FindAsync(userId).ConfigureAwait(false);
		if (user != null) {
			return user;
		}

		throw new UserNotFoundException($"User {userId} not found in database.");
	}
}