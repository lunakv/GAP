﻿using Microsoft.EntityFrameworkCore;
using PostgreSqlStorage.Data;
using Users.Exceptions;
using Users.GenData.Haplogroup;
using Users.UserAccess.Entities;
using Users.UserAccess.Exceptions;
using Users.UserAccess.Storage;

namespace PostgreSqlStorage.UsersGateway.UserAccess.Accessors;

public class UserFetcher : IUserFetcher {
	private readonly GapContext _context;

	public UserFetcher(GapContext context) {
		_context = context;
	}

	public async Task<User> FindUserByIdAsync(int userId) {
		Models.User? user = await IncludeEverything(_context.Users)
			.FirstOrDefaultAsync<Models.User?>(u => u != null && u.Id == userId).ConfigureAwait(false);

		if (user != null) {
			return user.ToUser();
		}

		throw new UserNotFoundException($"No user with {userId} found in database.");
	}

	public async Task<IReadOnlyDictionary<int, User>> FindUsersByIdAsync(IReadOnlyCollection<int> userIds) {
		List<int> distinctUserIds = userIds.Distinct().ToList();
		
		Dictionary<int, User> users = await IncludeEverything(_context.Users)
			.Where(u => distinctUserIds.Contains(u.Id))
			.Select(u => u.ToUser())
			.ToDictionaryAsync(user => user.Id, user => user).ConfigureAwait(false);

		if (users.Keys.Count != distinctUserIds.Count) {
			var notFoundInfo = ExceptionUtils.CreateIdDiff(requestedIds: distinctUserIds, foundIds: users.Keys);
			throw new UserNotFoundException($"Users {notFoundInfo} not found in database.");
		}

		return users;
	}

	public async Task<User> FindUserByEmailAsync(string email) {
		Models.User? user = await IncludeEverything(_context.Users)
			.FirstOrDefaultAsync<Models.User?>(u => u != null && u.Email == email).ConfigureAwait(false);

		if (user != null) {
			return user.ToUser();
		}

		throw new UserNotFoundException($"No user with {email} found in database.");
	}

	public async Task<IList<User>> GetUsersAsync() {
		return await IncludeEverything(_context.Users)
			.Select(u => u.ToUser())
			.ToListAsync().ConfigureAwait(false);
	}

	private IQueryable<Models.User> IncludeEverything(IQueryable<Models.User> queryable) {
		return queryable
			.Include(u => u.Profile)
			.ThenInclude(p => p.Address)
			.Include(u => u.Ancestor)
			.Include(u => u.AggregatedStrMarkers)
			.Include(u => u.Haplogroup)
			.Include(u => u.OriginalStrDataSources)
			.Include(u => u.UserAdministratorData);
	}  
}