﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;
using PostgreSqlStorage.Exceptions;
using PostgreSqlStorage.UsersGateway.GenData.Models;
using Users.GenData;
using Users.UserAccess.Entities;
using Haplogroup = PostgreSqlStorage.UsersGateway.GenData.Models.Haplogroup;
using OriginalStrDataSource = PostgreSqlStorage.UsersGateway.GenData.Models.OriginalStrDataSource;

namespace PostgreSqlStorage.UsersGateway.UserAccess.Models;

public class User {
	[DatabaseGenerated(DatabaseGeneratedOption.None)]
	public int Id { get; set; }
	public bool AdministrationAgreementSigned { get; set; }
	public string PublicId { get; set; } = null!;
	public int UnitRegionId { get; set; }

	public DateTime AddedAt { get; set; }
	public string Email { get; set; } = null!;
	
	#region NavigationalProperties
	
	// Profile
	public Profile Profile { get; set; } = new Profile();

	// Ancestor
	public Ancestor Ancestor { get; set; } = new Ancestor();

	// Genetic STR Data
	public AggregatedStrMarkers? AggregatedStrMarkers { get; set; }
	
	[ForeignKey(nameof(Haplogroup))]
	public int? HaplogroupId { get; set; }
	public Haplogroup? Haplogroup { get; set; }
	public List<OriginalStrDataSource> OriginalStrDataSources { get; set; } = new List<OriginalStrDataSource>();
	
	// Admin user notes
	public UserAdministratorData UserAdministratorData { get; set; } = new UserAdministratorData();

	#endregion
	
	public void Set(Users.UserAccess.Entities.User user) {
		if (Ancestor == null) {
			throw new NavigationalPropertyNullException($"Ancestor in user is null but must never be null when calling this method.");
		}
		
		Id = user.Id;
		AdministrationAgreementSigned = user.AdministrationAgreementSigned;
		PublicId = user.PublicId;
		UnitRegionId = user.RegionId;
		SetProfile(profile: user.Profile);
		Ancestor.Set(ancestor: user.Ancestor, userId: user.Id);
		SetGeneticData(geneticData: user.GeneticData);
		UserAdministratorData.Set(administratorData: user.AdministratorData);
	}
	
	public void SetProfile(Users.UserAccess.Entities.Profile profile) {
		Email = profile.Email;
		Profile.Set(profile: profile);
	}

	public void SetGeneticData(GeneticData? geneticData) {
		if (geneticData != null) {
			if (AggregatedStrMarkers == null) {
				AggregatedStrMarkers = new AggregatedStrMarkers();
			}
			AggregatedStrMarkers.Set(new StrMarkerParser().StrMarkersToJson(geneticData.StrMarkers.CloneAsDictionary()));
			HaplogroupId = geneticData.Haplogroup.Id;
		} else {
			AggregatedStrMarkers = null;
			HaplogroupId = null;
			Haplogroup = null;
		}
	}

	public global::Users.UserAccess.Entities.User ToUser() {
		if (Ancestor == null) {
			throw new NavigationalPropertyNullException($"Ancestor in user is null but must never be null when calling this method.");
		}
		
		if (UserAdministratorData == null) {
			throw new NavigationalPropertyNullException($"UserAdministratorData in user is null but must never be null when calling this method.");
		}
		
		GeneticData? geneticData = AggregatedStrMarkers != null && Haplogroup != null
			? new GeneticData(
				strMarkers: new StrMarkerParser().ParseGeneticData(AggregatedStrMarkers.StrMarkers),
				haplogroup: new global::Users.GenData.Haplogroup.Haplogroup(Haplogroup.Id, Haplogroup.Name),
				originalStrDataSources: OriginalStrDataSources.Select(d => d.Id).ToList()
			)
			: null;

		return new global::Users.UserAccess.Entities.User(
			id: Id,
			administrationAgreementSigned: AdministrationAgreementSigned,
            publicId: PublicId,
			profile: Profile.Get(email: Email),
			ancestor: Ancestor.Get(),
			regionId: UnitRegionId,
			geneticData: geneticData,
			administratorData: new AdministratorData(UserAdministratorData.AdministratorId, UserAdministratorData.AdministratorNote) 
		);
	}
}