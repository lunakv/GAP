using System.ComponentModel.DataAnnotations.Schema;
using Users.UserAccess.Entities;

namespace PostgreSqlStorage.UsersGateway.UserAccess.Models; 

public class Address {
	[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
	public int Id { get; set; }
	
	[ForeignKey(nameof(Profile))]
	public int ProfileId { get; set; }
	public Profile Profile { get; set; } = null!;
	
	public string? ResidenceTown { get; set; }
	public string? ResidenceMunicipality { get; set; }
	public string? ResidenceCounty { get; set; }
	public string? ResidenceStreet { get; set; }
	public string? ResidenceZipCode { get; set; }

	public string? CorrespondenceTown { get; set; }
	public string? CorrespondenceStreet { get; set; }
	public string? CorrespondenceZipCode { get; set; }

	public void Set(Users.UserAccess.Entities.Profile profile) {
		ResidenceTown = profile.ResidenceAddress.Town;
		ResidenceMunicipality = profile.ResidenceAddress.Municipality;
		ResidenceCounty = profile.ResidenceAddress.County;
		ResidenceStreet = profile.ResidenceAddress.Street;
		ResidenceZipCode = profile.ResidenceAddress.ZipCode;
		CorrespondenceTown = profile.CorrespondenceAddress?.Town;
		CorrespondenceStreet = profile.CorrespondenceAddress?.Street;
		CorrespondenceZipCode = profile.CorrespondenceAddress?.ZipCode;
	}

	public ExpandedAddress GetResidenceAddress() {
		return new ExpandedAddress(
			town: ResidenceTown,
			municipality: ResidenceMunicipality,
			county: ResidenceCounty,
			street: ResidenceStreet,
			zipCode: ResidenceZipCode
		);
	}

	public Users.UserAccess.Entities.Address? GetCorrespondenceAddress() {
		if (CorrespondenceStreet == null) {
			return null;
		}
		
		return new Users.UserAccess.Entities.Address(
			town: CorrespondenceTown!,
			street: CorrespondenceStreet,
			zipCode: CorrespondenceZipCode!
		);
	}
}