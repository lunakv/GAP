using System.ComponentModel.DataAnnotations.Schema;

namespace PostgreSqlStorage.UsersGateway.UserAccess.Models; 

public class Ancestor {
	
	[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
	public int Id { get; set; }
	
	[ForeignKey(nameof(User))]
	public int UserId { get; set; }
	public User User { get; set; } = null!;
	
	public string? GivenName { get; set; }
	public string? FamilyName { get; set; }
	public DateOnly? BirthDate { get; set; }
	public string? PlaceOfBirth { get; set; }
	public DateOnly? DeathDate { get; set; }
	public string? PlaceOfDeath { get; set; }
	public string? Town { get; set; }
	public string? Municipality { get; set; }
	public string? County { get; set; }
	public string? Street { get; set; }
	public string? ZipCode { get; set; }

	public void Set(Users.UserAccess.Entities.Ancestor ancestor, int userId) {
		UserId = userId;
		GivenName = ancestor.GivenName;
		FamilyName = ancestor.FamilyName;
		BirthDate = ancestor.BirthDate;
		PlaceOfBirth = ancestor.PlaceOfBirth;
		DeathDate = ancestor.DeathDate;
		PlaceOfDeath = ancestor.PlaceOfDeath;
		Town = ancestor.Address.Town;
		Municipality = ancestor.Address.Municipality;
		County = ancestor.Address.County;
		Street = ancestor.Address.Street;
		ZipCode = ancestor.Address.ZipCode;
	}

	public Users.UserAccess.Entities.Ancestor Get() {
		return new Users.UserAccess.Entities.Ancestor(
			address: new Users.UserAccess.Entities.ExpandedAddress(
				town: Town,
				municipality: Municipality,
				county: County,
				street: Street,
				zipCode: ZipCode
			),
			givenName: GivenName,
			familyName: FamilyName,
			birthDate: BirthDate,
			placeOfBirth: PlaceOfBirth,
			deathDate: DeathDate,
			placeOfDeath: PlaceOfDeath
		);
	}
	
}