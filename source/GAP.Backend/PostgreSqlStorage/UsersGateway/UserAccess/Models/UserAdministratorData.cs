using System.ComponentModel.DataAnnotations.Schema;
using Users.UserAccess.Entities;

namespace PostgreSqlStorage.UsersGateway.UserAccess.Models; 

public class UserAdministratorData {
	[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
	public int Id { get; set; }
	
	public string? AdministratorId { get; set; }
	public string? AdministratorNote { get; set; }
	
	[ForeignKey(nameof(User))]
	public int UserId { get; set; }
	public User User { get; set; } = null!;
	
	public void Set(AdministratorData administratorData) {
		AdministratorId = administratorData.Id;
		AdministratorNote = administratorData.Note;
	}
}