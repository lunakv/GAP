using System.ComponentModel.DataAnnotations.Schema;

namespace PostgreSqlStorage.UsersGateway.UserAccess.Models; 

public class Profile {
	[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
	public int Id { get; set; }
	
	public string GivenName { get; set; } = null!;
	public string FamilyName { get; set; } = null!;
	public string? PhoneNumber { get; set; }

	public Address Address { get; set; } = new Address();

	public DateOnly? BirthDate { get; set; }
	
	[ForeignKey(nameof(User))]
	public int UserId { get; set; }
	public User User { get; set; } = null!;
	
	public void Set(Users.UserAccess.Entities.Profile profile) {
		GivenName = profile.GivenName;
		FamilyName = profile.FamilyName;
		PhoneNumber = profile.PhoneNumber;
		Address.Set(profile: profile);
		BirthDate = profile.BirthDate;
	}

	public Users.UserAccess.Entities.Profile Get(string email) {
		return new Users.UserAccess.Entities.Profile(
			givenName: GivenName,
			familyName: FamilyName,
			email: email,
			phoneNumber: PhoneNumber,
			residenceAddress: Address.GetResidenceAddress(),
			correspondenceAddress: Address.GetCorrespondenceAddress(),
			birthDate: BirthDate
		);
	}
}