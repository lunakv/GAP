using System.Text.Json;

namespace PostgreSqlStorage.UsersGateway; 

public class StrMarkerParser {
	public Dictionary<string, int> ParseGeneticData(JsonDocument gd) {
		Dictionary<string, int> markers = new();

		foreach (var e in gd.RootElement.EnumerateArray()) {
			string? marker = e.GetProperty("Marker").GetString();
			if (marker != null) {
				markers[marker] = e.GetProperty("Value").GetInt32();
			}
		}

		return markers;
	}
	
	public JsonDocument StrMarkersToJson(IDictionary<string, int> markers) {
		return JsonDocument.Parse(JsonSerializer.Serialize(markers.Select(marker => new { Marker = marker.Key, Value = marker.Value })));
	}
}