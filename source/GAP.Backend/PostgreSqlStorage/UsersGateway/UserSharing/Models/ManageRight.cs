namespace PostgreSqlStorage.UsersGateway.UserSharing.Models; 

public class ManageRight {
	public int ManagerId { get; set; }
	public int ManagedUserId { get; set; }

	public override string ToString() {
		return $"ManageRight: ManagerId = {ManagerId}, ManagedUserId = {ManagedUserId}";
	}
}