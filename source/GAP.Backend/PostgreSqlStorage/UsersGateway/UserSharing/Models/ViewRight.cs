namespace PostgreSqlStorage.UsersGateway.UserSharing.Models; 

public class ViewRight {
	public int ViewerId { get; set; }
	public int ViewableUserId { get; set; }

	public override string ToString() {
		return $"View Right = ViewerId = {ViewerId}, ViewableUserId = {ViewableUserId}";
	}
}