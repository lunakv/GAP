using Microsoft.EntityFrameworkCore;
using PostgreSqlStorage.Data;
using Users.UserSharing.UserManagementSharing;
using Users.UserSharing.UserManagementSharing.Storage;

namespace PostgreSqlStorage.UsersGateway.UserSharing.Accessors;

public class UserManagementSettingsFetcher : IUserManagementSettingsFetcher {
	private readonly GapContext _context;

	public UserManagementSettingsFetcher(GapContext context) {
		_context = context;
	}

	public async Task<UserManagementSettings> GetUserManagementSettingsAsync(int userId) {
		var manageRights = await _context.ManageRights.Where(right => right.ManagerId == userId || right.ManagedUserId == userId).ToListAsync()
			.ConfigureAwait(false);

		return new UserManagementSettings(
			userId: userId,
			manageRights: manageRights
				.Select(right => new ManageRight(managerId: right.ManagerId, managedUserId: right.ManagedUserId))
				.ToList()
		);
	}
}