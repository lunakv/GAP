using Microsoft.EntityFrameworkCore;
using PostgreSqlStorage.Data;
using Users.UserSharing.UserManagementSharing;
using Users.UserSharing.UserManagementSharing.Exceptions;
using Users.UserSharing.UserManagementSharing.Storage;

namespace PostgreSqlStorage.UsersGateway.UserSharing.Accessors; 

public class UserManageRightSaver : IUserManageRightSaver {

	private readonly GapContext _context;

	public UserManageRightSaver(GapContext context) {
		_context = context;
	}

	public async Task AddManageRightAsync(ManageRight manageRight) {
		Models.ManageRight? rightInDb = await GetPresentManageRight(manageRight).ConfigureAwait(false);
		if (rightInDb != null) {
			throw new ManageRightAlreadyPresentException($"{manageRight} is already present in db when adding it.");
		}

		await _context.ManageRights.AddAsync(new Models.ManageRight()
			{ ManagerId = manageRight.ManagerId, ManagedUserId = manageRight.ManagedUserId })
			.ConfigureAwait(false);

		await _context.SaveChangesAsync().ConfigureAwait(false);
	}

	public async Task RemoveManageRightAsync(int managerId, int managedUserId) {
		var manageRight = new ManageRight(managerId: managerId, managedUserId: managedUserId);
		Models.ManageRight? rightInDb = await GetPresentManageRight(manageRight).ConfigureAwait(false);
		if (rightInDb != null) {
			_context.ManageRights.Remove(rightInDb);

			await _context.SaveChangesAsync().ConfigureAwait(false);
		} else {
			throw new ManageRightNotPresentException($"{manageRight} is not present in db when deleting it.");
		}
	}

	private Task<Models.ManageRight?> GetPresentManageRight(ManageRight right) {
		return _context.ManageRights.FirstOrDefaultAsync<Models.ManageRight?>(
			r => r != null && r.ManagerId == right.ManagerId && r.ManagedUserId == right.ManagedUserId);
	}
}