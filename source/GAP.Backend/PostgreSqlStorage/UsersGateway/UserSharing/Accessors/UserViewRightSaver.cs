using Microsoft.EntityFrameworkCore;
using PostgreSqlStorage.Data;
using Users.UserSharing.UserViewSharing;
using Users.UserSharing.UserViewSharing.Exceptions;
using Users.UserSharing.UserViewSharing.Storage;

namespace PostgreSqlStorage.UsersGateway.UserSharing.Accessors;

public class UserViewRightSaver : IUserViewRightSaver {
	private readonly GapContext _context;

	public UserViewRightSaver(GapContext context) {
		_context = context;
	}

	public async Task AddViewRightAsync(ViewRight viewRight) {
		Models.ViewRight? rightInDb = await GetPresentViewRightAsync(viewRight).ConfigureAwait(false);
		if (rightInDb != null) {
			throw new ViewRightAlreadyPresentException($"{viewRight} is already present in db when adding it.");
		}

		await _context.AddAsync(new Models.ViewRight() {
			ViewerId = viewRight.ViewerId,
			ViewableUserId = viewRight.ViewableUserId
		}).ConfigureAwait(false);

		await _context.SaveChangesAsync().ConfigureAwait(false);
	}

	public async Task RemoveViewRightAsync(int viewerId, int viewableUserId) {
		var viewRight = new ViewRight(viewerId: viewerId, viewableUserId: viewableUserId);
		Models.ViewRight? rightInDb = await GetPresentViewRightAsync(viewRight).ConfigureAwait(false);
		if (rightInDb != null) {
			_context.ViewRights.Remove(rightInDb);

			await _context.SaveChangesAsync().ConfigureAwait(false);
		} else {
			throw new ViewRightNotPresentException(
				$"{viewRight} is not present in db to be deleted."
			);
		}
	}

	private Task<Models.ViewRight?> GetPresentViewRightAsync(ViewRight right) {
		return _context.ViewRights.FirstOrDefaultAsync<Models.ViewRight?>(
			r => r != null && r.ViewerId == right.ViewerId && r.ViewableUserId == right.ViewableUserId);
	}
}