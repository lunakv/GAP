using Microsoft.EntityFrameworkCore;
using PostgreSqlStorage.Data;
using Users.UserSharing.UserViewSharing;
using Users.UserSharing.UserViewSharing.Storage;

namespace PostgreSqlStorage.UsersGateway.UserSharing.Accessors; 

public class UserViewSettingsFetcher : IUserViewSettingsFetcher {

	private readonly GapContext _context;

	public UserViewSettingsFetcher(GapContext context) {
		_context = context;
	}
	
	public async Task<UserViewSettings> GetUserViewSettingsAsync(int userId) {
		var accessRights = await _context.ViewRights.Where(right => right.ViewerId == userId || right.ViewableUserId == userId)
			.ToListAsync().ConfigureAwait(false);

		return new UserViewSettings(
			userId: userId,
			viewRights: accessRights
				.Select(r => new ViewRight(viewerId: r.ViewerId, viewableUserId: r.ViewableUserId))
				.ToList()
		);
	}
}