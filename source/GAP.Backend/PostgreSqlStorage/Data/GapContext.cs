﻿using Microsoft.EntityFrameworkCore;
using PostgreSqlStorage.FamilyTreeStorageGateway;
using PostgreSqlStorage.LaboratoryGateway.KitManagement.Models;
using PostgreSqlStorage.LaboratoryGateway.LabResults.Models;
using PostgreSqlStorage.MapGateway.Models;
using PostgreSqlStorage.RegistrationGateway.Models;
using PostgreSqlStorage.UsersGateway.GenData.Models;
using PostgreSqlStorage.UsersGateway.UserAccess.Models;
using PostgreSqlStorage.UsersGateway.UserSharing.Models;

namespace PostgreSqlStorage.Data;

public class GapContext : DbContext {

    // User & Genetic Data Storage
    public DbSet<User> Users { get; set; } = null!;
    public DbSet<Profile> Profiles { get; set; } = null!;
    public DbSet<Address> Adresses { get; set; } = null!;
    public DbSet<Ancestor> Ancestors { get; set; } = null!;
    public DbSet<AggregatedStrMarkers> AggregatedStrMarkers { get; set; } = null!;
    public DbSet<UserAdministratorData> UserAdministratorData { get; set; } = null!;
    public DbSet<Haplogroup> Haplogroups { get; set; } = null!;
    public DbSet<OriginalStrDataSource> OriginalStrDataSources { get; set; } = null!;
    public DbSet<StrConflict> StrConflicts { get; set; } = null!;
    public DbSet<ManageRight> ManageRights { get; set; } = null!;
    public DbSet<ViewRight> ViewRights { get; set; } = null!;
    
    // Laboratory Storage
    public DbSet<Kit> Kits { get; set; } = null!;
    public DbSet<SequencerInput> SequencerInputs { get; set; } = null!;

    // Haplogroup Map Storage
    public DbSet<Region> Regions { get; set; } = null!;
    public DbSet<UnitRegionMapping> UnitRegionMappings { get; set; } = null!;
    
    // Registration Temporal Storage
    public DbSet<RegistrationData> RegistrationData { get; set; } = null!;
    
    // Family Tree Storage
    public DbSet<FamilyTreeModel> FamilyTrees { get; set; } = null!;
    
    public GapContext(DbContextOptions<GapContext> options) : base(options) { }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder) {
        modelBuilder.Entity<ManageRight>()
            .HasKey(r => new { r.ManagerId, ManageeId = r.ManagedUserId });
        
        modelBuilder.Entity<ViewRight>()
            .HasKey(r => new { r.ViewerId, r.ViewableUserId});

        modelBuilder.Entity<KitEvent>().HasNoKey();

        modelBuilder.Entity<UnitRegionMapping>()
            .HasKey(m => new { m.RegionId, m.UnitRegionId });
    }
}
