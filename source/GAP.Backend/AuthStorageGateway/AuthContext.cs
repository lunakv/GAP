using Auth.Identity;
using Auth.Identity.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AuthStorageGateway;

public class AuthContext : IdentityDbContext<User, IdentityRole<int>, int> {
	public AuthContext(DbContextOptions<AuthContext> options) : base(options) { }
}