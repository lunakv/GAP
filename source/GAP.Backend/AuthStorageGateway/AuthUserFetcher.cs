using System.Security.Claims;
using Auth.Identity;
using Auth.Identity.Entities;
using Auth.Identity.Storage;
using Microsoft.EntityFrameworkCore;

namespace AuthStorageGateway;

/// <summary>
/// Postgres implementation for raw access to identity postgres database.
/// </summary>
public class AuthUserFetcher : IAuthUserFetcher {
	private readonly AuthContext _context;

	public AuthUserFetcher(AuthContext context) {
		_context = context;
	}

	public async Task<IDictionary<int, User>> GetUsersAsync(IReadOnlyCollection<int> userIds) {
		return await _context.Users
			.Where(user => userIds.Contains(user.Id))
			.ToDictionaryAsync(user => user.Id, user => user)
			.ConfigureAwait(false);
	}

	public async Task<IDictionary<string, ICollection<User>>> GetUserWithRolesAsync(IReadOnlyCollection<string> roles) {
		var claimsWithUsers = await (
			from claim in _context.UserClaims.Where(claim => claim.ClaimType == ClaimTypes.Role)
			join user in _context.Users on claim.UserId equals user.Id
			select new { user, claim }
		).ToListAsync().ConfigureAwait(false);

		IDictionary<string, ICollection<User>> roleToUsersTable = new Dictionary<string, ICollection<User>>();
		foreach (string role in roles) {
			roleToUsersTable.Add(key: role, value: new List<User>());
		}

		foreach (var pair in claimsWithUsers.Where(pair => roles.Contains(pair.claim.ClaimValue))) {
			roleToUsersTable[pair.claim.ClaimValue].Add(pair.user);
		}

		return roleToUsersTable;
	}
}