﻿using DataLoader.MockDataLoaders;
using Users.GenData;
using Users.GenData.Haplogroup;
using Users.UserAccess.Entities;

namespace TestUtils;

public class UserCreator {
	private static int _seed = 0;

	public static User CreateUser(int id, bool administrationAgreementSigned = true, string? publicId = null, string? givenName = null, string? familyName = null, string? email = null, string? phoneNumber = null, string? town = null,
		string? municipality = null, string? county = null, string? street = null, string? zipCode = null, DateOnly? birthDate = null,
		string? ancestorGivenName = null, string? ancestorFamilyName = null, DateOnly? ancestorBirthDate = null, string? ancestorPlaceOfBirth = null,
		DateOnly? ancestorDeathDate = null, string? ancestorPlaceOfDeath = null, string? ancestorApproximateBirthYear = null, string? ancestorApproximatePlaceOfBirth = null,
		string? ancestorApproximateDeathYear = null, string? ancestorApproximatePlaceOfDeath = null, int? regionId = null, GeneticData? geneticData = null,
		string? adminDataId = null, string? adminNote = null, bool correspondenceAddress = false, string? adminTown = null) {
		IList<Haplogroup> haplogroups = GetHaplogroups();

		User user = new MockUserSource(
			haplogroups: haplogroups,
			regionKeys: new int[] { regionId ?? 1 },
			usersToGenerate: 1,
			administrationAgreementSigned: administrationAgreementSigned,
			seed: Interlocked.Increment(ref _seed)
		).GetData()[0];
		return new User(
			id: id,
			administrationAgreementSigned: administrationAgreementSigned,
			publicId: publicId ?? user.PublicId,
			new Profile(
				givenName: givenName ?? user.Profile.GivenName,
				familyName: familyName ?? user.Profile.FamilyName,
				email: email ?? user.Profile.Email,
				phoneNumber: phoneNumber ?? user.Profile.PhoneNumber,
				residenceAddress: new ExpandedAddress(
					town: town ?? user.Profile.ResidenceAddress.Town,
					municipality: municipality ?? user.Profile.ResidenceAddress.Municipality,
					county: county ?? user.Profile.ResidenceAddress.County,
					street: street ?? user.Profile.ResidenceAddress.Street,
					zipCode: zipCode ?? user.Profile.ResidenceAddress.ZipCode
				),
				correspondenceAddress: correspondenceAddress ? user.Profile.CorrespondenceAddress : null,
				birthDate: new DateOnly(year: 1984, month: 3, day: 22)
			),
			new Ancestor(
				address: new ExpandedAddress(town: adminTown),
				givenName: ancestorGivenName ?? user.Ancestor.GivenName,
				familyName: ancestorFamilyName ?? user.Ancestor.FamilyName,
				birthDate: ancestorBirthDate ?? user.Ancestor.BirthDate,
				placeOfBirth: ancestorPlaceOfBirth ?? user.Ancestor.PlaceOfBirth,
				deathDate: ancestorDeathDate ?? user.Ancestor.DeathDate,
				placeOfDeath: ancestorPlaceOfDeath ?? user.Ancestor.PlaceOfDeath
			),
			regionId: regionId ?? 1,
			geneticData: geneticData ?? user.GeneticData,
			administratorData: new AdministratorData(
				id: adminDataId ?? $"ADMINISTRATOR INTERNAL ID: {id}",
				note: adminNote ?? user.AdministratorData?.Note
			)
		);
	}

	public static CompleteUser CreateUser(int id, IDictionary<string, int> strMarkers) {
		var user = CreateUser(id, geneticData: new GeneticData(strMarkers, new Haplogroup(-1, "X"), new List<int>()));
		return new CompleteUser(
			id: user.Id,
			administrationAgreementSigned: user.AdministrationAgreementSigned,
			publicId: user.PublicId,
			profile: user.Profile,
			ancestor: user.Ancestor,
			regionId: user.RegionId,
			geneticData: user.GeneticData!,
			administratorData: user.AdministratorData
		);
	}

	public static IList<Haplogroup> GetHaplogroups() {
		return new MockHaplogroupSource(50)
			.GetData()
			.Select((hs, i) => new Haplogroup(i + 1, hs.Name))
			.ToList();
	}
}