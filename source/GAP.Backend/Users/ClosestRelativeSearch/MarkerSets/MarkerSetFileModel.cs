namespace Users.ClosestRelativeSearch.MarkerSets; 

public struct MarkerSetFileModel {
	public int MaxDistance { get; set; }
	public List<string> Markers { get; set; }

	public MarkerSetFileModel(int maxDistance, List<string> markers) {
		MaxDistance = maxDistance;
		Markers = markers;
	}

	public MarkerSet ToMarkerSet(string markerSetName) {
		return new MarkerSet(
			name: markerSetName,
			markerCount: Markers.Count,
			maxDistance: MaxDistance,
			markers: Markers.AsReadOnly()
		);
	}
}