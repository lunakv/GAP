using System.Text.Json;
using Users.ClosestRelativeSearch.Exceptions;

namespace Users.ClosestRelativeSearch.MarkerSets; 

public class MarkerSetFetcher : IMarkerSetFetcher {

	private readonly string _markerSetsDirectory;

	public MarkerSetFetcher(string markerSetsDirectory) {
		_markerSetsDirectory = markerSetsDirectory;
	}
	
	public async Task<IList<MarkerSet>> FetchMarkerSetsAsync() {
		var markerSetFileNames = Directory.GetFiles(_markerSetsDirectory);

		var markerSetFileModels = await Task.WhenAll(markerSetFileNames.Select(ReadMarkerSetAsync))
			.ConfigureAwait(false);

		return markerSetFileModels.Select(parsedSet => parsedSet.Model.ToMarkerSet(parsedSet.Name)).ToList();
	}

	public async Task<MarkerSet> FindMarkerSetAsync(string name) {
		var markerSetFileNames = Directory.GetFiles(_markerSetsDirectory);
		var markerSetFilename = markerSetFileNames.Where(fn => Path.GetFileNameWithoutExtension(fn) == name).ToList();
		if (markerSetFilename.Count == 1) {
			var (markerSetFileModel, _) = await ReadMarkerSetAsync(markerSetFilename[0])
				.ConfigureAwait(false);

			return markerSetFileModel.ToMarkerSet(name);
		}

		throw new MarkerSetNotFoundException($"Marker set {name} does not exist.");
	}

	public void Initialize() {
		try {
			Directory.CreateDirectory(_markerSetsDirectory);
		} catch (Exception ex) {
			throw new Exception(
				$"Directory {_markerSetsDirectory} did not exist and failed to be created.", ex);
		}
	}
	
	private async Task<(MarkerSetFileModel Model, string Name)> ReadMarkerSetAsync(string markerSetFileName) {
		var options = new FileStreamOptions() {
			Options = FileOptions.Asynchronous,
			Mode = FileMode.Open,
			Share = FileShare.Read,
			Access = FileAccess.Read
		};
		await using FileStream fs = File.Open(markerSetFileName, options: options);
		
		var setModel = await JsonSerializer.DeserializeAsync<MarkerSetFileModel>(fs).AsTask()
			.ConfigureAwait(false);
		return (setModel, Path.GetFileNameWithoutExtension(markerSetFileName));
	}

}