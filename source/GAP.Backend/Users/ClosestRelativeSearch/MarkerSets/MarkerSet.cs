using Users.GenData;

namespace Users.ClosestRelativeSearch.MarkerSets; 

public class MarkerSet {
	public string Name { get; }
	public int MarkerCount { get; }
	public int MaxDistance { get; }
	public IReadOnlyCollection<string> Markers { get; }

	public MarkerSet(string name, int markerCount, int maxDistance, IReadOnlyCollection<string> markers) {
		Name = name;
		MarkerCount = markerCount;
		MaxDistance = maxDistance;
		Markers = markers;
	}

	public IList<int?> ExtractUserValues(StrMarkerCollection<string, int> strMarkers) {
		// return Markers.Select<string, int?>(marker => strMarkers.HasMarker(marker) ? strMarkers[marker] : null).ToList();
		return Markers.Select<string, int?>(marker => strMarkers.FindMarker(m => MarkerComparer.AreSame(a: marker, b: m))?.Value).ToList();
	}


}