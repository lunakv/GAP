namespace Users.ClosestRelativeSearch.MarkerSets; 

public interface IMarkerSetFetcher {
	Task<IList<MarkerSet>> FetchMarkerSetsAsync();
	Task<MarkerSet> FindMarkerSetAsync(string name);
}