namespace Users.ClosestRelativeSearch; 

public struct GeneticDistance {
	public int Distance { get; }
	public int ComparedMarkers { get; }
	public IList<int?> MarkerValues { get; }

	public GeneticDistance(int distance, int comparedMarkers, IList<int?> markerValues) {
		Distance = distance;
		ComparedMarkers = comparedMarkers;
		MarkerValues = markerValues;
	}
}