using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Users.ClosestRelativeSearch.Filters;
using Users.ClosestRelativeSearch.MarkerSets;
using Users.UserAccess;
using Users.UserAccess.Entities;
using Users.UserAccess.UserFilter;

namespace Users.ClosestRelativeSearch;

public class ClosestRelativeSearchController {
	private readonly CompleteUserFetcher _userFetcher;
	private readonly ClosestRelativeSearchConfig _config;
	private readonly IMarkerSetFetcher _markerSetFetcher;

	public ClosestRelativeSearchController(CompleteUserFetcher userFetcher, IOptions<UsersConfig> userConfig) {
		_userFetcher = userFetcher;
		_config = userConfig.Value.ClosestRelativeSearchConfig;
		_markerSetFetcher = new MarkerSetFetcher(markerSetsDirectory: _config.MarkerSetsDirectory);
	}

	public async Task<ClosestRelativeSearchResult<CompleteUser>> GetClosestStrRelativesAsync(int userId, string markerSetName, bool ownFamilyNameFilter, int[]? onlyGivenHaplos) {
		CompleteUser user = await _userFetcher.FindCompleteUserByIdAsync(userId);

		MarkerSet markerSet = await _markerSetFetcher.FindMarkerSetAsync(name: markerSetName)
			.ConfigureAwait(false);

		IList<CompleteUser> users = await _userFetcher.GetUsersAsync(
			CreateUserFilter(user: user, ownFamilyNameFilter: ownFamilyNameFilter, onlyGivenHaplos: onlyGivenHaplos)
		).ConfigureAwait(false);

		IReadOnlyList<UserRelativeDistance<CompleteUser>> closestRelatives = new ClosestRelativeFinder<CompleteUser>().FindClosestRelatives(
			user: user,
			users: users,
			distanceCalculator: new StrMarkerDistanceCalculator(markerSet),
			distanceFilter: CreateDistanceFilter(user: user, maxSetDistance: markerSet.MaxDistance, onlyGivenHaplos, ownFamilyNameFilter: ownFamilyNameFilter)
		);

		return new ClosestRelativeSearchResult<CompleteUser>(
			userMarkerSetValues: markerSet.ExtractUserValues(user.GeneticData.StrMarkers),
			markerSet: markerSet,
			userRelativeDistances: closestRelatives
		);
	}

	public Task<IList<MarkerSet>> ListAvailableMarkerSetsAsync() {
		return _markerSetFetcher.FetchMarkerSetsAsync();
	}

	public Task<MarkerSet> GetMarkerSetAsync(string name) {
		return _markerSetFetcher.FindMarkerSetAsync(name);
	}

	private IClosestRelativeDistanceFilter<CompleteUser> CreateDistanceFilter(CompleteUser user, int maxSetDistance, int[]? givenHplogroups, bool ownFamilyNameFilter) {
		IClosestRelativeDistanceFilter<CompleteUser> filter = new NotCertainUserFilter<CompleteUser>(user.Id);

		if ((givenHplogroups == null || givenHplogroups.Length == 0) && !ownFamilyNameFilter) {
			filter = new FilterDecorator<CompleteUser>(filter, new MaxDistanceFilter<CompleteUser>(maxSetDistance));
		}

		return filter;
	}

	private UserFilter CreateUserFilter(CompleteUser user, bool ownFamilyNameFilter, int[]? onlyGivenHaplos) {
		UserFilter filter = new EmptyUserFilter();

		if(onlyGivenHaplos != null) {
			filter = filter.Chain(new HaplogroupUserFilter(onlyGivenHaplos));
		}

		if (ownFamilyNameFilter) {
			filter = filter.Chain(new FamilyNameUserFilter(user.Profile.FamilyName));
		}

		return filter;
	}
}