namespace Users.ClosestRelativeSearch; 

public class ClosestRelativeSearchConfig {
	public string MarkerSetsDirectory { get; set; }

	public ClosestRelativeSearchConfig() {
		MarkerSetsDirectory = string.Empty;
	}
}