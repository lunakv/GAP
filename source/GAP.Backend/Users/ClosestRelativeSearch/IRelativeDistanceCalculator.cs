using Users.GenData;

namespace Users.ClosestRelativeSearch; 

public interface IRelativeDistanceCalculator {
	GeneticDistance ComputeDistance(StrMarkerCollection<string, int> aMarkers, StrMarkerCollection<string, int> bMarkers);
}