using Users.UserAccess.Entities.Interfaces;

namespace Users.ClosestRelativeSearch.Filters; 

public interface IClosestRelativeDistanceFilter<TUser> where TUser : IHasUserId, IHasGeneticData{
	IEnumerable<UserRelativeDistance<TUser>> Filter(IEnumerable<UserRelativeDistance<TUser>> distances);
}