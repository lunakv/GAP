using Users.UserAccess.Entities.Interfaces;

namespace Users.ClosestRelativeSearch.Filters; 

public class NotCertainUserFilter<TUser> : IClosestRelativeDistanceFilter<TUser> where TUser : IHasUserId, IHasGeneticData {

	private readonly int _userId;
	
	public NotCertainUserFilter(int userId) {
		_userId = userId;
	}
	
	public IEnumerable<UserRelativeDistance<TUser>> Filter(IEnumerable<UserRelativeDistance<TUser>> distances) {
		return distances.Where(urd => urd.User.Id != _userId);
	}
}