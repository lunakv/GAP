using Users.UserAccess.Entities.Interfaces;

namespace Users.ClosestRelativeSearch.Filters; 

public class FilterDecorator<TUser> : IClosestRelativeDistanceFilter<TUser> where TUser : IHasUserId, IHasGeneticData {

	private readonly IClosestRelativeDistanceFilter<TUser> _wrappee;
	private readonly IClosestRelativeDistanceFilter<TUser> _filter;
	

	public FilterDecorator(IClosestRelativeDistanceFilter<TUser> wrappee, IClosestRelativeDistanceFilter<TUser> filter) {
		_wrappee = wrappee;
		_filter = filter;
	}
	
	public IEnumerable<UserRelativeDistance<TUser>> Filter(IEnumerable<UserRelativeDistance<TUser>> distances) {
		return _filter.Filter(_wrappee.Filter(distances));
	}
}