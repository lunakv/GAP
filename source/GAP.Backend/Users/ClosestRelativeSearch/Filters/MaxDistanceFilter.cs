using Users.UserAccess.Entities.Interfaces;

namespace Users.ClosestRelativeSearch.Filters; 

public class MaxDistanceFilter<TUser> : IClosestRelativeDistanceFilter<TUser> where TUser : IHasUserId, IHasGeneticData {

	private readonly int _maxDistance;
	
	public MaxDistanceFilter(int maxDistance) {
		_maxDistance = maxDistance;
	}
	
	public IEnumerable<UserRelativeDistance<TUser>> Filter(IEnumerable<UserRelativeDistance<TUser>> distances) {
		return distances.Where(urd => urd.Distance.Distance < _maxDistance);
	}
}