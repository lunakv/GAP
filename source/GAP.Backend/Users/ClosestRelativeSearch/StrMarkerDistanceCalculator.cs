using Users.ClosestRelativeSearch.MarkerSets;
using Users.GenData;

namespace Users.ClosestRelativeSearch; 

public class StrMarkerDistanceCalculator : IRelativeDistanceCalculator {

	private readonly MarkerSet _markerSet;
	
	public StrMarkerDistanceCalculator(MarkerSet markerSet) {
		_markerSet = markerSet;
	}
	public GeneticDistance ComputeDistance(StrMarkerCollection<string, int> aMarkers, StrMarkerCollection<string, int> bMarkers) {
		int distance = 0;
		int comparedMarkers = 0;
		IList<int?> aMarkerValues = _markerSet.ExtractUserValues(aMarkers);
		IList<int?> bMarkerValues = _markerSet.ExtractUserValues(bMarkers);

		for (int i = 0; i < _markerSet.Markers.Count; ++i) {
			if (aMarkerValues[i] != null && bMarkerValues[i] != null) {
				++comparedMarkers;
				if (aMarkerValues[i] != bMarkerValues[i]) {
					++distance;
				}
			}
		}
		
		return new GeneticDistance(
			distance: distance,
			comparedMarkers: comparedMarkers,
			markerValues: bMarkerValues
		);
	}
}