using Users.ClosestRelativeSearch.MarkerSets;
using Users.UserAccess.Entities.Interfaces;

namespace Users.ClosestRelativeSearch; 

public class ClosestRelativeSearchResult<TUser> where TUser : IHasUserId, IHasGeneticData {
	public IList<int?> UserMarkerSetValues { get; }
	public MarkerSet MarkerSet { get; }
	public IReadOnlyCollection<UserRelativeDistance<TUser>> UserRelativeDistances { get; }

	public ClosestRelativeSearchResult(IList<int?> userMarkerSetValues, MarkerSet markerSet, IReadOnlyCollection<UserRelativeDistance<TUser>> userRelativeDistances) {
		UserMarkerSetValues = userMarkerSetValues;
		MarkerSet = markerSet;
		UserRelativeDistances = userRelativeDistances;
	}
}