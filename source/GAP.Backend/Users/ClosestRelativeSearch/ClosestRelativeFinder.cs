using Users.ClosestRelativeSearch.Filters;
using Users.UserAccess;
using Users.UserAccess.Entities.Interfaces;
using Users.UserAccess.UserFilter;

namespace Users.ClosestRelativeSearch;

public class ClosestRelativeFinder<TUser> where TUser : IHasUserId, IHasGeneticData {
	
	public ClosestRelativeFinder() { }

	class RelativesComparer : IComparer<UserRelativeDistance<TUser>> {
		public int Compare(UserRelativeDistance<TUser> x, UserRelativeDistance<TUser> y) {
			if (x.Distance.Distance < y.Distance.Distance) {
				return -1;
			}
			
			if (x.Distance.Distance > y.Distance.Distance) {
				return 1;
			}

			if (x.Distance.ComparedMarkers > y.Distance.ComparedMarkers) {
				return -1;
			}
			
			if (x.Distance.ComparedMarkers < y.Distance.ComparedMarkers) {
				return 1;
			}

			if (x.User.Id < y.User.Id) {
				return -1;
			}
			
			if (x.User.Id > y.User.Id) {
				return 1;
			}

			return 0;
		}
	}

	public IReadOnlyList<UserRelativeDistance<TUser>> FindClosestRelatives(TUser user, IList<TUser> users, IRelativeDistanceCalculator distanceCalculator,
		IClosestRelativeDistanceFilter<TUser> distanceFilter) {
		
		return distanceFilter.Filter(users
			.Select(u => new UserRelativeDistance<TUser>(
				user: u,
				distance: distanceCalculator.ComputeDistance(user.GeneticData.StrMarkers, u.GeneticData.StrMarkers))
			))
			.Where(urd => urd.User.Id != user.Id)
			.OrderBy(p => p, new RelativesComparer())
			.ToList();
	}
	
}