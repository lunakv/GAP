using Users.UserAccess.Entities.Interfaces;

namespace Users.ClosestRelativeSearch; 

public struct UserRelativeDistance<TUser> where TUser : IHasUserId, IHasGeneticData {
	public TUser User { get; }
	public GeneticDistance Distance { get; }

	public UserRelativeDistance(TUser user, GeneticDistance distance) {
		User = user;
		Distance = distance;
	}
}