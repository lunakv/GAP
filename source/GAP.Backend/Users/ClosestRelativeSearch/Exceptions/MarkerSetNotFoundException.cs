using Users.Exceptions;

namespace Users.ClosestRelativeSearch.Exceptions; 

public class MarkerSetNotFoundException : UserException {
	public MarkerSetNotFoundException(string message) : base(message) { }
	public override string GetRepresentation => nameof(MarkerSetNotFoundException);
}