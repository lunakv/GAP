using System.Text;

namespace Users.Exceptions; 

public class ExceptionUtils {
	public static string CreateIdDiff(IEnumerable<int> requestedIds, IEnumerable<int> foundIds, string separator = ",") {
		return new StringBuilder()
			.AppendJoin(separator: separator, values: requestedIds.Except(foundIds))
			.ToString();
	}
	
	public static string CollectionToString<T>(IEnumerable<T> collection, string separator = ",") {
		return new StringBuilder()
			.AppendJoin(separator: separator, values: collection)
			.ToString();
	}
}