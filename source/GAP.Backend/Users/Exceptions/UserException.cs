namespace Users.Exceptions;

/// <summary>
/// Exception representing a client side fault - incorrect input parameters, etc...
/// </summary>
public abstract class UserException : Exception {
	public UserException(string message) : base(message) { }
	public UserException(string message, Exception? innerException) : base(message: message, innerException: innerException) { }

	/// <summary>
	/// String represenation for the user fault - usually the name of exception. 
	/// </summary>
	public abstract string GetRepresentation { get; }
}