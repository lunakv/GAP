namespace Users.UserSharing.UserViewSharing;

public class UserViewSettings {
	public IReadOnlyCollection<int> Viewers { get; }
	public IReadOnlyCollection<int> ViewableUsers { get; }

	public UserViewSettings(IReadOnlyCollection<int> viewers, IReadOnlyCollection<int> viewableUsers) {
		Viewers = viewers;
		ViewableUsers = viewableUsers;
	}

	public UserViewSettings(int userId, IReadOnlyCollection<ViewRight> viewRights) {
		Viewers = viewRights
			.Where(r => r.ViewableUserId == userId)
			.Select(right => right.ViewerId).ToList();
		ViewableUsers = viewRights
			.Where(r => r.ViewerId == userId)
			.Select(right => right.ViewableUserId).ToList();
	}

	public bool IsUserViewable(int userId) {
		return ViewableUsers.Any(viewableUser => viewableUser == userId);
	}
}