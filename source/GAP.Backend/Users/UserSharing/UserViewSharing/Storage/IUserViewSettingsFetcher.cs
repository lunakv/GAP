namespace Users.UserSharing.UserViewSharing.Storage; 

public interface IUserViewSettingsFetcher {
	Task<UserViewSettings> GetUserViewSettingsAsync(int userId);
}