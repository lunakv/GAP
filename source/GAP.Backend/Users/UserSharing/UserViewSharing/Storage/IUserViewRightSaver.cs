namespace Users.UserSharing.UserViewSharing.Storage; 

public interface IUserViewRightSaver {
	Task AddViewRightAsync(ViewRight viewRight);
	Task RemoveViewRightAsync(int viewerId, int viewableUserId);
}