using Users.Exceptions;

namespace Users.UserSharing.UserViewSharing.Exceptions; 

public class ViewRightNotPresentException : UserException {
	public ViewRightNotPresentException(string message) : base(message) { }
	public override string GetRepresentation => nameof(ViewRightAlreadyPresentException);
}