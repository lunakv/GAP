using Users.Exceptions;

namespace Users.UserSharing.UserViewSharing.Exceptions; 

public class ViewRightAlreadyPresentException : UserException {
	public ViewRightAlreadyPresentException(string message) : base(message) { }
	public override string GetRepresentation => nameof(ViewRightAlreadyPresentException);
}