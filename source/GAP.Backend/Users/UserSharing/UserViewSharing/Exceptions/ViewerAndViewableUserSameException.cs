using Users.Exceptions;

namespace Users.UserSharing.UserViewSharing.Exceptions; 

public class ViewerAndViewableUserSameException : UserException {
	public ViewerAndViewableUserSameException(string message) : base(message) { }
	public override string GetRepresentation => nameof(ViewerAndViewableUserSameException);
}