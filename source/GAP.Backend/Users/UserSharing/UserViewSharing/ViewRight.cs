namespace Users.UserSharing.UserViewSharing; 

public class ViewRight {
	public int ViewerId { get; }
	public int ViewableUserId { get; }

	public ViewRight(int viewerId, int viewableUserId) {
		ViewerId = viewerId;
		ViewableUserId = viewableUserId;
	}
}