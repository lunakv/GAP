using Users.UserAccess;
using Users.UserAccess.Entities;
using Users.UserAccess.Storage;
using Users.UserSharing.UserViewSharing.Exceptions;
using Users.UserSharing.UserViewSharing.Storage;

namespace Users.UserSharing.UserViewSharing;

public class UserViewController {
	private readonly IUserViewRightSaver _userViewRightSaver;
	private readonly IUserViewSettingsFetcher _userViewSettingsFetcher;
	private readonly IUserFetcher _userFetcher;

	public UserViewController(IUserViewRightSaver userViewRightSaver, IUserViewSettingsFetcher userViewSettingsFetcher, IUserFetcher userFetcher) {
		_userViewRightSaver = userViewRightSaver;
		_userViewSettingsFetcher = userViewSettingsFetcher;
		_userFetcher = userFetcher;
	}

	public Task<UserViewSettings> GetUserViewSettingsAsync(int userId) {
		return _userViewSettingsFetcher.GetUserViewSettingsAsync(userId: userId);
	}

	public async Task<UserViewSettings> AddUserViewerAsync(int userId, string viewerEmail) {
		await _userFetcher.FindUserByIdAsync(userId: userId).ConfigureAwait(false);
		User viewer = await _userFetcher.FindUserByEmailAsync(email: viewerEmail).ConfigureAwait(false);

		if (userId == viewer.Id) {
			throw new ViewerAndViewableUserSameException(
				$"View right viewer (id = {userId}) cannot be the user who gives view right to {viewerEmail}");
		}

		var accessRight = new ViewRight(viewerId: viewer.Id, viewableUserId: userId);
		await _userViewRightSaver.AddViewRightAsync(accessRight).ConfigureAwait(false);

		return await _userViewSettingsFetcher.GetUserViewSettingsAsync(userId: userId).ConfigureAwait(false);
	}

	public async Task<UserViewSettings> RemoveUserViewerAsync(int userId, int viewerId) {
		await _userFetcher.FindUserByIdAsync(userId: userId).ConfigureAwait(false);
		User viewer = await _userFetcher.FindUserByIdAsync(userId: viewerId).ConfigureAwait(false);

		if (userId == viewer.Id) {
			throw new ViewerAndViewableUserSameException(
				$"View right viewer (id = {userId}) cannot be the user who gives view right to {viewerId}");
		}

		await _userViewRightSaver.RemoveViewRightAsync(viewerId: viewer.Id, viewableUserId: userId).ConfigureAwait(false);

		return await _userViewSettingsFetcher.GetUserViewSettingsAsync(userId: userId).ConfigureAwait(false);
	}
}