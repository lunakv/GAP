using Users.UserAccess;
using Users.UserAccess.Entities;
using Users.UserAccess.Storage;
using Users.UserSharing.UserManagementSharing.Exceptions;
using Users.UserSharing.UserManagementSharing.Storage;

namespace Users.UserSharing.UserManagementSharing;

public class UserManagementController {
	private readonly IUserManagementSettingsFetcher _userManagementSettingsFetcher;
	private readonly IUserManageRightSaver _userManageRightSaver;
	private readonly IUserFetcher _userFetcher;

	public UserManagementController(IUserFetcher userFetcher, IUserManagementSettingsFetcher userManagementSettingsFetcher,
		IUserManageRightSaver userManageRightSaver) {
		_userManagementSettingsFetcher = userManagementSettingsFetcher;
		_userManageRightSaver = userManageRightSaver;
		_userFetcher = userFetcher;
	}

	public Task<UserManagementSettings> GetUserManagementSettingsAsync(int userId) {
		return _userManagementSettingsFetcher.GetUserManagementSettingsAsync(userId: userId);
	}

	public async Task<UserManagementSettings> AddUserManagerAsync(int userId, string managerEmail) {
		await _userFetcher.FindUserByIdAsync(userId).ConfigureAwait(false);
		User manager = await _userFetcher.FindUserByEmailAsync(managerEmail)
			.ConfigureAwait(false);

		if (userId == manager.Id) {
			throw new ManagerAndManagedUserSameException($"Manage rule cannot have the same manager and managedUser {userId}");
		}

		await _userManageRightSaver.AddManageRightAsync(new ManageRight(managerId: manager.Id, managedUserId: userId))
			.ConfigureAwait(false);
		return await _userManagementSettingsFetcher.GetUserManagementSettingsAsync(userId: userId).ConfigureAwait(false);
	}

	public async Task<UserManagementSettings> RemoveUserManagerAsync(int userId, int managerId) {
		// Check that both user and manager exist.
		await _userFetcher.FindUserByIdAsync(managerId).ConfigureAwait(false);
		await _userFetcher.FindUserByIdAsync(userId)
			.ConfigureAwait(false);

		if (userId == managerId) {
			throw new ManagerAndManagedUserSameException($"Manage rule cannot have the same manager and managedUser {userId}");
		}

		await _userManageRightSaver.RemoveManageRightAsync(managerId: managerId, managedUserId: userId)
			.ConfigureAwait(false);
		return await _userManagementSettingsFetcher.GetUserManagementSettingsAsync(userId: userId).ConfigureAwait(false);
	}
}