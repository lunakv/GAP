namespace Users.UserSharing.UserManagementSharing; 

public class UserManagementSettings  {
	public IReadOnlyCollection<int> Managers { get; }
	public IReadOnlyCollection<int> ManagedUsers { get; }

	public UserManagementSettings(IReadOnlyCollection<int> managers, IReadOnlyCollection<int> managedUsers) {
		Managers = managers;
		ManagedUsers = managedUsers;
	}

	public UserManagementSettings(int userId, IReadOnlyCollection<ManageRight> manageRights) {
		Managers = manageRights.Where(r => r.ManagedUserId == userId).Select(right => right.ManagerId).ToList();
		ManagedUsers = manageRights.Where(r => r.ManagerId == userId).Select(right => right.ManagedUserId).ToList();
	}

	public bool IsUserManageable(int userId) {
		return ManagedUsers.Any(managedUser => managedUser == userId);
	}
}