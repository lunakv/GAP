namespace Users.UserSharing.UserManagementSharing.Storage; 

public interface IUserManageRightSaver {
	Task AddManageRightAsync(ManageRight manageRight);
	Task RemoveManageRightAsync(int managerId, int managedUserId);
}