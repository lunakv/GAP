namespace Users.UserSharing.UserManagementSharing.Storage; 

public interface IUserManagementSettingsFetcher {
	Task<UserManagementSettings> GetUserManagementSettingsAsync(int userId);
}