namespace Users.UserSharing.UserManagementSharing; 

public struct ManageRight {
	public int ManagerId { get; }
	public int ManagedUserId { get; }

	public ManageRight(int managerId, int managedUserId) {
		ManagerId = managerId;
		ManagedUserId = managedUserId;
	}
}