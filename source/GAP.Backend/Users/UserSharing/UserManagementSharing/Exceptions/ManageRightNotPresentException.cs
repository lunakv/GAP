using Users.Exceptions;

namespace Users.UserSharing.UserManagementSharing.Exceptions; 

public class ManageRightNotPresentException : UserException {
	public ManageRightNotPresentException(string message) : base(message) { }
	public override string GetRepresentation => nameof(ManageRightAlreadyPresentException);
}