using Users.Exceptions;

namespace Users.UserSharing.UserManagementSharing.Exceptions; 

public class ManagerAndManagedUserSameException : UserException {
	public ManagerAndManagedUserSameException(string message) : base(message) { }
	public override string GetRepresentation => nameof(ManagerAndManagedUserSameException);
}