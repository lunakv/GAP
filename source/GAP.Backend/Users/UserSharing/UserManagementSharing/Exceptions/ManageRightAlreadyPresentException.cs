using Users.Exceptions;

namespace Users.UserSharing.UserManagementSharing.Exceptions;

public class ManageRightAlreadyPresentException : UserException {
	public ManageRightAlreadyPresentException(string message) : base(message) { }
	public override string GetRepresentation => nameof(ManageRightAlreadyPresentException);
}