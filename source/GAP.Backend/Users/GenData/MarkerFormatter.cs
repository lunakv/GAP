namespace Users.GenData; 

using StrMarker = StrMarkerCollection<string, int>.StrMarker;

public class MarkerFormatter {
	public static StrMarkerCollection<string, int> FormatMarkers(StrMarkerCollection<string, int> markers) {
		return new StrMarkerCollection<string, int>(
			markers
				.CloneAsList()
				.Select(m => new StrMarker(name: FormatMarker(m.Name), value: m.Value))
				.ToDictionary(keySelector: m => m.Name, elementSelector: m => m.Value)
		);
	}

	public static string FormatMarker(string marker) {
		// Remove `-` and any whitespaces (namely spaces - ` `).
		marker = string.Concat(
			marker
				.Replace(oldChar: '-', newChar: ' ')
				.Where(ch => !Char.IsWhiteSpace(ch))
		);
		
		char[] anyNumber = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
		// Uppercase everything before the first number if any.
		int lastNumberIndex = marker.LastIndexOfAny(anyOf: anyNumber);
		if (lastNumberIndex != -1) {
			marker = marker[..lastNumberIndex].ToUpper() + marker[lastNumberIndex..];
		} else {
			marker = marker.ToUpper();
		}

		return marker;
	}
}