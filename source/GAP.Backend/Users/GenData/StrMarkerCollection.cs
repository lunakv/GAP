using System.Collections.Immutable;

namespace Users.GenData;

public class StrMarkerCollection<TMarker, TValue> : IEquatable<StrMarkerCollection<TMarker, TValue>> where TMarker : notnull where TValue : IEquatable<TValue> {
	private readonly IDictionary<TMarker, TValue> _markers;

	public IList<TMarker> MarkerOrdering = new List<TMarker>();

	public StrMarkerCollection(IDictionary<TMarker, TValue> markers) {
		_markers = markers;
	}

	public StrMarkerCollection() {
		_markers = new Dictionary<TMarker, TValue>();
	}

	public TValue this[TMarker marker] {
		get { return _markers[marker]; }
		set => AssignOrUpdateMarkerValue(marker, value);
	}

	public int Count => _markers.Count;

	public ICollection<TMarker> ListMarkers() {
		return _markers.Keys;
	}

	public bool HasMarker(TMarker marker) {
		return _markers.ContainsKey(marker);
	}
	
	public StrMarker? FindMarker(Predicate<TMarker> p) {
		var found = _markers.Select(kvp => new StrMarker(name: kvp.Key, kvp.Value)).Where(m => p(m.Name)).ToList();
		if (found.Count != 0) {
			return found[0];
		} 

		return null;
	}

	public bool HasMarkers(IEnumerable<TMarker> markers) {
		foreach (TMarker marker in markers) {
			if (!HasMarker(marker: marker)) {
				return false;
			}
		}

		return true;
	}

	public void AssignOrUpdateMarkerValue(TMarker marker, TValue value) {
		_markers[marker] = value;
	}

	public void AssignMarkerValue(TMarker marker, TValue value) {
		if (_markers.ContainsKey(marker)) {
			throw new ArgumentException($"Marker {marker} is already present in Marker Collection.");
		}

		_markers[marker] = value;
	}

	public void AddMarkers(StrMarkerCollection<TMarker, TValue> markers) {
		foreach (var marker in markers.CloneAsList()) {
			if (_markers.ContainsKey(marker.Name)) {
				throw new ArgumentException($"Marker {marker.Name} is already present in Marker Collection.");
			}

			_markers[marker.Name] = marker.Value;
		}
	}

	public StrMarker GetMarker(TMarker marker) {
		if (!HasMarker(marker)) {
			throw new ArgumentException($"Marker collection does not contain marker {marker}");
		}

		return new StrMarker(name: marker, _markers[marker]);
	}

	public void UpdateMarkerValue(TMarker marker, TValue value) {
		if (!_markers.ContainsKey(marker)) {
			throw new ArgumentException($"Marker {marker} is not in STR marker collection");
		}

		_markers[marker] = value;
	}

	public IDictionary<TMarker, TValue> CloneAsDictionary() {
		return _markers.ToImmutableDictionary();
	}

	public IList<StrMarker> CloneAsList() {
		return _markers.Select(kvp => new StrMarker(name: kvp.Key, value: kvp.Value)).ToList();
	}
	
	public IList<StrMarker> CloneAsOrderedList(Func<TMarker, TMarker, bool> areSame) {
		List<StrMarker> orderedMarkers = new List<StrMarker>();
		var asList = CloneAsList();

		if (MarkerOrdering.Count == 0) {
			return asList;
		}

		foreach (TMarker marker in MarkerOrdering) {
			var foundMarkers = asList.Where(m => areSame(m.Name, marker)).ToList();
			if (foundMarkers.Count != 0) {
				orderedMarkers.Add(new StrMarker(name: marker, value: foundMarkers[0].Value));
			}
		}

		foreach (var marker in asList) {
			if (!MarkerOrdering.Any(m => areSame(marker.Name, m))) {
				orderedMarkers.Add(marker);
			}
		}

		return orderedMarkers;
	}

	public StrMarkerCollection<TMarker, TValue> Clone() {
		return new StrMarkerCollection<TMarker, TValue>(CloneAsDictionary());
	}

	public struct StrMarker {
		public TMarker Name { get; }
		public TValue Value { get; }

		public StrMarker(TMarker name, TValue value) {
			Name = name;
			Value = value;
		}
	}

	public bool Equals(StrMarkerCollection<TMarker, TValue>? other) {
		if (other == null) {
			return false;
		}

		if (_markers.Keys.Except(other._markers.Keys).Any()) {
			return false;
		}

		if (other._markers.Keys.Except(_markers.Keys).Any()) {
			return false;
		}

		if (_markers.Keys.Any(marker => !_markers[marker].Equals(other._markers[marker]))) {
			return false;
		}

		return true;
	}

	public override bool Equals(object? obj) {
		if (obj == null) {
			return false;
		}

		if (obj is StrMarkerCollection<TMarker, TValue> strMarkers) {
			return Equals(strMarkers);
		}

		return false;
	}

	public override int GetHashCode() {
		return _markers.GetHashCode();
	}
}