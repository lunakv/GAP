﻿namespace Users.GenData;

public class GeneticData {
	public StrMarkerCollection<string, int> StrMarkers { get; }
	public Haplogroup.Haplogroup Haplogroup { get; }
	public IReadOnlyCollection<int> OriginalStrDataSources { get; }

	public GeneticData(IDictionary<string, int> strMarkers, Haplogroup.Haplogroup haplogroup, IReadOnlyCollection<int> originalStrDataSources) {
		StrMarkers = new StrMarkerCollection<string, int>(strMarkers);
		Haplogroup = haplogroup;
		OriginalStrDataSources = originalStrDataSources;
	}
	
	public GeneticData(StrMarkerCollection<string, int> strMarkers, Haplogroup.Haplogroup haplogroup, IReadOnlyCollection<int> originalStrDataSources) {
		StrMarkers = strMarkers;
		Haplogroup = haplogroup;
		OriginalStrDataSources = originalStrDataSources;
	}
}