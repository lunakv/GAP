﻿namespace Users.GenData.Haplogroup;

public class Haplogroup : IEquatable<Haplogroup> {
    public int Id { get; }
    public string Name { get; }

    public Haplogroup(int id, string name) {
        Id = id;
        Name = name;
    }

    public override int GetHashCode() {
        return Id;
    }

    public override bool Equals(object? obj) {
        if (obj == null) {
            return false;
        }

        if (obj.GetType() != typeof(Haplogroup)) {
            return false;
        }

        return Equals((Haplogroup)obj);
    }

    public bool Equals(Haplogroup? other) {
        if (other == null) {
            return false;
        }

        return this.Id == other.Id;
    }

}
