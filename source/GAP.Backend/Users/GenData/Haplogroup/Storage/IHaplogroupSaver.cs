namespace Users.GenData.Haplogroup.Storage;

public interface IHaplogroupSaver {
	Task AddHaplogroupAsync(string haplogroupName);
	Task AddHaplogroupsAsync(IList<string> haplogroupNames);
}