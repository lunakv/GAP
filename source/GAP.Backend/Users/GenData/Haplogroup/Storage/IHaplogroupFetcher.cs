using Users.UserAccess.UserFilter;

namespace Users.GenData.Haplogroup.Storage;

public interface IHaplogroupFetcher {
	Task<Haplogroup> FindHaplogroupByIdAsync(int haplogroupId);
	Task<IList<Haplogroup>> GetHaplogroupsAsync();   
	Task<IList<Haplogroup>> GetHaplogroupsAsync(IHaplogroupFilter haplogroupFilter);   
}