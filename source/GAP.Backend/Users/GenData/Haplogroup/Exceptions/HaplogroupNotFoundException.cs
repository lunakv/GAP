using Users.Exceptions;

namespace Users.GenData.Haplogroup.Exceptions;

public class HaplogroupNotFoundException : UserException {
	public HaplogroupNotFoundException(string message) : base(message) { }
	public override string GetRepresentation => nameof(HaplogroupNotFoundException);
}