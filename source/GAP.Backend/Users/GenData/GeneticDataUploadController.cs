using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Users.GenData.Merge;
using Users.GenData.OriginalStrDataSources;
using Users.GenData.OriginalStrDataSources.Exceptions;
using Users.GenData.OriginalStrDataSources.Storage;
using Users.GenData.OriginalStrDataSources.Storage.File;
using Users.GenData.Parsers;
using Users.HaplogroupPrediction;
using Users.UserAccess.Storage;

namespace Users.GenData;

public class GeneticDataUploadController {
	private readonly IUserFetcher _userFetcher;
	private readonly GeneticDataUploader _geneticDataUploader;
	private readonly IOriginalStrDataSourceFetcher _originalStrDataSourceFetcher;
	private readonly ILogger _logger;

	public GeneticDataUploadController(IUserFetcher userFetcher, GeneticDataUploader geneticDataUploader,
		IOriginalStrDataSourceFetcher originalStrDataSourceFetcher, ILoggerFactory loggerFactory) {
		_userFetcher = userFetcher;
		_geneticDataUploader = geneticDataUploader;
		_originalStrDataSourceFetcher = originalStrDataSourceFetcher;
		_logger = loggerFactory.CreateLogger(nameof(GeneticDataUploadController));
	}

	public async Task<GeneticData> UploadExternalStrFile(int userId, Stream file, IExternalStrFileParser externalStrFileParser) {
		Stream parseStream = await ParsingUtils.CopyFileStreamAsync(fileStream: file).ConfigureAwait(false);
		StrParsingResult strParsingResult = await externalStrFileParser.ParseAsync(parseStream).ConfigureAwait(false);

		// Check that user with userId exists.
		await _userFetcher.FindUserByIdAsync(userId)
			.ConfigureAwait(false);

		IList<OriginalStrDataSource> originalStrDataSources = await _originalStrDataSourceFetcher.GetOriginalStrDataSourceAsync(userId).ConfigureAwait(false);

		foreach (OriginalStrDataSource source in originalStrDataSources.Where(source => source.TestProvider == strParsingResult.GeneticTestProvider)) {
			if (source.StrMarkers.Equals(strParsingResult.StrMarkers)) {
				throw new OriginalStrDataSourcePresentException($"Source with {source.Id} already contains exactly the same str markers.");
			}
		}

		string fileName = await _geneticDataUploader.UploadExternalStrFileAsync(file: file, userId: userId, fileExtension: strParsingResult.FileExtension);

		var newStrDataSource = new OriginalStrDataSource(
			userId: userId,
			testProvider: strParsingResult.GeneticTestProvider,
			strMarkers: strParsingResult.StrMarkers,
			fileName: fileName
		);

		return await _geneticDataUploader.AddOriginalStrDataSourceAsync(newOriginalStrDataSource: newStrDataSource)
			.ConfigureAwait(false);
	}
}