namespace Users.GenData.Parsers; 

public class StrParsingResult {
	public StrMarkerCollection<string, int> StrMarkers { get; }
	public GeneticTestProvider GeneticTestProvider { get; }
	public string FileExtension { get; }

	public StrParsingResult(StrMarkerCollection<string, int> strMarkers, GeneticTestProvider geneticTestProvider, string fileExtension) {
		StrMarkers = strMarkers;
		GeneticTestProvider = geneticTestProvider;
		FileExtension = fileExtension;
	}
}