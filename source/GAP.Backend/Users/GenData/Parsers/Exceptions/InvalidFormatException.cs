namespace Users.GenData.Parsers.Exceptions;

public class InvalidFormatException : Exception {
	public InvalidFormatException(string message) : base(message) { }
}