using Users.Exceptions;

namespace Users.GenData.Parsers.Exceptions; 

public class ParsingException : UserException {
	public ParsingException(string message) : base(message) { }
	public ParsingException(string message, Exception? innerException) : base(message: message) { }
	public override string GetRepresentation => nameof(ParsingException);
}