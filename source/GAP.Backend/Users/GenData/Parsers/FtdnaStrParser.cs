﻿using System.Data;

namespace Users.GenData.Parsers; 

public class FtdnaStrParser : IExternalStrFileParser {
    public string GeneticTestProvider => "FTDNA";
    public string FileExtension => ".csv";

    public async Task<StrParsingResult> ParseAsync(string filePath) { 
        FileStreamOptions fso = new FileStreamOptions();
        fso.Options = FileOptions.Asynchronous;
        fso.Mode = FileMode.Open;
        fso.Access = FileAccess.Read;
        await using Stream stream = File.Open(filePath, fso);
        return await ParseAsync(stream).ConfigureAwait(false);
    }


    /// <exception cref="ArgumentException">If the file has a wrong format.</exception>
    /// <exception cref="ConstraintException">If the data are layout badly within the file.</exception>
    /// <exception cref="FormatException">If some of the marker values are not a number.</exception>
    public async Task<StrParsingResult> ParseAsync(Stream stream) {
        string? first = null, second = null;
        using (var reader = new StreamReader(stream))
        {
            if(!reader.EndOfStream)
                first = await reader.ReadLineAsync().ConfigureAwait(false);
            if(!reader.EndOfStream)
                second = await reader.ReadLineAsync().ConfigureAwait(false);
        }
        //await stream.DisposeAsync().ConfigureAwait(false);
        if(first == null || second == null) {
            throw new ArgumentException("File for data parse does not have matching data format.");
        }
        var names = first.Split(',');
        var values = second.Split(',');
        if(values.Length != names.Length) {
            throw new ConstraintException("Names and values count does not match in ftdna file.");
        }
        Dictionary<string, int> result = new();
        for(int i = 0; i < names.Length; i++) {
            AddOnePair(result, names[i], values[i]);
        }

        return new StrParsingResult(
            strMarkers: new StrMarkerCollection<string, int>(result),
            geneticTestProvider: new GeneticTestProvider(GeneticTestProvider),
            fileExtension: FileExtension
        );
    }

    private void AddOnePair(Dictionary<string, int> dictionary, string name, string value) {
        if(value.StartsWith('\"')) {
            value = value.Substring(1);
        }
        if(value.EndsWith('\"')) {
            value = value.Substring(0, value.Length - 1);
        }
        value = value.Trim();
        ParsingUtils.MarkersFromOneInput(dictionary, name, value);
    }
}