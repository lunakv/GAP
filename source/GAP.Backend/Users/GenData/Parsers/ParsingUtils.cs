﻿namespace Users.GenData.Parsers; 

public class ParsingUtils {
    /// <summary>
    /// Turns a record of marker into a marker values and adds it into a given dictionary. 
    /// Multiple values may be included, but all with be added with key based on the name given.
    /// </summary>
    /// <param name="into">The dictionary the result values will be added into.</param>
    /// <param name="name">The name of the marker, dictionary keys will be based on this.</param>
    /// <param name="value">The string including the marker values.</param>
    public static void MarkersFromOneInput(Dictionary<string, int> into, string name, string value) {
        var values = value.Split('-')
            .Select(s => s.Trim())
            .Where(s => s.Length > 0)
            .Select(s => int.Parse(s))
            .ToList();
        if(values.Count == 1) {
            into.Add(name, values[0]);
            return;
        }
        //if value is ommitted, then count of values is 0 and so nothing is added for given marker
        char ending = 'a';
        foreach(int val in values) {
            into.Add(name + (ending++), val);
        }
    }
    
    public static async Task<Stream> CopyFileStreamAsync(Stream fileStream) {
        var stream = new MemoryStream();
        await fileStream.CopyToAsync(stream).ConfigureAwait(false);

        // Set both stream to their beginning.
        stream.Seek(0, SeekOrigin.Begin);
        fileStream.Seek(0, SeekOrigin.Begin);
        return stream;
    }
}