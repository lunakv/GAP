namespace Users.GenData.Parsers; 

public interface IExternalStrFileParser {
	Task<StrParsingResult> ParseAsync(Stream file);
}