namespace Users.GenData.Parsers; 

public class GeneticTestProvider {
	public string Name { get; }

	public GeneticTestProvider(string name) {
		Name = name;
	}
}