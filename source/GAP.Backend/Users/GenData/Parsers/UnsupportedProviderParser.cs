namespace Users.GenData.Parsers; 

public class UnsupportedProviderParser : IExternalStrFileParser {

	private readonly GeneticTestProvider _geneticTestProvider;
	private readonly string _fileExtension;
	private readonly StrMarkerCollection<string, int> _strMarkers;

	public UnsupportedProviderParser(GeneticTestProvider geneticTestProvider, string fileExtension, StrMarkerCollection<string, int> strMarkers) {
		_geneticTestProvider = geneticTestProvider;
		_fileExtension = fileExtension;
		_strMarkers = strMarkers;
	}

	public Task<StrParsingResult> ParseAsync(Stream file) {
		return Task.FromResult(new StrParsingResult(
			strMarkers: _strMarkers.Clone(),
			geneticTestProvider: _geneticTestProvider,
			fileExtension: _fileExtension
		));
	}
}