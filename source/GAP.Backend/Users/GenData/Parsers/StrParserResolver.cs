using Users.GenData.Parsers.Exceptions;

namespace Users.GenData.Parsers;

public class StrParserResolver : IExternalStrFileParser {
	private readonly IList<IExternalStrFileParser> _parsers = new List<IExternalStrFileParser>() {
		new FtdnaStrParser()
	};

	public async Task<StrParsingResult> ParseAsync(Stream file) {
		var parseExceptions = new List<Exception>();
		foreach (IExternalStrFileParser parser in _parsers) {
			try {
				Stream parseStream = await ParsingUtils.CopyFileStreamAsync(fileStream: file).ConfigureAwait(false);
				StrParsingResult parsingResult = await parser.ParseAsync(file: parseStream).ConfigureAwait(false);
				return parsingResult;
			} catch (Exception e) {
				parseExceptions.Add(e);
			}
		}

		throw new ParsingException(
			message: "Str marker file parsing failed.",
			innerException: new AggregateException("Input file in unsupported format.", parseExceptions)
		);
	}
}