using Microsoft.Extensions.Logging;
using Users.GenData.Merge;
using Users.GenData.OriginalStrDataSources;
using Users.GenData.OriginalStrDataSources.Storage;
using Users.GenData.OriginalStrDataSources.Storage.File;
using Users.HaplogroupPrediction;
using Users.UserAccess.Entities;
using Users.UserAccess.Storage;

namespace Users.GenData;

/// <summary>
/// Main class for uploading genetic data. It does all the things that need to be done to correctly upload new STR markers
/// to the application.
/// </summary>
public class GeneticDataUploader {
	private readonly ISaverFactory _saverFactory;
	private readonly IUserFetcher _userFetcher;
	private readonly IHaplogroupPredictor _haplogroupPredictor;
	private readonly IOriginalStrFileSaver _originalStrFileSaver;
	
	private string CreateExternalStrFileBaseName(int userId) => $"str-external-{userId}-";
	private string LabStrFileBaseName => $"str-lab-{DateTime.Today:MM-dd-yy}-";

	public GeneticDataUploader(ISaverFactory saverFactory, IUserFetcher userFetcher, IHaplogroupPredictor haplogroupPredictor,
		IOriginalStrFileSaver originalStrFileSaver) {
		_saverFactory = saverFactory;
		_userFetcher = userFetcher;
		_haplogroupPredictor = haplogroupPredictor;
		_originalStrFileSaver = originalStrFileSaver;
	}

	public async Task<GeneticData> AddOriginalStrDataSourceAsync(OriginalStrDataSource newOriginalStrDataSource) {
		GeneticData geneticData;
		try {
			await _saverFactory.StartTransactionAsync().ConfigureAwait(false);
			geneticData = await AddOriginalStrDataSourceNoTransactionAsync(newOriginalStrDataSource: newOriginalStrDataSource).ConfigureAwait(false);
			await _saverFactory.CommitAsync().ConfigureAwait(false);
		} finally {
			await _saverFactory.DisposeAsync().ConfigureAwait(false);
		}

		return geneticData;
	}
	
	public async Task<ICollection<GeneticData>> AddOriginalStrDataSourcesAsync(IReadOnlyCollection<OriginalStrDataSource> newOriginalStrDataSources) {
		List<GeneticData> updatedData = new List<GeneticData>(); 
		try {
			await _saverFactory.StartTransactionAsync().ConfigureAwait(false);
			foreach (OriginalStrDataSource source in newOriginalStrDataSources) {
				updatedData.Add(await AddOriginalStrDataSourceNoTransactionAsync(newOriginalStrDataSource: source).ConfigureAwait(false));
			}
			await _saverFactory.CommitAsync().ConfigureAwait(false);
		} finally {
			await _saverFactory.DisposeAsync().ConfigureAwait(false);
		}

		return updatedData;
	}

	public async Task<GeneticData> CreateGeneticDataAsync(StrMarkerCollection<string, int> strMarkers, IReadOnlyCollection<int> originalStrDataSources) {
		return new GeneticData(
			strMarkers: strMarkers,
			haplogroup: await _haplogroupPredictor.Predict(strMarkers)
				.ConfigureAwait(false),
			originalStrDataSources: originalStrDataSources
		);
	}

	public Task<string> UploadExternalStrFileAsync(Stream file, int userId, string fileExtension) {
		return _originalStrFileSaver.SaveFileAsync(
			baseFileName: CreateExternalStrFileBaseName(userId: userId),
			extension: fileExtension,
			file: file
		);
	}
	
	public Task<string> UploadLabStrFileAsync(Stream file, string fileExtension) {
		return _originalStrFileSaver.SaveFileAsync(
			baseFileName: LabStrFileBaseName,
			extension: fileExtension,
			file: file
		);
	}

	public async Task<GeneticData> AddOriginalStrDataSourceNoTransactionAsync(OriginalStrDataSource newOriginalStrDataSource) {
		User user = await _userFetcher.FindUserByIdAsync(userId: newOriginalStrDataSource.UserId).ConfigureAwait(false);

		newOriginalStrDataSource.StrMarkers = MarkerFormatter.FormatMarkers(newOriginalStrDataSource.StrMarkers);
		
		var originalStrDataSaver = _saverFactory.GetOriginalStrDataSourceSaver();
		newOriginalStrDataSource = await originalStrDataSaver.AddOriginalStrDataSourceAsync(newOriginalStrDataSource)
			.ConfigureAwait(false);

		StrMarkerCollection<string, int> strMarkers = newOriginalStrDataSource.StrMarkers;
		List<int> originalStrDataSources = new List<int>() { newOriginalStrDataSource.Id };
		if (user.GeneticData != null) {
			StrMergeResult mergeResult = new StrMerger().Merge(user.GeneticData.StrMarkers, newOriginalStrDataSource.StrMarkers);
			strMarkers = mergeResult.MergedStrMarkers;
			var conflictSaver = _saverFactory.GetStrConflictSaver();
			await conflictSaver.AddConflictAsync(userId: user.Id, conflictingMarkers: mergeResult.ConflictingMarkers)
				.ConfigureAwait(false);
			originalStrDataSources.AddRange(user.GeneticData.OriginalStrDataSources);
		}

		GeneticData geneticData = await CreateGeneticDataAsync(strMarkers: strMarkers, originalStrDataSources: originalStrDataSources)
			.ConfigureAwait(false);

		var userSaver = _saverFactory.GetUserSaver();
		await userSaver.UpdateGeneticDataAsync(userId: user.Id, geneticData: geneticData).ConfigureAwait(false);

		return geneticData;
	}

	public async Task<GeneticData> AddStrMarkersWithPredictedHaplogroupAsync(OriginalStrDataSource originalStrDataSource, Haplogroup.Haplogroup haplogroup) {
		var originalStrDataSaver = _saverFactory.GetOriginalStrDataSourceSaver();
		
		originalStrDataSource.StrMarkers = MarkerFormatter.FormatMarkers(originalStrDataSource.StrMarkers);
		
		OriginalStrDataSource addedSource = await originalStrDataSaver.AddOriginalStrDataSourceAsync(originalStrDataSource)
			.ConfigureAwait(false);
		GeneticData geneticData = new GeneticData(
			strMarkers: originalStrDataSource.StrMarkers,
			haplogroup: haplogroup,
			originalStrDataSources: new List<int>() { addedSource.Id });
			var userSaver = _saverFactory.GetUserSaver();
		await userSaver.UpdateGeneticDataAsync(userId: originalStrDataSource.UserId, geneticData: geneticData).ConfigureAwait(false);
		
		return geneticData;
	}
}