using Microsoft.Extensions.Options;
using Users.ClosestRelativeSearch.MarkerSets;

namespace Users.GenData; 

public class MarkerSorter {
	private readonly IMarkerSetFetcher _markerSetFetcher;
	private List<string>? _markers;

	public MarkerSorter(IOptions<UsersConfig> userConfig) {
		_markerSetFetcher = new MarkerSetFetcher(markerSetsDirectory: userConfig.Value.ClosestRelativeSearchConfig.MarkerSetsDirectory);
		_markers = null;
	}

	public async Task LoadOrderingAsync() {
		if (_markers != null) {
			return;
		}
		try {
			var markerSets = await _markerSetFetcher.FetchMarkerSetsAsync();
			_markers = markerSets.OrderBy(m => m.MarkerCount).Reverse().First().Markers.ToList();
		} catch { }
	}
	
	public void AddOrdering(StrMarkerCollection<string, int> markers) {
		if (_markers != null) {
			markers.MarkerOrdering = _markers;
		}
	}

	public IReadOnlyCollection<string> FixMarkerNames(IReadOnlyCollection<string> markers) {
		if (_markers == null) {
			return markers;
		} 
		return markers.Select(marker => _markers.FirstOrDefault(m => MarkerComparer.AreSame(marker, m)) ?? marker).ToList();
	}
}