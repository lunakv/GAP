using Users.GenData.Conflict.Exceptions;
using Users.GenData.Conflict.Storage;
using Users.HaplogroupPrediction;
using Users.UserAccess;
using Users.UserAccess.Entities;

namespace Users.GenData.Conflict;

public class ConflictController {
	private readonly IStrConflictFetcher _strConflictFetcher;
	private readonly ISaverFactory _saverFactory;
	private readonly CompleteUserFetcher _userFetcher;
	private readonly GeneticDataUploader _geneticDataUploader;

	public ConflictController(IStrConflictFetcher strConflictFetcher, ISaverFactory saverFactory,
		CompleteUserFetcher userFetcher, GeneticDataUploader geneticDataUploader) {
		_strConflictFetcher = strConflictFetcher;
		_saverFactory = saverFactory;
		_userFetcher = userFetcher;
		_geneticDataUploader = geneticDataUploader;
	}

	public Task<ICollection<StrConflict>> GetStrConflictsAsync(IReadOnlyCollection<int>? userIds) {
		return _strConflictFetcher.GetConflictsAsync(userIds: userIds);
	}

	public async Task<StrMarkerCollection<string, int>> FixStrConflictAsync(int userId, StrMarkerCollection<string, int> newAggregatedMarkersValues) {
		StrConflict conflict = await _strConflictFetcher.GetUserStrConflictAsync(userId: userId)
			.ConfigureAwait(false);
		CompleteUser user = await _userFetcher.FindCompleteUserByIdAsync(conflict.UserId)
			.ConfigureAwait(false);

		if (!user.GeneticData.StrMarkers.HasMarkers(newAggregatedMarkersValues.ListMarkers())) {
			throw new NewAggregatedMarkersHaveDifferentMarkersException("New aggregated markers have markers which are not in current.");
		}

		if (!newAggregatedMarkersValues.HasMarkers(user.GeneticData.StrMarkers.ListMarkers())) {
			throw new NewAggregatedMarkersHaveDifferentMarkersException("New aggregated markers do not have all markers they should.");
		}

		GeneticData geneticData = await _geneticDataUploader.CreateGeneticDataAsync(
			strMarkers: newAggregatedMarkersValues,
			originalStrDataSources: user.GeneticData.OriginalStrDataSources
		).ConfigureAwait(false);

		await _saverFactory.StartTransactionAsync().ConfigureAwait(false);
		var userSaver = _saverFactory.GetUserSaver();
		await userSaver.UpdateGeneticDataAsync(userId: userId, geneticData: geneticData)
			.ConfigureAwait(false);
		var conflictSaver = _saverFactory.GetStrConflictSaver();
		await conflictSaver.DeleteConflictAsync(userId: userId)
			.ConfigureAwait(false);
		await _saverFactory.CommitAsync().ConfigureAwait(false);

		return newAggregatedMarkersValues;
	}
}