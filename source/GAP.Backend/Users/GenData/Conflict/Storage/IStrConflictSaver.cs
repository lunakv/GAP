namespace Users.GenData.Conflict.Storage; 

public interface IStrConflictSaver {
	Task DeleteConflictAsync(int userId);
	Task AddConflictAsync(int userId, IReadOnlyCollection<string> conflictingMarkers);
}