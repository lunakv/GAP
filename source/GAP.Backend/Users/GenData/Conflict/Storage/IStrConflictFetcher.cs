namespace Users.GenData.Conflict.Storage; 

public interface IStrConflictFetcher {
	Task<ICollection<StrConflict>> GetConflictsAsync(IReadOnlyCollection<int>? userIds = null);
	Task<ICollection<StrConflict>> TryGetConflictsAsync(IReadOnlyCollection<int>? userIds = null);
	Task<StrConflict> GetUserStrConflictAsync(int userId);
}