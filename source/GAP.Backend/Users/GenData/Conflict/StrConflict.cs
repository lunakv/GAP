namespace Users.GenData.Conflict;

public class StrConflict {
	public int UserId { get; }
	public IReadOnlyCollection<string> ConflictingMarkers { get; }

	public StrConflict(int userId, IReadOnlyCollection<string> conflictingMarkers) {
		UserId = userId;
		ConflictingMarkers = conflictingMarkers;
	}
}