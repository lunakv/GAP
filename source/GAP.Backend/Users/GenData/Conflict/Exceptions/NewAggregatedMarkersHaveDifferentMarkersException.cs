using Users.Exceptions;

namespace Users.GenData.Conflict.Exceptions;

public class NewAggregatedMarkersHaveDifferentMarkersException : UserException {
	public NewAggregatedMarkersHaveDifferentMarkersException(string message) : base(message) { }
	public override string GetRepresentation => nameof(NewAggregatedMarkersHaveDifferentMarkersException);
}