using Users.Exceptions;

namespace Users.GenData.Conflict.Exceptions; 

public class StrConflictNotFoundException : UserException {
	public StrConflictNotFoundException(string message) : base(message) { }
	public override string GetRepresentation => nameof(StrConflictNotFoundException);
}