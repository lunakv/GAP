namespace Users.GenData; 

public class MarkerComparer {
	public static bool AreSame(string a, string b) {
		a = MarkerFormatter.FormatMarker(a);
		b = MarkerFormatter.FormatMarker(b);

		a = a.ToLower();
		b = b.ToLower();

		if (a == b) {
			return true;
		}

		const string yPrefix = "y";
		if (yPrefix + a == b) {
			return true;
		}

		if (a == yPrefix + b) {
			return true;
		}

		return false;
	}
}