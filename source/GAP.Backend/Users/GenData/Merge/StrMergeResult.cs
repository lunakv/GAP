namespace Users.GenData.Merge; 

public record StrMergeResult(
	StrMarkerCollection<string, int> MergedStrMarkers,
	IReadOnlyCollection<string> ConflictingMarkers
);