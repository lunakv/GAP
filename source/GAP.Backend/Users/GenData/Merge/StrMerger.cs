namespace Users.GenData.Merge;

public class StrMerger {
	public StrMergeResult Merge(StrMarkerCollection<string, int> currentMarkers, StrMarkerCollection<string, int> additionMarkers) {
		var mergedMarkers = new StrMarkerCollection<string, int>();
		List<string> conflictingMarkers = new();

		foreach (var marker in currentMarkers.CloneAsList()) {
			if (additionMarkers.HasMarker(marker.Name) && additionMarkers[marker.Name] != marker.Value) {
				conflictingMarkers.Add(marker.Name);
			}

			mergedMarkers[marker.Name] = marker.Value;
		}

		foreach (var marker in additionMarkers.CloneAsList()) {
			if (!currentMarkers.HasMarker(marker.Name)) {
				mergedMarkers[marker.Name] = marker.Value;
			}
		}

		return new StrMergeResult(
			MergedStrMarkers: mergedMarkers,
			ConflictingMarkers: conflictingMarkers
		);
	}
}