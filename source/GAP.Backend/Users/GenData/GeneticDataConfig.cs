namespace Users.GenData; 

public class GeneticDataConfig {
	public string OriginalStrFileDirectory { get; set; }
	public bool UseNevGenPredictor { get; set; }

	public GeneticDataConfig() {
		OriginalStrFileDirectory = string.Empty;
		UseNevGenPredictor = false;
	}
}