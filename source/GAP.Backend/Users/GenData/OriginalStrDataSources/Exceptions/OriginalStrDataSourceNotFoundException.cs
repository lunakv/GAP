using Users.Exceptions;

namespace Users.GenData.OriginalStrDataSources.Exceptions; 

public class OriginalStrDataSourceNotFoundException : UserException {
	public OriginalStrDataSourceNotFoundException(string message) : base(message) { }
	public override string GetRepresentation => nameof(OriginalStrDataSourceNotFoundException);
}