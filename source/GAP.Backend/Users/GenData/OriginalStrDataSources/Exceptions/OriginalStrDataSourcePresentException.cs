using Users.Exceptions;

namespace Users.GenData.OriginalStrDataSources.Exceptions;

public class OriginalStrDataSourcePresentException : UserException {
	public OriginalStrDataSourcePresentException(string message) : base(message) { }
	public OriginalStrDataSourcePresentException(string message, Exception? innerException) : base(message, innerException) { }
	public override string GetRepresentation => nameof(OriginalStrDataSourcePresentException);
}