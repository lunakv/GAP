using Microsoft.Extensions.Options;

namespace Users.GenData.OriginalStrDataSources.Storage.File;

public class OriginalStrFileFetcher : IOriginalStrFileFetcher {
	private readonly string _originalStrFileDirectory;

	public OriginalStrFileFetcher(string originalStrFileDirectory) {
		_originalStrFileDirectory = originalStrFileDirectory;
	}

	public OriginalStrFileFetcher(IOptions<UsersConfig> config) {
		_originalStrFileDirectory = config.Value.GeneticDataConfig.OriginalStrFileDirectory;
	}

	public string GetFilePath(string fileName) {
		return Path.Combine(_originalStrFileDirectory, fileName);
	}
}