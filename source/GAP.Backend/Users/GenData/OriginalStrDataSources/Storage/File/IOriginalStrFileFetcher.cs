namespace Users.GenData.OriginalStrDataSources.Storage.File;

public interface IOriginalStrFileFetcher {
	string GetFilePath(string fileName);
}