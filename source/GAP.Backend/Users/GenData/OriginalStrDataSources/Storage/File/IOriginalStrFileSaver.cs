namespace Users.GenData.OriginalStrDataSources.Storage.File; 

public interface IOriginalStrFileSaver {
	Task<string> SaveFileAsync(string baseFileName, string extension, Stream file);
}