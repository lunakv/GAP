using Microsoft.Extensions.Options;

namespace Users.GenData.OriginalStrDataSources.Storage.File;

public class OriginalStrFileSaver : IOriginalStrFileSaver {
	private readonly string _originalStrFileDirectory;

	public OriginalStrFileSaver(string originalStrFileDirectory) {
		_originalStrFileDirectory = originalStrFileDirectory;
	}

	public OriginalStrFileSaver(IOptions<UsersConfig> usersConfig) {
		_originalStrFileDirectory = usersConfig.Value.GeneticDataConfig.OriginalStrFileDirectory;
	}

	public async Task<string> SaveFileAsync(string baseFileName, string extension, Stream file) {
		string fileName = baseFileName + Guid.NewGuid() + extension;
		string path = Path.Combine(_originalStrFileDirectory, fileName);
		await using var outputStream = new FileStream(path, new FileStreamOptions() {
			Access = FileAccess.Write,
			Mode = FileMode.Create,
			Options = FileOptions.Asynchronous,
		});
		await file.CopyToAsync(outputStream).ConfigureAwait(false);
		return fileName;
	}

	public void Initialize() {
		if (!Directory.Exists(_originalStrFileDirectory)) {
			try {
				Directory.CreateDirectory(_originalStrFileDirectory);
			} catch (Exception ex) {
				throw new Exception($"Creating directory ${_originalStrFileDirectory} failed.", ex);
			}
		}
	}
}