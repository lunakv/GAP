namespace Users.GenData.OriginalStrDataSources.Storage; 

public interface IOriginalStrDataSourceSaver {
	Task<OriginalStrDataSource> AddOriginalStrDataSourceAsync(OriginalStrDataSource originalStrDataSource);
}