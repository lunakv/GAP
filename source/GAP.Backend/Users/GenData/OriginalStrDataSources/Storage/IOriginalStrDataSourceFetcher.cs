namespace Users.GenData.OriginalStrDataSources.Storage; 

public interface IOriginalStrDataSourceFetcher {
	Task<IList<OriginalStrDataSource>> GetOriginalStrDataSourceAsync(int userId);
	Task<IDictionary<int, OriginalStrDataSource>> GetOriginalStrDataSourcesAsync(IReadOnlyCollection<int> sourceIds);
	Task<ICollection<OriginalStrDataSource>> TryGetOriginalStrDataSourceAsync(string fileName);
}