using Users.GenData.Parsers;

namespace Users.GenData.OriginalStrDataSources; 

public class OriginalStrDataSource {
	public int Id { get; }
	public int UserId { get; }
	public GeneticTestProvider TestProvider { get; }
	public StrMarkerCollection<string, int> StrMarkers { get; set; }
	public string? FileName { get; }

	public OriginalStrDataSource(int userId, GeneticTestProvider testProvider, StrMarkerCollection<string, int> strMarkers, string? fileName) {
		Id = -1;
		UserId = userId;
		TestProvider = testProvider;
		StrMarkers = strMarkers;
		FileName = fileName;
	}
	
	public OriginalStrDataSource(int id, int userId, GeneticTestProvider testProvider, StrMarkerCollection<string, int> strMarkers, string? fileName) {
		Id = id;
		UserId = userId;
		TestProvider = testProvider;
		StrMarkers = strMarkers;
		FileName = fileName;
	}
	
}