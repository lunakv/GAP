using Users.GenData;
using Users.UserAccess.Entities;

namespace Users.UserAccess.Storage;

public interface IUserSaver {
	Task AddUserAsync(User user);
	Task UpdateProfileAsync(int userId, Profile profile, int? regionId);
	Task UpdateAncestorAsync(int userId, Ancestor ancestor);
	Task UpdateGeneticDataAsync(int userId, GeneticData geneticData);
	Task UpdateAdministrationAgreementSignStatus(int userId, bool administrationAggreementSigned);
	Task UpdateEmailAsync(int userId, string email);
}