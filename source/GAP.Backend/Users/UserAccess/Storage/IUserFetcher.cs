using Users.UserAccess.Entities;

namespace Users.UserAccess.Storage;

public interface IUserFetcher {
	Task<User> FindUserByIdAsync(int userId);
	Task<IReadOnlyDictionary<int, User>> FindUsersByIdAsync(IReadOnlyCollection<int> userIds);
	Task<User> FindUserByEmailAsync(string email);
	Task<IList<User>> GetUsersAsync();
}