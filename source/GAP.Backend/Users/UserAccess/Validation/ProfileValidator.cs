using FluentValidation;
using Users.UserAccess.Entities;

namespace Users.UserAccess.Validation; 

public class ProfileValidator : AbstractValidator<Profile> {
	private const string GivenNameError = "ProfileGivenNameValidationFailure";
	private const string FamilyNameError = "ProfileFamilyValidationFailure";
	private const string EmailError = "ProfileEmailValidationFailure";
	private const string PhoneNumberError = "ProfilePhoneNumberValidationFailure";
	private const string BirthDateError = "ProfileBirthDateValidationFailure";
	
	public ProfileValidator() {
		RuleFor(profile => profile.GivenName)
			.NotNull()
			.Matches(Patterns.CreateHumanNameRegex())
			.WithErrorCode(GivenNameError);
		
		RuleFor(profile => profile.FamilyName)
			.NotNull()
			.Matches(Patterns.CreateHumanNameRegex())
			.WithErrorCode(FamilyNameError);
		
		RuleFor(profile => profile.Email)
			.NotNull()
			.EmailAddress()
			.WithErrorCode(EmailError);
		
		RuleFor(profile => profile.PhoneNumber)
			.Matches(Patterns.CreatePhoneRegex()).When(profile => profile.PhoneNumber != null)
			.WithErrorCode(PhoneNumberError);

		RuleFor(profile => profile.ResidenceAddress).SetValidator(new ExpandedAddressValidator());
		
		RuleFor(profile => profile.CorrespondenceAddress).SetValidator(new AddressValidator()!)
			.When(profile => profile.CorrespondenceAddress != null);

		RuleFor(profile => profile.BirthDate)
			.Must(date => date == null || TimeValidationHelper.BeforeNow(date.Value))
			.WithErrorCode(BirthDateError);

	}
}