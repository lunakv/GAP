using FluentValidation;
using Users.UserAccess.Entities;

namespace Users.UserAccess.Validation;

public class AddressValidator : AbstractValidator<Address> {
	private const string TownError = "AddressTownValidationFailure";
	private const string StreetError = "AddressStreetValidationFailure";
	private const string ZipCodeError = "AddressZipCodeValidationFailure";

	public AddressValidator() {
		RuleFor(address => address.Town)
			.Matches(Patterns.CreateLocationRegex())
			.WithErrorCode(TownError);

		RuleFor(address => address.Street)
			.Matches(Patterns.CreateLocationRegex())
			.WithErrorCode(StreetError);

		RuleFor(address => address.ZipCode)
			.Matches(Patterns.CreateZipCodeRegex())
			.WithErrorCode(ZipCodeError);
	}
}