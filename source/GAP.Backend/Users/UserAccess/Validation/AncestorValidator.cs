using FluentValidation;
using Users.UserAccess.Entities;

namespace Users.UserAccess.Validation; 

public class AncestorValidator : AbstractValidator<Ancestor> {
	private const string GivenNameError = "AncestorGivenNameValidationFailure";
	private const string FamilyNameError = "AncestorFamilyValidationFailure";
	private const string BirthDateError = "AncestorBirthDateValidationFailure";
	private const string PlaceOfBirthError = "AncestorPlaceOfBirthValidationFailure";
	private const string DeathDateError = "AncestorDeathDateValidationFailure";
	private const string PlaceOfDeathError = "AncestorPlaceOfDeathValidationFailure";
	
	public AncestorValidator() {
		RuleFor(address => address.Address).SetValidator(new ExpandedAddressValidator());
		
		RuleFor(ancestor => ancestor.GivenName)
			.Matches(Patterns.CreateHumanNameRegex()).When(ancestor => ancestor.GivenName != null)
			.WithErrorCode(GivenNameError);
		
		RuleFor(ancestor => ancestor.FamilyName)
			.Matches(Patterns.CreateHumanNameRegex()).When(ancestor => ancestor.FamilyName != null)
			.WithErrorCode(FamilyNameError);
		
		RuleFor(ancestor => ancestor.BirthDate)
			.Must(date => date == null || TimeValidationHelper.BeforeNow(date.Value))
			.WithErrorCode(BirthDateError);
		
		RuleFor(ancestor => ancestor.PlaceOfBirth)
			.Matches(Patterns.CreateLocationRegex()).When(ancestor => ancestor.PlaceOfBirth != null)
			.WithErrorCode(PlaceOfBirthError);
		
		RuleFor(ancestor => ancestor.DeathDate)
			.Must(date => date == null || TimeValidationHelper.BeforeNow(date.Value))
			.WithErrorCode(DeathDateError);
		
		RuleFor(ancestor => ancestor.PlaceOfDeath)
			.Matches(Patterns.CreateLocationRegex()).When(ancestor => ancestor.PlaceOfDeath != null)
			.WithErrorCode(PlaceOfDeathError);

		RuleFor(ancestor => ancestor.BirthDate)
			.Must((ancestor, _) => ancestor.BirthDate == null || ancestor.DeathDate == null ||
			                       TimeValidationHelper.ABeforeB(a: ancestor.BirthDate.Value, b: ancestor.DeathDate.Value))
			.WithErrorCode(BirthDateError);
	}
}