using System.Text.RegularExpressions;

namespace Users.UserAccess.Validation;

public class Patterns {
	private const string LocationPattern = @"^([\w ./-]){1,60}$";
	private const string HumanNamePattern = @"^([\w -]){1,40}$";
	private const string PhonePattern = @"^(\+([0-9]){1,3})?([0-9]){9}$";
	private const string ZipCodePattern = @"^([0-9]){5}$";

	public static Regex CreateLocationRegex() => CreateRegex(LocationPattern);
	public static Regex CreateHumanNameRegex() => CreateRegex(HumanNamePattern);
	public static Regex CreatePhoneRegex() => CreateRegex(PhonePattern);
	public static Regex CreateZipCodeRegex() => CreateRegex(ZipCodePattern);

	private static Regex CreateRegex(string pattern) {
		return new Regex(pattern: pattern, options: RegexOptions.CultureInvariant);
	}
}