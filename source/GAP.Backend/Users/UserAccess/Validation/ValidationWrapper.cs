using FluentValidation.Results;
using Users.UserAccess.Validation.Exceptions;

namespace Users.UserAccess.Validation; 

public class ValidationWrapper {
	public static async Task ValidateAsync(Func<Task<ValidationResult>> validationFunction) {
		ValidationResult validationResult = await validationFunction();
		if (!validationResult.IsValid) {
			var error = validationResult.Errors.First();
			string message = $"'{error.AttemptedValue}' fails validation for {error.PropertyName}.";
			throw new ValidationException(message: message, representation: error.ErrorCode);
		}
	}
}