using FluentValidation;
using Users.UserAccess.Entities;

namespace Users.UserAccess.Validation;

public class ExpandedAddressValidator : AbstractValidator<ExpandedAddress> {
	private const string TownError = "ExpandedAddressTownValidationFailure";
	private const string MunicipalityError = "ExpandedAddressMunicipalityValidationFailure";
	private const string CountyError = "ExpandedAddressCountyValidationFailure";
	private const string StreetError = "ExpandedAddressStreetValidationFailure";
	private const string ZipCodeError = "ExpandedAddressZipCodeValidationFailure";

	public ExpandedAddressValidator() {
		RuleFor(expandedAddress => expandedAddress.Town)
			.Matches(Patterns.CreateLocationRegex()).When(expandedAddress => expandedAddress.Town != null)
			.WithErrorCode(TownError);

		RuleFor(expandedAddress => expandedAddress.Municipality)
			.Matches(Patterns.CreateLocationRegex()).When(expandedAddress => expandedAddress.Municipality != null)
			.WithErrorCode(MunicipalityError);

		RuleFor(expandedAddress => expandedAddress.County)
			.Matches(Patterns.CreateLocationRegex()).When(expandedAddress => expandedAddress.County != null)
			.WithErrorCode(CountyError);

		RuleFor(expandedAddress => expandedAddress.Street)
			.Matches(Patterns.CreateLocationRegex()).When(expandedAddress => expandedAddress.Street != null)
			.WithErrorCode(StreetError);

		RuleFor(expandedAddress => expandedAddress.ZipCode)
			.Matches(Patterns.CreateZipCodeRegex()).When(expandedAddress => expandedAddress.ZipCode != null)
			.WithErrorCode(ZipCodeError);
	}
}