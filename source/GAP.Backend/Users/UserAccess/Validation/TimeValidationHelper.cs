namespace Users.UserAccess.Validation; 

public class TimeValidationHelper {
	public static bool BeforeNow(DateOnly date) {
		return ABeforeB(a: date, b: DateOnly.FromDateTime(DateTime.Now));
	}

	public static bool ABeforeB(DateOnly a, DateOnly b) {
		return a.CompareTo(b) < 1;	
	}
}