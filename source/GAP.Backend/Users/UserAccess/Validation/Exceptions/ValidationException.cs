using Users.Exceptions;

namespace Users.UserAccess.Validation.Exceptions;

public class ValidationException : UserException {
	private readonly string _representation;

	public ValidationException(string message, string representation) : base(message) {
		_representation = representation;
	}

	public override string GetRepresentation => _representation;
}