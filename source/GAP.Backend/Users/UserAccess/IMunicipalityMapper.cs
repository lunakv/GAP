namespace Users.UserAccess; 

public interface IMunicipalityMapper {
	Task<int> GetMunicipalityRegionIdAsync(string municipality);
	Task<string> FindCorrespondingCountyAsync(int municipalityId);
}