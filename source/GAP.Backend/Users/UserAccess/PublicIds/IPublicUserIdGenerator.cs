namespace Users.UserAccess.PublicIds; 

public interface IPublicUserIdGenerator {
	public string GetNextId();
}