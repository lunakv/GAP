namespace Users.UserAccess.PublicIds; 

public class GuidPublicUserIdGenerator : IPublicUserIdGenerator {
	public string GetNextId() {
		return Guid.NewGuid().ToString();
	}
}