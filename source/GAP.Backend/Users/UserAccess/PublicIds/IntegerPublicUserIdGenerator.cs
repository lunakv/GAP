using IdManagement;
using Users.UserAccess.Entities;

namespace Users.UserAccess.PublicIds; 

public class IntegerPublicUserIdGenerator : IPublicUserIdGenerator {
	private readonly IdGenerator _idGenerator;
	public IntegerPublicUserIdGenerator(IdGenerator idGenerator) {
		_idGenerator = idGenerator;
	}

	public string GetNextId() {
		return _idGenerator.FoolproofNext(typeof(User)).ToString();
	}
}