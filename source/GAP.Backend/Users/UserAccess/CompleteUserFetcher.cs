using Users.UserAccess.Entities;
using Users.UserAccess.Exceptions;
using Users.UserAccess.Storage;
using Users.UserAccess.UserFilter;

namespace Users.UserAccess; 

public class CompleteUserFetcher {
	private readonly IUserFetcher _userFetcher;

	public CompleteUserFetcher(IUserFetcher userFetcher) {
		_userFetcher = userFetcher;
	}

	public async Task<CompleteUser> FindCompleteUserByIdAsync(int userId) {
		User user = await _userFetcher.FindUserByIdAsync(userId)
			.ConfigureAwait(false);

		if (user.GeneticData != null) {
			return new CompleteUser(
				id: user.Id,
				administrationAgreementSigned: user.AdministrationAgreementSigned,
				publicId: user.PublicId,
				profile: user.Profile,
				ancestor: user.Ancestor,
				regionId: user.RegionId,
				geneticData: user.GeneticData,
				administratorData: user.AdministratorData
			);
		}

		throw new UserHasNoStrMarkersException($"User with id {userId} found but they did not contain genetic data.");
	}

	public Task<IList<CompleteUser>> GetUsersAsync() {
		return GetUsersAsync(new EmptyUserFilter());
	}

	public async Task<IList<CompleteUser>> GetUsersAsync(UserFilter.UserFilter userFilter) {
		IList<User> users = await _userFetcher.GetUsersAsync()
			.ConfigureAwait(false);

		return new HasGeneticDataFilter().Chain(userFilter).Filter(users).Select(user => new CompleteUser(
			id: user.Id,
			administrationAgreementSigned: user.AdministrationAgreementSigned,
			publicId: user.PublicId,
			profile: user.Profile,
			ancestor: user.Ancestor,
			regionId: user.RegionId,
			geneticData: user.GeneticData!,
			administratorData: user.AdministratorData
		)).ToList();
	}

}