using Users.UserAccess.Entities;

namespace Users.UserAccess.UserFilter;

public class FamilyNameUserFilter : UserFilter {

	private readonly IReadOnlyCollection<string> _eligibleFamilyNames;

	public FamilyNameUserFilter(IReadOnlyCollection<string> eligibleFamilyNames) {
		_eligibleFamilyNames = eligibleFamilyNames;
	}
	
	public FamilyNameUserFilter(params string[] eligibleFamilyNames) {
		_eligibleFamilyNames = eligibleFamilyNames;
	}
	
	public override IEnumerable<User> Filter(IEnumerable<User> users) {
		if (_eligibleFamilyNames.Count == 0) {
			return users;
		}
		return users.Where(user => _eligibleFamilyNames.Contains(user.Profile.FamilyName));
	}
}