using Users.GenData.Haplogroup;

namespace Users.UserAccess.UserFilter; 

public interface IHaplogroupFilter {
	IEnumerable<Haplogroup> Filter(IEnumerable<Haplogroup> haplogroups);
}