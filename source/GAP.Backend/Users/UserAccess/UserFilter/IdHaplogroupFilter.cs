using Users.GenData.Haplogroup;

namespace Users.UserAccess.UserFilter;

public class IdHaplogroupFilter : IHaplogroupFilter {

	private readonly IReadOnlyCollection<int> _eligibleHaplogroupIds;

	public IdHaplogroupFilter(IReadOnlyCollection<int> eligibleHaplogroupIds) {
		_eligibleHaplogroupIds = eligibleHaplogroupIds;
	}

	public IEnumerable<Haplogroup> Filter(IEnumerable<Haplogroup> haplogroups) {
		if (_eligibleHaplogroupIds.Count == 0) {
			return haplogroups;
		}
		return haplogroups.Where(haplogroup => _eligibleHaplogroupIds.Contains(haplogroup.Id));
	}
}