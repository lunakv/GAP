using Users.UserAccess.Entities;

namespace Users.UserAccess.UserFilter; 

public abstract class UserFilter {
	public abstract IEnumerable<User> Filter(IEnumerable<User> users);

	public UserFilter Chain(UserFilter filter) {
		return new UserFilterDecorator(this, filter);
	}
}