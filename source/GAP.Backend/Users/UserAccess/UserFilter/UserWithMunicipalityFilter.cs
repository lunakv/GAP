using Users.UserAccess.Entities;

namespace Users.UserAccess.UserFilter; 

public class UserWithMunicipalityFilter : UserFilter {
	public override IEnumerable<User> Filter(IEnumerable<User> users) {
		return users.Where(u => u.Profile.ResidenceAddress.Municipality != null);
	}
}