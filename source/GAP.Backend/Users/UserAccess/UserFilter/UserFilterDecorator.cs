using Users.UserAccess.Entities;

namespace Users.UserAccess.UserFilter;

public class UserFilterDecorator : UserFilter {

	private readonly UserFilter _wrappee;
	private readonly UserFilter _filter;

	public UserFilterDecorator(UserFilter wrappee, UserFilter filter) {
		_wrappee = wrappee;
		_filter = filter;
	}

	public override IEnumerable<User> Filter(IEnumerable<User> users) {
		return _filter.Filter(_wrappee.Filter(users));
	}
}