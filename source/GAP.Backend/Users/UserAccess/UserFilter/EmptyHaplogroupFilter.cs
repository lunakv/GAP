using Users.GenData.Haplogroup;

namespace Users.UserAccess.UserFilter;

public class EmptyHaplogroupFilter : IHaplogroupFilter {
	public IEnumerable<Haplogroup> Filter(IEnumerable<Haplogroup> haplogroups) {
		return haplogroups;
	}
}