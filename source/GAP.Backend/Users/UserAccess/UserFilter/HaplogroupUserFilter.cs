using Users.UserAccess.Entities;

namespace Users.UserAccess.UserFilter;

public class HaplogroupUserFilter : UserFilter {
	private readonly IReadOnlyCollection<int> _eligibleHaplogroupIds;

	public HaplogroupUserFilter(IReadOnlyCollection<int> eligibleHaplogroupIds) {
		_eligibleHaplogroupIds = eligibleHaplogroupIds;
	}
	
	public HaplogroupUserFilter(params int[] eligibleHaplogroupIds) {
		_eligibleHaplogroupIds = eligibleHaplogroupIds;
	}
	
	public sealed override IEnumerable<User> Filter(IEnumerable<User> users) {
		if (_eligibleHaplogroupIds.Count == 0) {
			return users;
		}
		return users.Where(user => user.GeneticData != null && _eligibleHaplogroupIds.Contains(user.GeneticData.Haplogroup.Id));
	}
}