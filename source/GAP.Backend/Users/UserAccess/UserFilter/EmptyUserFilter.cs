using Users.UserAccess.Entities;

namespace Users.UserAccess.UserFilter;

public class EmptyUserFilter : UserFilter {
	public override IEnumerable<User> Filter(IEnumerable<User> users) {
		return users;
	}
}