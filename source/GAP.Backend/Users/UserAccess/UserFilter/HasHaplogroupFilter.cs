using Users.UserAccess.Entities;

namespace Users.UserAccess.UserFilter; 

public class HasHaplogroupFilter : UserFilter {
	public override IEnumerable<User> Filter(IEnumerable<User> users) {
		return users.Where(u => u.GeneticData != null);
	}
}