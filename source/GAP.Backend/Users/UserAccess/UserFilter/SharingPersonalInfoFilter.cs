using Users.UserAccess.Entities;

namespace Users.UserAccess.UserFilter; 

public class SharingPersonalInfoFilter : UserFilter {
	public override IEnumerable<User> Filter(IEnumerable<User> users) {
		return users.Where(u => u.Id < 20);
	}
}