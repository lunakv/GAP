using Users.Exceptions;

namespace Users.UserAccess.Exceptions; 

public class UserNotFoundException : UserException {
	public UserNotFoundException(string message) : base(message) { }
	public override string GetRepresentation => nameof(UserNotFoundException);
}