using Users.Exceptions;

namespace Users.UserAccess.Exceptions; 

public class ForbiddenEmailChangeAttemptException : UserException {
	public ForbiddenEmailChangeAttemptException(string message) : base(message) { }
	public override string GetRepresentation => nameof(ForbiddenEmailChangeAttemptException);
}