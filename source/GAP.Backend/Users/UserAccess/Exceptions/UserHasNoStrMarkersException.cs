using Users.Exceptions;

namespace Users.UserAccess.Exceptions;

public class UserHasNoStrMarkersException : UserException {
	public UserHasNoStrMarkersException(string message) : base(message) { }
	public override string GetRepresentation => nameof(UserHasNoStrMarkersException);
}