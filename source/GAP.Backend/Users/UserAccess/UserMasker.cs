using Users.GenData;
using Users.UserAccess.Entities;

namespace Users.UserAccess; 

public class UserMasker {
	public static CompleteUser MaskUserWithoutSignedAgreement(CompleteUser user) {
		bool agreementSigned = user.AdministrationAgreementSigned;
		return new CompleteUser(
			id: user.Id,
			administrationAgreementSigned: user.AdministrationAgreementSigned,
			publicId: user.PublicId,
			profile: new Profile(
				givenName: agreementSigned ? user.Profile.GivenName : string.Empty,
				familyName: user.Profile.FamilyName,
				email: agreementSigned ? user.Profile.Email : string.Empty,
				phoneNumber: agreementSigned ? user.Profile.PhoneNumber : null,
				residenceAddress: new ExpandedAddress(
					town: agreementSigned ? user.Profile.ResidenceAddress.Town : null,
					municipality: agreementSigned ? user.Profile.ResidenceAddress.Municipality : null,
					county: agreementSigned ? user.Profile.ResidenceAddress.County : null,
					street: agreementSigned ? user.Profile.ResidenceAddress.Street : null,
					zipCode: agreementSigned ? user.Profile.ResidenceAddress.ZipCode : null
				),
				correspondenceAddress: agreementSigned ? user.Profile.CorrespondenceAddress : null,
				birthDate: agreementSigned ? user.Profile.BirthDate : null
			),
			ancestor: user.Ancestor,
			regionId: agreementSigned ? user.RegionId : -1,
			geneticData: new GeneticData(
				strMarkers: user.GeneticData.StrMarkers,
				haplogroup: user.GeneticData.Haplogroup,
				originalStrDataSources: agreementSigned ? user.GeneticData.OriginalStrDataSources : new List<int>()
			),
			administratorData: agreementSigned ? user.AdministratorData : new AdministratorData()
		);
	}
}