using Users.HaplogroupPrediction;
using Users.UserAccess.Entities;
using Users.UserAccess.Exceptions;
using Users.UserAccess.Storage;
using Users.UserAccess.Validation;

namespace Users.UserAccess;

public class UserController {
	private readonly IUserSaver _userSaver;
	private readonly IUserFetcher _userFetcher;
	private readonly IMunicipalityMapper _municipalityMapper;

	public UserController(IUserSaver userSaver, IUserFetcher userFetcher, IMunicipalityMapper municipalityMapper) {
		_userSaver = userSaver;
		_userFetcher = userFetcher;
		_municipalityMapper = municipalityMapper;
	}

	public async Task<Profile> UpdateProfileAsync(int userId, Profile profile) {
		await ValidationWrapper.ValidateAsync(() => new ProfileValidator().ValidateAsync(profile));
		User user = await _userFetcher.FindUserByIdAsync(userId: userId).ConfigureAwait(false);
		if (user.Profile.Email != profile.Email) {
			throw new ForbiddenEmailChangeAttemptException($"Email update to {profile.Email} through UpdateProfile detected for user {userId}");
		}

		int? regionId = profile.ResidenceAddress.Municipality != null
			? await _municipalityMapper.GetMunicipalityRegionIdAsync(municipality: profile.ResidenceAddress.Municipality)
			: null;
		
		await _userSaver.UpdateProfileAsync(userId: userId, profile: profile, regionId: regionId).ConfigureAwait(false);
		return profile;
	}

	public async Task<Ancestor> UpdateAncestorAsync(int userId, Ancestor ancestor) {
		await ValidationWrapper.ValidateAsync(() => new AncestorValidator().ValidateAsync(ancestor));
		await _userSaver.UpdateAncestorAsync(userId: userId, ancestor: ancestor).ConfigureAwait(false);
		return ancestor;
	}

	public async Task<bool> SignAdministrationAgreement(int userId) {
		await _userSaver.UpdateAdministrationAgreementSignStatus(userId: userId, administrationAggreementSigned: true)
			.ConfigureAwait(false);
		return true;
	}
}