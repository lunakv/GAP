using Users.GenData;
using Users.UserAccess.Entities.Interfaces;

namespace Users.UserAccess.Entities; 

public class CompleteUser : IHasUserId, IHasGeneticData {
	public int Id { get; }
	public bool AdministrationAgreementSigned { get; }
	public string PublicId { get; }
	public Profile Profile { get; set; }
	public Ancestor Ancestor { get; set; }
	public int RegionId { get; }
	public GeneticData GeneticData { get; set; }
	public AdministratorData AdministratorData { get; }

	public CompleteUser(int id, bool administrationAgreementSigned, string publicId, Profile profile, Ancestor ancestor, int regionId, GeneticData geneticData, AdministratorData administratorData) {
		Id = id;
		AdministrationAgreementSigned = administrationAgreementSigned;
		PublicId = publicId;
		Profile = profile;
		Ancestor = ancestor;
		RegionId = regionId;
		GeneticData = geneticData;
		AdministratorData = administratorData;
	}
	
	public User AsUser() {
		return new User(
			id: Id,
			administrationAgreementSigned: AdministrationAgreementSigned,
			publicId: PublicId,
			profile: Profile,
			ancestor: Ancestor,
			regionId: RegionId,
			geneticData: GeneticData,
			administratorData: AdministratorData
		);
	}

}