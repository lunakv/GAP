namespace Users.UserAccess.Entities; 

public class Address {
	public string Town { get; }
	public string Street { get; }
	public string ZipCode { get; }

	public Address(string town, string street, string zipCode) {
		Town = town;
		Street = street;
		ZipCode = zipCode;
	}
}