namespace Users.UserAccess.Entities;

public class Ancestor {
	public ExpandedAddress Address { get; }
	public string? GivenName { get; }
	public string? FamilyName { get; }
	public DateOnly? BirthDate { get; }
	public string? PlaceOfBirth { get; }
	public DateOnly? DeathDate { get; }
	public string? PlaceOfDeath { get; }

	
	public Ancestor(ExpandedAddress address, string? givenName = null, string? familyName = null, DateOnly? birthDate = null, string? placeOfBirth = null, DateOnly? deathDate = null,
		string? placeOfDeath = null) {
		Address = address;
		GivenName = givenName;
		FamilyName = familyName;
		BirthDate = birthDate;
		PlaceOfBirth = placeOfBirth;
		DeathDate = deathDate;
		PlaceOfDeath = placeOfDeath;
	}
}