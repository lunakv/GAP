namespace Users.UserAccess.Entities.Interfaces; 

public interface IHasUserId {
	int Id { get; }	
}