using Users.GenData;

namespace Users.UserAccess.Entities.Interfaces;

public interface IHasGeneticData {
	GeneticData GeneticData { get; }
}
