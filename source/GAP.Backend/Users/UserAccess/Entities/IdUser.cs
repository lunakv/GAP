using Users.UserAccess.Entities.Interfaces;

namespace Users.UserAccess.Entities; 

public class IdUser : IHasUserId {
	public int Id { get; }

	public IdUser(int id) {
		Id = id;
	}
}