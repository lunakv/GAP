namespace Users.UserAccess.Entities;

public class Profile {
	public string GivenName { get; }
	public string FamilyName { get; }
	public string Email { get; }
	public string? PhoneNumber { get; }
	public ExpandedAddress ResidenceAddress { get; }
	public Address? CorrespondenceAddress { get; }
	public DateOnly? BirthDate { get; }

	public Profile(string givenName, string familyName, string email, string? phoneNumber,
		ExpandedAddress residenceAddress, Address? correspondenceAddress, DateOnly? birthDate) {
		GivenName = givenName;
		FamilyName = familyName;
		Email = email;
		PhoneNumber = phoneNumber;
		ResidenceAddress = residenceAddress;
		CorrespondenceAddress = correspondenceAddress;
		BirthDate = birthDate;
	}
}