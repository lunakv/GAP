namespace Users.UserAccess.Entities;

public class AdministratorData {
	public string? Id { get; }
	public string? Note { get; }

	public AdministratorData(string? id = null, string? note = null) {
		Id = id;
		Note = note;
	}
}