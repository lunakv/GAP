namespace Users.UserAccess.Entities; 

public class ExpandedAddress {
	public string? Town { get; }
	public string? Municipality { get; }
	public string? County { get; }
	public string? Street { get; }
	public string? ZipCode { get; }

	public ExpandedAddress(string? town = null, string? municipality = null, string? county = null, string? street = null, string? zipCode = null) {
		Town = town;
		Municipality = municipality;
		County = county;
		Street = street;
		ZipCode = zipCode;
	}
	
}