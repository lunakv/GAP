using Users.ClosestRelativeSearch;
using Users.GenData;

namespace Users;

public class UsersConfig {
	public GeneticDataConfig GeneticDataConfig { get; set; }
	public ClosestRelativeSearchConfig ClosestRelativeSearchConfig { get; set; }

	public UsersConfig() {
		GeneticDataConfig = new GeneticDataConfig();
		ClosestRelativeSearchConfig = new ClosestRelativeSearchConfig();
	}
}