﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.GenData;
using Users.GenData.Haplogroup;

namespace Users.HaplogroupPrediction {
	public interface IHaplogroupPredictor {
		/// <summary>
		/// Predicts a haplogroup given input str markers.
		/// </summary>
		/// <returns>The predicted Haplogroup. Even created if necessary.</returns>
		Task<Haplogroup> Predict(StrMarkerCollection<string, int> markers);

		/// <summary>
		/// If more than 1 users will be prompted then it's advised to use for performance improvement.
		/// </summary>
		Task InitializeComputations();

	}
}
