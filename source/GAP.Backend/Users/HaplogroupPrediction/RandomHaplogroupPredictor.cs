using Users.GenData;
using Users.GenData.Haplogroup;
using Users.GenData.Haplogroup.Storage;
using Users.UserAccess;

namespace Users.HaplogroupPrediction; 

/// <summary>
/// Predictor for predicting random existing haplogroups from STR data. Good for mock data.
/// </summary>
public class RandomHaplogroupPredictor : IHaplogroupPredictor {
	private readonly IHaplogroupFetcher _haplogroupFetcher;
	private static int _counter = 0;

	public RandomHaplogroupPredictor(IHaplogroupFetcher haplogroupFetcher) {
		_haplogroupFetcher = haplogroupFetcher;
	}

	public async Task<Haplogroup> Predict(StrMarkerCollection<string, int> markers) {
		IList<Haplogroup> haplogroups = await _haplogroupFetcher.GetHaplogroupsAsync().ConfigureAwait(false);
		return haplogroups[(_counter++ % haplogroups.Count)];
	}

	public Task InitializeComputations() {
		return Task.CompletedTask;
	}
}