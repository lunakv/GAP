﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.ComponentModel.DataAnnotations;
using Users.GenData;
using Users.GenData.Haplogroup;
using Users.GenData.Haplogroup.Storage;
using Users.UserAccess;
using Users.UserAccess.UserFilter;

namespace Users.HaplogroupPrediction {
	public class NevgenHaplogroupPredictor : IHaplogroupPredictor{
		private readonly IHaplogroupFetcher _fetcher;
		private readonly IHaplogroupSaver _haploSaver;
		private IEnumerable<string>? _markersOrder;
		private HttpClient? _client;
		public NevgenHaplogroupPredictor(IHaplogroupFetcher fetcher, IHaplogroupSaver saver) {
			this._fetcher = fetcher;
			this._haploSaver = saver;
			_markersOrder = null;
			_client = null;
		}

		public async Task<Haplogroup> Predict(StrMarkerCollection<string, int> origMarkers) {
			var client = _client ?? new HttpClient();
			string markers = await BuildMarkers(client, origMarkers).ConfigureAwait(false);
			Dictionary<string, string> values = new Dictionary<string, string>()
			{
				{"action", "calc" },
				{"ht", markers },
				{"order", "0" }, //0 here indicates ftdna order
				{"language", "1" }, //english
				{"spremiste", "0"},
				{"izabranasnpopcija", "No known SNP"},
			};
			var content = new FormUrlEncodedContent(values);
			string site = "https://nevgen.org/Racunac.php";
			var mes = await client.PostAsync(site, content).ConfigureAwait(false);
			if(!mes.IsSuccessStatusCode) { //TODO: userException místo http
				throw new HttpRequestException("could not reach " + site + " functionalities");
			}
			var res = await mes.Content.ReadAsStringAsync().ConfigureAwait(false);
			var arr = JsonSerializer.Deserialize<string[]>(res);
			if(arr == null || arr.Length == 0) {
				throw new NotImplementedException("The format of downloaded data is not supported anymore.");
			}
			string haplName = arr[0].Split('\t')[2].Trim();
			haplName = ProcessSpacesInHapl(haplName);
			return await NameToHaplogroup(haplName).ConfigureAwait(false);
		}

		private async Task<Haplogroup> NameToHaplogroup(string name) {
			var haps = await _fetcher.GetHaplogroupsAsync().ConfigureAwait(false);
			var hap = haps.FirstOrDefault(hap => hap?.Name == name, null);
			if(hap != null) {
				return hap;
			}
			await _haploSaver.AddHaplogroupAsync(name).ConfigureAwait(false);
			var updatedHaplogroups = await _fetcher.GetHaplogroupsAsync().ConfigureAwait(false);
			return updatedHaplogroups.First(haplogroup => haplogroup.Name == name);
		}

		private string ProcessSpacesInHapl(string hapl) {
			hapl = hapl.Trim();
			if(! hapl.Contains(' ')) {
				return hapl;
			}
			var parts = hapl.Split(' ');
			var res = parts.Length - 1;
			if(parts[res].EndsWith(')')) { //last part is a note
				while(res > 0 && !parts[res].StartsWith('(')) {
					res--;
				}
				res--;
				if(res < 0) {
					throw new NotImplementedException("Haplogroup prediction failed - new unexpected format found from nevgen.");
				}
			}
			return $"{hapl[0]}-{parts[res]}";
		}

		private async Task<IEnumerable<string>> GetMarkersOrder(HttpClient client) {
			string site = "https://nevgen.org";
			var mes = await client.GetStringAsync(site).ConfigureAwait(false);
			if(mes == null) {
				throw new HttpRequestException("could not reach " + site + " functionalities");
			}
			string fieldName = "dajFtdnaPoredak"; //name of their variable for order of markers
			int varIndex = mes.IndexOf(fieldName); 
			if(varIndex < 0) {
				throw new FieldAccessException(site + " no longer has internal variable called " + fieldName);
			}
			string res = mes.Substring(varIndex); 
			res = res.Substring(0, res.IndexOf(';') - 1);
			res = res.Substring(res.IndexOf('[') + 1);
			return res.Split(',').Select(m => m.Substring(1, m.Length - 2)); // removes quotes
		}

		private async Task<string> BuildMarkers(HttpClient client, StrMarkerCollection<string, int> data) {
			var marks = _markersOrder ?? await GetMarkersOrder(client).ConfigureAwait(false);
			return string.Join(
				separator: ",",
				values: marks.Select(mark => data.FindMarker(m => MarkerComparer.AreSame(a: mark, b: m))?.Value.ToString())
			);
		}

		public async Task InitializeComputations() {
			_client = new HttpClient();
			_markersOrder = await GetMarkersOrder(_client).ConfigureAwait(false);
		}
	}
}
