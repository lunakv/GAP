using Users.GenData.Conflict.Storage;
using Users.GenData.Haplogroup.Storage;
using Users.GenData.OriginalStrDataSources.Storage;
using Users.UserAccess.Storage;

namespace Users;

public interface ISaverFactory : IAsyncDisposable {
	IUserSaver GetUserSaver();
	IStrConflictSaver GetStrConflictSaver();
	IHaplogroupSaver GetHaplogroupSaver();
	IOriginalStrDataSourceSaver GetOriginalStrDataSourceSaver();
	Task StartTransactionAsync();
	Task CommitAsync();
}