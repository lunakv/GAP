﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace IdManagement; 
public class IdGenerator {
	private int MaxGenerated => 10_000_000;
	//when done like this all of the generated ids have the same number of digits in them
	private int MinGenerated => MaxGenerated / 10; 

	private object _lock = new object();
	private Dictionary<Type, GeneratorData> _generators = new Dictionary<Type, GeneratorData>();
	private readonly string _savingDirectory;

	public IdGenerator(IOptions<IdGenerationConfig> options) {
		_savingDirectory = options.Value.SavingDirectory;
		if(!Directory.Exists(_savingDirectory)) {
			Directory.CreateDirectory(_savingDirectory);
		}
	}

	/// <summary>
	/// Resets the progress of all id generators. Adviced to use only when database is dropped.
	/// </summary>
	public void ResetAll() {
		foreach(var f in Directory.EnumerateFiles(_savingDirectory)) {
			if(f.EndsWith(GeneratorData.Extension))
				File.Delete(f);
		}
	}

	/// <summary>
	/// Returns a unique identifier. 
	/// It is only unique for all calls with the same <paramref name="forWho"/> parameter.
	/// </summary>
	/// <param name="forWho"></param>
	public int GetNextRaw(Type forWho) {
		GeneratorData generator;
			lock(_lock) {
			if (!_generators.ContainsKey(forWho)) {
				_generators.Add(
					forWho, 
					new GeneratorData(forWho, MinGenerated, _savingDirectory));
				_generators[forWho].Recreate();
			}
			generator = _generators[forWho];
		}
		int next = generator.GetNext();
		if(next > MaxGenerated) {
			throw new NotImplementedException("The ids count has reached it's limit.");
		}
		return next;
	}

	/// <summary>
	/// Generates unique id using the <c>GetNextRaw</c> method.
	/// It is then tweaked to have minimum of 2 digits differnt from all other identifiors.
	/// That makes it better suitable for human use.
	/// </summary>
	/// <param name="forWho"></param>
	/// <returns></returns>
	public int FoolproofNext(Type forWho) {
		var input = GetNextRaw(forWho);
		int sum = 0;
		int n = input;
		while(n > 0) {
			sum += n % 10;      
			n = n / 10; 
		}
		return input * 100 + sum;
	}
}
