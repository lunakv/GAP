﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdManagement; 

public class IdGenerationConfig {
	public string SavingDirectory { get; set; }

	public IdGenerationConfig() {
		SavingDirectory = string.Empty;
	}
}