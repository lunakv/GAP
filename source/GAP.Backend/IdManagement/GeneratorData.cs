﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdManagement; 

/// <summary>
/// A class used to generate unique ids for a given C# type. 
/// It is threadsafe and saves the generated ids in case of the server crashing.
/// Those two properties together make all of its methods very slow,
/// because they need to access files synchronously.
/// </summary>
internal class GeneratorData {
	public const string Extension = ".gds";
	private readonly Type _forWho;
	private readonly int _range;
	private readonly int _usersPerRange;
	private readonly int _min;
	private readonly object _lock = new object();
	private readonly object _fileLock = new object();
	private readonly HashSet<int> _used = new HashSet<int>();
	private readonly string _saveDirectory;
	private Random _random;
	private int _advencement = 0;
	private int _usedFileIndex = 0;
	private StreamWriter? _currFile = null;

	private int CurrMin => _min + _advencement * _range;
	private int CurrMax => _min + (_advencement + 1) * _range;

	public GeneratorData(Type forWho, int min, string saveDirectory, int range = 10_000, int usersPerRange = 750) {
		_min = min;
		_forWho = forWho;
		_random = new Random(forWho.GetHashCode());
		_range = range;
		_usersPerRange = usersPerRange;
		_saveDirectory = saveDirectory;
	}

	/// <summary>
	/// Retrieves next unique identifier and save it.
	/// </summary>
	public int GetNext() {
		int next;
		lock(_lock) {
			if(_used.Count >= _usersPerRange) { //to many users in this range - lets advance to the next one
				AdvanceRange();
			}
			int i = 0;
			do { //let's find unused id
				next = _random.Next(CurrMin, CurrMax);
				i++;
				if(i > _usersPerRange * 2) { //too many ids searched, lets change the random seed
					_random = new Random(next);
				}
			} while(_used.Contains(next));
			lock(_fileLock){
				if(_currFile == null) {
					throw new InvalidOperationException($"{nameof(GeneratorData)}<{_forWho.Name}>: Called {nameof(GetNext)} before calling {nameof(Recreate)}.");
				}
				_currFile.WriteLine(next.ToString()); //number generated, lets add it into the used file
				_currFile.Flush();
				_used.Add(next); // put it into the memory of used numbers
			}
		}
		return next;
	}

	/// <summary>
	/// Repopulates the memory of used identifiers with the saved ones.
	/// </summary>
	public void Recreate() {

		int maxAdvancement = -1;
		for(int i = 0; i < 2; i++) { //let's go through both files and ind the one with advancement level in it
			string path = GetSavePath(i);
			int? advancement  = GetAdvancementFromFile(path);
			if(advancement.HasValue && advancement > maxAdvancement) {
				_usedFileIndex = i;
				maxAdvancement = advancement.Value;
			}else if(File.Exists(path)) { // if the file has lower advancement we can delete it
				File.Delete(path);
			}
		}
		if(maxAdvancement >= 0) //a file was found with records in it - let's read them
		{
			_advencement = maxAdvancement;
			var lines = File.ReadAllLines(GetSavePath()).Skip(1); //first line contains the advancement level
			foreach(var line in lines) {
				_used.Add(int.Parse(line));
			}
			ActualizeFile();
		} else { //no file found, let's create one
			_advencement = -1; //advance range will do _advancement++ -> so it has to be -1 beforehand to set it for file creation
			AdvanceRange();
		}
	}

	private string GetSavePath(int number)
		=> Path.Combine(_saveDirectory, $"{(_forWho.FullName ?? _forWho.Name)}{number}{Extension}");

	private string GetSavePath()
		=> GetSavePath(_usedFileIndex);
	
	/// <returns>The advancement level found on the specified path and value also indicating if the file exists.</returns>
	private int? GetAdvancementFromFile(string path) {
		if(File.Exists(path)) {
			StreamReader? fs = null;
			try{ 
				fs = new StreamReader(File.OpenRead(path));
				var line1 = fs.ReadLine();
				if(line1 != null) {
					return int.Parse(line1);
				}
			} finally {
				fs?.Close();
			}
		}
		return null;
	}

	private void AdvanceRange() {
		_advencement++;
		_used.Clear();
		var pre = GetSavePath(_usedFileIndex);
		_usedFileIndex = -_usedFileIndex + 1; //does 0 -> 1 and 1 -> 0
		lock(_fileLock) {
			ActualizeFile();
			_currFile!.WriteLine(_advencement);
			_currFile!.Flush();
			File.Delete(pre); //only delete the previous file when the current is safely closed
		}
	}

	private void ActualizeFile() {
		_currFile?.Close();
		_currFile = new StreamWriter(File.Open(GetSavePath(), FileMode.Append, FileAccess.Write, FileShare.None));
	}
}
