﻿namespace DataLoader; 

/// <summary>
/// A class substituing for haplogroups. 
/// Since haplogroup ids are created by the database it is better to
/// store the data without it.
/// </summary>
public record HaploSubstitute(string Name);
