﻿namespace DataLoader; 

/// <summary>
/// Interface ensuring the ability to supply data of type <typeparamref name="T"/>.
/// </summary>
public interface IDataSource<T> {
    IList<T> GetData();
}