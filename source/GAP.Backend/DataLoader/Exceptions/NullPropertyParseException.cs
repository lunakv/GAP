﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLoader.Exceptions;

public class NullPropertyParseException : Exception {
    public NullPropertyParseException(string msg) : base(msg) { }
}
