﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLoader.Exceptions;

public class InvalidParentRegionLayerParseException : Exception {
    public InvalidParentRegionLayerParseException(string msg) : base(msg) { }
}
