namespace DataLoader.Exceptions;

public class ServiceNotFoundException : Exception {
	public ServiceNotFoundException(string message) : base(message) { }
}