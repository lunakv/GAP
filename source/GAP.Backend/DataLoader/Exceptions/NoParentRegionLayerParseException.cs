﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLoader.Exceptions;

public class NoParentRegionLayerParseException : Exception {
    public NoParentRegionLayerParseException(string msg) : base(msg) {}
}
