﻿using DataLoader;
using PostgreSqlStorage.Data;
using CommandLine;
using CommandLine.Text;
using DataLoader.CmdArgsParsers;
using Map.ValueObjects;
using Users.UserAccess;
using AuthStorageGateway;
using Auth.Policies;
using Auth.Registration;
using Auth.Registration.Objects;
using Auth.Registration.RegistrationDataSources;
using DataLoader.Exceptions;
using DataLoader.MockDataLoaders;
using DataLoader.UserRegistration;
using Laboratory.KitManagement.Kits;
using Laboratory.KitManagement.Storage;
using Map.RegionDataSaver;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using PhyloTree.DataClasses;
using PhyloTree;
using PhyloTree.Access;
using Users.GenData;
using Users.GenData.Haplogroup;
using Users.GenData.Haplogroup.Storage;
using Users.GenData.Parsers;
using Users.UserAccess.Entities.Interfaces;
using Users.UserAccess.PublicIds;
using Users.UserAccess.Storage;
using LogsContext = PostgreSqlStorage.LoggingGateway.LogsContext;
using Users.UserAccess.Entities;

var parser = new Parser();

var parserResult = parser.ParseArguments<Options>(args);

var helpText = HelpText.AutoBuild(parserResult, h => { return HelpText.DefaultParsingErrorsHandler(parserResult, h); }, e => e);

try {
	parserResult.WithParsed(o => {
		var app = ServiceConfig.BuildApplication(args);
		Console.WriteLine("Command line options parsed.");
		DataSourceParser dataSourceParser = new DataSourceParser();
		dataSourceParser.Parse(o);
		ModifiableDataSource<HaploSubstitute> haplogroupSource = dataSourceParser.HaplogroupSource;
		IDataSource<AdministrativeRegionLayer> regionSource = dataSourceParser.RegionSource;
		IUserDataSource userSource = dataSourceParser.UserSource;
		IPhyloTreeFetcher<PhyloVariantedNode> fullTreeSource = dataSourceParser.FullTreeSource;
		Console.WriteLine("Data sources created.");
		
		InitializeDatabases(app: app);
		// Real old users already have their real haplogroups.
		// so we need to fill the `haplogroupSource` with those haplogroups.
		userSource.AddResources(haplogroupSource);

		var haplogroupSaver = (IHaplogroupSaver)(
			app.Services.GetService(typeof(IHaplogroupSaver)) ?? throw new ServiceNotFoundException("No IHaplogroupSaver in DI.")
		);
		haplogroupSaver.AddHaplogroupsAsync(haplogroupSource.GetData().Select(h => h.Name).ToList()).Wait();
		Console.WriteLine("Haplogroups added to database.");

		var administrativeRegionLayerSaver = (IAdministrativeRegionLayerSaver)(
			app.Services.GetService(typeof(IAdministrativeRegionLayerSaver))
			?? throw new ServiceNotFoundException("No IAdministrativeRegionLayerSaver in DI.")
		);
		administrativeRegionLayerSaver.AddLayers(regionSource.GetData());
		Console.WriteLine("Regions added to database.");
		
		RegisterAdmin(
			app: app,
			email: o.RootEmail,
			role: nameof(RootRole),
			password: o.RootPassword
		);
		Console.WriteLine($"Root {o.RootEmail} added to database.");
		
		//users need haplogroups to be created
		var haploFetcher = (IHaplogroupFetcher)(
			app.Services.GetService(typeof(IHaplogroupFetcher)) ?? throw new ServiceNotFoundException("No HaplogroupFetcher in DI.")
		);
		var hs = haploFetcher.GetHaplogroupsAsync().Result;

		var users = userSource.GetData(hs);
		RegisterUsers(app: app, users: users, password: o.UserPassword);

		if (o.CreatePhyloTree) {
			new PhyloTreeOriginalJsonProcessor(o.PhyloTreeOutput).SaveTree(fullTreeSource.GetTreeAsync().Result);
			Console.WriteLine($"Phylo tree saved to {o.PhyloTreeOutput}.");
		}

		if (o.MockAdmins) {
			RegisterMockAdmins(app: app, password: o.MockAdminPassword);
		}
		
		var userFetcher = (IUserFetcher)(
			app.Services.GetService(typeof(IUserFetcher)) ?? throw new ServiceNotFoundException("No IUserFetcher in DI.")
		);
		var us = userFetcher.GetUsersAsync().Result;
		int startingId = us.Min(u => u.Id);

		if (o.MultipleDataSources) {
			AddMockStrDataFromMultipleSources(app: app, startOnId: startingId, userCount: users.Count);
		}

		if (o.MockKits) {
			AddMockKits(app: app, mockKitsPerStage: o.MockKitsPerStage, startOnId: startingId, userCount: users.Count);
		}
	});
} catch (ArgumentException ex) {
	helpText.AddPreOptionsText("ERROR(S):");
	helpText.AddPreOptionsText($"  {ex.Message}");
	Console.Write(helpText);
}

parserResult.WithNotParsed((e) => { Console.WriteLine(helpText); });

void InitializeDatabases(WebApplication app) {
	GapContext context = (GapContext)(
		app.Services.GetService(typeof(GapContext))
		?? throw new ServiceNotFoundException("No GapContext in DI.")
	);
	context.Database.EnsureDeleted();
	context.Database.EnsureCreated();
	Console.WriteLine("Business logic database deleted and created.");

	AuthContext authContext = (AuthContext)(
		app.Services.GetService(typeof(AuthContext))
		?? throw new ServiceNotFoundException("No AuthContext in DI.")
	);
	authContext.Database.EnsureDeleted();
	authContext.Database.EnsureCreated();
	Console.WriteLine("Auth database deleted and created.");

	LogsContext logsContext = (LogsContext)(
		app.Services.GetService(typeof(LogsContext))
		?? throw new ServiceNotFoundException("No LogsContext in DI.")
	);
	logsContext.Database.EnsureDeleted();
	logsContext.Database.EnsureCreated();
	Console.WriteLine("Logs database deleted and created.");
}

void RegisterUsers(WebApplication app, IList<CompleteRegistrationUserInput> users, string? password) {
	int usersAdded = 1;
	var registrar = (Registrar)(
		app.Services.GetService(typeof(Registrar)) ?? throw new ServiceNotFoundException("No Registrar in DI.")
	);
	var municipalityMapper = (IMunicipalityMapper)(
		app.Services.GetService(typeof(IMunicipalityMapper)) ?? throw new ServiceNotFoundException("No IMunicipalityMapper in DI.")
	);
	
	var publicUserIdGenerator = (IPublicUserIdGenerator)(
		app.Services.GetService(typeof(IPublicUserIdGenerator)) ?? throw new ServiceNotFoundException("No IPublicUserIdGenerator in DI.")
	);
	foreach (var user in users) {
		if (user.UserInput.Profile.ResidenceAddress.Municipality == null) {
			throw new NullPropertyParseException("Attempting to add user with null municipality!");
		}
		var registrationResult = registrar.RegisterAsync(
			registrationUser: new RegistrationUser(
				administrationAgreementSigned: user.UserInput.AdministrationAgreementSigned,
				publicId: publicUserIdGenerator.GetNextId(),
				profile: user.UserInput.Profile,
				ancestor: user.UserInput.Ancestor,
				regionId: municipalityMapper.GetMunicipalityRegionIdAsync(user.UserInput.Profile.ResidenceAddress.Municipality).Result,
				password: password
			),
			registrationDataSources: new List<IRegistrationDataSource>() {
				new OldGeneticDataRegistrationSource(strMarkers: user.GeneticData.StrMarkers, haplogroup: user.GeneticData.Haplogroup)
			},
			sendConfirmationEmail: false
		).Result;
		registrar.ConfirmEmailAsync(userId: registrationResult.UserId, token: registrationResult.EmailConfirmationToken).Wait();
		if (usersAdded % 50 == 0) {
			Console.WriteLine($"{usersAdded} users loaded.");
		}

		++usersAdded;
	}

	Console.WriteLine("Users added to database.");
}

void RegisterMockAdmins(WebApplication app, string password) {
	RegisterAdmin(
		app: app,
		email: "admin@dnaportal.cz",
		role: nameof(AdminRole),
		password: password
	);

	RegisterAdmin(
		app: app,
		email: "labTech@dnaportal.cz",
		role: nameof(LabTechRole),
		password: password
	);

	Console.WriteLine("Mock admins added to database.");
}

void RegisterAdmin(WebApplication app, string email, string role, string password) {
	var registrar = (Registrar)(
		app.Services.GetService(typeof(Registrar))
		?? throw new ServiceNotFoundException("No Registrar in DI.")
	);
	var registrationResult = registrar.CreateAccount(
		email: email,
		role: role,
		password: password,
		sendConfirmationEmail: false
	).Result;
	registrar.ConfirmEmailAsync(userId: registrationResult.UserId, token: registrationResult.EmailConfirmationToken).Wait();
}

void AddMockStrDataFromMultipleSources(WebApplication app, int startOnId, int userCount) {
	//user id's start on 1, but root is first
	for (int j = startOnId; j < startOnId + Math.Min(50, userCount); ++j) {
		var userFetcher = (IUserFetcher)(
			app.Services.GetService(typeof(IUserFetcher)) ?? throw new ServiceNotFoundException("No IUserFetcher in DI.")
		);
		var user = userFetcher.FindUserByIdAsync(j).Result;

		var mockOriginalDataSource = new MockOriginalDataSource(
			user: user,
			numberOfSources: 3,
			seed: j
		);
		foreach (var originalData in mockOriginalDataSource.GetData()) {
			var strDataUploader = (GeneticDataUploadController)(
				app.Services.GetService(typeof(GeneticDataUploadController)) ?? throw new ServiceNotFoundException("No GeneticDataUploadController in DI.")
			);
			strDataUploader.UploadExternalStrFile(
				userId: user.Id,
				file: originalData.File,
				externalStrFileParser: new UnsupportedProviderParser(
					geneticTestProvider: new GeneticTestProvider("MockTP"),
					fileExtension: ".txt",
					strMarkers: originalData.StrMarkerCollection
				)).Wait();
		}
	}

	Console.WriteLine("Mock original data sources added to database.");
}

void AddMockKits(WebApplication app, int mockKitsPerStage, int startOnId, int userCount) {
	var kitSaver = (IKitSaver<IHasUserId>)(
		app.Services.GetService(typeof(IKitSaver<IHasUserId>)) ?? throw new ServiceNotFoundException("No IKitSaver in DI.")
	);
	IReadOnlyCollection<Kit<IHasUserId>> kits = new MockKitSource(mockKitsPerStage, startOnId, userCount).GetData().ToList();
	kitSaver.AddKitsAsync(kits).Wait();
	Console.WriteLine("Mock kits for all stages added to database.");
}