﻿namespace DataLoader;

/// <summary>
/// Id incrementor that only remembers a value and increments it by a given amount every iteration.
/// </summary>
public class SimpleIdIncrementor : IIdIncrementor {
	private int _value;
	private readonly int _increase;
	public SimpleIdIncrementor(int value = 0, int increase = 1) {
		_value = value-increase;
		_increase = increase;
	}
	public int GetNextId() {
		_value += _increase;
		return _value;
	}
}
