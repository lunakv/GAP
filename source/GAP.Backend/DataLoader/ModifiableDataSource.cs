﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLoader {
	public class ModifiableDataSource<T> : IDataSource<T>{
		private IDataSource<T> _source;
		private List<T> _adding = new List<T>();
		public ModifiableDataSource(IDataSource<T> source) { 
			_source = source;
		}

		public IList<T> GetData() {
			return _source.GetData().Concat(_adding).ToList();
		}

		public void Add(T item) {
			_adding.Add(item);
		}
	}
}
