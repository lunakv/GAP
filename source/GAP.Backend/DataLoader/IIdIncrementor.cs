﻿namespace DataLoader; 
public interface IIdIncrementor {
    int GetNextId();
}
