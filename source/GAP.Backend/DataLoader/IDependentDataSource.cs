﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLoader; 

/// <summary>
/// Interface ensuring the ability to supply data of type <typeparamref name="T"/>
/// if data of type <typeparamref name="TDepend"/> are supplied.
/// </summary>
public interface IDependentDataSource<T, TDepend> {
    IList<T> GetData(IList<TDepend> args);
}