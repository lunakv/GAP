﻿using PhyloTree.DataClasses;
using PhyloTree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace DataLoader.RealDataLoaders; 
internal class RealPhyloTreeSource : IPhyloTreeFetcher<PhyloVariantedNode> {
	private readonly FullTreeSaver _treeSaver;

	public RealPhyloTreeSource(IPhyloTreeDownloader treeDownloader, IPhyloTreeWebParser treeWebParser, string treeWebUrl) {
		_treeSaver = new FullTreeSaver(
			new TreeOptions(new PhyloTreeConfig() {
				FullTreeWebPage = treeWebUrl
			}),
			treeDownloader,
			treeWebParser,
			new FakeTreeSaver()
		);
	}
	public Task<PhylogeneticTree<PhyloVariantedNode>> GetTreeAsync() {
		Console.WriteLine("Downloading full phylo tree...");
		return _treeSaver.GetFullTree();
	}

	private record TreeOptions(PhyloTreeConfig Value) : IOptions<PhyloTreeConfig>;

	private class FakeTreeSaver : IPhyloTreeSaver<PhyloVariantedNode> {
		public void SaveTree(PhylogeneticTree<PhyloVariantedNode> tree) {
			throw new NotImplementedException();
		}
	}
}
