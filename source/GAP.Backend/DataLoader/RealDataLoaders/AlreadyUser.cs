﻿using System.Globalization;
using System.Text.Json;
using Users.UserAccess.Entities;

namespace DataLoader.RealDataLoaders; 

/// <summary>
/// When some old user data needs to be added to the database
/// on its creation, it should be supplied in this form.
/// </summary>
public record AlreadyUser(
	AlreadyProfile Profile, 
	AlreadyAncestor Ancestor, 
	AlreadyGeneticData GeneticData
);

public record AlreadyGeneticData(
	Dictionary<string, int> StrMarkers, 
	string HaplogroupName
);

public record AlreadyProfile(string GivenName, string FamilyName, string Email, string? PhoneNumber, AlreadyExpandedAddress ResidenceAddress,
	Address? CorrespondenceAddress, DateOnly? BirthDate) {
	public Profile ToProfile() {
		return new Profile(
			givenName: GivenName,
			familyName: FamilyName,
			email: Email,
			phoneNumber: PhoneNumber,
			residenceAddress: ResidenceAddress.ToExpandedAddress(),
			correspondenceAddress: CorrespondenceAddress, 
			birthDate: BirthDate
		);
	}
}

public record AlreadyExpandedAddress(string? Town, string? Municipality, string? County, string? Street, string? HouseNumber, string? ZipCode) {
	public AlreadyExpandedAddress() 
		:this(null, null, null, null, null, null){ }
	public ExpandedAddress ToExpandedAddress() {
		return new ExpandedAddress(
			town: Town,
			municipality: Municipality,
			county: County,
			street: Street + " " + HouseNumber,
			zipCode: ZipCode
		);
	}
}

/// <summary>
/// Same as <see cref="Users.UserAccess.Entities.Ancestor"/> but with json serialization support.
/// </summary>
public class AlreadyAncestor {
	public ExpandedAddress Address { get; }
	public string? GivenName { get; }
	public string? FamilyName { get; }
	[System.Text.Json.Serialization.JsonConverter(typeof(DateOnlyJsonConverter))]
	public DateOnly? BirthDate { get; }
	public string? PlaceOfBirth { get; }
	[System.Text.Json.Serialization.JsonConverter(typeof(DateOnlyJsonConverter))]
	public DateOnly? DeathDate { get; }
	public string? PlaceOfDeath { get; }


	public AlreadyAncestor(ExpandedAddress address, string? givenName = null, string? familyName = null, DateOnly? birthDate = null, string? placeOfBirth = null, DateOnly? deathDate = null,
		string? placeOfDeath = null) {
		Address = address;
		GivenName = givenName;
		FamilyName = familyName;
		BirthDate = birthDate;
		PlaceOfBirth = placeOfBirth;
		DeathDate = deathDate;
		PlaceOfDeath = placeOfDeath;
	}
	
	public static AlreadyAncestor FromAncestor(Ancestor ancestor) {
		return new AlreadyAncestor(
			address: ancestor.Address,
			givenName: ancestor.GivenName,
			familyName: ancestor.FamilyName,
			birthDate: ancestor.BirthDate,
			placeOfBirth: ancestor.PlaceOfBirth,
			deathDate: ancestor.DeathDate,
			placeOfDeath: ancestor.PlaceOfDeath
		);
	}
	public Ancestor ToAncestor() {
		return new Ancestor(
			Address,
			GivenName,
			FamilyName,
			BirthDate,
			PlaceOfBirth,
			DeathDate,
			PlaceOfDeath);
	}
}

/// <summary>
/// This class provides json serialization methods for <see cref="DateOnly"/> class
/// which is not support by default.
/// </summary>
public class DateOnlyJsonConverter : System.Text.Json.Serialization.JsonConverter<DateOnly>
{
    private const string Format = "yyyy-MM-dd";

    public override DateOnly Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        return DateOnly.ParseExact(reader.GetString(), Format, CultureInfo.InvariantCulture);
    }

    public override void Write(Utf8JsonWriter writer, DateOnly value, JsonSerializerOptions options)
    {
        writer.WriteStringValue(value.ToString(Format, CultureInfo.InvariantCulture));
    }
}