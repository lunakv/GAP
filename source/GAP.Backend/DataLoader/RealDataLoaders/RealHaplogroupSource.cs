using Users.GenData.Haplogroup;

namespace DataLoader.RealDataLoaders; 

public class RealHaplogroupSource : IDataSource<HaploSubstitute>  {
	public IList<HaploSubstitute> GetData() {
		return new List<HaploSubstitute>();
	}
}