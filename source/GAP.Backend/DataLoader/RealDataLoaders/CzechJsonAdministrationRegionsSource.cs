﻿using System.Text.Json;
using DataLoader.Exceptions;
using Map.ValueObjects;

namespace DataLoader.RealDataLoaders;

public class CzechJsonAdministrationRegionsSource : IDataSource<AdministrativeRegionLayer> {

    private readonly string _jsonFile;

    private const string _regionLayerObjectsProperty = "objects";
    private const string _orpLayerProperty = "ORP_P";
    private const string _okresyLayerProperty = "OKRESY_P";
    private const string _vuscLayerProperty = "VUSC_P";
    private const string _regionListProperty = "geometries";
    private const string _regionIdProperty = "id";
    private const string _regionPropertiesProperty = "properties";
    private const string _parentRegionIdProperty = "parent_id";
    private const string _regionNameProperty = "name";
    
    public CzechJsonAdministrationRegionsSource(string jsonFile) {
        _jsonFile = jsonFile;
    }

    public IList<AdministrativeRegionLayer> GetData() {
        using StreamReader sr = new StreamReader(_jsonFile);
        JsonDocument document = JsonDocument.Parse(sr.ReadToEnd());
        JsonElement layers = document.RootElement.GetProperty(_regionLayerObjectsProperty);

        return new List<AdministrativeRegionLayer>() {
            ParseLayer(layers.GetProperty(_orpLayerProperty), layerId: 0, regionType: AdministrativeRegion.RegionType.Municipality, shouldHaveParentLayer: true),
            ParseLayer(layers.GetProperty(_okresyLayerProperty), layerId: 1, regionType: AdministrativeRegion.RegionType.District, shouldHaveParentLayer: true),
            ParseLayer(layers.GetProperty(_vuscLayerProperty), layerId: 2, regionType: AdministrativeRegion.RegionType.County, shouldHaveParentLayer: false)
        };
    }

    private AdministrativeRegionLayer ParseLayer(JsonElement layerElement, int layerId, AdministrativeRegion.RegionType regionType,  bool shouldHaveParentLayer) {
        IList<AdministrativeRegion> regions = layerElement.GetProperty(_regionListProperty).EnumerateArray().Select(region => {
            int regionId = region.GetProperty(_regionIdProperty).GetInt32();
            
            JsonElement regionProperties = region.GetProperty(_regionPropertiesProperty);

            string regionName = regionProperties.GetProperty(_regionNameProperty).GetString()
                    ?? throw new NullPropertyParseException($"Name of Region {regionId} is null.");

            bool parentIdPresent = regionProperties.TryGetProperty(_parentRegionIdProperty, out JsonElement parentIdElement);
            if (shouldHaveParentLayer != parentIdPresent) {
                if (shouldHaveParentLayer) {
                    throw new NoParentRegionLayerParseException($"Region {regionId} does not have parent region even though it should.");
                } else {
                    throw new InvalidParentRegionLayerParseException($"Region {regionId} should not have a parent region.");
                }
            }

            return new AdministrativeRegion(
                id: regionId,
                type: regionType,
                name: regionName,
                parentRegionId: parentIdPresent ? parentIdElement.GetInt32() : null
            );
        }).ToList();

        return new AdministrativeRegionLayer(
            id: layerId,
            regions: regions
        );
    }
}
