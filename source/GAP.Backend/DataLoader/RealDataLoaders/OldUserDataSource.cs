﻿using System.Text.Json;
using Auth.Registration.Objects;
using DataLoader.UserRegistration;
using Users.GenData;
using Users.GenData.Haplogroup;

namespace DataLoader.RealDataLoaders; 

/// <summary>
/// Provides a <c>public</c> way to save users into a file.
/// Also provides an <c>internal</c> way to read that file 
/// and parse haplogroups as well as users out of it.
/// </summary>
public class OldUserDataSource : IUserDataSource {
	private string _filePath;
	private bool _administrationAgreementSigned;
	private readonly int _maxUserCount;

	/// <param name="administrationAgreementSigned">
	/// Whether the resulting users should be considered as if they signed the agreement.
	/// </param>
	internal OldUserDataSource(string filePath,
		bool administrationAgreementSigned,
		int maxUserCount) {
		_filePath = filePath;
		_administrationAgreementSigned = administrationAgreementSigned;
		_maxUserCount = maxUserCount;
	}

	/// <summary>
	/// Given the list of existent <paramref name="haplogroups"/> creates
	/// the final users list from the file supplied to the constructor.
	/// </summary>
	public IList<CompleteRegistrationUserInput> GetData(IList<Haplogroup> haplogroups) {
		var users = GetRawUsers(_filePath, _maxUserCount);

		return users
			.Select(u => new CompleteRegistrationUserInput(
			userInput: new RegistrationUserInput(
				administrationAgreementSigned: _administrationAgreementSigned,
				profile: u.Profile.ToProfile(),
				ancestor: u.Ancestor.ToAncestor(),
				password: null,
				labTestRequested: false
			),
			geneticData: new GeneticData(
				strMarkers: u.GeneticData.StrMarkers,
				haplogroup: GetHaplogroup(u.GeneticData.HaplogroupName, haplogroups),
				originalStrDataSources: new List<int>()
			))).ToList();

	}
	
	/// <summary>
	/// Populates the <paramref name="to"/> resource with all haplogroups
	/// users have within the file supplied to the constructor.
	/// </summary>
	public void AddResources(ModifiableDataSource<HaploSubstitute> to) {
		var users = GetRawUsers(_filePath, _maxUserCount);
		foreach(var u in users) {
			var name = u.GeneticData.HaplogroupName;
			if(name == null)
				throw new ArgumentNullException("haplogroup name has to have a value.");
			var h = to.GetData().FirstOrDefault(h => h?.Name == name, null);
			if(h != null)
				continue;
			to.Add(new HaploSubstitute(name));
		}
	}

	/// <summary>
	/// This function should be used to save external <paramref name="users"/> data.
	/// They will be stored in the <paramref name="fileName"/>.
	/// </summary>
	/// <param name="users"></param>
	/// <param name="fileName"></param>
	public static void SaveUsers(IList<AlreadyUser> users, string fileName) {
		using FileStream fs = new FileStream($"{fileName}.json", FileMode.Create);
		System.Text.Json.JsonSerializer.Serialize(
			fs,
			users,
			typeof(IList<AlreadyUser>)
		);
	}

	/// <summary>
	/// When supplied with a <paramref name="fileName"/> saved by
	/// <see cref="SaveUsers(IList{AlreadyUser}, string)"/> it can retrieve
	/// all the users.
	/// </summary>
	public static IList<AlreadyUser> GetRawUsers(string fileName, int maxUserCount=0) {
		using FileStream fs = new FileStream(fileName, FileMode.Open);

		var users = (IList<AlreadyUser>?)System.Text.Json.JsonSerializer.Deserialize(
			fs, 
			typeof(IList<AlreadyUser>), 
			new JsonSerializerOptions { PropertyNameCaseInsensitive = true});
		
		if(users == null) {
			throw new FormatException($"File {fileName} has no users in it.");
		}

		return users
			.Take(maxUserCount == 0 ? int.MaxValue : maxUserCount)
			.ToList();
	}

	/// <summary>
	/// Finds the haplogroup with <paramref name="name"/> within the list of <paramref name="haplogroups"/>.
	/// </summary>
	private static Haplogroup GetHaplogroup(string? name, IList<Haplogroup> haplogroups) {
		var h = haplogroups.FirstOrDefault(h => h?.Name == name, null);
		if(h == null) {
				throw new ArgumentNullException($"Haplogroup of name {name} does not exist within the haplogroup source.");
		}
		return h;
	}
}
