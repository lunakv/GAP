using Auth.Registration;
using Auth.Registration.Objects;
using Bogus.DataSets;
using Users.GenData;

namespace DataLoader.UserRegistration; 

/// <summary>
/// A way to represent a user that is being registered with his markers
/// and potentialy also haplogroup already known.
/// </summary>
public class CompleteRegistrationUserInput {
	public RegistrationUserInput UserInput { get; }
	public GeneticData GeneticData { get; }

	public CompleteRegistrationUserInput(RegistrationUserInput userInput, GeneticData geneticData) {
		UserInput = userInput;
		GeneticData = geneticData;
	}
}