using Auth.Registration;
using Auth.Registration.Objects;
using Auth.Registration.RegistrationDataSources;
using Microsoft.AspNetCore.Routing.Constraints;
using Users.GenData;
using Users.GenData.Haplogroup;
using Users.GenData.OriginalStrDataSources;
using Users.GenData.Parsers;

namespace DataLoader.UserRegistration; 

public class OldGeneticDataRegistrationSource : IRegistrationDataSource {
	private readonly StrMarkerCollection<string, int> _strMarkers;
	private readonly Haplogroup _haplogroup;

	public OldGeneticDataRegistrationSource(StrMarkerCollection<string, int> strMarkers, Haplogroup haplogroup) {
		_strMarkers = strMarkers;
		_haplogroup = haplogroup;
	}
	
	public Task AddToAsync(RegistrationData registrationData) {
		registrationData.UploadedStrDataSource = new OriginalStrDataSource(
			userId: registrationData.UserId,
			testProvider: new GeneticTestProvider(name: "???"),
			strMarkers: _strMarkers,
			fileName: null
		);
		registrationData.Haplogroup = _haplogroup;
		return Task.CompletedTask;
	}
}