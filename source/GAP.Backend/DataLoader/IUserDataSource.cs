﻿using DataLoader.UserRegistration;

namespace DataLoader; 

/// <summary>
/// Interface ensuring the ability to create data of type <typeparamref name="T"/>
/// if data of type <typeparamref name="TDepend"/> are supplied.
/// While also providing other type of data as well adding them to a data source.
/// </summary>
public interface IUserDataSource<T, TDepend, TCreate> : IDependentDataSource<T, TDepend> {
    void AddResources(ModifiableDataSource<TCreate> to);
}

/// <summary>
/// This is the typical use of the above <see cref="IUserDataSource{T, TDepend, TCreate}"/>
/// interface and so it is here for convinience.
/// </summary>
public interface IUserDataSource 
    : IUserDataSource<
        CompleteRegistrationUserInput, 
        Users.GenData.Haplogroup.Haplogroup, 
        HaploSubstitute> { }

