using System.Text.Json;
using Bogus;
using Users.GenData;

namespace DataLoader.MockDataLoaders; 

public class StrMarkersSource : IDataSource<StrMarkerCollection<string, int>> {
	
	private const string StrMarkersJson = @"
        {
            ""markers"" : [ ""DYS576"", ""DYS389 I"", ""DYS448"", ""DYS389 II"", ""DYS19"", ""DYS391"", ""DYS481"", ""DYS549"", ""DYS533"",
                            ""DYS438"", ""DYS437"", ""DYS570"", ""DYS635"", ""DYS390"", ""DYS439"", ""DYS392"", ""DYS643"", ""DYS393"",
                            ""DYS458"", ""DYS385"", ""DYS456"", ""YGATAH4"" ]
        }
        ";

	private readonly int _markerCollectionsToGenerate;
	private readonly int _seed;
	private readonly Random _rnd;

	public StrMarkersSource(int markerCollectionsToGenerate, int seed) {
		_markerCollectionsToGenerate = markerCollectionsToGenerate;
		_seed = seed;
		_rnd = new Random(_seed);
	}
	
	public IList<StrMarkerCollection<string, int>> GetData() {
		var strMarkersGenerator = new Faker<StrMarkerCollection<string, int>>("cz")
            .UseSeed(_seed)
            .CustomInstantiator(GenerateStrMarkers);

		return strMarkersGenerator.Generate(_markerCollectionsToGenerate);
	}

	public StrMarkerCollection<string, int> GenerateStrMarkers(Faker f) {
		JsonDocument jsonMarkers = JsonDocument.Parse(StrMarkersJson);
		string[] markers = jsonMarkers
			.RootElement.GetProperty("markers").EnumerateArray()
			.Select(e => e.GetString())
			.Where(m => m != null)
			.Cast<string>()
			.ToArray();
		
		var strMarkers = markers
			.Select(marker => new { Marker = marker, Value = f.Random.Int(min: 15, max: 20) })
			.Where(mv => _rnd.Next(minValue: 0, maxValue: 100) < 95)
			.ToDictionary(m => m.Marker, m => m.Value);

		return new StrMarkerCollection<string, int>(strMarkers);
	}
}