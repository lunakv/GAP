﻿using System.Text.Json;
using Map.ValueObjects;

namespace DataLoader.MockDataLoaders;

public class MockAdministrationRegionsSource : IDataSource<AdministrativeRegionLayer> {

    private static readonly string _orpJson = @"
        {
            ""regions"": [ ""Aš"", ""Benešov"", ""Beroun"", ""Bílina"", ""Bílovec"", ""Blansko"", ""Blatná"", ""Blovice"", ""Bohumín"",
                           ""Boskovice"", ""Brandýs nad Labem-Stará Boleslav"", ""Břeclav"", ""Brno"", ""Broumov"", ""Bruntál"",
                           ""Bučovice"", ""Bystřice nad Pernštejnem"", ""Bystřice pod Hostýnem"", ""Cheb"", ""Chomutov"", ""Chotěboř"",
                           ""Chrudim"", ""Čáslav"", ""Černošice"", ""Česká Lípa"", ""Česká Třebová"", ""České Budějovice"",
                           ""Český Brod"", ""Český Krumlov"", ""Český Těšín"", ""Dačice"", ""Děčín"", ""Dobříš"", ""Dobruška"",
                           ""Domažlice"", ""Dvůr Králové nad Labem"", ""Frenštát pod Radhoštěm"", ""Frýdek-Místek"", ""Frýdlant"",
                           ""Frýdlant nad Ostravicí"", ""Havířov"", ""Havlíčkův Brod"", ""Hlavní město Praha"", ""Hlinsko"", ""Hlučín"",
                           ""Hodonín"", ""Holešov"", ""Holice"", ""Horažďovice"", ""Hořice"", ""Hořovice"", ""Horšovský Týn"",
                           ""Hradec Králové"", ""Hranice"", ""Humpolec"", ""Hustopeče"", ""Ivančice"", ""Jablonec nad Nisou"",
                           ""Jablunkov"", ""Jaroměř"", ""Jeseník"", ""Jičín"", ""Jihlava"", ""Jilemnice"", ""Jindřichův Hradec"",
                           ""Kadaň"", ""Kaplice"", ""Karlovy Vary"", ""Karviná"", ""Kladno"", ""Klatovy"", ""Kolín"", ""Konice"",
                           ""Kopřivnice"", ""Kostelec nad Orlicí"", ""Kralovice"", ""Kralupy nad Vltavou"", ""Kraslice"", ""Kravaře"",
                           ""Králíky"", ""Krnov"", ""Kroměříž"", ""Kuřim"", ""Kutná Hora"", ""Kyjov"", ""Lanškroun"", ""Liberec"",
                           ""Lipník nad Bečvou"", ""Litoměřice"", ""Litomyšl"", ""Litovel"", ""Litvínov"", ""Louny"", ""Lovosice"",
                           ""Luhačovice"", ""Lysá nad Labem"", ""Mariánské Lázně"", ""Mělník"", ""Mikulov"", ""Milevsko"",
                           ""Mladá Boleslav"", ""Mnichovo Hradiště"", ""Mohelnice"", ""Moravská Třebová"", ""Moravské Budějovice"",
                           ""Moravský Krumlov"", ""Most"", ""Náchod"", ""Náměšť nad Oslavou"", ""Nepomuk"", ""Neratovice"", ""Nová Paka"",
                           ""Nové Město na Moravě"", ""Nové Město nad Metují"", ""Nový Bor"", ""Nový Bydžov"", ""Nový Jičín"",
                           ""Nymburk"", ""Nýřany"", ""Odry"", ""Olomouc"", ""Opava"", ""Orlová"", ""Ostrava"", ""Ostrov"", ""Otrokovice"",
                           ""Pacov"", ""Pardubice"", ""Pelhřimov"", ""Písek"", ""Plzeň"", ""Podbořany"", ""Poděbrady"", ""Pohořelice"",
                           ""Polička"", ""Prachatice"", ""Přelouč"", ""Přerov"", ""Přeštice"", ""Příbram"", ""Prostějov"", ""Rakovník"",
                           ""Říčany"", ""Rokycany"", ""Rosice"", ""Roudnice nad Labem"", ""Rožnov pod Radhoštěm"", ""Rumburk"",
                           ""Rychnov nad Kněžnou"", ""Rýmařov"", ""Sedlčany"", ""Semily"", ""Slaný"", ""Slavkov u Brna"", ""Soběslav"",
                           ""Sokolov"", ""Stod"", ""Strakonice"", ""Stříbro"", ""Sušice"", ""Světlá nad Sázavou"", ""Svitavy"", ""Šlapanice"",
                           ""Šternberk"", ""Šumperk"", ""Tachov"", ""Tanvald"", ""Tábor"", ""Telč"", ""Teplice"", ""Tišnov"", ""Třebíč"",
                           ""Třeboň"", ""Trhové Sviny"", ""Třinec"", ""Trutnov"", ""Turnov"", ""Týn nad Vltavou"", ""Uherské Hradiště"",
                           ""Uherský Brod"", ""Uničov"", ""Ústí nad Labem"", ""Ústí nad Orlicí"", ""Valašské Klobouky"", ""Valašské Meziříčí"",
                           ""Varnsdorf"", ""Velké Meziříčí"", ""Veselí nad Moravou"", ""Vimperk"", ""Vítkov"", ""Vizovice"", ""Vlašim"",
                           ""Vodňany"", ""Votice"", ""Vrchlabí"", ""Vsetín"", ""Vysoké Mýto"", ""Vyškov"", ""Zábřeh"", ""Zlín"", ""Znojmo"",
                           ""Žamberk"", ""Žatec"", ""Žďár nad Sázavou"", ""Železný Brod"", ""Židlochovice"" ]
        }
        ";

    public MockAdministrationRegionsSource() {}

    public IList<AdministrativeRegionLayer> GetData() {
        JsonDocument jsonRegions = JsonDocument.Parse(_orpJson);

        string[] regionNames = jsonRegions
            .RootElement.GetProperty("regions").EnumerateArray()
            .Select(e => e.GetString())
            .Where(r => r != null)
            .Cast<string>()
            .ToArray();

        IList<AdministrativeRegion> regions = Enumerable.Range(0, regionNames.Length)
            .Select(i => new AdministrativeRegion(id: i, type: AdministrativeRegion.RegionType.Municipality, name: regionNames[i], parentRegionId: null))
            .ToList();

        return new[] { new AdministrativeRegionLayer(id: 0, regions: regions) }.ToList();
    }

}
