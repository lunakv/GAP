using Laboratory.KitManagement.Events;
using Laboratory.KitManagement.Kits;
using Users.UserAccess.Entities;
using Users.UserAccess.Entities.Interfaces;

namespace DataLoader.MockDataLoaders;

public class MockKitSource : IDataSource<Kit<IHasUserId>> {
	private readonly int _kitsPerStage;
	private readonly int _usersStartingId;
	private readonly int _totalUserCount;

	public MockKitSource(int kitsPerStage, int usersStartingId, int totalUserCount) {
		_kitsPerStage = kitsPerStage;
		_usersStartingId = usersStartingId;
		_totalUserCount = totalUserCount;
	}

	public IList<Kit<IHasUserId>> GetData() {
		var kits = new List<Kit<IHasUserId>>();
		int totalCount = 0;
		int stagesCount = Enum.GetValues<Kit<IHasUserId>.KitStage>().Length;
		for (int i = 0; i < _kitsPerStage; ++i) {
			if(totalCount + stagesCount > _totalUserCount) {
				break;
			}
			int id = _usersStartingId + stagesCount * (i + 1);
			kits.Add(new PendingKit<IHasUserId>(
				id: -1,
				createKitNote: "Mock from data loader - Create",
				user: new IdUser(id: id),
				note: "MockNote"
			));
			kits.Add(new SentKit<IHasUserId>(
				id: -1,
				history: new List<KitEvent>() {
					new CreateKitEvent(date: DateTime.Now.ToUniversalTime(), note: "Mock from data loader - Create"),
					new SendKitEvent(date: DateTime.Now.ToUniversalTime(), note: "Mock from data loader - Send")
				},
				user: new IdUser(id: id + 1),
				note: "MockNote"
			));
			kits.Add(new ReceivedKit<IHasUserId>(
				id: -1,
				history: new List<KitEvent>() {
					new CreateKitEvent(date: DateTime.Now.ToUniversalTime(), note: "Mock from data loader - Create"),
					new SendKitEvent(date: DateTime.Now.ToUniversalTime(), note: "Mock from data loader - Send"),
					new ReceiveKitEvent(date: DateTime.Now.ToUniversalTime(), note: "Mock from data loader - Receive")
				},
				user: new IdUser(id: id + 2),
				note: "MockNote"
			));
			kits.Add(new AnalysedKit<IHasUserId>(
				id: -1,
				history: new List<KitEvent>() {
					new CreateKitEvent(date: DateTime.Now.ToUniversalTime(), note: "Mock from data loader - Create"),
					new SendKitEvent(date: DateTime.Now.ToUniversalTime(), note: "Mock from data loader - Send"),
					new ReceiveKitEvent(date: DateTime.Now.ToUniversalTime(), note: "Mock from data loader - Receive"),
					new AnalyseKitEvent(date: DateTime.Now.ToUniversalTime(), note: "Mock from data loader - Analyse")
				},
				user: new IdUser(id: id + 3),
				note: "MockNote"
			));
		}

		return kits;
	}
}