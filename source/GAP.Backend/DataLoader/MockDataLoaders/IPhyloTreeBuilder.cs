﻿using PhyloTree.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLoader.MockDataLoaders;

/// <summary>
/// Interface ensuring the ability to create a phylogenetic tree structure when provided by node indices.
/// </summary>
internal interface IPhyloTreeBuilder {
	(IList<PhyloVariantedNode> nodes, IList<int> roots) GetNodes(int[] indices);
}