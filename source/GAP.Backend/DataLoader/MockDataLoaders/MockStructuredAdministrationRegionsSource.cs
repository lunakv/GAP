﻿using Map.ValueObjects;

namespace DataLoader.MockDataLoaders;

public class MockStructuredAdministrationRegionsSource : IDataSource<AdministrativeRegionLayer> {

    public MockStructuredAdministrationRegionsSource() { }

    public IList<AdministrativeRegionLayer> GetData() {
        const int semilyId = 9;
        const int trutnovId = 10;
        const int jicinId = 11;
        const int libereckyId = 12;
        const int kraloveHradeckyId = 13;
        const int ceskaRepublikaId = 14;

        return new List<AdministrativeRegionLayer> {
            new AdministrativeRegionLayer(
                id: 0,
                regions: new List<AdministrativeRegion> {
                    new AdministrativeRegion(id: 0, type: AdministrativeRegion.RegionType.Municipality, name: "Jilemnice", parentRegionId: semilyId),
                    new AdministrativeRegion(id: 1, type: AdministrativeRegion.RegionType.Municipality, name: "Turnov", parentRegionId: semilyId),
                    new AdministrativeRegion(id: 2, type: AdministrativeRegion.RegionType.Municipality, name: "Semily", parentRegionId: semilyId),
                    new AdministrativeRegion(id: 3, type: AdministrativeRegion.RegionType.Municipality, name: "Dvur Kralove nad Labem", parentRegionId: trutnovId),
                    new AdministrativeRegion(id: 4, type: AdministrativeRegion.RegionType.Municipality, name: "Trutnov", parentRegionId: trutnovId),
                    new AdministrativeRegion(id: 5, type: AdministrativeRegion.RegionType.Municipality, name: "Vrchlabi", parentRegionId: trutnovId),
                    new AdministrativeRegion(id: 6, type: AdministrativeRegion.RegionType.Municipality, name: "Horice", parentRegionId: jicinId),
                    new AdministrativeRegion(id: 7, type: AdministrativeRegion.RegionType.Municipality, name: "Jicin", parentRegionId: jicinId),
                    new AdministrativeRegion(id: 8, type: AdministrativeRegion.RegionType.Municipality, name: "Nova Paka", parentRegionId: jicinId)
                }
            ),
            new AdministrativeRegionLayer(
                id: 1,
                regions: new List<AdministrativeRegion> {
                    new AdministrativeRegion(id: semilyId, type: AdministrativeRegion.RegionType.District, name: "Okres Semily", parentRegionId: libereckyId),
                    new AdministrativeRegion(id: trutnovId, type: AdministrativeRegion.RegionType.District, name: "Okres Trutnov", parentRegionId: kraloveHradeckyId),
                    new AdministrativeRegion(id: jicinId, type: AdministrativeRegion.RegionType.District, name: "Okres Jicin", parentRegionId: kraloveHradeckyId)
                }
            ),
            new AdministrativeRegionLayer(
                id: 2,
                regions: new List<AdministrativeRegion> {
                    new AdministrativeRegion(id: libereckyId, type: AdministrativeRegion.RegionType.County, name: "Liberecky kraj", parentRegionId: ceskaRepublikaId),
                    new AdministrativeRegion(id: kraloveHradeckyId, type: AdministrativeRegion.RegionType.County, name: "Kralovehradecky kraj", parentRegionId: ceskaRepublikaId)
                }
            ),
            new AdministrativeRegionLayer(
                id: 3,
                regions: new List<AdministrativeRegion> {
                    new AdministrativeRegion(id: ceskaRepublikaId, type: AdministrativeRegion.RegionType.Country, name: "Ceska republika", parentRegionId: null)
                }
            )
        };
    }

}
