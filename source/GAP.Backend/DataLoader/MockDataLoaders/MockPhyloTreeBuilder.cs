﻿using PhyloTree;
using PhyloTree.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Users.GenData.Haplogroup;

namespace DataLoader.MockDataLoaders;

internal class MockPhyloTreeBuilder : IPhyloTreeBuilder {
	private readonly Func<IList<HaploSubstitute>> _creatorHaplogroups;
	private IList<HaploSubstitute>? _haplogroups = null;
	private readonly Random _ran;
	private readonly bool _quiet;
	private readonly TextWriter _output;

	public MockPhyloTreeBuilder(IList<HaploSubstitute> haplogroups,
		Random ran,
		bool quiet = true)
		: this(haplogroups, ran, System.Console.Out, quiet) { }
	
	public MockPhyloTreeBuilder(IList<HaploSubstitute> haplogroups,
		Random ran,
		TextWriter outputWriter,
		bool quiet = true) :this(
			() => haplogroups,
			ran,
			outputWriter,
			quiet){
		_haplogroups = haplogroups;
	}
	public MockPhyloTreeBuilder(Func<IList<HaploSubstitute>> haplogroupsCreator,
		Random ran,
		TextWriter outputWriter,
		bool quiet = true) {
		this._creatorHaplogroups = haplogroupsCreator;
		this._ran = ran;
		this._output = outputWriter;
		this._quiet = quiet;
	}

	/// <summary>
	/// Creates tree structure from given <paramref name="indices"/>.
	/// </summary>
	public (IList<PhyloVariantedNode> nodes, IList<int> roots) GetNodes(int[] indices) {
		List<int> roots = new List<int>();
		List<PhyloVariantedNode> Recurse(PhyloVariantedNode parent, int startIndex,
		   int siblCount, int areaSize, int indent, float secondRoot) {
			List<PhyloVariantedNode> result = new();
			int index = startIndex + siblCount;
			for(int i = 0; i < siblCount; i++) {
				var size = GetMySize(siblCount - i, startIndex + siblCount + areaSize - index);
				var children = GetChildCount(size);
				var id = indices[startIndex + i];
				if(!_quiet)
					_output.WriteLine(new string(' ', indent) + id);
				var hapl = GetHaplo(startIndex - 1 + i, indices.Length);
				bool isSecondaryRoot = _ran.NextDouble() < secondRoot;
				if(isSecondaryRoot)
					roots.Add(id);
				var node = new PhyloVariantedNode(
					name: hapl.Name,
					id: id,
					approximateYear: parent.ApproximateYear - i * 1000 + id,
					parentId: parent.Id,
					children: Spanize(indices, index, children).ToList(),
					variants: new List<string>() { hapl.Name[..hapl.Name.IndexOf('-')] }
				);
				result.AddRange(Recurse(node, index, children, size - children, indent + 1, isSecondaryRoot ? 0 : secondRoot * 1.15f));
				result.Add(node);
				index += size;
			}
			return result;
		}

		if(!_quiet) {
			_output.WriteLine("All indices in order:");
			_output.WriteLine("[" + string.Join(", ", indices) + "]");
		}

		int indent = 0;

		var ch = GetChildCount(indices.Length - 1);
		var id = indices[0];
		if(!_quiet) {
			_output.WriteLine("\nAnd the tree structure:");
			_output.WriteLine(new string(' ', indent) + id);
		}
		var root = new PhyloVariantedNode(
			id: id,
			parentId: null,
			name: $"{id}-ROOT_id",
			children: Spanize(indices, 1, ch).ToList(),
			approximateYear: 500_000,
			variants: new List<string>() { "ROOT_id" }
		);
		roots.Add(id);
		var result = Recurse(root, 1, ch, indices.Length - 1 - ch, indent + 1, 0.15f);
		result.Add(root);
		return (result, roots);
	}
	
	/// <summary>
	/// Determines how many children should a node have
	/// when the <paramref name="subtreeSize"/> is given.
	/// </summary>
	private int GetChildCount(int subtreeSize)
		=> Math.Min(_ran.Next(2, _ran.Next(4, 7)), subtreeSize);

	/// <summary>
	/// Finds out how big should be a subtree of a node with
	/// given count of <paramref name="siblings"/> and total
	/// of nodes <paramref name="available"/>.
	/// </summary>
	private int GetMySize(int siblings, int available) {
		if(siblings == 1)
			return available;
		double rand;
		while(true) {
			rand = _ran.NextDouble();
			var chance = Math.Abs(rand - (1f / siblings));
			if(_ran.NextDouble() > chance)
				break;
		}
		return (int)(available * rand);
	}

	private HaploSubstitute GetHaplo(int index, int totalCount) {
		if(_haplogroups == null) {
			_haplogroups = _creatorHaplogroups();
		}
		if(_haplogroups.Count == totalCount)
			return _haplogroups[index];
		return _haplogroups[_ran.Next(_haplogroups.Count)];
	}

	private static IEnumerable<T> Spanize<T>(T[] array, int startIndex, int count) {
		for(int i = 0; i < count; i++) {
			yield return array[startIndex + i];
		}
	}

}
