using System.Text.Json;
using Auth.Registration;
using Auth.Registration.Objects;
using Bogus;
using DataLoader.UserRegistration;
using Users.GenData;
using Users.GenData.Haplogroup;
using Users.UserAccess.Entities;

namespace DataLoader.MockDataLoaders; 

/// <summary>
/// Creates mock registration users ready to be used by the data loader.
/// </summary>
public class MockRegistrationUserSource : IUserDataSource {
    private readonly IList<string> _regionNames;
    private readonly int _usersToGenerate;

    private readonly bool _administrationAgreementSigned;
    private readonly int _seed;

    private static readonly string _strMarkersJson = @"
        {
            ""markers"" : [ ""DYS576"", ""DYS389 I"", ""DYS448"", ""DYS389 II"", ""DYS19"", ""DYS391"", ""DYS481"", ""DYS549"", ""DYS533"",
                            ""DYS438"", ""DYS437"", ""DYS570"", ""dys635"", ""DYS390"", ""DYS439"", ""DYS392"", ""DYS643"", ""DYS393"",
                            ""DYS458A"", ""DYS385"", ""dys456"", ""Y-GATAH-4"" ]
        }
        ";

    public MockRegistrationUserSource(IList<string> regionNames, int usersToGenerate,
        bool administrationAgreementSigned, int seed = 42) {
        _regionNames = regionNames;
        _usersToGenerate = usersToGenerate;
        _administrationAgreementSigned = administrationAgreementSigned;
        _seed = seed;
    }

    /// <summary>
    /// Creates users using the <see cref="Faker"/> class. 
    /// The users are populated by <paramref name="haplogroups"/>.
    /// </summary>
    public IList<CompleteRegistrationUserInput> GetData(IList<Haplogroup> haplogroups) {
        JsonDocument jsonMarkers = JsonDocument.Parse(_strMarkersJson);
        Random rnd = new Random(_seed);
        string[] markers = jsonMarkers
            .RootElement.GetProperty("markers").EnumerateArray()
            .Select(e => e.GetString())
            .Where(m => m != null)
            .Cast<string>()
            .ToArray();

        var getRandomGeneticData = (Faker f) => {
            var strMarkers = markers
                .Select(marker => new { Marker = marker, Value = f.Random.Int(min: 15, max: 20) })
                .Where(mv => rnd.Next(minValue: 0, maxValue: 100) < 95)
                .ToDictionary(m => m.Marker, m => m.Value);
            return new GeneticData(
                strMarkers: strMarkers, 
                haplogroup: f.PickRandom(haplogroups), 
                originalStrDataSources: new List<int>());
        };

        var usersGenerator = new Faker<CompleteRegistrationUserInput>("cz")
            .UseSeed(_seed)
            .CustomInstantiator(f => new CompleteRegistrationUserInput(
                userInput: new RegistrationUserInput(
                    administrationAgreementSigned: _administrationAgreementSigned ? _administrationAgreementSigned : f.Random.Bool(weight: 0.8f),
                    profile: new Profile(
                        givenName: f.Name.FirstName(),
                        familyName: f.Name.LastName(),
                        email: f.IndexVariable++ + f.Internet.Email(),
                        phoneNumber: f.Phone.PhoneNumber("#########"),
                        residenceAddress: new ExpandedAddress(
                            town: f.Address.City(),
                            municipality: f.PickRandom(_regionNames),
                            county: f.Address.County(),
                            street: f.Address.StreetName() + " " + f.Random.Int(min: 1, max: 200).ToString() ,
                            zipCode: f.Address.ZipCode()
                        ),
                        correspondenceAddress: new Address(
                            town: f.Address.City(),
                            street: f.Address.StreetName() + " " + f.Random.Int(min: 1, max: 200).ToString(),
                            zipCode: f.Address.ZipCode()
                        ),
                        birthDate: DateOnly.FromDateTime(f.Date.Past(yearsToGoBack: 40, refDate: new DateTime(2000, 12, 24)).ToUniversalTime())
                    ),
                    ancestor: new Ancestor(
                        address: new ExpandedAddress(
                            town: f.Address.City(),
                            municipality: f.PickRandom(_regionNames),
                            county: f.Address.County(),
                            street: f.Address.StreetName() + " " + f.Random.Int(min: 1, max: 200).ToString(),
                            zipCode: f.Address.ZipCode()
                        ),
                        givenName: f.Name.FirstName(),
                        familyName: f.Name.LastName(),
                        birthDate: DateOnly.FromDateTime(f.Date.Past(yearsToGoBack: 40, refDate: new DateTime(2000, 12, 24)).ToUniversalTime()),
                        placeOfBirth: f.Address.City(),
                        deathDate: DateOnly.FromDateTime(f.Date.Past(yearsToGoBack: 40, refDate: new DateTime(2000, 12, 24)).ToUniversalTime()),
                        placeOfDeath: f.Address.City()
                    ),
                    password: "TotallyRandom_42",
                    labTestRequested: false
                ),
                geneticData: getRandomGeneticData(f)
            ));
                
        return usersGenerator.Generate(_usersToGenerate);
    }

	public void AddResources(ModifiableDataSource<HaploSubstitute> to) {}
}