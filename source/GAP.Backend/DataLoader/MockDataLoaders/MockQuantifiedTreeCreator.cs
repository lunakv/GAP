﻿using PhyloTree;
using PhyloTree.DataClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.UserAccess.Entities;

namespace DataLoader.MockDataLoaders;

/// <summary>
/// When provided with a <see cref="IPhyloTreeBuilder"/> that can create tree structure
/// this class creates an actual pruned <see cref="PhylogeneticTree{PhyloQuantifiedNode}"/> out of it. 
/// </summary>
internal class MockPhyloQuantifiedTreeCreator : MockPhyloTreeCreator, IPhyloTreeFetcher<PhyloQuantifiedNode>, IDataSource<PhyloQuantifiedNode>
{
	private PhylogeneticTree<PhyloQuantifiedNode> _tree = null!;
	private IList<CompleteUser> _users;

	public IReadOnlyList<CompleteUser> Users
		=> _users.ToList();

	public MockPhyloQuantifiedTreeCreator(int nodesCount, IPhyloTreeBuilder builder, IList<User> users)
		: this(nodesCount, builder, new Random(), users) { }

	public MockPhyloQuantifiedTreeCreator(int nodesCount, IPhyloTreeBuilder builder, int seed, IList<User> users)
		: this(nodesCount, builder, new Random(seed), users) { }

	public MockPhyloQuantifiedTreeCreator(int nodesCount, IPhyloTreeBuilder builder, Random random, IList<User> users)
		: base(nodesCount, builder, random)
	{
		_users = new List<CompleteUser>(users
			.Where(u => u.GeneticData != null)
			.Select(u => new CompleteUser(
				u.Id,
				u.AdministrationAgreementSigned,
				u.PublicId,
				u.Profile,
				u.Ancestor,
				u.RegionId,
				u.GeneticData!, //never null, checked above
				u.AdministratorData)));
	}

	IList<PhyloQuantifiedNode> IDataSource<PhyloQuantifiedNode>.GetData()
	{
		return GetQuantifiedTree().Nodes.Values.ToList();
	}

	Task<PhylogeneticTree<PhyloQuantifiedNode>> IPhyloTreeFetcher<PhyloQuantifiedNode>.GetTreeAsync()
		=> Task.Run(() => GetQuantifiedTree());

	/// <summary>
	/// Creates the pruned and quantified mock tree.
	/// </summary>
	private PhylogeneticTree<PhyloQuantifiedNode> GetQuantifiedTree()
	{
		Random r = new Random();
		if (_tree != null)
			return _tree;
		var tree = base.GetTree();
		var quantifiedHaplos = _users
			.GroupBy(u => u.GeneticData.Haplogroup)
			.Select(g => (g.Key, g.Select(u => u.Id).ToList()))
			.ToDictionary(g => g.Key.Name, e => (e.Key.Id, e.Item2));
		PhylogeneticTree<PhyloQuantifiedNode>.TreeBuilder builder = new();
		int Recurse(PhyloTreeNode node, bool isHaploRoot, bool isRoot = false)
		{
			int sum = 0;
			isHaploRoot = isHaploRoot && r.NextDouble() < 0.25f;
			foreach (var ch in node.Children.Select(i => tree.Nodes[i]))
			{
				sum += Recurse(ch, isHaploRoot);
			}
			int inside = quantifiedHaplos[node.Name].Item2.Count();
			builder.Add(new PhyloQuantifiedNode(
				node,
				quantifiedHaplos[node.Name].Id, 
					//we can do that beacause we only got haplogroups that some user is in
				new NumberOfPeople(inside, sum),
				quantifiedHaplos[node.Name].Item2,
				isHaploRoot), isRoot);
			return sum + inside;
		}
		foreach (var root in tree.Roots.Select(r => tree.Nodes[r]))
		{
			Recurse(root, true);
		}
		builder.SetDate(tree.CreationTime ?? DateTime.Now);
		builder.SetTreeID(tree.TreeId ?? throw new InvalidOperationException("tree id not set in mock"));
		_tree = builder.GetTree();
		return _tree;
	}
}
