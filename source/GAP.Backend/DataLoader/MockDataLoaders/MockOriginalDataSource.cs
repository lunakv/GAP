using System.Text;
using Users.GenData;
using Users.UserAccess.Entities;

namespace DataLoader.MockDataLoaders; 

public class MockOriginalDataSource : IDataSource<(StrMarkerCollection<string, int> StrMarkerCollection, Stream File)> {

	private readonly User _user;
	private readonly int _numberOfSources;
	private readonly int _seed;
	
	public MockOriginalDataSource(User user, int numberOfSources, int seed = 42) {
		_user = user;
		_numberOfSources = numberOfSources;
		_seed = seed;
	}
	
	public IList<(StrMarkerCollection<string, int> StrMarkerCollection, Stream File)> GetData() {
		List<(StrMarkerCollection<string, int> StrMarkerCollection, Stream File)> results = new();

		var markersSource = new StrMarkersSource(markerCollectionsToGenerate: _numberOfSources, seed: _seed);
		var markersCollections = markersSource.GetData();
		
		for (int i = 0; i < _numberOfSources; ++i) {
			results.Add(
				(
					StrMarkerCollection: markersCollections[i],
					File: new MemoryStream(
						buffer: Encoding.ASCII.GetBytes(
							markersCollections[i]
								.CloneAsList()
								.Select(s => new StringBuilder(s.Name + s.Value))
								.Aggregate((s1, s2) => s1.Append(s2.ToString())).ToString()
						)
					)
				)
			);
		}

		return results;
	}
}