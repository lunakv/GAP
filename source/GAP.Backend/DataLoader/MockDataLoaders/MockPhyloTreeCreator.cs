﻿using PhyloTree;
using PhyloTree.DataClasses;

namespace DataLoader.MockDataLoaders;

/// <summary>
/// When provided with a <see cref="IPhyloTreeBuilder"/> that can create tree structure
/// this class creates an actual <see cref="PhylogeneticTree{PhyloVariantedNode}"/> out of it. 
/// </summary>
internal class MockPhyloTreeCreator :
	IDataSource<PhyloVariantedNode>,
	IPhyloTreeFetcher<PhyloVariantedNode>,
	IDataSource<PhylogeneticTree<PhyloVariantedNode>> {
	private readonly int _nodesCount;
	private readonly Random _ran = new Random();
	private readonly int[] _indices;
	private (IList<PhyloVariantedNode> nodes, IList<int> roots)? _modelNodes = null!;
	private readonly IPhyloTreeBuilder _phyloTreeBuilder;
	private PhylogeneticTree<PhyloVariantedNode> _tree = null!;

	public MockPhyloTreeCreator(int nodesCount, IPhyloTreeBuilder builder)
		: this(nodesCount, builder, 56789) { }

	public MockPhyloTreeCreator(int nodesCount, IPhyloTreeBuilder builder, int seed)
		: this(nodesCount, builder, new Random(seed)) { }

	public MockPhyloTreeCreator(int nodesCount, IPhyloTreeBuilder builder, Random random) {
		this._nodesCount = nodesCount;
		this._ran = random;
		this._phyloTreeBuilder = builder;
		_indices = Enumerable.Range(1, nodesCount + 1).ToArray();
	}

	/// <summary>
	/// Shuffles the indices in O(n).
	/// </summary>
	private void RandomizeIndices() {
		for(int i = 0; i < _nodesCount; i++) {
			int switchElem = _indices[i];
			var rand = _ran.Next(_nodesCount);
			_indices[i] = _indices[rand];
			_indices[rand] = switchElem;
		}
	}

	private (IList<PhyloVariantedNode> nodes, IList<int> roots) GetModelNodes() {
		if(_modelNodes == null) {
			RandomizeIndices();
			_modelNodes = _phyloTreeBuilder.GetNodes(_indices);
		}
		return _modelNodes.Value;
	}

	IList<PhyloVariantedNode> IDataSource<PhyloVariantedNode>.GetData()
		=> GetModelNodes().nodes;

	public Task<PhylogeneticTree<PhyloVariantedNode>> GetTreeAsync()
		=> Task.FromResult(GetTree());

	/// <summary>
	/// Creates an actual tree from the tree structure.
	/// </summary>
	public PhylogeneticTree<PhyloVariantedNode> GetTree() {
		if(_tree != null)
			return _tree;
		PhylogeneticTree<PhyloVariantedNode>.TreeBuilder builder = new();
		(var nodes, var roots) = GetModelNodes();
		foreach(var node in nodes) {
			builder.Add(node, roots.Contains(node.Id));
		}
		builder.SetDate(new DateTime(2001,1,1,1,1,1,1));
		builder.SetTreeID(-123456);
		_tree = builder.GetTree();
		return _tree;
	}

	IList<PhylogeneticTree<PhyloVariantedNode>> IDataSource<PhylogeneticTree<PhyloVariantedNode>>.GetData() {
		return new List<PhylogeneticTree<PhyloVariantedNode>>() { GetTree() };
	}
}

