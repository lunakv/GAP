﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Bogus;
using Users.GenData;
using Users.GenData.Haplogroup;
using Users.UserAccess.Entities;
using Users.UserAccess.PublicIds;

namespace DataLoader.MockDataLoaders;

public class MockUserSource : IDataSource<User> {
    
    private readonly IList<Haplogroup> _haplogroups;
    private readonly IList<int> _regionKeys;
    private readonly int _usersToGenerate;
    private readonly IIdIncrementor _idSource;
    private readonly bool _administrationAgreementSigned;
    private readonly int _seed;

    public MockUserSource(IList<Haplogroup> haplogroups, IList<int> regionKeys, int usersToGenerate, bool administrationAgreementSigned, int seed = 42) {
        _haplogroups = haplogroups;
        _regionKeys = regionKeys;
        _usersToGenerate = usersToGenerate;
        _idSource = new SimpleIdIncrementor();
        _administrationAgreementSigned = administrationAgreementSigned;
        _seed = seed;
    }

    public MockUserSource(IList<Haplogroup> haplogroups, IList<int> regionKeys, int usersToGenerate, IIdIncrementor idSource, bool administrationAgreementSigned, int seed = 42) {
        _haplogroups = haplogroups;
        _regionKeys = regionKeys;
        _usersToGenerate = usersToGenerate;
        _idSource = idSource;
        _administrationAgreementSigned = administrationAgreementSigned;
        _seed = seed;
    }

    public IList<User> GetData() {
        StrMarkersSource strMarkersSource = new StrMarkersSource(markerCollectionsToGenerate: 1, seed: _seed);

        var getRandomGeneticData = (Faker f)
            => new GeneticData(strMarkers: strMarkersSource.GenerateStrMarkers(f), haplogroup: f.PickRandom(_haplogroups), new List<int>());

        var usersGenerator = new Faker<User>("cz")
            .UseSeed(_seed)
            .CustomInstantiator(f => new User(
                id: _idSource.GetNextId(),
                administrationAgreementSigned: _administrationAgreementSigned ? _administrationAgreementSigned : f.Random.Bool(weight: 0.9f),
                publicId: new GuidPublicUserIdGenerator().GetNextId(),
                new Profile(
                    givenName: f.Name.FirstName(),
                    familyName: f.Name.LastName(),
                    email: f.Internet.Email(),
                    phoneNumber: f.Phone.PhoneNumber("#########"),
                    residenceAddress: new ExpandedAddress(
                        town: f.Address.City(),
                        municipality: f.Address.County(),
                        county: f.Address.County(),
                        street: f.Address.StreetName() + " " + f.Random.Int(min: 1, max: 200).ToString(),
                        zipCode: f.Address.ZipCode()
                    ),
                    correspondenceAddress: new Address(
                        town: f.Address.City(),
                        street: f.Address.StreetName() + " " + f.Random.Int(min: 1, max: 200).ToString(),
                        zipCode: f.Address.ZipCode()
                    ),
                    birthDate: DateOnly.FromDateTime(f.Date.Past(yearsToGoBack: 40, refDate: new DateTime(2000, 12, 24)).ToUniversalTime())
                ),
                new Ancestor(
                    address: new ExpandedAddress(
                        town: f.Address.City(),
                        municipality: f.Address.County(),
                        county: f.Address.County(),
                        street: f.Address.StreetName() + " " + f.Random.Int(min: 1, max: 200).ToString(),
                        zipCode: f.Address.ZipCode()
                    ),
                    givenName: f.Name.FirstName(),
                    familyName: f.Name.LastName(),
                    birthDate: DateOnly.FromDateTime(f.Date.Past(yearsToGoBack: 40, refDate: new DateTime(2000, 12, 24)).ToUniversalTime()),
                    placeOfBirth: f.Address.City(),
                    deathDate: DateOnly.FromDateTime(f.Date.Past(yearsToGoBack: 40, refDate: new DateTime(2000, 12, 24)).ToUniversalTime()),
                    placeOfDeath: f.Address.City()
                ),
                regionId: f.PickRandom(_regionKeys),
                geneticData: getRandomGeneticData(f),
                administratorData: new AdministratorData(
                    id: $"MOCK ADMINISTRATOR ID-{_idSource.GetNextId().ToString()}",
                    note: f.Name.FullName()
                )
            ));
                
        return usersGenerator.Generate(_usersToGenerate);
    }
}
