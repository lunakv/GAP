﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLoader.MockDataLoaders;
using DataLoader.RealDataLoaders;
using DataLoader.UserRegistration;
using Map.ValueObjects;
using PhyloTree;
using PhyloTree.DataClasses;
using Users.GenData.Haplogroup;
using Users.UserAccess.Entities;

namespace DataLoader.CmdArgsParsers;

/// <summary>
/// This class creates all data sources the way specified 
/// in the <see cref="Options"/> parameter.
/// </summary>
public class DataSourceParser {
	private ModifiableDataSource<HaploSubstitute>? _haplogroupSource;

	public ModifiableDataSource<HaploSubstitute> HaplogroupSource =>
		_haplogroupSource ?? throw new InvalidOperationException("HaplogroupSource was accessed but not parsed.");

	private IDataSource<AdministrativeRegionLayer>? _regionSource;

	public IDataSource<AdministrativeRegionLayer> RegionSource =>
		_regionSource ?? throw new InvalidOperationException("RegionSource was accessed but not parsed.");

	private IUserDataSource? _userSource;

	public IUserDataSource UserSource =>
		_userSource ?? throw new InvalidOperationException("UserSource was accessed but not parsed.");

    private IPhyloTreeFetcher<PhyloVariantedNode>? _fullTreeSource;
	public IPhyloTreeFetcher<PhyloVariantedNode> FullTreeSource =>
		_fullTreeSource ?? throw new InvalidOperationException("FullTreeSource was accessed but not parsed.");

	public DataSourceParser() {
		_haplogroupSource = null;
		_regionSource = null;
		_userSource = null;
		_fullTreeSource = null;
	}

	/// <summary>
	/// Populates all the properties of this instance by data sources
	/// created based on arguments from <paramref name="o"/>.
	/// </summary>
	public void Parse(Options o) {
		Dictionary<string, Func<Options, IDataSource<HaploSubstitute>>> haplogroupSources = new() {
			{
				Options.Mock,
				(options) => new MockHaplogroupSource(options.MockHaplogroupCount)
			}, { // Real haplogroups are added by listing real user source
				Options.Real,
				(options) => new RealHaplogroupSource()
			}
		};

		if (!haplogroupSources.ContainsKey(o.Haplogroups)) {
			throw new ArgumentException($"Value for option --haplogroups: '{o.Haplogroups}' is not in correct format.");
		}

		_haplogroupSource = new ModifiableDataSource<HaploSubstitute>(haplogroupSources[o.Haplogroups](o));

		Dictionary<string, Func<Options, IDataSource<AdministrativeRegionLayer>>> regionSources = new() {
			{
				Options.Mock,
				(options) => new MockAdministrationRegionsSource()
			}, {
				Options.CzechJson,
				(options) => {
					if (options.RegionsFilePath == null) {
						throw new ArgumentException($"Value for option --regions-file: '{options.RegionsFilePath}' is missing.");
					}

					if (options.RegionsFilePath.Length == 0) {
						throw new ArgumentException($"Value for option --regions-file: '{options.RegionsFilePath}' is missing.");
					}

					if (!File.Exists(options.RegionsFilePath)) {
						throw new ArgumentException($"Value for option --regions-file: '{options.RegionsFilePath}' is wrong - no such file.");
					}

					return new CzechJsonAdministrationRegionsSource(options.RegionsFilePath);
				}
			}
		};

		if (!regionSources.ContainsKey(o.Regions)) {
			throw new ArgumentException($"Value for option --regions: '{o.Regions}' is not in correct format.");
		}

		_regionSource = regionSources[o.Regions](o);

		if (o.MockUserCount <= 0) {
			throw new ArgumentException($"Value for option --user-count must be greater than 0.");
		}

		Dictionary<string, Func<Options, IUserDataSource>> userSources = new() {
			{
				Options.Mock,
				(options) => new MockRegistrationUserSource(
					regionNames: _regionSource.GetData()[0].Regions.Select(r => r.Name).ToList(),
					usersToGenerate: o.MockUserCount,
					administrationAgreementSigned: o.UsersAdministrationAgreementSigned
				)
			}, {
				Options.Real,
				(options) => {
					if (!File.Exists(options.RealUsersFile)) {
						throw new ArgumentException($"The file containing real users ({options.RealUsersFile}) does not exist.");
					}

					return new OldUserDataSource(
						filePath: options.RealUsersFile,
						administrationAgreementSigned: o.UsersAdministrationAgreementSigned,
						maxUserCount: o.RealUserCount
					);
				}
			}
		};

		if (!userSources.ContainsKey(o.Users)) {
			throw new ArgumentException($"Value for option --users: '{o.Users}' is not in correct format.");
		}

		_userSource = userSources[o.Users](o);

		int phyloTreeCount;
		if (o.PhyloTreeNodesCount != 0) //count set as argument, so it will be used
			phyloTreeCount = o.PhyloTreeNodesCount;
		else if (o.MockHaplogroupCount != 0) //otherwise we look for the count of haplogroups
			phyloTreeCount = o.MockHaplogroupCount; //if it was specified, we use it
		else
			phyloTreeCount = _haplogroupSource.GetData().Count; //if not we calculate it

		Random random = new Random(42);

		Dictionary<string, Func<Options, IPhyloTreeFetcher<PhyloVariantedNode>>> fullTreeSources = new() {
			{
				Options.Mock,
				(options) => new MockPhyloTreeCreator(
					nodesCount: phyloTreeCount,
					builder: new MockPhyloTreeBuilder(
						haplogroupsCreator: () => _haplogroupSource.GetData(),
						ran: random,
						System.Console.Out
					),
					random: random
				)
			},
			{
				Options.Real,
				(options) => new RealPhyloTreeSource(
					new PhyloTreeWebDownloader(),
					new FtDnaTreeParser(),
					options.PhyloTreeWebUrl
				)
			}
		};

		if (!userSources.ContainsKey(o.PhyloTree)) {
			throw new ArgumentException($"Value for option --phylo-tree: '{o.PhyloTree}' is not in correct format.");
		}

		_fullTreeSource = fullTreeSources[o.PhyloTree](o);
	}
}