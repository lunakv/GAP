﻿using CommandLine;

namespace DataLoader.CmdArgsParsers;

public class Options {
	public const string Mock = "mock";
	public const string Real = "real";
	public const string CzechJson = "czech-json";

	[Option(
		Default = Mock,
		HelpText = $"Specify which haplogroups should be loaded to database. Acceptable values are: '{Mock}', '{Real}'."
	)]
	public string Haplogroups { get; set; } = null!;

	[Option(
		"mock-haplogroup-count",
		Default = 0,
		HelpText = $"Specify how many MOCK haplogroups to create. If 0, some default collection will be created."
	)]
	public int MockHaplogroupCount { get; set; }

	[Option(
		Default = Mock,
		HelpText = $"Specify which regions should be loaded to database. Acceptable values are: '{Mock}', '{CzechJson}'."
	)]
	public string Regions { get; set; } = null!;

	[Option(
		"regions-file",
		HelpText = "Specify file with regions."
	)]
	public string RegionsFilePath { get; set; } = null!;

	[Option(
		longName: "users",
		Default = Mock,
		HelpText = $"Specify which users should be loaded to database. Acceptable values are: '{Mock}', '{Real}'."
	)]
	public string Users { get; set; } = null!;

	[Option(
		"mock-user-count",
		Default = 500,
		HelpText = "Specify how many MOCK users should be loaded to the database."
	)]
	public int MockUserCount { get; set; }
	
	[Option(
		"real-user-count",
		Default = 0,
		HelpText = "Specify how many REAL users should be loaded to the database."
	)]
	public int RealUserCount { get; set; }

	[Option(
		"users-administration-agreement-signed",
		Required = false,
		Default = false,
		HelpText = $"Specify whether users to be registered have signed or not the agreement. For mock users, setting false means that only some have false value."
	)]
	public bool UsersAdministrationAgreementSigned { get; set; }

	[Option(
		"real-users-file",
		Default = "realUsers.json",
		HelpText = $"Specify the path from the real users should be loaded from."
	)]
	public string RealUsersFile { get; set; } = null!;

	[Option(
		"user-password",
		Default = null,
		HelpText = $"Specify if standard genetic data users should all have one same password or not password at all."
	)]
	public string? UserPassword { get; set; } = null!;
	
	[Option(
		"root-email",
		Default = "root@dnaportal.cz",
		HelpText = $"Specify email for which root user should be created."
	)]
	public string RootEmail { get; set; } = null!;

	[Option(
		"root-password",
		Required = true,
		HelpText = $"Specify root password."
	)]
	public string RootPassword { get; set; } = null!;
	
	[Option(
		"create-phylo-tree",
		Default = false,
		HelpText = $"Specify whether create and output PhyloTree (can contain downloading and pruning the tree)."
	)]
	public bool CreatePhyloTree { get; set; }
	
	[Option(
		"phylo-tree",
		Default = Mock,
		HelpText = $"Specify how phylogenetic trees should be loaded. Acceptable values are: '{Mock}', '{Real}'."
	)]
	public string PhyloTree { get; set; } = null!;
	
	[Option(
		"phylo-tree-web-url",
		Default = "https://www.familytreedna.com/public/y-dna-haplotree/get",
		HelpText = "Specify where to download the real phylogenetic tree from."
	)]
	public string PhyloTreeWebUrl { get; set; } = null!;

	[Option(
		"mock-phylo-tree-nodes-count",
		Default = 0,
		HelpText = $"Specify how many nodes should the MOCK phylogenetic trees have. If 0, same as haplogroups-count."
	)]
	public int PhyloTreeNodesCount { get; set; }

	[Option(
		"phylo-tree-output",
		Default = "./phyloTree.json",
		HelpText = $"Specify path where the generated tree should be created."
	)]
	public string PhyloTreeOutput { get; set; } = null!;
	
	[Option(
		"multiple-data-sources",
		Default = false,
		HelpText = $"Specify whether user with id 1 should have str markers from multiple sources. Acceptable values are: 'true', 'false'."
	)]
	public bool MultipleDataSources { get; set; }

	[Option(
		"mock-kits",
		Default = false,
		HelpText = $"Specify whether to include mock kit values. Acceptable values are: 'true', 'false'."
	)]
	public bool MockKits { get; set; }

	[Option(
		"mock-kits-per-stage",
		Default = (ushort)100,
		HelpText = $"Specify how many mock kits per stage to show."
	)]
	public ushort MockKitsPerStage { get; set; }
	
	[Option(
		"mock-admins",
		Default = false,
		HelpText = $"Specify whether lab tech user and admin user should be created."
	)]
	public bool MockAdmins { get; set; }

	[Option(
		"mock-admin-password",
		Default = "TotallyRandom_42",
		HelpText = $"Specify admins password."
	)]
	public string MockAdminPassword { get; set; } = null!;
}