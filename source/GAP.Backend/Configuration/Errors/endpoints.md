# Endpoint user exception info.
## Users
### GetUser(userId)
- UserNotFoundException - user with userId not found in db.

### GetUsers()

### GetFamilyNames()

### UpdateProfile(userId, profile)
- RegionNotFoundException - municipalita nesedi na zadnou municipalitu v db.
- UserNotFoundException - user with userId not found in db.
- ForbiddenEmailChangeAttemptException - email nesedi s aktualnim !!!
- ProfileGivenNameValidationFailure 
- ProfileFamilyValidationFailure
- ProfileEmailValidationFailure
- ProfilePhoneNumberValidationFailure
- ProfileBirthDateValidationFailure
- AddressTownValidationFailure - Correspondence Address
- AddressStreetValidationFailure - Correspondence Address
- AddressHouseNumberValidationFailure - Correspondence Address
- AddressZipCodeValidationFailure - Correspondence Address
- ExpandedAddressTownValidationFailure - Residence Address
- ExpandedAddressMunicipalityValidationFailure - Residence Address
- ExpandedAddressCountyValidationFailure - Residence Address
- ExpandedAddressStreetValidationFailure - Residence Address
- ExpandedAddressHouseNumberValidationFailure - Residence Address
- ExpandedAddressZipCodeValidationFailure - Residence Address

### UpdateAncestor(userId, ancestor)
- UserNotFoundException - user with userId not found in db.
- AncestorGivenNameValidationFailure
- AncestorFamilyValidationFailure
- AncestorBirthDateValidationFailure
- AncestorPlaceOfBirthValidationFailure
- AncestorDeathDateValidationFailure
- AncestorPlaceOfDeathValidationFailure
- ExpandedAddressTownValidationFailure - Address
- ExpandedAddressMunicipalityValidationFailure - Address
- ExpandedAddressCountyValidationFailure - Address
- ExpandedAddressStreetValidationFailure - Address
- ExpandedAddressHouseNumberValidationFailure - Address
- ExpandedAddressZipCodeValidationFailure - Address

## UserSharing
### GetUserManagementSettings(userId)
- nic, pri spatnym userId jsou settings prazdny (stejna situace jako kdyz je fakt nema)

### GetUserViewSettings(userId)
- nic, pri spatnym userId jsou settings prazdny (stejna situace jako kdyz je fakt nema)

### AddUserManager(userId, managerEmail)
- UserNotFoundException - pro oba parametry - nelze rozlisit, co selhalo
- ManagerAndManagedUserSameException
- ManageRightAlreadyPresentException

### RemoveUserManager(userId, managerId)
- UserNotFoundException - pro oba parametry - nelze rozlisit, co selhalo
- ManagerAndManagedUserSameException
- ManageRightNotPresentException

### AddUserViewer(userId, viewerId)
- UserNotFoundException - pro oba parametry - nelze rozlisit, co selhalo
- ViewerAndViewableUserSameException
- ViewRightAlreadyPresentException

### RemoveUserViewer(userId, viewerId)
- UserNotFoundException - pro oba parametry - nelze rozlisit, co selhalo
- ViewerAndViewableUserSameException
- ViewRightNotPresentException

## Map

### GetRegionLayerAsync(layerId, filter)
- LayerNotFoundException

### GetRegionDetailAsync(regionId, filter)
- RegionNotFoundException

## Logging

### GetUserLogs(logFilter, includeRawLogs)
- PerformerUserNotFoundException - given performer email is prolly wrong
- AboutUserNotFoundException - given about user email is prolly wrong

## Laboratory

### GetKits([int] kitIds)
- Zadna, obecny filter veci jsou v ramci graphql knihovny a fungujou listove

### AdvanceKitsToNextStage([KitAction] kitActions)
- KitNotFoundException - one or more kits not found
- MultipleActionOnOneKitException - two action on the same kit
- KitsNotInSameStageException - Kits from kitActions are in different stages
- AnalysedKitNoNextStageException - at least one kit from kitAction is in final ANALYSED stage

### UpdateKit(kitUpdateInput)
- KitNotFoundException - kit by kitId in kitUpdateInput not found

### GetAvailableLabelLayouts()

### GenerateLabels(kitIds, userLabelSelection, labelLayout)
- InvalidLabelSelectionException - 0 nebo moc hodne labelu per typ labelu
- KitNotFoundException - one or more of kitIds not found
- LayoutNotFoundException - labelLayout incorrect
- zatim vsechno se mapuje na BadRequest s nejakou peknou messagi, chcete custom cody?

### PrepareSequencerInputAsync([int] kitIds)
- KitNotFoundException - kitId not found
- SequencerInputKitLimitExceededException - Too many kit ids specified - sequencer input is full.

### GenerateSequencerInput(sequencerInputInput)

## GenData

### GetStrConflicts(userIds)
- StrConflictNotFoundException - one or more conflicts not found

### FixStrConflict(userId, newAggregatedMarkerValues)
- StrConflictNotFoundException - no str conflict for userId found
- UserNotFoundException - user userId nenalezen
- NewAggregatedMarkersHaveDifferentMarkersException - nove agregovane markery maji spatne markery

### ServeOriginalStrFile(fileName)
- BadRequest - kdyz je filename null, empty nebo obsahuje invalid chars.
- NotFound - kdyz je spatny fileName v url a nic jsem nenasel
- Unauthorized 

### GetHaplogroups()

### GetHaplogroup(haplogroupId)
- HaplogroupNotFoundException

### AddStrMarkers(userId, geneticTestProvider, strMarkers, file)
- UserNotFoundException

### AddStrMarkersFile(userId, file)
- UserNotFoundException
- ParsingException - spatnej file format nejspis
- OriginalStrDataSourcePresentException - stejnej zdroj dat od stejnyho provider uz vlozen


### TODO add UploadLabStrMarkersFile exception info - not implemented yet

## ClosestRelativeSearch
### GetClosestStrRelatives(int userId, string markerSetName,
- TODO jeste bude update api na spravny chovani pro barvicky a nody
- UserNotFoundException - user neni v db
- UserHasNoStrMarkersException - user nema str markery (pending)
- MarkerSetNotFoundException 

### ListMarkersForGivenHaplo(userId, markerSetName, int haplogroupId)
- UserNotFoundException - user neni v db
pro neplatné haploId vrátí pouze markery zadaného uživatele
- NoRootSpecifiedException
- WrongHaplogroupBranchException

### ListMarkersForGivenHaploSubtree(userId, markerSetName, int haplogroupId)
- UserNotFoundException - user neni v db
- HaplogroupNotFoundException
- NoRootSpecifiedException
- WrongHaplogroupBranchException
- HaplogroupNotInPhyloTreeException

### ListAvailableMarkerSets()

### GetMarkerSet(name)
- MarkerSetNotFoundException

## Auth

### Login
- UserNameOrPasswordIncorrectException - wrong email or password

### Registration
- RegionNotFoundException - ORP je spatne a backend ji nenasel dle jmena v db.
- ParsingException - jen pro registrace, kde se dava file :) 
- DuplicateEmailException - email uz ma jienj user
- MunicipalityNullException
- ProfileGivenNameValidationFailure
- ProfileFamilyValidationFailure
- ProfileEmailValidationFailure
- ProfilePhoneNumberValidationFailure
- ProfileBirthDateValidationFailure
- AncestorGivenNameValidationFailure
- AncestorFamilyValidationFailure
- AncestorBirthDateValidationFailure
- AncestorPlaceOfBirthValidationFailure
- AncestorDeathDateValidationFailure
- AncestorPlaceOfDeathValidationFailure
- AddressTownValidationFailure - Correspondence Address
- AddressStreetValidationFailure - Correspondence Address
- AddressHouseNumberValidationFailure - Correspondence Address
- AddressZipCodeValidationFailure - Correspondence Address
- ExpandedAddressTownValidationFailure - Residence Address OR Ancestor Address
- ExpandedAddressMunicipalityValidationFailure - Residence Address OR Ancestor Address
- ExpandedAddressCountyValidationFailure - Residence Address OR Ancestor Address
- ExpandedAddressStreetValidationFailure - Residence Address OR Ancestor Address
- ExpandedAddressHouseNumberValidationFailure - Residence Address OR Ancestor Address
- ExpandedAddressZipCodeValidationFailure - Residence Address OR Ancestor Address

### ConfirmEmail(userId, token)
- AuthUserNotFoundException
- InvalidTokenException

### ChangePassword(userId, currentPassword, newPassword)
- AuthUserNotFoundException
- PasswordMismatchException
  
### ChangeEmail(userId, newEmail, token)
- AuthUserNotFoundException
- InvalidTokenException

### ResetPassword(userId, newPassword, token)
- AuthUserNotFoundException
- InvalidTokenException

### RequestEmailChange(userId, newEmail)
- AuthUserNotFoundException

### RequestPasswordReset(userId)
- AuthUserNotFoundException


## Phylogenetic Tree
TODO Cyril

## Family Tree

### GetFamilyTree(userId, treeId)
- UserNotFoundException - user with userId not found in db.
- FamilyTreeOwnershipException - user nevlastní tento strom

### GetFamilyTreesOfUser(userId)
nic - pro neexistujícího usera vrátí prázdný list

### GetFamilyTreeBackup(userId, treeId)
- UserNotFoundException - user with userId not found in db.
- FamilyTreeOwnershipException - user nevlastní tento strom

### UpdateFamilyTree(userId, treeId, nodes)
- UserNotFoundException - user with userId not found in db.
- FamilyTreeOwnershipException - user nevlastní tento strom

### AddFamilyTree(userId)
- UserNotFoundException - user with userId not found in db.

### RemoveFamilyTree(userId, treeId)
- UserNotFoundException - user with userId not found in db.
- FamilyTreeOwnershipException - user nevlastní tento strom
