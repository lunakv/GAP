﻿using PhyloTree.DataClasses;
using System.Drawing;

namespace PhyloTreeTests {
	public class IndicatedNode : PhyloQuantifiedNode {
		public IndicatedNode(int id, int? parentId, int haplogroupId, int year, string name, List<int> children, NumberOfPeople numberOfPeople, IList<int> peopleSample, bool isPruned)
			: base(id, parentId, name, year, children, numberOfPeople, peopleSample, false, haplogroupId) {
			IsPruned = isPruned;
		}

		public bool IsPruned { get; }
	}
}
