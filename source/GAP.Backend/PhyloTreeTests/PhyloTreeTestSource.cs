﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestUtils;
using Users.GenData.Haplogroup;
using Users.UserAccess.Entities;

namespace PhyloTreeTests {
	public class PhyloTreeTestSource : TestSource {
		public int TreeId { get; }
		public DateTime DateTime { get; }
		public IList<Haplogroup> Haplogroups { get; }
		public IDictionary<Haplogroup, IList<CompleteUser>> HaplogroupDistribution { get; }

		public PhyloTreeTestSource(
			IList<Haplogroup> haplogroups,
			IDictionary<Haplogroup, IList<CompleteUser>> haplogroupDistribution,
			int treeId,
			[CallerMemberName] string testName = "No test name provided.")
			: this(haplogroups, haplogroupDistribution, DateTime.Now, treeId, testName) { }

		public PhyloTreeTestSource(
			IList<Haplogroup> haplogroups,
			IDictionary<Haplogroup, IList<CompleteUser>> haplogroupDistribution,
			DateTime time,
			int treeId,
			[CallerMemberName] string testName = "No test name provided.")
			: base(testName) {
			//removing the miliseconds - those should not be tested
			DateTime = new DateTime(time.Year, time.Month, time.Day, time.Hour, time.Minute, time.Second);
			TreeId = treeId;
			HaplogroupDistribution = haplogroupDistribution;
			Haplogroups = haplogroups;
		}
	}
}
