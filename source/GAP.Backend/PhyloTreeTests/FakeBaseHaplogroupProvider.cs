﻿using PhyloTree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhyloTreeTests {
	internal record FakeBaseHaplogroupProvider(List<string> Ignored, List<string> Added) : IBaseHaplogroupsProvider {
		public FakeBaseHaplogroupProvider() :this(new List<string>(), new List<string>()) { }
		public List<string> GetAdded() => Added;

		public List<string> GetIgnored() => Ignored;
	}
}
