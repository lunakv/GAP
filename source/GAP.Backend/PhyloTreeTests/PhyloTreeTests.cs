﻿using DataLoader.MockDataLoaders;
using DataLoader;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using System.Text;
using System.Threading.Tasks;
using PhyloTree;
using Xunit;
using PhyloTree.DataClasses;
using PhyloTree.Access;
using System.Drawing;
using Users.GenData.Haplogroup;
using Users.UserAccess.Entities;

namespace PhyloTreeTests {
	[Collection(nameof(TestCollection))]
	public sealed class PhyloTreeTests : IDisposable {
		private static readonly bool removeFiles = false;

		private static string FileForType<T>()
			=> FileForString(nameof(T));

		private static string FileForString(string s)
			=> s + "_Test.json";

		public PhyloTreeTests() { }

		public void Dispose() {
			if(removeFiles) {
				if(File.Exists(FileForType<PhyloTreeNode>())) {
					File.Delete(FileForType<PhyloTreeNode>());
				}
				if(File.Exists(FileForType<PhyloQuantifiedNode>())) {
					File.Delete(FileForType<PhyloQuantifiedNode>());
				}
			}
		}

		[Theory]
		[MemberData(nameof(GetData))]
		public void GetUsersTests(PhyloTreeTestSource testSource) {

			var tree = GetQuantifiedLinearTree(testSource);

			tree.Roots.Should()
				.HaveCount(1);

			var root = tree.Nodes[tree.Roots[0]];

			var total = root.PeopleCount.Inside + root.PeopleCount.InSubtree;
			Assert.Equal(total, testSource.HaplogroupDistribution.Sum(u => u.Value.Count));

			tree.Nodes.Select(n => n.Value.PeopleCount.Inside);
			//we do not test people samples because they are supposed to be
			//much smaller than the given test users list
		}

		[Theory]
		[MemberData(nameof(GetData))]
		public void SimpleLinearTreeSavingTest(PhyloTreeTestSource testSource) {
			PhyloTreeOriginalJsonProcessor treeManager =
				new PhyloTreeOriginalJsonProcessor(FileForType<PhyloTreeOriginalJsonProcessor>());
			var tree = GetSimpleLinearTree(testSource);
			treeManager.SaveTree(tree);
			var loadedTreeTask = treeManager.GetTreeAsync();
			loadedTreeTask.Wait();
			var loadedTree = loadedTreeTask.Result;

			loadedTree.Should()
				.NotBeNull();

			loadedTree.Should()
				.BeEquivalentTo(tree);
		}

		[Theory]
		[MemberData(nameof(GetData))]
		public void TreeDateTest(PhyloTreeTestSource source) {
			var tree = GetQuantifiedLinearTree(source);

			Assert.Equal(source.DateTime, tree.CreationTime);
		}

		[Theory]
		[MemberData(nameof(GetData))]
		public void TreeIdTest(PhyloTreeTestSource source) {
			var tree = GetQuantifiedLinearTree(source);

			Assert.Equal(source.TreeId, tree.TreeId);
		}

		static PhylogeneticTree<PhyloQuantifiedNode> GetQuantifiedLinearTree(PhyloTreeTestSource source) {
			IList<int> GetUsers(int index)
				=> source.HaplogroupDistribution[source.Haplogroups[index]].Select(u => u.Id).ToList();

			int peopleSum = 0;

			void NodeDone(int i)
				=> peopleSum += GetUsers(i).Count;
			PhyloQuantifiedNode CreateNode(int i) {
				List<int> children = new();
				if(i < source.Haplogroups.Count - 1)
					children.Add(i + 1);
				return new PhyloQuantifiedNode(
					i + source.Haplogroups.Count, //so that if we confuse indices it just doesn't work instead of making something weird
					i == 0 ? null : i - 1 + source.Haplogroups.Count,
					$"{i}_id",
					1500,
					children, new NumberOfPeople(GetUsers(i).Count, peopleSum),
					GetUsers(i),
					false,
					source.Haplogroups[i].Id
					);
			}

			return GetTree(source,
				CreateNode,
				NodeDone);
		}

		static PhylogeneticTree<PhyloVariantedNode> GetSimpleLinearTree(PhyloTreeTestSource source) {

			PhyloVariantedNode CreateNode(int i) {

				List<int> children = new();
				if(i < source.Haplogroups.Count - 1)
					children.Add(i + 1);
				return new PhyloVariantedNode(
					i,
					i == 0 ? null : i + source.Haplogroups.Count - 1,
					$"{i}_name",
					1000,
					children,
					new List<string>()
					);
			}
			return GetTree(source, CreateNode, _ => { });
		}

		static PhylogeneticTree<T> GetTree<T>
			(PhyloTreeTestSource source,
			Func<int, T> factory,
			Action<int> nodeDone) where T : PhyloTreeNode {
			PhylogeneticTree<T>.TreeBuilder builder = new();
			for(int i = source.Haplogroups.Count - 1; i >= 0; i--) {
				builder.Add(factory(i), i == 0);
				nodeDone(i);
			}
			builder.SetDate(source.DateTime);
			builder.SetTreeID(source.TreeId);
			return builder.GetTree();
		}

		public static IEnumerable<object[]> GetData() {
			yield return SingleNodeTree();
			yield return MultiNodeTree();
		}

		private static object[] SingleNodeTree() {
			IList<Haplogroup> haplogroups = new List<Haplogroup> {
				new Haplogroup(id: 0, name: "Ra1")
			};

			var users = GetMockUsersForHaplogroup(haplogroups[0], 20);

			IDictionary<Haplogroup, IList<CompleteUser>> haplogroupDistribution = new Dictionary<Haplogroup, IList<CompleteUser>> {
				{ haplogroups[0], users }
			};

			return new object[] { new PhyloTreeTestSource(
					haplogroups: haplogroups,
					haplogroupDistribution: haplogroupDistribution,
					treeId: new Random().Next(1000)
				)
			};
		}

		private static object[] MultiNodeTree() {
			IList<Haplogroup> haplogroups = new List<Haplogroup> {
				new Haplogroup(id: 0, name: "Ra1"),
				new Haplogroup(id: 1, name: "A2"),
				new Haplogroup(id: 2, name: "Nsd3"),
				new Haplogroup(id: 3, name: "Psa4"),
				new Haplogroup(id: 4, name: "L3"),
				new Haplogroup(id: 5, name: "Oa")
			};

			Random rnd = new Random();

			IDictionary<Haplogroup, IList<CompleteUser>> haplogroupDistribution = new Dictionary<Haplogroup, IList<CompleteUser>>();

			foreach(Haplogroup haplogroup in haplogroups) {
				var users = GetMockUsersForHaplogroup(haplogroup, rnd.Next(10, 20));
				haplogroupDistribution[haplogroup] = users;
			}

			return new object[] { new PhyloTreeTestSource(
					haplogroups: haplogroups,
					haplogroupDistribution: haplogroupDistribution,
					treeId: new Random().Next(1000)
				)
			};
		}

		public static IList<CompleteUser> GetMockUsersForHaplogroup(Haplogroup haplogroup, int userCount) {
			IList<int> regionKeys = new List<int> { 0 };

			return new MockUserSource(
					haplogroups: new Haplogroup[] { haplogroup },
					regionKeys: regionKeys,
					usersToGenerate: userCount,
					administrationAgreementSigned: true
				).GetData()
				.Select(u => u.ToCompleteUser())
				.ToList();
		}
	}
}
