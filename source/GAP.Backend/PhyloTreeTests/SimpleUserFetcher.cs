﻿using Users.UserAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.UserAccess.UserFilter;
using Microsoft.AspNetCore.Server.IIS.Core;
using Users.UserAccess.Entities;
using Users.UserAccess.Storage;

namespace PhyloTreeTests {
	class SimpleUserFetcher : IUserFetcher {
		private IList<User> _users;
		public SimpleUserFetcher(IList<User> users)
			=> _users = new List<User>(users);
		public SimpleUserFetcher(IList<CompleteUser> users)
			=> _users = new List<User>(users.Select(u => u.ToUser()));

		public User FindUserById(int userId) {
			var u = _users.FirstOrDefault(u => u.Id == userId);
			if(u == null) {
				throw new ArgumentException($"no user found with id {userId}");
			}
			return u;
		}

		public IList<User> GetUsers()
			=> new List<User>(_users);

		public Task<User> FindUserByIdAsync(int userId) {
			return Task.FromResult(FindUserById(userId));
		}

		public Task<IReadOnlyDictionary<int, User>> FindUsersByIdAsync(IReadOnlyCollection<int> userIds) {
			var users = new Dictionary<int, User>();
			foreach (int id in userIds) {
				users[id] = FindUserById(id);
			}

			return Task.FromResult((IReadOnlyDictionary<int, User>)users);
		}

		public Task<IList<User>> GetUsersAsync() {
			return Task.FromResult(GetUsers());
		}

		public async Task<IList<User>> GetUsersAsync(UserFilter userFilter) {
			return userFilter.Filter(await GetUsersAsync()).ToList();
		}

		public Task<User> FindUserByEmailAsync(string email) {
			var u = _users.FirstOrDefault(u => u.Profile.Email == email);
			if(u == null) {
				throw new ArgumentException($"no user found with email {email}");
			}
			return Task.FromResult(u);
		}
	}
}
