﻿using PhyloTree;
using PhyloTree.DataClasses;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.GenData.Haplogroup;
using Users.UserAccess.Entities;
using Xunit;

namespace PhyloTreeTests {

	[Collection(nameof(TestCollection))]
	public class PruningTests {

		private PhylogeneticTree<TNode> CopyTree<TNode>(PhylogeneticTree<TNode> source) 
			where TNode : PhyloTreeNode{
			PhylogeneticTree<TNode>.TreeBuilder builder = new();
			var roots = source.Roots.Select(r => (source.Nodes[r], true));
			Queue<(TNode, bool)> que = new Queue<(TNode, bool)>(roots);
			while(que.Count > 0) { 
				var curr = que.Dequeue();
				foreach(var ch in curr.Item1.Children) {
					que.Enqueue((source.Nodes[ch], false));
				}
				builder.Add(curr.Item1, curr.Item2);
			}
			if(source.CreationTime != null)
				builder.SetDate(source.CreationTime.Value);
			if(source.TreeId != null)
				builder.SetTreeID(source.TreeId.Value);
			return builder.GetTree();
		}

		private async Task<PhylogeneticTree<PhyloQuantifiedNode>> Prune(PruningTestSource testSource) {
			var hm = new HaplogroupManager(testSource.Haplogroups);
			var pruner = new PhyloTreePruner<CompleteUser>(
				hm,
				testSource.Users,
				new FakeBaseHaplogroupProvider(),
				hm);
			var preTree = await testSource.PhyloTreeGetter.GetTreeAsync();
			var treeTask = pruner.Prune(preTree);
			treeTask.Wait();
			return treeTask.Result ?? throw new ArgumentException("test tree could not be pruned");
		}

		[Theory]
		[MemberData(nameof(GetData))]
		public async Task PreservingUsersTests(PruningTestSource testSource) {
			int PeopleSum<TNode>(PhylogeneticTree<TNode> tree) where TNode : PhyloQuantifiedNode {
				return tree.Roots
					.Select(r => tree.Nodes[r])
					.Sum(n => n.PeopleCount.InSubtree + n.PeopleCount.Inside);
			}

			int count = testSource.HaplogroupDistribution.Sum(v => v.Value.Count);
			int testCount = PeopleSum(testSource.PrunnedArtifitialTree);
			Assert.Equal(count, testCount); //test source has it right
			var pruned = await Prune(testSource);
			var prunedCount = PeopleSum(pruned);
			Assert.Equal(count, prunedCount);
		}

		[Theory]
		[MemberData(nameof(GetData))]
		public async Task AreNodesPresentTests(PruningTestSource testSource) {
			var ids = testSource.PrunnedArtifitialTree.Nodes.Values
				.Where(n => !n.IsPruned) //the remaining ones
				.Select(n => n.Id);
			var pruned = await Prune(testSource);
			foreach(var id in ids) {
				pruned.Nodes.Keys.Should().Contain(id);
			}
		}
		
		[Theory]
		[MemberData(nameof(GetData))]
		public async Task AreNodesGoneTests(PruningTestSource testSource) {
			var ids = testSource.PrunnedArtifitialTree.Nodes.Values
				.Where(n => n.IsPruned) //the pruned ones
				.Select(n => n.Id);
			var pruned = await Prune(testSource);
			foreach(var id in ids) {
				pruned.Nodes.Keys.Should().NotContain(id);
			}
		}
		
		[Theory]
		[MemberData(nameof(GetData))]
		public async Task UsersHaplogroupsTests(PruningTestSource testSource) {
			var pruned = await Prune(testSource);
			foreach(var key in testSource.HaplogroupDistribution.Keys) {
				if(testSource.HaplogroupDistribution[key].Count == 0)
					continue;
				var node = pruned.Nodes.Values.FirstOrDefault(v => v.HaplogroupId == key.Id);
				node.Should().NotBeNull();
				Assert.Equal(
					node?.PeopleCount.Inside,
					testSource.HaplogroupDistribution[key].Count);
			}
		}

		public static IEnumerable<object[]> GetData() {
			yield return SingleNodeTree();
			yield return TwoNodeTreeEmptyRoot();
		}

		private static void AddInBoth(
			PhylogeneticTree<PhyloVariantedNode>.TreeBuilder fullBuilder,
			PhylogeneticTree<IndicatedNode>.TreeBuilder processedBuilder,
			int ID,
			int? parentId,
			int haplogroupId,
			string name,
			int year,
			List<int> children,
			NumberOfPeople numberOfPeople,
			IList<CompleteUser> peopleSample,
			bool isPruned,
			bool root) {
			fullBuilder.Add(new PhyloVariantedNode(
				ID, 
				parentId,  
				name,
				year,
				children,
				new List<string>()), 
				root);
			processedBuilder.Add(new IndicatedNode(
				ID, 
				parentId, 
				haplogroupId,
				year,
				name,
				children, 
				numberOfPeople,
				peopleSample.Select(u => u.Id).ToList(),
				isPruned), 
				root);
		}
		
		public static object[] SingleNodeTree() {
			List<Haplogroup> haplogroups = new() {
				new Haplogroup(id: 0, "ha1"),
			};
			IList<CompleteUser> users = PhyloTreeTests.GetMockUsersForHaplogroup(haplogroups[0], 1);
			
			IDictionary<Haplogroup, IList<CompleteUser>> haplogroupDistribution = new Dictionary<Haplogroup, IList<CompleteUser>> {
				{ haplogroups[0], users },
			};

			int id = new Random().Next(1000);
			DateTime time = DateTime.Now;

			PhylogeneticTree<PhyloVariantedNode>.TreeBuilder fullBuilder = new();
			fullBuilder.Add(new PhyloVariantedNode(0, null, "root", 1000, new List<int>(), new List<string>()), true);
			fullBuilder.SetDate(time);
			fullBuilder.SetTreeID(id);
			
			PhylogeneticTree<IndicatedNode>.TreeBuilder processedBuilder = new();
			processedBuilder.SetDate(time);
			processedBuilder.SetTreeID(id);

			processedBuilder.Add(new IndicatedNode(
				id: 0, 
				parentId: null, 
				haplogroupId: 0,
				year: -56789,
				name: "root", 
				children: new List<int>(), 
				numberOfPeople: new NumberOfPeople(1, 0),
				peopleSample: users.Select(u => u.Id).ToList(),
				isPruned: false), true);



			return new object[] { new PruningTestSource(
					haplogroups: haplogroups,
					haplogroupDistribution: haplogroupDistribution,
					creationTime: time,
					treeId: id,
					fullBuilder.GetTree(),
					processedBuilder.GetTree(),
					users: users
				) };
		}
		public static object[] TwoNodeTreeEmptyRoot() {
			List<Haplogroup> haplogroups = new() {
				new Haplogroup(id: 0, "ha1"),
				new Haplogroup(id: 1, "ri2"),
			};
			IList<CompleteUser> users = PhyloTreeTests.GetMockUsersForHaplogroup(haplogroups[1], 1);
			
			IDictionary<Haplogroup, IList<CompleteUser>> haplogroupDistribution = new Dictionary<Haplogroup, IList<CompleteUser>> {
				{ haplogroups[0], new List<CompleteUser>() },
				{ haplogroups[1], users },
			};

			int id = new Random().Next(1000);
			DateTime time = DateTime.Now;

			PhylogeneticTree<PhyloVariantedNode>.TreeBuilder fullBuilder = new();
			fullBuilder.SetDate(time);
			fullBuilder.SetTreeID(id);
			
			PhylogeneticTree<IndicatedNode>.TreeBuilder processedBuilder = new();
			processedBuilder.SetDate(time);
			processedBuilder.SetTreeID(id);

			AddInBoth(
				fullBuilder,
				processedBuilder,
				ID: 0, 
				parentId: null, 
				haplogroupId: 0,
				name: "root",
				year: 2340,
				children: new List<int>(), 
				numberOfPeople: new NumberOfPeople(Inside: 0,  InSubtree: 1),
				peopleSample: new List<CompleteUser>(),
				isPruned: true,
				root: true);
			AddInBoth(
				fullBuilder,
				processedBuilder,
				ID: 1, 
				parentId: 0, 
				haplogroupId: 1, 
				name: "leaf",
				year: 1556,
				children: new List<int>(), 
				numberOfPeople: new NumberOfPeople(Inside: 1,  InSubtree: 0),
				peopleSample: users, 
				isPruned: false,
				root: false);



			return new object[] { new PruningTestSource(
					haplogroups: haplogroups,
					haplogroupDistribution: haplogroupDistribution,
					creationTime: time,
					treeId: id,
					fullBuilder.GetTree(),
					processedBuilder.GetTree(),
					users: users
				) };
		}
	}
}
