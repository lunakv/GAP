﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.GenData.Haplogroup;
using Users.GenData.Haplogroup.Storage;
using Users.UserAccess.UserFilter;

namespace PhyloTreeTests {
	internal class HaplogroupManager : IHaplogroupFetcher, IHaplogroupSaver {
		private readonly List<Haplogroup> haplos;
		private int maxId;
		public HaplogroupManager(IList<Haplogroup> haplos) {
			this.haplos = new List<Haplogroup>(haplos);
			this.maxId = haplos.Max(h => h.Id);
		}

		public Task<Haplogroup> FindHaplogroupByIdAsync(int haplogroupId) {
			return Task.FromResult(haplos.FirstOrDefault(h => h.Id== haplogroupId));
		}

		public Task<IList<Haplogroup>> GetHaplogroupsAsync() {
			return Task.FromResult((IList<Haplogroup>)new List<Haplogroup>(haplos));
		}

		public Task<IList<Haplogroup>> GetHaplogroupsAsync(IHaplogroupFilter haplogroupFilter) {
			return Task.FromResult((IList<Haplogroup>)(haplogroupFilter.Filter(haplos).ToList()));
		}

		public Task AddHaplogroupAsync(string haplogroupName) {
			return AddHaplogroupsAsync(new List<string>() { haplogroupName });
		}

		public Task AddHaplogroupsAsync(IList<string> haplogroupNames) {
			foreach(var h in haplogroupNames) {
				haplos.Add(new Haplogroup(++maxId, h));
			}
			return Task.CompletedTask;
		}
	}
}
