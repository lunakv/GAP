﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhyloTree;
using PhyloTree.DataClasses;

namespace PhyloTreeTests {
	class SimpleTreeLoader : IPhyloTreeFetcher<PhyloVariantedNode> {
		private PhylogeneticTree<PhyloVariantedNode> _tree;
		public SimpleTreeLoader(PhylogeneticTree<PhyloVariantedNode> tree)
			=> _tree = tree;

		public Task<PhylogeneticTree<PhyloVariantedNode>> GetTreeAsync() 
			=> Task.FromResult(_tree);
	}
}
