﻿using PhyloTree;
using PhyloTree.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using PhyloTree.Exceptions;
using Users.GenData.Haplogroup;
using Users.UserAccess.Entities;

namespace PhyloTreeTests {

	[Collection(nameof(TestCollection))]
	public class TreePruningExceptionsTests {

		[Fact]
		public void NoRootExceptions() {
			PhylogeneticTree<PhyloVariantedNode>.TreeBuilder builder = new();
			builder.Add(new PhyloVariantedNode(0, null, "root", 1000, new List<int>(), new List<string>()), true);
			var hm = new HaplogroupManager(new List<Haplogroup>());
			IPhyloTreePruner pruner = new PhyloTreePruner<CompleteUser>(
				hm,
				new List<CompleteUser>(),
				new FakeBaseHaplogroupProvider(),
				hm);
			pruner.Invoking(p => p.Prune(builder.GetTree())?.Wait())
				.Should().Throw<NoRootSpecifiedException>();
		}
	}
}
