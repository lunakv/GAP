﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.UserAccess.Entities;

namespace PhyloTreeTests {
	internal static class UserExtensions {
		public static CompleteUser ToCompleteUser(this User u) {
			return new CompleteUser(
					u.Id,
					u.AdministrationAgreementSigned,
					u.PublicId,
					u.Profile,
					u.Ancestor,
					u.RegionId,
					u.GeneticData 
						?? throw new ArgumentException("Trying to make a complete user out of a user with no genetic data"),
					u.AdministratorData);
		}
		public static User ToUser(this CompleteUser u) {
			return new User(
					u.Id,
					u.AdministrationAgreementSigned,
					u.PublicId,
					u.Profile,
					u.Ancestor,
					u.RegionId,
					u.GeneticData,
					u.AdministratorData);
		}
	}
}
