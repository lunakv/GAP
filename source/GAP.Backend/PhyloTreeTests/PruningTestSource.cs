﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Users.UserAccess;
using PhyloTree;
using PhyloTree.DataClasses;
using TestUtils;
using Users.GenData.Haplogroup;
using Users.GenData.Haplogroup.Storage;
using Users.UserAccess.Entities;
using Users.UserAccess.Storage;
using Users.UserAccess.UserFilter;

namespace PhyloTreeTests {
	public class PruningTestSource : TestSource {
		public IList<Haplogroup> Haplogroups { get; }
		public IDictionary<Haplogroup, IList<CompleteUser>> HaplogroupDistribution { get; }
		public DateTime CreationTime { get; }
		public int TreeId { get; }
		public PhylogeneticTree<PhyloVariantedNode> FullTree { get; }
		public IPhyloTreeFetcher<PhyloVariantedNode> PhyloTreeGetter 
			=> new SimpleTreeLoader(FullTree);
		public IList<CompleteUser> Users { get; }
		public IUserFetcher UserFetcher
			=> new SimpleUserFetcher(Users);
		public IHaplogroupFetcher HaploFetcher { get; }
		public PhylogeneticTree<IndicatedNode> PrunnedArtifitialTree { get; }
		
		public PruningTestSource(
			IList<Haplogroup> haplogroups,
			IDictionary<Haplogroup, IList<CompleteUser>> haplogroupDistribution,
			DateTime creationTime,
			int treeId,
			PhylogeneticTree<PhyloVariantedNode> fullTree,
			PhylogeneticTree<IndicatedNode> prunnedTree,
			IList<CompleteUser> users,
			[CallerMemberName] string testName = "No test name provided.") 
			 : base(testName){ 
			this.Haplogroups = haplogroups;
			this.HaplogroupDistribution = haplogroupDistribution;
			this.CreationTime = creationTime;
			this.TreeId = treeId;
			this.FullTree = fullTree;
			this.PrunnedArtifitialTree = prunnedTree;
			this.Users = users;
			this.HaploFetcher = new FakeHaploFetcher(haplogroups);
		}
	}
}

class FakeHaploFetcher : IHaplogroupFetcher {
		private IList<Haplogroup> _haplos;
		public FakeHaploFetcher(IList<Haplogroup> haplos) {
			_haplos = haplos;
		}

		public Task<Haplogroup> FindHaplogroupByIdAsync(int haplogroupId) {
			var result = _haplos.FirstOrDefault(h => h.Id == haplogroupId)
				?? throw new ArgumentException($"there is no haplogroup of id {haplogroupId}, or it's null");
			
			return Task.FromResult(result);
		}

		public  Task<IList<Haplogroup>> GetHaplogroupsAsync() {
			return Task.FromResult<IList<Haplogroup>>(new List<Haplogroup>(_haplos));
		}

		public Task<IList<Haplogroup>> GetHaplogroupsAsync(IHaplogroupFilter haplogroupFilter) {
			return Task.FromResult<IList<Haplogroup>>(haplogroupFilter.Filter(_haplos).ToList());
		}
	}