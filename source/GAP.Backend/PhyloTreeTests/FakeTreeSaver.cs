﻿using PhyloTree.DataClasses;
using PhyloTree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhyloTreeTests {
	class FakeTreeSaver : IPhyloTreeSaver<PhyloQuantifiedNode> {
		public void SaveTree(PhylogeneticTree<PhyloQuantifiedNode> tree) {}
	}
}
