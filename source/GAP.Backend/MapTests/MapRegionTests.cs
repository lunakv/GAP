using System.Collections.Generic;
using FluentAssertions;
using Map.Map;
using Map.ValueObjects;
using Users.GenData.Haplogroup;
using Xunit;

namespace MapTests; 

public class MapRegionTests {
	
    [Theory]
    [MemberData(nameof(MapRegionTestData))]
    public void MapRegionUserCountTest(MapRegionTestSource testSource) {
        var region = new MapRegion(
            region: testSource.HierarchicalRegion,
            users: testSource.Users,
            haplogroups: testSource.Haplogroups
        );

        region.GetUserCount().Should().Be(testSource.ExpectedUserCount);
    }
    
    [Theory]
    [MemberData(nameof(MapRegionTestData))]
    public void MapRegionHaplogroupCountsTest(MapRegionTestSource testSource) {
        var region = new MapRegion(
            region: testSource.HierarchicalRegion,
            users: testSource.Users,
            haplogroups: testSource.Haplogroups
        );

        region.GetHaplogroupCounts().Should().BeEquivalentTo(testSource.ExpectedHaplogroupCounts,
            options => options
                .ComparingByMembers<HaplogroupCount>()
                .ComparingByMembers<Haplogroup>()
        );
    }

    [Theory]
    [MemberData(nameof(MapRegionTestData))]
    public void MapRegionHaplogroupFamilyNameCountsTest(MapRegionTestSource testSource) {
        var region = new MapRegion(
            region: testSource.HierarchicalRegion,
            users: testSource.Users,
            haplogroups: testSource.Haplogroups
        );

        region.GetHaplogroupFamilyNameCounts().Should().BeEquivalentTo(testSource.ExpectedHaplogroupFamilyNameCounts,
            options => options
                .ComparingByMembers<HaplogroupFamilyNameCounts>()
                .ComparingByMembers<HaplogroupCount>()
                .ComparingByMembers<Haplogroup>()
        );
    }
    public static IEnumerable<object[]> MapRegionTestData() {
        yield return SimpleTest();
    }
    
    private static object[] SimpleTest() {
        var haplogroups = new List<Haplogroup> {
            new Haplogroup(0, "HAPLO-A"),
            new Haplogroup(1, "HAPLO-B"),
            new Haplogroup(2, "HAPLO-C"),
            new Haplogroup(3, "HAPLO-D"),
        };
        
        var users = new List<MapUser>() {
            new MapUser(id: 0, familyName: "Mrazak", haplogroup: haplogroups[0], regionId: 1, municipality: "MUNI"),
            new MapUser(id: 1, familyName: "Novak", haplogroup: haplogroups[1], regionId: 1, municipality: "MUNI"),
            new MapUser(id: 2, familyName: "Mrazak", haplogroup: haplogroups[1], regionId: 1, municipality: "MUNI"),
            new MapUser(id: 3, familyName: "Novak", haplogroup: haplogroups[2], regionId: 1, municipality: "MUNI"),
            new MapUser(id: 4, familyName: "Kolecko", haplogroup: haplogroups[2], regionId: 1, municipality: "MUNI"),
            new MapUser(id: 5, familyName: "Mrazak", haplogroup: haplogroups[3], regionId: 1, municipality: "MUNI"),
            new MapUser(id: 6, familyName: "Novak", haplogroup: haplogroups[2], regionId: 1, municipality: "MUNI"),
            new MapUser(id: 7, familyName: "Novak", haplogroup: haplogroups[1], regionId: 1, municipality: "MUNI"),
            new MapUser(id: 8, familyName: "Kolecko", haplogroup: haplogroups[2], regionId: 1, municipality: "MUNI"),
            new MapUser(id: 9, familyName: "Novak", haplogroup: haplogroups[2], regionId: 1, municipality: "MUNI"),
        };
        
        return new object[] { new MapRegionTestSource(
            hierarchicalRegion: new HierarchicalRegion(
                region: new AdministrativeRegion(id: 1, type: AdministrativeRegion.RegionType.Municipality, name: "MUNI", parentRegionId: 2),
                subregions: new List<AdministrativeRegion>()
            ),
            users: users,
            haplogroups: haplogroups,
            expectedUserCount: users.Count,
            expectedHaplogroupCounts: new List<HaplogroupCount>() {
                new HaplogroupCount(Haplogroup: haplogroups[0], Count: 1),
                new HaplogroupCount(Haplogroup: haplogroups[1], Count: 3),
                new HaplogroupCount(Haplogroup: haplogroups[2], Count: 5),
                new HaplogroupCount(Haplogroup: haplogroups[3], Count: 1),
            },
            expectedHaplogroupFamilyNameCounts: new List<HaplogroupFamilyNameCounts>() {
                new HaplogroupFamilyNameCounts(
                    Haplogroup: haplogroups[0],
                    FamilyNameCounts: new List<FamilyNameCount>() {
                        new FamilyNameCount(FamilyName: "Mrazak", 1)
                    }
                ),
                new HaplogroupFamilyNameCounts(
                    Haplogroup: haplogroups[1],
                    FamilyNameCounts: new List<FamilyNameCount>() {
                        new FamilyNameCount(FamilyName: "Novak", 2),
                        new FamilyNameCount(FamilyName: "Mrazak", 1)
                    }
                ),
                new HaplogroupFamilyNameCounts(
                    Haplogroup: haplogroups[2],
                    FamilyNameCounts: new List<FamilyNameCount>() {
                        new FamilyNameCount(FamilyName: "Novak", 3),
                        new FamilyNameCount(FamilyName: "Kolecko", 2)
                    }
                ),
                new HaplogroupFamilyNameCounts(
                    Haplogroup: haplogroups[3],
                    FamilyNameCounts: new List<FamilyNameCount>() {
                        new FamilyNameCount(FamilyName: "Mrazak", 1)
                    }
                )
            }
        )};
    }
}