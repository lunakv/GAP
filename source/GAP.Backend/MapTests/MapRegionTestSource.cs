using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Map.Map;
using Map.ValueObjects;
using Users.GenData.Haplogroup;

namespace MapTests;

public class MapRegionTestSource : TestSource {

	public MapRegionTestSource(
		HierarchicalRegion hierarchicalRegion,
		IList<MapUser> users,
		IList<Haplogroup> haplogroups,
		int expectedUserCount,
		IList<HaplogroupCount> expectedHaplogroupCounts,
		IList<HaplogroupFamilyNameCounts> expectedHaplogroupFamilyNameCounts,
		[CallerMemberName] string testName = "No test name provided.") : base(testName) {
		HierarchicalRegion = hierarchicalRegion;
		Users = users;
		Haplogroups = haplogroups;
		ExpectedUserCount = expectedUserCount;
		ExpectedHaplogroupCounts = expectedHaplogroupCounts;
		ExpectedHaplogroupFamilyNameCounts = expectedHaplogroupFamilyNameCounts;
	}
	
	public HierarchicalRegion HierarchicalRegion { get; }
	public IList<MapUser> Users { get; }
	public IList<Haplogroup> Haplogroups { get; }
	public int ExpectedUserCount { get; }
	public IList<HaplogroupCount> ExpectedHaplogroupCounts { get; }
	public IList<HaplogroupFamilyNameCounts> ExpectedHaplogroupFamilyNameCounts { get; }

}