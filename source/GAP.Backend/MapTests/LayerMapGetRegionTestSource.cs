using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Map.Map;
using Map.ValueObjects;
using Users.GenData.Haplogroup;

namespace MapTests; 

public class LayerMapGetRegionTestSource : TestSource {

	public LayerMapGetRegionTestSource(
		IList<HierachicalRegionLayer> layers,
		IList<MapUser> users,
		IList<Haplogroup> haplogroups,
		MapRegion expectedRegion,
		[CallerMemberName] string testName = "No test name provided.") : base(testName) {
		Layers = layers;
		Users = users;
		Haplogroups = haplogroups;
		ExpectedRegion = expectedRegion;
	}
	
	public IList<HierachicalRegionLayer> Layers { get; }
	public IList<MapUser> Users { get; }
	public IList<Haplogroup> Haplogroups { get; }
	public MapRegion ExpectedRegion { get; }
}