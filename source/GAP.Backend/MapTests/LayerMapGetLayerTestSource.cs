using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Map.Map;
using Map.ValueObjects;
using Users.GenData.Haplogroup;

namespace MapTests; 

public class LayerMapGetLayerTestSource : TestSource {

	public LayerMapGetLayerTestSource(
		IList<HierachicalRegionLayer> layers,
		IList<MapUser> users,
		IList<Haplogroup> haplogroups,
		MapRegionLayer expectedLayer,
		[CallerMemberName] string testName = "No test name provided.") : base(testName) {
		Layers = layers;
		Users = users;
		Haplogroups = haplogroups;
		ExpectedLayer = expectedLayer;
	}
	
	public IList<HierachicalRegionLayer> Layers { get; }
	public IList<MapUser> Users { get; }
	public IList<Haplogroup> Haplogroups { get; }
	public MapRegionLayer ExpectedLayer { get; }
}