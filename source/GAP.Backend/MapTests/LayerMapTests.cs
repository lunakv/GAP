using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Map.Map;
using Map.ValueObjects;
using Users.GenData.Haplogroup;
using Xunit;

namespace MapTests; 

public class LayerMapTests {
	
    [Theory]
    [MemberData(nameof(LayerMapGetLayerTestData))]
    public void LayerMapGetLayerTest(LayerMapGetLayerTestSource testSource) {
        IMap map = new LayerMap(
            layers: testSource.Layers,
            users: testSource.Users,
            haplogroups: testSource.Haplogroups
        );

        var createdLayer = map.GetLayer(testSource.ExpectedLayer.Id);
        createdLayer.Id.Should().Be(testSource.ExpectedLayer.Id);
        
        createdLayer.Regions.First(r => r.Region.AdministrativeRegion.Id == 11).GetUsers().Should().BeEquivalentTo(
            testSource.ExpectedLayer.Regions.First(r => r.Region.AdministrativeRegion.Id == 11).GetUsers(),
            options => options.ComparingByMembers<MapUser>()
                .ComparingByMembers<Haplogroup>());
        
        createdLayer.Regions.First(r => r.Region.AdministrativeRegion.Id == 12).GetUsers().Should().BeEquivalentTo(
            testSource.ExpectedLayer.Regions.First(r => r.Region.AdministrativeRegion.Id == 12).GetUsers(),
            options => options.ComparingByMembers<MapUser>()
                .ComparingByMembers<Haplogroup>());
        
        
        createdLayer.Regions.Should().BeEquivalentTo(
            testSource.ExpectedLayer.Regions,
            options => options
                .ComparingByMembers<MapRegion>()
                .ComparingByMembers<HierarchicalRegion>()
                .ComparingByMembers<AdministrativeRegion>()
        );
    }
    
    [Theory]
    [MemberData(nameof(LayerMapGetRegionTestData))]
    public void LayerMapGetRegionTest(LayerMapGetRegionTestSource testSource) {
        IMap map = new LayerMap(
            layers: testSource.Layers,
            users: testSource.Users,
            haplogroups: testSource.Haplogroups
        );

        var fetchedRegion = map.GetRegion(testSource.ExpectedRegion.Region.AdministrativeRegion.Id);
        fetchedRegion.Region.AdministrativeRegion.Id.Should().Be(testSource.ExpectedRegion.Region.AdministrativeRegion.Id);
        
        fetchedRegion.GetUsers().Should().BeEquivalentTo(
            testSource.ExpectedRegion.GetUsers(),
            options => options.ComparingByMembers<MapUser>()
                .ComparingByMembers<Haplogroup>());

        fetchedRegion.Should().BeEquivalentTo(
            testSource.ExpectedRegion,
            options => options
                .ComparingByMembers<MapRegion>()
                .ComparingByMembers<HierarchicalRegion>()
                .ComparingByMembers<AdministrativeRegion>()
        );
    }
    
    public static IEnumerable<object[]> LayerMapGetLayerTestData() {
        yield return SimpleTest();
    }

    public static IEnumerable<object[]> LayerMapGetRegionTestData() {
        yield return Region11();
        yield return Region12();
        yield return Region13();
    }

    private static object[] SimpleTest() {
        var (haplogroups, users, layers) = CreateMapData();
        return new object[] { new LayerMapGetLayerTestSource(
            users: users,
            haplogroups: haplogroups,
            layers: layers,
            expectedLayer: new MapRegionLayer(
                id: 1,
                regions: new List<MapRegion>() {
                    new MapRegion(
                        region:  new HierarchicalRegion(
                            region: new AdministrativeRegion(11, AdministrativeRegion.RegionType.District, "A1", 111),
                            subregions: new List<AdministrativeRegion>() {
                                new AdministrativeRegion(1, AdministrativeRegion.RegionType.Municipality, "B1", 4),
                                new AdministrativeRegion(2, AdministrativeRegion.RegionType.Municipality, "B2", 4)
                            }
                        ),
                        users: users.Where(user => new int[] {0, 1, 2, 6, 7}.Contains(user.Id)).ToList(),
                        haplogroups: haplogroups
                    ), 
                    new MapRegion(
                        region:   new HierarchicalRegion(
                            region: new AdministrativeRegion(12, AdministrativeRegion.RegionType.District, "A2", 111),
                            subregions: new List<AdministrativeRegion>() {
                                new AdministrativeRegion(3, AdministrativeRegion.RegionType.Municipality, "B3", 5) 
                            }
                        ),
                        users: users.Where(user => new int[] {3, 4, 5}.Contains(user.Id)).ToList(),
                        haplogroups: haplogroups
                    ),
                }
            )
        )};
    }

    private static object[] Region11() {
        var (haplogroups, users, layers) = CreateMapData();
        return new object[] { new LayerMapGetRegionTestSource(
            users: users,
            haplogroups: haplogroups,
            layers: layers,
            expectedRegion: new MapRegion(
                region:  new HierarchicalRegion(
                    region: new AdministrativeRegion(11, AdministrativeRegion.RegionType.District, "A1", 111),
                    subregions: new List<AdministrativeRegion>() {
                        new AdministrativeRegion(1, AdministrativeRegion.RegionType.Municipality, "B1", 4),
                        new AdministrativeRegion(2, AdministrativeRegion.RegionType.Municipality, "B2", 4)
                    }
                ),
                users: users.Where(user => new int[] {0, 1, 2, 6, 7}.Contains(user.Id)).ToList(),
                haplogroups: haplogroups
            )
        )};
    }
    
    private static object[] Region12() {
        var (haplogroups, users, layers) = CreateMapData();
        return new object[] { new LayerMapGetRegionTestSource(
            users: users,
            haplogroups: haplogroups,
            layers: layers,
            expectedRegion: new MapRegion(
                region:   new HierarchicalRegion(
                    region: new AdministrativeRegion(12, AdministrativeRegion.RegionType.District, "A2", 111),
                    subregions: new List<AdministrativeRegion>() {
                        new AdministrativeRegion(3, AdministrativeRegion.RegionType.Municipality, "B3", 5) 
                    }
                ),
                users: users.Where(user => new int[] {3, 4, 5}.Contains(user.Id)).ToList(),
                haplogroups: haplogroups
            )
        )};
    }
    
    private static object[] Region13() {
        var (haplogroups, users, layers) = CreateMapData();
        return new object[] { new LayerMapGetRegionTestSource(
            users: users,
            haplogroups: haplogroups,
            layers: layers,
            expectedRegion: new MapRegion(
                region: new HierarchicalRegion(
                    region: new AdministrativeRegion(13, AdministrativeRegion.RegionType.District, "A3", 111),
                    subregions: new List<AdministrativeRegion>() {
                        new AdministrativeRegion(4, AdministrativeRegion.RegionType.Municipality, "B4", 6)
                }),
                users: users.Where(user => new int[] { 8 }.Contains(user.Id)).ToList(),
                haplogroups: haplogroups
            )
        )};
    }

    private static (List<Haplogroup>, List<MapUser>, List<HierachicalRegionLayer>) CreateMapData() {
        var haplogroups = new List<Haplogroup> {
            new Haplogroup(0, "HAPLO-A"),
            new Haplogroup(1, "HAPLO-B"),
            new Haplogroup(2, "HAPLO-C"),
            new Haplogroup(3, "HAPLO-D"),
        };
        
        var users = new List<MapUser>() {
            new MapUser(id: 0, familyName: "Mrazak", haplogroup: haplogroups[0], regionId: 1, municipality: "MUNI"),
            new MapUser(id: 1, familyName: "Novak", haplogroup: haplogroups[1], regionId: 2, municipality: "PLZEN"),
            new MapUser(id: 2, familyName: "Kozak", haplogroup: haplogroups[1], regionId: 2, municipality: "PLZEN"),
            new MapUser(id: 3, familyName: "Kajak", haplogroup: haplogroups[2], regionId: 3, municipality: "BRNO"),
            new MapUser(id: 4, familyName: "Novak", haplogroup: haplogroups[2], regionId: 3, municipality: "BRNO"),
            new MapUser(id: 5, familyName: "Novak", haplogroup: haplogroups[3], regionId: 3, municipality: "BRNO"),
            new MapUser(id: 6, familyName: "Kolecko", haplogroup: haplogroups[2], regionId: 1, municipality: "MUNI"),
            new MapUser(id: 7, familyName: "Kolecko", haplogroup: haplogroups[2], regionId: 2, municipality: "PLZEN"),
            new MapUser(id: 8, familyName: "Kolecko", haplogroup: haplogroups[2], regionId: 4, municipality: "OSTRAVA"),
        };

        var layers = new List<HierachicalRegionLayer>() {
            new HierachicalRegionLayer(
                id: 1,
                regions: new List<HierarchicalRegion>() {
                    new HierarchicalRegion(
                        region: new AdministrativeRegion(11, AdministrativeRegion.RegionType.District, "A1", 111),
                        subregions: new List<AdministrativeRegion>() {
                            new AdministrativeRegion(1, AdministrativeRegion.RegionType.Municipality, "B1", 4),
                            new AdministrativeRegion(2, AdministrativeRegion.RegionType.Municipality, "B2", 4)
                        }
                    ),
                    new HierarchicalRegion(
                        region: new AdministrativeRegion(12, AdministrativeRegion.RegionType.District, "A2", 111),
                        subregions: new List<AdministrativeRegion>() {
                            new AdministrativeRegion(3, AdministrativeRegion.RegionType.Municipality, "B3", 5) 
                        }
                    ),
                }
            ),
            new HierachicalRegionLayer(
                id: 2,
                regions: new List<HierarchicalRegion>(new List<HierarchicalRegion>() {
                    new HierarchicalRegion(
                        region: new AdministrativeRegion(13, AdministrativeRegion.RegionType.District, "A3", 111),
                        subregions: new List<AdministrativeRegion>() {
                            new AdministrativeRegion(4, AdministrativeRegion.RegionType.Municipality, "B4", 6)
                        })
                }))
        };
        
        return (haplogroups, users, layers);
    }
}