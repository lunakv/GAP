using Users.Exceptions;

namespace Auth.Exceptions; 

public class InvalidAuthStateException : Exception{
	public InvalidAuthStateException(string message) : base(message) {}
}