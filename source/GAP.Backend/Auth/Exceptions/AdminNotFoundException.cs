using Users.Exceptions;

namespace Auth.Exceptions; 

public class AdminNotFoundException : UserException {
	public AdminNotFoundException(string message) : base(message) { }
	public override string GetRepresentation => nameof(AdminNotFoundException);
}