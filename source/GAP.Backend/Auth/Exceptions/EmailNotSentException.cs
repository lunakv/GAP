using Users.Exceptions;

namespace Auth.Exceptions; 

public class EmailNotSentException : UserException {
	public EmailNotSentException(string message) : base(message) { }
	public EmailNotSentException(string message, Exception? innerException) : base(message, innerException) { }
	public override string GetRepresentation => nameof(EmailNotSentException);
}