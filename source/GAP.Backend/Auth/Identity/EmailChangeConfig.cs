namespace Auth.Identity;

public class EmailChangeConfig {
	public const string Name = "EmailChange";
	public string CallbackAddress { get; set; }
	public string UserIdQueryParameter { get; set; }
	public string NewEmailQueryParameter { get; set; }
	public string TokenQueryParameter { get; set; }

	public EmailChangeConfig() {
		CallbackAddress = string.Empty;
		UserIdQueryParameter = string.Empty;
		NewEmailQueryParameter = string.Empty;
		TokenQueryParameter = string.Empty;
	}
}