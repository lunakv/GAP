using Users.Exceptions;

namespace Auth.Identity.Exceptions; 

public class AuthUserNotFoundException : UserException {
	public AuthUserNotFoundException(string message) : base(message) { }
	public AuthUserNotFoundException(string message, Exception? innerException) : base(message, innerException) { }
	public override string GetRepresentation => nameof(AuthUserNotFoundException);
}