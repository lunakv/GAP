using Users.Exceptions;

namespace Auth.Identity.Exceptions; 

public class PasswordMismatchException : UserException {
	public PasswordMismatchException(string message) : base(message) { }
	public PasswordMismatchException(string message, Exception? innerException) : base(message, innerException) { }
	public override string GetRepresentation => nameof(PasswordMismatchException);
}