using Users.Exceptions;

namespace Auth.Identity.Exceptions; 

public class InvalidTokenException : UserException {
	public InvalidTokenException(string message) : base(message) { }
	public InvalidTokenException(string message, Exception? innerException) : base(message, innerException) { }
	public override string GetRepresentation => nameof(InvalidTokenException);
}