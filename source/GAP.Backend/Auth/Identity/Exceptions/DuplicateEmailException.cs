using Users.Exceptions;

namespace Auth.Identity.Exceptions; 

public class DuplicateEmailException : UserException {
	public DuplicateEmailException(string message) : base(message) { }
	public DuplicateEmailException(string message, Exception? innerException) : base(message, innerException) { }
	public override string GetRepresentation => nameof(DuplicateEmailException);
}