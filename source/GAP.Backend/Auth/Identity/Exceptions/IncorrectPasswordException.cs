using Users.Exceptions;

namespace Auth.Identity.Exceptions; 

public class IncorrectPasswordException : UserException {
	public IncorrectPasswordException(string message) : base(message) { }
	public IncorrectPasswordException(string message, Exception? innerException) : base(message, innerException) { }
	public override string GetRepresentation => nameof(IncorrectPasswordException);
}