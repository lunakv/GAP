namespace Auth.Identity; 

public class PasswordResetConfig {
	public const string Name = "PasswordReset";
	public string CallbackAddress { get; set; }
	public string UserIdQueryParameter { get; set; }
	public string TokenQueryParameter { get; set; }

	public PasswordResetConfig() {
		CallbackAddress = string.Empty;
		UserIdQueryParameter = string.Empty;
		TokenQueryParameter = string.Empty;
	}
}
