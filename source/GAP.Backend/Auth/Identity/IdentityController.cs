using System.Security.Claims;
using System.Transactions;
using Auth.Emails;
using Auth.Emails.EmailTextFactories;
using Auth.Identity.Claims;
using Auth.Identity.Entities;
using Auth.Identity.Exceptions;
using Auth.Identity.Storage;
using Auth.Policies.AuthorizationHandlers;
using Auth.Token;
using Users.UserAccess.Exceptions;
using Users.UserAccess.Storage;

namespace Auth.Identity;

/// <summary>
/// Controller which contains business login for identity management which is used by server api endpoints.
/// </summary>
public class IdentityController {
	private readonly IIdentityManager _identityManager;
	private readonly IEmailSender _emailSender;
	private readonly EmailFactory _emailFactory;
	private readonly ITokenCreator _tokenCreator;
	private readonly IUserSaver _userSaver;

	public IdentityController(IIdentityManager identityManager, IEmailSender emailSender, EmailFactory emailFactory, ITokenCreator tokenCreator,
		IUserSaver userSaver) {
		_identityManager = identityManager;
		_emailSender = emailSender;
		_emailFactory = emailFactory;
		_tokenCreator = tokenCreator;
		_userSaver = userSaver;
	}

	public Task ChangePasswordAsync(int userId, string currentPassword, string newPassword) {
		return _identityManager.ChangePasswordAsync(userId: userId, currentPassword: currentPassword, newPassword: newPassword);
	}

	public async Task RequestEmailChangeAsync(int userId, string newEmail, IEmailTextFactory emailTextFactory) {
		string token = await _identityManager.GenerateEmailChangeTokenAsync(userId: userId, newEmail: newEmail).ConfigureAwait(false);
		Email email = _emailFactory.CreateEmailChangeEmail(userId: userId, newEmail: newEmail, token: token, emailTextFactory: emailTextFactory);
		await _emailSender.SendEmailAsync(email: email).ConfigureAwait(false);
	}

	public async Task ChangeEmailAsync(int userId, string newEmail, string token) {
		using TransactionScope transactionScope = new TransactionScope(
			scopeOption: TransactionScopeOption.Required,
			asyncFlowOption: TransactionScopeAsyncFlowOption.Enabled,
			transactionOptions: new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }
		);

		string decodedToken = AuthTokenEncoder.DecodeToken(token);
		await _identityManager.ChangeEmailAsync(userId: userId, newEmail: newEmail, token: decodedToken).ConfigureAwait(false);
		await _userSaver.UpdateEmailAsync(userId: userId, email: newEmail).ConfigureAwait(false);

		transactionScope.Complete();
	}

	public async Task RequestPasswordResetAsync(string email, IEmailTextFactory emailTextFactory) {
		int? userId = await _identityManager.GetUserIdAsync(email: email).ConfigureAwait(false);
		if (userId == null) {
			throw new AuthUserNotFoundException($"User with email {email} not found.");
		}

		string token = await _identityManager.GeneratePasswordResetTokenAsync(userId: userId.Value);
		Email message = _emailFactory.CreatePasswordResetEmail(userId: userId.Value, email: email, token: token, emailTextFactory: emailTextFactory);
		await _emailSender.SendEmailAsync(email: message).ConfigureAwait(false);
	}

	public Task ResetPasswordAsync(int userId, string newPassword, string token) {
		string decodedToken = AuthTokenEncoder.DecodeToken(token);
		return _identityManager.ResetPasswordAsync(userId: userId, newPassword: newPassword, token: decodedToken);
	}

	public Task<ICollection<Staffer>> GetStaffAsync() {
		return _identityManager.GetStaffAsync();
	}

	public async Task DeleteUserAsync(string email) {
		int? userId = await _identityManager.GetUserIdAsync(email: email).ConfigureAwait(false);
		if (userId == null) {
			throw new AuthUserNotFoundException($"User with {email} was not found");
		}

		await _identityManager.DeleteUserAsync(userId: userId.Value).ConfigureAwait(false);
	}
}