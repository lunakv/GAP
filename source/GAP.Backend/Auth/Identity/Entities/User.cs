using Microsoft.AspNetCore.Identity;

namespace Auth.Identity.Entities; 

/// <summary>
/// User class which represents any user registered in auth storage.
/// </summary>
public class User : IdentityUser<int> {
	public User(string userName) : base(userName: userName) {}
}