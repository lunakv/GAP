namespace Auth.Identity.Entities; 

public class Staffer {
	public int Id { get; }
	public StafferProfile Profile { get; }
	public string Role { get; }
	
	public Staffer(int id, StafferProfile profile, string role) {
		Id = id;
		Profile = profile;
		Role = role;
	}
}