namespace Auth.Identity.Entities; 

public class StafferProfile {
	public string Email { get; }

	public StafferProfile(string email) {
		Email = email;
	}
}