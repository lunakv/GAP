using Auth.Identity.Entities;

namespace Auth.Identity.Storage; 

/// <summary>
/// Api for raw access to identity database.
/// </summary>
public interface IAuthUserFetcher {
	Task<IDictionary<int, User>> GetUsersAsync(IReadOnlyCollection<int> userIds);
	Task<IDictionary<string, ICollection<User>>> GetUserWithRolesAsync(IReadOnlyCollection<string> roles);
}