using System.Security.Claims;
using System.Text;
using Auth.Exceptions;
using Auth.Identity.Entities;
using Auth.Identity.Exceptions;
using Auth.Policies;
using Microsoft.AspNetCore.Identity;

namespace Auth.Identity.Storage;

/// <summary>
/// Local (not outsourced but using own storage) implementation of identity manager.
/// It is mainly just a wrapper of <see href="https://learn.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.identity.usermanager-1?view=aspnetcore-6.0"/>.
/// </summary>
public class LocalIdentityManager : IIdentityManager {
	private readonly UserManager<User> _userManager;
	private readonly SignInManager<User> _signInManager;
	private readonly IAuthUserFetcher _authUserFetcher;

	public LocalIdentityManager(UserManager<User> userManager, SignInManager<User> signInManager, IAuthUserFetcher authUserFetcher) {
		_userManager = userManager;
		_signInManager = signInManager;
		_authUserFetcher = authUserFetcher;
	}
	
	public User FindUserById(int userId) {
		try {
			return _userManager.Users.First(u => u.Id == userId);
		} catch (InvalidOperationException ex) {
			throw new AuthUserNotFoundException(message: $"User with id {userId} not found in auth storage.", innerException: ex);
		}
	}

	public async Task<User> CreateUserAsync(string email, string password) {
		var user = new User(userName: email) {
			Email = email
		};

		var userCreatedResult = await _userManager.CreateAsync(user: user, password: password)
			.ConfigureAwait(false);

		if (!userCreatedResult.Succeeded) {
			if (DuplicateUserName(identityResult: userCreatedResult)) {
				throw new DuplicateEmailException($"Email {email} is already taken by another user.");
			} else {
				throw new ArgumentException(
					$"Registration failed for email: {email}, password, errors: {AggregateErrors(userCreatedResult.Errors.ToList())}"
				);
			}
		}

		return await _userManager.FindByNameAsync(userName: user.UserName)
			.ConfigureAwait(false);
	}

	public async Task<User> CreateUserAsync(string email) {
		var user = new User(userName: email) {
			Email = email
		};

		var userCreatedResult = await _userManager.CreateAsync(user: user)
			.ConfigureAwait(false);

		if (!userCreatedResult.Succeeded) {
			if (DuplicateUserName(identityResult: userCreatedResult)) {
				throw new DuplicateEmailException($"Email {email} is already taken by another user.");
			} else {
				throw new ArgumentException(
					$"Registration failed for email: {email}, errors: {AggregateErrors(userCreatedResult.Errors.ToList())}"
				);
			}
		}

		return await _userManager.FindByNameAsync(userName: user.UserName)
			.ConfigureAwait(false);
	}

	public Task AddClaimsAsync(User user, IReadOnlyCollection<Claim> claims) {
		return _userManager.AddClaimsAsync(user: user, claims: claims);
	}

	public Task RemoveIdentityAsync(User user) {
		return _userManager.DeleteAsync(user);
	}

	public async Task<ClaimsPrincipal> LoginAsync(string userName, string password) {
		User? user = await _userManager.FindByEmailAsync(email: userName)
			.ConfigureAwait(false);

		if (user == null) {
			throw new AuthUserNotFoundException($"User with username {userName} was not found");
		}

		SignInResult signInResult = await _signInManager.CheckPasswordSignInAsync(user: user, password: password, lockoutOnFailure: false)
			.ConfigureAwait(false);

		if (signInResult.Succeeded) {
			IList<Claim> claims = await _userManager.GetClaimsAsync(user: user)
				.ConfigureAwait(false);

			return new ClaimsPrincipal(new ClaimsIdentity[] { new ClaimsIdentity(claims: claims) });
		}

		throw new IncorrectPasswordException($"Login failed for user {userName}");
	}

	public Task<string> GenerateEmailConfirmationTokenAsync(int userId) {
		User user = FindUserById(userId: userId);
		return _userManager.GenerateEmailConfirmationTokenAsync(user: user);
	}

	public async Task ConfirmEmailAsync(int userId, string token) {
		User user = FindUserById(userId: userId);
		var identityResult = await _userManager.ConfirmEmailAsync(user: user, token: token);
		if (!identityResult.Succeeded) {
			throw new InvalidTokenException($"Email confirmation failed since token {token} is invalid for user {userId}.");
		}
	}

	public async Task ChangePasswordAsync(int userId, string currentPassword, string newPassword) {
		User user = FindUserById(userId: userId);
		var identityResult = await _userManager.ChangePasswordAsync(user: user, currentPassword: currentPassword, newPassword: newPassword);
		if (!identityResult.Succeeded) {
			throw new PasswordMismatchException(message: $"Password mismatch for user {userId}.");
		}
	}

	public Task<string> GenerateEmailChangeTokenAsync(int userId, string newEmail) {
		User user = FindUserById(userId: userId);
		return _userManager.GenerateChangeEmailTokenAsync(user: user, newEmail: newEmail);
	}

	public async Task ChangeEmailAsync(int userId, string newEmail, string token) {
		User user = FindUserById(userId: userId);
		var identityResult = await _userManager.ChangeEmailAsync(user: user, newEmail: newEmail, token: token).ConfigureAwait(false);
		if (!identityResult.Succeeded) {
			throw new InvalidTokenException($"Changing email to {newEmail} for user {userId} failed due to invalid token {token}.");
		}
	}

	public Task<string> GeneratePasswordResetTokenAsync(int userId) {
		User user = FindUserById(userId: userId);
		return _userManager.GeneratePasswordResetTokenAsync(user: user);
	}

	public async Task ResetPasswordAsync(int userId, string newPassword, string token) {
		User user = FindUserById(userId: userId);
		var identityResult = await _userManager.ResetPasswordAsync(user: user, token: token, newPassword: newPassword).ConfigureAwait(false);
		if (!identityResult.Succeeded) {
			throw new InvalidTokenException($"Resetting password for user {userId} failed due to invalid token {token}.");
		}
	}

	public Task<IList<Claim>> GetClaimsAsync(int userId) {
		User user = FindUserById(userId: userId);
		return _userManager.GetClaimsAsync(user: user);
	}

	public async Task<ICollection<Staffer>> GetStaffAsync() {
		IDictionary<string, ICollection<User>> rolesToUsersTable = await _authUserFetcher.GetUserWithRolesAsync(
			RoleManager.GetStaffRoles().ToList()
		).ConfigureAwait(false);

		List<Staffer> staffers = new List<Staffer>();
		foreach (string role in RoleManager.GetStaffRoles()) {
			staffers.AddRange(
				collection: rolesToUsersTable[role].Select(user => new Staffer(id: user.Id, new StafferProfile(email: user.Email), role: role))
			);
		}
		return staffers;
	}

	public async Task<ICollection<Staffer>> GetStaffAsync(IReadOnlyCollection<int> staffIds) {
		IReadOnlyCollection<int> distinctStafferIds = staffIds.Distinct().ToList();
		ICollection<Staffer> staffers = await GetStaffAsync().ConfigureAwait(false);
		IList<Staffer> foundStaffers = staffers.Where(admin => distinctStafferIds.Contains(admin.Id)).ToList();
		if (foundStaffers.Count != distinctStafferIds.Count) {
			string notFoundStaffers = new StringBuilder().AppendJoin(separator: ",", values: distinctStafferIds.Except(foundStaffers.Select(staffer => staffer.Id))).ToString();
			throw new AdminNotFoundException(
				$"One or more admin ids {notFoundStaffers} do not exist."
			);
		}

		return foundStaffers;
	}

	public async Task<IDictionary<int, string>> GetEmailsAsync(IReadOnlyCollection<int> userIds) {
		IDictionary<int, User> users = await _authUserFetcher.GetUsersAsync(userIds: userIds)
			.ConfigureAwait(false);
		return users.Values.ToDictionary(user => user.Id, user => user.Email);
	}

	public async Task<int?> GetUserIdAsync(string email) {
		User? user = await _userManager.FindByEmailAsync(email: email).ConfigureAwait(false);
		return user?.Id;
	}

	public async Task DeleteUserAsync(int userId) {
		User user = FindUserById(userId: userId);
		IdentityResult identityResult = await _userManager.DeleteAsync(user: user);

		if (!identityResult.Succeeded) {
			throw new ArgumentException($"User {userId} could not be deleted due to {AggregateErrors(identityResult.Errors.ToList())}");
		}
	}

	private string AggregateErrors(ICollection<IdentityError> errors) {
		string codes = string.Join(",", errors.Select(e => e.Code));
		string descriptions = string.Join(",", errors.Select(e => e.Description));
		return $"codes: {codes}; descriptions: {descriptions}";
	}

	private bool DuplicateUserName(IdentityResult identityResult) {
		return identityResult.Errors.Any(error => 
			error.Code == nameof(IdentityErrorDescriber.DuplicateEmail) ||
			error.Code == nameof(IdentityErrorDescriber.DuplicateUserName)
		);
	}
}