using System.Security.Claims;
using Auth.Identity.Entities;

namespace Auth.Identity.Storage;

/// <summary>
/// Api for managing user identity - adding identity, managing user claims, login and password and email changes.
/// </summary>
public interface IIdentityManager {
	User FindUserById(int userId);
	Task<User> CreateUserAsync(string email, string password);
	Task<User> CreateUserAsync(string email);
	Task AddClaimsAsync(User user, IReadOnlyCollection<Claim> claims);
	Task RemoveIdentityAsync(User user);
	Task<ClaimsPrincipal> LoginAsync(string userName, string password);
	Task<string> GenerateEmailConfirmationTokenAsync(int userId);
	Task ConfirmEmailAsync(int userId, string token);
	Task ChangePasswordAsync(int userId, string currentPassword, string newPassword);
	Task<string> GenerateEmailChangeTokenAsync(int userId, string newEmail);
	Task ChangeEmailAsync(int userId, string newEmail, string token);
	Task<string> GeneratePasswordResetTokenAsync(int userId);
	Task ResetPasswordAsync(int userId, string newPassword, string token);
	Task<IList<Claim>> GetClaimsAsync(int userId);

	Task<ICollection<Staffer>> GetStaffAsync();
	Task<ICollection<Staffer>> GetStaffAsync(IReadOnlyCollection<int> staffIds);
	Task<IDictionary<int, string>> GetEmailsAsync(IReadOnlyCollection<int> userIds);

	Task<int?> GetUserIdAsync(string email);
	Task DeleteUserAsync(int userId);
}