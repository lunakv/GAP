namespace Auth.Identity;

public class IdentityConfig {
	public const string Name = "Identity";
	public EmailChangeConfig EmailChange { get; set; }
	public PasswordResetConfig PasswordReset { get; set; }

	public IdentityConfig() {
		EmailChange = new EmailChangeConfig();
		PasswordReset = new PasswordResetConfig();
	}
}