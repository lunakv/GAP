using System.Security.Claims;
using Auth.Identity.Entities;

namespace Auth.Identity.Claims;

/// <summary>
/// Class for creating standard claims which should any user have.
/// </summary>
public class ClaimCreator {
	/// <summary>
	/// Create claims which any user should have.
	/// </summary>
	/// <param name="role">Role for role claim</param>
	/// <param name="user">User for user id claim</param>
	/// <returns>List of created claims</returns>
	public IList<Claim> CreateClaims(string role, User user) {
		return new List<Claim>() {
			new Claim(type: ClaimTypes.Role, value: role),
			new Claim(type: CustomClaimTypes.UserId, value: user.Id.ToString())
		};
	}
}