using System.Security.Claims;
using Auth.Exceptions;
using Auth.Policies;

namespace Auth.Identity.Claims;

/// <summary>
/// Provides functionality for parsing ClaimsPrincipal which is usually taken from the security token.
/// </summary>
public class ClaimsRetriever {
	public int GetClaimUserId(ClaimsPrincipal user) {
		string? claimUserIdValue = user.Claims
			.FirstOrDefault(claim => claim is { Type: CustomClaimTypes.UserId }, defaultValue: null)?.Value;
		if (int.TryParse(claimUserIdValue, out int claimUserId)) {
			return claimUserId;
		}

		throw new InvalidAuthStateException(@"User id not found in ClaimsPrincipal (from security token) which should never happen.");
	}

	public bool HasSignedAdministrationAgreement(ClaimsPrincipal user) {
		string? administrationAgreementSignedRaw = user.Claims
			.FirstOrDefault(claim => claim is { Type: CustomClaimTypes.AdministrationAgreementSigned }, defaultValue: null)?.Value;

		if (bool.TryParse(administrationAgreementSignedRaw, out bool administrationAgreementSigned)) {
			return administrationAgreementSigned;
		}

		throw new InvalidAuthStateException(
			$"User {GetClaimUserId(user: user)} had incorrect value {administrationAgreementSignedRaw} in {CustomClaimTypes.AdministrationAgreementSigned} claim."
		);
	}

	public bool IsAdmin(ClaimsPrincipal user) {
		Claim? adminRoleClaim = user.Claims.FirstOrDefault(claim => claim is { Type: ClaimTypes.Role, Value: nameof(AdminRole) },
			defaultValue: null);

		adminRoleClaim ??= user.Claims.FirstOrDefault(claim => claim is { Type: ClaimTypes.Role, Value: nameof(RootRole) },
			defaultValue: null);
		
		return adminRoleClaim != null;
	}

	public bool IsRoot(ClaimsPrincipal user) {
		Claim? rootRoleClaim = user.Claims.FirstOrDefault(claim => claim is { Type: ClaimTypes.Role, Value: nameof(RootRole) },
			defaultValue: null);

		return rootRoleClaim != null;
	}
	
	public bool IsLabTech(ClaimsPrincipal user) {
		Claim? labTechRoleClaim = user.Claims.FirstOrDefault(claim => claim is { Type: ClaimTypes.Role, Value: nameof(LabTechRole) },
			defaultValue: null);

		return labTechRoleClaim != null;
	}
}