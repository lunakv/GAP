namespace Auth.Identity.Claims; 

public class CustomClaimTypes {
	public const string UserId = "UserId";
	public const string AdministrationAgreementSigned = "AdministrationAgreementSigned";
}