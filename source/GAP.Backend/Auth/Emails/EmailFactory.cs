using System.Text;
using System.Text.Encodings.Web;
using Auth.Emails.EmailTextFactories;
using Auth.Identity;
using Auth.Registration;
using Auth.Token;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;

namespace Auth.Emails;
/// <summary>
/// Generates emails with callback links using <see cref="IEmailTextFactory"/> for multiple language support.
/// </summary>
public class EmailFactory {
	private readonly EmailConfirmationConfig _emailConfirmationConfig;
	private readonly EmailChangeConfig _emailChangeConfig;
	private readonly PasswordResetConfig _passwordResetConfig;

	public EmailFactory(IOptions<EmailConfirmationConfig> emailConfirmationConfig, IOptions<EmailChangeConfig> emailChangeConfig,
		IOptions<PasswordResetConfig> passwordResetConfig) {
		_emailConfirmationConfig = emailConfirmationConfig.Value;
		_emailChangeConfig = emailChangeConfig.Value;
		_passwordResetConfig = passwordResetConfig.Value;
	}

	/// <summary>
	/// Create email for email confirmation. It is sent to the user with callback url which upon clicking can confirm email.
	/// </summary>
	/// <param name="userId">User whose email can be confirmed</param>
	/// <param name="email">Email to be confirmed and the receiver</param>
	/// <param name="token">Token which is sent back to backend via query</param>
	/// <param name="emailTextFactory">Provides generic email text</param>
	/// <returns></returns>
	public Email CreateEmailConfirmationEmail(int userId, string email, string token, IEmailTextFactory emailTextFactory) {
		string emailConfirmationToken = AuthTokenEncoder.EncodeToken(token: token);
		string callbackUrl = $"{_emailConfirmationConfig.CallbackAddress}" +
		                     $"?{_emailConfirmationConfig.UserIdQueryParameter}={userId}" +
		                     $"&{_emailConfirmationConfig.ConfirmationTokenQueryParameter}={emailConfirmationToken}";
		return new Email(
			to: email,
			subject: emailTextFactory.CreateEmailConfirmationSubject(),
			body: emailTextFactory.CreateEmailConfirmationBody(callbackUrl: HtmlEncoder.Default.Encode(callbackUrl))
		);
	}
	
	/// <summary>
	/// Creates email for changing email.
	/// </summary>
	/// <param name="userId">User whose email can be changed</param>
	/// <param name="newEmail">Receiver email</param>
	/// <param name="token">Token which is sent back to backend</param>
	/// <param name="emailTextFactory">Provider genetic email text</param>
	/// <returns></returns>
	public Email CreateEmailChangeEmail(int userId, string newEmail, string token, IEmailTextFactory emailTextFactory) {
		string emailChangeToken = AuthTokenEncoder.EncodeToken(token: token);
		string callbackUrl = $"{_emailChangeConfig.CallbackAddress}" +
		                     $"?{_emailChangeConfig.UserIdQueryParameter}={userId}" +
		                     $"&{_emailChangeConfig.NewEmailQueryParameter}={newEmail}" +
		                     $"&{_emailChangeConfig.TokenQueryParameter}={emailChangeToken}";
		return new Email(
			to: newEmail,
			subject: emailTextFactory.CreateEmailChangeSubject(),
			body: emailTextFactory.CreateEmailChangeBody(callbackUrl: HtmlEncoder.Default.Encode(callbackUrl))
		);
	}

	/// <summary>
	/// Creates email for resetting email.
	/// </summary>
	/// <param name="userId">User whose password can be reset</param>
	/// <param name="email">Receiver for password reset email</param>
	/// <param name="token">Token to be sent back to backend</param>
	/// <param name="emailTextFactory">Provides generic email text</param>
	/// <returns></returns>
	public Email CreatePasswordResetEmail(int userId, string email, string token, IEmailTextFactory emailTextFactory) {
		string passwordResetToken = AuthTokenEncoder.EncodeToken(token: token);
		string callbackUrl = $"{_passwordResetConfig.CallbackAddress}" +
		                     $"?{_passwordResetConfig.UserIdQueryParameter}={userId}" +
		                     $"&{_passwordResetConfig.TokenQueryParameter}={passwordResetToken}";
		return new Email(
			to: email,
			subject: emailTextFactory.CreatePasswordResetSubject(),
			body: emailTextFactory.CreatePasswordResetBody(callbackUrl: HtmlEncoder.Default.Encode(callbackUrl))
		);
	}
}