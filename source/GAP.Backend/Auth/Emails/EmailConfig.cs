namespace Auth.Emails;

/// <summary>
/// Configuration for connecting to mail server.
/// </summary>
public class EmailConfig {
	public string DisplayName { get; set; }
	public string From { get; set; }
	public string UserName { get; set; }
	public string Password { get; set; }
	public string Host { get; set; }
	public int Port { get; set; }
	public bool UseSsl { get; set; }
	public bool UseStartTls { get; set; }

	public EmailConfig() {
		DisplayName = string.Empty;
		From = string.Empty;
		UserName = string.Empty;
		Password = string.Empty;
		Host = string.Empty;
		Port = 0;
		UseSsl = false;
		UseStartTls = false;
	}
}