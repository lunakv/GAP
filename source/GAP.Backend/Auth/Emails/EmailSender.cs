using Auth.Exceptions;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using Users.Exceptions;

namespace Auth.Emails;

/// <summary>
/// Real implementation for sending emails using <see href="https://github.com/jstedfast/MailKit">MailKit</see>.
/// </summary>
public class EmailSender : IEmailSender {
	private readonly EmailConfig _config;
	private readonly ILogger<EmailSender> _logger;

	public EmailSender(IOptions<EmailConfig> config, ILogger<EmailSender> logger) {
		_config = config.Value;
		_logger = logger;
	}

	/// <inheritdoc/>
	public async Task SendEmailAsync(Email email) {
		try {
			var message = new MimeMessage();

			message.From.Add(new MailboxAddress(name: _config.DisplayName, address: email.From ?? _config.From));
			message.Sender = new MailboxAddress(name: email.DisplayName ?? _config.DisplayName, address: email.From ?? _config.From);

			foreach (string mailAddress in email.To) {
				message.To.Add(MailboxAddress.Parse(mailAddress));
			}

			if (!string.IsNullOrEmpty(email.ReplyTo)) {
				message.ReplyTo.Add(new MailboxAddress(name: email.ReplyToName, address: email.ReplyTo));
			}

			FillReceiversInCopy(email: email, message: message);

			var body = new BodyBuilder();
			message.Subject = email.Subject;
			body.HtmlBody = email.Body;
			message.Body = body.ToMessageBody();

			using var smtpClient = new SmtpClient();

			await ConnectToSmtpClientAsync(smtpClient: smtpClient).ConfigureAwait(false);

			await smtpClient.AuthenticateAsync(userName: _config.UserName, password: _config.Password);
			await smtpClient.SendAsync(message: message);
			await smtpClient.DisconnectAsync(quit: true);
		} catch (Exception ex) {
			_logger.LogError("Mail {@Mail} failed to be sent. Exception: {@Exception}", email, ex);
			throw new EmailNotSentException(message: $"Mail to {ExceptionUtils.CollectionToString(email.To)} failed to be sent.", innerException: ex);
		}
	}

	private void FillReceiversInCopy(Email email, MimeMessage message) {
		if (email.Bcc.Any()) {
			foreach (string mailAddress in email.Bcc.Where(x => !string.IsNullOrWhiteSpace(x))) {
				message.Bcc.Add(MailboxAddress.Parse(mailAddress.Trim()));
			}
		}

		if (email.Cc.Any()) {
			foreach (string mailAddress in email.Cc.Where(x => !string.IsNullOrWhiteSpace(x))) {
				message.Cc.Add(MailboxAddress.Parse(mailAddress.Trim()));
			}
		}
	}

	private async Task ConnectToSmtpClientAsync(SmtpClient smtpClient) {
		if (_config.UseSsl) {
			await smtpClient.ConnectAsync(host: _config.Host, port: _config.Port, options: SecureSocketOptions.SslOnConnect);
		} else if (_config.UseStartTls) {
			await smtpClient.ConnectAsync(host: _config.Host, port: _config.Port, options: SecureSocketOptions.StartTls);
		} else {
			await smtpClient.ConnectAsync(host: _config.Host, port: _config.Port, options: SecureSocketOptions.None);
		}
	}
}