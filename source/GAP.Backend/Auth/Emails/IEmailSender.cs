namespace Auth.Emails; 

public interface IEmailSender {
	/// <summary>
	/// Send email using SMTP.
	/// </summary>
	/// <param name="email">Email to send</param>
	/// <exception cref="EmailNotSentException">Email failed to be sent</exception>
	/// <returns>Task indicating if email was sent</returns>
	Task SendEmailAsync(Email email);
}