namespace Auth.Emails.EmailTextFactories;

/// <summary>
/// Api for providing email content. The implementations should be in different languages of the same email content.
/// </summary>
public interface IEmailTextFactory {
	string CreateEmailConfirmationSubject();
	string CreateEmailConfirmationBody(string callbackUrl);
	string CreateEmailChangeSubject();
	string CreateEmailChangeBody(string callbackUrl);
	string CreatePasswordResetSubject();
	string CreatePasswordResetBody(string callbackUrl);
}