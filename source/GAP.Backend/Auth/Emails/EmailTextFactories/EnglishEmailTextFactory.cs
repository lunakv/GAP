namespace Auth.Emails.EmailTextFactories;

/// <summary>
/// Class for providing english (en) email content.
/// </summary>
public class EnglishEmailTextFactory : IEmailTextFactory {
	public string CreateEmailConfirmationSubject() => "Email Confirmation";

	public string CreateEmailConfirmationBody(string callbackUrl)
		=> $"Confirm your email by <a href='{callbackUrl}'>clicking on this.</a>";
	
	public string CreateEmailChangeSubject() => "Email Change";

	public string CreateEmailChangeBody(string callbackUrl)
		=> $"Change your email by <a href='{callbackUrl}'>clicking on this.</a>";

	public string CreatePasswordResetSubject() => "Password Reset";

	public string CreatePasswordResetBody(string callbackUrl)
		=> $"Reset your password by <a href='{callbackUrl}'>clicking on this.</a>";
}