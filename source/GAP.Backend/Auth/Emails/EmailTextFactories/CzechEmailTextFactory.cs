namespace Auth.Emails.EmailTextFactories; 

/// <summary>
/// Class for providing czech (cs) email content.
/// </summary>
public class CzechEmailTextFactory : IEmailTextFactory {
	public string CreateEmailConfirmationSubject() => "Potvrzení emailu";

	public string CreateEmailConfirmationBody(string callbackUrl)
		=> $"Potvrďte svůj email <a href='{callbackUrl}'>kliknutím na tento odkaz.</a>";
	
	public string CreateEmailChangeSubject() => "Změna emailu";

	public string CreateEmailChangeBody(string callbackUrl)
		=> $"Změnte svůj email <a href='{callbackUrl}'>kliknutím na tento odkaz.</a>";

	public string CreatePasswordResetSubject() => "Resetování hesla";

	public string CreatePasswordResetBody(string callbackUrl)
		=> $"Resetujte své heslo <a href='{callbackUrl}'>kliknutím na tento odkaz.</a>";
}