namespace Auth.Policies;

/// <summary>
/// Only users who are managers of the user whose data are accessed can call the endpoint where this policy is applied.
/// The users themselves must have all agreement signed.
///
/// ROOT has unrestricted access.
/// </summary>
public class HasUserManageRightPolicy { }

/// <summary>
/// Only users who are viewers of the user whose data are accessed can call the endpoint where this policy is applied.
/// The users themselves must have all agreement signed.
///
/// ROOT has unrestricted access.
/// </summary>
public class HasUserViewRightPolicy { }

/// <summary>
/// Only root users can access the endpoint where this policy is applied.
///
/// ROOT has unrestricted access.
/// </summary>
public class IsRootPolicy { }

/// <summary>
/// Only admin and root users can access the endpoint where this policy is applied.
///
/// ROOT has unrestricted access.
/// </summary>
public class IsAdminPolicy { }

/// <summary>
/// Any UI user (admin, user, root, labTech) can access the endpoint where this policy is applied.
/// The users themselves must have all agreement signed.
///
/// ROOT has unrestricted access.
/// </summary>
public class IsUiUserPolicy { }

/// <summary>
/// Users with access to lab (admin, root, labTech) can access the endpoint where this policy is applied.
///
/// ROOT has unrestricted access.
/// </summary>
public class HasLabAccessPolicy { }

/// <summary>
/// Users with access to public api (admin, root, publicApiConsumer) can access the endpoint where this policy is applied.
///
/// ROOT has unrestricted access.
/// </summary>
public class HasPublicApiAccessPolicy { }


/// <summary>
/// Only standard users who access their own data or any admin (admin, root) can access the endpoint where this policy is applied.
/// The users themselves must have all agreement signed.
///
/// ROOT has unrestricted access.
/// </summary>
public class IsUserThemselfOrAdminPolicy { }

/// <summary>
/// Only standard users who access their own data or any admin (admin, root) can access the endpoint where this policy is applied.
/// The standard users do not need to have signed administration agreement.
///
/// This is the only exception where ROOT cannot use endpoints with this policy!
/// </summary>
public class IsUserThemselfWithoutSignedAdministrationAgreementPolicy { }

/// <summary>
/// Users (any - admin, standard user, lab tech, ...) who access their own data can access the endpoint where this policy is applied.
/// The users themselves must have all agreement signed.
///
/// ROOT has unrestricted access.
/// </summary>
public class IsAuthUserThemselfPolicy { }

/// <summary>
/// Only standard users who access their own data can access the endpoint where this policy is applied.
/// The users themselves must have all agreement signed.
///
/// ROOT has unrestricted access.
/// </summary>
public class IsUserThemselfPolicy { }