namespace Auth.Policies; 

public class RoleManager {
	public static IList<string> GetStaffRoles() {
		return new List<string>() { nameof(RootRole), nameof(AdminRole), nameof(LabTechRole) };
	}
}