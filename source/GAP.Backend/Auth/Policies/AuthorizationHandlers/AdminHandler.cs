using System.Security.Claims;
using Auth.Identity;
using Auth.Identity.Claims;
using Auth.Policies.AuthorizationRequirements;
using HotChocolate.Resolvers;
using Microsoft.AspNetCore.Authorization;

namespace Auth.Policies.AuthorizationHandlers;

/// <summary>
/// Satisfies requirements for admin user.
/// </summary>
public class AdminHandler : IAuthorizationHandler {
	public Task HandleAsync(AuthorizationHandlerContext context) {
		IList<IAuthorizationRequirement> pendingRequirements = context.PendingRequirements.ToList();

		if (context.Resource is IResolverContext resolverContext) {
			foreach (IAuthorizationRequirement requirement in pendingRequirements) {
				if (requirement.GetType() == typeof(UserManagerRequirement) ||
				    requirement.GetType() == typeof(UserViewerRequirement) ||
				    requirement.GetType() == typeof(HasSignedAgreementRequirement) ||
				    requirement.GetType() == typeof(IsUserThemselfOrAdminRequirement)) {
					HandleRequirement(context: context, requirement: requirement, resolverContext: resolverContext);
				}
			}
		}

		return Task.CompletedTask;
	}

	private void HandleRequirement(AuthorizationHandlerContext context, IAuthorizationRequirement requirement, IResolverContext resolverContext) {
		if (new ClaimsRetriever().IsAdmin(context.User)) {
			context.Succeed(requirement);
		}
	}
}