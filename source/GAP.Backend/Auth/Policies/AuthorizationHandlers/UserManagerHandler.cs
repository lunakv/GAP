using System.Security.Claims;
using Auth.Identity;
using Auth.Identity.Claims;
using Auth.Policies.AuthorizationRequirements;
using HotChocolate.Resolvers;
using Microsoft.AspNetCore.Authorization;
using Users.UserSharing.UserManagementSharing;
using Users.UserSharing.UserManagementSharing.Storage;

namespace Auth.Policies.AuthorizationHandlers;

/// <summary>
/// If standard user is manager of another user and accesses their data in read or write manner,
/// this handler satisfies corresponding requirement.
/// </summary>
public class UserManagerHandler : IAuthorizationHandler {
	private readonly IUserManagementSettingsFetcher _userManagementSettingsFetcher;

	public UserManagerHandler(IUserManagementSettingsFetcher userManagementSettingsFetcher) {
		_userManagementSettingsFetcher = userManagementSettingsFetcher;
	}

	public async Task HandleAsync(AuthorizationHandlerContext context) {
		IList<IAuthorizationRequirement> pendingRequirements = context.PendingRequirements.ToList();

		if (context.Resource is IResolverContext resolverContext) {
			foreach (IAuthorizationRequirement requirement in pendingRequirements) {
				if (requirement.GetType() == typeof(UserManagerRequirement) ||
				    requirement.GetType() == typeof(UserViewerRequirement)) {
					await HandleRequirementAsync(context: context, requirement: requirement, resolverContext: resolverContext);
				}
			}
		}
	}

	private async Task HandleRequirementAsync(AuthorizationHandlerContext context, IAuthorizationRequirement requirement, IResolverContext resolverContext) {
		int? userIdArgumentValue = new GraphqlResolverContextParser(resolverContext: resolverContext).GetUserIdArgumentValue();
		int connectingUserId = new ClaimsRetriever().GetClaimUserId(context.User);

		if (userIdArgumentValue.HasValue) {
			UserManagementSettings userManagementSettings = await _userManagementSettingsFetcher.GetUserManagementSettingsAsync(userId: connectingUserId);
			if (userManagementSettings.IsUserManageable(userIdArgumentValue.Value)) {
				context.Succeed(requirement);
			}
		}
	}
}