using HotChocolate.Language;
using HotChocolate.Resolvers;

namespace Auth.Policies.AuthorizationHandlers;

public class GraphqlResolverContextParser {
	private readonly IResolverContext _resolverContext;
	public const string UserIdArgumentName = "userId";

	public GraphqlResolverContextParser(IResolverContext resolverContext) {
		_resolverContext = resolverContext;
	}

	public int? GetUserIdArgumentValue() {
		FieldNode selection = _resolverContext.Selection.SyntaxNode;

		IValueNode? valueNode = FindUserIdArgumentValueNode(argumentsNodes: selection.Arguments, userId: UserIdArgumentName);

		if (valueNode is VariableNode userIdVariableNode) {
			valueNode = GetVariableValue(variableNode: userIdVariableNode);
		}

		if (valueNode is IntValueNode intValueNode) {
			if (int.TryParse(intValueNode.Value, out int userId)) {
				return userId;
			}
		}

		return null;
	}

	private IValueNode? FindUserIdArgumentValueNode(IReadOnlyList<ArgumentNode> argumentsNodes, string userId) {
		return argumentsNodes
			.Where(arg => arg.Name.Value == userId)
			.FirstOrDefault(defaultValue: null)?.Value;
	}

	private IValueNode? GetVariableValue(VariableNode variableNode) {
		var legitVariableNodes = _resolverContext.Variables.Where(v => v.Name == variableNode.Value).ToList();
		if (legitVariableNodes.Count == 1) {
			return legitVariableNodes[0].Value;
		}

		return null;
	}
}