using System.Security.Claims;
using Auth.Identity;
using Auth.Identity.Claims;
using Auth.Policies.AuthorizationRequirements;
using HotChocolate.Resolvers;
using Microsoft.AspNetCore.Authorization;
using Users.UserSharing.UserViewSharing;
using Users.UserSharing.UserViewSharing.Storage;

namespace Auth.Policies.AuthorizationHandlers;

/// <summary>
/// If standard user is viewer of another user and accesses their data in read manner,
/// this handler satisfies corresponding requirement.
/// </summary>
public class UserViewerHandler : IAuthorizationHandler {
	private readonly IUserViewSettingsFetcher _userViewSettingsFetcher;

	public UserViewerHandler(IUserViewSettingsFetcher userViewSettingsFetcher) {
		_userViewSettingsFetcher = userViewSettingsFetcher;
	}

	public async Task HandleAsync(AuthorizationHandlerContext context) {
		IList<IAuthorizationRequirement> pendingRequirements = context.PendingRequirements.ToList();

		if (context.Resource is IResolverContext resolverContext) {
			foreach (IAuthorizationRequirement requirement in pendingRequirements) {
				if (requirement.GetType() == typeof(UserViewerRequirement)) {
					await HandleRequirementAsync(context: context, requirement: requirement, resolverContext: resolverContext);
				}
			}
		}
	}

	private async Task HandleRequirementAsync(AuthorizationHandlerContext context, IAuthorizationRequirement requirement, IResolverContext resolverContext) {
		int? userIdArgumentValue = new GraphqlResolverContextParser(resolverContext: resolverContext).GetUserIdArgumentValue();
		int connectingUserId = new ClaimsRetriever().GetClaimUserId(context.User);

		if (userIdArgumentValue.HasValue) {
			UserViewSettings userViewSettings = await _userViewSettingsFetcher.GetUserViewSettingsAsync(userId: connectingUserId);
			if (userViewSettings.IsUserViewable(userIdArgumentValue.Value)) {
				context.Succeed(requirement);
			}
		}
	}
}