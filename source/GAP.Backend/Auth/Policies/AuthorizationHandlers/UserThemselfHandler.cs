using System.Security.Claims;
using Auth.Identity;
using Auth.Identity.Claims;
using Auth.Policies.AuthorizationRequirements;
using HotChocolate.Language;
using HotChocolate.Resolvers;
using HotChocolate.Utilities;
using Microsoft.AspNetCore.Authorization;

namespace Auth.Policies.AuthorizationHandlers;

/// <summary>
/// If user performs action about their account, this handler satisfies requirements.
/// </summary>
public class UserThemselfHandler : IAuthorizationHandler {
	public Task HandleAsync(AuthorizationHandlerContext context) {
		IList<IAuthorizationRequirement> pendingRequirements = context.PendingRequirements.ToList();

		if (context.Resource is IResolverContext resolverContext) {
			foreach (IAuthorizationRequirement requirement in pendingRequirements) {
				if (requirement.GetType() == typeof(UserManagerRequirement) ||
				    requirement.GetType() == typeof(UserViewerRequirement) ||
				    requirement.GetType() == typeof(IsUserThemselfOrAdminRequirement) ||
				    requirement.GetType() == typeof(IsAuthUserThemselfRequirement)) {
					HandleRequirement(context: context, requirement: requirement, resolverContext: resolverContext);
				}
			}
		}

		return Task.CompletedTask;
	}

	private void HandleRequirement(AuthorizationHandlerContext context, IAuthorizationRequirement requirement, IResolverContext resolverContext) {
		int claimUserId = new ClaimsRetriever().GetClaimUserId(context.User);
		var resolverContextParser = new GraphqlResolverContextParser(resolverContext: resolverContext);
		int? userId = resolverContextParser.GetUserIdArgumentValue();

		if (userId.HasValue) {
			if (claimUserId == userId.Value) {
				context.Succeed(requirement);
			}
		}
	}
}