using Auth.Identity;
using Auth.Identity.Claims;
using Auth.Policies.AuthorizationRequirements;
using HotChocolate.Resolvers;
using Microsoft.AspNetCore.Authorization;

namespace Auth.Policies.AuthorizationHandlers; 

/// <summary>
/// Satisfies requirement for lab tech.
/// </summary>
public class LabTechHandler  : IAuthorizationHandler {
	public Task HandleAsync(AuthorizationHandlerContext context) {
		IList<IAuthorizationRequirement> pendingRequirements = context.PendingRequirements.ToList();

		if (context.Resource is IResolverContext resolverContext) {
			foreach (IAuthorizationRequirement requirement in pendingRequirements) {
				if (requirement.GetType() == typeof(HasSignedAgreementRequirement)) {
					HandleRequirement(context: context, requirement: requirement, resolverContext: resolverContext);
				}
			}
		}

		return Task.CompletedTask;
	}

	private void HandleRequirement(AuthorizationHandlerContext context, IAuthorizationRequirement requirement, IResolverContext resolverContext) {
		if (new ClaimsRetriever().IsLabTech(context.User)) {
			context.Succeed(requirement);
		}
	}
}