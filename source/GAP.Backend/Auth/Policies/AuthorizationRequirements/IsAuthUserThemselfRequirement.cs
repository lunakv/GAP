using Microsoft.AspNetCore.Authorization;

namespace Auth.Policies.AuthorizationRequirements;

/// <summary>
/// Represents a condition that the user accessing the endpoint under this requirement must be accessing or mutating
/// their own data.
/// </summary>
public class IsAuthUserThemselfRequirement : IAuthorizationRequirement { }