using Microsoft.AspNetCore.Authorization;

namespace Auth.Policies.AuthorizationRequirements;

/// <summary>
/// Represents a requirement that the user accessing the endpoint with this requirement
/// must be viewer (read rights) of the user whose data are accessed.
/// </summary>
public class UserViewerRequirement : IAuthorizationRequirement { }