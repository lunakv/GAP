using Microsoft.AspNetCore.Authorization;

namespace Auth.Policies.AuthorizationRequirements;

/// <summary>
/// Represents a requirement that the user accessing the endpoint under this requirement must be accessing or mutating
/// their own data or be an admin.
/// </summary>
public class IsUserThemselfOrAdminRequirement : IAuthorizationRequirement { }