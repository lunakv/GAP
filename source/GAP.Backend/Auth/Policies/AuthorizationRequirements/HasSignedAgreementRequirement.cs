using Microsoft.AspNetCore.Authorization;

namespace Auth.Policies.AuthorizationRequirements;

/// <summary>
/// Represents a requirement that the user accesing the endpoint under this requirement must have signed an agreement. For a standard user
/// this means that they have signed the administration agreement.
/// </summary>
public class HasSignedAgreementRequirement : IAuthorizationRequirement { }