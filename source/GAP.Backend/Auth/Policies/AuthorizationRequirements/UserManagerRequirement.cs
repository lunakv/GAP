using Microsoft.AspNetCore.Authorization;

namespace Auth.Policies.AuthorizationRequirements;

/// <summary>
/// Represents a requirement that the user accessing the endpoint with this requirement
/// must be manager (write, read rights) of the user whose data are accessed or mutated.
/// </summary>
public class UserManagerRequirement : IAuthorizationRequirement { }