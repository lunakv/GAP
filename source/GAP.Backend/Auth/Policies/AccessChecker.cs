using System.Security.Claims;
using Auth.Identity;
using Auth.Identity.Claims;
using Auth.Policies.AuthorizationHandlers;
using Users.UserSharing.UserManagementSharing;
using Users.UserSharing.UserManagementSharing.Storage;
using Users.UserSharing.UserViewSharing;
using Users.UserSharing.UserViewSharing.Storage;

namespace Auth.Policies;

public class AccessChecker {
	private readonly IUserManagementSettingsFetcher _userManagementSettingsFetcher;
	private readonly IUserViewSettingsFetcher _userViewSettingsFetcher;

	public AccessChecker(IUserManagementSettingsFetcher userManagementSettingsFetcher, IUserViewSettingsFetcher userViewSettingsFetcher) {
		_userManagementSettingsFetcher = userManagementSettingsFetcher;
		_userViewSettingsFetcher = userViewSettingsFetcher;
	}

	public bool IsAdmin(ClaimsPrincipal accessingUser) {
		bool admin = new ClaimsRetriever().IsAdmin(accessingUser);
		if (admin) {
			return true;
		}

		return false;
	}

	public async Task<bool> HasViewAccessAsync(ClaimsPrincipal accessingUser, int accessedUserId) {
		if (IsAdmin(accessingUser)) {
			return true;
		}

		int claimUserId = new ClaimsRetriever().GetClaimUserId(accessingUser);
		if (claimUserId == accessedUserId) {
			return true;
		}

		UserManagementSettings userManagementSettings = await _userManagementSettingsFetcher.GetUserManagementSettingsAsync(userId: claimUserId);
		if (userManagementSettings.IsUserManageable(accessedUserId)) {
			return true;
		}

		UserViewSettings userViewSettings = await _userViewSettingsFetcher.GetUserViewSettingsAsync(userId: claimUserId);
		if (userViewSettings.IsUserViewable(accessedUserId)) {
			return true;
		}

		return false;
	}
}