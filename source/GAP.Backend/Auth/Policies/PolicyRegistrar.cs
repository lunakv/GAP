using Microsoft.AspNetCore.Authorization;

namespace Auth.Policies;

/// <summary>
/// Is responsible for adding authorization requirements to policies. Also adds the policies to authorization middleware.
/// </summary>
public class PolicyRegistrar {
	public static void RegisterPolicies(AuthorizationOptions options) {
		options.AddPolicy(nameof(HasUserManageRightPolicy), policy => {
			policy.Requirements.Add(new AuthorizationRequirements.UserManagerRequirement());
			policy.Requirements.Add(new AuthorizationRequirements.HasSignedAgreementRequirement());
			policy.RequireRole(nameof(UserRole), nameof(AdminRole), nameof(RootRole));
		});
		options.AddPolicy(nameof(HasUserViewRightPolicy), policy => {
			policy.Requirements.Add(new AuthorizationRequirements.UserViewerRequirement());
			policy.Requirements.Add(new AuthorizationRequirements.HasSignedAgreementRequirement());
			policy.RequireRole(nameof(UserRole), nameof(AdminRole), nameof(RootRole));
		});
		options.AddPolicy(nameof(IsUserThemselfOrAdminPolicy), policy => {
			policy.Requirements.Add(new AuthorizationRequirements.IsUserThemselfOrAdminRequirement());
			policy.Requirements.Add(new AuthorizationRequirements.HasSignedAgreementRequirement());
			policy.RequireRole(nameof(UserRole), nameof(AdminRole), nameof(RootRole));
		});
		options.AddPolicy(nameof(IsUserThemselfWithoutSignedAdministrationAgreementPolicy), policy => {
			policy.Requirements.Add(new AuthorizationRequirements.IsAuthUserThemselfRequirement());
			policy.RequireRole(nameof(UserRole));
		});
		options.AddPolicy(nameof(IsAuthUserThemselfPolicy), policy => {
			policy.Requirements.Add(new AuthorizationRequirements.IsAuthUserThemselfRequirement());
			policy.Requirements.Add(new AuthorizationRequirements.HasSignedAgreementRequirement());
			policy.RequireRole(nameof(AdminRole), nameof(RootRole), nameof(UserRole), nameof(LabTechRole));
		});
		options.AddPolicy(nameof(IsUserThemselfPolicy), policy => {
			policy.Requirements.Add(new AuthorizationRequirements.IsAuthUserThemselfRequirement());
			policy.Requirements.Add(new AuthorizationRequirements.HasSignedAgreementRequirement());
			policy.RequireRole(nameof(UserRole), nameof(RootRole));
		});
		options.AddPolicy(nameof(IsRootPolicy), policy =>
			policy.RequireRole(nameof(RootRole))
		);
		options.AddPolicy(nameof(IsAdminPolicy), policy =>
			policy.RequireRole(nameof(AdminRole), nameof(RootRole))
		);
		options.AddPolicy(nameof(IsUiUserPolicy), policy => {
			policy.Requirements.Add(new AuthorizationRequirements.HasSignedAgreementRequirement());
			policy.RequireRole(nameof(AdminRole), nameof(RootRole), nameof(UserRole), nameof(LabTechRole));
		});
		options.AddPolicy(nameof(HasLabAccessPolicy), policy =>
			policy.RequireRole(nameof(AdminRole), nameof(LabTechRole), nameof(RootRole))
		);
		options.AddPolicy(nameof(HasPublicApiAccessPolicy), policy =>
			policy.RequireRole(nameof(AdminRole), nameof(PublicApiConsumerRole), nameof(RootRole))
		);
	}
}