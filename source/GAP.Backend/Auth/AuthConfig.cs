using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Builder;
using Microsoft.IdentityModel.Tokens;

namespace Auth; 

public class AuthConfig {

	private const char Days = 'd';
	private const char Hours = 'h';
	private const char Minutes = 'm';
	private readonly string _tokenExpirationPattern = $"([0-9])+[{Days}{Hours}{Minutes}]";  

	public string TokenExpiration { get; set; }
	public string SymmetricKey { get; set; }

	public AuthConfig() {
		TokenExpiration = string.Empty;
		SymmetricKey = string.Empty;
	}
	
	public TimeSpan GetTokenExpiration() {
		CheckTokenExpirationFormat(TokenExpiration);
		char timeMeasure = TokenExpiration[^1];
		
		TimeSpan tokenExpiration;
		
		switch (timeMeasure) {
			case Days:
				tokenExpiration = new TimeSpan(days: int.Parse(TokenExpiration[..^1]), hours: 0, minutes: 0, seconds: 0);
				break;
			case Hours:
				tokenExpiration = new TimeSpan(days: 0, hours: int.Parse(TokenExpiration[..^1]), minutes: 0, seconds: 0);
				break;
			case Minutes:
				tokenExpiration = new TimeSpan(days: 0, hours: 0, minutes:  int.Parse(TokenExpiration[..^1]), seconds: 0);
				break;
			default:
				throw new AuthConfigurationException($"TokenExpiration configuration value {TokenExpiration} has invalid format: it must be ${_tokenExpirationPattern}.");
		}

		return tokenExpiration;
	}

	

	private void CheckTokenExpirationFormat(string tokenExpiration) {
		if (!new Regex(_tokenExpirationPattern, RegexOptions.CultureInvariant).IsMatch(tokenExpiration)) {
			throw new AuthConfigurationException($"TokenExpiration configuration value {TokenExpiration} has invalid format: it must be ${_tokenExpirationPattern}");
		}
	}
	
}