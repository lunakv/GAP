using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Auth.Token;

/// <summary>
/// Creates, validates and refreshes JWT tokens for authentication and authorization.
/// </summary>
public class JwtTokenCreator : ITokenCreator {
	private readonly SigningCredentials _signingCredentials;
	private readonly AuthConfig _config;

	public JwtTokenCreator(IOptions<AuthConfig> config) {
		_config = config.Value;
		var key = Encoding.UTF8.GetBytes(config.Value.SymmetricKey);
		_signingCredentials = new SigningCredentials(
			key: new SymmetricSecurityKey(key),
			algorithm: SecurityAlgorithms.HmacSha256Signature
		);
	}

	public static TokenValidationParameters GetTokenValidationParameters(string issuerSigningKey) {
		return new TokenValidationParameters() {
			ValidateIssuer = false,
			ValidateAudience = false,
			ValidateLifetime = true,
			ValidateIssuerSigningKey = true,
			IssuerSigningKey = new SymmetricSecurityKey(
				Encoding.UTF8.GetBytes(issuerSigningKey)
			)
		};
	}

	public string CreateSerializedToken(IReadOnlyCollection<Claim> claims) {
		var tokenHandler = new JwtSecurityTokenHandler();
		var tokenDescriptor = new SecurityTokenDescriptor {
			Subject = new ClaimsIdentity(claims: claims),
			Expires = DateTime.Now.Add(_config.GetTokenExpiration()),
			SigningCredentials = _signingCredentials,
		};
		JwtSecurityToken token = tokenHandler.CreateJwtSecurityToken(tokenDescriptor);
		return tokenHandler.WriteToken(token);
	}

	public string RefreshToken(string token) {
		ClaimsPrincipal principal = ParseToken(token: token);

		return CreateSerializedToken(
			claims: principal.Claims.ToList()
		);
	}

	public ClaimsPrincipal ParseToken(string token) {
		var tokenHandler = new JwtSecurityTokenHandler();
		ClaimsPrincipal principal = tokenHandler.ValidateToken(
			token,
			validationParameters: GetTokenValidationParameters(_config.SymmetricKey),
			validatedToken: out SecurityToken securityToken
		);

		if (securityToken is not JwtSecurityToken jwtSecurityToken ||
		    !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase)) {
			throw new SecurityTokenException("Invalid token");
		}

		return principal;
	}
}