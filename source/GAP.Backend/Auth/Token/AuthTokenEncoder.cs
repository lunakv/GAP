using System.Text;
using Auth.Identity.Exceptions;
using Microsoft.AspNetCore.WebUtilities;

namespace Auth.Token; 

/// <summary>
/// Provides methods for encoding tokens to a form which can be added to URL. Also is capable of decoding the tokens back.
/// </summary>
public class AuthTokenEncoder {
	public static string EncodeToken(string token) {
		return WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(token));
	}
	
	public static string DecodeToken(string token) {
		try {
			return Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(token));
		} catch (FormatException ex) {
			throw new InvalidTokenException($"Token {token} has invalid format", innerException: ex);
		}
	}
}