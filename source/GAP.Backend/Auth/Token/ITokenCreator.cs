using System.Security.Claims;

namespace Auth.Token; 

/// <summary>
/// Provides security token creation, validation and refresh.
/// </summary>
public interface ITokenCreator {
	string CreateSerializedToken(IReadOnlyCollection<Claim> claims);
	string RefreshToken(string token);
	ClaimsPrincipal ParseToken(string token);
}