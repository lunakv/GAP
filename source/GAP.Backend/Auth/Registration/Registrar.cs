using System.Security.Claims;
using System.Transactions;
using Auth.Emails;
using Auth.Emails.EmailTextFactories;
using Auth.Identity;
using Auth.Identity.Claims;
using Auth.Identity.Storage;
using Auth.Policies;
using Auth.Registration.Objects;
using Auth.Registration.RegistrationDataSources;
using Auth.Registration.Storage;
using Laboratory.KitManagement.Kits;
using Laboratory.KitManagement.Storage;
using Users.GenData;
using Users.UserAccess.Entities;
using Users.UserAccess.Entities.Interfaces;
using Users.UserAccess.Storage;
using User = Auth.Identity.Entities.User;

namespace Auth.Registration;

/// <summary>
/// Main class for providing registration for standard users and admins. It has two parts: Register user and save data, Confirm email.
/// </summary>
public class Registrar {
	private readonly IIdentityManager _identityManager;
	private readonly IRegistrationDataSaver _registrationDataSaver;
	private readonly IRegistrationDataFetcher _registrationDataFetcher;
	private readonly IEmailSender _emailSender;
	private readonly EmailFactory _emailFactory;
	private readonly ClaimCreator _claimCreator;
	private readonly IUserSaver _userSaver;
	private readonly IKitSaver<IHasUserId> _kitSaver;
	private readonly GeneticDataUploader _geneticDataUploader;
	
	public IEmailTextFactory EmailTextFactory { get; set; }

	public Registrar(IIdentityManager identityManager, IRegistrationDataSaver registrationDataSaver, IRegistrationDataFetcher registrationDataFetcher,
		IEmailSender emailSender, EmailFactory emailFactory, ClaimCreator claimCreator, IUserSaver userSaver, IKitSaver<IHasUserId> kitSaver,
		GeneticDataUploader geneticDataUploader) {
		_identityManager = identityManager;
		_registrationDataSaver = registrationDataSaver;
		_registrationDataFetcher = registrationDataFetcher;
		_emailSender = emailSender;
		_emailFactory = emailFactory;
		_claimCreator = claimCreator;
		_userSaver = userSaver;
		_kitSaver = kitSaver;
		_geneticDataUploader = geneticDataUploader;
		EmailTextFactory = new EnglishEmailTextFactory();
	}

	/// <summary>
	/// Registered standard user with genetic data.
	/// </summary>
	/// <param name="registrationUser">User data for registration</param>
	/// <param name="registrationDataSources">Optional user data that can be added to <see cref="RegistrationData"/></param>
	/// <param name="sendConfirmationEmail">Whether to send confirmatio email</param>
	/// <returns>Registration result</returns>
	public async Task<RegistrationResult> RegisterAsync(RegistrationUser registrationUser, IReadOnlyCollection<IRegistrationDataSource> registrationDataSources,
		bool sendConfirmationEmail = true) {
		using TransactionScope transactionScope = new TransactionScope(
			scopeOption: TransactionScopeOption.Required,
			asyncFlowOption: TransactionScopeAsyncFlowOption.Enabled,
			transactionOptions: new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }
		);

		
		User identityUser = registrationUser.Password != null
			? await _identityManager.CreateUserAsync(email: registrationUser.Profile.Email, password: registrationUser.Password).ConfigureAwait(false)
			: await _identityManager.CreateUserAsync(email: registrationUser.Profile.Email).ConfigureAwait(false);

		await _identityManager.AddClaimsAsync(
			user: identityUser,
			claims: _claimCreator.CreateClaims(role: nameof(UserRole), user: identityUser).ToList()
		).ConfigureAwait(false);

		var registrationData = new RegistrationData(userId: identityUser.Id, user: registrationUser);
		foreach (var registrationDataSource in registrationDataSources) {
			await registrationDataSource.AddToAsync(registrationData: registrationData).ConfigureAwait(false);
		}

		await _registrationDataSaver.AddRegistrationDataAsync(registrationData: registrationData).ConfigureAwait(false);

		string emailConfirmationToken = await SendEmailAsync(
			userId: registrationData.UserId,
			email: registrationData.User.Profile.Email,
			sendConfirmationEmail: sendConfirmationEmail
		).ConfigureAwait(false);

		transactionScope.Complete();
		return new RegistrationResult(userId: identityUser.Id, emailConfirmationToken: emailConfirmationToken);
	}

	/// <summary>
	/// Confirm email for standard user or any admin account.
	/// </summary>
	/// <param name="userId">User who to confirm email for</param>
	/// <param name="token">Confirmation Token</param>
	public async Task ConfirmEmailAsync(int userId, string token) {
		using TransactionScope transactionScope = new TransactionScope(
			scopeOption: TransactionScopeOption.Required,
			asyncFlowOption: TransactionScopeAsyncFlowOption.Enabled,
			transactionOptions: new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }
		);
		await _identityManager.ConfirmEmailAsync(userId: userId, token: token);

		IList<Claim> claims = await _identityManager.GetClaimsAsync(userId: userId).ConfigureAwait(false);
		string role = claims.First(claim => claim is { Type: ClaimTypes.Role }).Value;

		if (role == nameof(UserRole)) {
			RegistrationData registrationData = await _registrationDataFetcher.GetRegistrationDataAsync(userId: userId);
			await _userSaver.AddUserAsync(user: registrationData.User.ToUser(registrationData.UserId)).ConfigureAwait(false);
			if (registrationData.UploadedStrDataSource != null) {
				if (registrationData.Haplogroup != null) {
					await _geneticDataUploader.AddStrMarkersWithPredictedHaplogroupAsync(
						originalStrDataSource: registrationData.UploadedStrDataSource,
						haplogroup: registrationData.Haplogroup
					).ConfigureAwait(false);
				} else {
					await _geneticDataUploader.AddOriginalStrDataSourceNoTransactionAsync(newOriginalStrDataSource: registrationData.UploadedStrDataSource)
						.ConfigureAwait(false);
				}
			}

			if (registrationData.LabTestRequested) {
				var pendingKit = new PendingKit<IHasUserId>(new IdUser(id: registrationData.UserId));
				await _kitSaver.AddKitAsync(kit: pendingKit).ConfigureAwait(false);
			}

			await _registrationDataSaver.DeleteRegistrationDataAsync(userId: registrationData.UserId).ConfigureAwait(false);
		}
		
		transactionScope.Complete();
	}

	/// <summary>
	/// Create account for given role.
	/// </summary>
	/// <param name="email">New user account email</param>
	/// <param name="role">New user role</param>
	/// <param name="password">New password</param>
	/// <param name="sendConfirmationEmail">Whether to send confirmation email</param>
	/// <returns>Registration result</returns>
	public async Task<RegistrationResult> CreateAccount(string email, string role, string? password = null, bool sendConfirmationEmail = true) {
		using TransactionScope transactionScope = new TransactionScope(
			scopeOption: TransactionScopeOption.Required,
			asyncFlowOption: TransactionScopeAsyncFlowOption.Enabled,
			transactionOptions: new TransactionOptions() { IsolationLevel = IsolationLevel.ReadCommitted }
		);

		User identityUser = password != null 
			? await _identityManager.CreateUserAsync(email: email, password: password).ConfigureAwait(false)
			: await _identityManager.CreateUserAsync(email: email).ConfigureAwait(false);
		await _identityManager.AddClaimsAsync(
			user: identityUser,
			claims: _claimCreator.CreateClaims(role: role, user: identityUser).ToList()
		).ConfigureAwait(false);

		string emailConfirmationToken = await SendEmailAsync(
			userId: identityUser.Id,
			email: email,
			sendConfirmationEmail: sendConfirmationEmail
		).ConfigureAwait(false);

		transactionScope.Complete();

		return new RegistrationResult(
			userId: identityUser.Id,
			emailConfirmationToken: emailConfirmationToken
		);
	}

	private async Task<string> SendEmailAsync(int userId, string email, bool sendConfirmationEmail) {
		string emailConfirmationToken = await _identityManager.GenerateEmailConfirmationTokenAsync(userId).ConfigureAwait(false);
		if (sendConfirmationEmail) {
			Email message = _emailFactory.CreateEmailConfirmationEmail(
				userId: userId,
				email: email,
				token: emailConfirmationToken,
				emailTextFactory: EmailTextFactory
			);
			await _emailSender.SendEmailAsync(email: message).ConfigureAwait(false);
		}

		return emailConfirmationToken;
	}
}