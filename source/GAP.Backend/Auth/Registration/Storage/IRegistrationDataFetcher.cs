using Auth.Registration.Objects;

namespace Auth.Registration.Storage; 

/// <summary>
/// Api for fetching registration data <see cref="RegistrationData"/> from persistent storage.
/// </summary>
public interface IRegistrationDataFetcher {
	Task<RegistrationData> GetRegistrationDataAsync(int userId);
}