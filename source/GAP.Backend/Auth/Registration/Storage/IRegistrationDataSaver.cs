using Auth.Registration.Objects;

namespace Auth.Registration.Storage; 

/// <summary>
/// Api for saving registration data <see cref="RegistrationData"/> to persistent storage.
/// </summary>
public interface IRegistrationDataSaver {
	Task AddRegistrationDataAsync(RegistrationData registrationData);
	Task DeleteRegistrationDataAsync(int userId);
}