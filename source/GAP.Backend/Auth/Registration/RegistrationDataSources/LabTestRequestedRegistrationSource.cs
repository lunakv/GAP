using Auth.Registration.Objects;

namespace Auth.Registration.RegistrationDataSources;

/// <summary>
/// Class for adding request for lab test to registration data <see cref="RegistrationData"/>.
/// </summary>
public class LabTestRequestedRegistrationSource : IRegistrationDataSource {
	private readonly bool _labTestRequested;

	public LabTestRequestedRegistrationSource(bool labTestRequested) {
		_labTestRequested = labTestRequested;
	}

	/// <summary>
	/// Adds request for lab test to registration data <see cref="RegistrationData"/>.
	/// </summary>
	/// <param name="registrationData">Where to add the request</param>
	public Task AddToAsync(RegistrationData registrationData) {
		registrationData.LabTestRequested = _labTestRequested;
		return Task.CompletedTask;
	}
}