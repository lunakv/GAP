using Auth.Registration.Objects;

namespace Auth.Registration.RegistrationDataSources; 

/// <summary>
/// Api for adding optional user data to registration data <see cref="RegistrationData"/>.
/// </summary>
public interface IRegistrationDataSource {
	public Task AddToAsync(RegistrationData registrationData);
}