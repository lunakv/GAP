using Auth.Registration.Objects;
using Users.GenData;
using Users.GenData.OriginalStrDataSources;
using Users.GenData.OriginalStrDataSources.Storage.File;
using Users.GenData.Parsers;

namespace Auth.Registration.RegistrationDataSources;

/// <summary>
/// Class for adding genetic data to registration data <see cref="RegistrationData"/>.
/// </summary>
public class ExternalStrDataRegistrationSource : IRegistrationDataSource {
	private readonly GeneticDataUploader _geneticDataUploader;
	private readonly Stream _strFile;
	private readonly GeneticTestProvider _testProvider;
	private readonly string _fileExtension;
	private readonly StrMarkerCollection<string, int> _strMarkers;

	public ExternalStrDataRegistrationSource(GeneticDataUploader geneticDataUploader, Stream strFile, GeneticTestProvider testProvider, string fileExtension,
		StrMarkerCollection<string, int> strMarkers) {
		_geneticDataUploader = geneticDataUploader;
		_strFile = strFile;
		_testProvider = testProvider;
		_fileExtension = fileExtension;
		_strMarkers = strMarkers;
	}

	/// <summary>
	/// Add original str data source to registration data.
	/// </summary>
	/// <param name="registrationData">Where to add original str data source</param>
	public async Task AddToAsync(RegistrationData registrationData) {
		string fileName = await _geneticDataUploader.UploadExternalStrFileAsync(file: _strFile, userId: registrationData.UserId, fileExtension: _fileExtension);
		registrationData.UploadedStrDataSource = new OriginalStrDataSource(
			userId: registrationData.UserId,
			testProvider: _testProvider,
			strMarkers: _strMarkers,
			fileName: fileName
		);
	}
}