using Auth.Registration.Exceptions;
using Map.MapFetcher.Storage;
using Map.ValueObjects;
using Users.UserAccess;

namespace Auth.Registration; 

/// <summary>
/// Class for mapping municipality names to known supported administrative regions which can be further used e.g. in haplogroup map. 
/// </summary>
public class MunicipalityMapper : IMunicipalityMapper {
	private readonly IRegionFetcher _regionFetcher;

	public MunicipalityMapper(IRegionFetcher regionFetcher) {
		_regionFetcher = regionFetcher;
	}

	/// <summary>
	/// Map municipality name to its id.
	/// </summary>
	/// <param name="municipality">Name of municipality to map</param>
	/// <returns>Id of the municipality</returns>
	public async Task<int> GetMunicipalityRegionIdAsync(string municipality) {
		AdministrativeRegion region = await _regionFetcher.GetMunicipalityRegionByName(municipalityName: municipality).ConfigureAwait(false);
		return region.Id;
	}

	/// <summary>
	/// Find county for given municipality.
	/// </summary>
	/// <param name="municipalityId">Id of given municipality</param>
	/// <returns>County which contains the municipality</returns>
	/// <exception cref="CountyNotFoundException">No county found for the municipality</exception>
	public async Task<string> FindCorrespondingCountyAsync(int municipalityId) {
		IList<AdministrativeRegion> regionHierarchy = await _regionFetcher.GetParentRegionsAsync(unitRegionId: municipalityId).ConfigureAwait(false);
		AdministrativeRegion? county = regionHierarchy.FirstOrDefault(region => region.Type == AdministrativeRegion.RegionType.County);
		if (county != null) {
			return county.Name;
		}

		throw new CountyNotFoundException(message: $"No county region found from municipality witd id {municipalityId}.");
	}
}