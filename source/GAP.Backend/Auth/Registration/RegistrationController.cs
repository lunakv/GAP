using System.Security.Claims;
using Auth.Emails;
using Auth.Emails.EmailTextFactories;
using Auth.Identity;
using Auth.Policies;
using Auth.Registration.Exceptions;
using Auth.Registration.Objects;
using Auth.Registration.RegistrationDataSources;
using Auth.Token;
using Users.GenData;
using Users.GenData.Parsers;
using Users.UserAccess;
using Users.UserAccess.Entities;
using Users.UserAccess.PublicIds;
using Users.UserAccess.Validation;
using RegistrationUserValidator = Auth.Registration.Validation.RegistrationUserValidator;

namespace Auth.Registration;

/// <summary>
/// Controller which provides business logic for registration which is used from Api controllers.
/// </summary>
public class RegistrationController {
	private readonly Registrar _registrar;
	private readonly GeneticDataUploader _geneticDataUploader;
	private readonly IMunicipalityMapper _municipalityMapper;
	private readonly IPublicUserIdGenerator _publicUserIdGenerator;

	public RegistrationController(Registrar registrar, GeneticDataUploader geneticDataUploader, IMunicipalityMapper municipalityMapper, IPublicUserIdGenerator publicUserIdGenerator) {
		_registrar = registrar;
		_geneticDataUploader = geneticDataUploader;
		_municipalityMapper = municipalityMapper;
		_publicUserIdGenerator = publicUserIdGenerator;
	}

	public async Task<RegistrationResult> RegisterUserWithoutExternalDataAsync(RegistrationUserInput registrationUserInput, IEmailTextFactory emailTextFactory) {
		CheckSignedAdministrationAgreement(registrationUserInput: registrationUserInput);
		RegistrationUser registrationUser = await CreateRegistrationUserAsync(registrationUserInput: registrationUserInput).ConfigureAwait(false);
		await ValidationWrapper.ValidateAsync(() => new RegistrationUserValidator().ValidateAsync(registrationUser));
		_registrar.EmailTextFactory = emailTextFactory;
		RegistrationResult registrationResult = await _registrar.RegisterAsync(
			registrationUser: registrationUser,
			registrationDataSources: new List<IRegistrationDataSource>() {
				new LabTestRequestedRegistrationSource(labTestRequested: true)
			}
		);

		return registrationResult;
	}

	public async Task<RegistrationResult> RegisterUserWithExternalDataAsync(RegistrationUserInput registrationUserInput, Stream strFile,
		IExternalStrFileParser externalStrFileParser, IEmailTextFactory emailTextFactory) {
		CheckSignedAdministrationAgreement(registrationUserInput: registrationUserInput);
		StrParsingResult parsingResult = await externalStrFileParser.ParseAsync(file: strFile).ConfigureAwait(false);

		RegistrationUser registrationUser = await CreateRegistrationUserAsync(registrationUserInput: registrationUserInput).ConfigureAwait(false);
		await ValidationWrapper.ValidateAsync(() => new RegistrationUserValidator().ValidateAsync(registrationUser));
		
		_registrar.EmailTextFactory = emailTextFactory;
		RegistrationResult registrationResult = await _registrar.RegisterAsync(
			registrationUser: registrationUser,
			registrationDataSources: new List<IRegistrationDataSource>() {
				new ExternalStrDataRegistrationSource(
					geneticDataUploader: _geneticDataUploader,
					strFile: strFile,
					testProvider: parsingResult.GeneticTestProvider,
					fileExtension: parsingResult.FileExtension,
					strMarkers: parsingResult.StrMarkers
				),
				new LabTestRequestedRegistrationSource(labTestRequested: registrationUserInput.LabTestRequested)
			}
		).ConfigureAwait(false);
		return registrationResult;
	}

	public Task ConfirmEmailAsync(int userId, string token) {
		return _registrar.ConfirmEmailAsync(userId: userId, token: AuthTokenEncoder.DecodeToken(token));
	}

	public Task<RegistrationResult> CreateAccount(string email, string role, IEmailTextFactory emailTextFactory) {
		_registrar.EmailTextFactory = emailTextFactory;
		return _registrar.CreateAccount(email: email, role: role, sendConfirmationEmail: true);
	}

	private async Task<RegistrationUser> CreateRegistrationUserAsync(RegistrationUserInput registrationUserInput) {
		if (registrationUserInput.Profile.ResidenceAddress.Municipality == null) {
			throw new MunicipalityNullException($"Municipality for user {registrationUserInput.Profile.Email} is null when registering.");
		}

		int municipalityId = await _municipalityMapper.GetMunicipalityRegionIdAsync(municipality: registrationUserInput.Profile.ResidenceAddress.Municipality)
			.ConfigureAwait(false);

		string county = await _municipalityMapper.FindCorrespondingCountyAsync(municipalityId: municipalityId).ConfigureAwait(false);

		var profileWithCounty = new Profile(
			givenName: registrationUserInput.Profile.GivenName,
			familyName: registrationUserInput.Profile.FamilyName,
			email: registrationUserInput.Profile.Email,
			phoneNumber: registrationUserInput.Profile.PhoneNumber,
			residenceAddress: new ExpandedAddress(
				town: registrationUserInput.Profile.ResidenceAddress.Town,
				municipality: registrationUserInput.Profile.ResidenceAddress.Municipality,
				county: county,
				street: registrationUserInput.Profile.ResidenceAddress.Street,
				zipCode: registrationUserInput.Profile.ResidenceAddress.ZipCode
			),
			correspondenceAddress: registrationUserInput.Profile.CorrespondenceAddress,
			birthDate: registrationUserInput.Profile.BirthDate
		);

		return new RegistrationUser(
			administrationAgreementSigned: registrationUserInput.AdministrationAgreementSigned,
			publicId: _publicUserIdGenerator.GetNextId(),
			profile: profileWithCounty,
			ancestor: registrationUserInput.Ancestor,
			regionId: municipalityId,
			password: registrationUserInput.Password
		);
	}

	private void CheckSignedAdministrationAgreement(RegistrationUserInput registrationUserInput) {
		if (!registrationUserInput.AdministrationAgreementSigned) {
			throw new AdministrationAgreementNotSignedException($"User {registrationUserInput} did not sign administration agreement.");
		}
		
	}
}