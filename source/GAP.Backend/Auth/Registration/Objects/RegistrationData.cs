using Users.GenData.Haplogroup;
using Users.GenData.OriginalStrDataSources;

namespace Auth.Registration.Objects;

/// <summary>
/// Object for storing all user data when registering before email is confirmed.
/// </summary>
public class RegistrationData {
	public int UserId { get; }
	public RegistrationUser User { get; }
	public bool LabTestRequested { get; set; }
	public OriginalStrDataSource? UploadedStrDataSource { get; set; }
	public Haplogroup? Haplogroup { get; set; }

	public RegistrationData(int userId, RegistrationUser user) {
		UserId = userId;
		User = user;
		LabTestRequested = false;
		UploadedStrDataSource = null;
		Haplogroup = null;
	}
	
	public RegistrationData(int userId, RegistrationUser user, bool labTestRequested, OriginalStrDataSource? uploadedStrDataSource, Haplogroup? haplogroup) {
		UserId = userId;
		User = user;
		LabTestRequested = labTestRequested;
		UploadedStrDataSource = uploadedStrDataSource;
		Haplogroup = haplogroup;
	}
}