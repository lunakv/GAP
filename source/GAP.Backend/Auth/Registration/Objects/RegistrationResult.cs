namespace Auth.Registration.Objects;

public class RegistrationResult {
	public int UserId { get; }
	public string EmailConfirmationToken { get; }

	public RegistrationResult(int userId, string emailConfirmationToken) {
		UserId = userId;
		EmailConfirmationToken = emailConfirmationToken;
	}
}