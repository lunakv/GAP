using Users.UserAccess.Entities;

namespace Auth.Registration.Objects;

/// <summary>
/// Represents user which can be standalone registered in the system.
/// </summary>
public class RegistrationUser {
	public bool AdministrationAgreementSigned { get; }
	public string PublicId { get; }
	public Profile Profile { get; }
	public Ancestor Ancestor { get; }
	public int RegionId { get; }
	public string? Password { get; }

	public RegistrationUser(bool administrationAgreementSigned, string publicId, Profile profile, Ancestor ancestor, int regionId, string? password) {
		AdministrationAgreementSigned = administrationAgreementSigned;
		Profile = profile;
		Ancestor = ancestor;
		RegionId = regionId;
		PublicId = publicId;
		Password = password;
	}

	public User ToUser(int id) {
		return new User(
			id: id,
			administrationAgreementSigned: AdministrationAgreementSigned,
			publicId: PublicId,
			profile: Profile,
			ancestor: Ancestor,
			regionId: RegionId,
			geneticData: null,
			administratorData: new AdministratorData()
		);
	}
}