using Users.UserAccess.Entities;

namespace Auth.Registration.Objects;

/// <summary>
/// Represents the input which is provided by the human user. The input can be transformed to <see cref="RegistrationUser"/>
/// and registrated.
/// </summary>
public class RegistrationUserInput {
	public bool AdministrationAgreementSigned { get; }
	public Profile Profile { get; set; }
	public Ancestor Ancestor { get; set; }
	public string? Password { get; set; }
	public bool LabTestRequested { get; set; }

	public RegistrationUserInput(bool administrationAgreementSigned, Profile profile, Ancestor ancestor, string? password, bool labTestRequested) {
		AdministrationAgreementSigned = administrationAgreementSigned;
		Profile = profile;
		Ancestor = ancestor;
		Password = password;
		LabTestRequested = labTestRequested;
	}
}