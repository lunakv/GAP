namespace Auth.Registration; 

public class RegistrationConfig {
	public const string Name = "Registration";
	
	public EmailConfirmationConfig EmailConfirmation { get; set; }

	public RegistrationConfig() {
		EmailConfirmation = new EmailConfirmationConfig();
	}
	
}