using Users.Exceptions;

namespace Auth.Registration.Exceptions; 

public class AdministrationAgreementNotSignedException : UserException {
	public AdministrationAgreementNotSignedException(string message) : base(message) { }
	public AdministrationAgreementNotSignedException(string message, Exception? innerException) : base(message, innerException) { }
	public override string GetRepresentation => nameof(AdministrationAgreementNotSignedException);
}