namespace Auth.Registration.Exceptions;

public class CountyNotFoundException : Exception {
	public CountyNotFoundException(string message) : base(message) { }
}