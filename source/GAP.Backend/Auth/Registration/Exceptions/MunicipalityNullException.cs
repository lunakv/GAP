using Users.Exceptions;

namespace Auth.Registration.Exceptions; 

public class MunicipalityNullException : UserException {
	public MunicipalityNullException(string message) : base(message) { }
	public MunicipalityNullException(string message, Exception? innerException) : base(message, innerException) { }
	public override string GetRepresentation => nameof(MunicipalityNullException);
}