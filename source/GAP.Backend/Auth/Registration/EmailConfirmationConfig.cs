namespace Auth.Registration; 

public class EmailConfirmationConfig {
	public const string Name = "EmailConfirmation";
	public string CallbackAddress { get; set; }
	public string UserIdQueryParameter { get; set; }
	public string ConfirmationTokenQueryParameter { get; set; }
	
	public EmailConfirmationConfig() {
		CallbackAddress = string.Empty;
		UserIdQueryParameter = string.Empty;;
		ConfirmationTokenQueryParameter = string.Empty;
	}
}