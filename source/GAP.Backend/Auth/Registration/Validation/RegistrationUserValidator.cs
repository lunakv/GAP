using Auth.Registration.Objects;
using FluentValidation;
using Users.UserAccess.Entities;
using Users.UserAccess.Validation;

namespace Auth.Registration.Validation; 

/// <summary>
/// Validation definition for <see cref="RegistrationUser"/>.
/// </summary>
public class RegistrationUserValidator : AbstractValidator<RegistrationUser> {
	public RegistrationUserValidator() {
		RuleFor(registrationUser => registrationUser.Profile).SetValidator(new ProfileValidator());
		RuleFor(registrationUser => registrationUser.Ancestor).SetValidator(new AncestorValidator());
	}
}