namespace Auth; 

public class AuthConfigurationException : Exception {
	public AuthConfigurationException(string msg) : base(msg) {}
}