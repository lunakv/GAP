namespace Auth.Login; 

public class LoginResult {
	public int UserId { get; }
	public string Token { get; }
	public string Role { get; }

	public LoginResult(int userId, string token, string role) {
		UserId = userId;
		Token = token;
		Role = role;
	}
	
}