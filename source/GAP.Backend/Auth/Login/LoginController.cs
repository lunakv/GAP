using System.Security.Claims;
using Auth.Identity;
using Auth.Identity.Claims;
using Auth.Identity.Storage;
using Auth.Policies;
using Auth.Token;
using Microsoft.Extensions.Options;
using Users.UserAccess.Storage;

namespace Auth.Login;

/// <summary>
/// Controller for business logic for login which is used from server api endpoints.
/// </summary>
public class LoginController {
	private readonly ITokenCreator _tokenCreator;
	private readonly IIdentityManager _identityManager;
	private readonly IUserFetcher _userFetcher;
	private readonly AuthConfig _config;

	public LoginController(ITokenCreator tokenCreator, IIdentityManager identityManager, IUserFetcher userFetcher, IOptions<AuthConfig> config) {
		_tokenCreator = tokenCreator;
		_identityManager = identityManager;
		_userFetcher = userFetcher;
		_config = config.Value;
	}

	/// <summary>
	/// Login using password.
	/// </summary>
	/// <param name="userName">Username of user to log in</param>
	/// <param name="password">Password to log in</param>
	/// <returns>Login Result</returns>
	public async Task<LoginResult> LoginByPasswordAsync(string userName, string password) {
		var claimsPrincipal = await _identityManager.LoginAsync(userName: userName, password: password)
			.ConfigureAwait(false);
		int userId = int.Parse(claimsPrincipal.Claims.First(c => c.Type == CustomClaimTypes.UserId).Value);
		List<Claim> claims = await PrepareClaimsForTokenAsync(claimsPrincipal: claimsPrincipal, userId: userId).ConfigureAwait(false);

		string token = _tokenCreator.CreateSerializedToken(claims: claims);

		return new LoginResult(userId: userId, token: token, role: claims.First(claim => claim.Type == ClaimTypes.Role).Value);
	}

	public async Task<string> GenerateLoginToken(int userId) {
		var claims = await _identityManager.GetClaimsAsync(userId: userId);
		var user = await _userFetcher.FindUserByIdAsync(userId: userId).ConfigureAwait(false);
		claims.Add(new Claim(
			type: CustomClaimTypes.AdministrationAgreementSigned, value: user.AdministrationAgreementSigned.ToString()
		));

		return _tokenCreator.CreateSerializedToken(claims: claims.ToList());
	}

	private async Task<List<Claim>> PrepareClaimsForTokenAsync(ClaimsPrincipal claimsPrincipal, int userId) {
		List<Claim> claims = claimsPrincipal.Claims.ToList();
		if (claimsPrincipal.HasClaim(type: ClaimTypes.Role, value: nameof(UserRole))) {
			var user = await _userFetcher.FindUserByIdAsync(userId: userId).ConfigureAwait(false);
			claims.Add(new Claim(
				type: CustomClaimTypes.AdministrationAgreementSigned, value: user.AdministrationAgreementSigned.ToString()
			));
		}

		return claims;
	}
}