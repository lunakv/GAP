﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyTree; 
public record FamilyTreePersonData(

    string? GivenName,
    string? FamilyName,
    string? BirthDate,
    string? DeathDate,
    string? Avatar,
    Gender? Gender
);
