﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyTree; 
public record FamilyTreeNode
(
    string Id,
    FamilyTreePersonData PersonData,
    IList<string> Partners,
    IList<string> Children,
    string? FatherId,
    string? MotherId
);
