﻿using System.Text;
using System.Text.Json;

namespace FamilyTree.Access; 
internal class FamilyTreeConverter {
	public static async Task<string> SerializeAsync(IList<FamilyTreeNode> nodes) {
		MemoryStream str = new MemoryStream();
		await JsonSerializer.SerializeAsync(str, nodes).ConfigureAwait(false);
		str.Position = 0;
		using StreamReader sr = new StreamReader(str);
		return await sr.ReadToEndAsync().ConfigureAwait(false);
	}

	public static async Task<IList<FamilyTreeNode>> DeserializeAsync(string? repr) {
		List<FamilyTreeNode>? result = null;
		if(repr != null) {
			MemoryStream str = new MemoryStream(Encoding.UTF8.GetBytes(repr));
			result = await JsonSerializer.DeserializeAsync<List<FamilyTreeNode>?>(str).ConfigureAwait(false);
		}
		return result ?? new List<FamilyTreeNode>();
	}
}
