﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyTree.Access;
public interface IFamilyTreeFetcher
{
    Task<IList<FamilyTreeNode>> GetTreeAsync(int userId, int treeId);
    Task<IList<FamilyTreeNode>> GetBackupAsync(int userId, int treeId);
    Task<IList<int>> GetTreesOfUserAsync(int userId);
}
