﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PostgreSqlStorage.FamilyTreeStorageGateway;
using Users.UserAccess.Storage;

namespace FamilyTree.Access {

	/// <summary>
	/// A class that stands beween family tree api calls and the database. It serves as both IFamilyTreeFetcher and IFamilyTreeSaver.
	/// </summary>
	public class FamilyTreeController : IFamilyTreeFetcher, IFamilyTreeSaver {
		private FamilyTreeStorageFetcher _treeFetcher;
		private FamilyTreeStorageSaver _treeSaver;
		private IUserFetcher _userFetcher;
		public FamilyTreeController(FamilyTreeStorageSaver treeSaver, FamilyTreeStorageFetcher treeFetcher, IUserFetcher userFetcher) {
			_treeFetcher = treeFetcher;
			_treeSaver = treeSaver;
			_userFetcher = userFetcher;
		}

		/// <summary>
		/// Gets the <b>current version</b> of the family tree with id <paramref name="treeId"/> 
		/// if it belongs to the user with <paramref name="userId"/>.
		/// </summary>
		public async Task<IList<FamilyTreeNode>> GetTreeAsync(int userId, int treeId) {
			await CheckUserExistence(userId);
			var holder = await _treeFetcher.GetFamilyTreeAsync(treeId);
			if(holder.UserId == userId) {
				return await FamilyTreeConverter.DeserializeAsync(holder.Current);
			} else {
				throw new FamilyTreeOwnershipException($"User {userId} tried to GET family tree {treeId} which he doesn't own.");
			}
		}
		
		/// <summary>
		/// Gets the <b>backup version</b> of the family tree with <paramref name="treeId"/> 
		/// if it belongs to the user with <paramref name="userId"/>.
		/// </summary>
		public async Task<IList<FamilyTreeNode>> GetBackupAsync(int userId, int treeId) {
			await CheckUserExistence(userId);
			var holder = await _treeFetcher.GetFamilyTreeAsync(treeId);
			if(holder.UserId == userId) {
				return await FamilyTreeConverter.DeserializeAsync(holder.Backup);
			} else {
				throw new FamilyTreeOwnershipException($"User {userId} tried to GET family tree {treeId} which he doesn't own.");
			}
		}
		
		/// <summary>
		/// Updates the tree with <paramref name="treeId"/> 
		/// if it belongs to the user with <paramref name="userId"/>.
		///  The backup version is <b>discarded</b> and 
		/// the previous version becomes the new backup.
		/// </summary>
		public async Task<IList<FamilyTreeNode>> UpdateTreeAsync(int userId, int treeId, IList<FamilyTreeNode> familyTree) {
			await CheckUserExistence(userId);
			string repr = await FamilyTreeConverter.SerializeAsync(familyTree);
			var previous = await _treeFetcher.GetFamilyTreeAsync(treeId);
			if(previous.UserId == userId) {
				var curr = await _treeSaver.UpdateFamilyTreeAsync(previous.Update(repr));
				return await FamilyTreeConverter.DeserializeAsync(curr.Current);
			} else {
				throw new FamilyTreeOwnershipException($"User {userId} tried to UPDATE family tree {treeId} which he doesn't own.");
			}
		}

		public async Task<int> AddNewTreeAsync(int userId) {
			await CheckUserExistence(userId);
			var holder = new FamilyTreeHolder(id: -1, userId: userId);
			return await _treeSaver.AddFamilyTreeAsync(holder);
		}

		public async Task RemoveTreeAsync(int userId, int treeId) {
			await CheckUserExistence(userId);
			var tree = await _treeFetcher.GetFamilyTreeAsync(treeId);
			if(tree.UserId == userId) {
				await _treeSaver.RemoveFamilyTreeAsync(treeId);
			} else {
				throw new FamilyTreeOwnershipException($"User {userId} tried to REMOVE family tree {treeId} which he doesn't own.");
			}
		}

		/// <summary>
		/// Retrieves all ids of trees owned by user with <paramref name="userId"/>.
		/// </summary>
		public async Task<IList<int>> GetTreesOfUserAsync(int userId) {
			var trees = await _treeFetcher.GetFamilyTreesAsync();

			var userTrees = trees
				.Where(t => t.UserId == userId)
				.Select(t => t.Id);
			return userTrees.ToList();
		}

		private Task CheckUserExistence(int userId) {
			return _userFetcher.FindUserByIdAsync(userId);
		}
	}
}
