﻿using Users.Exceptions;

namespace FamilyTree.Access; 
public class FamilyTreeOwnershipException : UserException {
	public FamilyTreeOwnershipException(string message) : base(message) {
	}

	public override string GetRepresentation => nameof(FamilyTreeOwnershipException);

}
