﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamilyTree.Access;
public interface IFamilyTreeSaver
{
    Task<IList<FamilyTreeNode>> UpdateTreeAsync(int userId, int treeId, IList<FamilyTreeNode> familyTree);
    Task<int> AddNewTreeAsync(int userId);
    Task RemoveTreeAsync(int userId, int treeId);
}
