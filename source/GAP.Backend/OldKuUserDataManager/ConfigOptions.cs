﻿using CommandLine.Text;
using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldKuUserDataManager; 
internal class ConfigOptions {

    [Option(
        'g',
        "xml-genetic",
        Default = "",
        HelpText = "Path to the file with xml genetic data."
    )]
    public string XmlGenetic { get; set; } = null!;

    [Option(
        'a',
		"xml-addresses",
		Default = "",
		HelpText = "Path to the file with xml addresses data."
	)]
	public string XmlAddresses { get; set; } = null!;

    [Option(
        'u',
		"xml-users",
		Default = "",
		HelpText = "Path to the file with xml data containing user info."
	)]
	public string XmlUsers { get; set; } = null!;

    [Option(
        'e',
		"excel-data",
		Default = "",
		HelpText = "Path to the file with excel user data."
	)]
	public string ExcelData { get; set; } = null!;
    
    [Option(
        'c',
		"csv-genetic",
		Default = "",
		HelpText = "Path to the file with csv genetic data."
	)]
	public string CsvGenetic { get; set; } = null!;
	
    [Option(
        'r',
		"result-file",
		Default = "old_users_data.json",
		HelpText = "Path where the json with data is stored."
	)]
	public string ResultFile { get; set; } = null!;

    [Option(
		"data-loader-file",
		Default = "data_loader_real_users",
		HelpText = "Path where the final data will be stored by DataLoader. Without extension."
	)]
	public string ExternalResultFile { get; set; } = null!;
	
	[Option(
		'p',
		"proceed",
		Default = "n",
		HelpText = "Use this to skip data parsing and only take the parsed data and continue predicting them. Possible values are y/n."
	)]
	public string Proceed { get; set; } = null!;
	
	[Option(
		'o',
		"only-parse",
		Default = "n",
		HelpText = "When this option is used, the data will only be parsed without any predicting. Possible values are y/n."
	)]
	public string OnlyParse { get; set; } = null!;
	
	[Option(
		"predict-count-haplogroups",
		Default = int.MaxValue,
		HelpText = "How many haplogroups will be predicted."
	)]
	public int PredictCountHaplogroup { get; set; }
	
    [Option(
		"haplo-predict-only-unknown",
		Default = "y",
		HelpText = "Whether to predict only the haplogroups that has not been predicted yet or to try predicting all the unpredictable ones again. Possible value: y/n."
	)]
	public string PredictHaploUnknownOnly { get; set; } = null!;

	[Option(
		"predict-count-regions",
		Default = int.MaxValue,
		HelpText = "How many regions will be predicted."
	)]
	public int PredictCountRegions { get; set; }
	
	[Option(
		"print-predict-count",
		Default = "n",
		HelpText = "When this option is used, prediction statistics will be printed after. Possible values are y/n."
	)]
	public string WritePredictCount { get; set; } = null!;
	
	[Option(
		"only-emails",
		Default = "n",
		HelpText = "When used nothing but creation of a file with wrong emails list will be done. Possible values are y/n."
	)]
	public string OnlyEmails { get; set; } = null!;
	
	[Option(
		"save-csv-users",
		Default = "n",
		HelpText = "When used nothing but user saving in csv (for purpose of human reading) will be done. Possible values are y/n."
	)]
	public string OnlySaveUsersCsv { get; set; } = null!;

	[Option(
		's',
		"save-app-users",
		Default = "n",
		HelpText = "When used nothing but user saving for the purpose of being loaded for the server will be done. Possible values are y/n."
	)]
	public string OnlySaveUsersApp { get; set; } = null!;
}
