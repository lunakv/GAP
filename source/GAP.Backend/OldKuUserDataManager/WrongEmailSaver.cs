﻿using OldKuUserDataManager.OldUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldKuUserDataManager {
	internal class WrongEmailSaver {
		private static string fileName = "wrong_emails.txt";

		/// <param name="includeDuplicit">When used, the output file will have users with duplicit emails in it.</param>
		public static void SaveWrong(IEnumerable<OldUser> users, bool includeDuplicit)
			=> GetUsers(users, includeDuplicit, true);

		/// <param name="excludeDuplicit">When used, the resulting list will not have users with duplicit emails in it.</param>
		public static IList<OldUser> GetRightUsers(IEnumerable<OldUser> users, bool excludeDuplicit)
			=> GetUsers(users, excludeDuplicit, false);


		private static IList<OldUser> GetUsers(IEnumerable<OldUser> users, bool excludeDuplicit, bool saveWrong) {
			IList<OldUser> result1 = new List<OldUser>();
			using var str = new StreamWriter(fileName);
			void WriteUser(OldUser u) { 
				if (saveWrong)
					str.WriteLine($"{u.Profile.FamilyName} {u.Profile.GivenName}{new string('\t', 2)}{u.Profile.Email}"); 
			}
			foreach(var u in users) {
				if(!u.Profile.EmailOk) {
					WriteUser(u);
				} else {
					result1.Add(u);
				}
			}
			IList<OldUser> result2 = new List<OldUser>();
			if(excludeDuplicit) {
				str.WriteLine();
				str.WriteLine(new string('-', 20));
				str.WriteLine();
				WithDuplicits(result1, u => WriteUser(u), u => result2.Add(u));
			}
			return result2;
		}

		private static void WithDuplicits(IList<OldUser> users, Action<OldUser> withDuplicit, Action<OldUser> withDistinct) {
			HashSet<int> seen = new HashSet<int>();
			for(int i = 0; i < users.Count; i++) {
				if(users[i].Profile.Email == null || seen.Remove(i))
					continue;
				int hash = users[i].Profile.Email!.ToLower().GetHashCode();
				bool hasDuplicit = false;
				for(int j = i + 1; j < users.Count; j++) {
					if(users[j].Profile.Email?.ToLower().GetHashCode() == hash) {
						withDuplicit(users[j]);
						hasDuplicit = true;
						seen.Add(j);
					}
				}
				if(hasDuplicit) {
					withDuplicit(users[i]);
				} else {
					withDistinct(users[i]);
				}
			}
		}
	}
}
