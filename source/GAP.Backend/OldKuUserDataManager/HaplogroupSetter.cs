﻿using OldKuUserDataManager.OldUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.GenData;
using Users.GenData.Haplogroup;
using Users.HaplogroupPrediction;

namespace OldKuUserDataManager {
	internal class HaplogroupSetter {
		private string timeFormat = "HH:mm:ss ff";
		//when too high, there are too many demands on the nevgen predictor resulting in
		//some haplogroups not being predicted
		private int defaulfBatch = 8; 
		private const int defaultDivison = 10;
		public Task<IList<OldUser>> PredictHaplo(IList<OldUser> users, IHaplogroupPredictor predictor, int count, bool quiet = false)
			=> PredictHaplo(users, predictor, count, new PropertySetState[] { PropertySetState.Unknown }, quiet);
		public async Task<IList<OldUser>> PredictHaplo(IList<OldUser> users, IHaplogroupPredictor predictor, int count, PropertySetState[] predictWhen, bool quiet = false) {

			int batchSize = Math.Min(defaulfBatch, (int)Math.Ceiling(count * 1.0f/defaultDivison));
			count = Math.Min(count, users.Where(u => predictWhen.Contains(u.HaploPredictState)).Count());
			if (!quiet) 
				Console.WriteLine($"<{DateTime.Now.ToString(timeFormat)}> predicting {count} haplogroups, █ = cca {count / defaultDivison} predicted");

			var divided = Math.Max(1, count / defaultDivison);

			List<OldUser> result = new List<OldUser>();
			List<OldUser> changedUsers = new List<OldUser>();
			int i = 0;
			int last = 0;
			List<Task<OldUser>> tasks = new(batchSize);
			foreach (OldUser user in users) {
				if((!predictWhen.Contains(user.HaploPredictState)) || i == count) {
					result.Add(user);
					continue;
				}
				tasks.Add(SetHaplogroup(user, predictor, quiet));
				i++;
				if(i % batchSize == 0) {
					changedUsers.AddRange(await Task.WhenAll(tasks));
					tasks.Clear();
					if(last < i / divided) {
						last = i / divided;
						Console.Write('█');
					}
				}
			}
			Console.WriteLine();
			changedUsers.AddRange(await Task.WhenAll(tasks));
			result.AddRange(changedUsers);

			if(!quiet) {
				Console.WriteLine($"<{DateTime.Now.ToString(timeFormat)}> haplogroups predicted");
			}
			return result;
		}
		
		private async Task<OldUser> SetHaplogroup(OldUser u, IHaplogroupPredictor predictor, bool quiet) {
			if(u.GeneticData.StrMarkers == null) {
				if (!quiet) Console.WriteLine("str markers are null for user " + u.ToString());
				return new OldUser(
					u.Profile,
					u.Ancestor,
					u.OrpRegionName,
					u.GeneticData,
					u.AdminNote) { HaploPredictState = PropertySetState.Unpredictable};
			}
			Haplogroup? pred = null;
			PropertySetState isPred = PropertySetState.Unpredictable;
			if(u.GeneticData.StrMarkers.Count > 0) {
				try {
					pred = await predictor.Predict(new StrMarkerCollection<string, int>(u.GeneticData.StrMarkers)).ConfigureAwait(false);
					isPred = pred != null ? PropertySetState.Predicted : PropertySetState.Unpredictable;
				} catch(Exception) {
					isPred = PropertySetState.Unpredictable;
				}
			}
			return new OldUser(
				u.Profile,
				u.Ancestor,
				u.OrpRegionName,
				new OldGeneticData(u.GeneticData.StrMarkers, pred?.Name),
				u.AdminNote) { 
					HaploPredictState = isPred,
					RegionSetState = u.RegionSetState
				};
		}

	}
}
