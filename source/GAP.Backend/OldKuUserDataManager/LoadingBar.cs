﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldKuUserDataManager; 

/// <summary>
/// A helper class that displays progress of a given action.
/// </summary>
internal class LoadingBar {
	private const int fractionDefault = 10;
	private int _fraction;
	TextWriter _output;
	private int _iteration;
	private int _finalCount = -1;
	public LoadingBar(int fraction=fractionDefault)
		:this(Console.Out, fraction){}
	public LoadingBar(TextWriter output, int fraction=fractionDefault) {
		this._fraction = fraction;
		this._output = output;
		this._iteration = 0;
	}

	/// <summary>
	/// Start displaying.
	/// </summary>
	public LoadingBar InitializeLoading(int finalCount) {
		if(finalCount < 0) {
			throw new ArgumentException($"loading bar count cannot be less than 0 ({finalCount}).");
		}
		this._finalCount = finalCount;
		if(finalCount > _fraction) {
			_output.WriteLine(new string(' ', _fraction - 1) + "↓");
		}
		return this;
	}

	/// <summary>
	/// Reports that progress was made within the action.
	/// </summary>
	public void Iterate() {
		if(_finalCount < 0) {
			throw new InvalidOperationException("loding bar intialization was not called");
		}
		_iteration++;
		if(_iteration % (_finalCount / _fraction) == 0) {
			_output.Write('█');
		}
	}

	/// <summary>
	/// End the progress reporting process.
	/// </summary>
	public void FinalizeLoading() {
		_output.WriteLine();
	}
}
