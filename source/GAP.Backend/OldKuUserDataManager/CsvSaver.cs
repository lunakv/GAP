﻿using OldKuUserDataManager.OldUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldKuUserDataManager {
	internal class CsvSaver {
        private readonly Encoding _csvEncoding;
        public CsvSaver() {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            //PrintEncodings();
            _csvEncoding = Encoding.GetEncoding(1250);
        }

		public void SaveUsers(IEnumerable<OldUser> users) {

			new SaveClass<OldUser>(
				u => string.Join('|', 
					u.GeneticData.StrMarkers
						?.Select(p => $"{p.Key}:{p.Value}") ?? new List<string>()), "Markery");
			var props = new List<SaveClass<OldUser>>() {
					new SaveClass<OldUser>(u => u.Profile.GivenName, "Křestní"),
					new SaveClass<OldUser>(u => u.Profile.FamilyName, "Příjmení"),
					new SaveClass<OldUser>(u => u.Profile.Email, "Email"),
					new SaveClass<OldUser>(u => u.Profile.Address.Street, "Ulice"),
					new SaveClass<OldUser>(u => u.Profile.Address.StreetNumber, "Číslo"),
					new SaveClass<OldUser>(u => u.Profile.Address.Town, "Město"),
					new SaveClass<OldUser>(u => u.Profile.Address.ZipCode, "PSČ"),
					new SaveClass<OldUser>(u => string.Empty, "Markery"),
				};

			var markerProps = new List<string>();

			foreach (var u in users) {
				var keys = u.GeneticData.StrMarkers?.Keys ?? new List<string>();
				foreach(var k in keys) {
					if(!markerProps.Contains(k)) {
						markerProps.Add(k);
					}
				}
			}

			foreach(var m in markerProps) {
				props.Add(new SaveClass<OldUser>(
					u => u.GeneticData.StrMarkers != null && u.GeneticData.StrMarkers.TryGetValue(m, out var val) ? val.ToString() : string.Empty,
					m));
			}

			SaveAllAsCsv(users,
				props,
				"all_users.csv",
				delimiter: ';');
		}
			
		public void SaveAllAsCsv<T>(
			IEnumerable<T> values, 
			List<SaveClass<T>> properties, 
			string file, 
			char delimiter = ',') {
			
			using StreamWriter sr = new StreamWriter(file, false, _csvEncoding);
			sr.WriteLine(string.Join(delimiter, properties.Select(p => p.Name)));
			foreach(var v in values) {
				sr.WriteLine(string.Join(delimiter, properties.Select(p => p.Func(v))));
			}
		}
	}
	internal record SaveClass<T> {
		public string Name;
		public Func<T, string> Func;

		public SaveClass(Func<T, string?> Func, string Name) {
			this.Name = Name;
			this.Func = (t) => Func(t) ?? string.Empty;
		}
	}
}
