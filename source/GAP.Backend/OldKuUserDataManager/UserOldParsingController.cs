﻿using OldKuUserDataManager.OldUsers;

namespace OldKuUserDataManager; 
internal class UserOldParsingController{
	private readonly string _geneticXmlFile;
	private readonly string _addressesXmlFile;
	private readonly string _userInfoXmlFile;
	private readonly string _userDataExcelFile;
	private readonly string _geneticCsvFile;
	public UserOldParsingController(string geneticXmlFile, string addressesXmlFile, string userInfoXmlFile, string userDataExcelFile, string geneticDataCsvFile) {
		_geneticXmlFile = geneticXmlFile;
		_addressesXmlFile = addressesXmlFile;
		_userInfoXmlFile = userInfoXmlFile;
		_userDataExcelFile = userDataExcelFile;
		_geneticCsvFile = geneticDataCsvFile;
	}

	public IList<OldUser> GetData() {
		var got = KuOldDataExcelParser.Parse(_userDataExcelFile, _geneticCsvFile).ToList();
		got.AddRange(KuOldDataXmlParser.Parse(_geneticXmlFile, _userInfoXmlFile, _addressesXmlFile));
		return got.Select(u => new OldUser(
				new OldProfile(
					u.Profile.GivenName?.Trim(),
					u.Profile.FamilyName?.Trim(),
					u.Profile.Email?.Trim(),
					u.Profile.PhoneNumber?.Trim(),
					new OldAddress(
						u.Profile.Address.Town?.Trim(),
						u.Profile.Address.Street?.Trim(),
						u.Profile.Address.StreetNumber?.Trim(),
						u.Profile.Address.ZipCode?.Trim()
					),
					u.Profile.BirthDate
				),
				new OldAncestor(
					u.Ancestor.GivenName?.Trim(),
					u.Ancestor.FamilyName?.Trim(),
					u.Ancestor.PlaceOfDeath?.Trim(),
					u.Ancestor.ApproximateDeathYear?.Trim()
				),
				u.OrpRegionName,
				u.GeneticData,
				u.AdminNote?.Trim()
			))
			.ToList();
	}
}
