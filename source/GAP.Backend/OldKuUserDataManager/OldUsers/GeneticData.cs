﻿using Users.GenData.Haplogroup;

namespace OldKuUserDataManager.OldUsers; 
public class OldGeneticData {
    public IDictionary<string, int>? StrMarkers { get; }
    public string? Haplogroup { get; }

    public OldGeneticData(IDictionary<string, int>? strMarkers, string? haplogroup) {
        StrMarkers = strMarkers;
        Haplogroup = haplogroup;
    }
}
