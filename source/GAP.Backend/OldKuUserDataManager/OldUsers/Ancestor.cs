using Users.UserAccess.Entities;

namespace OldKuUserDataManager.OldUsers;

public struct OldAncestor {
	public string? GivenName { get; set; }
	public string? FamilyName { get; set; }
	public string? PlaceOfDeath { get; set; }
	public string? ApproximateDeathYear { get; set; }

	public OldAncestor(string? givenName, string? familyName, string? placeOfDeath, string? approximateDeathYear) {
		GivenName = givenName;
		FamilyName = familyName;
		PlaceOfDeath = placeOfDeath;
		ApproximateDeathYear = approximateDeathYear;
	}

	public Ancestor ToAncestor() {
		int? year = null;
		if(ApproximateDeathYear != null) {
			if(int.TryParse(ApproximateDeathYear, out int y)) {
				year = y;
			}else if(ApproximateDeathYear.Length > 2 && int.TryParse(ApproximateDeathYear[1..^1], out y)) {
				year = y;
			}
		}
		return new Ancestor(
			address: new ExpandedAddress(), 
			givenName: GivenName, 
			familyName: FamilyName, 
			deathDate:  year.HasValue ? new DateOnly(year.Value, 1, 1) : null, 
			placeOfDeath: PlaceOfDeath);
	}
}