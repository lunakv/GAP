﻿namespace OldKuUserDataManager.OldUsers;

public class OldUser
{
    public OldProfile Profile { get; }
    public OldAncestor Ancestor { get; }
    public string? OrpRegionName { get; }
    public OldGeneticData GeneticData { get; }
    public string? AdminNote { get; }
    public PropertySetState HaploPredictState { get; set; } = PropertySetState.Unknown;
    public PropertySetState RegionSetState { get; set; } = PropertySetState.Unknown;
    public OldUser(OldProfile profile, OldAncestor ancestor, string? orpRegionName, OldGeneticData geneticData, string? adminNote) {
        Profile = profile;
        Ancestor = ancestor;
        OrpRegionName = orpRegionName;
        GeneticData = geneticData;
        AdminNote = adminNote;
    }

	public override string ToString() {
        string name = string.Empty;
        if(Profile != null) {
            if(Profile.FamilyName != null) {
                name = $"{Profile.FamilyName} ";
            }
            if(Profile.GivenName != null) {
                name += Profile.GivenName;
            }
        }
        return $"{nameof(OldUser)}<{name}>";
	}
}
