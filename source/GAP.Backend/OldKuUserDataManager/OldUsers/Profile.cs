﻿using System.Text.Json.Serialization;

namespace OldKuUserDataManager.OldUsers; 
public class OldProfile {	
	public string? GivenName { get; }
	public string? FamilyName { get; }
	public string? Email { get; }
	[JsonIgnore]
	public bool EmailOk => 
		Email != null && 
		!Email.Contains(' ') && 
		Email.Contains('@') && 
		Email.IndexOf('@') == Email.LastIndexOf('@') && 
		Email.LastIndexOf('.') > Email.IndexOf('@');
	public string? PhoneNumber { get; }
	public OldAddress Address { get; }
	public DateOnly? BirthDate { get; }

	public OldProfile(string? givenName, string? familyName, string? email, string? phoneNumber,
		OldAddress address, DateOnly? birthDate) {
		GivenName = givenName;
		FamilyName = familyName;
		Email = email?.Trim();
		PhoneNumber = phoneNumber;
		Address = address;
		BirthDate = birthDate;
	}

}
