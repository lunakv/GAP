using Users.UserAccess.Entities;

namespace OldKuUserDataManager.OldUsers; 

public struct OldAddress {
	public string? Town { get; set; }
	public string? Street { get; set; }
	public string? StreetNumber { get; set; }
	public string? ZipCode { get; set; }

	public OldAddress(string? town, string? street, string? streetNumber, string? zipCode) {
		Town = town;
		Street = street;
		StreetNumber = streetNumber;
		ZipCode = zipCode;
	}
	
	public Address? ToAddress() {
		if(Town == null || Street == null || StreetNumber == null || ZipCode == null) 
			return null;
		return new Address(town: Town, street: Street + " " + StreetNumber, zipCode: ZipCode);
	}
}