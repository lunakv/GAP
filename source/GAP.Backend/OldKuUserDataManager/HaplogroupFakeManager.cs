﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.GenData.Haplogroup;
using Users.GenData.Haplogroup.Storage;
using Users.UserAccess;
using Users.UserAccess.UserFilter;

namespace OldKuUserDataManager {
	internal class HaplogroupFakeManager : IHaplogroupFetcher, IHaplogroupSaver {

		private List<Haplogroup> _haplos = new List<Haplogroup>();

		public Task<Haplogroup> FindHaplogroupByIdAsync(int haplogroupId) {
			return Task<Haplogroup>.FromResult<Haplogroup>(
				_haplos.FirstOrDefault(h => h.Id == haplogroupId)!
				);
		}

		public Task<IList<Haplogroup>> GetHaplogroupsAsync() {
			return Task<IList<Haplogroup>>.FromResult<IList<Haplogroup>>(
				new List<Haplogroup>(_haplos)
			);
		}

		public Task<IList<Haplogroup>> GetHaplogroupsAsync(IHaplogroupFilter haplogroupFilter) {
			return Task<IList<Haplogroup>>.FromResult<IList<Haplogroup>>(
				haplogroupFilter.Filter(_haplos).ToList()
			); 
		}

		public Task AddHaplogroupAsync(string haplogroup) {
			return AddHaplogroupsAsync(new List<string>() { haplogroup});
		}

		public Task AddHaplogroupsAsync(IList<string> haplogroups) {
			var max = _haplos.Select(h => h.Id).Append(0).Max();
			for(int i = 0; i < haplogroups.Count; i++) {
				_haplos.Add(new Haplogroup(max + 1 + i, haplogroups[i]));
			}
			return Task.CompletedTask;
		}
	}
}
