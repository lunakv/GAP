﻿// See https://aka.ms/new-console-template for more information

using CommandLine;
using CommandLine.Text;
using OldKuUserDataManager;
using OldKuUserDataManager.OldUsers;
using OldKuUserDataManager.RegionSetting;
using System.Threading.Tasks;
using Users.HaplogroupPrediction;

var argsParseResult = new Parser().ParseArguments<ArgsOptions>(args);

var helpText = HelpText.AutoBuild(argsParseResult, h => {
    return HelpText.DefaultParsingErrorsHandler(argsParseResult, h);
}, e => e);
ParserResult<ConfigOptions>? parserResult = null;
try {
	argsParseResult.WithParsed(o => {
		parserResult = new Parser().ParseArguments<ConfigOptions>(
			File.ReadAllLines(o.ConfigFile)
				.Where(s => s.Length > 0)
				.Where(s => !s.StartsWith('#'))
		);
	});
}catch(Exception ex) {
	helpText.AddPreOptionsLine("CONFIG_FILE_ERROR:");
	helpText.AddPreOptionsLine($"{ex.Message}");
	Console.WriteLine(helpText);
}

helpText = HelpText.AutoBuild(parserResult, h => {
    return HelpText.DefaultParsingErrorsHandler(parserResult, h);
}, e => e);

try {
	parserResult.WithParsed(o => {
		IList<OldUser> users;
		if(o.Proceed == "n") {
			if(!File.Exists(o.XmlGenetic)) {
				Console.WriteLine($"Xml genetic file does not exist - {o.XmlGenetic}.");
				return;
			} else
			if(!File.Exists(o.XmlAddresses)) {
				Console.WriteLine($"Xml addresses file does not exist - {o.XmlAddresses}.");
				return;
			} else
			if(!File.Exists(o.XmlUsers)) {
				Console.WriteLine($"Xml users file does not exist - {o.XmlUsers}.");
				return;
			} else
			if(!File.Exists(o.ExcelData)) {
				Console.WriteLine($"Excel user data file does not exist - {o.ExcelData}.");
				return;
			} else
			if(!File.Exists(o.CsvGenetic)) {
				Console.WriteLine($"Csv genetic file does not exist - {o.CsvGenetic}.");
				return;
			} else {
				var rawUsers = new UserOldParsingController(
					o.XmlGenetic, o.XmlAddresses, o.XmlUsers, o.ExcelData, o.CsvGenetic).GetData();
				users = new UsersMatcher().Match(rawUsers, UsersStorageManager.Load(o.ResultFile));
			}
		} else {
			users = UsersStorageManager.Load(o.ResultFile);
		}
		if(o.OnlySaveUsersApp == "y") {
			FinalDataDistributor.SaveToDataLoader(o.ExternalResultFile, users);
			return;
		}
		if(o.OnlySaveUsersCsv == "y") {
			new CsvSaver().SaveUsers(users);
			return;
		}
		if(o.OnlyEmails == "y") {
			WrongEmailSaver.SaveWrong(users, true);
			return;
		}
		if(o.OnlyParse == "n") {
			HaplogroupFakeManager hfm = new();
			var haploPred = new NevgenHaplogroupPredictor(hfm, hfm);
			haploPred.InitializeComputations().Wait();
			var task = new RegionSetter().SetRegion(users, o.PredictCountRegions);
			task.Wait();
			//saving the invalid regions - should be conditioned from args in future
			InvalidRegionsFinder.SaveInvalid(task.Result, "invalid_regions.txt");
			task = new HaplogroupSetter().PredictHaplo(task.Result, haploPred, o.PredictCountHaplogroup, 
				o.PredictHaploUnknownOnly == "y" ?
				new PropertySetState[] {PropertySetState.Unknown} :
				new PropertySetState[] {PropertySetState.Unknown, PropertySetState.Unpredictable});
			task.Wait();
			users = task.Result;
		}
		UsersStorageManager.Save(users.ToList(), o.ResultFile);
		if(o.WritePredictCount == "y") {
			var us = UsersStorageManager.Load(o.ResultFile);
			int hp = us.Where(u => u.HaploPredictState != PropertySetState.Unknown).Count();
			int hp2 = us.Where(u => u.HaploPredictState == PropertySetState.Predicted).Count();
			Console.WriteLine();
			Console.WriteLine($"total haplogroups predicted = {hp}");
			Console.WriteLine($"\tsuccessfully = {hp2}");
			Console.WriteLine($"\tremaining = {us.Count - hp}");
			int rp = us.Where(u => u.RegionSetState != PropertySetState.Unknown).Count();
			int rp2 = us.Where(u => u.RegionSetState == PropertySetState.Predicted).Count();
			Console.WriteLine($"total regions predicted = {rp}");
			Console.WriteLine($"\tsuccessfully = {rp2}");
			Console.WriteLine($"\tremaining = {us.Count - rp}");
			return;
		}
	});
}catch(Exception ex) {
	helpText.AddPreOptionsLine("ARGS_ERROR:");
	helpText.AddPreOptionsLine($"{ex.Message}");
	Console.WriteLine(helpText);
}