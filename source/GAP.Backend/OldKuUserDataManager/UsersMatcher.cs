﻿using OldKuUserDataManager.OldUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldKuUserDataManager {
	internal class UsersMatcher {
		/// <summary>
		/// Joins the two groups together. If two same users exist in both, they will become one. 
		/// Values from the <paramref name="first"/> list are prefered.
		/// </summary>
		public IList<OldUser> Match(IList<OldUser> first, IList<OldUser> others) { 
			List<OldUser> result = new List<OldUser>();
			List<OldUser> second = new List<OldUser>(others);
			foreach (OldUser user in first) {
				bool found = false;
				for(int i = 0; i < second.Count; i++) {
					if(UserMatch(second[i], user)) {
						result.Add(Join(user, second[i]));
						second.RemoveAt(i);
						found = true;
						break;
					}
				}
				if(!found) {
					result.Add(user);
				}
			}
			result.AddRange(second);
			return result;
		}

		private OldUser Join(OldUser u1, OldUser u2) {
			return new OldUser(
				new OldProfile(
					u1.Profile.GivenName ?? u2.Profile.GivenName,
					u1.Profile.FamilyName ?? u2.Profile.FamilyName,
					u1.Profile.EmailOk ? (u1.Profile.Email ?? u2.Profile.Email) : (u2.Profile.Email ?? u1.Profile.Email),
					u1.Profile.PhoneNumber ?? u2.Profile.PhoneNumber,
					new OldAddress(
						u1.Profile.Address.Town ?? u2.Profile.Address.Town,
						u1.Profile.Address.Street ?? u2.Profile.Address.Street,
						u1.Profile.Address.StreetNumber ?? u2.Profile.Address.StreetNumber,
						u1.Profile.Address.ZipCode ?? u2.Profile.Address.ZipCode),
					u1.Profile.BirthDate ?? u2.Profile.BirthDate),
				new OldAncestor(
					u1.Ancestor.GivenName ?? u2.Ancestor.GivenName,
					u1.Ancestor.FamilyName ?? u2.Ancestor.FamilyName,
					u1.Ancestor.PlaceOfDeath ?? u2.Ancestor.PlaceOfDeath,
					u1.Ancestor.ApproximateDeathYear ?? u2.Ancestor.ApproximateDeathYear),
				u1.OrpRegionName ?? u2.OrpRegionName,
				new OldGeneticData(
					u1.GeneticData.StrMarkers != null && u1.GeneticData.StrMarkers.Count > 0 ? u1.GeneticData.StrMarkers : u2.GeneticData.StrMarkers,
					u1.GeneticData.Haplogroup ?? u2.GeneticData.Haplogroup),
				u1.AdminNote ?? u2.AdminNote
				) {
				HaploPredictState = u1.HaploPredictState != PropertySetState.Unknown ? u1.HaploPredictState : u2.HaploPredictState,
				RegionSetState = u1.RegionSetState != PropertySetState.Unknown ? u1.RegionSetState : u2.RegionSetState
			};
		}

		private bool UserMatch(OldUser u1, OldUser u2) {
			return ProfileMatch(u1.Profile, u2.Profile);
		}

		private bool ProfileMatch(OldProfile? p1, OldProfile? p2) {
			if(p1 == null || p2 == null) return false;
			return p1.GivenName == p2.GivenName &&
				p1.FamilyName == p2.FamilyName && p2.FamilyName != null &&
				p1.Email == p2.Email && p1.Email != null;
		}		

	}				  
}					  
					 