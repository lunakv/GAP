﻿using OldKuUserDataManager.OldUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldKuUserDataManager.RegionSetting
{
    internal class RegionSetter
    {
        public async Task<IList<OldUser>> SetRegion(IEnumerable<OldUser> users, int count)
        {
            IRegionProvider parseResult = await new RegionParsingController().Parse();
            IList<OldUser> result = new List<OldUser>();
            IList<OldUser> wrong = new List<OldUser>();
            int i = 0;
            int zipCodesCounter = 0;
            foreach(var u in users) {
                if(u.RegionSetState != PropertySetState.Unknown || i == count) {
                    result.Add(u);
                    continue;
                }
                string? orp = null;
                string? zipCode = u.Profile.Address.ZipCode;
                PropertySetState state = PropertySetState.Unpredictable;
                if(u.Profile.Address.Town != null){
                    AddressResult? address = parseResult.Get(u.Profile.Address.Town, u.Profile.Address.Street);
                    if(address != null) {
                        orp = address.OrpName;
                        if(zipCode == null && address.ZipCodes.Count == 1) {
                            zipCode = address.ZipCodes[0];
                            zipCodesCounter++;
                        }
                        state = PropertySetState.Predicted;
                    }
                    else {
                        wrong.Add(u);
                    }
                }
                result.Add(new OldUser(
                    new OldProfile(
                        u.Profile.GivenName,
                        u.Profile.FamilyName,
                        u.Profile.Email,
                        u.Profile.PhoneNumber,
                        new OldAddress(
                            u.Profile.Address.Town,
                            u.Profile.Address.Street,
                            u.Profile.Address.StreetNumber,
                            zipCode),
                        u.Profile.BirthDate),
                    u.Ancestor,
                    orp,
                    u.GeneticData,
                    u.AdminNote) { 
                    RegionSetState = state, 
                    HaploPredictState = u.HaploPredictState });
            }
            foreach(var w in wrong) {
                var town = (w.Profile.Address.Town ?? "null") + $" : {w.Profile.Address.Street}";
                Console.WriteLine($"{w}: {town}");
            }
            if(count > 0) {
                Console.WriteLine($"changed zip codes for {zipCodesCounter} users.");
            }
            return result.ToList();
        }

    }
}
