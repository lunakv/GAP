﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace OldKuUserDataManager.RegionSetting; 

internal class RegionProvider : IRegionProvider {
	public Dictionary<string, RegionCode> _Unique { private set; get; }
	public Dictionary<string, Dictionary<string, RegionCode?>> _Duplicit { private set; get; }

    [JsonConverter(typeof(DictionaryCodeConverter<string>))]
	public Dictionary<RegionCode, string> _Orps { private set; get; }

    [JsonConverter(typeof(DictionaryCodeConverter<List<string>>))]
	public Dictionary<RegionCode, List<string>> _ZipCodes { private set; get; }
    public RegionProvider(Dictionary<string, RegionCode> _Unique,
        Dictionary<string, Dictionary<string, RegionCode?>> _Duplicit,
        Dictionary<RegionCode, string> _Orps, Dictionary<RegionCode, List<string>> _ZipCodes) {
		    this._Unique = _Unique;
		    this._Duplicit = _Duplicit;
		    this._Orps = _Orps;
		    this._ZipCodes = _ZipCodes;
	}

	public AddressResult? Get(string municipality, string? street) {
        RegionCode? code;
        //if there is only one municipality of that name
        if(TryGetValue(_Unique, municipality, out code)
                    
            || (street != null
            //or if there are multiple
            && TryGetValue(_Duplicit, municipality, out var streetDict)
            //but given a street the right one can be deduced
            && streetDict!.TryGetValue(street, out code)
            && code != null)) {
            return new AddressResult(_ZipCodes[code!], _Orps[code!]);

        }
        //code cannot be found
        return null;
	}
    
    private bool TryGetValue<T>(Dictionary<string, T> from, string municipality, out T? val) where T:class{
        if(from.TryGetValue(municipality, out T? res)) {
            val = res;
            return true;
        }

        var parts = municipality.Split('-');
        for(int i = 0; i < parts.Length; i++) {
            var part = string.Join("-", parts.Take(i+1)).Trim();
            if(from.TryGetValue(part, out res)) {
                val = res;
                return true;
            }
            int lastSpace = part.LastIndexOf(' ');
            if(lastSpace != -1
                && from.TryGetValue(part.Substring(0, lastSpace), out res)) {
                val = res;
                return true;
            }
        }
        for(int i = 1; i < parts.Length-1; i++) {
            var part = string.Join("-", parts.Skip(i)).Trim();
            if(from.TryGetValue(part, out res)) {
                val = res;
                return true;
            }
            int lastSpace = part.LastIndexOf(' ');
            if(lastSpace != -1
                && from.TryGetValue(part.Substring(0, lastSpace), out res)) {
                val = res;
                return true;
            }
        }
        val = null;
        return false;
    }
}
