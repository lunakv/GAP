﻿using OldKuUserDataManager.OldUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldKuUserDataManager.RegionSetting {
	internal class InvalidRegionsFinder {
		public static List<OldUser> GetRight(List<OldUser> all) {
			return all
				.Where(x => x.RegionSetState == PropertySetState.Predicted)
				.ToList();
		} 

		public static void SaveInvalid(IEnumerable<OldUser> all, string fileName) {
			using var str = new StreamWriter(fileName);
			foreach(var item in all.Where(x => x.RegionSetState != PropertySetState.Predicted)) {
				str.Write("\t");
				str.WriteLine($"{item.Profile.GivenName} {item.Profile.FamilyName} <{item.Profile.Address.Town} ; {item.Profile.Address.Street}>");
			} 
		} 

	}
}
