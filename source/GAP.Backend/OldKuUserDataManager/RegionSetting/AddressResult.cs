﻿using CsvHelper;
using OldKuUserDataManager.OldUsers;
using System;
using System.Collections.Generic;
using System.Formats.Asn1;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace OldKuUserDataManager.RegionSetting; 
internal record AddressResult(List<string> ZipCodes, string OrpName);