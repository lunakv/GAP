﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;

namespace OldKuUserDataManager.RegionSetting; 
internal class RegionFilesDownloader {
	public async Task Download(RegionDownloadFileNames files, bool force = false) {
		Task regionTask = DownloadFile(files.Regions, "https://services.cuzk.cz/shp/stat/epsg-5514/1.zip", force);
		Task zipTask = DownloadFile(files.Adresses, "https://vdp.cuzk.cz/vymenny_format/csv/20230131_OB_ADR_csv.zip", force);

		await regionTask;
		await zipTask;
	}


	private async Task DownloadFile(string file, string url, bool force) {
		if(force || !System.IO.File.Exists(file)) {
			HttpClient client = new HttpClient();
			using Stream stream = await client.GetStreamAsync(url);
			using FileStream fileStream = new FileStream(file, FileMode.OpenOrCreate);
			stream.CopyTo(fileStream);
		}
	}
}
