﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldKuUserDataManager.RegionSetting {
	internal class MunicipalityMapper {
        private readonly Encoding _csvEncoding;

        public MunicipalityMapper() {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            //PrintEncodings();
            _csvEncoding = Encoding.GetEncoding(1250);
        }

        /// <summary>
        /// Maps municipality names to respective municipality codes.
        /// </summary>
        /// <param name="directory">Where to find all addresses.</param>
        /// <param name="municNameHeader">Column in a .csv file where to find the MUNICIPALITY NAME.</param>
        /// <param name="municCodeHeader">Column in a .csv file where to find the MUNICIPALITY CODE.</param>
        /// <param name="streetHeader">Column in a .csv file where to find the STREET NAMES.</param>
        /// <returns></returns>
		public MunicipalityCode MapNameToCode(string directory, 
            string municNameHeader, string municCodeHeader, string streetHeader) {

			municNameHeader = RemoveDiacritics(municNameHeader).ToLower();
			municCodeHeader = RemoveDiacritics(municCodeHeader).ToLower();
			streetHeader = RemoveDiacritics(streetHeader).ToLower();

            //name to code
            Dictionary<string, RegionCode> distinct = new ();
            //code to file 
            Dictionary<RegionCode, string> fileDict = new ();
            //name to (street to code)
            Dictionary<string, Dictionary<string, RegionCode?>> duplicit = new ();
            var files = Directory.GetFiles(directory);
            Console.WriteLine("reading all municipalities and their codes");

            var loading = new LoadingBar(10).InitializeLoading(files.Length);
            foreach(string f in files) {
                loading.Iterate();
                if(!f.EndsWith(".csv"))
                    continue;
                var res = GetOneForFile(f, new List<string>() { municNameHeader, municCodeHeader });
                if(res == null)
                    continue;
                if(res[0] == null)
                    throw new ArgumentException($"file {f} has no municipality name column in it");
                string municName = res[0]!;
                if(res[1] == null)
                    throw new ArgumentException($"file {f} has no municipality code column in it");
                RegionCode municCode = new RegionCode(res[1]!);
                if(distinct.ContainsKey(municName) || duplicit.ContainsKey(municName)) {
                    duplicit.TryAdd(municName, new Dictionary<string, RegionCode?>());

                    if(distinct.TryGetValue(municName, out var preCode)) {
                        distinct.Remove(municName);
                        AddStreets(duplicit[municName], fileDict[preCode], preCode, streetHeader, municName);
                    }
                    AddStreets(duplicit[municName], f, municCode, streetHeader, municName);
                } else {
                    distinct.Add(municName, municCode);
                    fileDict.Add(municCode, f);
                }
            }
            loading.FinalizeLoading();

            Console.WriteLine("done");
            return new MunicipalityCode(distinct, duplicit);
		}
       
        public Dictionary<RegionCode, List<string>> MapCodeToZip(string directory, 
            string municCodeHeader, string zipCodeHeader) {

			zipCodeHeader = RemoveDiacritics(zipCodeHeader).ToLower();
			municCodeHeader = RemoveDiacritics(municCodeHeader).ToLower();

            Dictionary<RegionCode, List<string>> result = new ();

            Console.WriteLine("Getting all zip codes...");
            var files = Directory.GetFiles(directory);
            LoadingBar loading = new LoadingBar(10).InitializeLoading(files.Length);
            foreach(string f in files) {
                loading.Iterate();
                if(!f.EndsWith(".csv"))
                    continue;
                var res = GetOneForFile(f, new List<string>() { municCodeHeader });
                if(res == null)
                    continue;
                if(res[0] == null)
                    throw new ArgumentException($"file {f} has no municipality code column in it");
                
                result.Add(new RegionCode(res[0]!), GetAllForFile(f, zipCodeHeader).Distinct().ToList());
            }
            loading.FinalizeLoading();
            Console.WriteLine("Done.");

            return result;
		}

        private void AddStreets(Dictionary<string, RegionCode?> into, string file, RegionCode code, string streetHeader, string muniName) {
            
            var streets = GetAllForFile(file, streetHeader)
                .Distinct()
                .Where(s => s.Length > 0);

            foreach(string street in streets) {
                if(!into.TryAdd(street, code)) {
                    into[street] = null;
                }
            }
            //if there is no such street, it's probably a small village and so
            //street name is often set as the village name within address
            if(!streets.Any()) {
                //but if there are multiple such small villages, we can't tell
                if(!into.TryAdd(muniName, code)) {
                    into[muniName] = null;
                }
            }
        }

		private List<string> GetAllForFile(string file, string valueHeader) {
            //read the file using the central europian encoding
            using TextFieldParser parser = new TextFieldParser(file, _csvEncoding);
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(";");
            if(parser.EndOfData)
                throw new ArgumentException($"file {file} has nothing in it");
            
            //only null if no data, but that is checked above
            List<string>? header = parser.ReadFields()!
                .Select(s => RemoveDiacritics(s))
                .Select(s => s.ToLower())
                .ToList();

            int index = header.IndexOf(valueHeader);
            if(index == -1)
                throw new ArgumentException($"file {file} has no zip code column in it");

            List<string> result = new List<string>();

            string[]? line;
            while((line = parser.ReadFields()) != null) {
                result.Add(line[index]);
            }
            return result;
		}

        /// <summary>
        /// From a file gets the values of respective columns on the first data row.
        /// </summary>
        /// <param name="file">File from which to get the data.</param>
        /// <param name="headerLines">The list of header names of the columns the values should be from.</param>
        /// <returns>List of values in the same order as the headers. Null if no data is present in the file. 
        /// If a given column is not present, then the value in the result list is null.</returns>
        /// <exception cref="ArgumentException">If the file data cannot be read or it has no header in it.</exception>
		private List<string?>? GetOneForFile(string file, List<string> headerLines) {
            //read the file using the central europian encoding
            using TextFieldParser parser = new TextFieldParser(file, _csvEncoding);
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(";");
            if(parser.EndOfData)
                throw new ArgumentException($"file {file} has nothing in it");
            
            //only null if no data, but that is checked above
            List<string> header = parser.ReadFields()! 
                .Select(s => RemoveDiacritics(s))
                .Select(s => s.ToLower())
                .ToList();
            IEnumerable<int> indices = headerLines
                .Select(h => header.IndexOf(h));

            //if there are no addresses present - such document actually is in the dataset
            if(parser.EndOfData) 
                return null;

            string[] fields = parser.ReadFields()!; //since it's not yet data end, this is never null
            return indices
                .Select(i => i == -1 ? null : fields[i])
                .ToList();
		}

        private static string RemoveDiacritics(string text) 
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder(capacity: normalizedString.Length);

            for (int i = 0; i < normalizedString.Length; i++)
            {
                char c = normalizedString[i];
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder
                .ToString()
                .Normalize(NormalizationForm.FormC);
        }

        private static void PrintEncodings()  {

              // Print the header.
              Console.Write( "Name               " );
              Console.Write( "CodePage  " );
              Console.Write( "BodyName           " );
              Console.Write( "HeaderName         " );
              Console.Write( "WebName            " );
              Console.WriteLine( "Encoding.EncodingName" );

              // For every encoding, compare the name properties with EncodingInfo.Name.
              // Display only the encodings that have one or more different names.
              foreach( EncodingInfo ei in Encoding.GetEncodings() )  {
                 Encoding e = ei.GetEncoding();

                 if (( ei.Name != e.BodyName ) || ( ei.Name != e.HeaderName ) || ( ei.Name != e.WebName ))  {
                    Console.Write( "{0,-18} ", ei.Name );
                    Console.Write( "{0,-9} ",  e.CodePage );
                    Console.Write( "{0,-18} ", e.BodyName );
                    Console.Write( "{0,-18} ", e.HeaderName );
                    Console.Write( "{0,-18} ", e.WebName );
                    Console.WriteLine( "{0} ", e.EncodingName );
                 }
              }
           }
	}
}
