﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldKuUserDataManager.RegionSetting {
	internal record MunicipalityCode(
		Dictionary<string, RegionCode> Unique, 
		Dictionary<string, Dictionary<string, RegionCode?>> Duplicit);
}
