﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DbfDataReader;

namespace OldKuUserDataManager.RegionSetting; 

/// <summary>
/// Parses file of .dbf format.
/// </summary>
internal class DbfDataMapper {
    private readonly Encoding _encoding;

    public DbfDataMapper() {
        Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        _encoding = Encoding.GetEncoding(1250);
    }

	/// <summary>
	/// From the <paramref name="file"/> finds goes through all rows and retrieves
	/// values from the column of <paramref name="keyHeader"/> and <paramref name="valHeader"/>
	/// putting them into a dictionary.
	/// </summary>
	public Dictionary<string, string> Map(string file, string keyHeader, string valHeader) {
		Dictionary<string, string> result = new();
		using DbfTable dbfTable = new DbfTable(file, _encoding);

		int? keyColumn = null;
		int? valColumn = null;

		foreach (var dbfColumn in dbfTable.Columns)
		{
			if(dbfColumn.ColumnName == keyHeader) {
				keyColumn = dbfColumn.ColumnOrdinal;
			} else if(dbfColumn.ColumnName == valHeader){
				valColumn = dbfColumn.ColumnOrdinal;
			}
		}
		if(keyColumn == null || valColumn == null) {
			throw new FormatException($"Dbf file {file} lacks at least one of the specified columns : {keyHeader}; {valHeader}.");
		}
		var dbfRecord = new DbfRecord(dbfTable);

		while (dbfTable.Read(dbfRecord)){
			if (dbfRecord.IsDeleted){
				continue;
			}
			var key = dbfRecord.GetStringValue(keyColumn.Value);
			result.Add(key, dbfRecord.GetStringValue(valColumn.Value));
		}
		return result;
	}

}
