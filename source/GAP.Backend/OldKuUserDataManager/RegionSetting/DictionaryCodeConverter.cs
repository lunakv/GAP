﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Threading.Tasks;

namespace OldKuUserDataManager.RegionSetting; 

internal class DictionaryCodeConverter<TValue> : JsonConverter<Dictionary<RegionCode, TValue>>
{
    private readonly Type _valueType;

    public DictionaryCodeConverter()
    {
        _valueType = typeof(TValue);
    }

    public override Dictionary<RegionCode, TValue> Read(
        ref Utf8JsonReader reader,
        Type typeToConvert,
        JsonSerializerOptions options)
    {
        if (reader.TokenType != JsonTokenType.StartObject)
        {
            throw new JsonException();
        }

        var dictionary = new Dictionary<RegionCode, TValue>();

        while (reader.Read())
        {
            if (reader.TokenType == JsonTokenType.EndObject)
            {
                return dictionary;
            }

            // Get the key.
            if (reader.TokenType != JsonTokenType.PropertyName)
            {
                throw new JsonException();
            }

            string propertyName = reader.GetString()!; //is not null, checked by the token type

            // Get the value.
            reader.Read();
            TValue value = ((JsonConverter<TValue>)options
            .GetConverter(typeof(TValue))).Read(ref reader, _valueType, options)!;

            // Add to dictionary.
            dictionary.Add(new RegionCode(propertyName), value);
        }

        throw new JsonException();
    }

    public override void Write(
        Utf8JsonWriter writer,
        Dictionary<RegionCode, TValue> dictionary,
        JsonSerializerOptions options)
    {
        writer.WriteStartObject();

        foreach ((RegionCode key, TValue value) in dictionary)
        {
            writer.WritePropertyName(key.Value);

            ((JsonConverter<TValue>)options
            .GetConverter(typeof(TValue))).Write(writer, value, options);
        }

        writer.WriteEndObject();
    }
}

