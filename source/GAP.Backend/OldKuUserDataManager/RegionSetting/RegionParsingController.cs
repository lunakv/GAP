﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace OldKuUserDataManager.RegionSetting {
	internal class RegionParsingController {
		private string _file = "munic_orp.Json";

		/// <summary>
		/// Maps Municipality names to respective ORP names.
		/// </summary>
		/// <returns></returns>
		public async Task<IRegionProvider> Parse() {
			if(File.Exists(_file)) {
				var loaded = Load();
				if(loaded != null) {
					return loaded;
				}
			}
			var initialFiles = new RegionDownloadFileNames("addresses.zip", "regions.zip");
			await new RegionFilesDownloader().Download(initialFiles);
			var directories = new RegionDownloadFileNames("addressesDir", "regionsDir");
			new RegionFileExtracter().Extract(initialFiles, directories);
			MunicipalityCode municCode = new MunicipalityMapper()
				.MapNameToCode(Path.Combine(directories.Adresses,"CSV"), "Název obce", "Kód obce", "Název ulice");
			var zipCodes = new MunicipalityMapper()
				.MapCodeToZip(Path.Combine(directories.Adresses, "CSV"), "Kód obce", "PSČ");
			var municOrp = new DbfDataMapper()
				.Map(Path.Combine(directories.Regions,"1","OBCE_P.dbf"), "KOD", "ORP_KOD");
			var orpName = new DbfDataMapper()
				.Map(Path.Combine(directories.Regions,"1","ORP_P.dbf"), "KOD", "NAZEV");

			RegionProvider result = new RegionProvider(municCode.Unique, 
				municCode.Duplicit, 
				municOrp.ToDictionary(p => new RegionCode(p.Key), p => orpName[p.Value]),
				zipCodes);

			Save(result);
			return result;
		}

		private RegionProvider? Load() {
			using var fs = new FileStream(_file, FileMode.Open);
			return (RegionProvider?)JsonSerializer.Deserialize(fs, typeof(RegionProvider));
		}
		
		private void Save(RegionProvider val) {
			using var fs = new FileStream(_file, FileMode.Create);
			JsonSerializer.Serialize(fs, val, typeof(RegionProvider), new JsonSerializerOptions { WriteIndented = true });
		}
	}
}
