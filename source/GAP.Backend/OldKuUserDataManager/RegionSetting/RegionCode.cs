﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldKuUserDataManager.RegionSetting; 

internal class RegionCode {
    public string Value { get; }
    public RegionCode(string value) {
        this.Value = value;
    }
	public override string ToString() {
		return $"RegCode<{Value}>";
	}
	public override int GetHashCode() {
		return Value.GetHashCode();
	}
	public override bool Equals(object? obj) {
		return obj is RegionCode && Value.Equals((obj as RegionCode)!.Value);
	}
}

