﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO.Compression;
using System.Text;
using System.Threading.Tasks;

namespace OldKuUserDataManager.RegionSetting; 
internal class RegionFileExtracter {
	public void Extract(RegionDownloadFileNames filesFrom, RegionDownloadFileNames filesTo, bool force = false) {
		if(force || !Directory.Exists(filesTo.Regions))
			ZipFile.ExtractToDirectory(filesFrom.Regions, filesTo.Regions);
		if(force || !Directory.Exists(filesTo.Adresses))
			ZipFile.ExtractToDirectory(filesFrom.Adresses, filesTo.Adresses);
	}
}
