﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldKuUserDataManager.RegionSetting {
    internal interface IRegionProvider {
        AddressResult? Get(string municipality, string? street);
    }
}
