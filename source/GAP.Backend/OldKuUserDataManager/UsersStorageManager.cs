﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Reflection;

namespace OldKuUserDataManager {
	internal class UsersStorageManager {
		internal static void Save(List<OldUsers.OldUser> users, string filePath) {
			string str = JsonSerializer.Serialize<List<OldUsers.OldUser>>(users);
			File.WriteAllText(filePath, str);
		}
		public static List<OldUsers.OldUser> Load(string filePath) {
			if(!File.Exists(filePath)) {
				return new List<OldUsers.OldUser>();
			}
			string str = File.ReadAllText(filePath);
			List<OldUsers.OldUser>? users =  JsonSerializer.Deserialize<List<OldUsers.OldUser>>(str);
			return users ?? new List<OldUsers.OldUser>();
		}
	}
}
