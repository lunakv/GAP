﻿using OldKuUserDataManager.OldUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLoader.RealDataLoaders;
using Users.UserAccess;
using Users.UserAccess.Entities;

namespace OldKuUserDataManager; 
internal class FinalDataDistributor{

	/// <summary>
	/// Saves the <paramref name="users"/> into the <paramref name="saveFile"/> using 
	/// external save function (from <see cref="DataLoader.RealDataLoaders.OldUserDataSource"/>).
	/// It also filters the <paramref name="users"/> using the 
	/// <see cref="WrongUserRemover.Remove(IList{OldUser}, IEnumerable{UserFilterer})"/> function.
	/// </summary>
	public static void SaveToDataLoader(string saveFile, IList<OldUser> users) {
		UserFilterer[] funcs = new UserFilterer[] {
			new UserFilterer("right email format", u => WrongEmailSaver.GetRightUsers(u, true))
		};

		users = WrongUserRemover.Remove(users, WrongUserRemover.DefaultFilters.Concat(funcs));

		DataLoader.RealDataLoaders.OldUserDataSource.SaveUsers(
			users.Select(u => new DataLoader.RealDataLoaders.AlreadyUser(
			new DataLoader.RealDataLoaders.AlreadyProfile(
				u.Profile.GivenName!,
				u.Profile.FamilyName!,
				u.Profile.Email!,
				u.Profile.PhoneNumber,
				GetExpandedAddress(u),
				u.Profile.Address.ToAddress(),
				u.Profile.BirthDate),
			AlreadyAncestor.FromAncestor(u.Ancestor.ToAncestor()),
			new DataLoader.RealDataLoaders.AlreadyGeneticData(
				//both already checked by wrong users remover
				new Dictionary<string, int>(u.GeneticData.StrMarkers!),
				u.GeneticData.Haplogroup!))
			).ToList(), saveFile);
	}
	
	private static AlreadyExpandedAddress GetExpandedAddress(OldUser u) {
		var a = u.Profile.Address;
		return new AlreadyExpandedAddress(a.Town,
			u.RegionSetState == PropertySetState.Predicted ? u.OrpRegionName : a.Town,
			County: null,
			a.Street,
			a.StreetNumber,
			a.ZipCode);
	}
}
