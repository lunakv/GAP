﻿using OldKuUserDataManager.OldUsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldKuUserDataManager; 
internal class WrongUserRemover {
	/// <summary>
	/// List of properties that are considered unacceptable by default.
	/// </summary>
	public static IEnumerable<UserFilterer> DefaultFilters => new List<UserFilterer>() {
		new UserFilterer("haplos", users => users.Where(u => u.HaploPredictState == PropertySetState.Predicted)),
		new UserFilterer("regions", users => users.Where(u => u.RegionSetState == PropertySetState.Predicted)),
		new UserFilterer("genetic data", users => users.Where(u => u.GeneticData.StrMarkers != null && u.GeneticData.StrMarkers.Count > 0)),
		new UserFilterer("email", users => users.Where(u => u.Profile.Email != null)),
		new UserFilterer("given name", users => users.Where(u => u.Profile.GivenName != null)),
		new UserFilterer("family name", users => users.Where(u => u.Profile.FamilyName != null))
	};

	/// <summary>
	/// Filters <paramref name="users"/> to remove those with unacceptable properties. 
	/// Unacceptable properties are defined by <paramref name="filterFuncs"/>.
	/// </summary>
	public static IList<OldUser> Remove(IList<OldUser> users, IEnumerable<UserFilterer> filterFuncs) {
		Console.WriteLine($"input: {users.Count} users");
		IEnumerable<OldUser> res = users;
		foreach(var f in filterFuncs) {
			res = f.FilterFunc(res);
			Console.WriteLine($"with {f.Name}: {res.Count()} users");
		}

		return res.ToList();
	}
}


internal record UserFilterer(string Name, Func<IEnumerable<OldUser>, IEnumerable<OldUser>> FilterFunc);
