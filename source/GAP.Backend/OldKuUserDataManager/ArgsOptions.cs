﻿using CommandLine.Text;
using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OldKuUserDataManager; 
internal class ArgsOptions {
    [Option(
        'c',
        "config",
        Default = "oldDataConfig.cfg",
        HelpText = "Path to the file with configuration."
    )]
    public string ConfigFile { get; set; } = null!;
}
