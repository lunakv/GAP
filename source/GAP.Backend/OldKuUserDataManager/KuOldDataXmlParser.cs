﻿using System.Xml.Linq;
using OldKuUserDataManager.OldUsers;

namespace OldKuUserDataManager;

internal class KuOldDataXmlParser {

	/// <summary>
	/// Parses users from xml files.
	/// </summary>
	public static IList<OldUser> Parse(string geneticDataFile, string userInfoFile, string addressesFile) {
		var userInfos = ParseUserInfo(userInfoFile);
		if(userInfos == null) {
			Console.WriteLine($"there was a problem parsing the user info file: {userInfoFile}");
			return new List<OldUser>();
		}
		var addresses = ParseAddresses(addressesFile);
		var geneticData = ParseGenetic(geneticDataFile);
		List<OldUser> result = new();
		int noAddress = 0;
		int noGenetic = 0;
		foreach(UserRepr ur in userInfos) {
			OldAddress address;
			if(! addresses.TryGetValue(ur.Id, out address)) {
				address = default;
				noAddress++;
				Console.WriteLine($"No address found for user of id {ur.Id}. Default will be used.");
			}
			Dictionary<string, int>? markers;
			if(! geneticData.TryGetValue(ur.Id, out markers)) {
				markers = new Dictionary<string, int>();
				noGenetic++;
				Console.WriteLine($"No genetic data found for user of id {ur.Id}. Default will be used.");
			}
			result.Add(new OldUser(
				new OldProfile(
					ur.FirstName,
					ur.FamilyName,
					ur.Email,
					ur.PhoneNumber,
					address,
					default),
				ur.Ancestor ?? default,
				orpRegionName: null,
				new OldGeneticData(
					markers,
					null),
				ur.AdminNote)
				);
		}
		Console.WriteLine($"Intotal, there were {noAddress} users with no address and {noGenetic} users with no genetic data.");
		Console.WriteLine($"And there were {result.Count} users successfuly read.");
		return result;
	}

	/// <summary>
	/// Loads surname of a person contained with the xml <paramref name="item"/>.
	/// </summary>
	private static string? ReadName(XElement? item) {
		string? Value(string name) {
			return item?.Attribute(name)?.Value;
		}
		return Value("prijmeni") ?? Value("jmeno");
	}

	/// <summary>
	/// Parses all <b>personal</b> information about users from a given xml file.
	/// </summary>
	private static IList<UserRepr>? ParseUserInfo(string file) {
		OldAncestor? GetAncestor(XElement elem) {
			var lastName = Val("predekJmeno", elem);
			var municipality = Val("predekObec", elem);
			var district = Val("predekOkr", elem) ?? Val("predekOkres", elem);
			var yearStr = Val("predekRok", elem) ?? Val("predek", elem);
			if(lastName == null  //if we know nothing, just act as if the user is not there
				&& municipality == null
				&& district == null
				&& yearStr == null) {
					return null;

			}
			return new OldAncestor(
				givenName: null,
				familyName: lastName,
				placeOfDeath: municipality + ", " + district,
				approximateDeathYear: yearStr
				);
		}
		string? Val(string name, XElement item) {
			return item.Attribute(name)?.Value;
		}

		XElement root = XElement.Load(file);
		List<UserRepr> result = new();
		var users = root.Descendants();
		int line = -1;
		foreach(var item in users) {
			line++;
			string? Value(string name) 
				=> Val(name, item);
			var id = Value("id");
			var lastName = ReadName(item);
			if(id == null) {
				Console.WriteLine($"No id found on line {line} (name {lastName}).");
				continue;
			}
			if(lastName == null) {
				Console.WriteLine($"No last name found on line {line} (id {id}).");
				continue;
			}
			var mail = Value("novymail") ?? Value("mail2")?? Value("mail");
			if(mail == null) {
				Console.WriteLine($"No email found on line {line} for id {id} (name {lastName}).");
				continue;
			}
			var givenName = Value("krestni");
			var phone = Value("tel");
			var adminNote = Value("pozn");
			var district = Value("okr") ?? Value("OKR");
			var municipality = Value("obec");
			var ancestor = GetAncestor(item);
			result.Add(new UserRepr(
				lastName,
				givenName,
				mail,
				phone,
				district,
				adminNote,
				municipality,
				ancestor,
				id));
		}
		return result;
	}
	
	/// <summary>
	/// Parses all <b>genetic</b> information about users from a given xml file.
	/// </summary>
	private static Dictionary<string, Dictionary<string, int>> ParseGenetic(string file) {
		Dictionary<string, Dictionary<string, int>> result = new();
		XElement root = XElement.Load(file);
		var users = root.Elements();
		int index = -1;
		foreach ( var user in users ) {
			index++;
			var id = user.Attribute("id")?.Value;
			if(id == null) { 
				var lastName = user.Attribute("prijmeni")?.Value;
				lastName = lastName == null ? string.Empty : $"(with last name {lastName})";
				Console.WriteLine($"User on line {index} {lastName} has no valid id.");				
				continue; 
			}
			var es = user.Descendants().Select(d => d.Attributes()).SelectMany(x => x);
			Dictionary<string, int> markers = new();
			foreach(var e in es ) {
				if(e.Value.Length > 0) {
					if(int.TryParse(e.Value.Split('.')[0], out int val))
						markers.Add(e.Name.LocalName, val);
					else {
						Console.WriteLine($"User of id {id} has invalid marker value: {e.Name.LocalName}={e.Value}");
					}
				}
			}
			if(!result.TryAdd(id, markers)) {
				Console.WriteLine($"Markers for user with id {id} (line {index}) are defined in the file multiple times.");
			}
		}
		return result;
	}

	/// <summary>
	/// Holds personal infromation about the user that is needed for its construction.
	/// </summary>
    record UserRepr(string? FamilyName, string? FirstName, string? Email, string? PhoneNumber, string? District, string? AdminNote, string? Municipality, OldAncestor? Ancestor, string Id);

	/// <summary>
	/// Finds all addresses within a given <paramref name="file"/>.
	/// </summary>
	/// <returns>Dictionary from users old ids to their addresses.</returns>
	private static Dictionary<string, OldAddress> ParseAddresses(string file) {
		Dictionary<string, OldAddress> result = new();
		var root = XElement.Load(file);
		int lane = -1;
		foreach(var item in root.Descendants()) {
			lane++;
			var id = item.Attribute("id")?.Value;
			if(id == null) {
				Console.WriteLine($"Id cannot be read from record number {lane} in addresses file.");
				continue;
			}
			var adStr = item.Attribute("address")?.Value;
			if(adStr == null) {
				Console.WriteLine($"Adress cannot be read from record id {id} in addresses file.");
				continue;
			}
			var parts = adStr.Split(',');
			if(parts.Length == 1) {
				if(adStr == "unknown") {
					Console.WriteLine($"Address not known for user of id {id}.");
				} else {
					Console.WriteLine($"Address format not supported for id {id} in addresses file."); 
				}
				continue;
			}
			string municipality = parts[1];
			int i = parts[0].LastIndexOf(" ");
			if(i == -1) {
				Console.WriteLine($"Street or number not present in address of {id} in addresses file.");
				continue;
			}
			var street = parts[0].Substring(0, i);
			var streetNum = parts[0].Substring(i+1);
			result.TryAdd(id, new OldAddress(
				town: municipality,
				street,
				streetNum,
				zipCode: null
				));
		}
		return result;
	}

	/** POSSIBLE VALUES OF ATTRIBUTES IN THE USER INFO FILE
USED:
	 prijmeni
	 id
	 krestni
	 mail
	 jmeno
	 tel
	 pozn
	 mail2 - chceme podporu pro vice emailu? zatim mam jen jeden
	 novymail
	
   	 okr
   	 obec
   	 OKR
	
   	 predek
   	 predekJmeno
   	 predekRok
   	 predekObec
   	 predekOkr
   	 predekOkres
SHOULD BE USED:
   	 uzPozn - chceme zachovat uzivatelskou poznamku?
	
*/
	/*
WHAT TODO WITH THIS?:
   	 pri - some alternative family name
   	 uzUcet - only 10 users have it, what is it?
   	 Skod - some random number...
   	 stareid - oldId, does it have any usecase? it seems that not, but just being sure about it
   	 likvidaceVzorku - values are 1/0 - do we need it?
   	 uzTest - again values 1/0 and again no clear meaning
   	 predekPozn - ancestor note - should those be saved? if so, it is not implemented yet

   	 posta - the address to the users post office
   	 poslat
   	 odberovkuposlat
   	 postaSada
USELESS:
   	 mailuzne
   	 postastara
   	 mailstary
   	 heslo
   	 pwd
   	 postaStara
   	 prijmeniSada
   	 krestniSada
   	 postaold
   	 mailold
   	 postastara20181018
   	 posta2019
   	 owner
   	 adrDoLabu - this is a date, supposedly when the kit arrived to the lab
   	 registrace
   	 predekObecLat
   	 predekObecLon
   	 souprava
**/

	private static readonly string testAttribute = "address";

	/// <summary>
	/// Helper function that prints statistics of given 
	/// <paramref name="attribute"/> from the <paramref name="file"/>.
	/// </summary>
	private static void TestAttributes(string file, string? attribute) {
		attribute = attribute ?? testAttribute;
		XElement root = XElement.Load(file);
		var users = root.Descendants();
		int line = -1;
		int seen = 0;
		int seenNonempty = 0;
		Console.WriteLine($"results for attribute {attribute}:");
		foreach(var item in users) {
			line++;
			var attr = item.Attribute(attribute);
			if(attr != null) {
				seen++;
				if(attr.Value.Length > 0) {
					Console.WriteLine($"{line}: {attr.Value} ({ReadName(item)})");
					seenNonempty++;
				}
			}
		}
		Console.WriteLine($"In total seen {seen} times. (including empty - nonempty were {seenNonempty})");
	}

	/// <summary>
	/// Helper function that writes all attribute names that exist in a given <paramref name="file"/>.
	/// </summary>
	private static void AllAttributes(string file) 
		=> AllAttributes(XElement.Load(file));
	
	/// <summary>
	/// Helper function that writes all attribute names that exist within a given xml <paramref name="element"/>.
	/// </summary>
	private static void AllAttributes(XElement element) {
		HashSet<string> list = new HashSet<string>();
		var users = element.Descendants();
		Console.WriteLine("possible attributes for " + element.Name.NamespaceName + ":");
		foreach(var item in users) {
			foreach(var attr in item.Attributes()) {
				if(attr != null && !list.Contains(attr.Name.LocalName)) {
					list.Add(attr.Name.LocalName);
					Console.Write(attr.Name.LocalName + ", ");
				}
			}
		}
	}

}
