﻿using System.Data;
using System.Text;
using ExcelDataReader;
using OldKuUserDataManager.OldUsers;
using Users.GenData.Parsers;

namespace OldKuUserDataManager;
internal class KuOldDataExcelParser {
    /// <summary>
    /// Parses user data from 2 files: excel with contact and ancestor data; csv with markers data
    /// </summary>
    /// <param name="idsCreator">The wrapper that creates user ids.</param>
	public static IList<OldUser> Parse(string userDataFilePath, string geneticDataFilePath) {
        var tmpDict = new Dictionary<string ,UserRepr>();
        List<UserReprPlus> final = new List<UserReprPlus>();

        System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

        using(var reader = ExcelReaderFactory.CreateReader(File.Open(userDataFilePath, FileMode.Open, FileAccess.Read), 
            new ExcelReaderConfiguration(){
            FallbackEncoding = Encoding.UTF8
        })) {
            reader.Read();
            while(reader.Read()) {
                var u = GetUser(reader);
                if(u == null)
                    continue;
                if(!tmpDict.TryAdd(u.Sample, u)) {
                    throw new DataException($"Two samples of the same name: {u.Sample}");
                }
            }
            reader.NextResult();
            reader.Read();
            int line = 0;
            while(reader.Read()) {
                line++;
                if(!TryGetIdAndAncestor(reader, out string id, out string sample, out OldAncestor anc)) {
                    Console.WriteLine($"Sample {sample} or Id {id} not parsed from contact table (line {line}).");
                    continue;
                }
                if(!tmpDict.TryGetValue(sample, out var user)) {
                    Console.WriteLine($"Sample {sample} is in the ancestors, but not in the contact table (line {line}).");
                    continue;
                };
                final.Add(new UserReprPlus(user, anc, id));
            }
        }
        return ParseAndAddGeneticData(final, geneticDataFilePath);
	}

    private static UserRepr? GetUser(IExcelDataReader reader) {
        string sample = reader.GetString(0);
        string familyName = reader.GetString(1);
        string firstName = reader.GetString(2);
        string email = reader.GetString(5);
        if(sample == null || familyName == null || email == null)
            return null;
        string addre = reader.GetString(6);
        OldAddress adress = ParseAdress(addre);
        return new UserRepr(familyName, firstName, email, adress, sample);
    }

    /// <summary>
    /// Parses the adress from given adress string (with line breaks as separators).
    /// </summary>
    /// <param name="address">The string</param>
    /// <returns>An Address object with no municipality and no county set.</returns>
    private static OldAddress ParseAdress(string address) {
        if(address == null)
            return new OldAddress();
        var lines = address.Split('\n')
            .Skip(1)
            .Select(s => s.Trim())
            .ToList();
        if(lines.Count != 2) { // adress has wrong number of lines
            return new OldAddress();
        }

        int sepInt = lines[0].LastIndexOf(' ');
        if(sepInt == -1) { // either street number or street name missing
            return new OldAddress();
        }
        var street = lines[0].Substring(0, sepInt);
        var number = lines[0].Substring(sepInt + 1);

        sepInt = lines[1].IndexOf(" ");
        if(sepInt == -1) { // either zip code or city name missing
            return new OldAddress();
        }
        if(sepInt < 5) { // the zip code is written with space in it
            if(lines[1].Length < 7)
                return new OldAddress();
            string zip2 = lines[1].Substring(sepInt + 1, 3);
            var sepInt2 = zip2.IndexOf(' ');
            if(sepInt2 == -1) { // the resulting zipcode is too long
                return new OldAddress();
            }
            sepInt += 1 + sepInt2;
        }
        var zipCode = lines[1].Substring(0, sepInt);
        zipCode = zipCode.Replace(" ", "");
        var town = lines[1].Substring(sepInt + 1);
        return new OldAddress(
            town: town,
            street: street,
            streetNumber: number,
            zipCode: zipCode);
    }
    
    private static bool TryGetIdAndAncestor(IExcelDataReader reader, out string id, out string sample, out OldAncestor ancestor) {
        sample = reader.GetString(1);
        id = reader.GetString(12);
        if(sample == null || id == null) {
            ancestor = default;
            return false;
        }
        string? year = reader.GetValue(11)?.ToString();
        string place = $"{reader.GetString(9)},{reader.GetString(8)}";
        ancestor = new OldAncestor(
            givenName: null, 
            familyName: null,
            placeOfDeath: place,
            approximateDeathYear: year ?? null);
        return true;
    }

    record UserRepr(string FamilyName, string FirstName, string Email, OldAddress Address, string Sample);

    record UserReprPlus(UserRepr pre, OldAncestor Ancestor, string GenebazeId) 
        : UserRepr(pre.FamilyName, pre.FirstName, pre.Email, pre.Address, pre.Sample);

    private static List<OldUser> ParseAndAddGeneticData(List<UserReprPlus> users, string fileName) {
        List<string>? markerNames;
        Dictionary<string, List<string>> values = new();
        using (StreamReader sr = new StreamReader(fileName)) {
            markerNames = sr.ReadLine()
                ?.Split(',')
                .Skip(1)
                .Select(s => s.Substring(1, s.Length - 2))
                .ToList();
            if(markerNames == null) {
                throw new ArgumentException("The given genetic data file is empty.");
            }
            while(!sr.EndOfStream) {
                AddOneRecord(values, sr.ReadLine());
            }
        }
        List<OldUser> result = new List<OldUser>();
        int wrong = 0;
        int wrongEmails = 0;
        foreach(var u in users) {
            Dictionary<string, int> markers = new Dictionary<string, int>();
            List<string>? vals;
            if(!values.TryGetValue(u.GenebazeId, out vals)) {
                Console.WriteLine($"User with id {u.GenebazeId} does not have a genetic data record.");
                wrong++;
                continue;
            }
            if(vals.Count != markerNames.Count) {
                Console.WriteLine($"Marker count different in definition and in values for id:{u.GenebazeId}");
                wrong++;
                continue;
            }
            for(int i = 0; i < markerNames.Count; i++) {
                ParsingUtils.MarkersFromOneInput(markers, markerNames[i], vals[i]);
            }
            if(u.Email == null || !u.Email.Contains('@')){
                Console.WriteLine($"bad or no email for user {u.FamilyName} (id {u.Sample}).");
                wrongEmails++;
            }
            result.Add(new OldUser(
                new OldProfile(
                    u.FirstName,
                    u.FamilyName,
                    u.Email,
                    phoneNumber: null,
                    u.Address,
                    birthDate: null),
                u.Ancestor,
                orpRegionName: null,
                new OldGeneticData(
                    markers, 
                    haplogroup: null),
                adminNote: null
                ));
        }
        Console.WriteLine($"there were in total {wrong} users with no or bad genetic data records.");
        Console.WriteLine($"And {wrongEmails} wrong emails.");
        return result;
    }

    private static void AddOneRecord(Dictionary<string, List<string>> into, string? line) {
        if(line == null)
            return;
        var words = line.Split(',');
        var name = words[0];
        if(name[0] == '"')
            name = name.Substring(1);
        if(name.EndsWith('"'))
            name = name.Substring(0, name.Length - 1);
        if(!into.TryAdd(name, words.Skip(1).ToList())) {
            throw new DataException($"The file with genetic data includes sample {words[0]} twice.");
        }
    }
}
