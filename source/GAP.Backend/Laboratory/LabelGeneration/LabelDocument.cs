using Laboratory.LabelGeneration.LabelLayout;
using Laboratory.LabelGeneration.Labels;
using QuestPDF.Drawing;
using QuestPDF.Fluent;
using QuestPDF.Helpers;
using QuestPDF.Infrastructure;

namespace Laboratory.LabelGeneration; 

public class LabelDocument : IDocument {

	private const float PointToMillimetersScale = 2.8346457f;
	private const float LabelBorder = 0.1f;

	private readonly ILabelLayout _layout;
	private readonly IReadOnlyList<ILabel> _labels;
	private readonly string _font;

	public LabelDocument(ILabelLayout layout, IReadOnlyList<ILabel> labels, string font) {
		_layout = layout;
		_labels = labels;
		_font = font;
	}

	public DocumentMetadata GetMetadata() {
		return DocumentMetadata.Default;
	}

	public void Compose(IDocumentContainer container) {

		container
			.Page(page => { 
				page.DefaultTextStyle(style => style.FontFamily(_font));
				
				page.Header().Column(column => {
					column.Item().Background(Colors.White).Height(_layout.TopIndentation, Unit.Millimetre);
				});
				page.Footer().Column(column => {
					column.Item().Background(Colors.White).Height(_layout.BottomIndentation, Unit.Millimetre);
				});
				
				page.Content().Column(column => {
					using IEnumerator<ILabel> labelEnumerator = _labels.GetEnumerator();

					int numberOfRows = _labels.Count / _layout.Columns;
					for (int i = 0; i < numberOfRows; ++i) {
						AddRow(column: column, startIndex: i * _layout.Columns, numberOfLabels: _layout.Columns);
					}

					int lastRowLabels = _labels.Count % _layout.Columns;
					if (lastRowLabels != 0) {
						AddRow(column: column, startIndex: numberOfRows * _layout.Columns, numberOfLabels: lastRowLabels);
					}
				});
			});
	}
	
	private void AddRow(ColumnDescriptor column, int startIndex, int numberOfLabels) {
		column.Item()
			.PaddingLeft(_layout.LeftIndentation, Unit.Millimetre)
			.PaddingRight(_layout.RightIndentation, Unit.Millimetre)
			.Row(row => {
				row.Spacing(_layout.ColumnGap * PointToMillimetersScale);
				for (int i = 0; i < numberOfLabels; ++i) {
					AddLabel(row: row, label: _labels[startIndex + i]);
				}
			});
	}
	
	private void AddLabel(RowDescriptor row, ILabel label) {
		var labelContainer = row.ConstantItem(size: _layout.LabelWidth, Unit.Millimetre)
			.Height(_layout.LabelHeight, Unit.Millimetre)
			.Border(LabelBorder, Unit.Millimetre)
			.AlignCenter()
			.AlignMiddle()
			.ScaleToFit();
		label.AddLabelContent(labelContainer);
	}
	
}