using Laboratory.KitManagement.Kits;
using Laboratory.KitManagement.Storage;
using Laboratory.LabelGeneration.Exceptions;
using Laboratory.LabelGeneration.LabelLayout;
using Laboratory.LabelGeneration.Labels;
using Microsoft.Extensions.Options;
using QuestPDF.Fluent;
using Users.UserAccess;
using Users.UserAccess.Entities;

namespace Laboratory.LabelGeneration;

public class LabelController {
	private readonly IKitFetcher<User> _kitFetcher;
	private readonly LabelGenerationConfig _labelGenerationConfig;
	private readonly ILayoutFetcher _layoutFetcher;

	public LabelController(IKitFetcher<User> kitFetcher, IOptions<LaboratoryConfig> laboratoryConfig) {
		_kitFetcher = kitFetcher;
		_labelGenerationConfig = laboratoryConfig.Value.LabelGenerationConfig;
		_layoutFetcher = new JsonLayoutFetcher(layoutDirectory: _labelGenerationConfig.LabelLayoutDirectory);
	}

	public async Task<byte[]> GenerateLabelsAsync(List<int> kitIds, UserLabelSelection userLabelSelection, string labelLayout) {
		CheckLabelCount(userLabelSelection: userLabelSelection);

		IReadOnlyCollection<Kit<User>> kits = (await _kitFetcher.FindKitsById(kitIds: kitIds)
			.ConfigureAwait(false)).Values.ToList();

		ILabelLayout layout = await _layoutFetcher.GetLayoutAsync(layoutName: labelLayout)
			.ConfigureAwait(false);

		List<ILabel> labels = CreateLabels(kits: kits, userLabelSelection: userLabelSelection, layout: layout);
		var document = new LabelDocument(layout: layout, labels: labels, font: _labelGenerationConfig.Font);
		return document.GeneratePdf();
	}

	private List<ILabel> CreateLabels(IReadOnlyCollection<Kit<User>> kits, UserLabelSelection userLabelSelection, ILabelLayout layout) {
		var labelFactory = new LabelFactory(kits);

		return new List<ILabel>().Concat(
			labelFactory.CreateAddressLabels(labelsPerUser: userLabelSelection.AddressLabel)
		).Concat(
			labelFactory.CreateSampleLabels(
				labelsPerUser: userLabelSelection.SampleLabel,
				barcodeFormat: _labelGenerationConfig.Barcode,
				labelSize: new Size((int)layout.LabelHeight, (int)layout.LabelWidth)
			)
		).OrderBy(label => label.KitId).ToList();
	}

	private void CheckLabelCount(UserLabelSelection userLabelSelection) {
		if (userLabelSelection.AddressLabel < 0 || userLabelSelection.AddressLabel > _labelGenerationConfig.MaxOneTypeLabels) {
			throw new InvalidLabelSelectionException(
				$"Request argument number of address labels is not between 0 and {_labelGenerationConfig.MaxOneTypeLabels} but {userLabelSelection.AddressLabel}"
			);
		}

		if (userLabelSelection.SampleLabel < 0 || userLabelSelection.SampleLabel > _labelGenerationConfig.MaxOneTypeLabels) {
			throw new InvalidLabelSelectionException(
				$"Request argument number of sample labels is not between 0 and {_labelGenerationConfig.MaxOneTypeLabels} but {userLabelSelection.SampleLabel}"
			);
		}
	}
}