using QuestPDF.Infrastructure;

namespace Laboratory.LabelGeneration.Labels; 

public interface ILabel {
	int KitId { get; }
	void AddLabelContent(IContainer container);
}