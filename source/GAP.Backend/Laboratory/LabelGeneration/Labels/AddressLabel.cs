using Laboratory.KitManagement.Kits;
using QuestPDF.Fluent;
using QuestPDF.Infrastructure;
using Users.UserAccess.Entities;

namespace Laboratory.LabelGeneration.Labels; 

public class AddressLabel : ILabel {
	private readonly Kit<User> _kit;
	
	public AddressLabel(Kit<User> kit) {
		_kit = kit;
	}

	public int KitId => _kit.Id;

	public void AddLabelContent(IContainer container) {
		Profile userProfile = _kit.User.Profile;
		container.Text(text => {
			text.Line(" ");
			text.Line($"{userProfile.GivenName} {userProfile.FamilyName}");
			if (userProfile.CorrespondenceAddress != null) {
				text.Line($"{userProfile.CorrespondenceAddress.Street}");
				text.Line($"{userProfile.CorrespondenceAddress.ZipCode} {userProfile.CorrespondenceAddress.Town}");
			}
			else {
				text.Line($"{userProfile.ResidenceAddress.Street}");
				text.Line($"{userProfile.ResidenceAddress.ZipCode} {userProfile.ResidenceAddress.Town}");
			}
		});
	}
}