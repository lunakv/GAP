using Laboratory.KitManagement.Kits;
using QuestPDF.Fluent;
using QuestPDF.Infrastructure;
using Users.UserAccess.Entities;

namespace Laboratory.LabelGeneration.Labels; 

public class KitIdentificationLabel : ILabel {
	private readonly byte[] _barcode;
	private readonly Kit<User> _kit;
	
	public KitIdentificationLabel(byte[] barcode, Kit<User> kit) {
		_barcode = barcode;
		_kit = kit;
	}

	public int KitId => _kit.Id;

	public void AddLabelContent(IContainer container) {
		container.Column(column => {
			column.Item().Image(_barcode, ImageScaling.FitArea);
			column.Item().AlignCenter().Text($"Id: {_kit.Id.ToString()}");
			column.Item().AlignCenter().Text($"{_kit.User.Profile.GivenName} {_kit.User.Profile.FamilyName}");
		});
	}
}