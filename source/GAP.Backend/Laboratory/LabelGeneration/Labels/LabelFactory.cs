using Laboratory.KitManagement.Kits;
using Laboratory.LabelGeneration.Barcodes;
using SixLabors.ImageSharp.Formats.Png;
using Users.UserAccess.Entities;

namespace Laboratory.LabelGeneration.Labels; 

public class LabelFactory {
	private readonly IReadOnlyCollection<Kit<User>> _kits;

	private const int BarcodeRelativeHeightDenominator = 3;

	public LabelFactory(IReadOnlyCollection<Kit<User>> kits) {
		_kits = kits;
	}

	public IList<ILabel> CreateAddressLabels(int labelsPerUser) {
		var labels = new List<ILabel>();
		foreach (Kit<User> kit in _kits) {
			for (int i = 0; i < labelsPerUser; ++i) {
				labels.Add(new AddressLabel(kit: kit));
			}
		}

		return labels;
	}
	
	public IList<ILabel> CreateSampleLabels(int labelsPerUser, string barcodeFormat, Size labelSize) {
		var labels = new List<ILabel>();
		var barcodeGenerator = new BarcodeGenerator(
			barcodeFormat: BarcodeMapper.GetBarcode(barcodeFormat),
			size: new Size(labelSize.Height / BarcodeRelativeHeightDenominator, labelSize.Width),
			imageEncoder: new PngEncoder()
		);
		
		foreach (Kit<User> kit in _kits) {
			for (int i = 0; i < labelsPerUser; ++i) {
				labels.Add(new KitIdentificationLabel(
					barcode: barcodeGenerator.GenerateBarcode(content: kit.Id.ToString()),
					kit: kit
				));
			}
		}

		return labels;
	}
}