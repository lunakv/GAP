namespace Laboratory.LabelGeneration; 

public class UserLabelSelection {
	public int AddressLabel { get; }
	public int SampleLabel { get; }
	
	public UserLabelSelection(int addressLabel, int sampleLabel) {
		AddressLabel = addressLabel;
		SampleLabel = sampleLabel;
	}
}