namespace Laboratory.LabelGeneration; 

public class LabelGenerationConfig {
	public string LabelLayoutDirectory { get; set; }
	public string Barcode { get; set; }
	public int MaxOneTypeLabels { get; set; }
	public string Font { get; set; }

	public LabelGenerationConfig() {
		// Suppress warnings by assigning arbitrary values.
		LabelLayoutDirectory = string.Empty;
		Barcode = string.Empty;
		MaxOneTypeLabels = 0;
		Font = string.Empty;
	}
}