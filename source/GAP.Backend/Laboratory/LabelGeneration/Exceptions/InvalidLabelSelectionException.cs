using Laboratory.Exceptions;

namespace Laboratory.LabelGeneration.Exceptions; 

public class InvalidLabelSelectionException : LaboratoryUserException {
	public InvalidLabelSelectionException(string message) : base(message) { }
	public override string GetRepresentation => nameof(InvalidLabelSelectionException);
}