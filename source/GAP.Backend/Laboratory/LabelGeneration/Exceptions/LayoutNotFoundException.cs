using Laboratory.Exceptions;

namespace Laboratory.LabelGeneration.Exceptions; 

public class LayoutNotFoundException : LaboratoryUserException {
	public LayoutNotFoundException(string message) : base(message) { }
	public override string GetRepresentation => nameof(LayoutNotFoundException);
}