using Laboratory.Exceptions;

namespace Laboratory.LabelGeneration.Exceptions; 

public class InvalidBarcodeTypeException : LaboratoryException {
	public InvalidBarcodeTypeException(string message) : base(message) { }
}