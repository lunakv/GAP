namespace Laboratory.LabelGeneration.LabelLayout; 

public interface ILabelLayout {
	public float LabelWidth { get; }
	public float LabelHeight { get; }
	public int Columns { get; }
	public int Rows { get; }
	public float TopIndentation { get; }
	public float BottomIndentation { get; }
	public float LeftIndentation { get; }
	public float RightIndentation { get; }
	public float ColumnGap { get; }
	public float RowGap { get; }
}


public class Arch3X7Layout : ILabelLayout {
	public float LabelWidth => 63.5f;
	public float LabelHeight => 38.1f;
	public int Columns => 3;
	public int Rows => 7;
	public float TopIndentation => 15.15f;
	public float BottomIndentation => 15.15f;
	public float LeftIndentation => 7.25f;
	public float RightIndentation => 7.25f;
	public float ColumnGap => 2.5f;
	public float RowGap => 0.0f;
}

public class Arch3X9Layout : ILabelLayout {
	public float LabelWidth => 63.5f;
	public float LabelHeight => 29.6f;
	public int Columns => 3;
	public int Rows => 9;
	public float TopIndentation => 15.3f;
	public float BottomIndentation => 15.3f;
	public float LeftIndentation => 7.75f;
	public float RightIndentation => 7.75f;
	public float ColumnGap => 2f;
	public float RowGap => 0.0f;
}

public class Arch3X92Layout : ILabelLayout {
	public float LabelWidth => 68f;
	public float LabelHeight => 32f;
	public int Columns => 3;
	public int Rows => 9;
	public float TopIndentation => 3.5f;
	public float BottomIndentation => 3.5f;
	public float LeftIndentation => 1f;
	public float RightIndentation => 1f;
	public float ColumnGap => 0f;
	public float RowGap => 0.0f;
}