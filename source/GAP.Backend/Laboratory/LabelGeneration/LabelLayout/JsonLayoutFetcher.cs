using System.Text.Json;
using Laboratory.LabelGeneration.Exceptions;

namespace Laboratory.LabelGeneration.LabelLayout;

public class JsonLayoutFetcher : ILayoutFetcher {
	private readonly string _layoutDirectory;

	public JsonLayoutFetcher(string layoutDirectory) {
		_layoutDirectory = layoutDirectory;
	}

	public async Task<ILabelLayout> GetLayoutAsync(string layoutName) {
		var files = Directory.EnumerateFiles(_layoutDirectory);
		string? layoutPath = files
			.FirstOrDefault(predicate: file => Path.GetFileNameWithoutExtension(file) == layoutName, defaultValue: null);

		if (layoutPath == null) {
			throw new LayoutNotFoundException($"Layout {layoutName} not found as json file in {_layoutDirectory}");
		}

		await using var fs = new FileStream(
			path: layoutPath,
			options: new FileStreamOptions() {
				Access = FileAccess.Read,
				Mode = FileMode.Open,
				Options = FileOptions.Asynchronous,
				Share = FileShare.Read
			}
		);
		JsonLabelLayout? layout = await JsonSerializer.DeserializeAsync<JsonLabelLayout>(utf8Json: fs)
			.ConfigureAwait(false);

		if (layout == null) {
			throw new LayoutNotFoundException(
				$"Layout {layoutName} could not be put in class {nameof(JsonLabelLayout)}"
			);
		}

		return layout;
	}

	public IList<string> GetLayoutNames() {
		return Directory
			.EnumerateFiles(_layoutDirectory)
			.Select(Path.GetFileNameWithoutExtension)
			.Where(file => file != null).Cast<string>()
			.ToList();
	}
}