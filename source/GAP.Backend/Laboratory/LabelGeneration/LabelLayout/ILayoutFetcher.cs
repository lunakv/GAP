namespace Laboratory.LabelGeneration.LabelLayout; 

public interface ILayoutFetcher {
	Task<ILabelLayout> GetLayoutAsync(string layoutName);
	IList<string> GetLayoutNames();
}