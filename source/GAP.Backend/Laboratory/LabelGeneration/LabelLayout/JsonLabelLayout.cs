namespace Laboratory.LabelGeneration.LabelLayout;

public class JsonLabelLayout : ILabelLayout {
	public float LabelWidth { get; }
	public float LabelHeight { get; }
	public int Columns { get; }
	public int Rows { get; }
	public float TopIndentation { get; }
	public float BottomIndentation { get; }
	public float LeftIndentation { get; }
	public float RightIndentation { get; }
	public float ColumnGap { get; }
	public float RowGap { get; }

	public JsonLabelLayout(float labelWidth, float labelHeight, int columns, int rows, float topIndentation,
		float bottomIndentation, float leftIndentation, float rightIndentation, float columnGap, float rowGap) {
		LabelWidth = labelWidth;
		LabelHeight = labelHeight;
		Columns = columns;
		Rows = rows;
		TopIndentation = topIndentation;
		BottomIndentation = bottomIndentation;
		LeftIndentation = leftIndentation;
		RightIndentation = rightIndentation;
		ColumnGap = columnGap;
		RowGap = rowGap;
	}
}