using Laboratory.LabelGeneration.Exceptions;
using ZXing;

namespace Laboratory.LabelGeneration.Barcodes;

public class BarcodeMapper {
	public static BarcodeFormat GetBarcode(string barcode) {
		BarcodeFormat format = barcode switch {
			"Code 39" => BarcodeFormat.CODE_39,
			"Code 93" => BarcodeFormat.CODE_93,
			"QR Code" => BarcodeFormat.QR_CODE,
			"EAN 8" => BarcodeFormat.EAN_8,
			"EAN 13" => BarcodeFormat.EAN_13,
			"IMB" => BarcodeFormat.IMB,
			"ITF" => BarcodeFormat.ITF,
			"MSI" => BarcodeFormat.MSI,
			"PDF 417" => BarcodeFormat.PDF_417,
			"RSS 14" => BarcodeFormat.RSS_14,
			"UPC A" => BarcodeFormat.UPC_A,
			"UPC E" => BarcodeFormat.UPC_E,
			"Aztec" => BarcodeFormat.AZTEC,
			"Codabar" => BarcodeFormat.CODABAR,
			"Plessey" => BarcodeFormat.PLESSEY,
			"Maxicode" => BarcodeFormat.MAXICODE,
			"Data Matrix" => BarcodeFormat.DATA_MATRIX,
			"Pharma Code" => BarcodeFormat.PHARMA_CODE,
			"RSS Expanded" => BarcodeFormat.RSS_EXPANDED,
			"UPC EAN Extension" => BarcodeFormat.UPC_EAN_EXTENSION,
			_ => throw new InvalidBarcodeTypeException($"Barcode type: {barcode} is unknown")
		};

		return format;
	}
}