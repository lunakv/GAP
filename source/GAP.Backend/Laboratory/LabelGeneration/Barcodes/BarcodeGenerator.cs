using Laboratory.LabelGeneration.Labels;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.PixelFormats;
using ZXing;
using ZXing.Common;

namespace Laboratory.LabelGeneration.Barcodes; 

public class BarcodeGenerator {
	private readonly ZXing.ImageSharp.BarcodeWriter<Rgba32> _barcodeWriter;
	private readonly IImageEncoder _imageEncoder;
	
	public BarcodeGenerator(BarcodeFormat barcodeFormat, Size size, IImageEncoder imageEncoder) {
		_barcodeWriter = new ZXing.ImageSharp.BarcodeWriter<Rgba32>() {
			Format = barcodeFormat,
			Options = new EncodingOptions {
				Height = size.Height,
				Width = size.Width,
				PureBarcode = true,
			}
		};
		_imageEncoder = imageEncoder;
	}

	public byte[] GenerateBarcode(string content) {
		var barcode = _barcodeWriter.Write(contents: content);
		var ms = new MemoryStream();
		barcode.Save(stream: ms, encoder: _imageEncoder);
		return ms.ToArray();
	}
}