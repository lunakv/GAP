using Laboratory.KitManagement.Kits;
using Users.UserAccess.Entities.Interfaces;

namespace Laboratory.KitManagement.Storage; 

public interface IKitSaver<TUser> where TUser : IHasUserId {
	Task AddKitAsync(Kit<TUser> kit);
	Task AddKitsAsync(IReadOnlyCollection<Kit<TUser>> kits);
	Task UpdateKitsAsync(IReadOnlyCollection<Kit<TUser>> kits);
}