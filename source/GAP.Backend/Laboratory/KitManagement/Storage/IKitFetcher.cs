using Laboratory.KitManagement.Kits;
using Users.UserAccess.Entities.Interfaces;

namespace Laboratory.KitManagement.Storage; 

public interface IKitFetcher<TUser> where TUser : IHasUserId {
	Task<IReadOnlyCollection<Kit<TUser>>> GetKitsAsync(KitStorageFilter? filter);
	Task<IReadOnlyDictionary<int, Kit<TUser>>> FindKitsById(IReadOnlyCollection<int> kitIds);
}