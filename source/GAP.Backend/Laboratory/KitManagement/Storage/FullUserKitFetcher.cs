using Laboratory.KitManagement.Kits;
using Users.UserAccess;
using Users.UserAccess.Entities;
using Users.UserAccess.Entities.Interfaces;
using Users.UserAccess.Storage;

namespace Laboratory.KitManagement.Storage; 

public class FullUserKitFetcher : IKitFetcher<User> {

	private readonly IKitFetcher<IHasUserId> _kitFetcher;
	private readonly IUserFetcher _userFetcher;

	public FullUserKitFetcher(IKitFetcher<IHasUserId> kitFetcher, IUserFetcher userFetcher) {
		_kitFetcher = kitFetcher;
		_userFetcher = userFetcher;
	}

	public async Task<IReadOnlyCollection<Kit<User>>> GetKitsAsync(KitStorageFilter? filter) {
		IReadOnlyCollection<Kit<IHasUserId>> kits = await _kitFetcher.GetKitsAsync(filter)
			.ConfigureAwait(false);

		IReadOnlySet<int> userIds = kits.Select(kit => kit.User.Id).ToHashSet();

		IReadOnlyDictionary<int, User> users = await _userFetcher.FindUsersByIdAsync(userIds);
		
		return kits.Select(kit => kit.CloneWithDifferentUserType(users[kit.User.Id])).ToList();
	}

	public async Task<IReadOnlyDictionary<int, Kit<User>>> FindKitsById(IReadOnlyCollection<int> kitIds) {
		IReadOnlyDictionary<int, Kit<IHasUserId>> kits = await _kitFetcher.FindKitsById(kitIds)
			.ConfigureAwait(false);
		
		IReadOnlySet<int> userIds = kits.Values.Select(kit => kit.User.Id).ToHashSet();
		
		IReadOnlyDictionary<int, User> users = await _userFetcher.FindUsersByIdAsync(userIds);
		
		return kits.Values.Select(kit => kit.CloneWithDifferentUserType(users[kit.User.Id])).ToDictionary(kit => kit.Id, kit => kit);
	}
}