namespace Laboratory.KitManagement.Storage; 

public class KitStorageFilter {
	public bool? ActionRequired { get; }
	public bool? Active { get; }

	public KitStorageFilter(bool? actionRequired, bool? active) {
		ActionRequired = actionRequired;
		Active = active;
	}
}