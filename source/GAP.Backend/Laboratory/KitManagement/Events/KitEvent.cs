namespace Laboratory.KitManagement.Events; 

public abstract class KitEvent {
	public DateTime Date { get; }
	public string Note { get; }

	public enum Event {
		Create,
		Send,
		Receive,
		Analyse
	}

	public KitEvent(DateTime date, string note) {
		Date = date;
		Note = note;
	}

	public abstract Event GetEvent();
}