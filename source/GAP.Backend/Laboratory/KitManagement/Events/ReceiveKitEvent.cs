namespace Laboratory.KitManagement.Events;

public class ReceiveKitEvent : KitEvent {
	public ReceiveKitEvent(DateTime date, string note) : base(date, note) { }

	public override Event GetEvent() {
		return Event.Receive;
	}
}