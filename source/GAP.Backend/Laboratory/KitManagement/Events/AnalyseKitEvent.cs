namespace Laboratory.KitManagement.Events;

public class AnalyseKitEvent : KitEvent {
	public AnalyseKitEvent(DateTime date, string note) : base(date, note) { }

	public override Event GetEvent() {
		return Event.Analyse;
	}
}