namespace Laboratory.KitManagement.Events;

public class SendKitEvent : KitEvent {
	public SendKitEvent(DateTime date, string note) : base(date, note) { }

	public override Event GetEvent() {
		return Event.Send;
	}
}