namespace Laboratory.KitManagement.Events;

public class CreateKitEvent : KitEvent {
	public CreateKitEvent(DateTime date, string note) : base(date, note) { }

	public override Event GetEvent() {
		return Event.Create;
	}
}