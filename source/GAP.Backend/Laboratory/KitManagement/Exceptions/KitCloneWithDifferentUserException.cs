using Laboratory.Exceptions;

namespace Laboratory.KitManagement.Exceptions; 

public class KitCloneWithDifferentUserException : LaboratoryException {
	public KitCloneWithDifferentUserException(string message) : base(message) { }
}