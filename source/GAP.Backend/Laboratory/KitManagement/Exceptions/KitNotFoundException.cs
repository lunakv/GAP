using Laboratory.Exceptions;

namespace Laboratory.KitManagement.Exceptions; 

public class KitNotFoundException : LaboratoryUserException {
	public KitNotFoundException(string message) : base(message) { }
	public override string GetRepresentation => nameof(KitNotFoundException);
}