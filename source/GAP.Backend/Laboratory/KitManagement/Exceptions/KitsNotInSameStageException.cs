using Laboratory.Exceptions;

namespace Laboratory.KitManagement.Exceptions;

public class KitsNotInSameStageException : LaboratoryUserException {
	public KitsNotInSameStageException(string message) : base(message) { }
	public override string GetRepresentation => nameof(KitsNotInSameStageException);
}