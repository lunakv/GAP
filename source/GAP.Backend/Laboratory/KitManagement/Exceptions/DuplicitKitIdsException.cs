using Laboratory.Exceptions;

namespace Laboratory.KitManagement.Exceptions; 

public class DuplicitKitIdsException : LaboratoryUserException {
	public DuplicitKitIdsException(string message) : base(message) { }
	public override string GetRepresentation => nameof(DuplicitKitIdsException);
}