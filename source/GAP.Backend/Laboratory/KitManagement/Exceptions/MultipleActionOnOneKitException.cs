using Laboratory.Exceptions;

namespace Laboratory.KitManagement.Exceptions; 

public class MultipleActionOnOneKitException : LaboratoryUserException {
	public MultipleActionOnOneKitException(string message) : base(message) { }
	public override string GetRepresentation => nameof(MultipleActionOnOneKitException);
}