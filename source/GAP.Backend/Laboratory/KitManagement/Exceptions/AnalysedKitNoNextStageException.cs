using Laboratory.Exceptions;

namespace Laboratory.KitManagement.Exceptions; 

public sealed class AnalysedKitNoNextStageException : LaboratoryUserException {
	public AnalysedKitNoNextStageException(string message) : base(message) { }
	public override string GetRepresentation => nameof(AnalysedKitNoNextStageException);
}