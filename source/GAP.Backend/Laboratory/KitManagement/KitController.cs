using System.Text;
using Laboratory.KitManagement.Exceptions;
using Laboratory.KitManagement.Kits;
using Laboratory.KitManagement.KitStageAdvancement;
using Laboratory.KitManagement.Storage;
using Users.UserAccess.Entities.Interfaces;

namespace Laboratory.KitManagement;

public class KitController {
	private readonly IKitFetcher<IHasUserId> _kitFetcher;
	private readonly IKitSaver<IHasUserId> _kitSaver;

	public KitController(IKitSaver<IHasUserId> kitSaver, IKitFetcher<IHasUserId> kitFetcher) {
		_kitFetcher = kitFetcher;
		_kitSaver = kitSaver;
	}

	public Task<IReadOnlyCollection<Kit<IHasUserId>>> GetKitsAsync(KitStorageFilter? filter) {
		return _kitFetcher.GetKitsAsync(filter);
	}

	public async Task<IReadOnlyCollection<Kit<IHasUserId>>> AdvanceKitsToNextStage(IReadOnlyCollection<KitAction> kitActions) {
		if (kitActions.DistinctBy(action => action.KitId).Count() != kitActions.Count) {
			throw new MultipleActionOnOneKitException($"Detected multiple actions to be performed on one kit.");
		}

		var kitIds = kitActions.Select(action => action.KitId).ToList();
		IReadOnlyDictionary<int, Kit<IHasUserId>> kits = await _kitFetcher.FindKitsById(kitIds)
			.ConfigureAwait(false);

		IReadOnlyCollection<Kit<IHasUserId>> advancedKits = new KitStageAdvancer<IHasUserId>().AdvanceKits(
			kits: kits,
			kitActions: kitActions
		);

		await _kitSaver.UpdateKitsAsync(kits: advancedKits)
			.ConfigureAwait(false);

		return advancedKits;
	}

	public async Task<Kit<IHasUserId>> UpdateKitAsync(int kitId, string note) {
		IReadOnlyDictionary<int, Kit<IHasUserId>> kits = await _kitFetcher.FindKitsById(new[] { kitId })
			.ConfigureAwait(false);
		Kit<IHasUserId> kit = kits[kitId];
		kit.Note = note;
		await _kitSaver.UpdateKitsAsync(new[] { kit }).ConfigureAwait(false);
		return kit;
	}
}