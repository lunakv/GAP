namespace Laboratory.KitManagement.KitStageAdvancement;

public class KitAction {
	public int KitId { get; }
	public string Note { get; }

	public KitAction(int kitId, string note) {
		KitId = kitId;
		Note = note;
	}
}