using System.Text;
using Laboratory.KitManagement.Exceptions;
using Laboratory.KitManagement.Kits;
using Users.UserAccess.Entities.Interfaces;

namespace Laboratory.KitManagement.KitStageAdvancement;

public class KitStageAdvancer<TUser> where TUser : IHasUserId {
	public IReadOnlyCollection<Kit<TUser>> AdvanceKits(IReadOnlyDictionary<int, Kit<TUser>> kits, IReadOnlyCollection<KitAction> kitActions) {
		if (kits.Count == 0) {
			return new List<Kit<TUser>>();
		}

		var kitType = kits.First().Value.GetType();

		bool anyMismatchingKitType = kits.Select(kit => kit.Value.GetType()).Any(type => kitType != type);
		if (anyMismatchingKitType) {
			ThrowMismatchException(kits: kits);
		}

		List<Kit<TUser>> advancedKits = kitActions
			.Select(action => kits[action.KitId].CreateKitInNextStage(note: action.Note))
			.ToList();

		return advancedKits;
	}

	public IReadOnlyCollection<Kit<TUser>> AdvanceKitsToAnalysedStage(ICollection<Kit<TUser>> kits) {
		if (kits.Count == 0) {
			return new List<Kit<TUser>>();
		}

		List<Kit<TUser>> outputKits = new List<Kit<TUser>>();
		foreach (Kit<TUser> kit in kits) {
			Kit<TUser> outputKit = kit;
			while (outputKit is not AnalysedKit<TUser>) {
				outputKit = outputKit.CreateKitInNextStage(note: string.Empty);
			}
			outputKits.Add(outputKit);
		}

		return outputKits;
	}

	private void ThrowMismatchException(IReadOnlyDictionary<int, Kit<TUser>> kits) {
		string kitsInfoJoined = new StringBuilder()
			.AppendJoin(
				separator: ",",
				values: kits.Select(kit => $"{kit.Value.Id}:{kit.Value.GetType()}")
			).ToString();
		throw new KitsNotInSameStageException(
			$"Kits to be advanced to next stage are in different stages: {kitsInfoJoined}.");
	}
}