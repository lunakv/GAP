using Laboratory.KitManagement.Events;
using Laboratory.KitManagement.Exceptions;
using Users.UserAccess.Entities.Interfaces;

namespace Laboratory.KitManagement.Kits;

public abstract class Kit<TUser> where TUser : IHasUserId {
	public int Id { get; }
	public IReadOnlyCollection<KitEvent> History { get; }
	public TUser User { get; }
	public string Note { get; set; }
	public abstract KitStage Stage { get; }
	public abstract bool ActionRequired { get; }
	public abstract bool Active { get; }

	private const string SampleNamePrefix = "GAPNG-";

	public enum KitStage {
		Pending,
		Sent,
		Received,
		Analysed
	};

	protected Kit(int id, IReadOnlyCollection<KitEvent> history, TUser user, string note) {
		Id = id;
		History = history;
		User = user;
		Note = note;
	}

	public string GetSampleName() => SampleNamePrefix + Id;

	public static int GetKitId(string sampleName) {
		return int.Parse(sampleName[SampleNamePrefix.Length..]);
	}

	internal abstract Kit<TUser> CreateKitInNextStage(string note);

	internal abstract Kit<TDifferentUser> CloneWithDifferentUserType<TDifferentUser>(TDifferentUser user)
		where TDifferentUser : IHasUserId;

	protected void CloneSameUserCheck(IHasUserId user) {
		if (User.Id != user.Id) {
			throw new KitCloneWithDifferentUserException(
				$"Kit {Id} with user {User.Id} was attempted to be clone with {user.Id}");
		}
	}

	protected IReadOnlyCollection<KitEvent> AddEventToHistory(KitEvent kitEvent) {
		List<KitEvent> history = History.ToList();
		history.Add(kitEvent);
		return history;
	}
}