using Laboratory.KitManagement.Events;
using Users.UserAccess.Entities.Interfaces;

namespace Laboratory.KitManagement.Kits;

public class PendingKit<TUser> : Kit<TUser> where TUser : IHasUserId {
	public override bool ActionRequired => true;
	public override bool Active => true;

	public PendingKit(int id, string createKitNote, TUser user, string note) : base(
		id: id,
		history: new List<KitEvent>() {
			new CreateKitEvent(date: DateTime.Now.ToUniversalTime(), note: createKitNote)
		},
		user: user,
		note: note
	) { }
	
	public PendingKit(TUser user) : base(
		id: -1,
		history: new List<KitEvent>() {
			new CreateKitEvent(date: DateTime.Now.ToUniversalTime(), note: string.Empty)
		},
		user: user,
		note: string.Empty
	) { }
	
	public PendingKit(int id, IReadOnlyCollection<KitEvent> history, TUser user, string note) : base(id: id, history: history, user: user, note: note) { }

	public override KitStage Stage => KitStage.Pending;

	internal override Kit<TUser> CreateKitInNextStage(string note) {
		KitEvent kitEvent = new SendKitEvent(date: DateTime.Now.ToUniversalTime(), note: note);
		return new SentKit<TUser>(
			id: Id,
			history: AddEventToHistory(kitEvent: kitEvent),
			user: User, 
			note: Note
		);
	}

	internal override Kit<TDifferentUser> CloneWithDifferentUserType<TDifferentUser>(TDifferentUser user) {
		CloneSameUserCheck(user);
		return new PendingKit<TDifferentUser>(id: Id, history: History, user: user, note: Note);
	}
}