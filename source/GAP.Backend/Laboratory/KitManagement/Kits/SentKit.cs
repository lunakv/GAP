using Laboratory.KitManagement.Events;
using Users.UserAccess.Entities.Interfaces;

namespace Laboratory.KitManagement.Kits; 

public class SentKit<TUser> : Kit<TUser> where TUser : IHasUserId {
	public override bool ActionRequired => false;
	public override bool Active => true;
	
	public SentKit(int id, IReadOnlyCollection<KitEvent> history, TUser user, string note) : base(id: id, history: history, user: user, note: note) { }

	public override KitStage Stage => KitStage.Sent;

	internal override Kit<TUser> CreateKitInNextStage(string note) {
		KitEvent kitEvent = new ReceiveKitEvent(date: DateTime.Now.ToUniversalTime(), note: note);
		return new ReceivedKit<TUser>(
			id: Id,
			history: AddEventToHistory(kitEvent: kitEvent),
			user: User,
			note: Note
		);
	}
	
	internal override Kit<TDifferentUser> CloneWithDifferentUserType<TDifferentUser>(TDifferentUser user) {
		CloneSameUserCheck(user);
		return new SentKit<TDifferentUser>(id: Id, history: History, user: user, note: Note);
	}
}