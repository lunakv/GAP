using Laboratory.KitManagement.Events;
using Users.UserAccess.Entities.Interfaces;

namespace Laboratory.KitManagement.Kits;

public class ReceivedKit<TUser> : Kit<TUser> where TUser : IHasUserId {
	public override bool ActionRequired => true;
	public override bool Active => true;

	public ReceivedKit(int id, IReadOnlyCollection<KitEvent> history, TUser user, string note) : base(id: id, history: history, user: user, note: note) { }

	public override KitStage Stage => KitStage.Received;

	internal override Kit<TUser> CreateKitInNextStage(string note) {
		KitEvent kitEvent = new AnalyseKitEvent(date: DateTime.Now.ToUniversalTime(), note: note);
		return new AnalysedKit<TUser>(
			id: Id,
			history: AddEventToHistory(kitEvent: kitEvent),
			user: User,
			note: Note
		);
	}

	internal override Kit<TDifferentUser> CloneWithDifferentUserType<TDifferentUser>(TDifferentUser user) {
		CloneSameUserCheck(user);
		return new ReceivedKit<TDifferentUser>(id: Id, history: History, user: user, note: Note);
	}
}