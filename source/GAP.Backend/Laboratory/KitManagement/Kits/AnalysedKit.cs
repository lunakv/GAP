using Laboratory.KitManagement.Events;
using Laboratory.KitManagement.Exceptions;
using Users.UserAccess.Entities.Interfaces;

namespace Laboratory.KitManagement.Kits;

public class AnalysedKit<TUser> : Kit<TUser> where TUser : IHasUserId {
	public override bool ActionRequired => false;
	public override bool Active => false;

	public AnalysedKit(int id, IReadOnlyCollection<KitEvent> history, TUser user, string note) : base(id: id, history: history, user: user, note: note) { }

	public override KitStage Stage => KitStage.Analysed;

	internal override Kit<TUser> CreateKitInNextStage(string note) {
		throw new AnalysedKitNoNextStageException($"Kit ({Id}) already analysed is the last stage and cannot be advanced to next stage.");
	}

	internal override Kit<TDifferentUser> CloneWithDifferentUserType<TDifferentUser>(TDifferentUser user) {
		CloneSameUserCheck(user);
		return new AnalysedKit<TDifferentUser>(id: Id, history: History, user: user, note: Note);
	}
}