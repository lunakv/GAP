using Users.GenData.OriginalStrDataSources;

namespace Laboratory.LabResults; 

public class LabStrFileUploadResult {
	public ICollection<OriginalStrDataSource> OriginalStrDataSources { get; }
	public bool KitsAdvanced { get; }

	public LabStrFileUploadResult(ICollection<OriginalStrDataSource> originalStrDataSources, bool kitsAdvanced) {
		OriginalStrDataSources = originalStrDataSources;
		KitsAdvanced = kitsAdvanced;
	}
}