using Laboratory.KitManagement.Kits;
using Laboratory.KitManagement.KitStageAdvancement;
using Laboratory.KitManagement.Storage;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Users;
using Users.GenData;
using Users.GenData.OriginalStrDataSources;
using Users.GenData.OriginalStrDataSources.Storage.File;
using Users.GenData.Parsers;
using Users.UserAccess.Entities.Interfaces;

namespace Laboratory.LabResults;

public class LabResultsController {
	private readonly IKitFetcher<IHasUserId> _kitFetcher;
	private readonly IKitSaver<IHasUserId> _kitSaver;
	private readonly GeneticDataUploader _geneticDataUploader;
	private readonly ILogger<LabResultsController> _logger;

	public LabResultsController(IKitFetcher<IHasUserId> kitFetcher, IKitSaver<IHasUserId> kitSaver, GeneticDataUploader geneticDataUploader,
		ILogger<LabResultsController> logger) {
		_kitFetcher = kitFetcher;
		_kitSaver = kitSaver;
		_geneticDataUploader = geneticDataUploader;
		_logger = logger;
	}

	public async Task<LabStrFileUploadResult> UploadLabStrFile(Stream file) {
		Stream parseStream = await ParsingUtils.CopyFileStreamAsync(fileStream: file).ConfigureAwait(false);
		var parser = new LabStrFileParser();
		IDictionary<string, StrMarkerCollection<string, int>> samples = await parser.ParseFileAsync(file: parseStream)
			.ConfigureAwait(false);

		string fileName = await _geneticDataUploader.UploadLabStrFileAsync(file: file, fileExtension: parser.FileExtension).ConfigureAwait(false);

		List<int> kitIds = samples.Keys.Select(sampleName => Kit<IHasUserId>.GetKitId(sampleName)).ToList();
		IReadOnlyDictionary<int, Kit<IHasUserId>> kits = await _kitFetcher.FindKitsById(kitIds: kitIds).ConfigureAwait(false);
		List<OriginalStrDataSource> sources = kits.Values.Select(kit => {
			return new OriginalStrDataSource(userId: kit.User.Id, testProvider: parser.GeneticTestProvider, strMarkers: samples[kit.GetSampleName()], fileName: fileName);
		}).ToList();
		IReadOnlyCollection<Kit<IHasUserId>> advancedKits = new KitStageAdvancer<IHasUserId>().AdvanceKitsToAnalysedStage(kits: kits.Values.ToList());
		bool kitsAdvanced = true;
		await _geneticDataUploader.AddOriginalStrDataSourcesAsync(newOriginalStrDataSources: sources).ConfigureAwait(false);

		try {
			await _kitSaver.UpdateKitsAsync(kits: advancedKits).ConfigureAwait(false);
		} catch (Exception ex) {
			kitsAdvanced = false;
			_logger.LogCritical("Saving advanced kits failed: {@Exception}", ex);
		}

		return new LabStrFileUploadResult(originalStrDataSources: sources, kitsAdvanced: kitsAdvanced);
	}
}