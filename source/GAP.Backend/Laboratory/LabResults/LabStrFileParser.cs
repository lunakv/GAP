using System.Globalization;
using CsvHelper;
using CsvHelper.Configuration;
using Users.GenData;
using Users.GenData.Parsers;
using Users.GenData.Parsers.Exceptions;

namespace Laboratory.LabResults;

public class LabStrFileParser {
	public GeneticTestProvider GeneticTestProvider => new GeneticTestProvider(name: "KU");
	public string FileExtension => ".txt";
	
	private const string SampleColumn = "Sample Name";
	private const string MarkerColumn = "Marker";
	private const string AlleleBaseColumn = "Allele ";
	private const int MaxAlleleNumber = 26;
	private const char MarkerPartStart = 'a';

	private const string Delimiter = "\t";

	public async Task<IDictionary<string, StrMarkerCollection<string, int>>> ParseFileAsync(Stream file) {
		using var reader = new StreamReader(stream: file);
		var csvConfiguration = new CsvConfiguration(CultureInfo.InvariantCulture) {
			Delimiter = Delimiter
		};
		using var csv = new CsvReader(reader: reader, configuration: csvConfiguration);
		ICollection<string> alleleColumns = GetAlleleColumns();
		await csv.ReadAsync().ConfigureAwait(false);
		csv.ReadHeader();
		IDictionary<string, StrMarkerCollection<string, int>> parsedMarkerValues = new Dictionary<string, StrMarkerCollection<string, int>>();
		while (await csv.ReadAsync().ConfigureAwait(false)) {
			string sampleName = csv.GetField<string>(SampleColumn) ?? throw new ParsingException($"Sample name is null");
			if (!parsedMarkerValues.ContainsKey(sampleName)) {
				parsedMarkerValues[sampleName] = new StrMarkerCollection<string, int>();
			}

			string marker = csv.GetField<string>(MarkerColumn) ?? throw new ParsingException($"Marker column in null");
			var markerValues = new StrMarkerCollection<string, int>();
			foreach (string alleleColumn in alleleColumns) {
				char markerPart = MarkerPartStart;
				if (csv.TryGetField<string>(alleleColumn, out string? alleleValue)) {
					if (alleleValue == null) {
						throw new ParsingException("Allele column missing");
					}

					if (int.TryParse(alleleValue, out int alleleValueParsed)) {
						markerValues[marker + markerPart] = alleleValueParsed;
						++markerPart;
					}
				}
			}

			if (markerValues.Count == 1) {
				parsedMarkerValues[sampleName].AssignMarkerValue(marker: marker, value: markerValues[marker + MarkerPartStart]);
			} else {
				parsedMarkerValues[sampleName].AddMarkers(markerValues);
			}
		}

		return parsedMarkerValues;
	}

	private ICollection<string> GetAlleleColumns() {
		return Enumerable.Range(start: 1, count: MaxAlleleNumber - 1).Select(alleleNumber => AlleleBaseColumn + alleleNumber.ToString()).ToList();
	}
}