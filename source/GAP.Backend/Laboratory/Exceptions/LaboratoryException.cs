namespace Laboratory.Exceptions; 

public class LaboratoryException : Exception {
	public LaboratoryException(string message) : base(message) {}
}