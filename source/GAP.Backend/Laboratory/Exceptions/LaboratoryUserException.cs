using Users.Exceptions;

namespace Laboratory.Exceptions; 

public abstract class LaboratoryUserException : UserException {
	protected LaboratoryUserException(string message) : base(message) { }
}