using System.Globalization;
using CsvHelper;
using CsvHelper.Configuration;
using Laboratory.SequencerInputGeneration.Input;

namespace Laboratory.SequencerInputGeneration;

public class CsvSequencerInputGenerator {
	private readonly string _delimiter;

	public CsvSequencerInputGenerator(string delimiter) {
		_delimiter = delimiter;
	}

	public async Task<Stream> GenerateSequencerInputAsync(SequencerInput sequencerInput) {
		var outputStream = new MemoryStream();
		await using (StreamWriter sw = new StreamWriter(outputStream, leaveOpen: true)) {
			var csvConfiguration = new CsvConfiguration(CultureInfo.InvariantCulture) {
				Delimiter = _delimiter
			};
			await using (var csvWriter = new CsvWriter(writer: sw, configuration: csvConfiguration, leaveOpen: true)) {
				await AddVersionAsync(sequencerInput: sequencerInput, csvWriter: csvWriter);
				await AddFirstTableAsync(sequencerInput: sequencerInput, csvWriter: csvWriter);
				await AddSecondTableAsync(sequencerInput: sequencerInput, csvWriter: csvWriter);
			}
		}

		outputStream.Seek(0, SeekOrigin.Begin);
		return outputStream;
	}

	private async Task AddVersionAsync(SequencerInput sequencerInput, CsvWriter csvWriter) {
		csvWriter.WriteField(sequencerInput.Version);
		await csvWriter.NextRecordAsync().ConfigureAwait(false);
		await csvWriter.NextRecordAsync().ConfigureAwait(false);
	}

	private async Task AddFirstTableAsync(SequencerInput sequencerInput, CsvWriter csvWriter) {
		csvWriter.WriteRecord(new FirstTableHeader());
		await csvWriter.NextRecordAsync().ConfigureAwait(false);
		csvWriter.WriteRecord(sequencerInput.FirstTableRow);
		await csvWriter.NextRecordAsync().ConfigureAwait(false);
		await csvWriter.NextRecordAsync().ConfigureAwait(false);
	}

	private async Task AddSecondTableAsync(SequencerInput sequencerInput, CsvWriter csvWriter) {
		csvWriter.WriteRecord(new SecondTableHeader());
		await csvWriter.NextRecordAsync().ConfigureAwait(false);
		foreach (SecondTableRow secondTableRow in sequencerInput.SecondTableRows) {
			csvWriter.WriteRecord(secondTableRow);
			await csvWriter.NextRecordAsync().ConfigureAwait(false);
		}
	}
}