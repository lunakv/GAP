namespace Laboratory.SequencerInputGeneration; 

public class SequencerInputConfig {
	public string SequencerInputDefaultPath { get; set; }
	public string Delimiter { get; set; }
	public string PlateNamePrefix { get; set; }

	public SequencerInputConfig() {
		SequencerInputDefaultPath = string.Empty;
		Delimiter = string.Empty;
		PlateNamePrefix = string.Empty;
	}
}