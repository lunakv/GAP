using Laboratory.Exceptions;

namespace Laboratory.SequencerInputGeneration.Exceptions; 

public class SequencerInputDefaultException : LaboratoryException {
	public SequencerInputDefaultException(string message) : base(message) { }
}