using Laboratory.Exceptions;

namespace Laboratory.SequencerInputGeneration.Exceptions; 

public class SequencerInputKitLimitExceededException : LaboratoryUserException {
	public SequencerInputKitLimitExceededException(string message) : base(message) { }
	public override string GetRepresentation => nameof(SequencerInputKitLimitExceededException);
}