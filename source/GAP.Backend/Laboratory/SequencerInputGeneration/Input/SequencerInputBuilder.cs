using Laboratory.KitManagement.Kits;
using Laboratory.SequencerInputGeneration.Default;
using Laboratory.SequencerInputGeneration.Exceptions;
using Users.UserAccess.Entities.Interfaces;

namespace Laboratory.SequencerInputGeneration.Input; 

internal class SequencerInputBuilder {
	private readonly string _version;
	private readonly FirstTableHeader _firstTableHeader;
	private readonly FirstTableRow _firstTableRow;
	private readonly SecondTableHeader _secondTableHeader;
	private readonly List<SecondTableRow> _secondTableRows;
	private readonly SecondTableRow _secondTableRowDefault;
	private readonly IEnumerator<string> _wellRange;
	
	internal SequencerInputBuilder(SequencerInputDefault sequencerInputDefault) {
		_version = sequencerInputDefault.Version;
		_firstTableHeader = new FirstTableHeader();
		_firstTableRow = sequencerInputDefault.FirstTableRow;
		_secondTableHeader = new SecondTableHeader();
		_secondTableRowDefault = sequencerInputDefault.SecondTableRow;
		_secondTableRows = new List<SecondTableRow>();
		_wellRange = WellRange.GetWellRangeEnumerator();
	}

	internal void AddKit(Kit<IHasUserId> kit) {
		if (_wellRange.MoveNext()) {
			_secondTableRows.Add(_secondTableRowDefault.Clone(
				well: _wellRange.Current,
				sampleName: kit.GetSampleName()
			));
		} else {
			throw new SequencerInputKitLimitExceededException(
				$"Kit {kit.Id} cannot be added to sequencer input since the input is already full"
			);
		}
	}

	internal SequencerInput BuildSequencerInput() {
		return new SequencerInput(version: _version,
			firstTableHeader: _firstTableHeader,
			firstTableRow: _firstTableRow,
			secondTableHeader: _secondTableHeader,
			secondTableRows: _secondTableRows
		);
	}
}