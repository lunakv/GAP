using CsvHelper.Configuration.Attributes;

namespace Laboratory.SequencerInputGeneration.Input;

public class FirstTableHeader {
	[Index(0)] public string PlateName => "Plate Name";
	[Index(1)] public string ApplicationType => "Application Type";
	[Index(2)] public string CapillaryLength => "Capillary Length (cm)";
	[Index(3)] public string Polymer => "Polymer";
	[Index(4)] public string NumberOfWells => "Number of Wells";
	[Index(5)] public string OwnerName => "Owner Name";
	[Index(6)] public string BarcodeNumber => "Barcode Number";
	[Index(7)] public string Comments => "Comments";
}