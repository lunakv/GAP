using CsvHelper.Configuration.Attributes;

namespace Laboratory.SequencerInputGeneration.Input;

public class FirstTableRow {
	[Index(0)] public string PlateName { get; internal set; } 
	[Index(1)] public string ApplicationType { get; }
	[Index(2)] public int CapillaryLength { get; }
	[Index(3)] public string Polymer { get; }
	[Index(4)] public int NumberOfWells { get; }
	[Index(5)] public string OwnerName { get; }
	[Index(6)] public string BarcodeNumber { get; }
	[Index(7)] public string Comments { get; }

	public FirstTableRow(string plateName, string applicationType, int capillaryLength, string polymer, int numberOfWells, string ownerName, string barcodeNumber, string comments) {
		PlateName = plateName;
		ApplicationType = applicationType;
		CapillaryLength = capillaryLength;
		Polymer = polymer;
		NumberOfWells = numberOfWells;
		OwnerName = ownerName;
		BarcodeNumber = barcodeNumber;
		Comments = comments;
	}
}