using Laboratory.KitManagement.Kits;
using Laboratory.SequencerInputGeneration.Default;
using Laboratory.SequencerInputGeneration.Exceptions;

namespace Laboratory.SequencerInputGeneration.Input;

public class SequencerInput {
	public string Version { get; }
	public FirstTableHeader FirstTableHeader { get; }
	public FirstTableRow FirstTableRow { get; }
	public SecondTableHeader SecondTableHeader { get; }
	public IReadOnlyCollection<SecondTableRow> SecondTableRows { get; }

	public SequencerInput(string version, FirstTableHeader firstTableHeader, FirstTableRow firstTableRow,
		SecondTableHeader secondTableHeader, IReadOnlyCollection<SecondTableRow> secondTableRows) {
		Version = version;
		FirstTableHeader = firstTableHeader;
		FirstTableRow = firstTableRow;
		SecondTableHeader = secondTableHeader;
		SecondTableRows = secondTableRows;
	}
}