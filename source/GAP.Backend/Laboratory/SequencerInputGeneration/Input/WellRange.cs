namespace Laboratory.SequencerInputGeneration.Input;

internal class WellRange {
	private static readonly string[] RowRange = { "A", "B", "C", "D", "E", "F", "G", "H" };
	private static readonly string[] ColumnRange = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };

	internal static IEnumerator<string> GetWellRangeEnumerator() {
		foreach (string column in ColumnRange) {
			foreach (string row in RowRange) {
				yield return row + column;
			}
		}
	}
}