using CsvHelper.Configuration.Attributes;

namespace Laboratory.SequencerInputGeneration.Input;

public class SecondTableRow {
	[Index(0)] public string Well { get; private set; }
	[Index(1)] public string SampleName { get; private set; }
	[Index(2)] public string Assay { get; }
	[Index(3)] public string FileNameConvention { get; }
	[Index(4)] public string ResultsGroup { get; }
	[Index(5)] public string SampleType { get; }
	[Index(6)] public string UserDefinedField1 { get; }
	[Index(7)] public string UserDefinedField2 { get; }
	[Index(8)] public string UserDefinedField3 { get; }
	[Index(9)] public string UserDefinedField4 { get; }
	[Index(10)] public string UserDefinedField5 { get; }
	[Index(11)] public string Comments { get; }

	public SecondTableRow(string well, string sampleName, string assay, string fileNameConvention, string resultsGroup, string sampleType, string userDefinedField1, string userDefinedField2, string userDefinedField3, string userDefinedField4, string userDefinedField5, string comments) {
		Well = well;
		SampleName = sampleName;
		Assay = assay;
		FileNameConvention = fileNameConvention;
		ResultsGroup = resultsGroup;
		SampleType = sampleType;
		UserDefinedField1 = userDefinedField1;
		UserDefinedField2 = userDefinedField2;
		UserDefinedField3 = userDefinedField3;
		UserDefinedField4 = userDefinedField4;
		UserDefinedField5 = userDefinedField5;
		Comments = comments;
	}

	public SecondTableRow Clone(string well, string sampleName) {
		var copy = (SecondTableRow)this.MemberwiseClone();
		copy.Well = well;
		copy.SampleName = sampleName;
		return copy;
	}
}