using CsvHelper.Configuration.Attributes;

namespace Laboratory.SequencerInputGeneration.Input;

public class SecondTableHeader {
	[Index(0)] public string Well => "Well";
	[Index(1)] public string SampleName => "Sample Name";
	[Index(2)] public string Assay => "Assay";
	[Index(3)] public string FileNameConvention => "File Name Convention";
	[Index(4)] public string ResultsGroup => "Results Group";
	[Index(5)] public string SampleType => "Sample Type";
	[Index(6)] public string UserDefinedField1 => "User Defined Field 1";
	[Index(7)] public string UserDefinedField2 => "User Defined Field 2";
	[Index(8)] public string UserDefinedField3 => "User Defined Field 3";
	[Index(9)] public string UserDefinedField4 => "User Defined Field 4";
	[Index(10)] public string UserDefinedField5 => "User Defined Field 5";
	[Index(11)] public string Comments => "Comments";
}