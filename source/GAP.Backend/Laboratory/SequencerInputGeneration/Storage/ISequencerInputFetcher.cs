using Laboratory.SequencerInputGeneration.Input;

namespace Laboratory.SequencerInputGeneration.Storage; 

public interface ISequencerInputFetcher {
	Task<int> CreateNewSequencerInput();
}