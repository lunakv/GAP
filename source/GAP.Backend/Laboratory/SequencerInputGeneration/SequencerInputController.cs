using Laboratory.KitManagement.Kits;
using Laboratory.KitManagement.Storage;
using Laboratory.SequencerInputGeneration.Default;
using Laboratory.SequencerInputGeneration.Input;
using Laboratory.SequencerInputGeneration.Storage;
using Microsoft.Extensions.Options;
using Users.UserAccess.Entities.Interfaces;

namespace Laboratory.SequencerInputGeneration;

public class SequencerInputController {
	private readonly IKitFetcher<IHasUserId> _kitFetcher;
	private readonly SequencerInputConfig _config;
	private readonly ISequencerInputDefaultFetcher _sequencerInputDefaultFetcher;
	private readonly ISequencerInputFetcher _sequencerInputFetcher;

	public SequencerInputController(IKitFetcher<IHasUserId> kitFetcher, IOptions<LaboratoryConfig> laboratoryConfig, ISequencerInputFetcher sequencerInputFetcher) {
		_kitFetcher = kitFetcher;
		_config = laboratoryConfig.Value.SequencerInputConfig;
		_sequencerInputDefaultFetcher = new JsonSequencerInputDefaultFetcher(jsonFile: _config.SequencerInputDefaultPath);
		_sequencerInputFetcher = sequencerInputFetcher;
	}

	public async Task<SequencerInput> PrepareSequencerInputAsync(IReadOnlyCollection<int> kitIds) {
		IEnumerable<Kit<IHasUserId>> kits = (
			await _kitFetcher.FindKitsById(kitIds).ConfigureAwait(false)
		).Values;

		SequencerInputDefault sequencerInputDefault = await _sequencerInputDefaultFetcher.GetSequencerInputDefaultAsync()
			.ConfigureAwait(false);

		var sequencerInputBuilder = new SequencerInputBuilder(sequencerInputDefault: sequencerInputDefault);
		foreach (var kit in kits) {
			sequencerInputBuilder.AddKit(kit: kit);
		}

		return sequencerInputBuilder.BuildSequencerInput();
	}

	public async Task<Stream> GenerateSequencerInputAsync(SequencerInput sequencerInput) {
		int plateNumber = await _sequencerInputFetcher.CreateNewSequencerInput().ConfigureAwait(false);
		if (sequencerInput.FirstTableRow.PlateName == string.Empty) {
			sequencerInput.FirstTableRow.PlateName = _config.PlateNamePrefix + plateNumber.ToString();
		}
		var csvSequencerInputGenerator = new CsvSequencerInputGenerator(_config.Delimiter);
		return await csvSequencerInputGenerator.GenerateSequencerInputAsync(sequencerInput: sequencerInput).ConfigureAwait(false);
	}
}