using Laboratory.SequencerInputGeneration.Input;

namespace Laboratory.SequencerInputGeneration.Default;

public class SequencerInputDefault {
	public string Version { get; }
	public FirstTableRow FirstTableRow { get; }
	public SecondTableRow SecondTableRow { get; }

	public SequencerInputDefault(string version, FirstTableRow firstTableRow, SecondTableRow secondTableRow) {
		Version = version;
		FirstTableRow = firstTableRow;
		SecondTableRow = secondTableRow;
	}
}