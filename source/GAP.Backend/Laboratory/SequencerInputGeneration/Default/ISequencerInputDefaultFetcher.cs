namespace Laboratory.SequencerInputGeneration.Default;

public interface ISequencerInputDefaultFetcher {
	Task<SequencerInputDefault> GetSequencerInputDefaultAsync();
}