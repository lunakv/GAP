using System.Text.Json;
using Laboratory.SequencerInputGeneration.Exceptions;

namespace Laboratory.SequencerInputGeneration.Default;

public class JsonSequencerInputDefaultFetcher : ISequencerInputDefaultFetcher {
	private readonly string _file;

	public JsonSequencerInputDefaultFetcher(string jsonFile) {
		_file = jsonFile;
	}

	public async Task<SequencerInputDefault> GetSequencerInputDefaultAsync() {
		await using var fs = new FileStream(
			path: _file,
			options: new FileStreamOptions() {
				Access = FileAccess.Read,
				Mode = FileMode.Open,
				Options = FileOptions.Asynchronous,
				Share = FileShare.Read
			}
		);
		SequencerInputDefault? sequencerInputDefault = await JsonSerializer.DeserializeAsync<SequencerInputDefault>(utf8Json: fs)
			.ConfigureAwait(false);

		if (sequencerInputDefault == null) {
			throw new SequencerInputDefaultException(
				$"Sequencer Input Default could not be put in class {nameof(SequencerInputDefault)}");
		}

		return sequencerInputDefault;
	}
}