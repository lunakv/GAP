using Laboratory.LabelGeneration;
using Laboratory.SequencerInputGeneration;

namespace Laboratory;

public class LaboratoryConfig {
	public LabelGenerationConfig LabelGenerationConfig { get; set; }
	public SequencerInputConfig SequencerInputConfig { get; set; }

	public LaboratoryConfig() {
		LabelGenerationConfig = new LabelGenerationConfig();
		SequencerInputConfig = new SequencerInputConfig();
	}
}