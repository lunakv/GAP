using System.Runtime.CompilerServices;
using Users.ClosestRelativeSearch.MarkerSets;
using Users.GenData;

namespace UsersTests.ClosestRelativeSearchTests; 

public class StrMarkerDistanceCalculatorTestSource : TestSource {

	public StrMarkerDistanceCalculatorTestSource(
		StrMarkerCollection<string, int> a,
		StrMarkerCollection<string, int> b,
		MarkerSet markerSet,
		int expectedDistance,
		int expectedComparedMarkers,
		IList<int?> expectedBMarkerValues,
		[CallerMemberName] string testName = "No test name provided.") : base(testName) {
		AMarkers = a;
		BMarkers = b;
		MarkerSet = markerSet;
		ExpectedDistance = expectedDistance;
		ExpectedComparedMarkers = expectedComparedMarkers;
		ExpectedBMarkerValues = expectedBMarkerValues;
	}

	public StrMarkerCollection<string, int> AMarkers { get; }
	public StrMarkerCollection<string, int> BMarkers { get; }
	public MarkerSet MarkerSet { get; }
	public int ExpectedDistance { get; }
	public int ExpectedComparedMarkers { get; }
	public IList<int?> ExpectedBMarkerValues { get; }
}
