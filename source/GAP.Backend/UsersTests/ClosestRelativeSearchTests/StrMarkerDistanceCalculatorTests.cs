using DataLoader.MockDataLoaders;
using FluentAssertions;
using Users.ClosestRelativeSearch;
using Users.ClosestRelativeSearch.MarkerSets;
using Users.GenData;
using Xunit;

namespace UsersTests.ClosestRelativeSearchTests; 

public class StrMarkerDistanceCalculatorTests {

    [Theory]
    [MemberData(nameof(GetTestGeneticData))]
    public void ComputeGeneticDistance(StrMarkerDistanceCalculatorTestSource testSource) {

        IRelativeDistanceCalculator distanceCalculator = new StrMarkerDistanceCalculator(testSource.MarkerSet);
        GeneticDistance geneticDistance = distanceCalculator.ComputeDistance(aMarkers: testSource.AMarkers, bMarkers: testSource.BMarkers);

        geneticDistance.Distance.Should().Be(testSource.ExpectedDistance);
        geneticDistance.ComparedMarkers.Should().Be(testSource.ExpectedComparedMarkers);
        geneticDistance.MarkerValues.Should().Equal(testSource.ExpectedBMarkerValues);
    }

    public static IEnumerable<object[]> GetTestGeneticData() {
        yield return SameSingleDistance();
        yield return SingleDifferentDistance();
        yield return SingleDifferentDistanceEmptyMarkerSet();
        yield return MultipleMarkersButSameMarkerSet();
        yield return BothWithMarkersNotInMarkerSet();
        yield return DashesInMarkerNames();
        yield return SpacesInMarkerNames();
        yield return RandomCasingInMarkerNames();
        yield return YPrefixInMarkerNames();
        yield return YPrefixInOtherMarkerNames();
        yield return YNotInMarkerSet();
        yield return UncomparableMarkerNames();
        yield return ComplexFormatting();
    }

    private static object[] SameSingleDistance() {
        return new object[] { new StrMarkerDistanceCalculatorTestSource(
                a: new StrMarkerCollection<string, int>(new Dictionary<string, int>() { {"YATAH", 20}}),
                b: new StrMarkerCollection<string, int>(new Dictionary<string, int>() { {"YATAH", 20}}),
                markerSet: new MarkerSet(name: "TestMS", markerCount: 1, maxDistance: 10, new []{ "YATAH" }),
                expectedDistance: 0,
                expectedComparedMarkers: 1,
                expectedBMarkerValues: new List<int?>() { 20 }
            )
        };
    }
    
    private static object[] SingleDifferentDistance() {
        return new object[] { new StrMarkerDistanceCalculatorTestSource(
                a: new StrMarkerCollection<string, int>(new Dictionary<string, int>() { {"YATAH", 19}}),
                b: new StrMarkerCollection<string, int>(new Dictionary<string, int>() { {"YATAH", 20}}),
                markerSet: new MarkerSet(name: "TestMS", markerCount: 1, maxDistance: 10, new []{ "YATAH" }),
                expectedDistance: 1,
                expectedComparedMarkers: 1,
                expectedBMarkerValues: new List<int?>() { 20 }
            )
        };
    }
    
    private static object[] SingleDifferentDistanceEmptyMarkerSet() {
        return new object[] { new StrMarkerDistanceCalculatorTestSource(
                a: new StrMarkerCollection<string, int>(new Dictionary<string, int>() { {"YATAH", 19}}),
                b: new StrMarkerCollection<string, int>(new Dictionary<string, int>() { {"YATAH", 20}}),
                markerSet: new MarkerSet(name: "TestMS", markerCount: 1, maxDistance: 10, new []{ "XXX" }),
                expectedDistance: 0,
                expectedComparedMarkers: 0,
                expectedBMarkerValues: new List<int?>() { null }
            )
        };
    }
    
    private static object[] MultipleMarkersButSameMarkerSet() {
       return new object[] { new StrMarkerDistanceCalculatorTestSource(
                a: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"YATAH", 19},
                    {"DYS 233", 15},
                    {"GIS", 16},
                    {"PPP", 14}
                }),
                b: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"YATAH", 14},
                    {"DYS 233", 15},
                    {"GIS", 13},
                    {"PPP", 12}
                }),
                markerSet: new MarkerSet(name: "TestMS", markerCount: 4, maxDistance: 10, new [] {
                    "YATAH", "DYS 233", "GIS", "PPP"
                }),
                expectedDistance: 3,
                expectedComparedMarkers: 4,
                expectedBMarkerValues: new List<int?>() { 14, 15, 13, 12 }
            )
        };
    }
    
    private static object[] BothWithMarkersNotInMarkerSet() {
         return new object[] { new StrMarkerDistanceCalculatorTestSource(
                a: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"YATAH", 19},
                    {"DYS 233", 15},
                    {"AAA", 12},
                    {"CCC", 12},
                    {"PPP", 12}
                }),
                b: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"GIS", 13},
                    {"PPP", 13},
                    {"CCC", 12},
                    {"AAA", 14},
                }),
                markerSet: new MarkerSet(name: "TestMS", markerCount: 5, maxDistance: 10, new [] {
                    "YATAH", "GIS", "AAA", "BBB", "CCC"
                }),
                expectedDistance: 1,
                expectedComparedMarkers: 2,
                expectedBMarkerValues: new List<int?>() { null, 13, 14, null, 12 }
            )
        };
    }
    
    private static object[] DashesInMarkerNames() {
        return new object[] { new StrMarkerDistanceCalculatorTestSource(
                a: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"YATAH", 19},
                    {"DYS-233", 15},
                    {"GIS", 16},
                    {"PPP", 14}
                }),
                b: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"Y-ATA-H", 14},
                    {"DYS 233", 15},
                    {"GIS", 13},
                    {"PPP", 12}
                }),
                markerSet: new MarkerSet(name: "TestMS", markerCount: 4, maxDistance: 10, new [] {
                    "YATAH", "DYS 233", "GIS", "PPP"
                }),
                expectedDistance: 3,
                expectedComparedMarkers: 4,
                expectedBMarkerValues: new List<int?>() { 14, 15, 13, 12 }
            )
        };
    }
    
    private static object[] SpacesInMarkerNames() {
        return new object[] { new StrMarkerDistanceCalculatorTestSource(
                a: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"YATA H", 19},
                    {"DYS233", 15},
                    {"GIS", 16},
                    {"PPP", 14}
                }),
                b: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"YATAH", 14},
                    {"DYS 233", 15},
                    {"GIS", 13},
                    {"PPP", 12}
                }),
                markerSet: new MarkerSet(name: "TestMS", markerCount: 4, maxDistance: 10, new [] {
                    "YATAH", "DYS 233", "GIS", "PPP"
                }),
                expectedDistance: 3,
                expectedComparedMarkers: 4,
                expectedBMarkerValues: new List<int?>() { 14, 15, 13, 12 }
            )
        };
    }
    
    private static object[] RandomCasingInMarkerNames() {
        return new object[] { new StrMarkerDistanceCalculatorTestSource(
                a: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"yatah", 19},
                    {"DyS233aB", 15},
                    {"GIS", 16},
                    {"pPp", 14}
                }),
                b: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"YATAH", 14},
                    {"DYS233aB", 15},
                    {"GIS", 13},
                    {"PPP", 12}
                }),
                markerSet: new MarkerSet(name: "TestMS", markerCount: 4, maxDistance: 10, new [] {
                    "YATAH", "DYS233aB", "GIS", "PPP"
                }),
                expectedDistance: 3,
                expectedComparedMarkers: 4,
                expectedBMarkerValues: new List<int?>() { 14, 15, 13, 12 }
            )
        };
    }
    
    private static object[] YPrefixInMarkerNames() {
        return new object[] { new StrMarkerDistanceCalculatorTestSource(
                a: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"YGATAH4", 19},
                    {"DyS233aB", 15},
                    {"GIS", 16},
                    {"PPP", 14}
                }),
                b: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"GATAH4", 14},
                    {"DYS233aB", 15},
                    {"GIS", 13},
                    {"PPP", 12}
                }),
                markerSet: new MarkerSet(name: "TestMS", markerCount: 4, maxDistance: 10, new [] {
                    "YGATAH4", "DYS233aB", "GIS", "PPP"
                }),
                expectedDistance: 3,
                expectedComparedMarkers: 4,
                expectedBMarkerValues: new List<int?>() { 14, 15, 13, 12 }
            )
        };
    }
    
    private static object[] YPrefixInOtherMarkerNames() {
        return new object[] { new StrMarkerDistanceCalculatorTestSource(
                a: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"GATAH4", 19},
                    {"DyS233aB", 15},
                    {"GIS", 16},
                    {"PPP", 14}
                }),
                b: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"YGATAH4", 14},
                    {"DYS233aB", 15},
                    {"GIS", 13},
                    {"PPP", 12}
                }),
                markerSet: new MarkerSet(name: "TestMS", markerCount: 4, maxDistance: 10, new [] {
                    "YGATAH4", "DYS233aB", "GIS", "PPP"
                }),
                expectedDistance: 3,
                expectedComparedMarkers: 4,
                expectedBMarkerValues: new List<int?>() { 14, 15, 13, 12 }
            )
        };
    }
    
    private static object[] YNotInMarkerSet() {
        return new object[] { new StrMarkerDistanceCalculatorTestSource(
                a: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"GATAH4", 19},
                    {"DyS233aB", 15},
                    {"GIS", 16},
                    {"PPP", 14}
                }),
                b: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"YGATAH4", 14},
                    {"DYS233aB", 15},
                    {"GIS", 13},
                    {"PPP", 12}
                }),
                markerSet: new MarkerSet(name: "TestMS", markerCount: 4, maxDistance: 10, new [] {
                    "GATAH4", "DYS233aB", "GIS", "PPP"
                }),
                expectedDistance: 3,
                expectedComparedMarkers: 4,
                expectedBMarkerValues: new List<int?>() { 14, 15, 13, 12 }
            )
        };
    }
    
    private static object[] UncomparableMarkerNames() {
        return new object[] { new StrMarkerDistanceCalculatorTestSource(
                a: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"YYGATAH4", 1},
                    {"DyS233aB3", 1},
                    {"GIS", 1}
                }),
                b: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"YGATAH4", 1},
                    {"DYS233aB3", 1},
                    {"GISY", 1}
                }),
                markerSet: new MarkerSet(name: "TestMS", markerCount: 4, maxDistance: 10, new [] {
                    "GATAH4", "DYS233aB3", "GIS"
                }),
                expectedDistance: 0,
                expectedComparedMarkers: 1,
                expectedBMarkerValues: new List<int?>() { 1, 1, null, }
            )
        };
    }
    
    private static object[] ComplexFormatting() {
        return new object[] { new StrMarkerDistanceCalculatorTestSource(
                a: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"G--  A--T--  A H4", 19},
                    {"Dy--S233aB", 15},
                    {"GIS           ", 16},
                    {"PPP", 14}
                }),
                b: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
                    {"YGAT  AH4", 14},
                    {"YDYS233aB", 15},
                    {"GIS", 13},
                    {"yPPP", 12}
                }),
                markerSet: new MarkerSet(name: "TestMS", markerCount: 4, maxDistance: 10, new [] {
                    "GATAH4", "DYS233aB", "yGIS", "PPP"
                }),
                expectedDistance: 3,
                expectedComparedMarkers: 4,
                expectedBMarkerValues: new List<int?>() { 14, 15, 13, 12 }
            )
        };
    }
}