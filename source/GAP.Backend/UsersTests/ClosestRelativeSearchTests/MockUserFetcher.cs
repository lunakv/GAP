using Users.UserAccess;
using Users.UserAccess.Entities;
using Users.UserAccess.Storage;
using Users.UserAccess.UserFilter;

namespace UsersTests.ClosestRelativeSearchTests; 

public class MockUserFetcher : IUserFetcher {

	private readonly IList<User> _users;
	
	public MockUserFetcher(IList<User> users) {
		_users = users;
	}
	public Task<User> FindUserByIdAsync(int userId) {
		return Task.FromResult(_users.First(u => u.Id == userId));
	}

	public Task<IReadOnlyDictionary<int, User>> FindUsersByIdAsync(IReadOnlyCollection<int> userIds) {
		IReadOnlyDictionary<int, User> users = _users.Where(user => userIds.Contains(user.Id)).ToDictionary(user => user.Id, user => user);
		return Task.FromResult(users);
	}

	public Task<User> FindUserByEmailAsync(string email) {
		return Task.FromResult(_users.First(u => u.Profile.Email == email));
	}

	public Task<IList<User>> GetUsersAsync() {
		return GetUsersAsync(new EmptyUserFilter());
	}
	
	public Task<IList<User>> GetUsersAsync(UserFilter userFilter) {
		return Task.FromResult((IList<User>)userFilter.Filter(_users).ToList());
	}

}