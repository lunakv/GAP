using FluentAssertions;
using TestUtils;
using Users.ClosestRelativeSearch;
using Users.ClosestRelativeSearch.Filters;
using Users.ClosestRelativeSearch.MarkerSets;
using Users.UserAccess.Entities;
using Xunit;

namespace UsersTests.ClosestRelativeSearchTests;

public class ClosestRelativeFinderTests {

    public ClosestRelativeFinderTests() {
        AssertionOptions.FormattingOptions.MaxLines = 400;
    }

    [Theory]
    [MemberData(nameof(FindClosestRelativesTestData))]
    public void FindClosestRelativesTest(ClosestRelativeFinderTestSource testSource) {

        ClosestRelativeFinder<CompleteUser> closestRelativeFinder = new ClosestRelativeFinder<CompleteUser>();
        IReadOnlyList<UserRelativeDistance<CompleteUser>> closestRelatives = closestRelativeFinder.FindClosestRelatives(
            user: testSource.User,
            users: testSource.Users,
            distanceCalculator: testSource.DistanceCalculator,
            distanceFilter: testSource.DistanceFilter

        );

        closestRelatives.Select(urd => urd.User.Id).Should().Equal(testSource.ExpectedClosestRelatives.Select(urd => urd.User.Id));
        closestRelatives.Select(urd => urd.Distance.Distance).Should().Equal(testSource.ExpectedClosestRelatives.Select(urd => urd.Distance.Distance));
        closestRelatives.Select(urd => urd.Distance.ComparedMarkers).Should().Equal(testSource.ExpectedClosestRelatives.Select(urd => urd.Distance.ComparedMarkers));
    }

    public static IEnumerable<object[]> FindClosestRelativesTestData() {
        yield return NoMaxLimit();
        yield return MaxDistanceLimit();
    }

    private static object[] NoMaxLimit() {
        var xx = UserCreator.CreateUser(1, new Dictionary<string, int>() { }); 
        return new object[] { new ClosestRelativeFinderTestSource(
            user: UserCreator.CreateUser(1, new Dictionary<string, int>() { { "AAA", 15 }, { "BBB", 16 }, { "CCC", 17 }, { "EEE", 20 } }),
            users: new List<CompleteUser>() {
                UserCreator.CreateUser(3, new Dictionary<string, int>() { { "AAA", 12 } }),
                UserCreator.CreateUser(4, new Dictionary<string, int>() { { "AAA", 12 }, { "BBB", 13 }, { "CCC", 17}, }),
                UserCreator.CreateUser(5, new Dictionary<string, int>() { { "AAA", 13 }, { "BBB", 14 }, }),
                UserCreator.CreateUser(6, new Dictionary<string, int>() { { "AAA", 12 }, { "BBB", 13 }, { "CCC", 15}, { "DDD", 14 }, { "EEE", 20 } }),
            },
            distanceCalculator: new StrMarkerDistanceCalculator(new MarkerSet(name: "TMS", markerCount: 4, maxDistance: 5, markers: new []{ "AAA", "BBB", "CCC", "EEE" })),
            distanceFilter: new MaxDistanceFilter<CompleteUser>(666),
            expectedClosestRelatives: new UserRelativeDistance<CompleteUser>[] {
                new UserRelativeDistance<CompleteUser>(
                   user: UserCreator.CreateUser(3, new Dictionary<string, int>() { { "AAA", 15 } }),
                   distance: new GeneticDistance(
                       distance: 1,
                       comparedMarkers: 1,
                       markerValues: new List<int?>())
                ),
                new UserRelativeDistance<CompleteUser>(
                   user: UserCreator.CreateUser(4, new Dictionary<string, int>() { { "AAA", 12 }, { "BBB", 13 }, { "CCC", 17}, }),
                   distance: new GeneticDistance(
                       distance: 2,
                       comparedMarkers: 3,
                       markerValues: new List<int?>())
                ),
                new UserRelativeDistance<CompleteUser>(
                   user: UserCreator.CreateUser(5, new Dictionary<string, int>() { { "AAA", 13 }, { "BBB", 14 }, }),
                   distance: new GeneticDistance(
                       distance: 2,
                       comparedMarkers: 2,
                       markerValues: new List<int?>())
                ),
                new UserRelativeDistance<CompleteUser>(
                   user: UserCreator.CreateUser(6, new Dictionary<string, int>() { { "AAA", 12 }, { "BBB", 13 }, { "CCC", 15}, { "DDD", 14 }, { "EEE", 20 } }),
                   distance: new GeneticDistance(
                       distance: 3,
                       comparedMarkers: 4,
                       markerValues: new List<int?>())
                ),
            }
        )};
    }

    private static object[] MaxDistanceLimit() {
       var xx = UserCreator.CreateUser(1, new Dictionary<string, int>() { }); 
        return new object[] { new ClosestRelativeFinderTestSource(
            user: UserCreator.CreateUser(1, new Dictionary<string, int>() { { "AAA", 15 }, { "BBB", 16 }, { "CCC", 17 }, { "EEE", 20 } }),
            users: new List<CompleteUser>() {
                UserCreator.CreateUser(3, new Dictionary<string, int>() { { "AAA", 12 } }),
                UserCreator.CreateUser(4, new Dictionary<string, int>() { { "AAA", 12 }, { "BBB", 13 }, { "CCC", 17}, }),
                UserCreator.CreateUser(5, new Dictionary<string, int>() { { "AAA", 13 }, { "BBB", 14 }, }),
                UserCreator.CreateUser(6, new Dictionary<string, int>() { { "AAA", 12 }, { "BBB", 13 }, { "CCC", 15}, { "DDD", 14 }, { "EEE", 20 } }),
            },
            distanceCalculator: new StrMarkerDistanceCalculator(new MarkerSet(name: "TMS", markerCount: 4, maxDistance: 3, markers: new []{ "AAA", "BBB", "CCC", "EEE" })),
            distanceFilter: new MaxDistanceFilter<CompleteUser>(3),
            expectedClosestRelatives: new UserRelativeDistance<CompleteUser>[] {
                new UserRelativeDistance<CompleteUser>(
                   user: UserCreator.CreateUser(3, new Dictionary<string, int>() { { "AAA", 15 } }),
                   distance: new GeneticDistance(
                       distance: 1,
                       comparedMarkers: 1,
                       markerValues: new List<int?>())
                ),
                new UserRelativeDistance<CompleteUser>(
                   user: UserCreator.CreateUser(4, new Dictionary<string, int>() { { "AAA", 12 }, { "BBB", 13 }, { "CCC", 17}, }),
                   distance: new GeneticDistance(
                       distance: 2,
                       comparedMarkers: 3,
                       markerValues: new List<int?>())
                ),
                new UserRelativeDistance<CompleteUser>(
                   user: UserCreator.CreateUser(5, new Dictionary<string, int>() { { "AAA", 13 }, { "BBB", 14 }, }),
                   distance: new GeneticDistance(
                       distance: 2,
                       comparedMarkers: 2,
                       markerValues: new List<int?>())
                ),
            }
        )};
    }

}