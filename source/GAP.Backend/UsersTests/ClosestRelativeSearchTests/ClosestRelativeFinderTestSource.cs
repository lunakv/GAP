using System.Runtime.CompilerServices;
using Users.ClosestRelativeSearch;
using Users.ClosestRelativeSearch.Filters;
using Users.UserAccess;
using Users.UserAccess.Entities;

namespace UsersTests.ClosestRelativeSearchTests; 

public class ClosestRelativeFinderTestSource  : TestSource {
	
	public ClosestRelativeFinderTestSource(
		IRelativeDistanceCalculator distanceCalculator,
		CompleteUser user,
		IList<CompleteUser> users,
		IClosestRelativeDistanceFilter<CompleteUser> distanceFilter,
		IReadOnlyList<UserRelativeDistance<CompleteUser>> expectedClosestRelatives,
		[CallerMemberName] string testName = "No test name provided.") : base(testName) {
		DistanceCalculator = distanceCalculator;
		User = user;
		Users = users;
		DistanceFilter = distanceFilter;
		ExpectedClosestRelatives = expectedClosestRelatives;
	}

	public IRelativeDistanceCalculator DistanceCalculator { get; }
	public CompleteUser User { get; }
	public IList<CompleteUser> Users { get; }
	public IClosestRelativeDistanceFilter<CompleteUser> DistanceFilter;
	public IReadOnlyList<UserRelativeDistance<CompleteUser>> ExpectedClosestRelatives { get; }
}
