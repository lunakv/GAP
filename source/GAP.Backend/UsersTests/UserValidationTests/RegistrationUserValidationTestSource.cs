using System.Runtime.CompilerServices;
using Auth.Registration;
using Auth.Registration.Objects;
using Users.UserAccess.Entities;

namespace UsersTests.UserValidationTests; 

public class RegistrationUserValidationTestSource : TestSource {

	public RegistrationUserValidationTestSource(
		RegistrationUser user,
		bool valid,
		[CallerMemberName] string testName = "No test name provided.") : base(testName) {
		User = user;
		Valid = valid;
	}
	
	public RegistrationUser User { get; }
	public bool Valid { get; }
}