using FluentAssertions;
using FluentValidation;
using FluentValidation.Results;
using Users.UserAccess.Entities;
using Users.UserAccess.Validation;
using Xunit;

namespace UsersTests.UserValidationTests; 

public class ProfileValidationTests {
	
    [Theory]
    [MemberData(nameof(ProfileValidationTestData))]
    public async Task ProfileValidatorTest(ProfileValidationTestSource testSource) {

        var validator = new ProfileValidator();

        ValidationResult validationResult = await validator.ValidateAsync(testSource.Profile);
        if (testSource.Valid) {
            validator.Invoking(v => v.ValidateAndThrowAsync(testSource.Profile).Wait()).Should().NotThrow();
        } else {
            validator.Invoking(v => v.ValidateAndThrowAsync(testSource.Profile).Wait()).Should().Throw<ValidationException>();
        }
        validationResult.IsValid.Should().Be(testSource.Valid);
    }

    public static IEnumerable<object[]> ProfileValidationTestData() {
        yield return ValidProfileWithoutCorrespondenceAddress();
        yield return ValidProfile();
        yield return ValidProfileNulls();
        yield return InvalidProfileNullGivenName();
        yield return InvalidProfileFamilyNameNull();
        yield return InvalidProfileEmailNull();
        yield return ValidProfilePastBirthDate();
        yield return ValidProfileLongGivenName();
        yield return ValidProfileLongTown();
        yield return ValidProfileLongStreet();
        yield return ValidProfileShortPhoneNumber();
        yield return ValidProfileCountryLongPhoneNumber();
        yield return ValidProfileCountryShortPhoneNumber(); 
        yield return ValidProfileGivenNameSpecialChars(); 
        yield return ValidProfileGivenTownSpecialChars(); 
        yield return ValidProfileCountryShortPhoneNumber(); 
        yield return ValidProfileCountryShortPhoneNumber(); 
        yield return InvalidProfileEmptyGivenName();
        yield return InvalidProfileEmptyFamilyName();
        yield return InvalidProfileEmptyEmail();
        yield return InvalidProfileEmptyPhoneNumber();
        yield return InvalidProfileLongPhoneNumber();
        yield return InvalidProfileDashPhoneNumber();
        yield return InvalidProfileSpacePhoneNumber();
        yield return InvalidProfileEmptyTown();
        yield return InvalidProfileEmptyMunicipality();
        yield return InvalidProfileEmptyCounty();
        yield return InvalidProfileEmptyStreet();
        yield return InvalidProfileEmptyZipCode();
        yield return InvalidProfileTooLongGivenName();
        yield return InvalidProfileTooLongTown();
        yield return InvalidProfileLongZipCode();
        yield return InvalidProfileShortZipCode();
        yield return InvalidProfileNoAtEmail();
        yield return InvalidProfileFutureTime();
        yield return InvalidProfileGivenNameSpecialChars();
        yield return InvalidProfileFamilyNameSpecialChars();
        yield return InvalidProfileTownSpecialChars();
        yield return InvalidProfileStreetSpecialChars();
        yield return InvalidProfileMunicipalitySpecialChars();
        yield return InvalidProfileCountySpecialChars();
        yield return InvalidProfileZipCodeLetters();
    }

    private static Profile GetValidProfile(string? givenName = null, string? familyName = null, string? email = null,
        string? phoneNumber = null, string? town = null, string? municipality = null, string? county = null, string? street = null,
        string? zipCode = null, DateOnly? birthDate = null) {
        return new Profile(
            givenName: givenName ?? "Lubomírěščřžýáíóíúůňďťľ",
            familyName: familyName ?? "Budíšňť",
            email: email ?? "random=š2@gmail.com",
            phoneNumber: phoneNumber ?? "+420324089324",
            residenceAddress: new ExpandedAddress(
                town: town ?? "滁州市e",
                municipality: municipality ?? "Łódź",
                county: county ?? "Vranov nad Topľou",
                street: street ?? "Київ 5. Května ěščřžýáíóíúůňďťľ",
                zipCode: zipCode ?? "43098"
            ),
            correspondenceAddress: new Address(
                town: town ?? "Münster ěščřžýáíóíúůňďť",
                street: municipality ?? "Częstochowa",
                zipCode: zipCode ?? "45678"
            ),
            birthDate: birthDate ?? new DateOnly(year: 1990, month: 2, day: 12)
        );
    }

    private static object[] ValidProfileWithoutCorrespondenceAddress() {
        return new object[] { new ProfileValidationTestSource(
            profile: new Profile(
                givenName: "Lubomír ",
                familyName: "Budíšňť",
                email: "random=š2@gmail.com", 
                phoneNumber: "+420324089324",
                residenceAddress: new ExpandedAddress(
                    town: "Vrchlabí",
                    municipality: "Jičín",
                    county: "Vranov nad Topľou",
                    street: "5. Května",
                    zipCode: "43098"
                ),
                correspondenceAddress: null,
                birthDate: null
            ),
            valid: true
        )};
    }
    
    private static object[] ValidProfile() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(),
            valid: true
        )};
    }

    private static object[] ValidProfileNulls() {
        return new object[] {
            new ProfileValidationTestSource(
                profile: new Profile(
                    givenName: "Lubomír",
                    familyName: "Budíšňť",
                    email: "random=š2@gmail.com",
                    phoneNumber: null,
                    residenceAddress: new ExpandedAddress(
                        town: null,
                        municipality: null,
                        county: null,
                        street: null,
                        zipCode: null
                    ),
                    correspondenceAddress: null,
                    birthDate: null
                ),
                valid: true
            )
        };
    }
    
    private static object[] ValidProfilePastBirthDate() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(birthDate: new DateOnly(1970, 3, 3)),
            valid: true
        )};
    }

    private static object[] ValidProfileGivenNameSpecialChars() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(givenName: "Nu-zha La"),
            valid: true
        )};
    }
     
    private static object[] ValidProfileGivenTownSpecialChars() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(town: "St. lake-- La"),
            valid: true
        )};
    }
    
    private static object[] InvalidProfileNullGivenName() {
        return new object[] {
            new ProfileValidationTestSource(
                profile: new Profile(
                    givenName: null!,
                    familyName: "Budíšňť",
                    email: "random=š2@gmail.com",
                    phoneNumber: null,
                    residenceAddress: new ExpandedAddress(
                        town: null,
                        municipality: null,
                        county: null,
                        street: null,
                        zipCode: null
                    ),
                    correspondenceAddress: null,
                    birthDate: null
                ),
                valid: false
            )
        };
    }
    
    private static object[] InvalidProfileFamilyNameNull() {
        return new object[] {
            new ProfileValidationTestSource(
                profile: new Profile(
                    givenName: "Lubomír",
                    familyName: null!,
                    email: "random=š2@gmail.com",
                    phoneNumber: null,
                    residenceAddress: new ExpandedAddress(
                        town: null,
                        municipality: null,
                        county: null,
                        street: null,
                        zipCode: null
                    ),
                    correspondenceAddress: null,
                    birthDate: null
                ),
                valid: false 
            )
        };
    }
      
    private static object[] InvalidProfileEmailNull() {
        return new object[] {
            new ProfileValidationTestSource(
                profile: new Profile(
                    givenName: "Lubomír",
                    familyName: "Budíšňť",
                    email: null!,
                    phoneNumber: null,
                    residenceAddress: new ExpandedAddress(
                        town: null,
                        municipality: null,
                        county: null,
                        street: null,
                        zipCode: null
                    ),
                    correspondenceAddress: null,
                    birthDate: null
                ),
                valid: false
            )
        };
    }
    
    private static object[] ValidProfileLongGivenName() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(givenName: "nejaky random šěčšřčšžřčž"),
            valid: true 
        )};
    }
    
    private static object[] ValidProfileLongTown() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(town: "nejaky random šěčšřčšžřčž erter"),
            valid: true 
        )};
    }
    
    private static object[] ValidProfileLongStreet() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(town: "nejaky43 random ěčšřčšžřčž erter"),
            valid: true 
        )};
    }
     
    private static object[] ValidProfileShortPhoneNumber() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(phoneNumber: "123456789"),
            valid: true 
        )};
    }
    
    private static object[] ValidProfileCountryLongPhoneNumber() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(phoneNumber: "+420123456789"),
            valid: true 
        )};
    }
    
     private static object[] ValidProfileCountryShortPhoneNumber() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(phoneNumber: "+45123456789"),
            valid: true 
        )};
    }
     
    private static object[] InvalidProfileEmptyGivenName() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(givenName: ""),
            valid: false
        )};
    }
    
    private static object[] InvalidProfileEmptyFamilyName() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(familyName: ""),
            valid: false
        )}; 
    }
    
    private static object[] InvalidProfileEmptyEmail() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(email: ""), 
            valid: false
        )}; 
    }
    
    private static object[] InvalidProfileEmptyPhoneNumber() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(phoneNumber: ""),
            valid: false
        )};
    }
    
   private static object[] InvalidProfileLongPhoneNumber() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(phoneNumber: "1234567890"),
            valid: false
        )};
   }
   
   private static object[] InvalidProfileDashPhoneNumber() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(phoneNumber: "123-456-789"),
            valid: false
        )};
   }
   
   private static object[] InvalidProfileSpacePhoneNumber() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(phoneNumber: "123 456 789"),
            valid: false
        )};
   }
   
   private static object[] InvalidProfileEmptyTown() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(town: ""),
            valid: false
        )};
   }
   
   private static object[] InvalidProfileEmptyMunicipality() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(municipality: ""),
            valid: false
        )};
   }
   
   private static object[] InvalidProfileEmptyCounty() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(county: ""),
            valid: false
        )};
   }
   
   private static object[] InvalidProfileEmptyStreet() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(street: ""),
            valid: false
        )};
   }

   private static object[] InvalidProfileEmptyZipCode() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(zipCode: ""),
            valid: false
        )}; 
   }
    
    private static object[] InvalidProfileTooLongGivenName() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(givenName: "qwert qwert qwert qwert qwert qwert qwert qwert qwert qwert qwert qwert qwert qwert qwert"),
            valid: false
        )}; 
    }
    
    private static object[] InvalidProfileTooLongTown() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(town: "qwert qwert qwert qwert qwert qwert qwert qwert qwert qwert qwert qwert qwert qwert qwert qwert qwert qwert qwert"),
            valid: false
        )}; 
    }

    private static object[] InvalidProfileLongZipCode() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(zipCode: "123600"),
            valid: false
        )};
    }
    
    private static object[] InvalidProfileShortZipCode() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(zipCode: "1236"),
            valid: false
        )};
    }
    
    private static object[] InvalidProfileNoAtEmail() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(email: "email.google.com"),
            valid: false
        )}; 
    }
    
    private static object[] InvalidProfileFutureTime() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(birthDate: DateOnly.FromDateTime(DateTime.Today.AddDays(1))),
            valid: false
        )}; 
    }
    
    private static object[] InvalidProfileGivenNameSpecialChars() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(givenName: "Tomas+"),
            valid: false
        )}; 
    }

    private static object[] InvalidProfileFamilyNameSpecialChars() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(familyName: "Novak$"),
            valid: false
        )}; 
    }
    
    private static object[] InvalidProfileTownSpecialChars() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(town: "Vrchlabi'"),
            valid: false
        )}; 
    }
    
    private static object[] InvalidProfileStreetSpecialChars() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(street: "5, Kvetna"),
            valid: false
        )}; 
    }
     
    private static object[] InvalidProfileMunicipalitySpecialChars() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(municipality: "Georgetown%"),
            valid: false
        )}; 
    }
    
    private static object[] InvalidProfileCountySpecialChars() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(county: "Georgetown#"),
            valid: false
        )}; 
    }

    private static object[] InvalidProfileZipCodeLetters() {
        return new object[] { new ProfileValidationTestSource(
            profile: GetValidProfile(zipCode: "324S4"),
            valid: false
        )}; 
    }
}