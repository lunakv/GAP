using System.Runtime.CompilerServices;
using Users.UserAccess.Entities;

namespace UsersTests.UserValidationTests; 

public class ProfileValidationTestSource : TestSource {

	public ProfileValidationTestSource(
		Profile profile,
		bool valid,
		[CallerMemberName] string testName = "No test name provided.") : base(testName) {
		Profile = profile;
		Valid = valid;
	}
	
	public Profile Profile { get; }
	public bool Valid { get; }
	
}