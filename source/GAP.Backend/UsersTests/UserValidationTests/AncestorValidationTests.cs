using FluentAssertions;
using FluentValidation;
using FluentValidation.Results;
using Users.UserAccess.Entities;
using Users.UserAccess.Validation;
using Xunit;

namespace UsersTests.UserValidationTests; 

public class AncestorValidationTests {
	
    [Theory]
    [MemberData(nameof(AncestorValidationTestData))]
    public async Task AncestorValidatorTest(AncestorValidationTestSource testSource) {

        var validator = new AncestorValidator();

        ValidationResult validationResult = await validator.ValidateAsync(testSource.Ancestor);
        if (testSource.Valid) {
            validator.Invoking(v => v.ValidateAndThrowAsync(testSource.Ancestor).Wait()).Should().NotThrow();
        } else {
            validator.Invoking(v => v.ValidateAndThrowAsync(testSource.Ancestor).Wait()).Should().Throw<ValidationException>();
        }
        validationResult.IsValid.Should().Be(testSource.Valid);
    }

    public static IEnumerable<object[]> AncestorValidationTestData() {
        yield return ValidAncestor();
        yield return ValidAncestorNulls();
        yield return InvalidAncestorGivenNameEmpty();
        yield return InvalidAncestorFamilyNameEmpty();
        yield return InvalidAncestorPlaceOfBirthEmpty();
        yield return InvalidAncestorPlaceOfDeathEmpty();
        yield return InvalidAncestorTownEmpty();
        yield return InvalidAncestorMunicipalityEmpty();
        yield return InvalidAncestorCountyEmpty();
        yield return InvalidAncestorStreetEmpty();
        yield return InvalidAncestorZipCodeEmpty();
        yield return InvalidAncestorGivenNameSpecialChars();
        yield return InvalidAncestorFamilyNameSpecialChars();
        yield return InvalidAncestorPlaceOfBirthSpecialChars();
        yield return InvalidAncestorPlaceOfDeathSpecialChars();
        yield return InvalidAncestorTownSpecialChars();
        yield return InvalidAncestorMunicipalitySpecialChars();
        yield return InvalidAncestorCountySpecialChars();
        yield return InvalidAncestorStreetSpecialChars();
        yield return InvalidAncestorZipCodeSpecialChars();
        yield return InvalidAncestorZipCodeLetters();
        yield return InvalidAncestorBirthDateFuture();
        yield return InvalidAncestorDeathDateFuture();
        yield return InvalidAncestorDeathDateBeforeBirthDate();
    }

    private static Ancestor GetValidAncestor(string? givenName = null, string? familyName = null, string? town = null, string? municipality = null,
        string? county = null, string? street = null, string? zipCode = null, DateOnly? birthDate = null,
        string? placeOfBirth = null, DateOnly? deathDate = null, string? placeOfDeath = null) {
        return new Ancestor(
            address: new ExpandedAddress(
                town: town ?? "滁州市e",
                municipality: municipality ?? "Łódź32 čšžýžáýáíňťď",
                county: county ?? "Vranov nad Topľou",
                street: street ?? "Київ 5. Května ěščřžýáíóíúůňďťľ",
                zipCode: zipCode ?? "43098"
            ),
            givenName: givenName ?? "Lubomír3 -ěščřžýáíóíúůňďťľ",
            familyName: familyName ?? "Budíšňť- 5ěščřžýáíóíúůňďťľ",
            birthDate: birthDate ?? new DateOnly(1700, 12, 3),
            placeOfBirth: placeOfBirth ?? "Varšava ěščřžýáíóíúůňďťľ . -",
            deathDate: deathDate ?? new DateOnly(1750, 4, 19),
            placeOfDeath: placeOfDeath ?? "Varšava-čšžýžáýáíňťď . -"
        );
    }

    private static object[] ValidAncestor() {
        return new object[] { new AncestorValidationTestSource(
            ancestor: GetValidAncestor(),
            valid: true
        )};
    }
    
    private static object[] ValidAncestorNulls() {
        return new object[] { new AncestorValidationTestSource(
            ancestor: new Ancestor(
                address: new ExpandedAddress(
                    town: null,
                    municipality: null,
                    county: null,
                    street: null,
                    zipCode: null
                ),
                givenName: null,
                familyName: null,
                birthDate: null,
                placeOfBirth: null,
                deathDate: null,
                placeOfDeath: null
            ),
            valid: true
        )};
    }

    private static object[] InvalidAncestorGivenNameEmpty() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(givenName: ""),
                valid: false
            )
        };
    }
   
    private static object[] InvalidAncestorFamilyNameEmpty() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(familyName: ""),
                valid: false
            )
        };
    }
    
    private static object[] InvalidAncestorPlaceOfBirthEmpty() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(placeOfBirth: ""),
                valid: false
            )
        };
    }
    
    private static object[] InvalidAncestorPlaceOfDeathEmpty() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(placeOfDeath: ""),
                valid: false
            )
        };
    }
    
    private static object[] InvalidAncestorTownEmpty() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(town: ""),
                valid: false
            )
        };
    }
    
    private static object[] InvalidAncestorMunicipalityEmpty() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(municipality: ""),
                valid: false
            )
        };
    }
    
    private static object[] InvalidAncestorCountyEmpty() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(county: ""),
                valid: false
            )
        };
    }
    
    private static object[] InvalidAncestorStreetEmpty() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(street: ""),
                valid: false
            )
        };
    }
    
    private static object[] InvalidAncestorZipCodeEmpty() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(zipCode: ""),
                valid: false
            )
        }; 
    }
    
    private static object[] InvalidAncestorGivenNameSpecialChars() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(givenName: ";gfg"),
                valid: false
            )
        };
    }
   
    private static object[] InvalidAncestorFamilyNameSpecialChars() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(familyName: "ank!"),
                valid: false
            )
        };
    }
    
    private static object[] InvalidAncestorPlaceOfBirthSpecialChars() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(placeOfBirth: "{ret"),
                valid: false
            )
        };
    }
    
    private static object[] InvalidAncestorPlaceOfDeathSpecialChars() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(placeOfDeath: "}err"),
                valid: false
            )
        };
    }
    
    private static object[] InvalidAncestorTownSpecialChars() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(town: "asdr'"),
                valid: false
            )
        };
    }
    
    private static object[] InvalidAncestorMunicipalitySpecialChars() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(municipality: "aaaa@"),
                valid: false
            )
        };
    }
    
    private static object[] InvalidAncestorCountySpecialChars() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(county: "<dd>"),
                valid: false
            )
        };
    }

    private static object[] InvalidAncestorStreetSpecialChars() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(street: "sddf:"),
                valid: false
            )
        };
    }
    
    private static object[] InvalidAncestorZipCodeSpecialChars() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(zipCode: "344 30"),
                valid: false
            )
        }; 
    }

    private static object[] InvalidAncestorZipCodeLetters() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(zipCode: "344O0"),
                valid: false
            )
        }; 
    }    
    
    private static object[] InvalidAncestorBirthDateFuture() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(birthDate: DateOnly.FromDateTime(DateTime.Today.AddDays(1))),
                valid: false
            )
        }; 
    }

    private static object[] InvalidAncestorDeathDateFuture() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(deathDate: DateOnly.FromDateTime(DateTime.Today.AddDays(1))),
                valid: false
            )
        }; 
    }

    private static object[] InvalidAncestorDeathDateBeforeBirthDate() {
        return new object[] {
            new AncestorValidationTestSource(
                ancestor: GetValidAncestor(
                    birthDate: new DateOnly(1700, 3, 2),
                    deathDate: new DateOnly(1670, 4, 4)
                ),
                valid: false
            )
        }; 
    }
}