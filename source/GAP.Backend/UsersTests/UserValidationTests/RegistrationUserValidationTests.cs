using Auth.Registration;
using Auth.Registration.Objects;
using Auth.Registration.Validation;
using FluentAssertions;
using FluentValidation;
using FluentValidation.Results;
using Users.UserAccess.Entities;
using Users.UserAccess.Validation;
using Xunit;

namespace UsersTests.UserValidationTests; 

public class RegistrationUserValidationTests {
    
	[Theory]
    [MemberData(nameof(RegistrationUserValidationTestData))]
    public async Task RegistrationUserValidatorTest(RegistrationUserValidationTestSource testSource) {

        // RegistrationUserValidator only forwards validation to ancestor and profile validators,
        // so this test only non-exhaustively checks that ancestor and profile validation is done.
        var validator = new RegistrationUserValidator();

        ValidationResult validationResult = await validator.ValidateAsync(testSource.User);
        if (testSource.Valid) {
            validator.Invoking(v => v.ValidateAndThrowAsync(testSource.User).Wait()).Should().NotThrow();
        } else {
            validator.Invoking(v => v.ValidateAndThrowAsync(testSource.User).Wait()).Should().Throw<ValidationException>();
        }
        validationResult.IsValid.Should().Be(testSource.Valid);
    }

    public static IEnumerable<object[]> RegistrationUserValidationTestData() {
        yield return ValidUser();
        yield return InvalidUserProfileGivenName();
        yield return InvalidUserProfileFamilyName();
        yield return InvalidUserProfileTown();
        yield return InvalidUserProfileStreet();
        yield return InvalidUserProfileBirthDate();
        yield return InvalidUserAncestorGivenName();
        yield return InvalidUserAncestorFamilyName();
        yield return InvalidUserAncestorTown();
        yield return InvalidUserAncestorPlaceOfBirth();
    }

    public static RegistrationUser GetValidUser(string? givenName = null, string? familyName = null, string? email = null,
        string? phoneNumber = null, string? town = null, string? municipality = null, string? county = null, string? street = null,
        string? zipCode = null, DateOnly? birthDate = null, string? placeOfBirth = null,
        DateOnly? deathDate = null, string? placeOfDeath = null, string? ancestorGivenName = null, string? ancestorFamilyName = null,
        string? ancestorTown = null) {
        return new RegistrationUser(
            administrationAgreementSigned: true,
            publicId: "JKLSHDA-JSAKDLHSA",
            profile: new Profile(
                givenName: givenName ?? "Lubomírěščřžýáíóíúůňďťľ",
                familyName: familyName ?? "Budíšňť",
                email: email ?? "random=š2@gmail.com",
                phoneNumber: phoneNumber ?? "+420324089324",
                residenceAddress: new ExpandedAddress(
                    town: town ?? "滁州市e",
                    municipality: municipality ?? "Łódź",
                    county: county ?? "Vranov nad Topľou",
                    street: street ?? "Київ 5. Května ěščřžýáíóíúůňďťľ",
                    zipCode: zipCode ?? "43098"
                ),
                correspondenceAddress: new Address(
                    town: town ?? "Münster ěščřžýáíóíúůňďť",
                    street: municipality ?? "Częstochowa",
                    zipCode: zipCode ?? "45678"
                ),
                birthDate: birthDate ?? new DateOnly(year: 1990, month: 2, day: 12)
            ),
            ancestor: new Ancestor(
                address: new ExpandedAddress(
                    town: ancestorTown ?? "滁州市e",
                    municipality: municipality ?? "Łódź32 čšžýžáýáíňťď",
                    county: county ?? "Vranov nad Topľou",
                    street: street ?? "Київ 5. Května ěščřžýáíóíúůňďťľ",
                    zipCode: zipCode ?? "43098"
                ),
                givenName: ancestorGivenName ?? "Lubomír3 -ěščřžýáíóíúůňďťľ",
                familyName: ancestorFamilyName ?? "Budíšňť- 5ěščřžýáíóíúůňďťľ",
                birthDate: new DateOnly(1700, 12, 3),
                placeOfBirth: placeOfBirth ?? "Varšava ěščřžýáíóíúůňďťľ . -",
                deathDate: deathDate ?? new DateOnly(1750, 4, 19),
                placeOfDeath: placeOfDeath ?? "Varšava-čšžýžáýáíňťď . -"
            ),
            regionId: 0,
            password: "password"
        );
    }

    private static object[] ValidUser() {
        return new object[] { new RegistrationUserValidationTestSource(
            user: GetValidUser(),
            valid: true
        )};
    }
    
    private static object[] InvalidUserProfileGivenName() {
        return new object[] { new RegistrationUserValidationTestSource(
            user: GetValidUser(givenName: "invalidni***"),
            valid: false
        )};
    }
    
    private static object[] InvalidUserProfileFamilyName() {
        return new object[] { new RegistrationUserValidationTestSource(
            user: GetValidUser(familyName: ""),
            valid: false
        )};
    }
    
    private static object[] InvalidUserProfileTown() {
        return new object[] { new RegistrationUserValidationTestSource(
            user: GetValidUser(town: ""),
            valid: false
        )};
    }
    
    private static object[] InvalidUserProfileStreet() {
        return new object[] { new RegistrationUserValidationTestSource(
            user: GetValidUser(street: "5, Kvetna"),
            valid: false
        )};
    }

    private static object[] InvalidUserProfileBirthDate() {
        return new object[] { new RegistrationUserValidationTestSource(
            user: GetValidUser(birthDate: DateOnly.FromDateTime(DateTime.Now.AddDays(1))),
            valid: false
        )};
    }
    
    private static object[] InvalidUserAncestorGivenName() {
        return new object[] { new RegistrationUserValidationTestSource(
            user: GetValidUser(ancestorGivenName: "invalidni***"),
            valid: false
        )};
    }
    
    private static object[] InvalidUserAncestorFamilyName() {
        return new object[] { new RegistrationUserValidationTestSource(
            user: GetValidUser(ancestorFamilyName: ""),
            valid: false
        )};
    }
    
    private static object[] InvalidUserAncestorTown() {
        return new object[] { new RegistrationUserValidationTestSource(
            user: GetValidUser(ancestorTown: ""),
            valid: false
        )};
    }

    private static object[] InvalidUserAncestorPlaceOfBirth() {
        return new object[] {
            new RegistrationUserValidationTestSource(
                user: GetValidUser(placeOfBirth: "place%place"),
                valid: false 
            )
        };
    }
    
    
}