using System.Runtime.CompilerServices;
using Users.UserAccess.Entities;

namespace UsersTests.UserValidationTests; 

public class AncestorValidationTestSource : TestSource {

	public AncestorValidationTestSource(
		Ancestor ancestor,
		bool valid,
		[CallerMemberName] string testName = "No test name provided.") : base(testName) {
		Ancestor = ancestor;
		Valid = valid;
	}
	
	public Ancestor Ancestor { get; }
	public bool Valid { get; }
	
}