using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Auth.Registration;
using Auth.Registration.Objects;
using TestUtils;

namespace PostgreSqlStorageTests.RegistrationGateway; 

public class SaveAndFetchRegistrationDataTestSource  : TestSource {

	public SaveAndFetchRegistrationDataTestSource(
		RegistrationData inputRegistrationData,
		RegistrationData expectedRegistrationData,
		[CallerMemberName] string testName = "No test name provided.") : base(testName) {
		InputRegistrationData = inputRegistrationData;
		ExpectedRegistrationData = expectedRegistrationData;
	}

	public RegistrationData InputRegistrationData { get; }
	public RegistrationData ExpectedRegistrationData { get; }
}
