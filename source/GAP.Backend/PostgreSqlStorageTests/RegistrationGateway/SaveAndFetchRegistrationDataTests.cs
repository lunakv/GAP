using System;
using System.Collections.Generic;
using System.Linq;
using Auth.Registration;
using Auth.Registration.Objects;
using DataLoader.MockDataLoaders;
using FluentAssertions;
using Map.ValueObjects;
using PostgreSqlStorage.Data;
using PostgreSqlStorage.MapGateway.Accessors;
using PostgreSqlStorage.RegistrationGateway.Accessors;
using PostgreSqlStorage.UsersGateway.GenData.Accessors;
using PostgreSqlStorage.UsersGateway.UserAccess.Accessors;
using TestUtils;
using Users.GenData;
using Users.GenData.Haplogroup;
using Users.GenData.OriginalStrDataSources;
using Users.GenData.Parsers;
using Users.UserAccess.Entities;
using Xunit;

namespace PostgreSqlStorageTests.RegistrationGateway;

[Collection(nameof(TestCollection))]
public sealed class SaveAndFetchRegistrationDataTests : IDisposable, IClassFixture<TestDatabaseFixture> {
	private readonly TestDatabaseFixture _fixture;

	private readonly GapContext _context;

	public SaveAndFetchRegistrationDataTests(TestDatabaseFixture fixture) {
		_fixture = fixture;
		_fixture.ResetDatabase();
		_context = _fixture.CreateContext();
	}

	public void Dispose() {
		_context.Dispose();
	}

	[Theory]
	[MemberData(nameof(SaveAndFetchUsersData))]
	public void SaveAndFetchRegistrationData(SaveAndFetchRegistrationDataTestSource testSource) {
		new RegistrationDataSaver(context: _context).AddRegistrationDataAsync(registrationData: testSource.InputRegistrationData).Wait();

		_context.ChangeTracker.Clear();

		RegistrationData fetchedRegistrationData = new RegistrationDataFetcher(context: _context)
			.GetRegistrationDataAsync(userId: testSource.InputRegistrationData.UserId).Result;

		fetchedRegistrationData.Should().BeEquivalentTo(testSource.ExpectedRegistrationData);
	}

	[Theory]
	[MemberData(nameof(SaveAndFetchUsersData))]
	public void SaveAndFetchAndDeleteRegistrationData(SaveAndFetchRegistrationDataTestSource testSource) {
		new RegistrationDataSaver(context: _context).AddRegistrationDataAsync(registrationData: testSource.InputRegistrationData).Wait();

		_context.ChangeTracker.Clear();
		
		RegistrationData fetchedRegistrationData = new RegistrationDataFetcher(context: _context)
			.GetRegistrationDataAsync(userId: testSource.InputRegistrationData.UserId).Result;

		fetchedRegistrationData.Should().BeEquivalentTo(testSource.ExpectedRegistrationData);

		new RegistrationDataSaver(context: _context).DeleteRegistrationDataAsync(userId: testSource.InputRegistrationData.UserId).Wait();

		new RegistrationDataFetcher(context: _context)
			.Invoking(rdf => rdf.GetRegistrationDataAsync(userId: testSource.InputRegistrationData.UserId).Wait())
			.Should().Throw<ArgumentException>();
	}
	
	public static IEnumerable<object[]> SaveAndFetchUsersData() {
		yield return RegistrationDataWithoutGeneticData();
		yield return RegistrationDataWithLabTestRequested();
		yield return RegistrationDataWithStrData();
		yield return RegistrationDataWithStrDataAndHaplogroup();
	}

	private static object[] RegistrationDataWithoutGeneticData() {
		return new object[] {
			new SaveAndFetchRegistrationDataTestSource(
				inputRegistrationData: new RegistrationData(
					userId: 1,
					user: CreateRegistrationUser(password: "PASSWORD")
				),
				expectedRegistrationData: new RegistrationData(
					userId: 1,
					user: CreateRegistrationUser(password: string.Empty)
				)
			)
		};
	}
	
	private static object[] RegistrationDataWithLabTestRequested() {
		return new object[] {
			new SaveAndFetchRegistrationDataTestSource(
				inputRegistrationData: new RegistrationData(
					userId: 1,
					user: CreateRegistrationUser(password: "PASSWORD"),
					labTestRequested: true,
					uploadedStrDataSource: null,
					haplogroup: null
				),
				expectedRegistrationData: new RegistrationData(
					userId: 1,
					user: CreateRegistrationUser(password: string.Empty),
					labTestRequested: true,
					uploadedStrDataSource: null,
					haplogroup: null
				)
			)
		};
	}
	
	private static object[] RegistrationDataWithStrData() {
		int userId = 1;
		return new object[] {
			new SaveAndFetchRegistrationDataTestSource(
				inputRegistrationData: new RegistrationData(
					userId: userId,
					user: CreateRegistrationUser(password: "PASSWORD"),
					labTestRequested: true,
					uploadedStrDataSource: CreateOriginalStrDataSource(userId: userId),
					haplogroup: null
				),
				expectedRegistrationData: new RegistrationData(
					userId: userId,
					user: CreateRegistrationUser(password: string.Empty),
					labTestRequested: true,
					uploadedStrDataSource: CreateOriginalStrDataSource(userId: userId),
					haplogroup: null
				)
			)
		};
	}
	
	private static object[] RegistrationDataWithStrDataAndHaplogroup() {
		int userId = 1;
		return new object[] {
			new SaveAndFetchRegistrationDataTestSource(
				inputRegistrationData: new RegistrationData(
					userId: userId,
					user: CreateRegistrationUser(password: "PASSWORD"),
					labTestRequested: true,
					uploadedStrDataSource: CreateOriginalStrDataSource(userId: userId),
					haplogroup: new Haplogroup(id: 0, name: "Ra1b")
				),
				expectedRegistrationData: new RegistrationData(
					userId: userId,
					user: CreateRegistrationUser(password: string.Empty),
					labTestRequested: true,
					uploadedStrDataSource: CreateOriginalStrDataSource(userId: userId),
					haplogroup: new Haplogroup(id: 0, name: "Ra1b")
				)
			)
		};
	}

	public static RegistrationUser CreateRegistrationUser(string password) {
		return new RegistrationUser(
			administrationAgreementSigned: true,
			publicId: "6B29FC40-CA47-1067-B31D-00DD010662DA",
			profile: new Profile(
				givenName: "Mira",
				familyName: "konec",
				email: "c@gg.com",
				phoneNumber: "435567555",
				residenceAddress: new ExpandedAddress(
					town: "Misek",
					municipality: "Praha",
					county: "Plzen",
					street: "Malostranksá 21",
					zipCode: "22222"
				),
				correspondenceAddress: new Address(
					town: "Vrchlabi",
					street: "U Rudolfína 21",
					zipCode: "34400"
				),
				birthDate: DateOnly.FromDateTime(DateTime.UtcNow)
			),
			ancestor: new Ancestor(
				address: new ExpandedAddress(
					town: "Jicin",
					municipality: "Brno",
					county: "Brnisko",
					street: "U kohouta 23",
					zipCode: "99999"
				),
				givenName: "Tom",
				familyName: "Lužan",
				birthDate: DateOnly.FromDateTime(DateTime.UtcNow.Subtract(new TimeSpan(3, 3, 3, 3))),
				placeOfBirth: "Otava",
				deathDate: DateOnly.FromDateTime(DateTime.UtcNow.Subtract(new TimeSpan(2, 3, 3, 3))),
				placeOfDeath: "Ostrava"
			),
			regionId: 1,
			password: password
		);
	}

	public static OriginalStrDataSource CreateOriginalStrDataSource(int userId) {
		return new OriginalStrDataSource(
			userId: userId,
			testProvider: new GeneticTestProvider("KU"),
			strMarkers: new StrMarkerCollection<string, int>(new Dictionary<string, int>() {
				{"DYS389", 12},
				{"ABC", 17},
				{"PP", 15},
				{"OIOPO", 21},
				{"ksjdh", 13},
				{"KJ", 0}
			}),
			fileName: "FILENAME.txt"
			
		);
	}
}