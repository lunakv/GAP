﻿using DataLoader.MockDataLoaders;
using PostgreSqlStorage;
using PostgreSqlStorage.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostgreSqlStorageTests;

public class TestDatabaseFixture {
    private const string ConnectionString =
        @":-)";

    private static readonly object _lock = new();
    private static bool _databaseInitialized;

    public TestDatabaseFixture() {
        lock (_lock) {
            if (!_databaseInitialized) {
                using (var context = CreateContext()) {
                    context.Database.EnsureDeleted();
                    context.Database.EnsureCreated();

                    context.SaveChanges();
                }

                _databaseInitialized = true;
            }
        }
    }

    public void ResetDatabase() {
        lock (_lock) {
            using var context = CreateContext();
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            context.SaveChanges();
        }
    }

    public GapContext CreateContext() {
        var optionsBuilder = new DbContextOptionsBuilder<GapContext>();
        optionsBuilder.UseNpgsql(ConnectionString);
        return new GapContext(optionsBuilder.Options);
    }

}
