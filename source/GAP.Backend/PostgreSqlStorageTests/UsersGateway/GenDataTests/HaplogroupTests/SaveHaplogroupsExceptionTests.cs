﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using PostgreSqlStorage.Data;
using PostgreSqlStorage.UsersGateway.GenData.Accessors;
using Users.GenData.Haplogroup;
using Xunit;

namespace PostgreSqlStorageTests.UsersGateway.GenDataTests.HaplogroupTests;

[Collection(nameof(TestCollection))]
public class SaveHaplogroupsExceptionTests : IDisposable, IClassFixture<TestDatabaseFixture> {

    private readonly TestDatabaseFixture _fixture;

    private readonly GapContext _context;

    public SaveHaplogroupsExceptionTests(TestDatabaseFixture fixture) {
        _fixture = fixture;
        _fixture.ResetDatabase();
        _context = _fixture.CreateContext();
    }

    public void Dispose() {
        _context.Dispose();
    }
}
