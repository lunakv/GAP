﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLoader.MockDataLoaders;
using FluentAssertions;
using PostgreSqlStorage.Data;
using PostgreSqlStorage.UsersGateway.GenData.Accessors;
using Users.GenData.Haplogroup;
using Xunit;

namespace PostgreSqlStorageTests.UsersGateway.GenDataTests.HaplogroupTests;

[Collection(nameof(TestCollection))]
public sealed class SaveAndFetchHaplogroupsTests : IDisposable, IClassFixture<TestDatabaseFixture> {

    private readonly TestDatabaseFixture _fixture;

    private readonly GapContext _context;

    public SaveAndFetchHaplogroupsTests(TestDatabaseFixture fixture) {
        _fixture = fixture;
        _fixture.ResetDatabase();
        _context = _fixture.CreateContext();
}

    public void Dispose() {
        _context.Dispose();
    }

    [Theory]
    [MemberData(nameof(SaveAndGetHaplogroupsData))]
    public void SaveAndGetHaplogroups(SaveAndFetchHaplogroupsTestSource testSource) {

        new HaplogroupSaver(_context).AddHaplogroupsAsync(testSource.Haplogroups).Wait();
        
        _context.ChangeTracker.Clear();

        IList<Haplogroup> fetchedHaplogroups = new HaplogroupFetcher(_context).GetHaplogroupsAsync().Result;

        fetchedHaplogroups.Should().HaveCount(testSource.Haplogroups.Count);
        fetchedHaplogroups.Select(h => h.Name).Should().BeEquivalentTo(testSource.Haplogroups, 
            options => options.ComparingByMembers<string>());
    }

    [Fact]
    public void SaveAndGetHaplogroupsMultipleSaveCalls() {
        
        IList<string> firstHaplogroups = new List<string> {
            "N1",
            "M1"
        };

        IList<string> secondHaplogroups = new List<string> {
            "A2",
            "B2"
        };

        IList<string> allSavedHaplogroups = firstHaplogroups.Concat(secondHaplogroups).ToList();

        new HaplogroupSaver(_context).AddHaplogroupsAsync(firstHaplogroups).Wait();
        new HaplogroupSaver(_context).AddHaplogroupsAsync(secondHaplogroups).Wait();
        
        _context.ChangeTracker.Clear();

        IList<Haplogroup> fetchedHaplogroups = new HaplogroupFetcher(_context).GetHaplogroupsAsync().Result;

        fetchedHaplogroups.Should().HaveCount(allSavedHaplogroups.Count);
        fetchedHaplogroups.Select(h => h.Name).Should().BeEquivalentTo(allSavedHaplogroups, 
            options => options.ComparingByMembers<string>());
    }

    [Theory]
    [MemberData(nameof(SaveAndGetHaplogroupsData))]
    public void SaveAndGetHaplogroup(SaveAndFetchHaplogroupsTestSource testSource)
    {
        foreach (string haplogroup in testSource.Haplogroups) {
            new HaplogroupSaver(_context).AddHaplogroupAsync(haplogroup).Wait();
        }

        IList<Haplogroup> fetchedHaplogroups = new HaplogroupFetcher(_context).GetHaplogroupsAsync().Result;

        fetchedHaplogroups.Should().HaveCount(testSource.Haplogroups.Count);
        fetchedHaplogroups.Select(h => h.Name).Should().BeEquivalentTo(testSource.Haplogroups, 
            options => options.ComparingByMembers<string>());
    }

    public static IEnumerable<object[]> SaveAndGetHaplogroupsData() {
        yield return SingleHaplogroup();
        yield return MultipleHaplogroupsSimple();
        yield return MultipleHaplogroupsComplex();
    }

    private static object[] SingleHaplogroup() {
        return new object[] { new SaveAndFetchHaplogroupsTestSource(
                haplogroups: new List<string> {
                    "Ra1"
                }
            )
        };
    }

    private static object[] MultipleHaplogroupsSimple() {
        return new object[] { new SaveAndFetchHaplogroupsTestSource(
                haplogroups: new List<string> {
                    "N2",
                    "Ia2"
                }
            )
        };
    }
    
    private static object[] MultipleHaplogroupsComplex() {
        return new object[] { new SaveAndFetchHaplogroupsTestSource(
                haplogroups: new MockHaplogroupSource().GetData().Select(hs => hs.Name).ToList()
            )
        };
    }

}
