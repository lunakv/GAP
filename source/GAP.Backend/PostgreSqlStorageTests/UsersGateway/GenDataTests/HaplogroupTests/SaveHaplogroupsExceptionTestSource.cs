﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TestUtils;
using Users.GenData.Haplogroup;

namespace PostgreSqlStorageTests.UsersGateway.GenDataTests.HaplogroupTests;

public class SaveHaplogroupsExceptionTestSource : TestSource {

    public SaveHaplogroupsExceptionTestSource(
       IList<Haplogroup> haplogroups,
       Exception exception,
       [CallerMemberName] string testName = "No test name provided.") : base(testName) {
        Haplogroups = haplogroups;
        ExceptionType = exception.GetType();
    }

    public IList<Haplogroup> Haplogroups { get; }

    public Type ExceptionType { get; }
}
