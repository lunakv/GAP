﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TestUtils;
using Users.GenData.Haplogroup;

namespace PostgreSqlStorageTests.UsersGateway.GenDataTests.HaplogroupTests;

public class SaveAndFetchHaplogroupsTestSource : TestSource {

    public SaveAndFetchHaplogroupsTestSource(
       IList<string> haplogroups,
       [CallerMemberName] string testName = "No test name provided.") : base(testName) {
        Haplogroups = haplogroups;
    }

    public IList<string> Haplogroups { get; }
}
