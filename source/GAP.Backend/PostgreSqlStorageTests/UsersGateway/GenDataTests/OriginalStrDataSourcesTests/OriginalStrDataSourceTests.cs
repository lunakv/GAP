using System;
using System.Collections.Generic;
using System.Linq;
using DataLoader.MockDataLoaders;
using FluentAssertions;
using PostgreSqlStorage.Data;
using PostgreSqlStorage.MapGateway.Accessors;
using PostgreSqlStorage.UsersGateway.GenData.Accessors;
using PostgreSqlStorage.UsersGateway.UserAccess.Accessors;
using PostgreSqlStorageTests.UsersGateway.UserAccessTests;
using TestUtils;
using Users.GenData;
using Users.GenData.Haplogroup;
using Users.GenData.OriginalStrDataSources;
using Users.GenData.Parsers;
using Users.UserAccess.Entities;
using Xunit;

namespace PostgreSqlStorageTests.UsersGateway.GenDataTests.OriginalStrDataSourcesTests; 

[Collection(nameof(TestCollection))]
public class OriginalStrDataSourceTests : IDisposable, IClassFixture<TestDatabaseFixture> {

	private readonly TestDatabaseFixture _fixture;
	private readonly GapContext _context;

	public OriginalStrDataSourceTests(TestDatabaseFixture fixture) {
		_fixture = fixture;
		_fixture.ResetDatabase();
		_context = _fixture.CreateContext();
		
		new AdministrativeRegionLayerStorageSaver(_context).AddLayers(
			new MockAdministrationRegionsSource().GetData()
		);

		new HaplogroupSaver(_context).AddHaplogroupsAsync(
			new MockHaplogroupSource(haplogroupsCount: 50).GetData().Select(hs => hs.Name).ToList()
		).Wait();
		
		foreach (User user in UserData()) {
			new UserSaver(_context).AddUserAsync(user: user).Wait();
		}
		
		_context.ChangeTracker.Clear();
	}

	public void Dispose() {
		_context.Dispose();
	}

	[Theory]
	[MemberData(nameof(AddAndFetchStrDataSourceData))]
	public void AddAndFetchStrDataSource(OriginalStrDataTestSource testSource) {
		List<OriginalStrDataSource> savedSources = new List<OriginalStrDataSource>();
		foreach (OriginalStrDataSource strDataSource in testSource.OriginalStrDataSources) {
			savedSources.Add(new OriginalStrDataSourceSaver(_context).AddOriginalStrDataSourceAsync(strDataSource).Result);
		}

		savedSources.Should().BeEquivalentTo(
			expectation: testSource.OriginalStrDataSources,
			config: o => o.Excluding(source => source.Id)
		);
		
		_context.ChangeTracker.Clear();

		var sources = new OriginalStrDataSourceFetcher(_context).GetOriginalStrDataSourcesAsync(
			sourceIds: new int[] { savedSources[0].Id, savedSources[2].Id, savedSources[3].Id }
		).Result;

		sources[savedSources[0].Id].Should().BeEquivalentTo(expectation: savedSources[0]);
		sources[savedSources[2].Id].Should().BeEquivalentTo(expectation: savedSources[2]);
		sources[savedSources[3].Id].Should().BeEquivalentTo(expectation: savedSources[3]);

		var user0Sources = new OriginalStrDataSourceFetcher(_context).GetOriginalStrDataSourceAsync(userId: 0).Result;
		user0Sources.Should().BeEquivalentTo(expectation: savedSources.Where(s => s.UserId == 0));
		
		var user1Sources = new OriginalStrDataSourceFetcher(_context).GetOriginalStrDataSourceAsync(userId: 1).Result;
		user1Sources.Should().BeEquivalentTo(expectation: savedSources.Where(s => s.UserId == 1));
		
		var user2Sources = new OriginalStrDataSourceFetcher(_context).GetOriginalStrDataSourceAsync(userId: 2).Result;
		user2Sources.Should().BeEquivalentTo(expectation: savedSources.Where(s => s.UserId == 2));
	}

	public static IEnumerable<object[]> AddAndFetchStrDataSourceData() {
		yield return StrSourceData();
	}
	
	[Fact]
	public void IdManagementTest() {
		List<OriginalStrDataSource> addedSources = new List<OriginalStrDataSource>();
		for (int i = 0; i < 10; ++i) {
			addedSources.Add(
				new OriginalStrDataSourceSaver(_context).AddOriginalStrDataSourceAsync(
					originalStrDataSource: new OriginalStrDataSource(
						id: -1,
						userId: 0,
						testProvider: new GeneticTestProvider(name: "TestTsP"),
						strMarkers: new StrMarkerCollection<string, int>(markers: new Dictionary<string, int>() {
							{ "DYS5d76", 20 }, { "aDYS389 I", 13 }, { "DYSd4248", 13 }, { "YGATAH", 13 }, { "DYS448a", 13 }, { "DYS389II", 13 },
						}),
						fileName: "FTDNAAA"
					)
				).Result
			);
			addedSources[^1].Id.Should().NotBe(unexpected: -1);
			foreach (OriginalStrDataSource addedSource in addedSources.GetRange(index: 0, count: addedSources.Count - 1)) {
				addedSources[^1].Id.Should().NotBe(unexpected: addedSource.Id);
			}
		}
	}
	
	private static object[] StrSourceData() {
		return new object[] {
			new OriginalStrDataTestSource(
				originalStrDataSources: new List<OriginalStrDataSource>() {
					new OriginalStrDataSource(
						userId: 0,
						testProvider: new GeneticTestProvider(name: "TestTsP"),
						strMarkers: new StrMarkerCollection<string, int>(markers: new Dictionary<string, int>() {
							{ "DYS5d76", 20 }, { "aDYS389 I", 13 }, { "DYSd4248", 13 }, { "YGATAH", 13 }, { "DYS448a", 13 }, { "DYS389II", 13 }, 
						}),
						fileName: "FTDNAAA"
					),
					new OriginalStrDataSource(
						userId: 2,
						testProvider: new GeneticTestProvider(name: "TestTdaP"),
						strMarkers: new StrMarkerCollection<string, int>(markers: new Dictionary<string, int>() {
							{ "DYS5s76", 20 }, { "DYS3a89 I", 13 }, { "DYS4d48", 13 }, { "YGATAH", 13 }, { "DYS448a", 13 }, { "DYS389II", 13 }, 
						}),
						fileName: "KU"
					),
					new OriginalStrDataSource(
						userId: 1,
						testProvider: new GeneticTestProvider(name: "TestTdP"),
						strMarkers: new StrMarkerCollection<string, int>(markers: new Dictionary<string, int>() {
							{ "DYS576", 20 }, { "DYS389 I", 13 }, { "DYaS448", 13 }, { "YGATAH", 13 }, { "DYS448a", 13 }, { "DY2S389II", 123 }, 
						}),
						fileName: "GENOMAC"
					),
					new OriginalStrDataSource(
						userId: 2,
						testProvider: new GeneticTestProvider(name: "TestfTP"),
						strMarkers: new StrMarkerCollection<string, int>(markers: new Dictionary<string, int>() {
							{ "DYS576", 20 }, { "DYSdf389 I", 13 }, { "DYS4448", 13 }, { "YGATAH", 13 }, { "DYS3448a", 13 }, { "DY2S389II", 13 }, 
						}),
						fileName: "FTDNA"
					),
					new OriginalStrDataSource(
						userId: 2,
						testProvider: new GeneticTestProvider(name: "TestgTP"),
						strMarkers: new StrMarkerCollection<string, int>(markers: new Dictionary<string, int>() {
							{ "DYS5df76", 21 }, { "DYS389 I", 123 }, { "DYS4d48", 143 }, { "YGAdTAH", 133 }, { "DYS44d8a", 143 }, { "DYS389II", 113 }, 
						}),
						fileName: null
					)
				}
			)
		};
	}

	private static List<User> UserData() {
		IList<Haplogroup> haplogroups = TestUtils.UserCreator.GetHaplogroups();

		return new List<User> {
			UserCreator.CreateUser(
				id: 0,
				givenName: "Jaromil",
				familyName: "Petráš",
				regionId: 2,
				email: "fake@gmail.com",
				town: "Jihlava",
				ancestorFamilyName: "Buldozer",
				geneticData: new GeneticData(
					strMarkers: new Dictionary<string, int>() {
						{ "DYS576", 20 }, { "DYS389 I", 14 }, { "DYS448", 13 }, { "DYS389 II", 16 }, { "DYS19", 17 },
						{ "DYS391", 16 }, { "DYS481", 15 }, { "DYS549", 14 }, { "DYS533", 18 },
						{ "DYS438", 19 }, { "DYS437", 12 }, { "DYS570", 14 }, { "DYS635", 21 }, { "DYS390", 18 },
						{ "DYS439", 19 }, { "DYS392", 17 }, { "DYS643", 16 }, { "DYS393", 15 }, { "DYS458", 16 },
						{ "DYS385", 17 }, { "DYS456", 18 }, { "YGATAH", 16 }
					},
					haplogroup: haplogroups[4],
					originalStrDataSources: new List<int>()
				),
				adminNote: "ADMIN NOTE",
				administrationAgreementSigned: true,
				publicId: "xx",
				ancestorPlaceOfBirth: "Jihlava",
				correspondenceAddress: true,
				adminTown: "ADMIN TOWN"
			),
			UserCreator.CreateUser(
				id: 1,
				givenName: "Tom",
				familyName: "Mike",
				regionId: 4,
				email: "fake2@seznam.cz",
				town: "Hradec",
				ancestorFamilyName: "Kral",
				geneticData: new GeneticData(
					strMarkers: new Dictionary<string, int>() {
						{ "DYS576", 23 }, { "DYS389 I", 15 }, { "DYS448", 14 }, { "DYS389 II", 16 }, { "DYS19", 17 },
						{ "DYS391", 17 }, { "DYS481", 15 }, { "DYS549", 16 }, { "DYS533", 18 },
						{ "DYS438", 18 }, { "DYS437", 12 }, { "DYS570", 13 }, { "DYS635", 21 }, { "DYS390", 19 },
						{ "DYS439", 19 }, { "DYS392", 16 }, { "DYS643", 12 }, { "DYS393", 15 }, { "DYS458", 14 },
						{ "DYS385", 17 }, { "DYS456", 19 }, { "YGATAH", 14 }
					},
					haplogroup: haplogroups[5],
					originalStrDataSources: new List<int>()
				)
			),
			UserCreator.CreateUser(
				id: 2,
				givenName: "Cert",
				familyName: "Cerv",
				regionId: 2,
				email: "OO@OO.com",
				administrationAgreementSigned: false,
				town: "Praha",
				ancestorFamilyName: "Cisar",
				geneticData: new GeneticData(
					strMarkers: new Dictionary<string, int>() {
						{ "DYS576", 23 }, { "DYS389 I", 13 }, { "DYS448", 14 }, { "DYS389 II", 16 }, { "DYS19", 17 },
						{ "DYS391", 17 }, { "DYS481", 15 }, { "DYS549", 16 }, { "DYS533", 18 },
						{ "DYS438", 18 }, { "DYS437", 13 }, { "DYS570", 13 }, { "DYS635", 21 }, { "DYS390", 19 },
						{ "DYS439", 14 }, { "DYS392", 16 }, { "DYS643", 12 }, { "DYS393", 15 }, { "DYS458", 14 },
						{ "DYS385", 17 }, { "DYS456", 13 }, { "YGATAH", 14 }
					},
					haplogroup: haplogroups[1],
					originalStrDataSources: new List<int>()
				)
			),
			UserCreator.CreateUser(
				id: 3,
				givenName: "Sunka",
				familyName: "Pacovsky",
				regionId: 4,
				email: "fake2@gmail.cz",
				town: "Pardubice",
				ancestorFamilyName: "Pacak",
				geneticData: new GeneticData(
					strMarkers: new Dictionary<string, int>() {
						{ "DYS5762", 23 }, { "DYS389 I", 15 }, { "DYS448", 14 }, { "DYS389 II", 16 }, { "DYS19", 17 },
						{ "DYS391", 17 }, { "DYS481", 15 }, { "DYS549", 16 }, { "DYS533", 18 },
						{ "DYS438", 18 }, { "D3YS437", 12 }, { "DYS570", 13 }, { "DYS6335", 21 }, { "DYS390", 19 },
						{ "DYS439", 19 }, { "DYS392", 16 }, { "DYS2643", 12 }, { "DYS393", 15 }, { "DYS458", 14 },
						{ "DYS385", 17 }, { "DYS3456", 19 }, { "YGATAH", 14 }
					},
					haplogroup: haplogroups[2],
					originalStrDataSources: new List<int>()
				)
			),
			UserCreator.CreateUser(
				id: 4,
				givenName: "Michal",
				familyName: "Konik",
				regionId: 6,
				email: "f34@seznam.cz",
				town: "vch",
				ancestorFamilyName: "Jak",
				geneticData: new GeneticData(
					strMarkers: new Dictionary<string, int>() {
						{ "DYS576", 23 }, { "DYS389 I", 15 }, { "DYS448", 14 }, { "DYS389 II", 16 }, { "DYS19", 17 },
						{ "DYS391", 17 }, { "DYS481", 13 }, { "DYS549", 16 }, { "DYS533", 18 },
						{ "DYS438", 18 }, { "DYS437", 12 }, { "DYS570", 23 }, { "DYS635", 23 }, { "DYS390", 19 },
						{ "DYS439", 19 }, { "DYS392", 16 }, { "DYS643", 12 }, { "DYS393", 15 }, { "DYS458", 14 },
						{ "DYS385", 17 }, { "DYS456", 19 }, { "YGATAH", 14 }
					},
					haplogroup: haplogroups[4],
					originalStrDataSources: new List<int>()
				)
			),
		};
	}
}