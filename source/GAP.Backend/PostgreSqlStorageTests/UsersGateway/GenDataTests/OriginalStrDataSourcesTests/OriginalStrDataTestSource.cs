using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TestUtils;
using Users.GenData.OriginalStrDataSources;
using Users.UserAccess.Entities;

namespace PostgreSqlStorageTests.UsersGateway.GenDataTests.OriginalStrDataSourcesTests;

public class OriginalStrDataTestSource : TestSource {
	public OriginalStrDataTestSource(
		IList<OriginalStrDataSource> originalStrDataSources,
		[CallerMemberName] string testName = "No test name provided."
	) : base(testName) {
		OriginalStrDataSources = originalStrDataSources;
	}

	public IList<OriginalStrDataSource> OriginalStrDataSources { get; }
}