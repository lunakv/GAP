using System;
using System.Collections.Generic;
using System.Linq;
using DataLoader.MockDataLoaders;
using FluentAssertions;
using PostgreSqlStorage.Data;
using PostgreSqlStorage.MapGateway.Accessors;
using PostgreSqlStorage.UsersGateway.GenData.Accessors;
using PostgreSqlStorage.UsersGateway.UserAccess.Accessors;
using TestUtils;
using Users.GenData;
using Users.GenData.Haplogroup;
using Users.UserAccess.Entities;
using Xunit;

namespace PostgreSqlStorageTests.UsersGateway.UserAccessTests; 

[Collection(nameof(TestCollection))]
public class UpdateUserTests : IDisposable, IClassFixture<TestDatabaseFixture> {
	
	private readonly TestDatabaseFixture _fixture;

	private readonly GapContext _context;

	public UpdateUserTests(TestDatabaseFixture fixture) {
		_fixture = fixture;
		_fixture.ResetDatabase();
		_context = _fixture.CreateContext();

		new AdministrativeRegionLayerStorageSaver(_context).AddLayers(
			new MockAdministrationRegionsSource().GetData()
		);

		new HaplogroupSaver(_context).AddHaplogroupsAsync(
			new MockHaplogroupSource(haplogroupsCount: 50).GetData().Select(hs => hs.Name).ToList()
		).Wait();

		foreach (User user in UserData()) {
			new UserSaver(_context).AddUserAsync(user: user).Wait();
		}
		
		_context.ChangeTracker.Clear();
	}

	public void Dispose() {
		_context.Dispose();
	}
	
	[Fact]
	public void UpdateProfileTest() {
		const int testedUserId = 2;

		User initialUser = new UserFetcher(_context).FindUserByIdAsync(userId: testedUserId).Result;

		Profile profile = new Profile(
			givenName: "Vlastimil",
			familyName: "Dobry",
			email: "Vlasta@gmail.com",
			phoneNumber: "666555999",
			residenceAddress: new ExpandedAddress(
				town: "Sternberk",
				municipality: "Olomouc",
				county: "Olomoucky kraj",
				street: "Zbyskova 33",
				zipCode: "77900"
			),
			correspondenceAddress: new Address(
				town: "Frystak",
				street: "Tomasova 22",
				zipCode: "76316"
			),
			birthDate: new DateOnly(year: 2000, month: 4, day: 15)
		);
		const int regionId = 3;

		new UserSaver(_context).UpdateProfileAsync(userId: testedUserId, profile: profile, regionId: regionId).Wait();
		
		_context.ChangeTracker.Clear();

		User updatedUser = new UserFetcher(_context).FindUserByIdAsync(userId: testedUserId).Result;

		updatedUser.Id.Should().Be(expected: initialUser.Id);
		updatedUser.AdministrationAgreementSigned.Should().Be(expected: initialUser.AdministrationAgreementSigned);
		updatedUser.PublicId.Should().Be(expected: initialUser.PublicId);
		updatedUser.Profile.Should().BeEquivalentTo(expectation: profile);
		updatedUser.Ancestor.Should().BeEquivalentTo(expectation: initialUser.Ancestor);
		updatedUser.RegionId.Should().Be(expected: regionId);
		updatedUser.AdministratorData.Should().BeEquivalentTo(expectation: initialUser.AdministratorData);
		updatedUser.GeneticData.Should().BeEquivalentTo(expectation: initialUser.GeneticData);
	}
	
	[Fact]
	public void UpdateAncestorTest() {
		const int testedUserId = 2;

		User initialUser = new UserFetcher(_context).FindUserByIdAsync(userId: testedUserId).Result;

		Ancestor ancestor = new Ancestor(
			address: new ExpandedAddress(
				town: "Brasy",
				municipality: "Radnice",
				county: "Plzensky kraj",
				street: "Radnicni 32",
				zipCode: "33828"
			),
			givenName: "Zbysek",
			familyName: "Stary",
			birthDate: new DateOnly(year: 1900, month: 9, day: 30),
			placeOfBirth: "Konopiste",
			deathDate: new DateOnly(year: 1930, month: 8, day: 29),
			placeOfDeath: "Kraluv Dvur"
		);

		new UserSaver(_context).UpdateAncestorAsync(userId: testedUserId, ancestor: ancestor).Wait();
		
		_context.ChangeTracker.Clear();

		User updatedUser = new UserFetcher(_context).FindUserByIdAsync(userId: testedUserId).Result;

		updatedUser.Id.Should().Be(expected: initialUser.Id);
		updatedUser.AdministrationAgreementSigned.Should().Be(expected: initialUser.AdministrationAgreementSigned);
		updatedUser.PublicId.Should().Be(expected: initialUser.PublicId);
		updatedUser.Profile.Should().BeEquivalentTo(expectation: initialUser.Profile);
		updatedUser.Ancestor.Should().BeEquivalentTo(expectation: ancestor);
		updatedUser.RegionId.Should().Be(expected: initialUser.RegionId);
		updatedUser.AdministratorData.Should().BeEquivalentTo(expectation: initialUser.AdministratorData);
		updatedUser.GeneticData.Should().BeEquivalentTo(expectation: initialUser.GeneticData);
	}
	
	[Fact]
	public void UpdateGeneticDataTest() {
		const int testedUserId = 2;

		User initialUser = new UserFetcher(_context).FindUserByIdAsync(userId: testedUserId).Result;

		GeneticData geneticData = new GeneticData(
			strMarkers: new Dictionary<string, int>() {
				{ "DYS576", 20 }, { "DYS389 I", 13 }, { "DYS448", 13 }, { "DYS389 II", 16 }, { "DYS19", 17 },
				{ "DYS-391", 16 }, { "DYS481", 14 }, { "DYS549", 14 }, { "DYS533", 18 },
				{ "DYS438", 19 }, { "DYS437", 17 }, { "DYS570", 124 }, { "DYS635", 21 }, { "DYS390", 18 },
				{ "DYS43 9", 193 }, { "DYS392", 17 }, { "DYS643", 16 }, { "DYS393", 153 }, { "DYS458", 165 },
				{ "DYS385", 17 }, { "DYS456", 18 }, { "Y-GATA-H", 16 }
			},
			haplogroup: new Haplogroup(id: 5, name: "new haplo whose name is irrelevant"),
			originalStrDataSources: new List<int>()
		);

		new UserSaver(_context).UpdateGeneticDataAsync(userId: testedUserId, geneticData: geneticData).Wait();
		
		_context.ChangeTracker.Clear();

		User updatedUser = new UserFetcher(_context).FindUserByIdAsync(userId: testedUserId).Result;

		updatedUser.Id.Should().Be(expected: initialUser.Id);
		updatedUser.AdministrationAgreementSigned.Should().Be(expected: initialUser.AdministrationAgreementSigned);
		updatedUser.PublicId.Should().Be(expected: initialUser.PublicId);
		updatedUser.Profile.Should().BeEquivalentTo(expectation: initialUser.Profile);
		updatedUser.Ancestor.Should().BeEquivalentTo(expectation: initialUser.Ancestor);
		updatedUser.RegionId.Should().Be(expected: initialUser.RegionId);
		updatedUser.AdministratorData.Should().BeEquivalentTo(expectation: initialUser.AdministratorData);
		updatedUser.GeneticData.Should().BeEquivalentTo(expectation: geneticData);
	}
	
	[Fact]
	public void UpdateAdministrationAgreementSignStatusTest() {
		const int testedUserId = 2;

		User initialUser = new UserFetcher(_context).FindUserByIdAsync(userId: testedUserId).Result;

		new UserSaver(_context).UpdateAdministrationAgreementSignStatus(userId: testedUserId, administrationAggreementSigned: true).Wait();
		
		_context.ChangeTracker.Clear();

		User updatedUser = new UserFetcher(_context).FindUserByIdAsync(userId: testedUserId).Result;

		updatedUser.Id.Should().Be(expected: initialUser.Id);
		updatedUser.AdministrationAgreementSigned.Should().Be(expected: true);
		updatedUser.PublicId.Should().Be(expected: initialUser.PublicId);
		updatedUser.Profile.Should().BeEquivalentTo(expectation: initialUser.Profile);
		updatedUser.Ancestor.Should().BeEquivalentTo(expectation: initialUser.Ancestor);
		updatedUser.RegionId.Should().Be(expected: initialUser.RegionId);
		updatedUser.AdministratorData.Should().BeEquivalentTo(expectation: initialUser.AdministratorData);
		updatedUser.GeneticData.Should().BeEquivalentTo(expectation: initialUser.GeneticData);
		
		new UserSaver(_context).UpdateAdministrationAgreementSignStatus(userId: testedUserId, administrationAggreementSigned: false).Wait();
		
		_context.ChangeTracker.Clear();

		updatedUser = new UserFetcher(_context).FindUserByIdAsync(userId: testedUserId).Result;

		updatedUser.Id.Should().Be(expected: initialUser.Id);
		updatedUser.AdministrationAgreementSigned.Should().Be(expected: false);
		updatedUser.PublicId.Should().Be(expected: initialUser.PublicId);
		updatedUser.Profile.Should().BeEquivalentTo(expectation: initialUser.Profile);
		updatedUser.Ancestor.Should().BeEquivalentTo(expectation: initialUser.Ancestor);
		updatedUser.RegionId.Should().Be(expected: initialUser.RegionId);
		updatedUser.AdministratorData.Should().BeEquivalentTo(expectation: initialUser.AdministratorData);
		updatedUser.GeneticData.Should().BeEquivalentTo(expectation: initialUser.GeneticData);
	}
	
	[Fact]
	public void UpdateEmailTest() {
		const int testedUserId = 2;

		User initialUser = new UserFetcher(_context).FindUserByIdAsync(userId: testedUserId).Result;

		const string email = @"jindra.ze.skalice@kc.com";
		
		new UserSaver(_context).UpdateEmailAsync(userId: testedUserId, email: email).Wait();
		
		_context.ChangeTracker.Clear();

		User updatedUser = new UserFetcher(_context).FindUserByIdAsync(userId: testedUserId).Result;

		Profile expectedProfile = new Profile(
			givenName: initialUser.Profile.GivenName,
			familyName: initialUser.Profile.FamilyName,
			email: email,
			phoneNumber: initialUser.Profile.PhoneNumber,
			residenceAddress: initialUser.Profile.ResidenceAddress,
			correspondenceAddress: initialUser.Profile.CorrespondenceAddress,
			birthDate: initialUser.Profile.BirthDate
		);
		
		updatedUser.Id.Should().Be(expected: initialUser.Id);
		updatedUser.AdministrationAgreementSigned.Should().Be(expected: initialUser.AdministrationAgreementSigned);
		updatedUser.PublicId.Should().Be(expected: initialUser.PublicId);
		updatedUser.Profile.Should().BeEquivalentTo(expectation: expectedProfile);
		updatedUser.Ancestor.Should().BeEquivalentTo(expectation: initialUser.Ancestor);
		updatedUser.RegionId.Should().Be(expected: initialUser.RegionId);
		updatedUser.AdministratorData.Should().BeEquivalentTo(expectation: initialUser.AdministratorData);
		updatedUser.GeneticData.Should().BeEquivalentTo(expectation: initialUser.GeneticData);
	}
	
	private static List<User> UserData() {
		IList<Haplogroup> haplogroups = TestUtils.UserCreator.GetHaplogroups();

		return new List<User> {
			UserCreator.CreateUser(
				id: 0,
				givenName: "Jaromil",
				familyName: "Petráš",
				regionId: 2,
				email: "fake@gmail.com",
				town: "Jihlava",
				ancestorFamilyName: "Buldozer",
				geneticData: new GeneticData(
					strMarkers: new Dictionary<string, int>() {
						{ "DYS576", 20 }, { "DYS389 I", 14 }, { "DYS448", 13 }, { "DYS389 II", 16 }, { "DYS19", 17 },
						{ "DYS391", 16 }, { "DYS481", 15 }, { "DYS549", 14 }, { "DYS533", 18 },
						{ "DYS438", 19 }, { "DYS437", 12 }, { "DYS570", 14 }, { "DYS635", 21 }, { "DYS390", 18 },
						{ "DYS439", 19 }, { "DYS392", 17 }, { "DYS643", 16 }, { "DYS393", 15 }, { "DYS458", 16 },
						{ "DYS385", 17 }, { "DYS456", 18 }, { "YGATAH", 16 }
					},
					haplogroup: haplogroups[4],
					originalStrDataSources: new List<int>()
				),
				adminNote: "ADMIN NOTE",
				administrationAgreementSigned: true,
				publicId: "xx",
				ancestorPlaceOfBirth: "Jihlava",
				correspondenceAddress: true,
				adminTown: "ADMIN TOWN"
			),
			UserCreator.CreateUser(
				id: 1,
				givenName: "Tom",
				familyName: "Mike",
				regionId: 4,
				email: "fake2@seznam.cz",
				town: "Hradec",
				ancestorFamilyName: "Kral",
				geneticData: new GeneticData(
					strMarkers: new Dictionary<string, int>() {
						{ "DYS576", 23 }, { "DYS389 I", 15 }, { "DYS448", 14 }, { "DYS389 II", 16 }, { "DYS19", 17 },
						{ "DYS391", 17 }, { "DYS481", 15 }, { "DYS549", 16 }, { "DYS533", 18 },
						{ "DYS438", 18 }, { "DYS437", 12 }, { "DYS570", 13 }, { "DYS635", 21 }, { "DYS390", 19 },
						{ "DYS439", 19 }, { "DYS392", 16 }, { "DYS643", 12 }, { "DYS393", 15 }, { "DYS458", 14 },
						{ "DYS385", 17 }, { "DYS456", 19 }, { "YGATAH", 14 }
					},
					haplogroup: haplogroups[5],
					originalStrDataSources: new List<int>()
				)
			),
			UserCreator.CreateUser(
				id: 2,
				givenName: "Cert",
				familyName: "Cerv",
				regionId: 2,
				email: "OO@OO.com",
				administrationAgreementSigned: false,
				town: "Praha",
				ancestorFamilyName: "Cisar",
				geneticData: new GeneticData(
					strMarkers: new Dictionary<string, int>() {
						{ "DYS576", 23 }, { "DYS389 I", 13 }, { "DYS448", 14 }, { "DYS389 II", 16 }, { "DYS19", 17 },
						{ "DYS391", 17 }, { "DYS481", 15 }, { "DYS549", 16 }, { "DYS533", 18 },
						{ "DYS438", 18 }, { "DYS437", 13 }, { "DYS570", 13 }, { "DYS635", 21 }, { "DYS390", 19 },
						{ "DYS439", 14 }, { "DYS392", 16 }, { "DYS643", 12 }, { "DYS393", 15 }, { "DYS458", 14 },
						{ "DYS385", 17 }, { "DYS456", 13 }, { "YGATAH", 14 }
					},
					haplogroup: haplogroups[1],
					originalStrDataSources: new List<int>()
				)
			),
			UserCreator.CreateUser(
				id: 3,
				givenName: "Sunka",
				familyName: "Pacovsky",
				regionId: 4,
				email: "fake2@gmail.cz",
				town: "Pardubice",
				ancestorFamilyName: "Pacak",
				geneticData: new GeneticData(
					strMarkers: new Dictionary<string, int>() {
						{ "DYS5762", 23 }, { "DYS389 I", 15 }, { "DYS448", 14 }, { "DYS389 II", 16 }, { "DYS19", 17 },
						{ "DYS391", 17 }, { "DYS481", 15 }, { "DYS549", 16 }, { "DYS533", 18 },
						{ "DYS438", 18 }, { "D3YS437", 12 }, { "DYS570", 13 }, { "DYS6335", 21 }, { "DYS390", 19 },
						{ "DYS439", 19 }, { "DYS392", 16 }, { "DYS2643", 12 }, { "DYS393", 15 }, { "DYS458", 14 },
						{ "DYS385", 17 }, { "DYS3456", 19 }, { "YGATAH", 14 }
					},
					haplogroup: haplogroups[2],
					originalStrDataSources: new List<int>()
				)
			),
			UserCreator.CreateUser(
				id: 4,
				givenName: "Michal",
				familyName: "Konik",
				regionId: 6,
				email: "f34@seznam.cz",
				town: "vch",
				ancestorFamilyName: "Jak",
				geneticData: new GeneticData(
					strMarkers: new Dictionary<string, int>() {
						{ "DYS576", 23 }, { "DYS389 I", 15 }, { "DYS448", 14 }, { "DYS389 II", 16 }, { "DYS19", 17 },
						{ "DYS391", 17 }, { "DYS481", 13 }, { "DYS549", 16 }, { "DYS533", 18 },
						{ "DYS438", 18 }, { "DYS437", 12 }, { "DYS570", 23 }, { "DYS635", 23 }, { "DYS390", 19 },
						{ "DYS439", 19 }, { "DYS392", 16 }, { "DYS643", 12 }, { "DYS393", 15 }, { "DYS458", 14 },
						{ "DYS385", 17 }, { "DYS456", 19 }, { "YGATAH", 14 }
					},
					haplogroup: haplogroups[4],
					originalStrDataSources: new List<int>()
				)
			),
		};
	}
}