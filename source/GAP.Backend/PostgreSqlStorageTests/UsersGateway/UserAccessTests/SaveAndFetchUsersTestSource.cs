﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TestUtils;
using Users.UserAccess.Entities;

namespace PostgreSqlStorageTests.UsersGateway.UserAccessTests;

public class SaveAndFetchUsersTestSource : TestSource {
	public SaveAndFetchUsersTestSource(
		IList<User> users,
		[CallerMemberName] string testName = "No test name provided.") : base(testName) {
		Users = users;
	}

	public IList<User> Users { get; }
}