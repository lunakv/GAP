﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLoader.MockDataLoaders;
using FluentAssertions;
using Map.ValueObjects;
using PostgreSqlStorage.Data;
using PostgreSqlStorage.MapGateway.Accessors;
using PostgreSqlStorage.UsersGateway.GenData.Accessors;
using PostgreSqlStorage.UsersGateway.UserAccess.Accessors;
using TestUtils;
using Users.GenData;
using Users.GenData.Haplogroup;
using Users.UserAccess.Entities;
using Xunit;

namespace PostgreSqlStorageTests.UsersGateway.UserAccessTests;

[Collection(nameof(TestCollection))]
public sealed class SaveAndFetchUsersTests : IDisposable, IClassFixture<TestDatabaseFixture> {
	private readonly TestDatabaseFixture _fixture;

	private readonly GapContext _context;

	public SaveAndFetchUsersTests(TestDatabaseFixture fixture) {
		_fixture = fixture;
		_fixture.ResetDatabase();
		_context = _fixture.CreateContext();
		
		new AdministrativeRegionLayerStorageSaver(_context).AddLayers(
			new MockAdministrationRegionsSource().GetData()
		);

		new HaplogroupSaver(_context).AddHaplogroupsAsync(
			new MockHaplogroupSource(haplogroupsCount: 50).GetData().Select(hs => hs.Name).ToList()
		).Wait();
		
		_context.ChangeTracker.Clear();
	}

	public void Dispose() {
		_context.Dispose();
	}

	[Theory]
	[MemberData(nameof(SaveUserData))]
	public void SaveUserTest(SaveAndFetchUsersTestSource testSource) {
		foreach(User user in testSource.Users) {
			new UserSaver(_context).AddUserAsync(user).Wait();
		}

		_context.ChangeTracker.Clear();

		IList<User> fetchedUsers = new UserFetcher(_context).GetUsersAsync().Result;

		fetchedUsers.Should().HaveCount(testSource.Users.Count);
		fetchedUsers.Should().BeEquivalentTo(testSource.Users);
	}

	public static IEnumerable<object[]> SaveUserData() {
		yield return SingleUser();
	}
	
	[Theory]
	[MemberData(nameof(GetUsersTestData))]
	public void GetUsersTest(SaveAndFetchUsersTestSource testSource) {
		foreach(User user in testSource.Users) {
			new UserSaver(_context).AddUserAsync(user).Wait();
		}

		_context.ChangeTracker.Clear();

		IList<User> fetchedUsers = new UserFetcher(_context).GetUsersAsync().Result;

		fetchedUsers.Should().HaveCount(testSource.Users.Count);
		fetchedUsers.Should().BeEquivalentTo(testSource.Users);
	}

	public static IEnumerable<object[]> GetUsersTestData() {
		yield return SingleUser();
		yield return SingleUserWithCorresponsenceAddress();
		yield return MultipleUsersSimple();
		yield return MultipleUsersComplex(30);
	}
	
	[Theory]
	[MemberData(nameof(FindUserByIdAsyncTestData))]
	public void FindUserByIdAsyncTest(SaveAndFetchUsersTestSource testSource) {
		foreach(User user in testSource.Users) {
			new UserSaver(_context).AddUserAsync(user).Wait();
		}

		_context.ChangeTracker.Clear();

		new UserFetcher(_context).FindUserByIdAsync(userId: 0).Result.Should().BeEquivalentTo(testSource.Users[0]);
		new UserFetcher(_context).FindUserByIdAsync(userId: 1).Result.Should().BeEquivalentTo(testSource.Users[1]);
		new UserFetcher(_context).FindUserByIdAsync(userId: 2).Result.Should().BeEquivalentTo(testSource.Users[2]);
		new UserFetcher(_context).FindUserByIdAsync(userId: 3).Result.Should().BeEquivalentTo(testSource.Users[3]);
		new UserFetcher(_context).FindUserByIdAsync(userId: 4).Result.Should().BeEquivalentTo(testSource.Users[4]);
	}

	public static IEnumerable<object[]> FindUserByIdAsyncTestData() {
		yield return FiveUsersWithIdSameAsIndices();
	}
	
	[Theory]
	[MemberData(nameof(FindUsersByIdAsyncTestData))]
	public void FindUsersByIdAsyncTest(SaveAndFetchUsersTestSource testSource) {
		foreach(User user in testSource.Users) {
			new UserSaver(_context).AddUserAsync(user).Wait();
		}

		_context.ChangeTracker.Clear();

		new UserFetcher(_context).FindUsersByIdAsync(userIds: new []{ 0, 1, 2 })
			.Result.Should().BeEquivalentTo(new Dictionary<int, User>(){ { 0, testSource.Users[0] }, { 1, testSource.Users[1] },  { 2, testSource.Users[2] } });
		new UserFetcher(_context).FindUsersByIdAsync(userIds: new []{ 0 })
			.Result.Should().BeEquivalentTo(new Dictionary<int, User>(){ { 0, testSource.Users[0] } });
		new UserFetcher(_context).FindUsersByIdAsync(userIds: new []{ 1 })
			.Result.Should().BeEquivalentTo(new Dictionary<int, User>(){ { 1, testSource.Users[1] } });
		new UserFetcher(_context).FindUsersByIdAsync(userIds: new []{ 3 })
			.Result.Should().BeEquivalentTo(new Dictionary<int, User>(){ { 3, testSource.Users[3] } });
		new UserFetcher(_context).FindUsersByIdAsync(userIds: new []{ 0, 1, 2, 3, 4 })
			.Result.Should().BeEquivalentTo(new Dictionary<int, User>() {
				{ 0, testSource.Users[0] }, { 1, testSource.Users[1] },  { 2, testSource.Users[2] }, { 3, testSource.Users[3] }, { 4, testSource.Users[4] }
			});
		new UserFetcher(_context).FindUsersByIdAsync(userIds: new []{ 0, 0, 1, 0, 2 })
			.Result.Should().BeEquivalentTo(new Dictionary<int, User>(){ { 0, testSource.Users[0] }, { 1, testSource.Users[1] },  { 2, testSource.Users[2] } });
	}

	public static IEnumerable<object[]> FindUsersByIdAsyncTestData() {
		yield return FiveUsersWithIdSameAsIndices();
	}
	
	[Theory]
	[MemberData(nameof(FindUserByEmailAsyncTestData))]
	public void FindUserByEmailAsyncTest(SaveAndFetchUsersTestSource testSource) {
		foreach(User user in testSource.Users) {
			new UserSaver(_context).AddUserAsync(user).Wait();
		}

		_context.ChangeTracker.Clear();

		new UserFetcher(_context).FindUserByEmailAsync(email: testSource.Users[0].Profile.Email).Result.Should().BeEquivalentTo(testSource.Users[0]);
		new UserFetcher(_context).FindUserByEmailAsync(email: testSource.Users[1].Profile.Email).Result.Should().BeEquivalentTo(testSource.Users[1]);
		new UserFetcher(_context).FindUserByEmailAsync(email: testSource.Users[2].Profile.Email).Result.Should().BeEquivalentTo(testSource.Users[2]);
		new UserFetcher(_context).FindUserByEmailAsync(email: testSource.Users[3].Profile.Email).Result.Should().BeEquivalentTo(testSource.Users[3]);
		new UserFetcher(_context).FindUserByEmailAsync(email: testSource.Users[4].Profile.Email).Result.Should().BeEquivalentTo(testSource.Users[4]);
	}

	public static IEnumerable<object[]> FindUserByEmailAsyncTestData() {
		yield return FiveUsersWithIdSameAsIndices();
	}

	private static object[] SingleUser() {
		IList<Haplogroup> haplogroups = TestUtils.UserCreator.GetHaplogroups();

		return new object[] {
			new SaveAndFetchUsersTestSource(
				users: new List<User> {
					UserCreator.CreateUser(id: 5, geneticData: new GeneticData(
						strMarkers: new Dictionary<string, int>() {
							{ "DYS576", 20 }, { "DYS389 I", 14 }, { "DYS448", 13 }, { "DYS389 II", 16 }, { "DYS19", 17 },
							{ "DYS391", 16 }, { "DYS481", 15 }, { "DYS549", 14 }, { "DYS533", 18 },
							{ "DYS438", 19 }, { "DYS437", 12 }, { "DYS570", 14 }, { "DYS635", 21 }, { "DYS390", 18 },
							{ "DYS439", 19 }, { "DYS392", 17 }, { "DYS643", 16 }, { "DYS393", 15 }, { "DYS458", 16 },
							{ "DYS385", 17 }, { "DYS456", 18 }, { "YGATAH", 16 }
						},
						haplogroup: haplogroups[3],
						new List<int>()
					)),
				}
			)
		};
	}

	private static object[] SingleUserWithCorresponsenceAddress() {
		IList<Haplogroup> haplogroups = TestUtils.UserCreator.GetHaplogroups();

		return new object[] {
			new SaveAndFetchUsersTestSource(
				users: new List<User> {
					UserCreator.CreateUser(
						id: 5,
						geneticData: new GeneticData(
							strMarkers: new Dictionary<string, int>() {
								{ "DYS576", 20 }, { "DYS389 I", 14 }, { "DYS448", 13 }, { "DYS389 II", 16 }, { "DYS19", 17 },
								{ "DYS391", 16 }, { "DYS481", 15 }, { "DYS549", 14 }, { "DYS533", 18 },
								{ "DYS438", 19 }, { "DYS437", 12 }, { "DYS570", 14 }, { "DYS635", 21 }, { "DYS390", 18 },
								{ "DYS439", 19 }, { "DYS392", 17 }, { "DYS643", 16 }, { "DYS393", 15 }, { "DYS458", 16 },
								{ "DYS385", 17 }, { "DYS456", 18 }, { "YGATAH", 16 }
							},
							haplogroup: haplogroups[3],
							originalStrDataSources: new List<int>()
						),
						correspondenceAddress: true),
				}
			)
		};
	}

	private static object[] MultipleUsersSimple() {
		IList<Haplogroup> haplogroups = TestUtils.UserCreator.GetHaplogroups();

		return new object[] {
			new SaveAndFetchUsersTestSource(
				users: new List<User> {
					UserCreator.CreateUser(
						id: 4,
						givenName: "Jaromil",
						familyName: "Petráš",
						regionId: 2,
						email: "fake@gmail.com",
						town: "Jihlava",
						ancestorFamilyName: "Buldozer",
						geneticData: new GeneticData(
							strMarkers: new Dictionary<string, int>() {
								{ "DYS576", 20 }, { "DYS389 I", 14 }, { "DYS448", 13 }, { "DYS389 II", 16 }, { "DYS19", 17 },
								{ "DYS391", 16 }, { "DYS481", 15 }, { "DYS549", 14 }, { "DYS533", 18 },
								{ "DYS438", 19 }, { "DYS437", 12 }, { "DYS570", 14 }, { "DYS635", 21 }, { "DYS390", 18 },
								{ "DYS439", 19 }, { "DYS392", 17 }, { "DYS643", 16 }, { "DYS393", 15 }, { "DYS458", 16 },
								{ "DYS385", 17 }, { "DYS456", 18 }, { "YGATAH", 16 }
							},
							haplogroup: haplogroups[4],
							originalStrDataSources: new List<int>()
						)
					),
					UserCreator.CreateUser(
						id: 5,
						givenName: "Tom",
						familyName: "Mike",
						regionId: 4,
						email: "fake2@seznam.cz",
						town: "Hradec",
						ancestorFamilyName: "Kral",
						geneticData: new GeneticData(
							strMarkers: new Dictionary<string, int>() {
								{ "DYS576", 23 }, { "DYS389 I", 15 }, { "DYS448", 14 }, { "DYS389 II", 16 }, { "DYS19", 17 },
								{ "DYS391", 17 }, { "DYS481", 15 }, { "DYS549", 16 }, { "DYS533", 18 },
								{ "DYS438", 18 }, { "DYS437", 12 }, { "DYS570", 13 }, { "DYS635", 21 }, { "DYS390", 19 },
								{ "DYS439", 19 }, { "DYS392", 16 }, { "DYS643", 12 }, { "DYS393", 15 }, { "DYS458", 14 },
								{ "DYS385", 17 }, { "DYS456", 19 }, { "YGATAH", 14 }
							},
							haplogroup: haplogroups[5],
							originalStrDataSources: new List<int>()
						),
						adminNote: "ADMIN NOTE",
						administrationAgreementSigned: true,
						publicId: "xx",
						ancestorPlaceOfBirth: "Jihlava",
						correspondenceAddress: true,
						adminTown: "ADMIN TOWN"
					),
				}
			)
		};
	}

	private static object[] MultipleUsersComplex(int userCount) {
		IList<Haplogroup> haplogroups = TestUtils.UserCreator.GetHaplogroups();
		IList<AdministrativeRegionLayer> administrationRegionLayers = new MockAdministrationRegionsSource().GetData();

		return new object[] {
			new SaveAndFetchUsersTestSource(
				users: new MockUserSource(
					administrationAgreementSigned: true,
					haplogroups: haplogroups,
					regionKeys: administrationRegionLayers[0].Regions.Select(r => r.Id).ToList(),
					usersToGenerate: userCount
				).GetData()
			)
		};
	}
	
	private static object[] FiveUsersWithIdSameAsIndices() {
		IList<Haplogroup> haplogroups = TestUtils.UserCreator.GetHaplogroups();

		return new object[] {
			new SaveAndFetchUsersTestSource(
				users: new List<User> {
					UserCreator.CreateUser(
						id: 0,
						givenName: "Jaromil",
						familyName: "Petráš",
						regionId: 2,
						email: "fake@gmail.com",
						town: "Jihlava",
						ancestorFamilyName: "Buldozer",
						geneticData: new GeneticData(
							strMarkers: new Dictionary<string, int>() {
								{ "DYS576", 20 }, { "DYS389 I", 14 }, { "DYS448", 13 }, { "DYS389 II", 16 }, { "DYS19", 17 },
								{ "DYS391", 16 }, { "DYS481", 15 }, { "DYS549", 14 }, { "DYS533", 18 },
								{ "DYS438", 19 }, { "DYS437", 12 }, { "DYS570", 14 }, { "DYS635", 21 }, { "DYS390", 18 },
								{ "DYS439", 19 }, { "DYS392", 17 }, { "DYS643", 16 }, { "DYS393", 15 }, { "DYS458", 16 },
								{ "DYS385", 17 }, { "DYS456", 18 }, { "YGATAH", 16 }
							},
							haplogroup: haplogroups[4],
							originalStrDataSources: new List<int>()
						),
						adminNote: "ADMIN NOTE",
						administrationAgreementSigned: true,
						publicId: "xx",
						ancestorPlaceOfBirth: "Jihlava",
						correspondenceAddress: true,
						adminTown: "ADMIN TOWN"
					),
					UserCreator.CreateUser(
						id: 1,
						givenName: "Tom",
						familyName: "Mike",
						regionId: 4,
						email: "fake2@seznam.cz",
						town: "Hradec",
						ancestorFamilyName: "Kral",
						geneticData: new GeneticData(
							strMarkers: new Dictionary<string, int>() {
								{ "DYS576", 23 }, { "DYS389 I", 15 }, { "DYS448", 14 }, { "DYS389 II", 16 }, { "DYS19", 17 },
								{ "DYS391", 17 }, { "DYS481", 15 }, { "DYS549", 16 }, { "DYS533", 18 },
								{ "DYS438", 18 }, { "DYS437", 12 }, { "DYS570", 13 }, { "DYS635", 21 }, { "DYS390", 19 },
								{ "DYS439", 19 }, { "DYS392", 16 }, { "DYS643", 12 }, { "DYS393", 15 }, { "DYS458", 14 },
								{ "DYS385", 17 }, { "DYS456", 19 }, { "YGATAH", 14 }
							},
							haplogroup: haplogroups[5],
							originalStrDataSources: new List<int>()
						)
					),
					UserCreator.CreateUser(
						id: 2,
						givenName: "Cert",
						familyName: "Cerv",
						regionId: 2,
						email: "OO@OO.com",
						town: "Praha",
						ancestorFamilyName: "Cisar",
						geneticData: new GeneticData(
							strMarkers: new Dictionary<string, int>() {
								{ "DYS576", 23 }, { "DYS389 I", 13 }, { "DYS448", 14 }, { "DYS389 II", 16 }, { "DYS19", 17 },
								{ "DYS391", 17 }, { "DYS481", 15 }, { "DYS549", 16 }, { "DYS533", 18 },
								{ "DYS438", 18 }, { "DYS437", 13 }, { "DYS570", 13 }, { "DYS635", 21 }, { "DYS390", 19 },
								{ "DYS439", 14 }, { "DYS392", 16 }, { "DYS643", 12 }, { "DYS393", 15 }, { "DYS458", 14 },
								{ "DYS385", 17 }, { "DYS456", 13 }, { "YGATAH", 14 }
							},
							haplogroup: haplogroups[1],
							originalStrDataSources: new List<int>()
						)
					),
					UserCreator.CreateUser(
						id: 3,
						givenName: "Sunka",
						familyName: "Pacovsky",
						regionId: 4,
						email: "fake2@gmail.cz",
						town: "Pardubice",
						ancestorFamilyName: "Pacak",
						geneticData: new GeneticData(
							strMarkers: new Dictionary<string, int>() {
								{ "DYS5762", 23 }, { "DYS389 I", 15 }, { "DYS448", 14 }, { "DYS389 II", 16 }, { "DYS19", 17 },
								{ "DYS391", 17 }, { "DYS481", 15 }, { "DYS549", 16 }, { "DYS533", 18 },
								{ "DYS438", 18 }, { "D3YS437", 12 }, { "DYS570", 13 }, { "DYS6335", 21 }, { "DYS390", 19 },
								{ "DYS439", 19 }, { "DYS392", 16 }, { "DYS2643", 12 }, { "DYS393", 15 }, { "DYS458", 14 },
								{ "DYS385", 17 }, { "DYS3456", 19 }, { "YGATAH", 14 }
							},
							haplogroup: haplogroups[2],
							originalStrDataSources: new List<int>()
						)
					),
					UserCreator.CreateUser(
						id: 4,
						givenName: "Michal",
						familyName: "Konik",
						regionId: 6,
						email: "f34@seznam.cz",
						town: "vch",
						ancestorFamilyName: "Jak",
						geneticData: new GeneticData(
							strMarkers: new Dictionary<string, int>() {
								{ "DYS576", 23 }, { "DYS389 I", 15 }, { "DYS448", 14 }, { "DYS389 II", 16 }, { "DYS19", 17 },
								{ "DYS391", 17 }, { "DYS481", 13 }, { "DYS549", 16 }, { "DYS533", 18 },
								{ "DYS438", 18 }, { "DYS437", 12 }, { "DYS570", 23 }, { "DYS635", 23 }, { "DYS390", 19 },
								{ "DYS439", 19 }, { "DYS392", 16 }, { "DYS643", 12 }, { "DYS393", 15 }, { "DYS458", 14 },
								{ "DYS385", 17 }, { "DYS456", 19 }, { "YGATAH", 14 }
							},
							haplogroup: haplogroups[4],
							originalStrDataSources: new List<int>()
						)
					),
				}
			)
		};
	}
}