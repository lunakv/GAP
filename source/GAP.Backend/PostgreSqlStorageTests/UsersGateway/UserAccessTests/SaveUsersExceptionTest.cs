﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataLoader.MockDataLoaders;
using FluentAssertions;
using PostgreSqlStorage.Data;
using PostgreSqlStorage.MapGateway.Accessors;
using PostgreSqlStorage.UsersGateway.GenData.Accessors;
using PostgreSqlStorage.UsersGateway.UserAccess.Accessors;
using TestUtils;
using Users.UserAccess.Entities;
using Xunit;

namespace PostgreSqlStorageTests.UsersGateway.UserAccessTests;

[Collection(nameof(TestCollection))]
public class SaveUsersExceptionTest : IDisposable, IClassFixture<TestDatabaseFixture> {

    private readonly TestDatabaseFixture _fixture;

    private readonly GapContext _context;

    public SaveUsersExceptionTest(TestDatabaseFixture fixture) {
        _fixture = fixture;
        _fixture.ResetDatabase();
        _context = _fixture.CreateContext();

        new AdministrativeRegionLayerStorageSaver(_context).AddLayers(
            new MockAdministrationRegionsSource().GetData()
        );

        new HaplogroupSaver(_context).AddHaplogroupsAsync(
            new MockHaplogroupSource().GetData().Select(hs => hs.Name).ToList()
        ).Wait();
        
        _context.ChangeTracker.Clear();
    }

    public void Dispose() {
        _context.Dispose();
    }

    [Fact]
    public void SaveUserWithSameId() {

        IList<User> users = GetDuplicitUsers();

        new UserSaver(_context).Invoking(us => us.AddUserAsync(users[0]).Wait())
            .Should().NotThrow();
        
        _context.ChangeTracker.Clear();

        new UserSaver(_context).Invoking(us => us.AddUserAsync(users[1]).Wait())
            .Should().NotThrow();
        
        _context.ChangeTracker.Clear();

        new UserSaver(_context).Invoking(us => us.AddUserAsync(users[2]).Wait())
            .Should().Throw<Microsoft.EntityFrameworkCore.DbUpdateException>();
    }

    private IList<User> GetDuplicitUsers() {
        return new List<User> {
            UserCreator.CreateUser(id: 5),
            UserCreator.CreateUser(id: 6),
            UserCreator.CreateUser(id: 5)
        };
    }

}
