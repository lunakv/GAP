using System.Collections.Generic;
using System.Linq;
using Map.ValueObjects;

namespace PostgreSqlStorageTests.MapGateway; 

public class RegionData {
	public static (IList<AdministrativeRegionLayer> InputLayers, IList<HierachicalRegionLayer> ExpectedLayers) Get3LayerRegionData() {
        int jilemniceOrpId = 0;
        int turnovOrpId = 1;
        int semilyOrpId = 2;
        int vrchlabiOrpId = 3;
        int novaPakaOrpId = 4;
        int semilyOkresId = 5;
        int trutnovOkresId = 6;
        int jicinOkresId = 7;
        int kraloveHradeckyKrajId = 8;

        var layers = new List<AdministrativeRegionLayer> {
            new AdministrativeRegionLayer(
                id: 0,
                regions: new List<AdministrativeRegion> {
                    new AdministrativeRegion(id: jilemniceOrpId, type: AdministrativeRegion.RegionType.District, name: "Jilemnice", parentRegionId: semilyOkresId),
                    new AdministrativeRegion(id: turnovOrpId, type: AdministrativeRegion.RegionType.District, name: "Turnov", parentRegionId: semilyOkresId),
                    new AdministrativeRegion(id: semilyOrpId, type: AdministrativeRegion.RegionType.District, name: "Semily", parentRegionId: semilyOkresId),
                    new AdministrativeRegion(id: vrchlabiOrpId, type: AdministrativeRegion.RegionType.District, name: "Vrchlabi", parentRegionId: trutnovOkresId),
                    new AdministrativeRegion(id: novaPakaOrpId, type: AdministrativeRegion.RegionType.District, name: "Nova Paka", parentRegionId: jicinOkresId)
                }
            ),
            new AdministrativeRegionLayer(
                id: 1,
                regions: new List<AdministrativeRegion> {
                    new AdministrativeRegion(id: semilyOkresId, type: AdministrativeRegion.RegionType.District, name: "Okres Semily", parentRegionId: kraloveHradeckyKrajId),
                    new AdministrativeRegion(id: trutnovOkresId, type: AdministrativeRegion.RegionType.District, name: "Okres Trutnov", parentRegionId: kraloveHradeckyKrajId),
                    new AdministrativeRegion(id: jicinOkresId, type: AdministrativeRegion.RegionType.District, name: "Okres Jicin", parentRegionId: kraloveHradeckyKrajId)
                }
            ),
            new AdministrativeRegionLayer(
                id: 2,
                regions: new List<AdministrativeRegion> {
                    new AdministrativeRegion(id: kraloveHradeckyKrajId, type: AdministrativeRegion.RegionType.County, name: "Kralovehradecky kraj", parentRegionId: null)
                }
            ),
        };
        
        var jilemniceOrpSubregions = new AdministrativeRegion[] { layers[0].Regions[jilemniceOrpId] };
        var turnovOrpSubregions = new AdministrativeRegion[] { layers[0].Regions[turnovOrpId] };
        var semilyOrpSubregions = new AdministrativeRegion[] { layers[0].Regions[semilyOrpId] };
        var vrchlabiOrpSubregions = new AdministrativeRegion[] { layers[0].Regions[vrchlabiOrpId] };
        var novaPakaOrpSubregions = new AdministrativeRegion[] { layers[0].Regions[novaPakaOrpId] };
        
        var semilyOkresSubregions = jilemniceOrpSubregions.Concat(turnovOrpSubregions).Concat(semilyOrpSubregions).ToArray();
        var trutnovOkresSubregions = vrchlabiOrpSubregions;
        var jicinOkresSubregions = novaPakaOrpSubregions;

        var kralovehradeckyKrajSubregions =
            semilyOkresSubregions.Concat(trutnovOkresSubregions).Concat(jicinOkresSubregions).ToArray();
        
        var expectedLayers = new List<HierachicalRegionLayer> {
            new HierachicalRegionLayer(
                id: 0,
                regions: new List<HierarchicalRegion> {
                    new HierarchicalRegion(region: layers[0].Regions[jilemniceOrpId], subregions: jilemniceOrpSubregions),
                    new HierarchicalRegion(region: layers[0].Regions[turnovOrpId], subregions: turnovOrpSubregions),
                    new HierarchicalRegion(region: layers[0].Regions[semilyOrpId], subregions: semilyOrpSubregions),
                    new HierarchicalRegion(region: layers[0].Regions[vrchlabiOrpId], subregions: vrchlabiOrpSubregions),
                    new HierarchicalRegion(region: layers[0].Regions[novaPakaOrpId], subregions: novaPakaOrpSubregions)
                }
            ),
            new HierachicalRegionLayer(
                id: 1,
                regions: new List<HierarchicalRegion> {
                    new HierarchicalRegion(region: layers[1].Regions[0], subregions: semilyOkresSubregions),
                    new HierarchicalRegion(region: layers[1].Regions[1], subregions: trutnovOkresSubregions),
                    new HierarchicalRegion(region: layers[1].Regions[2], subregions: jicinOkresSubregions)
                }
            ),
            new HierachicalRegionLayer(
                id: 2,
                regions: new List<HierarchicalRegion> {
                    new HierarchicalRegion(region: layers[2].Regions[0], subregions: kralovehradeckyKrajSubregions)
                }
            )
        };

        return (layers, expectedLayers);
    }

    public static (IList<AdministrativeRegionLayer> InputLayers, IList<HierachicalRegionLayer> ExpectedLayers) Get4LayerRegionData() {
        const int jilemniceOrpId = 0;
        const int turnovOrpId = 1;
        const int semilyOrpId = 2;
        const int dvurKraloveOrpId = 3;
        const int trutnovOrpId = 4;
        const int vrchlabiOrpId = 5;
        const int horiceOrpId = 6;
        const int jicinOrpId = 7;
        const int novaPakaOrpId = 8;
        
        const int semilyOkresId = 9;
        const int trutnovOkresId = 10;
        const int jicinOkresId = 11;
        
        const int libereckyKrajId = 12;
        const int kraloveHradeckyKrajId = 13;
        
        const int ceskaRepublikaId = 14;

        var layers = new List<AdministrativeRegionLayer> {
            new AdministrativeRegionLayer(
                id: 0,
                regions: new List<AdministrativeRegion> {
                    new AdministrativeRegion(id: jilemniceOrpId, type: AdministrativeRegion.RegionType.Municipality, name: "Jilemnice", parentRegionId: semilyOkresId),
                    new AdministrativeRegion(id: turnovOrpId, type: AdministrativeRegion.RegionType.Municipality, name: "Turnov", parentRegionId: semilyOkresId),
                    new AdministrativeRegion(id: semilyOrpId, type: AdministrativeRegion.RegionType.Municipality, name: "Semily", parentRegionId: semilyOkresId),
                    new AdministrativeRegion(id: dvurKraloveOrpId, type: AdministrativeRegion.RegionType.Municipality, name: "Dvur Kralove nad Labem",
                        parentRegionId: trutnovOkresId),
                    new AdministrativeRegion(id: trutnovOrpId, type: AdministrativeRegion.RegionType.Municipality, name: "Trutnov", parentRegionId: trutnovOkresId),
                    new AdministrativeRegion(id: vrchlabiOrpId, type: AdministrativeRegion.RegionType.Municipality, name: "Vrchlabi", parentRegionId: trutnovOkresId),
                    new AdministrativeRegion(id: horiceOrpId, type: AdministrativeRegion.RegionType.Municipality, name: "Horice", parentRegionId: jicinOkresId),
                    new AdministrativeRegion(id: jicinOrpId, type: AdministrativeRegion.RegionType.Municipality, name: "Jicin", parentRegionId: jicinOkresId),
                    new AdministrativeRegion(id: novaPakaOrpId, type: AdministrativeRegion.RegionType.Municipality, name: "Nova Paka", parentRegionId: jicinOkresId)
                }
            ),
            new AdministrativeRegionLayer(
                id: 1,
                regions: new List<AdministrativeRegion> {
                    new AdministrativeRegion(id: semilyOkresId, type: AdministrativeRegion.RegionType.District, name: "Okres Semily", parentRegionId: libereckyKrajId),
                    new AdministrativeRegion(id: trutnovOkresId, type: AdministrativeRegion.RegionType.District, name: "Okres Trutnov", parentRegionId: kraloveHradeckyKrajId),
                    new AdministrativeRegion(id: jicinOkresId, type: AdministrativeRegion.RegionType.District, name: "Okres Jicin", parentRegionId: kraloveHradeckyKrajId)
                }
            ),
            new AdministrativeRegionLayer(
                id: 2,
                regions: new List<AdministrativeRegion> {
                    new AdministrativeRegion(id: libereckyKrajId, type: AdministrativeRegion.RegionType.County, name: "Liberecky kraj", parentRegionId: ceskaRepublikaId),
                    new AdministrativeRegion(id: kraloveHradeckyKrajId, type: AdministrativeRegion.RegionType.County, name: "Kralovehradecky kraj", parentRegionId: ceskaRepublikaId)
                }
            ),
            new AdministrativeRegionLayer(
                id: 3,
                regions: new List<AdministrativeRegion> {
                    new AdministrativeRegion(id: ceskaRepublikaId, type: AdministrativeRegion.RegionType.Country, name: "Ceska republika", parentRegionId: null)
                }
            )
        };
        
        var jilemniceOrpSubregions = new AdministrativeRegion[] { layers[0].Regions[jilemniceOrpId] };
        var turnovOrpSubregions = new AdministrativeRegion[] { layers[0].Regions[turnovOrpId] };
        var semilyOrpSubregions = new AdministrativeRegion[] { layers[0].Regions[semilyOrpId] };
        var dvurKraloveOrpSubregions = new AdministrativeRegion[] { layers[0].Regions[dvurKraloveOrpId] };
        var trutnovOrpSubregions = new AdministrativeRegion[] { layers[0].Regions[trutnovOrpId] };
        var vrchlabiOrpSubregions = new AdministrativeRegion[] { layers[0].Regions[vrchlabiOrpId] };
        var horiceOrpSubregions = new AdministrativeRegion[] { layers[0].Regions[horiceOrpId] };
        var jicinOrpSubregions = new AdministrativeRegion[] { layers[0].Regions[jicinOrpId] };
        var novaPakaOrpSubregions = new AdministrativeRegion[] { layers[0].Regions[novaPakaOrpId] };
        
        var semilyOkresSubregions = jilemniceOrpSubregions.Concat(turnovOrpSubregions).Concat(semilyOrpSubregions).ToArray();
        var trutnovOkresSubregions = dvurKraloveOrpSubregions.Concat(trutnovOrpSubregions).Concat(vrchlabiOrpSubregions).ToArray();
        var jicinOkresSubregions = horiceOrpSubregions.Concat(jicinOrpSubregions).Concat(novaPakaOrpSubregions).ToArray();

        var libereckyKrajSubregions = semilyOkresSubregions;
        var kralovehradeckyKrajSubregions = trutnovOkresSubregions.Concat(jicinOkresSubregions).ToArray();
        
        var ceskaRepublikaSubregions = libereckyKrajSubregions.Concat(kralovehradeckyKrajSubregions).ToArray();
        
        var expectedLayers = new List<HierachicalRegionLayer> {
            new HierachicalRegionLayer(
                id: 0,
                regions: new List<HierarchicalRegion> {
                    new HierarchicalRegion(region: layers[0].Regions[jilemniceOrpId], subregions: jilemniceOrpSubregions),
                    new HierarchicalRegion(region: layers[0].Regions[turnovOrpId], subregions: turnovOrpSubregions),
                    new HierarchicalRegion(region: layers[0].Regions[semilyOrpId], subregions: semilyOrpSubregions),
                    new HierarchicalRegion(region: layers[0].Regions[dvurKraloveOrpId], subregions: dvurKraloveOrpSubregions),
                    new HierarchicalRegion(region: layers[0].Regions[trutnovOrpId], subregions: trutnovOrpSubregions),
                    new HierarchicalRegion(region: layers[0].Regions[vrchlabiOrpId], subregions: vrchlabiOrpSubregions),
                    new HierarchicalRegion(region: layers[0].Regions[horiceOrpId], subregions: horiceOrpSubregions),
                    new HierarchicalRegion(region: layers[0].Regions[jicinOrpId], subregions: jicinOrpSubregions),
                    new HierarchicalRegion(region: layers[0].Regions[novaPakaOrpId], subregions: novaPakaOrpSubregions)
                }
            ),
            new HierachicalRegionLayer(
                id: 1,
                regions: new List<HierarchicalRegion> {
                    new HierarchicalRegion(region: layers[1].Regions[0], subregions: semilyOkresSubregions),
                    new HierarchicalRegion(region: layers[1].Regions[1], subregions: trutnovOkresSubregions),
                    new HierarchicalRegion(region: layers[1].Regions[2], subregions: jicinOkresSubregions)
                }
            ),
            new HierachicalRegionLayer(
                id: 2,
                regions: new List<HierarchicalRegion> {
                    new HierarchicalRegion(region: layers[2].Regions[0], subregions: libereckyKrajSubregions),
                    new HierarchicalRegion(region: layers[2].Regions[1], subregions: kralovehradeckyKrajSubregions)
                }
            ),
            new HierachicalRegionLayer(
                id: 3,
                regions: new List<HierarchicalRegion> {
                    new HierarchicalRegion(region: layers[3].Regions[0], subregions: ceskaRepublikaSubregions)
                }
            )
        };

        return (layers, expectedLayers);
    }
}