﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Map.ValueObjects;
using PostgreSqlStorage.Data;
using PostgreSqlStorage.MapGateway.Accessors;
using Xunit;

namespace PostgreSqlStorageTests.MapGateway.RegionTests;

[Collection(nameof(TestCollection))]
public sealed class RegionFetcherTests : IDisposable, IClassFixture<TestDatabaseFixture> {

    private readonly TestDatabaseFixture _fixture;

    private readonly GapContext _context;

    public RegionFetcherTests(TestDatabaseFixture fixture) {
        _fixture = fixture;
        _fixture.ResetDatabase();
        _context = _fixture.CreateContext();
    }

    public void Dispose() {
        _context.Dispose();
    }

    [Theory]
    [MemberData(nameof(FindRegionByIdData))]
    public void FindRegionByIdTest(RegionFetcherTestSource testSource) {

        new AdministrativeRegionLayerStorageSaver(_context).AddLayers(testSource.RegionLayers);
        
        _context.ChangeTracker.Clear();

        HierarchicalRegion foundRegion = new RegionFetcher(_context).GetHierarchicalRegionByIdAsync(testSource.ExpectedRegion.AdministrativeRegion.Id).Result;

        foundRegion.Should().BeEquivalentTo(
            testSource.ExpectedRegion,
             options => options
                 .ComparingByMembers<HierarchicalRegion>()
                 .ComparingByMembers<AdministrativeRegion>()
        );
    }

    public static IEnumerable<object[]> FindRegionByIdData() {
        yield return StructuredRegionData(0);
        yield return StructuredRegionData(1);
        yield return StructuredRegionData(2);
        yield return StructuredRegionData(3);
        yield return StructuredRegionData(4);
        yield return StructuredRegionData(5);
        yield return StructuredRegionData(6);
        yield return StructuredRegionData(7);
        yield return StructuredRegionData(8);
    }

    private static object[] StructuredRegionData(int expectedRegionId) {
        var layers = RegionData.Get3LayerRegionData();
        
        IList<HierarchicalRegion> allRegions = layers.ExpectedLayers.SelectMany(layer => layer.Regions).ToList();

        return new object[] {
            new RegionFetcherTestSource(
                regionLayers: layers.InputLayers,
                expectedRegion: allRegions[expectedRegionId],
                testName: $"FindRegion{expectedRegionId}-{allRegions[expectedRegionId].AdministrativeRegion.Name}"
            )
        };

    }
}
