﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Map.ValueObjects;
using TestUtils;

namespace PostgreSqlStorageTests.MapGateway.RegionTests; 

public class RegionFetcherTestSource : TestSource {

    public RegionFetcherTestSource(
        IList<AdministrativeRegionLayer> regionLayers,
        HierarchicalRegion expectedRegion,
        [CallerMemberName] string testName = "No test name provided.") : base(testName) {

        RegionLayers = regionLayers;
        ExpectedRegion = expectedRegion;
    }
    public IList<AdministrativeRegionLayer> RegionLayers { get; }

    public HierarchicalRegion ExpectedRegion { get; }
}
