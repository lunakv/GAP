﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using Map.ValueObjects;
using PostgreSqlStorage.Data;
using PostgreSqlStorage.MapGateway.Accessors;
using Xunit;

namespace PostgreSqlStorageTests.MapGateway.RegionLayerTests;

[Collection(nameof(TestCollection))]
public class GetLayerStructureTests : IDisposable, IClassFixture<TestDatabaseFixture> {
	private readonly TestDatabaseFixture _fixture;

	private readonly GapContext _context;

	public GetLayerStructureTests(TestDatabaseFixture fixture) {
		_fixture = fixture;
		_fixture.ResetDatabase();
		_context = _fixture.CreateContext();
	}

	public void Dispose() {
		_context.Dispose();
	}

	[Fact]
	public void AddLayersOnlyUnitLayerTest() {
		int jilemniceOrpId = 0;
		int turnovOrpId = 1;
		int semilyOrpId = 2;
		int novaPakaOrpId = 3;
		int semilyOkresId = 4;
		int jicinOkresId = 5;
		int kraloveHradeckyKrajId = 6;

		var layers = new List<AdministrativeRegionLayer> {
			new AdministrativeRegionLayer(
				id: 0,
				regions: new List<AdministrativeRegion> {
					new AdministrativeRegion(id: jilemniceOrpId, type: AdministrativeRegion.RegionType.Municipality, name: "Jilemnice", parentRegionId: semilyOkresId),
					new AdministrativeRegion(id: turnovOrpId, type: AdministrativeRegion.RegionType.Municipality, name: "Turnov", parentRegionId: semilyOkresId),
					new AdministrativeRegion(id: semilyOrpId, type: AdministrativeRegion.RegionType.Municipality, name: "Semily", parentRegionId: semilyOkresId),
					new AdministrativeRegion(id: novaPakaOrpId, type: AdministrativeRegion.RegionType.Municipality, name: "Nova Paka", parentRegionId: jicinOkresId)
				}
			),
			new AdministrativeRegionLayer(
				id: 1,
				regions: new List<AdministrativeRegion> {
					new AdministrativeRegion(id: semilyOkresId, type: AdministrativeRegion.RegionType.District, name: "Okres Semily", parentRegionId: kraloveHradeckyKrajId),
					new AdministrativeRegion(id: jicinOkresId, type: AdministrativeRegion.RegionType.District, name: "Okres Jicin", parentRegionId: kraloveHradeckyKrajId)
				}
			),
			new AdministrativeRegionLayer(
				id: 2,
				regions: new List<AdministrativeRegion> {
					new AdministrativeRegion(id: kraloveHradeckyKrajId, type: AdministrativeRegion.RegionType.County, name: "Kralovehradecky kraj", parentRegionId: null)
				}
			),
		};

		IList<int> expectedLayerStructure = new List<int> { 0, 1, 2 };

		new AdministrativeRegionLayerStorageSaver(_context).AddLayers(layers);

		_context.ChangeTracker.Clear();

		IList<int> fetchedLayerStructure = new RegionLayerFetcher(_context).GetLayerStructureAsync().Result;

		fetchedLayerStructure.Should().Equal(expectedLayerStructure);
	}
}