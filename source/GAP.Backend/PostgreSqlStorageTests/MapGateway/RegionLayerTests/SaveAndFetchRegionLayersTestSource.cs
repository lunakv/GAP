﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Map.ValueObjects;
using TestUtils;

namespace PostgreSqlStorageTests.MapGateway.RegionLayerTests;

public class SaveAndFetchRegionLayersTestSource : TestSource {

    public SaveAndFetchRegionLayersTestSource(
        IList<AdministrativeRegionLayer> regionLayers,
        HierachicalRegionLayer expectedLayer,
        [CallerMemberName] string testName = "No test name provided.") : base(testName) {

        RegionLayers = regionLayers;
        ExpectedLayer = expectedLayer;
    }

    public IList<AdministrativeRegionLayer> RegionLayers { get; }

    public HierachicalRegionLayer ExpectedLayer { get; }
}
