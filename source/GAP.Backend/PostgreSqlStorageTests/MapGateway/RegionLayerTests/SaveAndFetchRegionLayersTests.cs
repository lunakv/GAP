﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using Map.ValueObjects;
using PostgreSqlStorage.Data;
using PostgreSqlStorage.MapGateway.Accessors;
using Xunit;

namespace PostgreSqlStorageTests.MapGateway.RegionLayerTests;

[Collection(nameof(TestCollection))]
public class SaveAndFetchRegionLayersTests : IDisposable, IClassFixture<TestDatabaseFixture> {

    private readonly TestDatabaseFixture _fixture;

    private readonly GapContext _context;

    public SaveAndFetchRegionLayersTests(TestDatabaseFixture fixture) {
        _fixture = fixture;
        _fixture.ResetDatabase();
        _context = _fixture.CreateContext();
    }

    public void Dispose() {
        _context.Dispose();
    }

    [Theory]
    [MemberData(nameof(SaveAndFetchRegionLayersData))]
    public void SaveAndFetchRegionLayersTest(SaveAndFetchRegionLayersTestSource testSource) {

        new AdministrativeRegionLayerStorageSaver(_context).AddLayers(testSource.RegionLayers);
        
        _context.ChangeTracker.Clear();

        HierachicalRegionLayer fetchedLayer = new RegionLayerFetcher(_context).GetHierarchicalLayerAsync(testSource.ExpectedLayer.Id).Result;

        fetchedLayer.Should().BeEquivalentTo(
            testSource.ExpectedLayer,
            options => options
                .ComparingByMembers<HierachicalRegionLayer>()
                .ComparingByMembers<HierarchicalRegion>()
                .ComparingByMembers<AdministrativeRegion>()
        );
    }

    public static IEnumerable<object[]> SaveAndFetchRegionLayersData() {
        yield return Regions3Layers(0);
        yield return Regions3Layers(1);
        yield return Regions3Layers(2);
        yield return Regions4Layers(0);
        yield return Regions4Layers(1);
        yield return Regions4Layers(2);
        yield return Regions4Layers(3);
    }

    private static object[] Regions4Layers(int expectedLayer) {

        var layers = RegionData.Get4LayerRegionData();

        return new object[] {
            new SaveAndFetchRegionLayersTestSource(
                regionLayers: layers.InputLayers,
                expectedLayer: layers.ExpectedLayers[expectedLayer],
                testName: $"Regions4LayersTestLayer{expectedLayer}"
            )
        };
    }

    private static object[] Regions3Layers(int expectedLayer) {

        var layers = RegionData.Get3LayerRegionData();

        return new object[] {
            new SaveAndFetchRegionLayersTestSource(
                regionLayers: layers.InputLayers,
                expectedLayer: layers.ExpectedLayers[expectedLayer],
                testName: $"Regions3LayersTestLayer{expectedLayer}"
            )
        };
    }
}