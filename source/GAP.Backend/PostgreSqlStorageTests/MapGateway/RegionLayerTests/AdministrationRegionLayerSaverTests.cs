﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Map.ValueObjects;
using PostgreSqlStorage.Data;
using PostgreSqlStorage.MapGateway.Accessors;
using PostgreSqlStorage.MapGateway.Models;
using Xunit;

namespace PostgreSqlStorageTests.MapGateway.RegionLayerTests;

[Collection(nameof(TestCollection))]
public class AdministrationRegionLayerSaverTests : IDisposable, IClassFixture<TestDatabaseFixture> {

    private readonly TestDatabaseFixture _fixture;

    private readonly GapContext _context;

    public AdministrationRegionLayerSaverTests(TestDatabaseFixture fixture) {
        _fixture = fixture;
        _fixture.ResetDatabase();
        _context = _fixture.CreateContext();
    }

    public void Dispose() {
        _context.Dispose();
    }

    [Fact]
    public void AddLayersOnlyUnitLayerTest() {

        var unitRegionLayer = new AdministrativeRegionLayer(
            id: 0,
            regions: new List<AdministrativeRegion> {
                new AdministrativeRegion(id: 0, type: AdministrativeRegion.RegionType.Municipality, name: "Jicin", parentRegionId: null),
                new AdministrativeRegion(id: 1, type: AdministrativeRegion.RegionType.Municipality, name: "Jilemnice", parentRegionId: null),
                new AdministrativeRegion(id: 2, type: AdministrativeRegion.RegionType.Municipality, name: "Vrchlabi", parentRegionId: null),
                new AdministrativeRegion(id: 3, type: AdministrativeRegion.RegionType.Municipality, name: "Kladno", parentRegionId: null),
                new AdministrativeRegion(id: 4, type: AdministrativeRegion.RegionType.Municipality, name: "Jihlava", parentRegionId: null),
                new AdministrativeRegion(id: 5, type: AdministrativeRegion.RegionType.Municipality, name: "Teplice", parentRegionId: null),
                new AdministrativeRegion(id: 6, type: AdministrativeRegion.RegionType.Municipality, name: "Sokolov", parentRegionId: null)
            }
        );

        IList<UnitRegionMapping> expectedUnitRegionMappings =
            unitRegionLayer.Regions.Select(
                r => new UnitRegionMapping {
                    UnitRegionId = r.Id,
                    RegionId = r.Id,

                }
        ).ToList();

        IList<AdministrativeRegion> expectedRegions = unitRegionLayer.Regions;

        var regionLayers = new List<AdministrativeRegionLayer> { unitRegionLayer };

        new AdministrativeRegionLayerStorageSaver(_context).AddLayers(regionLayers);
        
        _context.ChangeTracker.Clear();

        IList<UnitRegionMapping> fetchedUnitRegionMappings = 
            _context.UnitRegionMappings.AsEnumerable().Select(
                urm => new UnitRegionMapping {
                    UnitRegionId = urm.UnitRegionId,
                    RegionId = urm.RegionId,
                }
        ).ToList();

        IList<AdministrativeRegion> fetchedRegions = _context.Regions.AsEnumerable().Select(
            r => new AdministrativeRegion(id: r.Id, type: r.Type, name: r.Name, parentRegionId: r.ParentRegionId)
        ).ToList();


        fetchedUnitRegionMappings.Should().BeEquivalentTo(
            expectedUnitRegionMappings,
            options => options.ComparingByMembers<UnitRegionMapping>()
        );

        fetchedRegions.Should().BeEquivalentTo(
            expectedRegions,
            options => options.ComparingByMembers<AdministrativeRegion>()
        );
    }

}
