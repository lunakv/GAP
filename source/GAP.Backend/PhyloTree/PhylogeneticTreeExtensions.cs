﻿using PhyloTree.DataClasses;
using PhyloTree.DataClasses.Interfaces;
using PhyloTree.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhyloTree;
public static class PhylogeneticTreeExtensions {
	public static T NodeForHaplogroup<T>(this PhylogeneticTree<T> tree, int haploId) where T : PhyloTreeNode, IWithHaplogroup {
		var node = tree.Nodes.Values.FirstOrDefault(n => n.HaplogroupId == haploId);
		if(node == null) {
			throw new ArgumentException($"PhyloTree of id {tree.TreeId} has no node in it corresponding to haplogroup {haploId}");
		}
		return node;
	}
	public static bool AreSameBranch<T>(this PhylogeneticTree<T> tree, int haploId1, int haploId2) where T : PhyloTreeNode, IWithHaplogroup {
		var node = NodeForHaplogroup(tree, haploId1);
		return AreSameBranch(tree, haploId2, node);
	}

	public static bool AreSameBranch<T>(this PhylogeneticTree<T> tree, int haploId, T node) where T : PhyloTreeNode, IWithHaplogroup {
		Stack<(T Node, bool CommingFromChildren)> stack = new Stack<(T, bool)>();
		HashSet<int> visited = new HashSet<int>();
		stack.Push((node, true));
		while(stack.Count > 0) {
			(T curr, bool fromChild) = stack.Pop();
			if(curr.HaplogroupId == haploId) {
				return true;
			}
			if(visited.Contains(curr.Id)) {
				continue;
			}
			visited.Add(curr.Id);
			//if it is haplogroup root, then we would escape the current branch when following. So we do not add the parent, only the children.
			if(curr.Parent.HasValue && !curr.IsHaplogroupRoot)
				stack.Push((tree.Nodes[curr.Parent.Value], true));
			if(!curr.IsHaplogroupRoot || fromChild) {
				foreach(var ch in curr.Children) {
					stack.Push((tree.Nodes[ch], false));
				}
			}
		}
		return false;
	}

	public static T HaplogroopBranchRoot<T>(this PhylogeneticTree<T> tree, T node) where T : PhyloTreeNode, IWithHaplogroup {
		var root = node;
		while(!root.IsHaplogroupRoot) {
			if(root.Parent == null) {
				throw new PhyloTreeNodeNotInBranchException(node.Id);
			}
			root = tree.Nodes[root.Parent.Value];
		}
		return root;
	}

	public static IList<T> FullSubtreeOfNode<T>(this PhylogeneticTree<T> tree, T node) where T : PhyloTreeNode, IWithHaplogroup {
		var root = node;

		var nodes = new List<T>();
		Queue<int> que = new Queue<int>();
		que.Enqueue(root.Id);
		while(que.Count > 0) {
			var curr = que.Dequeue();
			nodes.Add(tree.Nodes[curr]);
			foreach(var ch in tree.Nodes[curr].Children) {
				que.Enqueue(ch);
			}
		}
		return nodes;
	}

	public static IList<T> GetAllBranchRoots<T>(this PhylogeneticTree<T> tree) where T : PhyloTreeNode, IWithHaplogroup {
		List<T> result = new List<T>();
		foreach(var r in tree.Roots) {
			Queue<T> que = new Queue<T>();
			que.Enqueue(tree.Nodes[r]);
			while(que.Count > 0) {
				var curr = que.Dequeue();
				if(curr.IsHaplogroupRoot) {
					result.Add(curr);
				} else {
					foreach(var ch in curr.Children) {
						que.Enqueue(tree.Nodes[ch]);
					}
				}
			}
		}
		return result;
	}
}
