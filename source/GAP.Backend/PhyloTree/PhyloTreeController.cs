﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhyloTree.DataClasses;
using PhyloTree.Exceptions;
using Users.GenData.Haplogroup.Storage;
using Users.UserAccess;
using Users.UserAccess.Entities;

namespace PhyloTree;

/// <summary>
/// This class holds the instance of the pruned tree.
/// </summary>
public class PhyloTreeController {

	private PhylogeneticTree<PhyloQuantifiedNode>? _current = null;

	private object _savingLock = new();
	private bool _isSavingFullTree = false;

	/// <summary>
	/// Prunes the tree and populates it by haplogroups from <paramref name="haploFetcher"/> and
	/// users from <paramref name="userFetcher"/>. For tree prunning an access to the prior is need 
	/// provided by <paramref name="treeGetter"/>.
	/// <exception cref="UnsetTreeException">Thrown if the tree was pruned badly.</exception>
	public async Task RepruneAndQuantifyAsync(
		IPhyloTreeFetcher<PhyloVariantedNode> treeGetter,
		IHaplogroupFetcher haploFetcher,
		IHaplogroupSaver haploSaver,
		CompleteUserFetcher userFetcher,
		IBaseHaplogroupsProvider baseHaploProvider) {

		var users = await userFetcher.GetUsersAsync().ConfigureAwait(false);
		var pruner = new PhyloTreePruner<CompleteUser>(haploFetcher, users, baseHaploProvider, haploSaver);
		var preTree = await treeGetter.GetTreeAsync().ConfigureAwait(false);
		var tree = await pruner.Prune(preTree);
		if(tree == null) {
			throw new UnsetTreeException("Tree is null after pruning.");
		}
		_current = tree;
	}

	/// <summary>
	/// Saves new tree copy with <paramref name="fullTreeSaver"/>, then fetches it with
	/// <paramref name="treeGetter"/> and finally calls the 
	/// <see cref="RepruneAndQuantifyAsync(IPhyloTreeFetcher{PhyloVariantedNode}, IHaplogroupFetcher, CompleteUserFetcher)"/>
	/// method.
	/// Does not allow multiple downloading to take place at the same time,
	/// if another downloading is taking place, does nothing and returns <c>false</c>.
	/// </summary>
	public async Task<bool> SaveFullTreeAsync(FullTreeSaver fullTreeSaver, 
		IPhyloTreeFetcher<PhyloVariantedNode> treeGetter,
		IHaplogroupFetcher haploFetcher,
		IHaplogroupSaver haploSaver,
		CompleteUserFetcher userFetcher,
		IBaseHaplogroupsProvider baseHaploProvider) {
		lock(_savingLock) {
			if(_isSavingFullTree) {
				return false;
			}
			_isSavingFullTree = true;
		}
		try {
			await fullTreeSaver.SaveFullTree().ConfigureAwait(false);
			await RepruneAndQuantifyAsync(treeGetter, haploFetcher, haploSaver, userFetcher, baseHaploProvider).ConfigureAwait(false);
		} finally {
			lock(_savingLock) {
				_isSavingFullTree = false;
			}
		}
		return true;
	}
	
	/// <summary>
	/// Populates the instance of the tree in memory with user information and returns the resulting tree.
	/// </summary>
	/// <exception cref="UnsetTreeException">Thrown if the tree was not pruned yet.</exception>
	public async Task<PhylogeneticTree<PhyloUserNode>> GetTreeAsync(CompleteUserFetcher userFetcher, int sampleSize) {
		var us = (await userFetcher.GetUsersAsync()).ToDictionary(u => u.Id);
		var tree = _current ?? throw new UnsetTreeException();
		return tree.Cast<PhyloUserNode>(n => FromQuantified(n, us, sampleSize));
	}

	/// <summary>
	/// Populates the instance of the tree in memory with user information and returns the resulting tree.
	/// </summary>
	/// <exception cref="UnsetTreeException">Thrown if the tree was not pruned yet.</exception>
	public  PhylogeneticTree<PhyloHaplogroupedNode> GetTreeUserless() {
		var tree = _current ?? throw new UnsetTreeException();
		return tree.Cast<PhyloHaplogroupedNode>(
			n => new PhyloHaplogroupedNode(n, n.HaplogroupId, n.IsHaplogroupRoot));
	}

	/// <summary>
	/// Helper function that populates individual nodes.
	/// </summary>
	private PhyloUserNode FromQuantified(PhyloQuantifiedNode node, Dictionary<int, CompleteUser> users, int sampleSize) {
		return new PhyloUserNode(
			node.Id,
			node.Parent,
			node.Name,
			node.ApproximateYear, node.Children,
			node.PeopleCount,
			node.PeopleSample.Take(sampleSize)
				.Select(i => users[i])
				.Select(u => UserMasker.MaskUserWithoutSignedAgreement(u))
				.ToList(),
			node.IsHaplogroupRoot,
			node.HaplogroupId);
	}
}