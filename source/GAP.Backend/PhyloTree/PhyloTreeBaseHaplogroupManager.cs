﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhyloTree; 
public class PhyloTreeBaseHaplogroupManager : IBaseHaplogroupsProvider{
	private const string IgnoredFile = "ignored.json";
	private const string AddedFile = "added.json";
	private readonly ValueJsonSaver<string> _ignored;
	private readonly ValueJsonSaver<string> _added;

	public PhyloTreeBaseHaplogroupManager(IOptions<PhyloTreeConfig> config) {
		if(! Directory.Exists(config.Value.BaseHaplogroupsDirectory)) {
			Directory.CreateDirectory(config.Value.BaseHaplogroupsDirectory);
		}
		_ignored = new ValueJsonSaver<string>(
			Path.Combine(config.Value.BaseHaplogroupsDirectory, IgnoredFile), 
			config.Value.DefaultIgnoredNodes);
		_added = new ValueJsonSaver<string>(
			Path.Combine(config.Value.BaseHaplogroupsDirectory, AddedFile), 
			config.Value.DefaultAddedNodes);
	}

	public List<string> GetIgnored() => _ignored.GetValue();
	public void AddIgnored(string haploName) => _ignored.Add(haploName);
	public void RemoveIgnored(string haploName) => _ignored.Remove(haploName);

	public List<string> GetAdded() => _added.GetValue();
	public void AddAdded(string haploName) => _added.Add(haploName);
	public void RemoveAdded(string haploName) => _added.Remove(haploName);
}

public interface IBaseHaplogroupsProvider {
	List<string> GetAdded();
	List<string> GetIgnored();
}