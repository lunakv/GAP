﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhyloTree; 
internal class PhyloTreeFileGetter : IPhyloTreeDownloader {
	public Task<Stream> DownloadTreeAsync(string fileName) {
		var fo = new FileStreamOptions() {
			Mode = FileMode.Open,
			Options = FileOptions.Asynchronous
		};
		return new Task<Stream>(() => new FileStream(fileName, fo));
	}
}
