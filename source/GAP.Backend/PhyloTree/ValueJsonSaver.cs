﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace PhyloTree; 
public class ValueJsonSaver<T> {
	private readonly object _lock = new object();
	private readonly JsonSerializerOptions _jsonOptions;
	private readonly string _file;

	public ValueJsonSaver(string file, List<T> starting, bool indentedJson = true) {
		_file = file;
		_jsonOptions = new JsonSerializerOptions() {
			WriteIndented = indentedJson
		};
		if(!File.Exists(_file)) {
			lock(_lock) {
				Save(starting);
			}
		}
	}

	public void Add(T val) {
		lock (_lock) {
			var all = Get();
			all.Add(val);
			Save(all);
		}
	}

	public void Remove(T val) {
		lock (_lock) {
			var all = Get();
			all.Remove(val);
			Save(all);
		}
	}

	public List<T> GetValue() {
		lock (_lock) {
			return Get();
		}
	}

	private List<T> Get() {
		List<T>? val;
		using(var s = File.Open(_file, FileMode.Open, FileAccess.Read)) {
			val = JsonSerializer.Deserialize<List<T>>(s, _jsonOptions);
		}
		if(val == null) {
			throw new Exception($"The file {_file} couldn't be parsed as a valid {nameof(List<T>)} value.");
		}
		return val;
	}

	private void Save(List<T> val) {
		using(var s = File.Open(_file, FileMode.Create, FileAccess.Write)) {
			JsonSerializer.Serialize<List<T>>(s, val, _jsonOptions);
		}
	}
}
