﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Users.UserAccess;
using PhyloTree.DataClasses;
using System.Runtime.CompilerServices;
using System.Drawing;
using System.Runtime.ConstrainedExecution;
using Users.GenData.Haplogroup;
using Users.UserAccess.Entities.Interfaces;
using PhyloTree.DataClasses.Interfaces;
using PhyloTree.Exceptions;
using Users.GenData.Haplogroup.Storage;
using System.Formats.Asn1;

namespace PhyloTree;

public class PhyloTreePruner<TUser> : IPhyloTreePruner where TUser : IHasUserId, IHasGeneticData {
	private readonly bool _quiet;
	private readonly IHaplogroupFetcher _haplos;
	private readonly IList<TUser> _users;
	private readonly IBaseHaplogroupsProvider _baseHaploProvider;
	private readonly IHaplogroupSaver _haploSaver;

	public PhyloTreePruner(IHaplogroupFetcher haplos, IList<TUser> users, IBaseHaplogroupsProvider baseHaploProvider,
		IHaplogroupSaver haploSaver,
		bool quiet = true) {

		this._haplos = haplos;
		this._users = users;
		this._quiet = quiet;
		this._baseHaploProvider = baseHaploProvider;
		this._haploSaver = haploSaver;
	}
	
	/// <summary>
	/// Prunes the <paramref name="fullTree"/> and assign users into it,
	/// creating <see cref="PhylogeneticTree{PhyloQuantifiedNode}"/>.
	/// </summary>
	public Task<PhylogeneticTree<PhyloQuantifiedNode>> Prune<T>(PhylogeneticTree<T> fullTree) where T : PhyloTreeNode, IWithVariants
			=> PruneAndQuantify(fullTree, _users);

	/// <summary>
	/// Prunes the <paramref name="fullTree"/> and assign <paramref name="users"/> into it,
	/// creating <see cref="PhylogeneticTree{PhyloQuantifiedNode}"/>.
	/// </summary>
	public async Task<PhylogeneticTree<PhyloQuantifiedNode>> PruneAndQuantify<T>(
		PhylogeneticTree<T> fullTree,
		IList<TUser> users) where T : PhyloTreeNode, IWithVariants {
		return await PrunedQuantifiedTree(fullTree, users, await _haplos.GetHaplogroupsAsync());
	}

	/// <summary>
	/// Prunes the <paramref name="fullTree"/> and assign <paramref name="users"/> into it,
	/// creating <see cref="PhylogeneticTree{PhyloQuantifiedNode}"/>.
	/// </summary>
	/// <param name="preTree">The tree to be pruned.</param>
	/// <param name="users">The users based on which the tree will be pruned.</param>
	/// <param name="haplos">The haplogroups that will populate the resulting tree.</param>
	/// <param name="pruneVerticaly">When <c>true</c>, any node with exactly one parent and exactly one child will be removed.</param>
	/// <param name="prune">If pruning should take place. When <c>false</c> it will only get populated by users and haplogroups.</param>
	/// <returns></returns>
	private async Task<PhylogeneticTree<PhyloQuantifiedNode>> PrunedQuantifiedTree<T>(
		PhylogeneticTree<T> preTree,
		IList<TUser> users, IList<Haplogroup> haplos, 
		bool pruneVerticaly = false,
		bool prune = true) where T : PhyloTreeNode, IWithVariants{

		int superRoot = GetSuperRoot(preTree);
		List<int> roots = new List<int>() { superRoot };

		var haploRoots = GetHaplogroupRoots(preTree, superRoot);

		Dictionary<string, IList<TUser>> haploDistribution = UserHaploDistribution(users);
		
		PrintTree(preTree.Roots, i => preTree.Nodes[i].Children, "before first prune:");
		var nodeDict = PruneRecursively(preTree, haploDistribution, roots, haploRoots, pruneVerticaly, prune);
		PrintTree(roots, i => nodeDict[i].Children, "after first prune:");

		var tree = await CreateFinalTree(preTree, haplos, nodeDict, haploRoots, superRoot);
		PrintTree(tree.Roots, i => tree.Nodes[i].Children, "Final pruned tree:");
		return tree;
	}

	/// <returns>Dictionary from haplogroup names to users.</returns>
	private Dictionary<string, IList<TUser>> UserHaploDistribution(IList<TUser> users) {
		Dictionary<string, IList<TUser>> dict = new();
		foreach(var u in users) {
			var haplo = ProcessHaploName(u.GeneticData.Haplogroup.Name);
			dict.TryAdd(haplo, new List<TUser>());
			dict[haplo].Add(u);
		}
		return dict;
	}

	/// <summary>
	/// Ensures validity of haplogroups, when new weird characters appear in haplogroups, this is where to remove them.
	/// </summary>
	private string ProcessHaploName(string haploName) {
		return haploName.Replace("*", "");
	}

	private void PrintTree(IEnumerable<int> roots, Func<int, IEnumerable<int>> childFunc, string message = "") 
		=> PrintTree(roots, childFunc, i => i.ToString(), message);

	/// <summary>
	/// Does nothing if <c>_quiet</c> is <c>true</c>.
	/// Otherwise prints the message proceeded by indented representation of 
	/// the tree represented by <paramref name="roots"/> and <paramref name="childFunc"/>.
	/// </summary>
	private void PrintTree(IEnumerable<int> roots, Func<int, IEnumerable<int>> childFunc, Func<int, string> vocalizer, string message = "") {
		if(_quiet)
			return;
		if(message.Length > 0) {
			Console.WriteLine(message);
		}
		void Recurse(int node, int indent) {
			Console.Write($"{vocalizer(node)} ");
			var children = childFunc(node).ToList();
			if(children.Count > 0) {
				Console.Write("[");
				foreach(var ch in children) {
					Recurse(ch, indent + 1);
				}
				Console.Write("]");
			}
		}
		foreach(var r in roots) {
			Recurse(r, 0);
			Console.WriteLine();
		}
	}

	/// <summary>
	/// Some of the roots in the input may be roots of important subbranches.
	/// But those are in fact just important internal nodes.
	/// This function finds all the actual roots.
	/// </summary>
	private List<int> SuperRoots(IEnumerable<int> roots, Func<int, IEnumerable<int>> childrenFunc) {
		List<int> result = new List<int>(roots);
		Stack<int> stack = new Stack<int>();
		foreach(int r in roots) {
			stack.Push(r);
			while(stack.Count > 0) {
				var curr = stack.Pop();
				foreach(var ch in childrenFunc(curr)) {
					if(!result.Remove(ch))
						stack.Push(ch);
				}
			}
		}
		return result;
	}

	/// <summary>
	/// The typical tree is supposed to only have one super root (<see cref="SuperRoots"/>).
	/// This function retrieves this singular super root of the <paramref name="tree"/>.
	/// </summary>
	/// <exception cref="ArgumentException">Thrown if the tree has more than one super root.</exception>
	private int GetSuperRoot<T>(PhylogeneticTree<T> tree) where T : PhyloTreeNode {
		List<int> superRoots = SuperRoots(tree.Roots, i => tree.Nodes[i].Children);
		PrintTree(superRoots, i => new List<int>(), "Super roots:");
		if(superRoots.Count != 1) {
			throw new ArgumentException($"the provided tree had more than one super root, ids: {string.Join(", ", superRoots)}");
		}
		return superRoots[0];
	}
	
	private List<int> GetHaplogroupRoots<T>(PhylogeneticTree<T> tree, int root) where T : PhyloTreeNode {
		List<int> result = new List<int>();
		Queue<(int id, string preName)> que = new Queue<(int, string)>();
		que.Enqueue((root, tree.Nodes[root].Name));
		var fix = _baseHaploProvider.GetAdded();
		var ignored = _baseHaploProvider.GetIgnored();
		while(que.Count > 0) {
			(var curr, var preName) = que.Dequeue();
			var node = tree.Nodes[curr];
			bool isHaploRoot = fix.Contains(node.Name)
				|| ((node.Name.IndexOf("-") == 1 && node.Name[1] != preName[1]) 
					&& !ignored.Contains(node.Name));
			if(isHaploRoot) {
				result.Add(curr);
			} else {
				foreach(var ch in node.Children) {
					que.Enqueue((ch, node.Name));
				}
			}
		}
		return result;
	}

	/// <param name="preTree">The tree to be pruned</param>
	/// <param name="haploDistribution">List of users given their haplogroup name.</param>
	/// <param name="startingIndicies">What indices to start the recursion on.</param>
	/// <param name="pruneVerticaly">Whether the a node with only one child should be removed and replaced by the child.</param>
	/// <param name="prune">If the pruning should accur at all.</param>
	/// <returns></returns>
	private Dictionary<int, NodeRepr> PruneRecursively<T>(
		PhylogeneticTree<T> preTree,
		Dictionary<string, IList<TUser>> haploDistribution,
		IEnumerable<int> startingIndicies,
		List<int> haplogroupRoots,
		bool pruneVerticaly = false,
		bool prune = true) where T : PhyloTreeNode, IWithVariants {

		Dictionary<int, NodeRepr> nodeDict = new();
		int count = 0;
		
		//returns how many people are inside the subtree and the list of the nodes, that remained in the tree
		(int, List<int>) PruneRecurse(
			IEnumerable<int> indices, 
			bool areRoots, 
			bool isSuperTree) {

			if(count > preTree.Nodes.Count) {
				throw new PhyloTreeIsNotATreeException();
			}
			count++;

			int sum = 0;
			List<int> result = new();
			foreach(int i in indices) {
				var n = preTree.Nodes[i];

				IList<TUser>? us = null;
				string start = n.Name.Substring(0, n.Name.IndexOf('-') + 1);
				foreach(var v in n.Variants) {
					if(haploDistribution.TryGetValue($"{start}{v}", out us)) {
						break;
					}
				}
				us ??= new List<TUser>();
				List<int> usIds = us.Select(u => u.Id).ToList();
				var res = PruneRecurse(n.Children, false, isSuperTree && (areRoots || !haplogroupRoots.Contains(i)));
				int sub = res.Item1 + us.Count;
				if(!prune || isSuperTree || sub > 0) {
					if(pruneVerticaly && us.Count == 0 && res.Item2.Count == 1) {
						result.Add(res.Item2[0]);
					} else {
						result.Add(i);
						nodeDict.Add(n.Id, new NodeRepr(new NumberOfPeople(us.Count, res.Item1), usIds, res.Item2));
					}
				}
				sum += sub;
			}
			return (sum, result);
		}
		PruneRecurse(startingIndicies, true, true);

		return nodeDict;
	}

	/// <summary>
	/// Given the input <paramref name="preTree"/> and node representations in <paramref name="nodeDict"/>
	/// this function populates the resulting tree by <paramref name="haplos"/>.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="preTree"></param>
	/// <param name="haplos"></param>
	/// <param name="nodeDict"></param>
	/// <param name="superRoot"></param>
	/// <returns></returns>
	private async Task<PhylogeneticTree<PhyloQuantifiedNode>> CreateFinalTree<T>(
		PhylogeneticTree<T> preTree,
		IList<Haplogroup> haplos,
		Dictionary<int, NodeRepr> nodeDict,
		List<int> haplogroupRoots,
		int superRoot) where T : PhyloTreeNode, IWithVariants {
		PhylogeneticTree<PhyloQuantifiedNode>.TreeBuilder builder = new();

		Dictionary<string, int> haploIds = haplos.ToDictionary(h => h.Name, elementSelector: h => h.Id);
		
		async Task CreateTreeRecurse(int currNode, int? parent, bool isRoot, bool hasSeenHaplogroupRoot) {
			var pre = preTree.Nodes[currNode];
			var n2 = new PhyloTreeNode(currNode, 
				parent,
				pre.Name,
				pre.ApproximateYear,
				nodeDict[currNode].Children);
			bool isHaploRoot = (!hasSeenHaplogroupRoot) && haplogroupRoots.Contains(currNode);
			if(!haploIds.TryGetValue(n2.Name, out int hapId)) {
				if(isHaploRoot) {
					await _haploSaver.AddHaplogroupAsync(n2.Name);
					var hs = await _haplos.GetHaplogroupsAsync();
					var h = hs.FirstOrDefault(h => h.Name == pre.Name);
					hapId = h?.Id ?? -1;
				} else {
					hapId = -1;
				}
			}
			builder.Add(new PhyloQuantifiedNode(n2,
				hapId,
				nodeDict[currNode].Counts,
				nodeDict[currNode].UserIds,
				isHaploRoot), isRoot);
			foreach(var ch in nodeDict[currNode].Children) {
				await CreateTreeRecurse(ch, currNode, false, 
					//hasSeenHaplogroupRoot || isHaploRoot
					false
					);
			}
		}

		await CreateTreeRecurse(superRoot, null, true, false);
		if(preTree.TreeId.HasValue)
			builder.SetTreeID(preTree.TreeId.Value);
		if(preTree.CreationTime.HasValue)
			builder.SetDate(preTree.CreationTime.Value);
		return builder.GetTree();
	
	}

	private record NodeRepr(NumberOfPeople Counts, List<int> UserIds, List<int> Children);
}
