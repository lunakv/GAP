﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhyloTree.DataClasses;
using PhyloTree.DataClasses.Interfaces;
using Users.UserAccess;

namespace PhyloTree
{
    public interface IPhyloTreePruner {
		Task<PhylogeneticTree<PhyloQuantifiedNode>>? Prune<T>(PhylogeneticTree<T> preTree) where T: PhyloTreeNode, IWithVariants;
	}
}
