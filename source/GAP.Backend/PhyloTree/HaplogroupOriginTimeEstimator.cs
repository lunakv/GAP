﻿using LZStringCSharp;
using System.Text.Json;

namespace PhyloTree; 

public class HaplogroupOriginTimeEstimator : IHaplogroupOriginTimeEstimator {
	private readonly Dictionary<string, int> _yearsApprox;
	private static string valueName = "value";
	private static string nodesName = "nodes";
	private static string dateName = "date";
	private static string tmrcaName = "tmrca"; // the most recent common ancestor
	private HaplogroupOriginTimeEstimator() {
		_yearsApprox = new Dictionary<string, int>();
	}

	/// <summary>
	/// Retrieves information about the time estimation of individual nodes within
	/// the <see cref="DataClasses.PhylogeneticTree{TNode}"/>.
	/// Downloads a file containing this info, so <b>web service</b> is required.
	/// </summary>
	public static async Task<HaplogroupOriginTimeEstimator> Create(bool quiet = true) {
		if(!quiet)
			Console.WriteLine("haplogroup time prediction download started.");
		HaplogroupOriginTimeEstimator result = new HaplogroupOriginTimeEstimator();
		string url = "http://scaledinnovation.com/gg/mapping/yTrackerData.pack.js";
		HttpClient client = new HttpClient();
		var response = await client.GetStreamAsync(url).ConfigureAwait(false);
		var parsed = await JsonDocument.ParseAsync(response);
		var value = parsed.RootElement.GetProperty(valueName);
		var str = LZString.DecompressFromBase64(value.GetString());
		parsed = await JsonDocument.ParseAsync(str.ToStream());
		var nodes = parsed.RootElement.GetProperty(nodesName);
		foreach(var node in nodes.EnumerateObject()) {
			if(!node.Value.TryGetProperty(dateName, out var dp) || !dp.TryGetProperty(tmrcaName, out var tmrca)) {
				Console.WriteLine($"Node {node.Name} has either no {dateName} or no {dateName}.{tmrcaName} property.");
				continue;
			}
			var en = tmrca.EnumerateArray();
			en.MoveNext(); en.MoveNext();
			if(en.Current.ValueKind == JsonValueKind.Number) {
				result._yearsApprox.Add(node.Name, en.Current.GetInt32());
			} else {
				Console.WriteLine($"Node {node.Name} has year {en.Current} which is not a number.");
			}
		}
		if(!quiet)
			Console.WriteLine("haplogroup time prediction download DONE.");
		return result;
	}

	/// <summary>
	/// Finds the time estimate for <see cref="DataClasses.PhyloTreeNode"/> 
	/// with a given <paramref name="haploName"/>.
	/// </summary>
	public int? GetTime(string haploName) {
		return _yearsApprox.TryGetValue(haploName, out int val) ? val : null;
	}
}
