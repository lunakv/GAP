﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhyloTree.DataClasses;

namespace PhyloTree
{
    public interface IPhyloTreeWebParser {
		PhylogeneticTree<PhyloVariantedNode> GetPhyloTree(Stream sourceStream, IHaplogroupOriginTimeEstimator timeEstimator);
	}
}
