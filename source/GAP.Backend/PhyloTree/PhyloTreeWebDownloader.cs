﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhyloTree; 

public class PhyloTreeWebDownloader : IPhyloTreeDownloader {
	/// <summary>
	/// Downlods the <see cref="DataClasses.PhylogeneticTree{TNode}"/> from a web <paramref name="url"/>.
	/// The downloading is not lazy, the whole stream is stored in the memory.
	/// </summary>
	public async Task<Stream> DownloadTreeAsync(string url) {
		var client = new HttpClient();
		var response = await client.GetStreamAsync(url);
		if(response != null) {
			var all = new StreamReader(response).ReadToEnd();
			return new MemoryStream(Encoding.ASCII.GetBytes(all));
		} else
			throw new ArgumentException($"Given url is not valid to download from it: \n{url}");
	}
}
