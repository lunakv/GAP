﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhyloTree; 
internal static class StringExtensions {
	public static Stream ToStream(this string s) {
		var memStream = new MemoryStream();
		var sw = new StreamWriter(memStream);
		sw.Write(s);
		sw.Flush();
		memStream.Seek(0, SeekOrigin.Begin);
		return memStream;
	}
}
