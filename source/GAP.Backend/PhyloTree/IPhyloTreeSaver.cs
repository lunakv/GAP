﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhyloTree.DataClasses;

namespace PhyloTree;

/// <summary>
/// Interface that ensures ability of saving a <see cref="PhylogeneticTree{TNode}"/>.
/// </summary>
/// <typeparam name="TNode">Type of the node.</typeparam>
public interface IPhyloTreeSaver<TNode> where TNode : PhyloTreeNode {
	void SaveTree(PhylogeneticTree<TNode> tree);
}
