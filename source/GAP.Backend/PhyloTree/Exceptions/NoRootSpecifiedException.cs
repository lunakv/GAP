﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhyloTree.Exceptions; 

/// <summary>
/// An exception indicating, that <see cref="DataClasses.PhylogeneticTree{TNode}"/> 
/// was created but it has no root. Can also mean that the tree has 0 nodes in it.
/// </summary>
public class NoRootSpecifiedException : FormatException
{
    internal NoRootSpecifiedException(int nodesCount)
        : base($"None out of {nodesCount} nodes in the tree is a root.") { }
}
