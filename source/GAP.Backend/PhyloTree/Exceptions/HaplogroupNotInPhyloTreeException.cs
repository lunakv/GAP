﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users.Exceptions;

namespace PhyloTree.Exceptions {
	public class HaplogroupNotInPhyloTreeException : UserException{
		public HaplogroupNotInPhyloTreeException(string message)
			:base(message){ }

		public override string GetRepresentation => nameof(HaplogroupNotInPhyloTreeException);
	}
}
