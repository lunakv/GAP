﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhyloTree.Exceptions; 

/// <summary>
/// Exception thrown when a backwards edge exists in the phylo tree.
/// </summary>
public class PhyloTreeIsNotATreeException : Exception {

}
