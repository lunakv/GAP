﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhyloTree.Exceptions {
	public class PhyloTreeNodeNotInBranchException : ArgumentException {
		internal PhyloTreeNodeNotInBranchException(int nodeId) 
			: base($"Trying to find the haplogroup branch root for node of id: ({nodeId}), which is outside of branches.")
			{ }
	}
}
