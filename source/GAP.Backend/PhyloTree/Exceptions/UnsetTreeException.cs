﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhyloTree.Exceptions; 

public class UnsetTreeException : Exception {
	internal UnsetTreeException()
		: base("Tree was not loaded nor pruned in controller, but current was accesed.") { }
	internal UnsetTreeException(string message)
		: base(message) { }
}
