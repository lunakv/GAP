﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhyloTree.Exceptions;

public class PhyloTreeFormatException : FormatException
{
    internal PhyloTreeFormatException(string message)
        : base(message) { }
}
