﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;

using PhylogeneticTree = PhyloTree.DataClasses.PhylogeneticTree<PhyloTree.DataClasses.PhyloVariantedNode>;
using PhyloTree.DataClasses;
using Users.UserAccess;
using System.Text.Json;
using System.Collections.Specialized;

namespace PhyloTree;

/// <summary>
/// Class used to parse the whole tree from the FtDNA format.
/// </summary>
public class FtDnaTreeParser : IPhyloTreeWebParser {
	private PhylogeneticTree.TreeBuilder _builder = new PhylogeneticTree.TreeBuilder();
	private const string _dateFormat = "HH:mm:ss:fff";
	private readonly Names _names = new Names();

	/// <summary>
	/// Parses the <paramref name="sourceStream"/> into individual <see cref="DataClasses.PhyloVariantedNode"/>
	/// which form the whole <see cref="PhylogeneticTree"/>. The time estimates are predicted by the given
	/// <paramref name="timeEstimator"/>.
	/// </summary>
	public PhylogeneticTree GetPhyloTree(Stream sourceStream, IHaplogroupOriginTimeEstimator timeEstimator)
		=> GetPhylogeneticTree(sourceStream, timeEstimator, false);
	
	/// <summary>
	/// Parses the <paramref name="sourceStream"/> into individual <see cref="DataClasses.PhyloVariantedNode"/>
	/// which form the whole <see cref="PhylogeneticTree"/>. The time estimates are predicted by the given
	/// <paramref name="timeEstimator"/>.
	/// </summary>
	/// <param name="quiet">When false, a report about the tree parsing is printed to the <see cref="System.Console"/></param>.
	public PhylogeneticTree GetPhylogeneticTree(Stream sourceStream, IHaplogroupOriginTimeEstimator estimator, bool quiet) {
		_builder = new PhylogeneticTree.TreeBuilder();
		using var jd = JsonDocument.Parse(sourceStream);
		var root = jd.RootElement;
		ReadAllNodes(root, estimator, quiet);
		SetTreeId(root);
		SetCreationDate(root);
		return _builder.GetTree();
	}

	private void ReadAllNodes(JsonElement json, IHaplogroupOriginTimeEstimator estimator, bool quiet = true) {
		if (!json.TryGetProperty(_names.allNodes, out var nodes)) {
			throw new JsonException("All nodes property not found during parsing json phylo tree from web.");
		}
		if(!quiet)
			Console.WriteLine($"{DateTime.Now.ToString(_dateFormat)} - parsing nodes:");
		int count = 0;
		foreach(var n in nodes.EnumerateObject()) {
			ReadOneNode(n, estimator);
			count++;
		}
		if(!quiet)
			Console.WriteLine($"{DateTime.Now.ToString(_dateFormat)} - all {count} nodes parsed");
	}

	private void ReadOneNode(JsonProperty elem, IHaplogroupOriginTimeEstimator estimator) {
		if(!int.TryParse(elem.Name, out int id)) {
			throw new JsonException($"Node id {elem.Name} is not a valid integer.");
		}
		if(!elem.Value.TryGetProperty(_names.name, out var nameProp)) {
			throw new JsonException($"Node {elem.Name} has no name property.");
		}
		string? name = nameProp.GetString();
		if(name == null) {
			throw new JsonException($"Node {elem.Name} name property is not interpretable as a string.");
		}
		if(!elem.Value.TryGetProperty(_names.isRoot, out var rootProp)) {
			throw new JsonException($"Node {elem.Name} has no root property.");
		}
		bool root = rootProp.GetBoolean();
		int parentId = -1;
		if(!root && 
			(!elem.Value.TryGetProperty(_names.parentId, out var parentProp) || 
			!parentProp.TryGetInt32(out parentId))) {

			throw new JsonException($"Node {elem.Name} has no parentId property.");
		};
		List<int> children = new List<int>();
		if(elem.Value.TryGetProperty(_names.children, out var childrenProp)) {
			foreach(var ch in childrenProp.EnumerateArray()) {
				if (!ch.TryGetInt32(out var val)) {
					throw new JsonException($"In node {elem.Name} one of the children is not a valid integer.");
				}
				children.Add(val);
			}
		}
		List<string> variants = new List<string>();
		if(elem.Value.TryGetProperty(_names.variants, out var variantsProp)) {
			foreach(var v in variantsProp.EnumerateArray()) {
				string? val = v.GetProperty(_names.variantName).GetString();

				if (val == null) {
					throw new JsonException($"In node {elem.Name} one of the variants cannot be interpreted as a string.");
				}
				variants.Add(val);
			}
		}
		var year = estimator.GetTime(name);
		_builder.Add(new PhyloVariantedNode(id, root ? null : parentId, name, year ?? -1, children, variants.AsReadOnly()), root);
	}

	private void SetTreeId(JsonElement json) {
		if(json.TryGetProperty(_names.id, out var idProp) && idProp.TryGetInt32(out int treeId)) {
			_builder.SetTreeID(treeId);
		}
	}

	private void SetCreationDate(JsonElement json) {
		if(json.TryGetProperty(_names.publishedDate, out var dateProp)) {
			var date = dateProp.GetString();
			if(date != null) {
				_builder.SetDate(DateTime.Parse(date));
			}
		}
	}

	/// <summary>
	/// Stores the exact string constants by which the tree can be parsed from the input Json. 
	/// </summary>
	private sealed record Names(string allNodes, string name, string isRoot, string parentId, string children, string id, string publishedDate, string variants, string variantName) {
		public Names()
			:this(nameof(allNodes),
				 nameof(name),
				 nameof(isRoot),
				 nameof(parentId),
				 nameof(children),
				 nameof(id),
				 nameof(publishedDate),
				 nameof(variants),
				 "variant") { }
	}
}
