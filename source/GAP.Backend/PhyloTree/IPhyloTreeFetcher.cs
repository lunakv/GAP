﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhyloTree.DataClasses;

namespace PhyloTree
{
    public interface IPhyloTreeFetcher<TNode> where TNode : PhyloTreeNode {
		Task<PhylogeneticTree<TNode>> GetTreeAsync();
	}
}
