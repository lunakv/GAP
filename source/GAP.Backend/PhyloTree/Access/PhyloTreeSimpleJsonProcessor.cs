﻿using Newtonsoft.Json;
using PhyloTree.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using System.Text.Json;
using System.Xml.Linq;

namespace PhyloTree.Access;

public sealed class PhyloTreeOriginalJsonProcessor : IPhyloTreeSaver<PhyloVariantedNode>, IPhyloTreeFetcher<PhyloVariantedNode>
{
	private PhyloTreeJsonProcessor<PhyloVariantedNode> _jsonProcessor;
	public PhyloTreeOriginalJsonProcessor(IOptions<PhyloTreeConfig> config)
		: this(config.Value.FullTreePath) {}

	public PhyloTreeOriginalJsonProcessor(string treeFile){
		_jsonProcessor = new PhyloTreeJsonProcessor<PhyloVariantedNode>(treeFile, SimpleNodeJsonExtender.Singleton);
	}

	public void SaveTree(PhylogeneticTree<PhyloVariantedNode> tree) {
		((IPhyloTreeSaver<PhyloVariantedNode>)_jsonProcessor).SaveTree(tree);
	}

	public Task<PhylogeneticTree<PhyloVariantedNode>> GetTreeAsync() {
		return ((IPhyloTreeFetcher<PhyloVariantedNode>)_jsonProcessor).GetTreeAsync();
	}

	private class SimpleNodeJsonExtender : INodeJsonExtender<PhyloVariantedNode> {
		private const string variantsName = "variants";
		private SimpleNodeJsonExtender() { }

		public static SimpleNodeJsonExtender Singleton => new();

		public PhyloVariantedNode ReadSpecialFields(PhyloTreeNode priorNode,
			JsonElement reader) {

			List<string> variants = new List<string>();
			foreach(var ch in reader.GetProperty(variantsName).EnumerateArray()) {
				var val = ch.GetString();
				if(val == null) {
					throw new PhyloTree.Exceptions.PhyloTreeFormatException("Variants within the tree cannot be read");
				}
				variants.Add(val);
			}

			return new PhyloVariantedNode(priorNode, variants);
		}

		public void WriteSpecialFields(Utf8JsonWriter writer, PhyloVariantedNode node) {
			writer.WritePropertyName(variantsName);
			writer.WriteStartArray();
			foreach(var v in node.Variants) {
				writer.WriteStringValue(v);
			}
			writer.WriteEndArray();
		}
	}

}
