﻿using PhyloTree.DataClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.Extensions.Options;
using System.Text.Json;
using PhyloTree.Exceptions;
using Newtonsoft.Json;
using Users.UserAccess;

namespace PhyloTree.Access;


public sealed class PhyloTreeQuantifiedJsonProcesor : IPhyloTreeSaver<PhyloQuantifiedNode>, IPhyloTreeFetcher<PhyloQuantifiedNode>
{
	private PhyloTreeJsonProcessor<PhyloQuantifiedNode> _jsonProcessor;
	public PhyloTreeQuantifiedJsonProcesor(IOptions<PhyloTreeConfig> config)
		: this(config.Value.QuantifiedTreePath) {
	}

	public PhyloTreeQuantifiedJsonProcesor(string treeFilePath){
		_jsonProcessor = new PhyloTreeJsonProcessor<PhyloQuantifiedNode>(treeFilePath, new QuantifiedNodeJsonExtender());
	}
	
	public void SaveTree(PhylogeneticTree<PhyloQuantifiedNode> tree) {
		_jsonProcessor.SaveTree(tree);
	}

	public Task<PhylogeneticTree<PhyloQuantifiedNode>> GetTreeAsync() {
		return _jsonProcessor.GetTreeAsync();
	}

	private sealed class QuantifiedNodeJsonExtender : INodeJsonExtender<PhyloQuantifiedNode> {

		private static readonly Names _names = new Names();

		public PhyloQuantifiedNode ReadSpecialFields(PhyloTreeNode priorNode, JsonElement je) {
			int subtree, inside, haploId;
			bool isHaplogroupRoot;
			if(!je.TryGetProperty(_names.PeopleCount, out var pc)) {
				throw new PhyloTreeFormatException($"Quantified node '{priorNode.Id}' does not have people count property."); 
			}
			if(!pc.TryGetProperty(_names.InSubstree, out var subtreeProp)) {
				throw new PhyloTreeFormatException($"Quantified node '{priorNode.Id}' people count does not have InSubtree property.");
			}
			if(!subtreeProp.TryGetInt32(out subtree)) {
				throw new PhyloTreeFormatException($"Quantified node '{priorNode.Id}' people count InSubtree property is not a valid integer.");
			}
			if(!pc.TryGetProperty(_names.Inside, out var insideProp)) {
				throw new PhyloTreeFormatException($"Quantified node '{priorNode.Id}' people count does not have Inside property.");
			}
			if(!insideProp.TryGetInt32(out inside)) {
				throw new PhyloTreeFormatException($"Quantified node '{priorNode.Id}' people count Inside property is not a valid integer.");
			}

			if(!(je.TryGetProperty(_names.HaplogroupId, out var haplProp) && haplProp.TryGetInt32(out haploId)))
				throw new PhyloTreeFormatException($"Quantified node '{priorNode.Id}' has a problem with HaplogroupId property.");
			
			if(!je.TryGetProperty(_names.IsHaplogroupRoot, out var haplRootProp))
				throw new PhyloTreeFormatException($"Quantified node '{priorNode.Id}' has no IsHaplogroupRoot property.");
			isHaplogroupRoot = haplRootProp.GetBoolean();

			if(!je.TryGetProperty(_names.PeopleSample, out var sampleProp))
				throw new PhyloTreeFormatException($"Quantified node '{priorNode.Id}' has no PeopleSample property.");
			
			List<int> sample = new List<int>();

			foreach(var s in sampleProp.EnumerateArray()) {
				//in sample there only are users with set genetic data, so we can do construct CompleteUser
				sample.Add(s.GetInt32());
			}
			return new PhyloQuantifiedNode(priorNode, haploId, new NumberOfPeople(inside, subtree), sample, isHaplogroupRoot);
		}

		public void WriteSpecialFields(Utf8JsonWriter jtw, PhyloQuantifiedNode node) {
			jtw.WritePropertyName(_names.PeopleCount);
			jtw.WriteStartObject();
			jtw.WriteNumber(_names.InSubstree, node.PeopleCount.InSubtree);
			jtw.WriteNumber(_names.Inside, node.PeopleCount.Inside);
			jtw.WriteEndObject();
			jtw.WriteNumber(_names.HaplogroupId, node.HaplogroupId);
			jtw.WriteBoolean(_names.IsHaplogroupRoot, node.IsHaplogroupRoot);
			jtw.WritePropertyName(_names.PeopleSample);
			jtw.WriteStartArray();
			foreach(var u in node.PeopleSample)
			{
				jtw.WriteNumberValue(u);
			}
			jtw.WriteEndArray();
		}

		private record Names(string PeopleCount, string InSubstree, string Inside, string HaplogroupId, string IsHaplogroupRoot, string PeopleSample) {
			public Names()
				:this(nameof(PeopleCount),
					 nameof(InSubstree),
					 nameof(Inside),
					 nameof(HaplogroupId),
					 nameof(IsHaplogroupRoot),
					 nameof(PeopleSample)) { }
		}
	}
}

