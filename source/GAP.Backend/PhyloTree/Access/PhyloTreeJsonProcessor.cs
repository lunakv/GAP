﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PhyloTree.DataClasses;
using PhyloTree.Exceptions;
using System.Text.Json;

namespace PhyloTree.Access;

/// <summary>
/// Class that can process serialization and deserialization of <see cref="PhylogeneticTree{TNode}"/>
/// with the corresponding <typeparamref name="TNode"/> as type parameter.
/// </summary>
internal class PhyloTreeJsonProcessor<TNode> : IPhyloTreeSaver<TNode>, IPhyloTreeFetcher<TNode>
	where TNode : PhyloTreeNode {
	private string _file;
	private readonly Names _names = new Names();
	private INodeJsonExtender<TNode> _nodeJsonExtender;

	/// <param name="file">File where the tree is going to be stored in and parsed from.</param>
	/// <param name="nodeJsonExtender">
	/// This is going to store the variables that differentiate 
	/// <see cref="DataClasses.PhyloTreeNode"/> from <typeparamref name="TNode"/>.
	/// </param>
	public PhyloTreeJsonProcessor(string file, INodeJsonExtender<TNode> nodeJsonExtender) {
		this._file = file;
		this._nodeJsonExtender = nodeJsonExtender;
	}
	
	public async Task<PhylogeneticTree<TNode>> GetTreeAsync() {

		List<int> roots = new List<int>();
		List<TNode> nodes = new();
		int? treeId;
		DateTime? creationTime;
		using var str = File.OpenRead(_file);
		using JsonDocument jd = await JsonDocument.ParseAsync(str);
		foreach(var r in jd.RootElement.GetProperty(_names.Roots).EnumerateArray()) {
			roots.Add(r.GetInt32());
		}
			
		foreach(var n in jd.RootElement.GetProperty(_names.Nodes).EnumerateArray()) {
			nodes.Add(LoadNode(n));
		}

		treeId = GetIntNullable(_names.TreeId, jd.RootElement, -1);
		var timeStr = jd.RootElement.GetProperty(_names.CreationTime).GetString();
		creationTime = DateTime.TryParse(timeStr, out var t) ? t : null;

		var nodesDict = nodes.ToDictionary(n => n.Id);
		PhylogeneticTree<TNode>.TreeBuilder builder = new();
		Queue<(int id, bool root)> que = new Queue<(int, bool)>(roots.Select(r => (r, true)));
		while(que.Any()) {
			int curr;
			bool isRoot;
			(curr, isRoot) = que.Dequeue();
			//the following if seems unnecessary, but the tree from
			//the website has such anomaly in it and so it is a must
			if(isRoot || !roots.Contains(curr)) {
				builder.Add(nodesDict[curr], isRoot);
				foreach(var ch in nodesDict[curr].Children) {
					que.Enqueue((ch, false));
				}
			}
		}
		if(creationTime.HasValue)
			builder.SetDate(creationTime.Value);
		if(treeId.HasValue)
			builder.SetTreeID(treeId.Value);
		return builder.GetTree();
	}
	
	public void SaveTree(PhylogeneticTree<TNode> tree) {
		using FileStream fs = File.Create(_file);
		Utf8JsonWriter jw = new Utf8JsonWriter(fs);
		jw.WriteStartObject();
		jw.WritePropertyName(_names.Roots);
		jw.WriteStartArray();
		foreach(var r in tree.Roots) {
			jw.WriteNumberValue(r);
		}
		jw.WriteEndArray();
		jw.WritePropertyName(_names.Nodes);
		jw.WriteStartArray();
		foreach(var n in tree.Nodes) {
			WriteNode(n.Value, jw);
		}
		jw.WriteEndArray();
		jw.WritePropertyName(_names.TreeId);
		if(tree.TreeId.HasValue)
			jw.WriteNumberValue(tree.TreeId.Value);
		else
			jw.WriteNullValue();
		jw.WriteString(_names.CreationTime, tree.CreationTime.ToString());
		jw.WriteEndObject();
		jw.Flush();
	}

	/// <summary>
	/// Gets an int from the json file.
	/// </summary>
	private int GetInt(string name, JsonElement jr, int? idToDisplay) {
		if(!jr.TryGetProperty(name, out var prop) || !prop.TryGetInt32(out int result)) {
			var dips = idToDisplay.HasValue ? idToDisplay.Value.ToString() : prop.GetString();
			throw new PhyloTreeFormatException($"Node '{dips}' {name} is not a valid integer.");
		}
		return result;
	}
	
	/// <summary>
	/// Gets a nullable int from the json file.
	/// </summary>
	private int? GetIntNullable(string name, JsonElement jr, int id) {
		if(jr.TryGetProperty(name, out var prop)) {
			if(prop.ValueKind == JsonValueKind.Null)
				return null;
			if(prop.TryGetInt32(out int resInt))
				return resInt;
		}
		throw new PhyloTreeFormatException($"Node '{id}' {name} is not a valid integer.");
	}

	/// <summary>
	/// Retrieves a node from the json file.
	/// </summary>
	private TNode LoadNode(JsonElement jr) {
		int id = GetInt(_names.Id, jr, null);
		string name;
		if(!(jr.TryGetProperty(_names.Name, out var nameProp) && (name = nameProp.GetString()!) != null)) { 
			throw new PhyloTreeFormatException($"Node ({id}) name is not a valid string."); 
		}
		int? parent = GetIntNullable(_names.Parent, jr, id);
		int year = GetInt(_names.Year, jr, id);
		List<int> children = new List<int>();
		foreach(var ch in jr.GetProperty(_names.Children).EnumerateArray()) {
			children.Add(ch.GetInt32());
		}
		if(!jr.TryGetProperty(_names.SpecialFields, out var sf)) {
			throw new PhyloTreeFormatException($"Node '{id}' does not have for special fields."); 
		}
		try {
			var result = _nodeJsonExtender.ReadSpecialFields(new PhyloTreeNode(
				id: id,
				parentId: parent,
				name: name,
				approximateYear: year,
				children: children), sf);
			if(result == null)
				throw new NullReferenceException($"Node of id {id} is null after loading.");
			return result;
		} catch(Exception e) {
			throw new JsonException("An exception was raised when factory tried to deserialize into a phylo tree node type.", e);
		}
	}

	/// <summary>
	/// Saves a <paramref name="node"/> to the json file.
	/// </summary>
	private void WriteNode(TNode node, Utf8JsonWriter jw) {
		jw.WriteStartObject();
		jw.WriteNumber(_names.Id, node.Id);
		jw.WriteString(_names.Name, node.Name);
		if(node.Parent != null)
			jw.WriteNumber(_names.Parent, node.Parent.Value);
		else
			jw.WriteNull(_names.Parent);
		jw.WriteNumber(_names.Year, node.ApproximateYear);
		jw.WritePropertyName(_names.Children);
		jw.WriteStartArray();
		foreach(var ch in node.Children) {
			jw.WriteNumberValue(ch);
		}
		jw.WriteEndArray();
		jw.WritePropertyName(_names.SpecialFields);
		jw.WriteStartObject();
		_nodeJsonExtender.WriteSpecialFields(jw, node);
		jw.WriteEndObject();
		jw.WriteEndObject();
	}

	/// <summary>
	/// Stores string constants by which the fields are recognised within the json file.
	/// </summary>
	private record Names(string Roots, string Nodes, string TreeId, string CreationTime, string Id, string Name, string Parent, string Year, string Children, string SpecialFields) {
		public Names() 
			:this(nameof(Roots), 
				 nameof(Nodes), 
				 nameof(TreeId), 
				 nameof(CreationTime), 
				 nameof(Id), 
				 nameof(Name), 
				 nameof(Parent), 
				 nameof(Year), 
				 nameof(Children), 
				 nameof(SpecialFields)){}
	}
}

