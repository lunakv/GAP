﻿using Newtonsoft.Json;
using PhyloTree.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace PhyloTree.Access;

internal interface INodeJsonExtender<TNode> where TNode : PhyloTreeNode {
	TNode ReadSpecialFields(PhyloTreeNode priorNode, JsonElement reader);
	void WriteSpecialFields(Utf8JsonWriter writer, TNode node);
}
