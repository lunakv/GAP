﻿using PhyloTree.DataClasses.Interfaces;

namespace PhyloTree.DataClasses;

public class PhyloVariantedNode : PhyloTreeNode, IWithVariants {
	public IReadOnlyCollection<string> Variants { get; }
	public PhyloVariantedNode(int id, int? parentId, string name, int approximateYear, List<int> children, IReadOnlyCollection<string> variants) 
		: base(id, parentId, name, approximateYear, children) {

		Variants = variants;
	}
	public PhyloVariantedNode(PhyloTreeNode prior, IReadOnlyCollection<string> variants) 
		: this(prior.Id, prior.Parent, prior.Name, prior.ApproximateYear, prior.Children, variants) {
	}


}
