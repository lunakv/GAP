﻿namespace PhyloTree.DataClasses; 

public record NumberOfPeople(int Inside, int InSubtree);
