﻿using PhyloTree.DataClasses.Interfaces;

namespace PhyloTree.DataClasses;

/// <summary>
/// Phylogenetic tree node used to store pruned tree with the haplogroups in the respective nodes.
/// </summary>
public class PhyloHaplogroupedNode : PhyloTreeNode, IWithHaplogroup { 
	public bool IsHaplogroupRoot { get; }
	public int HaplogroupId { get; }

    public PhyloHaplogroupedNode(int id, int? parentId, string name, int approximateYear, List<int> children,
        bool isHaplogroupRoot, int haplogroupId)
        : base(id, parentId, name, approximateYear, children)
    {
        HaplogroupId = haplogroupId;
        IsHaplogroupRoot = isHaplogroupRoot;
    }

    public PhyloHaplogroupedNode(PhyloTreeNode node, int haplogroupId, bool isHaplogroupRoot)
        : this(node.Id, node.Parent, node.Name, node.ApproximateYear, node.Children, isHaplogroupRoot, haplogroupId) { }

}

