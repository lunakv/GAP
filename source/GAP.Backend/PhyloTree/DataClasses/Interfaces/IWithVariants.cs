﻿namespace PhyloTree.DataClasses.Interfaces; 

public interface IWithVariants {
	IReadOnlyCollection<string> Variants { get; }
}