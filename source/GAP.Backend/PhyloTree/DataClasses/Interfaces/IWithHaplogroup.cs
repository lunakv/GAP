﻿namespace PhyloTree.DataClasses.Interfaces; 

public interface IWithHaplogroup {
	bool IsHaplogroupRoot { get; }
	int HaplogroupId { get; }

}
