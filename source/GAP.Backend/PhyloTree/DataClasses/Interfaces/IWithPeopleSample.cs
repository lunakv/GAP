﻿namespace PhyloTree.DataClasses.Interfaces; 

public interface IWithPeopleSample<T> {
	NumberOfPeople PeopleCount { get; }
	IList<T> PeopleSample { get; }
}
