﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhyloTree.DataClasses;

public class PhyloTreeNode
{
    public string Name { get; }
    public int Id { get; }
    public int? Parent { get; }
    public int ApproximateYear { get; }
    public List<int> Children { get; } = new List<int>();
    public PhyloTreeNode(int id, int? parentId, string name, int approximateYear, List<int> children)
    {
        Name = name;
        this.Id = id;
        Parent = parentId;
        Children = children;
        ApproximateYear = approximateYear;
    }
}
