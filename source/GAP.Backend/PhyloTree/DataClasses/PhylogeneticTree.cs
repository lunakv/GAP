﻿using PhyloTree.Exceptions;

namespace PhyloTree.DataClasses;

/// <summary>
/// Stores the whole tree.
/// </summary>
/// <typeparam name="TNode"></typeparam>
public class PhylogeneticTree<TNode> where TNode : PhyloTreeNode
{
    /// <summary>
    /// The ids of roots of the tree(s).
    /// </summary>
    public IReadOnlyList<int> Roots
        => _roots;

    private List<int> _roots = new List<int>();

    /// <summary>
    /// A dictionary from node ids to the actual nodes.
    /// </summary>
    public IReadOnlyDictionary<int, TNode> Nodes
        => _nodes;

    private Dictionary<int, TNode> _nodes = new();

    /// <summary>
    /// When the tree was created by the web source.
    /// </summary>
    public DateTime? CreationTime { get; private set; }

    /// <summary>
    /// The tree id. It is not a database id, it is an id parsed from the web source.
    /// </summary>
    public int? TreeId { get; private set; }
    
    /// <summary>
    /// Possible way to change to the nodes from <typeparamref name="TNode"/> to <typeparamref name="T"/>
    /// using the <paramref name="castingFunc"/>.
    /// </summary>
    public PhylogeneticTree<T> Cast<T>(Func<TNode, T> castingFunc) where T : PhyloTreeNode {
        return Cast(castingFunc, i => i);
    }
    
    /// <summary>
    /// Possible way to change to the nodes from <typeparamref name="TNode"/> to <typeparamref name="T"/>
    /// using the <paramref name="castingFunc"/>. Ids will change given the <paramref name="idChangeFunc"/>.
    /// </summary>
    public PhylogeneticTree<T> Cast<T>(Func<TNode, T> castingFunc, Func<int, int> idChangeFunc) where T : PhyloTreeNode {
        var result = new PhylogeneticTree<T>();
        result._roots = new List<int>(_roots.Select(i => idChangeFunc(i)));
        result.TreeId = TreeId;
        result.CreationTime = CreationTime;
        result._nodes = _nodes.ToDictionary(p => idChangeFunc(p.Key), p => castingFunc(p.Value));
        return result;
    }

    /// <summary>
    /// Private constructor of the class - it can only be created by the <see cref="TreeBuilder{TTree}"/> class
    /// (or for convinience <see cref="TreeBuilder"/>).
    /// </summary>
    private PhylogeneticTree() { }

    public class TreeBuilder : TreeBuilder<PhylogeneticTree<TNode>>
    {
        public TreeBuilder() : base(() => new PhylogeneticTree<TNode>()) { }
    }

    /// <summary>
    /// The class used to create a <see cref="PhylogeneticTree{TNode}"/>.
    /// </summary>
    public class TreeBuilder<TTree> where TTree : PhylogeneticTree<TNode>
    {
        readonly TTree _tree;
        public TreeBuilder(Func<TTree> func)
            => _tree = func();

        /// <summary>
        /// Adds a <paramref name="node"/> to the tree.
        /// </summary>
        public void Add(TNode node, bool isRoot = false)
        {
            _tree._nodes.Add(node.Id, node);
            if (isRoot)
                _tree._roots.Add(node.Id);
        }

        /// <summary>
        /// Sets the <see cref="CreationTime"/> of the <see cref="PhylogeneticTree{TNode}"/>.
        /// </summary>
        public void SetDate(DateTime time)
        {
            _tree.CreationTime = time;
        }

        /// <summary>
        /// Sets the <see cref="TreeId"/> of the <see cref="PhylogeneticTree{TNode}"/>.
        /// </summary>
        public void SetTreeID(int ID)
        {
            _tree.TreeId = ID;
        }

        /// <summary>
        /// Returns the current representation of the tree.
        /// </summary>
        /// <exception cref="NoRootSpecifiedException">Thrown if no roots were specified.</exception>
        public TTree GetTree()
        {
            if (_tree._roots.Count == 0)
                throw new NoRootSpecifiedException(_tree._nodes.Count);
            return _tree;
        }


    }
}
