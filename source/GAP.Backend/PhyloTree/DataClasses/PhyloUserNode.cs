﻿using PhyloTree.DataClasses.Interfaces;
using Users.UserAccess.Entities;

namespace PhyloTree.DataClasses; 

/// <summary>
/// Phylogenetic tree node used to store complete data about users. It does not store variants.
/// </summary>
public class PhyloUserNode : PhyloTreeNode, IWithHaplogroup, IWithPeopleSample<CompleteUser> {
    public IList<CompleteUser> PeopleSample { get; }
    public NumberOfPeople PeopleCount { get; }
	public bool IsHaplogroupRoot { get; }
	public int HaplogroupId { get; }

	public PhyloUserNode(int id, int? parentId, string name, int approximateYear, List<int> children,
        NumberOfPeople numberOfPeople, IList<CompleteUser> peopleSample, bool isHaplogroupRoot, int haplogroupId)
        : base(id, parentId, name, approximateYear, children) {
        PeopleSample = peopleSample;
        PeopleCount = numberOfPeople;
        IsHaplogroupRoot = isHaplogroupRoot;
        HaplogroupId = haplogroupId;
	}
}
