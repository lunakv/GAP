﻿using PhyloTree.DataClasses.Interfaces;

namespace PhyloTree.DataClasses;

/// <summary>
/// Phylogenetic tree node used to store pruned tree, but abstractly without users, only their Ids.
/// </summary>
public class PhyloQuantifiedNode : PhyloTreeNode, IWithHaplogroup, IWithPeopleSample<int>
{
    public IList<int> PeopleSample { get; }
	public NumberOfPeople PeopleCount { get; }
	public bool IsHaplogroupRoot { get; }
	public int HaplogroupId { get; }

    public PhyloQuantifiedNode(int id, int? parentId, string name, int approximateYear, List<int> children,
        NumberOfPeople numberOfPeople, IList<int> peopleSample, bool isHaplogroupRoot, int haplogroupId)
        : base(id, parentId, name, approximateYear, children)
    {
        PeopleSample = peopleSample;
        PeopleCount = numberOfPeople;
        HaplogroupId = haplogroupId;
        IsHaplogroupRoot = isHaplogroupRoot;
    }

    public PhyloQuantifiedNode(PhyloTreeNode node, int haplogroupId, NumberOfPeople peopleCount, IList<int> peopleSample, bool isHaplogroupRoot)
        : this(node.Id, node.Parent, node.Name, node.ApproximateYear, node.Children, peopleCount, peopleSample, isHaplogroupRoot, haplogroupId) { }

}

