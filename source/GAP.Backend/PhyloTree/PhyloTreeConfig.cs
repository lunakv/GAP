﻿namespace PhyloTree; 

public class PhyloTreeConfig {
	public string FullTreePath { get; set; }
	public string QuantifiedTreePath { get; set; }
	public string FullTreeWebPage { get; set; }
	public string BaseHaplogroupsDirectory { get; set; }
	public List<string> DefaultIgnoredNodes { get; set; }
	public List<string> DefaultAddedNodes { get; set; }

	public PhyloTreeConfig() {
		FullTreePath = string.Empty;
		QuantifiedTreePath = string.Empty;
		FullTreeWebPage = string.Empty;
		BaseHaplogroupsDirectory = string.Empty;
		DefaultIgnoredNodes = new List<string>();
		DefaultAddedNodes = new List<string>();
	}
}
