﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhyloTree {
	public interface IHaplogroupOriginTimeEstimator {
		int? GetTime(string haploName);
	}
}
