﻿using CsvHelper;
using Microsoft.Extensions.Options;
using PhyloTree.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhyloTree; 
public class FullTreeSaver {
	private readonly IPhyloTreeDownloader _downloader;
	private readonly IPhyloTreeWebParser _parser;
	private readonly IPhyloTreeSaver<PhyloVariantedNode> _fullTreeSaver;
	private readonly PhyloTreeConfig _treeConfig;

	public FullTreeSaver(IOptions<PhyloTreeConfig> treeConfig, IPhyloTreeDownloader downloader, IPhyloTreeWebParser parser, IPhyloTreeSaver<PhyloVariantedNode> fullTreeSaver) {
		_downloader = downloader;
		_parser = parser;
		_fullTreeSaver = fullTreeSaver;
		_treeConfig = treeConfig.Value;
	}

	public async Task SaveFullTree() {
		_fullTreeSaver.SaveTree(await GetFullTree());
	}

	public async Task<PhylogeneticTree<PhyloVariantedNode>> GetFullTree() {
		var url = _treeConfig.FullTreeWebPage;
		var stream = await _downloader.DownloadTreeAsync(url).ConfigureAwait(false);
		var estimator = await HaplogroupOriginTimeEstimator.Create().ConfigureAwait(false);
		return _parser.GetPhyloTree(stream, estimator);
	}
}
