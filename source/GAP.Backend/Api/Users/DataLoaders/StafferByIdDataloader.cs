using Auth.Identity;
using Auth.Identity.Entities;
using Auth.Identity.Storage;

namespace Api.Users.DataLoaders;

public class StafferByIdDataloader : BatchDataLoader<int, Staffer> {
	private readonly IIdentityManager _identityManager;

	public StafferByIdDataloader(IBatchScheduler batchScheduler, IIdentityManager identityManager) : base(batchScheduler) {
		_identityManager = identityManager;
	}

	protected override async Task<IReadOnlyDictionary<int, Staffer>> LoadBatchAsync(IReadOnlyList<int> keys, CancellationToken cancellationToken) {
		ICollection<Staffer> staffers = await _identityManager.GetStaffAsync(keys);
		return staffers.ToDictionary(staffer => staffer.Id, staffer => staffer);
	}
}