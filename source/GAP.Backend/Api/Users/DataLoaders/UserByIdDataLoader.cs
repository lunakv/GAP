using Users.GenData;
using Users.UserAccess.Entities;
using Users.UserAccess.Storage;

namespace Api.Users.DataLoaders;

public class UserByIdDataLoader : BatchDataLoader<int, User> {
	private readonly IUserFetcher _userFetcher;
	private readonly MarkerSorter _markerSorter;

	public UserByIdDataLoader(IBatchScheduler batchScheduler, IUserFetcher userFetcher, MarkerSorter markerSorter) : base(batchScheduler) {
		_userFetcher = userFetcher;
		_markerSorter = markerSorter;
	}

	protected override async Task<IReadOnlyDictionary<int, User>> LoadBatchAsync(IReadOnlyList<int> keys, CancellationToken cancellationToken) {
		var users = await _userFetcher.FindUsersByIdAsync(userIds: keys);
		await _markerSorter.LoadOrderingAsync();
		foreach (var kvp in users) {
			if (kvp.Value.GeneticData != null) {
				_markerSorter.AddOrdering(kvp.Value.GeneticData.StrMarkers);
			}
		}

		return users;
	}
}