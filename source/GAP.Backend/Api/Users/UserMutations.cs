using Api.Logging;
using Api.Users.Inputs;
using Api.Users.Types;
using Auth.Login;
using Auth.Policies;
using FluentValidation;
using HotChocolate.AspNetCore.Authorization;
using Users.UserAccess;
using Users.UserAccess.Entities;
using Users.UserAccess.Storage;
using Users.UserAccess.Validation;

namespace Api.Users;

[ExtendObjectType("Mutation")]
public class UserMutations {
	[Authorize(Policy = nameof(HasUserManageRightPolicy))]
	public async Task<Profile> UpdateProfile(int userId, ProfileInput profile, [Service] UserController userController,
		[Service] EndpointLogger<UserMutations> logger) {
		logger.LogAuthEndpointContext(aboutUserId: userId);
		logger.Logger.LogInformation("Request arguments: userId {UserId}, profile {@Profile}", userId, profile);
		var updatedProfile = await userController.UpdateProfileAsync(userId: userId, profile: profile.ToProfile());
		logger.LogSuccessfulRequest();
		return updatedProfile;
	}

	[Authorize(Policy = nameof(HasUserManageRightPolicy))]
	public async Task<Ancestor> UpdateAncestor(int userId, AncestorInput ancestor, [Service] UserController userController,
		[Service] EndpointLogger<UserMutations> logger) {
		logger.LogAuthEndpointContext(aboutUserId: userId);
		logger.Logger.LogInformation("Request arguments: userId {UserId}, ancestor {@Ancestor}", userId, ancestor);
		var updatedAncestor = await userController.UpdateAncestorAsync(userId: userId, ancestor: ancestor.ToAncestor());
		logger.LogSuccessfulRequest();
		return updatedAncestor;
	}
	
	[Authorize(Policy = nameof(IsUserThemselfWithoutSignedAdministrationAgreementPolicy))]
	public async Task<AdminitrationAgreementSignResult> SignAdministrationAgreementSignStatus(int userId, [Service] UserController userController, [Service] LoginController loginController,
		[Service] EndpointLogger<UserMutations> logger) {
		logger.LogAuthEndpointContext(aboutUserId: userId);
		logger.Logger.LogInformation("Request arguments: userId {UserId}.", userId);
		bool administrationAgreementSigned = await userController.SignAdministrationAgreement(userId: userId);
		string token = await loginController.GenerateLoginToken(userId: userId);
		logger.LogSuccessfulRequest();
		return new AdminitrationAgreementSignResult(success: administrationAgreementSigned, loginToken: token);
	}
}