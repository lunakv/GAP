﻿using Api.GenData.Types;
using Api.Map.Types;
using Api.Users.DataLoaders;
using Api.Users.Types;
using Laboratory.KitManagement.Kits;
using Map;
using Map.Map;
using Map.MapFetcher;
using Users.UserAccess.Storage;
using Users.UserAccess.Entities;
using Users.GenData;

namespace Api.Users.PublicApi;

[GraphQLName("PublicApiUser")]
public class PublicApiUserDto {
	[GraphQLName("id")]
	public string PublicId { get; set; }
	[GraphQLName("haplogroup")]
	public string HaplogroupName { get; set; }
	public string? Municipality { get; set; }
	public IList<StrMarkerPublicDto> StrMarkers { get; set; }

	public PublicApiUserDto(CompleteUser user) {
		PublicId = user.PublicId;
		HaplogroupName = user.GeneticData.Haplogroup.Name;
		StrMarkers = user.GeneticData.StrMarkers
			.CloneAsList()
			.Select(s => new StrMarkerPublicDto(s.Name, s.Value))
			.ToList();
		Municipality = user.Profile.ResidenceAddress.Municipality;
	}
}

[GraphQLName("StrMarkerPublic")]
public record StrMarkerPublicDto(string Name, int Value);