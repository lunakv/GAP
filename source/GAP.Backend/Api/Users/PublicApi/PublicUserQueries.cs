﻿using Api.Logging;
using Users.UserAccess;
using Users.UserAccess.UserFilter;
using Users.Exceptions;
using PhyloTree.DataClasses;
using HotChocolate.AspNetCore.Authorization;
using Auth.Policies;
using Api.Users.Types.Filtering;

namespace Api.Users.PublicApi; 

[ExtendObjectType("Query")]
public class PublicUserQueries {

	[Authorize(Policy = nameof(HasPublicApiAccessPolicy))]
	[UseOffsetPaging]
	[UseFiltering]
	public async Task<List<PublicApiUserDto>> GetPublicUsers([Service] CompleteUserFetcher userFetcher,
		[Service] EndpointLogger<PublicUserQueries> logger) {

		logger.LogAuthEndpointContext();

		var users = await userFetcher.GetUsersAsync();
		var result = users
			.Select(u => new PublicApiUserDto(u))
			.ToList();

		logger.LogSuccessfulRequest();

		return result;
	}
}
