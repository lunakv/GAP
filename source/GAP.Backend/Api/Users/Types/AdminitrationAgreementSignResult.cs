namespace Api.Users.Types; 

public class AdminitrationAgreementSignResult {
	public bool Success { get; set; }
	public string LoginToken { get; set; }

	public AdminitrationAgreementSignResult(bool success, string loginToken) {
		Success = success;
		LoginToken = loginToken;
	}
}