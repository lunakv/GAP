using Api.GenData.Types.Filtering;
using HotChocolate.Data.Filters;
using Users.UserAccess.Entities;

namespace Api.Users.Types.Filtering;

public class UserFilterInput : FilterInputType<User> {
	private const string UserFilterInputTypeName = nameof(UserFilterInput);

	protected override void Configure(IFilterInputTypeDescriptor<User> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(UserFilterInputTypeName);

		descriptor.Field(user => user.Id);
		descriptor.Field(user => user.Profile)
			.Type<ProfileFilterInput>();
		descriptor.Field(user => user.Ancestor)
			.Type<AncestorFilterInput>();
		descriptor.Field(user => user.RegionId);
		descriptor.Field(user => user.GeneticData)
			.Type<GeneticDataFilterInput>();
	}
}