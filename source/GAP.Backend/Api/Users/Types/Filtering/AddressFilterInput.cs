using HotChocolate.Data.Filters;
using Users.UserAccess.Entities;

namespace Api.Users.Types.Filtering;

public class AddressFilterInput : FilterInputType<Address> {
	private const string AddressFilterInputTypeName = nameof(AddressFilterInput);

	protected override void Configure(IFilterInputTypeDescriptor<Address> descriptor) {
		descriptor.BindFieldsImplicitly();

		descriptor.Name(AddressFilterInputTypeName);

		descriptor.Field(address => address.Town);
		descriptor.Field(address => address.Street);
		descriptor.Field(address => address.ZipCode);
	}
}