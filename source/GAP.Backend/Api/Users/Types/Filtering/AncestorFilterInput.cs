using HotChocolate.Data.Filters;
using Users.UserAccess.Entities;

namespace Api.Users.Types.Filtering;

public class AncestorFilterInput : FilterInputType<Ancestor> {
	private const string AncestorFilterInputTypeName = nameof(AncestorFilterInput);

	protected override void Configure(IFilterInputTypeDescriptor<Ancestor> descriptor) {
		descriptor.BindFieldsImplicitly();

		descriptor.Name(AncestorFilterInputTypeName);

		descriptor.Field(ancestor => ancestor.Address)
			.Type<ExpandedAddressFilterInput>();
		descriptor.Field(ancestor => ancestor.GivenName);
		descriptor.Field(ancestor => ancestor.FamilyName);
		descriptor.Field(ancestor => ancestor.BirthDate);
		descriptor.Field(ancestor => ancestor.PlaceOfBirth);
		descriptor.Field(ancestor => ancestor.DeathDate);
		descriptor.Field(ancestor => ancestor.PlaceOfDeath);
	}
}