using HotChocolate.Data.Filters;
using Users.UserAccess.Entities;

namespace Api.Users.Types.Filtering; 

public class ExpandedAddressFilterInput : FilterInputType<ExpandedAddress> {
	private const string AddressFilterInputTypeName = nameof(ExpandedAddressFilterInput);

	protected override void Configure(IFilterInputTypeDescriptor<ExpandedAddress> descriptor) {
		descriptor.BindFieldsImplicitly();

		descriptor.Name(AddressFilterInputTypeName);

		descriptor.Field(address => address.Town);
		descriptor.Field(address => address.Municipality);
		descriptor.Field(address => address.County);
		descriptor.Field(address => address.Street);
		descriptor.Field(address => address.ZipCode);
	}
}