using Api.Users.Inputs;
using HotChocolate.Data.Filters;
using Users.UserAccess.Entities;

namespace Api.Users.Types.Filtering; 

public class ProfileFilterInput : FilterInputType<Profile> {
	private const string ProfileFilterInputTypeName = nameof(ProfileFilterInput);

	protected override void Configure(IFilterInputTypeDescriptor<Profile> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(ProfileFilterInputTypeName);

		descriptor.Field(profile => profile.GivenName);
		descriptor.Field(profile => profile.FamilyName);
		descriptor.Field(profile => profile.Email);
		descriptor.Field(profile => profile.PhoneNumber);
		descriptor.Field(profile => profile.ResidenceAddress)
			.Type<ExpandedAddressFilterInput>();
		descriptor.Field(profile => profile.CorrespondenceAddress)
			.Type<AddressFilterInput>();
		descriptor.Field(profile => profile.BirthDate)
			.Type<DateType>();
	}
}