using Users.UserAccess.Entities;

namespace Api.Users.Types;

public class ProfileType : ObjectType<Profile> {
	private const string ProfileTypeName = "Profile";

	protected override void Configure(IObjectTypeDescriptor<Profile> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(ProfileTypeName);

		descriptor.Field(profile => profile.GivenName)
			.Type<NonNullType<StringType>>();
		descriptor.Field(profile => profile.FamilyName)
			.Type<NonNullType<StringType>>();
		descriptor.Field(profile => profile.Email)
			.Type<NonNullType<StringType>>();
		descriptor.Field(profile => profile.PhoneNumber)
			.Type<StringType>();
		descriptor.Field(profile => profile.ResidenceAddress)
			.Type<NonNullType<ExpandedAddressType>>();
		descriptor.Field(profile => profile.CorrespondenceAddress)
			.Type<AddressType>();
		descriptor.Field(profile => profile.BirthDate)
			.Type<DateType>();
	}
}