using Users.UserAccess.Entities;

namespace Api.Users.Types.Public;

public class PublicProfileType : ObjectType<Profile> {
	private const string PublicProfileTypeName = "PublicProfile";
	private const string ResidenceAddressPropertyName = "address";

	protected override void Configure(IObjectTypeDescriptor<Profile> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(PublicProfileTypeName);

		descriptor.Field(profile => profile.GivenName)
			.Resolve(ctx => ctx.Parent<Profile>().GivenName != string.Empty ? ctx.Parent<Profile>().GivenName : null)
			.Type<StringType>();
		descriptor.Field(profile => profile.FamilyName)
			.Type<StringType>();
		descriptor.Field(profile => profile.Email)
			.Resolve(ctx => ctx.Parent<Profile>().Email != string.Empty ? ctx.Parent<Profile>().Email : null)
			.Type<StringType>();
		descriptor.Field(profile => profile.ResidenceAddress)
			.Name(ResidenceAddressPropertyName)
			.Type<NonNullType<PublicAddressType>>();
	}
}