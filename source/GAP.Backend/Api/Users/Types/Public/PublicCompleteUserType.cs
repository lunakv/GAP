using Api.GenData.Types.Public;
using Users.UserAccess.Entities;

namespace Api.Users.Types.Public;

public class PublicCompleteUserType : ObjectType<CompleteUser> {
	private const string PublicUserTypeName = "PublicCompleteUser";

	protected override void Configure(IObjectTypeDescriptor<CompleteUser> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(PublicUserTypeName);

		descriptor.Field(user => user.Profile)
			.Type<NonNullType<PublicProfileType>>();
		descriptor.Field(user => user.GeneticData)
			.Type<NonNullType<PublicGeneticDataType>>();
		descriptor.Field(user => user.Ancestor)
			.Type<NonNullType<AncestorType>>();
	}
}