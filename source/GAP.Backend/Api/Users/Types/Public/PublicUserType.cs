using Api.GenData.Types.Public;
using Users.UserAccess.Entities;

namespace Api.Users.Types.Public;

public class PublicUserType : ObjectType<User> {
	private const string PublicUserTypeName = "PublicUser";

	protected override void Configure(IObjectTypeDescriptor<User> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(PublicUserTypeName);

		descriptor.Field(user => user.Profile)
			.Type<NonNullType<PublicProfileType>>();
		descriptor.Field(user => user.GeneticData)
			.Type<PublicGeneticDataType>();
		descriptor.Field(user => user.Ancestor)
			.Type<NonNullType<AncestorType>>();
	}
}