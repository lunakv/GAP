using Users.UserAccess.Entities;

namespace Api.Users.Types.Public;

public class PublicAddressType : ObjectType<ExpandedAddress> {
	private const string PublicAddressTypeName = "PublicAddress";

	protected override void Configure(IObjectTypeDescriptor<ExpandedAddress> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(PublicAddressTypeName);

		descriptor.Field(address => address.Municipality)
			.Type<StringType>();
	}
}