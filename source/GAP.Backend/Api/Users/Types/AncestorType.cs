using Users.UserAccess.Entities;

namespace Api.Users.Types;

public class AncestorType : ObjectType<Ancestor> {
	private const string AncestorTypeName = "Ancestor";

	protected override void Configure(IObjectTypeDescriptor<Ancestor> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(AncestorTypeName);

		descriptor.Field(ancestor => ancestor.Address)
			.Type<ExpandedAddressType>();
		descriptor.Field(ancestor => ancestor.GivenName)
			.Type<StringType>();
		descriptor.Field(ancestor => ancestor.FamilyName)
			.Type<StringType>();
		descriptor.Field(ancestor => ancestor.BirthDate)
			.Type<DateType>();
		descriptor.Field(ancestor => ancestor.PlaceOfBirth)
			.Type<StringType>();
		descriptor.Field(ancestor => ancestor.DeathDate)
			.Type<DateType>();
		descriptor.Field(ancestor => ancestor.PlaceOfDeath)
			.Type<StringType>();
	}
}