using Api.GenData.Types;
using Auth.Policies;
using Users.UserAccess.Entities;

namespace Api.Users.Types;

public class UserType : ObjectType<User> {
	private const string UserTypeName = "User";

	protected override void Configure(IObjectTypeDescriptor<User> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(UserTypeName);

		descriptor.Field(user => user.Id)
			.Type<NonNullType<IntType>>();
		descriptor.Field(user => user.AdministrationAgreementSigned)
			.Type<NonNullType<BooleanType>>()
			.Authorize(policy: nameof(IsAdminPolicy));
		descriptor.Field(user => user.PublicId)
			.Type<NonNullType<StringType>>()
			.Authorize(policy: nameof(IsAdminPolicy));
		descriptor.Field(user => user.Profile)
			.Type<NonNullType<ProfileType>>();
		descriptor.Field(user => user.Ancestor)
			.Type<NonNullType<AncestorType>>();
		descriptor.Field(user => user.RegionId)
			.Type<NonNullType<IntType>>();
		descriptor.Field(user => user.GeneticData)
			.Type<GeneticDataType>();
		descriptor.Field(user => user.AdministratorData)
			.Authorize(policy: nameof(HasLabAccessPolicy))
			.Type<NonNullType<AdministratorDataType>>();
	}
}