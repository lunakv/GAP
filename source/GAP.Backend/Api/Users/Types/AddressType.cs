using Users.UserAccess.Entities;

namespace Api.Users.Types;

public class AddressType : ObjectType<Address> {
	private const string AddressTypeName = "Address";

	protected override void Configure(IObjectTypeDescriptor<Address> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(AddressTypeName);

		descriptor.Field(address => address.Town)
			.Type<NonNullType<StringType>>();
		descriptor.Field(address => address.Street)
			.Type<NonNullType<StringType>>();
		descriptor.Field(address => address.ZipCode)
			.Type<NonNullType<StringType>>();
	}
}