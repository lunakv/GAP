using Users.UserAccess.Entities;

namespace Api.Users.Types;

public class AdministratorDataType : ObjectType<AdministratorData> {
	private const string AdministratorDataTypeName = "AdministratorData";

	protected override void Configure(IObjectTypeDescriptor<AdministratorData> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(AdministratorDataTypeName);

		descriptor.Field(adminData => adminData.Id)
			.Type<StringType>();
		descriptor.Field(adminData => adminData.Note)
			.Type<StringType>();
	}
}