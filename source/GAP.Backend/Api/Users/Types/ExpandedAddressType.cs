using Users.UserAccess.Entities;

namespace Api.Users.Types;

public class ExpandedAddressType : ObjectType<ExpandedAddress> {
	private const string ExpandedAddressTypeName = "ExpandedAddress";

	protected override void Configure(IObjectTypeDescriptor<ExpandedAddress> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(ExpandedAddressTypeName);

		descriptor.Field(address => address.Town)
			.Type<StringType>();
		descriptor.Field(address => address.Municipality)
			.Type<StringType>();
		descriptor.Field(address => address.County)
			.Type<StringType>();
		descriptor.Field(address => address.Street)
			.Type<StringType>();
		descriptor.Field(address => address.ZipCode)
			.Type<StringType>();
	}
}