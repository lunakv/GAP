﻿using Api.Logging;
using Api.Users.Types.Filtering;
using Auth.Policies;
using HotChocolate.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using Users.GenData;
using Users.UserAccess;
using Users.UserAccess.Entities;
using Users.UserAccess.Storage;

namespace Api.Users;

[ExtendObjectType("Query")]
public class UserQueries {
	[Authorize(Policy = nameof(HasUserViewRightPolicy))]
	public async Task<User> GetUser(int userId, [Service] IUserFetcher userFetcher, [Service] MarkerSorter markerSorter,  [Service] EndpointLogger<UserQueries> endpointLogger) {
		endpointLogger.LogAuthEndpointContext();
		endpointLogger.Logger.LogInformation("Request arguments: userId {UserId}", userId);
		User user = await userFetcher.FindUserByIdAsync(userId);
		await markerSorter.LoadOrderingAsync();
		if (user.GeneticData != null) {
			markerSorter.AddOrdering(user.GeneticData.StrMarkers);
		}
		endpointLogger.LogSuccessfulRequest();
		return user;
	}

	[UseOffsetPaging]
	[UseFiltering(filterType: typeof(UserFilterInput))]
	[Authorize(Policy = nameof(IsAdminPolicy))]
	public async Task<IList<User>> GetUsers([Service] IUserFetcher userFetcher, [Service] MarkerSorter markerSorter, [Service] EndpointLogger<UserQueries> endpointLogger) {
		endpointLogger.LogAuthEndpointContext();
		endpointLogger.Logger.LogInformation("No request arguments");
		IList<User> users = await userFetcher.GetUsersAsync();
		await markerSorter.LoadOrderingAsync();
		foreach (var user in users) {
			if (user.GeneticData != null) {
				markerSorter.AddOrdering(user.GeneticData.StrMarkers);
			}
		}
		endpointLogger.LogSuccessfulRequest();
		return users;
	}

	[Authorize(Policy = nameof(IsUiUserPolicy))]
	public async Task<IList<string>> GetFamilyNames([Service] IUserFetcher userFetcher, [Service] EndpointLogger<UserQueries> endpointLogger) {
		endpointLogger.LogAuthEndpointContext();
		endpointLogger.Logger.LogInformation("No request arguments");
		IList<User> users = await userFetcher.GetUsersAsync();
		return users.Select(u => u.Profile.FamilyName).Distinct().OrderBy(u => u).ToList();
	}
}