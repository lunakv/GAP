using Users.UserAccess.Entities;

namespace Api.Users.Inputs;

public class ProfileInput {
	public string GivenName { get; set; } = null!;
	public string FamilyName { get; set; } = null!;
	public string Email { get; set; } = null!;
	public string PhoneNumber { get; set; } = null!;
	public ExpandedAddressInput ResidenceAddress { get; set; } = null!;
	public AddressInput? CorrespondenceAddress { get; set; }
	public DateOnly BirthDate { get; set; }

	public ProfileInput() { }

	[GraphQLIgnore]
	public Profile ToProfile() {
		return new Profile(
			givenName: GivenName,
			familyName: FamilyName,
			email: Email,
			phoneNumber: PhoneNumber,
			residenceAddress: ResidenceAddress.ToExpandedAddress(),
			correspondenceAddress: CorrespondenceAddress?.ToAddress(),
			birthDate: BirthDate
		);
	}
}