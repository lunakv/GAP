using Users.UserAccess.Entities;

namespace Api.Users.Inputs; 

public class ExpandedAddressInput {
	public string Town { get; set; } = null!;
	public string Municipality { get; set; } = null!;
	public string County { get; set; } = null!;
	public string Street { get; set; } = null!;
	public string ZipCode { get; set; } = null!;
	
	public ExpandedAddressInput() {}

	[GraphQLIgnore]
	public ExpandedAddress ToExpandedAddress() {
		return new ExpandedAddress(
			town: Town,
			municipality: Municipality,
			county: County,
			street: Street,
			zipCode: ZipCode
		);
	}
}