using Users.UserAccess.Entities;

namespace Api.Users.Inputs;

public class AncestorInput {
	public NullableExpandedAddressInput? Address { get; set; }
	public string? GivenName { get; set; }
	public string? FamilyName { get; set; }
	public DateOnly? BirthDate { get; set; }
	public string? PlaceOfBirth { get; set; }
	public DateOnly? DeathDate { get; set; }
	public string? PlaceOfDeath { get; set; }

	public AncestorInput() { }

	[GraphQLIgnore]
	public Ancestor ToAncestor() {
		return new Ancestor(
			address: Address?.ToExpandedAddress() ?? new ExpandedAddress(),
			givenName: GivenName,
			familyName: FamilyName,
			birthDate: BirthDate,
			placeOfBirth: PlaceOfBirth,
			deathDate: DeathDate,
			placeOfDeath: PlaceOfDeath
		);
	}
}