using Users.UserAccess.Entities;

namespace Api.Users.Inputs; 

public class AddressInput {
	public string Town { get; set; } = null!;
	public string Street { get; set; } = null!;
	public string ZipCode { get; set; } = null!;

	public AddressInput() {}
	
	public AddressInput(Address address) {
		Town = address.Town;
		Street = address.Street;
		ZipCode = address.ZipCode;
	}
	
	[GraphQLIgnore]
	public Address ToAddress() {
		return new Address(
			town: Town,
			street: Street,
			zipCode: ZipCode
		);
	}
}