using Users.UserAccess.Entities;

namespace Api.Users.Inputs;

public class NullableExpandedAddressInput {
	public string? Town { get; set; }
	public string? Municipality { get; set; }
	public string? County { get; set; }
	public string? Street { get; set; }
	public string? ZipCode { get; set; }
	
	public NullableExpandedAddressInput() {}

	public NullableExpandedAddressInput(ExpandedAddress expandedAddress) {
		Town = expandedAddress.Town;
		Municipality = expandedAddress.Municipality;
		County = expandedAddress.County;
		Street = expandedAddress.Street;
		ZipCode = expandedAddress.ZipCode;
	}

	[GraphQLIgnore]
	public ExpandedAddress ToExpandedAddress() {
		return new ExpandedAddress(
			town: Town,
			municipality: Municipality,
			county: County,
			street: Street,
			zipCode: ZipCode
		);
	}
}