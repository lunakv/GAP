using Api.Auth.Dtos;
using Api.Auth.Registration.Inputs;
using Api.Logging;
using Auth;
using Auth.Policies;
using Auth.Registration;
using Auth.Registration.Objects;
using HotChocolate.AspNetCore.Authorization;
using Users.GenData;
using Users.GenData.Parsers;

namespace Api.Auth.Registration;

[ExtendObjectType("Mutation")]
public class RegistrationMutations {
	public async Task<RegistrationResult> RegisterWithLabTest(RegisterWithLabTestInput input, [Service] RegistrationController registrationController,
		[Service] EndpointLogger<RegistrationMutations> logger) {
		logger.LogPublicEndpointContext();
		logger.Logger.LogInformation("Request arguments: registerWithLabTestInput {@RegisterWithLabTestInput}", input);
		RegistrationResult registrationResult = await registrationController.RegisterUserWithoutExternalDataAsync(
			registrationUserInput: input.User.ToRegistrationUserInput(),
			emailTextFactory: LanguageEmailTextFactoryTranslator.GetEmailTextFactory(language: input.Language)
		);
		logger.LogEndpointContext(performedByUserId: registrationResult.UserId, aboutUserId: registrationResult.UserId);
		logger.LogSuccessfulRequest();
		return registrationResult;
	}

	public async Task<RegistrationResult> RegisterWithSupportedExternalTestProvider(RegisterWithSupportedExternalTestProviderInput input,
		[Service] RegistrationController registrationController, [Service] EndpointLogger<RegistrationMutations> logger) {
		logger.LogPublicEndpointContext();
		logger.Logger.LogInformation(
			"Request arguments: registerWithSupportedExternalTestProviderInput {@RegisterWithSupportedExternalTestProviderInput}", input
		);
		RegistrationResult registrationResult = await registrationController.RegisterUserWithExternalDataAsync(
			registrationUserInput: input.User.ToRegistrationUserInput(labTestRequested: input.LabTestRequested),
			strFile: input.StrFile.OpenReadStream(),
			externalStrFileParser: new StrParserResolver(),
			emailTextFactory: LanguageEmailTextFactoryTranslator.GetEmailTextFactory(language: input.Language)
		);
		logger.LogEndpointContext(performedByUserId: registrationResult.UserId, aboutUserId: registrationResult.UserId);
		logger.LogSuccessfulRequest();
		return registrationResult;
	}

	public async Task<RegistrationResult> RegisterWithNotSupportedExternalTestProvider(RegisterWithNotSupportedExternalTestProviderInput input,
		[Service] RegistrationController registrationController, [Service] EndpointLogger<RegistrationMutations> logger) {
		logger.LogPublicEndpointContext();
		logger.Logger.LogInformation(
			"Request arguments: registerWithNotSupportedExternalTestProviderInput {@RegisterWithNotSupportedExternalTestProviderInput}", input
		);
		string fileExtension = System.IO.Path.GetExtension(input.StrFile.Name) ?? string.Empty;
		RegistrationResult registrationResult = await registrationController.RegisterUserWithExternalDataAsync(
			registrationUserInput: input.User.ToRegistrationUserInput(labTestRequested: input.LabTestRequested),
			strFile: input.StrFile.OpenReadStream(),
			externalStrFileParser: new UnsupportedProviderParser(
				geneticTestProvider: input.TestProvider.ToGeneticTestProvider(),
				fileExtension: fileExtension,
				strMarkers: new StrMarkerCollection<string, int>(input.StrMarkers.ToDictionary(m => m.Name, m => m.Value))
			),
			emailTextFactory: LanguageEmailTextFactoryTranslator.GetEmailTextFactory(language: input.Language)
		);
		logger.LogEndpointContext(performedByUserId: registrationResult.UserId, aboutUserId: registrationResult.UserId);
		logger.LogSuccessfulRequest();
		return registrationResult;
	}
	
	public async Task<OperationResult> ConfirmEmail(int userId, string token, [Service] RegistrationController registrationController,
		[Service] EndpointLogger<RegistrationMutations> logger) {
		logger.LogPublicEndpointContext();
		logger.Logger.LogInformation("Request arguments: userId {UserId}, token {Token}", userId, token);
		await registrationController.ConfirmEmailAsync(userId: userId, token: token);
		logger.LogEndpointContext(aboutUserId: userId, performedByUserId: userId);
		logger.LogSuccessfulRequest();
		return new OperationResult();
	}

	[Authorize(Policy = nameof(IsRootPolicy))]
	public async Task<RegistrationResult> CreateAccount(string email, UserType userType, Language language, [Service] RegistrationController registrationController,
		[Service] EndpointLogger<RegistrationMutations> logger) {
		logger.LogAuthEndpointContext();
		logger.Logger.LogInformation("Request arguments: email {email}", email);
		
		RegistrationResult registrationResult = await registrationController.CreateAccount(
			email: email,
			role: TypeRoleTranslator.GetRole(userType: userType),
			emailTextFactory: LanguageEmailTextFactoryTranslator.GetEmailTextFactory(language: language)
		);
		logger.LogAuthEndpointContext(aboutUserId: registrationResult.UserId);
		logger.LogSuccessfulRequest();
		return registrationResult;
	}
}