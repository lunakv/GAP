using Auth.Registration;
using Auth.Registration.Objects;

namespace Api.Auth.Registration.Types; 

public class RegistrationResultType : ObjectType<RegistrationResult> {
	private const string RegistrationResultTypeName = "RegistrationResult";

	protected override void Configure(IObjectTypeDescriptor<RegistrationResult> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(RegistrationResultTypeName);

		descriptor.Field(registrationResult => registrationResult.UserId)
			.Type<NonNullType<IntType>>();
	}
}