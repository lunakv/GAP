using Api.Auth.Dtos;

namespace Api.Auth.Registration.Inputs; 

public class RegisterWithSupportedExternalTestProviderInput {
	public RegistrationUserInput User { get; set; } = null!;
	public IFile StrFile { get; set; } = null!;
	public bool LabTestRequested { get; set; }
	public Language Language { get; set; }
}