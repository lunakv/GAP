using Api.Users.Inputs;

namespace Api.Auth.Registration.Inputs; 

public class RegistrationUserInput {
	public bool AdministrationAgreementSigned { get; set; }
	public RegistrationProfileInput Profile { get; set; } = null!;
	public AncestorInput Ancestor { get; set; } = null!;
	public string Password { get; set; } = null!;

	public global::Auth.Registration.Objects.RegistrationUserInput ToRegistrationUserInput(bool labTestRequested = true) {
		return new global::Auth.Registration.Objects.RegistrationUserInput(
			administrationAgreementSigned: AdministrationAgreementSigned,
			profile: Profile.ToProfile(),
			ancestor: Ancestor.ToAncestor(),
			password: Password,
			labTestRequested: labTestRequested
		);
	}
}