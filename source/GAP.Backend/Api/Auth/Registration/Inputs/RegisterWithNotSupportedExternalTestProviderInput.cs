using Api.Auth.Dtos;
using Api.GenData.Inputs;

namespace Api.Auth.Registration.Inputs; 

public class RegisterWithNotSupportedExternalTestProviderInput {
	public RegistrationUserInput User { get; set; } = null!;
	public IList<StrMarkerInput> StrMarkers { get; set; } = null!;
	public IFile StrFile { get; set; } = null!;
	public GeneticTestProviderInput TestProvider { get; set; } = null!;
	public bool LabTestRequested { get; set; }
	public Language Language { get; set; }
}