using Api.Auth.Dtos;

namespace Api.Auth.Registration.Inputs; 

public class RegisterWithLabTestInput {
	public RegistrationUserInput User { get; set; } = null!;
	public Language Language { get; set; }
}