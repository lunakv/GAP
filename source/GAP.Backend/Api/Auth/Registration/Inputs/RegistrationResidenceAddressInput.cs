using Users.UserAccess.Entities;

namespace Api.Auth.Registration.Inputs; 

public class RegistrationResidenceAddressInput {
	public string Town { get; set; } = null!;
	public string Municipality { get; set; } = null!;
	public string Street { get; set; } = null!;
	public string ZipCode { get; set; } = null!;

	public ExpandedAddress ToExpandedAddress() {
		return new ExpandedAddress(
			town: Town,
			municipality: Municipality,
			county: null,
			street: Street,
			zipCode: ZipCode
		);
	}
}