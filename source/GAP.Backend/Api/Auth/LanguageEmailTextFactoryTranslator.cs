using Api.Auth.Dtos;
using Auth.Emails.EmailTextFactories;

namespace Api.Auth; 

public class LanguageEmailTextFactoryTranslator {
	public static IEmailTextFactory GetEmailTextFactory(Language language) {
		return language switch {
			Language.English => new EnglishEmailTextFactory(),
			Language.Czech => new CzechEmailTextFactory(),
			_ => throw new ArgumentException($"Emails in language {language} are not supported.")
		};
	}
}