using Api.Auth.Dtos;
using Auth.Login;

namespace Api.Auth.Login.Types; 

public class LoginResultType : ObjectType<LoginResult> {
	private const string LoginResultTypeName = "LoginResult";
	private const string UserTypePropertyName = "userType";

	protected override void Configure(IObjectTypeDescriptor<LoginResult> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(LoginResultTypeName);

		descriptor.Field(loginResult => loginResult.UserId)
			.Type<NonNullType<IntType>>();
		descriptor.Field(loginResult => loginResult.Token)
			.Type<NonNullType<StringType>>();
		descriptor.Field(loginResult => loginResult.Role)
			.Name(UserTypePropertyName)
			.Resolve(ctx => TypeRoleTranslator.GetUserType(role: ctx.Parent<LoginResult>().Role))
			.Type<NonNullType<EnumType<UserType>>>();
	}
}