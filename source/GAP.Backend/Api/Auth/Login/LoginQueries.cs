using Api.Logging;
using Auth;
using Auth.Identity.Exceptions;
using Auth.Login;
using Auth.Policies;
using HotChocolate.AspNetCore.Authorization;
using Users.UserAccess.Exceptions;

namespace Api.Auth.Login;

[ExtendObjectType("Query")]
public class LoginQueries {
	public async Task<LoginResult> LoginByPassword(string userName, string password, [Service] LoginController loginController, [Service] EndpointLogger<LoginQueries> logger) {
		logger.LogPublicEndpointContext();
		logger.Logger.LogInformation("Request arguments: userName {UserName}.", userName);
		try {
			LoginResult loginResult = await loginController.LoginByPasswordAsync(userName: userName, password: password);
			logger.LogSuccessfulRequest();
			return loginResult;
		} catch (Exception ex) when (ex is AuthUserNotFoundException or IncorrectPasswordException) {
			throw new UserNameOrPasswordIncorrectException(message: "UserName or password incorrect, masking the real exception.", innerException: ex);
		}
	}
}