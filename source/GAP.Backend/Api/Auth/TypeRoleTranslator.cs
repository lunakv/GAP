using Api.Auth.Dtos;
using Auth.Policies;

namespace Api.Auth; 

public class TypeRoleTranslator {
	public static string GetRole(UserType userType) {
		return userType switch {
			UserType.Root => nameof(RootRole),
			UserType.Admin => nameof(AdminRole),
			UserType.LabTech => nameof(LabTechRole),
			UserType.PublicApiConsumer => nameof(PublicApiConsumerRole),
			_ => throw new ArgumentException($"Usertype {userType} could not be translated to any role.")
		};
	}
	
	public static UserType GetUserType(string role) {
		return role switch {
			nameof(RootRole) => UserType.Root,
			nameof(UserRole) => UserType.User,
			nameof(AdminRole) => UserType.Admin,
			nameof(LabTechRole) => UserType.LabTech,
			nameof(PublicApiConsumerRole) => UserType.PublicApiConsumer,
			_ => throw new ArgumentException($"Role {role} could not be translated to any UserType.")
		};
	}
}