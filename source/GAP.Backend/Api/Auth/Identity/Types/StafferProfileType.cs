using Auth.Identity.Entities;

namespace Api.Auth.Identity.Types; 

public class StafferProfileType : ObjectType<StafferProfile> {
	private const string StafferProfileTypeName = "AdminProfile";

	protected override void Configure(IObjectTypeDescriptor<StafferProfile> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(StafferProfileTypeName);

		descriptor.Field(stafferProfile => stafferProfile.Email)
			.Type<NonNullType<StringType>>();
	}
}