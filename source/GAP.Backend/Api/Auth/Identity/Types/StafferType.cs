using Api.Auth.Dtos;
using Auth.Identity.Entities;

namespace Api.Auth.Identity.Types;

public class StafferType : ObjectType<Staffer> {
	private const string StafferTypeName = "Admin";
	private const string StafferTypePropertyName = "adminType";

	protected override void Configure(IObjectTypeDescriptor<Staffer> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(StafferTypeName);

		descriptor.Field(staffer => staffer.Id)
			.Type<NonNullType<IntType>>();
		descriptor.Field(staffer => staffer.Profile)
			.Type<NonNullType<StafferProfileType>>();
		descriptor.Field(staffer => staffer.Role)
			.Name(StafferTypePropertyName)
			.Resolve(ctx => TypeRoleTranslator.GetUserType(ctx.Parent<Staffer>().Role))
			.Type<NonNullType<EnumType<UserType>>>();
	}
}