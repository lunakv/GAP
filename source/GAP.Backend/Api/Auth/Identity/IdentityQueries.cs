using Api.Auth.Dtos;
using Api.Logging;
using Auth.Identity;
using Auth.Identity.Entities;
using Auth.Policies;
using HotChocolate.AspNetCore.Authorization;

namespace Api.Auth.Identity;

[ExtendObjectType("Query")]
public class IdentityQueries {
	[Authorize(Policy = nameof(IsAuthUserThemselfPolicy))]
	public async Task<OperationResult> RequestEmailChange(int userId, string newEmail, Language language, [Service] IdentityController identityController,
		[Service] EndpointLogger<IdentityQueries> logger) {
		logger.LogAuthEndpointContext(aboutUserId: userId);
		logger.Logger.LogInformation("Request arguments: userId {UserId}, newEmail {NewEmail}.", userId, newEmail);
		await identityController.RequestEmailChangeAsync(
			userId: userId,
			newEmail: newEmail,
			emailTextFactory: LanguageEmailTextFactoryTranslator.GetEmailTextFactory(language: language)
		);
		logger.LogSuccessfulRequest();
		return new OperationResult();
	}

	public async Task<OperationResult> RequestPasswordReset(string email, Language language, [Service] IdentityController identityController,
		[Service] EndpointLogger<IdentityQueries> logger) {
		logger.LogPublicEndpointContext();
		logger.Logger.LogInformation("Request arguments: email {Email}", email);
		await identityController.RequestPasswordResetAsync(
			email: email,
			LanguageEmailTextFactoryTranslator.GetEmailTextFactory(language: language)
		);
		logger.LogSuccessfulRequest();
		return new OperationResult();
	}
	
	[Authorize(Policy = nameof(IsRootPolicy))]
	public async Task<ICollection<Staffer>> GetStaff([Service] IdentityController identityController,
		[Service] EndpointLogger<IdentityQueries> logger) {
		logger.LogAuthEndpointContext();
		logger.Logger.LogInformation("No request arguments.");
		ICollection<Staffer> staff =await identityController.GetStaffAsync();
		logger.LogSuccessfulRequest();
		return staff;
	}
}