using Api.Auth.Dtos;
using Api.Logging;
using Auth.Identity;
using Auth.Policies;
using HotChocolate.AspNetCore.Authorization;

namespace Api.Auth.Identity; 

[ExtendObjectType("Mutation")]
public class IdentityMutations {
	[Authorize(Policy = nameof(IsAuthUserThemselfPolicy))]
	public async Task<OperationResult> ChangePassword(int userId, string currentPassword, string newPassword, 
		[Service] IdentityController identityController, [Service] EndpointLogger<IdentityMutations> logger) {
		logger.LogAuthEndpointContext(aboutUserId: userId);
		logger.Logger.LogInformation("Request arguments: userId {UserId} and passwords.", userId);
		await identityController.ChangePasswordAsync(userId: userId, currentPassword: currentPassword, newPassword: newPassword);
		logger.LogSuccessfulRequest();
		return new OperationResult();
	}
	
	public async Task<OperationResult> ChangeEmail(int userId, string newEmail, string token, [Service] IdentityController identityController,
		[Service] EndpointLogger<IdentityMutations> logger) {
		logger.LogPublicEndpointContext();
		logger.Logger.LogInformation("Request arguments: userId {UserId} newEmail {NewEmail}, token {Token}.", userId, newEmail, token);
		await identityController.ChangeEmailAsync(userId: userId, newEmail: newEmail, token: token);
		logger.LogSuccessfulRequest();
		return new OperationResult();
	}
	
	public async Task<OperationResult> ResetPassword(int userId, string newPassword, string token, [Service] IdentityController identityController,
		[Service] EndpointLogger<IdentityMutations> logger) {
		logger.LogPublicEndpointContext();
		logger.Logger.LogInformation("Request arguments: userId {UserId}, token {Token}.", userId, token);
		await identityController.ResetPasswordAsync(userId: userId, newPassword: newPassword, token: token);
		logger.LogSuccessfulRequest();
		return new OperationResult();
	}
	
	[Authorize(Policy = nameof(IsRootPolicy))]
	public async Task<OperationResult> Deleteuser(string email, [Service] IdentityController identityController,
		[Service] EndpointLogger<IdentityMutations> logger) {
		logger.LogAuthEndpointContext();
		logger.Logger.LogInformation("Request arguments: email {Email}", email);
		await identityController.DeleteUserAsync(email: email);
		logger.LogSuccessfulRequest();
		return new OperationResult();
	}
}