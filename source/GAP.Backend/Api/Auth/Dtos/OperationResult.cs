namespace Api.Auth.Dtos; 

public class OperationResult {
	public bool OperationSuccessful { get; set; } = true;
}