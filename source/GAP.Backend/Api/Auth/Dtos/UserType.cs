namespace Api.Auth.Dtos; 

public enum UserType { Root, Admin, PublicApiConsumer, LabTech, User }