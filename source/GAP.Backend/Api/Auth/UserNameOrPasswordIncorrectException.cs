using Users.Exceptions;

namespace Api.Auth; 

public class UserNameOrPasswordIncorrectException : UserException {
	public UserNameOrPasswordIncorrectException(string message) : base(message) { }
	public UserNameOrPasswordIncorrectException(string message, Exception? innerException) : base(message, innerException) { }
	public override string GetRepresentation => nameof(UserNameOrPasswordIncorrectException);
}