using Users.Exceptions;

namespace Api.Exceptions; 

public class HttpContextNotAccessibleException : Exception {
	public HttpContextNotAccessibleException(string message) : base(message) {}
}