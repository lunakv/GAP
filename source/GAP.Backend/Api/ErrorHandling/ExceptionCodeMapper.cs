using System.Text.Json;
using Microsoft.Extensions.Options;

namespace Api.ErrorHandling;

public class ExceptionCodeMapper {
	private IReadOnlyDictionary<string, string>? _mapping;
	private readonly string _exceptionToCodeMappingFile;

	public ExceptionCodeMapper(IOptions<ErrorConfig> errorConfig) {
		_exceptionToCodeMappingFile = errorConfig.Value.ExceptionToCodeMappingFile;
		_mapping = null;
	}

	public async Task LoadMappingFileAsync() {
		await using var fs = new FileStream(_exceptionToCodeMappingFile, new FileStreamOptions() {
			Mode = FileMode.Open,
			Access = FileAccess.Read,
			Share = FileShare.Read,
			Options = FileOptions.Asynchronous
		});
		_mapping = await JsonSerializer.DeserializeAsync<Dictionary<string, string>>(utf8Json: fs);
	}

	public string MapToCode(string exception) {
		if (_mapping == null) {
			throw new ErrorHandlingException($"{nameof(ExceptionCodeMapper)}.{nameof(_mapping)} was not loaded.");
		}

		bool codeFound = _mapping.TryGetValue(key: exception, out string? code);
		if (!codeFound || code == null) {
			throw new ErrorHandlingException($"Code for {exception} was not found.");
		}

		return code;
	}
}