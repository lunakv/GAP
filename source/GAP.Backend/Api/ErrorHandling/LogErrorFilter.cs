using System.Text.Json;
using Microsoft.Extensions.Options;
using Users.Exceptions;

namespace Api.ErrorHandling;

/// <summary>
/// Provides error handling for thrown user exceptions (are client side fault, not server error).
/// It also logs all exception errors.
/// <exception cref="UserException">Exception representing client side faults</exception>
/// </summary>
public class LogErrorFilter : IErrorFilter {
	private readonly ExceptionCodeMapper _exceptionCodeMapper;
	private readonly ILogger<LogErrorFilter> _logger;

	public LogErrorFilter(ExceptionCodeMapper exceptionCodeMapper, ILogger<LogErrorFilter> logger) {
		_exceptionCodeMapper = exceptionCodeMapper;
		_logger = logger;
	}

	/// <summary>
	/// Maps <exception cref="UserException">exceptions</exception> to error codes which are understood by frontend.
	/// </summary>
	/// <param name="error">Detected error</param>
	/// <returns>Transformed error - in case of user exception, </returns>
	public IError OnError(IError error) {
		var errorBuilder = ErrorBuilder.FromError(error);
		if (error.Exception != null) {
			if (error.Exception is UserException exception) {
				try {
					string code = _exceptionCodeMapper.MapToCode(exception.GetRepresentation);
					errorBuilder.SetCode(code);
				} catch (Exception e) {
					_logger.LogCritical("Exception was thrown in ErrorHandling: {Message}, {@e}", e.Message, e);
				} 
			}
			_logger.LogError("Exception was thrown: {Exception} {@error}", error?.Exception?.Message, error);
		} else {
			_logger.LogError("Error found but there is no exception: {@error}", error);
		}

		return errorBuilder.Build();
	}
}