namespace Api.ErrorHandling; 

public class ErrorConfig {
	public string ExceptionToCodeMappingFile { get; set; }

	public ErrorConfig() {
		ExceptionToCodeMappingFile = string.Empty;
	}
}