namespace Api.ErrorHandling; 

public class ErrorHandlingException : Exception {
	public ErrorHandlingException(string message) : base(message) { }
}