var app = ServiceConfig.BuildApplication(args);
await ServiceConfig.ConfigureTree(app);
await ServiceConfig.LoadExceptionToCodeMappingFile(app);

// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapGraphQL("/");
app.MapControllers();

app.Run();
