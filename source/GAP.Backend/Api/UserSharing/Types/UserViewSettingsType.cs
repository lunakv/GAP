using Api.UserSharing.Resolvers;
using Users.UserSharing.UserViewSharing;

namespace Api.UserSharing.Types;

public class UserViewSettingsType : ObjectType<UserViewSettings> {
	private const string UserViewSettingsTypeName = "UserViewSettings";

	protected override void Configure(IObjectTypeDescriptor<UserViewSettings> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(UserViewSettingsTypeName);

		descriptor.Field(userViewSettings => userViewSettings.Viewers)
			.ResolveWith<UserViewSettingsResolvers>(r => r.GetViewersAsync(default!, default!))
			.Type<NonNullType<ListType<NonNullType<ShareUserType>>>>();
		descriptor.Field(userViewSettings => userViewSettings.ViewableUsers)
			.ResolveWith<UserViewSettingsResolvers>(r => r.GetViewableUsersAsync(default!, default!))
			.Type<NonNullType<ListType<NonNullType<ShareUserType>>>>();
	}
}