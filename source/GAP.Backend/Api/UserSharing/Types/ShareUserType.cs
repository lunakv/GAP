using Api.Users.Types.Public;
using Users.UserAccess.Entities;

namespace Api.UserSharing.Types; 

public class ShareUserType : ObjectType<User> {
	private const string ShareUserTypeName = "ShareUser";

	protected override void Configure(IObjectTypeDescriptor<User> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(ShareUserTypeName);

		descriptor.Field(user => user.Id)
			.Type<NonNullType<IntType>>();
		descriptor.Field(user => user.Profile)
			.Type<NonNullType<PublicProfileType>>();
	}
}