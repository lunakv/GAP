using Api.UserSharing.Resolvers;
using Users.UserSharing.UserManagementSharing;

namespace Api.UserSharing.Types;

public class UserManagementSettingsType : ObjectType<UserManagementSettings> {
	private const string UserManagementSettingsTypeName = "UserManagementSettings";

	protected override void Configure(IObjectTypeDescriptor<UserManagementSettings> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(UserManagementSettingsTypeName);

		descriptor.Field(userManagementSettings => userManagementSettings.Managers)
			.ResolveWith<UserManagementSettingsResolvers>(r => r.GetManagersAsync(default!, default!))
			.Type<NonNullType<ListType<NonNullType<ShareUserType>>>>();
		descriptor.Field(userManagementSettings => userManagementSettings.ManagedUsers)
			.ResolveWith<UserManagementSettingsResolvers>(r => r.GetManagedUsersAsync(default!, default!))
			.Type<NonNullType<ListType<NonNullType<ShareUserType>>>>();
	}
}