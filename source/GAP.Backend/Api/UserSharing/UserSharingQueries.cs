using Api.Logging;
using Auth.Policies;
using HotChocolate.AspNetCore.Authorization;
using Users.UserSharing.UserManagementSharing;
using Users.UserSharing.UserViewSharing;

namespace Api.UserSharing;

[ExtendObjectType("Query")]
public class UserSharingQueries {
	[Authorize(Policy = nameof(HasUserManageRightPolicy))]
	public async Task<UserManagementSettings> GetUserManagementSettings(int userId, [Service] UserManagementController userManagementController, [Service] EndpointLogger<UserSharingQueries> logger) {
		logger.LogAuthEndpointContext();
		logger.Logger.LogInformation("Request arguments: userId {UserId}", userId);
		var userManagementSettings = await userManagementController.GetUserManagementSettingsAsync(userId: userId);
		logger.LogSuccessfulRequest();
		return userManagementSettings;
	}

	[Authorize(Policy = nameof(HasUserManageRightPolicy))]
	public async Task<UserViewSettings> GetUserViewSettings(int userId, [Service] UserViewController userViewController, [Service] EndpointLogger<UserSharingQueries> logger) {
		logger.LogAuthEndpointContext();
		logger.Logger.LogInformation("Request arguments: userId {UserId}", userId);
		UserViewSettings userViewSettings = await userViewController.GetUserViewSettingsAsync(userId: userId);
		logger.LogSuccessfulRequest();
		return userViewSettings;
	}
}