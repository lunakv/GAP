using Api.Logging;
using Auth.Policies;
using HotChocolate.AspNetCore.Authorization;
using Users.UserSharing.UserManagementSharing;
using Users.UserSharing.UserViewSharing;

namespace Api.UserSharing;

[ExtendObjectType("Mutation")]
public class UserSharingMutations {
	[Authorize(Policy = nameof(IsAuthUserThemselfPolicy))]
	public async Task<UserManagementSettings> AddUserManager(int userId, string managerEmail, [Service] UserManagementController userManagementController, [Service] EndpointLogger<UserSharingMutations> logger) {
		logger.LogAuthEndpointContext(aboutUserId: userId);
		logger.Logger.LogInformation("Request arguments: userId {UserId}, managerEmail {ManagerEmail}", userId, managerEmail);
		var accountManagement = await userManagementController.AddUserManagerAsync(userId: userId, managerEmail: managerEmail);
		logger.LogSuccessfulRequest();
		return accountManagement;
	}

	[Authorize(Policy = nameof(IsAuthUserThemselfPolicy))]
	public async Task<UserManagementSettings> RemoveUserManager(int userId, int managerId, [Service] UserManagementController userManagementController, [Service] EndpointLogger<UserSharingMutations> logger) {
		logger.LogAuthEndpointContext(aboutUserId: userId);
		logger.Logger.LogInformation("Request arguments: userId {UserId}, managerId {ManagerId}", userId, managerId);
		var accountManagement = await userManagementController.RemoveUserManagerAsync(userId: userId, managerId: managerId);
		logger.LogSuccessfulRequest();
		return accountManagement;
	}

	[Authorize(Policy = nameof(IsAuthUserThemselfPolicy))]
	public async Task<UserViewSettings> AddUserViewer(int userId, string viewerEmail, [Service] UserViewController userViewController, [Service] EndpointLogger<UserSharingMutations> logger) {
		logger.LogAuthEndpointContext(aboutUserId: userId);
		logger.Logger.LogInformation("Request arguments: userId {UserId}, viewerEmail {ViewerEmail}", userId, viewerEmail);
		var userDataAccessSettings = await userViewController.AddUserViewerAsync(userId: userId, viewerEmail: viewerEmail).ConfigureAwait(false);
		logger.LogSuccessfulRequest();
		return userDataAccessSettings;
	}

	[Authorize(Policy = nameof(IsAuthUserThemselfPolicy))]
	public async Task<UserViewSettings> RemoveUserViewer(int userId, int viewerId, [Service] UserViewController userViewController, [Service] EndpointLogger<UserSharingMutations> logger) {
		logger.LogAuthEndpointContext(aboutUserId: userId);
		logger.Logger.LogInformation("Request arguments: userId {UserId}, viewerId {ViewerId}", userId, viewerId);
		var userDataAccessSettings = await userViewController.RemoveUserViewerAsync(userId: userId, viewerId: viewerId).ConfigureAwait(false);
		logger.LogSuccessfulRequest();
		return userDataAccessSettings;
	}
}