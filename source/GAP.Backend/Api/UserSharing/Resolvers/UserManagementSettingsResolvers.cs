using Api.Users;
using Api.Users.DataLoaders;
using Users.UserAccess.Entities;
using Users.UserSharing.UserManagementSharing;

namespace Api.UserSharing.Resolvers; 

public class UserManagementSettingsResolvers {
	public async Task<IReadOnlyList<User>> GetManagersAsync([Parent] UserManagementSettings userViewSettings, UserByIdDataLoader userByIdDataLoader) {
		return await userByIdDataLoader.LoadAsync(userViewSettings.Managers);
	}
	
	public async Task<IReadOnlyList<User>> GetManagedUsersAsync([Parent] UserManagementSettings userViewSettings, UserByIdDataLoader userByIdDataLoader) {
		return await userByIdDataLoader.LoadAsync(userViewSettings.ManagedUsers);
	}
}