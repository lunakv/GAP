using Api.Users;
using Api.Users.DataLoaders;
using Users.UserAccess.Entities;
using Users.UserSharing.UserViewSharing;

namespace Api.UserSharing.Resolvers; 

public class UserViewSettingsResolvers {
	public async Task<IReadOnlyList<User>> GetViewersAsync([Parent] UserViewSettings userViewSettings, UserByIdDataLoader userByIdDataLoader) {
		return await userByIdDataLoader.LoadAsync(userViewSettings.Viewers);
	}
	
	public async Task<IReadOnlyList<User>> GetViewableUsersAsync([Parent] UserViewSettings userViewSettings, UserByIdDataLoader userByIdDataLoader) {
		return await userByIdDataLoader.LoadAsync(userViewSettings.ViewableUsers);
	}
}