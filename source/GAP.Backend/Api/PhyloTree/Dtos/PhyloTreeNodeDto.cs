﻿using Api.Users.PublicApi;
using PhyloTree.DataClasses;
using Users.UserAccess.Entities;

namespace Api.PhyloTree.Dtos
{

    [GraphQLName("PhyloTreeNode")]
    public class PhyloTreeNodeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public int HaplogroupId { get; set; }
        public bool IsHaplogroupRoot { get; set; }
        public NumberOfPeopleDto PeopleCount { get; set; }
        public int ApproximateYear { get; set; }

        public PhyloTreeNodeDto(int id, string name, int? parentId, int haplogroupId, NumberOfPeople peopleCount, bool isHaplogroupRoot, int approximateYear)
            :this(id, name, parentId, haplogroupId, new NumberOfPeopleDto(peopleCount), isHaplogroupRoot, approximateYear) { }
        
        protected PhyloTreeNodeDto(int id, string name, int? parentId, int haplogroupId, NumberOfPeopleDto peopleCount, bool isHaplogroupRoot, int approximateYear) { 
            Id = id;
            Name = name;
            ParentId = parentId;
            HaplogroupId = haplogroupId;
            PeopleCount = peopleCount;
            IsHaplogroupRoot = isHaplogroupRoot;
            ApproximateYear = approximateYear;
        }

        public PhyloTreeNodeDto(PhyloUserNode node)
            : this(
                id: node.Id,
                name: node.Name,
                parentId: node.Parent,
                haplogroupId: node.HaplogroupId,
                peopleCount: node.PeopleCount,
                isHaplogroupRoot: node.IsHaplogroupRoot,
                approximateYear: node.ApproximateYear)
        { }

        public override string ToString()
        {
            return $"{nameof(PhyloTreeNodeDto)}:{Id}[{Name}]";
        }
    }
    
    [GraphQLName("PhyloTreeNode")]
    public class PublicPhyloTreeNodeDto<TUser> : PhyloTreeNodeDto
    {
        public IList<TUser> UsersSample { get; set; }

        public PublicPhyloTreeNodeDto(int id, string name, int? parentId, int haplogroupId, NumberOfPeople peopleCount, bool isHaplogroupRoot, int approximateYear, IList<CompleteUser> usersSample, Func<CompleteUser, TUser> castFunc)
            :this(id, name, parentId, haplogroupId, peopleCount, isHaplogroupRoot, approximateYear, usersSample.Select(u => castFunc(u)).ToList()) 
        {}
        public PublicPhyloTreeNodeDto(int id, string name, int? parentId, int haplogroupId, NumberOfPeople peopleCount, bool isHaplogroupRoot, int approximateYear, IList<TUser> usersSample) 
            :this(id, name, parentId, haplogroupId, new NumberOfPeopleDto(peopleCount), isHaplogroupRoot, approximateYear, usersSample)
        {}
        public PublicPhyloTreeNodeDto(int id, string name, int? parentId, int haplogroupId, NumberOfPeopleDto peopleCount, bool isHaplogroupRoot, int approximateYear, IList<TUser> usersSample) 
            :base(id, name, parentId, haplogroupId, peopleCount, isHaplogroupRoot, approximateYear){ 
            UsersSample = usersSample;
        }


        public PublicPhyloTreeNodeDto(PhyloUserNode node, Func<CompleteUser, TUser> castFunc)
            : this(
                id: node.Id,
                name: node.Name,
                parentId: node.Parent,
                haplogroupId: node.HaplogroupId,
                peopleCount: node.PeopleCount,
                isHaplogroupRoot: node.IsHaplogroupRoot,
                approximateYear: node.ApproximateYear,
                usersSample: node.PeopleSample,
                castFunc: castFunc)
        { }

        public override string ToString()
        {
            return $"{nameof(PublicPhyloTreeNodeDto<TUser>)}:{Id}[{Name}]";
        }
    }
}
