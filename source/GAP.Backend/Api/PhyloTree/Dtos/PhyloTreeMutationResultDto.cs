﻿namespace Api.PhyloTree.Dtos;

[GraphQLName("PhyloTreeMutationResult")]
public record PhyloTreeMutationResultDto(bool WasDone, PhyloTreeMutationMessage Message)
{
    public PhyloTreeMutationResultDto()
        : this(true, PhyloTreeMutationMessage.OK) { }
}

