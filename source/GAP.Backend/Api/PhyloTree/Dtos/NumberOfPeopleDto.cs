﻿using PhyloTree.DataClasses;

namespace Api.PhyloTree.Dtos; 

[GraphQLName("NumberOfPeople")]
public record NumberOfPeopleDto(int Inside, int InSubtree) {
	public NumberOfPeopleDto(NumberOfPeople original) 
		:this(original.Inside, original.InSubtree){}
}
