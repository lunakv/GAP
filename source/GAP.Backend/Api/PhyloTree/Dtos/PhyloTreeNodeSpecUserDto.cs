﻿using PhyloTree.DataClasses;
using Users.UserAccess.Entities;

namespace Api.PhyloTree.Dtos; 
	
[GraphQLName("PhyloTreeNodeSpecUser")]
public class PhyloTreeNodeSpecUserDto : PublicPhyloTreeNodeDto<CompleteUser>
{
    public bool IsSpecifiedUser { get; set; }

    public PhyloTreeNodeSpecUserDto(PhyloUserNode node, bool isSpecificUser)
        : base(node.Id, node.Name, node.Parent, node.HaplogroupId, node.PeopleCount, node.IsHaplogroupRoot, node.ApproximateYear, node.PeopleSample)
    {
        IsSpecifiedUser = isSpecificUser;
    }
}
