﻿using PhyloTree;
using Users.UserAccess;
using Api.PhyloTree.Dtos;
using Api.Logging;
using Auth.Policies;
using HotChocolate.AspNetCore.Authorization;
using Api.Users.PublicApi;
using Users.UserAccess.Entities;
using Users.GenData;
using Users.GenData.Haplogroup;
using Api.ClosestRelativeSearch.Types;
using Users.GenData.Haplogroup.Storage;
using Users.UserAccess.UserFilter;

namespace Api.PhyloTree;

[ExtendObjectType("Query")]
public class PhyloTreeQueries {
	private const int _defaultSize = 5;
	private const int _maximalSize = 10;


	[Authorize(Policy = nameof(HasPublicApiAccessPolicy))]
	public async Task<IList<PublicPhyloTreeNodeDto<PublicApiUserDto>>> GetFullTree(
		[Service] PhyloTreeController treeController,
		[Service] CompleteUserFetcher userFetcher,
		[Service] EndpointLogger<PhyloTreeQueries> logger,
		int sampleSize=_defaultSize) {

		logger.LogPublicEndpointContext();
		logger.Logger.LogInformation("Request arguments: sampleSize {SampleSize}", sampleSize);

		sampleSize = Math.Min(sampleSize, _maximalSize);
		var result = (await treeController.GetTreeAsync(userFetcher, sampleSize)).Nodes.Values
			.Select(n => new PublicPhyloTreeNodeDto<PublicApiUserDto>(n, u => new PublicApiUserDto(u)))
			.OrderBy(n => n.Name)
			.ToList();
		
		logger.LogSuccessfulRequest();
		
		return result;
	}
	
	[Authorize(Policy = nameof(HasUserViewRightPolicy))]
	public async Task<List<Profile>> GetUsersForNode(
		[Service] PhyloTreeController treeController,
		[Service] CompleteUserFetcher userFetcher,
		[Service] EndpointLogger<PhyloTreeQueries> logger,
		int userId, int nodeId) {

		logger.LogAuthEndpointContext();
		logger.Logger.LogInformation("Request arguments: userId {UserId} nodeId {NodeId}", userId, nodeId);

		var tree = await treeController.GetTreeAsync(userFetcher, int.MaxValue);
		var u = await userFetcher.FindCompleteUserByIdAsync(userId);
		if(! tree.AreSameBranch(u.GeneticData.Haplogroup.Id, tree.Nodes[nodeId])) {
			throw new WrongHaplogroupBranchException($"User {userId} is not eligable to get users in node {nodeId}. The node is outside of his haplogroup subbranch."); 
		}
		var result = tree.Nodes[nodeId].PeopleSample
			.Select(p => p.Profile)
			.ToList();

		logger.LogSuccessfulRequest();

		return result;
	}
	
    [Authorize(Policy = nameof(HasUserViewRightPolicy))]
	public async Task<IList<PhyloTreeNodeSpecUserDto>> GetFullTreeUserfied(
		[Service] PhyloTreeController treeController,
		[Service] CompleteUserFetcher userFetcher,
		[Service] EndpointLogger<PhyloTreeQueries> logger,
		int userId, 
		int sampleSize=_defaultSize) {
		
		logger.LogAuthEndpointContext(aboutUserId: userId);
		logger.Logger.LogInformation("Request arguments: userId {UserId} sampleSize {SampleSize}", userId, sampleSize);
        
		sampleSize = Math.Min(sampleSize, _maximalSize);
		var u = await userFetcher.FindCompleteUserByIdAsync(userId).ConfigureAwait(false);

		var result = (await treeController.GetTreeAsync(userFetcher, sampleSize)).Nodes.Values
			.Select(n => new PhyloTreeNodeSpecUserDto(n, n.HaplogroupId == u?.GeneticData?.Haplogroup.Id))
			.OrderBy(n => n.Name)
			.ToList();

		logger.LogSuccessfulRequest();
		return result;
	}


	public Task<IList<Haplogroup>> GetSubtreeForHaplogroup(
		[Service] PhyloTreeController treeController,
		[Service] IHaplogroupFetcher haploFetcher,
		[Service] EndpointLogger<PhyloTreeQueries> logger,
		int haplogroupId) {
		
		logger.LogPublicEndpointContext();
		logger.Logger.LogInformation("Request arguments: haplogroupId {HaplogroupId}", haplogroupId);
        
		var tree = treeController.GetTreeUserless();
		var haploIds = tree.FullSubtreeOfNode(tree.NodeForHaplogroup(haplogroupId))
			.Select(n => n.HaplogroupId)
			.ToList();
		var result = haploFetcher.GetHaplogroupsAsync(new IdHaplogroupFilter(haploIds));

		logger.LogSuccessfulRequest();

		return result;
	}
	
	public async Task<IList<Haplogroup>> GetAllMainHaplogroups(
		[Service] PhyloTreeController treeController,
		[Service] IHaplogroupFetcher haploFetcher,
		[Service] EndpointLogger<PhyloTreeQueries> logger) {
		
		logger.LogPublicEndpointContext();
        
		var tree = treeController.GetTreeUserless();
		var haploIds = tree.GetAllBranchRoots()
			.Select(n => n.HaplogroupId)
			.ToList();
		var result = await haploFetcher.GetHaplogroupsAsync(new IdHaplogroupFilter(haploIds))
			.ConfigureAwait(false);
		result = result.OrderBy(h => h.Name).ToList();

		logger.LogSuccessfulRequest();

		return result;
	}

	/// <summary>
	/// Which haplogroups are fixed as haplogroup roots.
	/// </summary>
	[Authorize(Policy = nameof(IsAdminPolicy))]
	public Task<List<HaploName>> GetFixedPhyloTreeBaseHaplos(
		[Service] PhyloTreeBaseHaplogroupManager baseHaploManager,
		[Service] EndpointLogger<PhyloTreeQueries> logger) {

		logger.LogAuthEndpointContext();
		var result = baseHaploManager.GetAdded()
			.Select(s => new HaploName(s))
			.ToList();
		logger.LogSuccessfulRequest();
		return Task.FromResult(result);
	}
	
	/// <summary>
	/// Which haplogroups, that suffice the haplogroup root selection algo, are ignored as non roots.
	/// </summary>
	[Authorize(Policy = nameof(IsAdminPolicy))]
	public Task<List<HaploName>> GetIgnoredPhyloTreeBaseHaplos(
		[Service] PhyloTreeBaseHaplogroupManager baseHaploManager,
		[Service] EndpointLogger<PhyloTreeQueries> logger) {

		logger.LogAuthEndpointContext();
		var result = baseHaploManager.GetIgnored()
			.Select(s => new HaploName(s))
			.ToList();
		logger.LogSuccessfulRequest();
		return Task.FromResult(result);
	}
	
	/// <summary>
	/// Which haplogroups from the database do not belong to any haplogroup in the phylo tree.
	/// </summary>
	[Authorize(Policy = nameof(IsAdminPolicy))]
	public async Task<List<HaploName>> GetHaplosOutOfTree(
		[Service] PhyloTreeController treeController,
		[Service] CompleteUserFetcher userFetcher,
		[Service] EndpointLogger<PhyloTreeQueries> logger) {
		logger.LogAuthEndpointContext();
		
		var all = await userFetcher.GetUsersAsync().ConfigureAwait(false);

		var inTree = (await treeController.GetTreeAsync(userFetcher, all.Count)).Nodes.Values
			.Select(n => n.PeopleSample)
			.SelectMany(x => x)
			.Select(u => u.Id)
			.ToList();

		var result = all
			.Where(u => !inTree.Contains(u.Id))
			.Select(u => u.GeneticData.Haplogroup.Name)
			.Distinct()
			.Select(h => new HaploName(h))
			.ToList();

		logger.LogSuccessfulRequest();

		return result;
	}
	
	/// <summary>
	/// Which users from the database do not belong to any haplogroup in the phylo tree.
	/// </summary>
	[Authorize(Policy = nameof(IsAdminPolicy))]
	public async Task<List<HaploStr>> GetUsersOutOfTree(
		[Service] PhyloTreeController treeController,
		[Service] CompleteUserFetcher userFetcher,
		[Service] EndpointLogger<PhyloTreeQueries> logger) {
		
		logger.LogAuthEndpointContext();
		
		var all = await userFetcher.GetUsersAsync().ConfigureAwait(false);

		var inTree = (await treeController.GetTreeAsync(userFetcher, all.Count)).Nodes.Values
			.Select(n => n.PeopleSample)
			.SelectMany(x => x)
			.Select(u => u.Id)
			.ToList();

		var result =  all
			.Where(u => !inTree.Contains(u.Id))
			.OrderBy(u => u.GeneticData.Haplogroup.Name)
			.Select(u => new HaploStr(
				u.GeneticData.Haplogroup.Name, 
				//not taking null values here, bacause the resulting list is to be viewed
				//by an educated worker and so it needs to be human readable
				u.GeneticData.StrMarkers.CloneAsList().Where(m => m.Value != 0).ToList())) 
			.ToList();

		logger.LogSuccessfulRequest();

		return result;
	}
	public record HaploName(string Haplogroup);
	public record HaploStr(string Haplogroup, List<StrMarkerCollection<string, int>.StrMarker> Strs);
	
	/// <summary>
	/// How many users from the database do not belong to any haplogroup in the phylo tree.
	/// </summary>
	[Authorize(Policy = nameof(IsAdminPolicy))]
	public async Task<Count> GetUsersOutOfTreeCount(
		[Service] PhyloTreeController treeController,
		[Service] CompleteUserFetcher userFetcher,
		[Service] EndpointLogger<PhyloTreeQueries> logger) {
		
		logger.LogAuthEndpointContext();

		var all = await userFetcher.GetUsersAsync().ConfigureAwait(false);

		var tree = (await treeController.GetTreeAsync(userFetcher, 5)).Nodes.Values
			.Select(n => n.PeopleCount.Inside)
			.Sum();

		logger.LogSuccessfulRequest();

		return new Count(all.Count - tree, all.Count, tree * 1.0f / all.Count);
	}

	public record Count(int value, int all, float ratio);
}



