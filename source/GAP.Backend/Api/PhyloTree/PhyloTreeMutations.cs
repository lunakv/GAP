﻿using PhyloTree;
using Users.UserAccess;
using PhyloTree.DataClasses;
using Api.PhyloTree.Dtos;
using Api.Logging;
using Users.GenData.Haplogroup.Storage;
using HotChocolate.AspNetCore.Authorization;
using Auth.Policies;

namespace Api.PhyloTree;

[ExtendObjectType("Mutation")]
public class PhyloTreeMutations {
	
    [Authorize(Policy = nameof(IsAdminPolicy))]
	public async Task<PhyloTreeMutationResultDto> Reprune([Service] PhyloTreeController treeController,
		[Service] IPhyloTreeFetcher<PhyloVariantedNode> treeGetter,
		[Service] IHaplogroupFetcher haploFetcher,
		[Service] IHaplogroupSaver haploSaver,
		[Service] CompleteUserFetcher userFetcher,
		[Service] PhyloTreeBaseHaplogroupManager baseHaploManager,
		[Service] EndpointLogger<PhyloTreeMutations> logger) {

		logger.LogAuthEndpointContext();
		var result = new PhyloTreeMutationResultDto();
		await treeController.RepruneAndQuantifyAsync(treeGetter, haploFetcher, haploSaver, userFetcher, baseHaploManager);
		logger.LogSuccessfulRequest();
		return result;
	}
	
    [Authorize(Policy = nameof(IsAdminPolicy))]
	public async Task<PhyloTreeMutationResultDto> DownloadWholeTree(
		[Service] PhyloTreeController treeController,
		[Service] FullTreeSaver fullTreeSaver,
		[Service] IPhyloTreeFetcher<PhyloVariantedNode> treeGetter,
		[Service] IHaplogroupFetcher haploFetcher,
		[Service] IHaplogroupSaver haploSaver,
		[Service] CompleteUserFetcher userFetcher,
		[Service] PhyloTreeBaseHaplogroupManager baseHaploManager,
		[Service] EndpointLogger<PhyloTreeMutations> logger) {
		
		logger.LogAuthEndpointContext();
		var b = await treeController.SaveFullTreeAsync(fullTreeSaver, 
			treeGetter, haploFetcher, haploSaver, userFetcher, baseHaploManager).ConfigureAwait(false);
		if(b) {
			logger.LogSuccessfulRequest();
			return new PhyloTreeMutationResultDto();
		} else {
			return new PhyloTreeMutationResultDto(false, PhyloTreeMutationMessage.Tree_Already_Downloading);
		}
	}
	
    [Authorize(Policy = nameof(IsAdminPolicy))]
	public Task<PhyloTreeMutationResultDto> AddIgnoredBaseHaplo(string haploName,
		[Service] PhyloTreeBaseHaplogroupManager baseHaploManager,
		[Service] EndpointLogger<PhyloTreeQueries> logger) {

		logger.LogAuthEndpointContext();

		baseHaploManager.AddIgnored(haploName);

		logger.LogSuccessfulRequest();
		return Task.FromResult(new PhyloTreeMutationResultDto());
	}
	
    [Authorize(Policy = nameof(IsAdminPolicy))]
	public Task<PhyloTreeMutationResultDto> AddFixedBaseHaplo(string haploName,
		[Service] PhyloTreeBaseHaplogroupManager baseHaploManager,
		[Service] EndpointLogger<PhyloTreeQueries> logger) {

		logger.LogAuthEndpointContext();

		baseHaploManager.AddAdded(haploName);

		logger.LogSuccessfulRequest();
		return Task.FromResult(new PhyloTreeMutationResultDto());
	}
	
    [Authorize(Policy = nameof(IsAdminPolicy))]
	public Task<PhyloTreeMutationResultDto> RemoveIgnoredBaseHaplo(string haploName,
		[Service] PhyloTreeBaseHaplogroupManager baseHaploManager,
		[Service] EndpointLogger<PhyloTreeQueries> logger) {

		logger.LogAuthEndpointContext();

		baseHaploManager.RemoveIgnored(haploName);

		logger.LogSuccessfulRequest();
		return Task.FromResult(new PhyloTreeMutationResultDto());
	}
	
    [Authorize(Policy = nameof(IsAdminPolicy))]
	public Task<PhyloTreeMutationResultDto> RemoveFixedBaseHaplo(string haploName,
		[Service] PhyloTreeBaseHaplogroupManager baseHaploManager,
		[Service] EndpointLogger<PhyloTreeQueries> logger) {

		logger.LogAuthEndpointContext();

		baseHaploManager.RemoveAdded(haploName);

		logger.LogSuccessfulRequest();
		return Task.FromResult(new PhyloTreeMutationResultDto());
	}
}
