﻿namespace Api.PhyloTree; 
public enum PhyloTreeMutationMessage {
	OK,
	Tree_Already_Downloading,
	Error
}
