using System.Text;
using Api.Auth.Identity.Types;
using Api.Auth.Login.Types;
using Api.Auth.Registration;
using Api.Auth.Registration.Types;
using Api.ClosestRelativeSearch;
using Api.ClosestRelativeSearch.Types;
using Api.ErrorHandling;
using Api.GenData;
using Api.GenData.DataLoaders;
using Api.GenData.Inputs;
using Api.GenData.Payloads;
using Api.GenData.Types;
using Api.GenData.Types.Filtering;
using Api.Laboratory;
using Api.Laboratory.KitManagement;
using Api.Laboratory.KitManagement.Types;
using Api.Laboratory.KitManagement.Types.Filtering;
using Api.Laboratory.LabResults.Types;
using Api.Laboratory.SequencerInputGeneration.Types;
using Api.Logging;
using Api.Logging.Types;
using Api.Map;
using Api.Map.Types;
using Api.Users;
using Auth;
using Auth.Policies;
using AuthStorageGateway;
using Laboratory;
using Api.Users.DataLoaders;
using Api.Users.Types;
using Api.Users.Types.Filtering;
using Api.Users.Types.Public;
using Api.UserSharing.Types;
using HotChocolate.Types.Pagination;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using PostgreSqlStorage.MapGateway.Accessors;
using PostgreSqlStorage.UsersGateway;
using PostgreSqlStorage.UsersGateway.GenData.Accessors;
using PostgreSqlStorage.UsersGateway.UserAccess.Accessors;
using PostgreSqlStorage.UsersGateway.UserSharing.Accessors;
using Serilog;
using Users.GenData;
using Users.GenData.Haplogroup;
using Users.UserAccess.Entities;
using Users.UserAccess.Entities.Interfaces;
using LogsContext = PostgreSqlStorage.LoggingGateway.LogsContext;
using Api.FamilyTree;
using Auth.Emails;
using Auth.Identity.Claims;
using Auth.Identity.Storage;
using Auth.Login;
using Auth.Token;
using ILogger = Serilog.ILogger;
using PhyloTree;

/// <summary>
/// Class for registering all services to DI and application configuration.
/// </summary>
public static class ServiceConfig {
	public static WebApplication BuildApplication(string[] args) {
		var builder = WebApplication.CreateBuilder(args);
		
		// Add services to the container.
		builder.Services.AddDbContext<PostgreSqlStorage.Data.GapContext>(optionsAction: options =>
				options.UseNpgsql(builder.Configuration.GetConnectionString(nameof(PostgreSqlStorage.Data.GapContext))),
			contextLifetime: ServiceLifetime.Transient
		);

		builder.Services.AddDbContext<AuthStorageGateway.AuthContext>(optionsAction: options =>
				options.UseNpgsql(builder.Configuration.GetConnectionString(nameof(AuthStorageGateway.AuthContext))),
			contextLifetime: ServiceLifetime.Scoped
		);

		builder.Services.AddDbContext<LogsContext>(optionsAction: options =>
				options.UseNpgsql(builder.Configuration.GetConnectionString(nameof(LogsContext))),
			contextLifetime: ServiceLifetime.Transient
		);

		builder.Services
			.AddIdentity<Auth.Identity.Entities.User, IdentityRole<int>>()
			.AddEntityFrameworkStores<AuthStorageGateway.AuthContext>()
			.AddTokenProvider<DataProtectorTokenProvider<Auth.Identity.Entities.User>>(TokenOptions.DefaultProvider);

		builder.Services.Configure<IdentityOptions>(options => {
			options.User.RequireUniqueEmail = true;

			options.SignIn.RequireConfirmedEmail = true;

			options.Password.RequireDigit = false;
			options.Password.RequiredLength = 9;
			options.Password.RequireLowercase = false;
			options.Password.RequireNonAlphanumeric = false;
			options.Password.RequireUppercase = false;
			options.Password.RequiredUniqueChars = 1;
		});

		builder.Services.AddSingleton<Users.UsersConfig>();
		builder.Services.Configure<Users.UsersConfig>(builder.Configuration.GetSection(nameof(Users.UsersConfig)));
		builder.Services.AddSingleton<PhyloTree.PhyloTreeConfig>();
		builder.Services.Configure<PhyloTree.PhyloTreeConfig>(builder.Configuration.GetSection(nameof(PhyloTree.PhyloTreeConfig)));
		builder.Services.Configure<Auth.AuthConfig>(builder.Configuration.GetSection(nameof(Auth.AuthConfig)));
		builder.Services.Configure<EmailConfig>(builder.Configuration.GetSection(nameof(EmailConfig)));
		builder.Services.Configure<Auth.Registration.EmailConfirmationConfig>(
			builder.Configuration.GetSection(nameof(Auth.AuthConfig))
				.GetSection(Auth.Registration.RegistrationConfig.Name)
				.GetSection(Auth.Registration.EmailConfirmationConfig.Name)
		);
		builder.Services.Configure<Auth.Identity.EmailChangeConfig>(
			builder.Configuration.GetSection(nameof(Auth.AuthConfig))
				.GetSection(Auth.Identity.IdentityConfig.Name)
				.GetSection(Auth.Identity.EmailChangeConfig.Name)
		);
		builder.Services.Configure<Auth.Identity.PasswordResetConfig>(
			builder.Configuration.GetSection(nameof(Auth.AuthConfig))
				.GetSection(Auth.Identity.IdentityConfig.Name)
				.GetSection(Auth.Identity.PasswordResetConfig.Name)
		);
		builder.Services.Configure<Auth.Registration.RegistrationConfig>(
			builder.Configuration.GetSection(nameof(Auth.AuthConfig)).GetSection(Auth.Registration.RegistrationConfig.Name)
		);
		builder.Services.Configure<Logging.LogConfig>(builder.Configuration.GetSection(nameof(Logging.LogConfig)));
		builder.Services.Configure<Laboratory.LaboratoryConfig>(builder.Configuration.GetSection(nameof(Laboratory.LaboratoryConfig)));
		builder.Services.Configure<ErrorConfig>(builder.Configuration.GetSection(nameof(ErrorConfig)));

		builder.Host.UseSerilog((context, configuration) => { configuration.ReadFrom.Configuration(context.Configuration); });
        builder.Services.Configure<IdManagement.IdGenerationConfig>(builder.Configuration.GetSection(nameof(IdManagement.IdGenerationConfig)));
        builder.Services.AddSingleton<IdManagement.IdGenerator>();


		builder.Services.AddHttpContextAccessor();

		builder.Services.AddSingleton<ExceptionCodeMapper>();

		builder.Services.AddTransient(typeof(Api.Logging.EndpointLogger<>));
		builder.Services.AddTransient<Logging.LoggingController>();
		builder.Services.AddErrorFilter<Api.ErrorHandling.LogErrorFilter>();
		builder.Services.AddTransient<Logging.Storage.ILogFetcher, PostgreSqlStorage.LoggingGateway.JsonLogFetcher>();

		builder.Services.AddScoped<MarkerSorter>();

		builder.Services.AddTransient<Users.UserAccess.Storage.IUserFetcher, UserFetcher>();
		builder.Services.AddTransient<Users.UserAccess.Storage.IUserSaver, UserSaver>();
		builder.Services.AddTransient<Users.UserAccess.PublicIds.IPublicUserIdGenerator, Users.UserAccess.PublicIds.GuidPublicUserIdGenerator>();
		builder.Services.AddTransient<Users.UserAccess.CompleteUserFetcher>();
		builder.Services.AddTransient<Users.UserAccess.UserController>();
		builder.Services.AddTransient<Users.GenData.Haplogroup.Storage.IHaplogroupFetcher, HaplogroupFetcher>();
		builder.Services.AddTransient<Users.GenData.Haplogroup.Storage.IHaplogroupSaver, HaplogroupSaver>();
		builder.Services.AddTransient<Map.MapFetcher.Storage.IRegionLayerFetcher, RegionLayerFetcher>();
		builder.Services.AddTransient<Map.MapFetcher.Storage.IRegionFetcher, RegionFetcher>();
		builder.Services.AddTransient<Map.RegionDataSaver.IAdministrativeRegionLayerSaver, Map.RegionDataSaver.AdministrativeRegionLayerSaver>();
		builder.Services.AddTransient<Map.RegionDataSaver.IAdministrativeRegionLayerStorageSaver, PostgreSqlStorage.MapGateway.Accessors.AdministrativeRegionLayerStorageSaver>();

		builder.Services.AddTransient<Map.MapController>();
		builder.Services.AddTransient<Map.MapFetcher.LayerMapFetcher>();
		builder.Services.AddTransient<Map.MapFetcher.RegionMapFetcher>();
		builder.Services.AddTransient<Map.MapFetcher.MapUserFetcher>();
		if (bool.Parse(builder.Configuration
		    .GetSection(nameof(Users.UsersConfig))
		    .GetSection(nameof(Users.GenData.GeneticDataConfig)).GetSection("UseNevGenPredictor").Value)) {
			builder.Services.AddTransient<Users.HaplogroupPrediction.IHaplogroupPredictor, Users.HaplogroupPrediction.NevgenHaplogroupPredictor>();
		} else {
			builder.Services.AddTransient<Users.HaplogroupPrediction.IHaplogroupPredictor, Users.HaplogroupPrediction.RandomHaplogroupPredictor>();
		}
		builder.Services.AddTransient<Users.ClosestRelativeSearch.ClosestRelativeSearchController>();

		builder.Services.AddTransient<Users.GenData.GeneticDataUploadController>();
		builder.Services.AddTransient<Users.GenData.GeneticDataUploader>();
		builder.Services.AddTransient<Users.GenData.Conflict.ConflictController>();
		builder.Services.AddTransient<Users.ISaverFactory, SaverFactory>();
		builder.Services.AddTransient<Users.GenData.OriginalStrDataSources.Storage.IOriginalStrDataSourceFetcher, OriginalStrDataSourceFetcher>();
		builder.Services.AddTransient<Users.GenData.OriginalStrDataSources.Storage.File.IOriginalStrFileSaver, Users.GenData.OriginalStrDataSources.Storage.File.OriginalStrFileSaver>();
		builder.Services.AddTransient<Users.GenData.OriginalStrDataSources.Storage.File.IOriginalStrFileFetcher, Users.GenData.OriginalStrDataSources.Storage.File.OriginalStrFileFetcher>();

		builder.Services.AddTransient<Users.GenData.Conflict.Storage.IStrConflictFetcher, StrConflictFetcher>();

		builder.Services.AddSingleton<PhyloTree.PhyloTreeController>();
		builder.Services.AddTransient<PhyloTree.IPhyloTreeSaver<PhyloTree.DataClasses.PhyloVariantedNode>, PhyloTree.Access.PhyloTreeOriginalJsonProcessor>();
		builder.Services.AddTransient<PhyloTree.IPhyloTreeFetcher<PhyloTree.DataClasses.PhyloVariantedNode>, PhyloTree.Access.PhyloTreeOriginalJsonProcessor>();
		builder.Services.AddTransient<PhyloTree.IPhyloTreeDownloader, PhyloTree.PhyloTreeWebDownloader>();
		builder.Services.AddTransient<PhyloTree.IPhyloTreeWebParser, PhyloTree.FtDnaTreeParser>();
		builder.Services.AddTransient<PhyloTree.FullTreeSaver>();
		builder.Services.AddTransient<PhyloTree.PhyloTreeBaseHaplogroupManager>();

		builder.Services.AddTransient<Users.UserSharing.UserManagementSharing.UserManagementController>();
		builder.Services.AddTransient<Users.UserSharing.UserManagementSharing.Storage.IUserManagementSettingsFetcher, UserManagementSettingsFetcher>();
		builder.Services.AddTransient<Users.UserSharing.UserManagementSharing.Storage.IUserManageRightSaver, UserManageRightSaver>();

		builder.Services.AddTransient<Users.UserSharing.UserViewSharing.UserViewController>();
		builder.Services.AddTransient<Users.UserSharing.UserViewSharing.Storage.IUserViewSettingsFetcher, UserViewSettingsFetcher>();
		builder.Services.AddTransient<Users.UserSharing.UserViewSharing.Storage.IUserViewRightSaver, UserViewRightSaver>();

		builder.Services.AddTransient<Laboratory.LabelGeneration.LabelController>();
		builder.Services.AddTransient<Laboratory.KitManagement.Storage.IKitFetcher<IHasUserId>, PostgreSqlStorage.LaboratoryGateway.KitManagement.Accessors.KitFetcher>();
		builder.Services.AddTransient<Laboratory.KitManagement.Storage.IKitFetcher<User>, Laboratory.KitManagement.Storage.FullUserKitFetcher>();
		builder.Services.AddTransient<Laboratory.KitManagement.Storage.IKitSaver<IHasUserId>, PostgreSqlStorage.LaboratoryGateway.KitManagement.Accessors.KitSaver>();
		builder.Services.AddTransient<Laboratory.KitManagement.KitController>();
		builder.Services.AddTransient<Laboratory.SequencerInputGeneration.SequencerInputController>();
		builder.Services.AddTransient<Laboratory.SequencerInputGeneration.Storage.ISequencerInputFetcher, PostgreSqlStorage.LaboratoryGateway.LabResults.Accessors.SequencerInputFetcher>();
		builder.Services.AddTransient<Laboratory.LabResults.LabResultsController>();


		builder.Services.AddTransient<Auth.Registration.Registrar>();
		builder.Services.AddTransient<Auth.Registration.RegistrationController>();
		builder.Services.AddTransient<Users.UserAccess.IMunicipalityMapper, Auth.Registration.MunicipalityMapper>();
		builder.Services.AddTransient<Auth.Registration.Storage.IRegistrationDataFetcher, PostgreSqlStorage.RegistrationGateway.Accessors.RegistrationDataFetcher>();
		builder.Services.AddTransient<Auth.Registration.Storage.IRegistrationDataSaver, PostgreSqlStorage.RegistrationGateway.Accessors.RegistrationDataSaver>();
		builder.Services.AddTransient<IEmailSender, EmailSender>();
		builder.Services.AddTransient<EmailFactory>();
		builder.Services.AddTransient<LoginController>();
		builder.Services.AddTransient<AccessChecker>();
		builder.Services.AddTransient<Auth.Identity.IdentityController>();
		builder.Services.AddTransient<IIdentityManager, LocalIdentityManager>();
		builder.Services.AddTransient<ClaimCreator>();
		builder.Services.AddTransient<IAuthUserFetcher, AuthStorageGateway.AuthUserFetcher>();
		builder.Services.AddTransient<ITokenCreator, JwtTokenCreator>();

		builder.Services.AddTransient<FamilyTree.Access.IFamilyTreeFetcher, FamilyTree.Access.FamilyTreeController>();
		builder.Services.AddTransient<FamilyTree.Access.IFamilyTreeSaver, FamilyTree.Access.FamilyTreeController>();
		builder.Services.AddTransient<PostgreSqlStorage.FamilyTreeStorageGateway.FamilyTreeStorageFetcher>();
		builder.Services.AddTransient<PostgreSqlStorage.FamilyTreeStorageGateway.FamilyTreeStorageSaver>();

		builder.Services
			.AddGraphQLServer()
			.SetPagingOptions(new PagingOptions() {
				MaxPageSize = int.MaxValue,
				IncludeTotalCount = true
			})
			// .ModifyOptions(options => {
			//     options.DefaultBindingBehavior = BindingBehavior.Explicit;
			// })
			.AddAuthorization()
			.AddQueryType(d => d.Name("Query"))
			.AddTypeExtension<UserQueries>()
			.AddTypeExtension<HaplogroupQueries>()
			.AddTypeExtension<MapQueries>()
			.AddTypeExtension<ClosestRelativesQueries>()
			.AddTypeExtension<Api.Auth.Login.LoginQueries>()
			.AddTypeExtension<Api.Auth.Identity.IdentityQueries>()
			.AddTypeExtension<ConflictQueries>()
			.AddTypeExtension<Api.Laboratory.KitManagement.KitQueries>()
			.AddTypeExtension<Api.Laboratory.SequencerInputGeneration.SequencerInputQueries>()
			.AddTypeExtension<Api.Laboratory.LabelGeneration.LayoutQueries>()
			.AddTypeExtension<Api.Logging.LoggingQueries>()
			.AddTypeExtension<Api.PhyloTree.PhyloTreeQueries>()
			.AddTypeExtension<Api.UserSharing.UserSharingQueries>()
			.AddTypeExtension<FamilyTreeQueries>()
			.AddTypeExtension<Api.Users.PublicApi.PublicUserQueries>()
			.AddType<UploadType>()
			.AddType<MarkerSetType>()
			.AddType<RelativeSearchResultType>()
			.AddType<RequestLogType>()
			.AddType<LogUserType>()
			.AddType<KitType>()
			.AddType<KitEventEnumType>()
			.AddType<RegistrationResultType>()
			.AddType<GeneticDataType>()
			.AddType<OriginalStrDataSourceType>()
			.AddType<StrConflictType>()
			.AddType<FixConflictPayload>()
			.AddType<HaplogroupType>()
			.AddType<UserType>()
			.AddType<StafferType>()
			.AddType<PublicCompleteUserType>()
			.AddType<LoginResultType>()
			.AddType<PublicUserType>()
			.AddType<AddressType>()
			.AddType<AdministratorDataType>()
			.AddType<AncestorType>()
			.AddType<ExpandedAddressType>()
			.AddType<ProfileType>()
			.AddType<KitFilterInput>()
			.AddType<LabStrFileUploadResultType>()
			.AddType<StageFilterInput>()
			.AddType<SequencerInputType>()
			.AddType<FirstTableRowType>()
			.AddType<SecondTableRowType>()
			.AddType<MapRegionType>()
			.AddType<AdministrativeRegionType>()
			.AddType<MapRegionLayerType>()
			.AddType<FamilyNameCountType>()
			.AddType<HaplogroupFamilyNameCountsType>()
			.AddType<HaplogroupCountType>()
			.AddType<UserViewSettingsType>()
			.AddType<UserManagementSettingsType>()
			.AddDataLoader<UserByIdDataLoader>()
			.AddDataLoader<StafferByIdDataloader>()
			.AddDataLoader<OriginalStrDataSourceByIdDataLoader>()
			.AddDataLoader<StrConflictByUserIdDataLoader>()
			.AddDataLoader<NullableStrConflictByUserIdDataLoader>()
			.AddMutationType(d => d.Name("Mutation"))
			.AddTypeExtension<UserMutations>()
			.AddTypeExtension<RegistrationMutations>()
			.AddTypeExtension<Api.Auth.Identity.IdentityMutations>()
			.AddTypeExtension<ConflictMutations>()
			.AddTypeExtension<UploadGeneticDataMutations>()
			.AddTypeExtension<Api.Laboratory.KitManagement.KitMutations>()
			.AddTypeExtension<Api.Laboratory.LabResults.LabResultsMutations>()
			.AddTypeExtension<Api.PhyloTree.PhyloTreeMutations>()
			.AddTypeExtension<Api.UserSharing.UserSharingMutations>()
			.AddTypeExtension<FamilyTreeMutations>()
			.AddFiltering()
			.AddSorting();

		builder.Services.AddAuthentication(options => {
			options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
			options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
		}).AddJwtBearer(options => {
			options.SaveToken = true;
			options.TokenValidationParameters = JwtTokenCreator.GetTokenValidationParameters(
				issuerSigningKey: builder.Configuration["AuthConfig:SymmetricKey"]
			);
		});

		builder.Services.AddAuthorization(options => { PolicyRegistrar.RegisterPolicies(options: options); });

		builder.Services.AddSingleton<IAuthorizationHandler, Auth.Policies.AuthorizationHandlers.UserThemselfHandler>();
		builder.Services.AddTransient<IAuthorizationHandler, Auth.Policies.AuthorizationHandlers.UserManagerHandler>();
		builder.Services.AddTransient<IAuthorizationHandler, Auth.Policies.AuthorizationHandlers.UserViewerHandler>();
		builder.Services.AddSingleton<IAuthorizationHandler, Auth.Policies.AuthorizationHandlers.AdminHandler>();
		builder.Services.AddSingleton<IAuthorizationHandler, Auth.Policies.AuthorizationHandlers.RootHandler>();
		builder.Services.AddSingleton<IAuthorizationHandler, Auth.Policies.AuthorizationHandlers.LabTechHandler>();
		builder.Services.AddSingleton<IAuthorizationHandler, Auth.Policies.AuthorizationHandlers.UserHasSignedAdministrationAgreementHandler>();

		builder.Services.AddControllers();
		var app = builder.Build();

		return app;
	}

	public static async Task ConfigureTree(WebApplication app) {
		var treeController = app.Services.GetRequiredService<PhyloTree.PhyloTreeController>();
		using (var scope = app.Services.CreateScope()) {
			var treeGetter = scope.ServiceProvider.GetRequiredService<PhyloTree.IPhyloTreeFetcher<PhyloTree.DataClasses.PhyloVariantedNode>>();
			var haploFetcher = scope.ServiceProvider.GetRequiredService<Users.GenData.Haplogroup.Storage.IHaplogroupFetcher>();
			var haploSaver = scope.ServiceProvider.GetRequiredService<Users.GenData.Haplogroup.Storage.IHaplogroupSaver>();
			var userFetcher = scope.ServiceProvider.GetRequiredService<Users.UserAccess.CompleteUserFetcher>();
			var baseHaploManager = scope.ServiceProvider.GetRequiredService<PhyloTreeBaseHaplogroupManager>();

			try {
				await treeController.RepruneAndQuantifyAsync(treeGetter, haploFetcher, haploSaver, userFetcher, baseHaploManager);
			} catch (Exception ex) {
				ILogger logger = scope.ServiceProvider.GetRequiredService<ILogger>();
				logger.Warning("Phylogenetic tree could not be pruned on application start: {@Exception}.", ex);
				if (!app.Environment.IsDevelopment()) {
					throw;
				}
			}
		}
	}

	public static async Task LoadExceptionToCodeMappingFile(WebApplication app) {
		var exceptionCodeMapper = app.Services.GetRequiredService<Api.ErrorHandling.ExceptionCodeMapper>();
		await exceptionCodeMapper.LoadMappingFileAsync();
	}
}
