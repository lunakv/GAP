using Api.Logging;
using Auth.Policies;
using Users.ClosestRelativeSearch;
using HotChocolate.AspNetCore.Authorization;
using PhyloTree;
using PhyloTree.Exceptions;
using Users.ClosestRelativeSearch.MarkerSets;
using Users.UserAccess;
using Users.UserAccess.Entities;
using Api.ClosestRelativeSearch.Types;
using PostgreSqlStorage.UsersGateway.UserAccess.Accessors;
using Microsoft.EntityFrameworkCore;
using Users.UserAccess.Storage;
using PhyloTree.DataClasses;

namespace Api.ClosestRelativeSearch;

[ExtendObjectType("Query")]
public class ClosestRelativesQueries {
	/// <summary>
	/// Finds the closest relatives with given filters.
	/// </summary>
	/// <param name="userId">Closest relatives of this user will be found.</param>
	/// <param name="markerSetName">Within which markers do we try to find the relatives.</param>
	/// <param name="forHaplogroup">If set, only users with the given haplogroup id will be searched.</param>
	/// <param name="isSubtree">When for haplogroup has value and this is set to true, all users in subtree (within phylo tree) of given haplogroup will be searched.</param>
	/// <param name="ownFamilyNameFilter">When true, only people of the same family name will be searched.</param>
	[Authorize(Policy = nameof(HasUserViewRightPolicy))]
	public async Task<ClosestRelativeSearchResult<CompleteUser>> GetClosestStrRelatives(int userId, string markerSetName, int? forHaplogroup, bool? isSubtree, bool? ownFamilyNameFilter,
		[Service] ClosestRelativeSearchController closestRelativeSearchController, [Service] PhyloTreeController phyloTreeController, [Service] CompleteUserFetcher userFetcher, [Service] EndpointLogger<ClosestRelativesQueries> logger) {
		logger.LogAuthEndpointContext();
		logger.Logger.LogInformation("Request arguments: userId {UserId}, markerSetName {MarkerSetName}, forHaplogroup {ForHaplogroup}, isSubtree {IsSubtree}, ownFamilyNameFilter {OwnFamilyNameFilter}",
			userId, markerSetName, forHaplogroup, isSubtree, ownFamilyNameFilter
		);
		var searchResult = await closestRelativeSearchController.GetClosestStrRelativesAsync(
			userId: userId,
			markerSetName: markerSetName,
			ownFamilyNameFilter: ownFamilyNameFilter ?? false,
			onlyGivenHaplos: await HaploIdsSubtreeManager(userId, forHaplogroup, isSubtree, phyloTreeController, userFetcher)
		);

		logger.LogSuccessfulRequest();
		return searchResult;
	}

	public async Task<IList<MarkerSet>> ListAvailableMarkerSets([Service] ClosestRelativeSearchController closestRelativeSearchController, [Service] EndpointLogger<ClosestRelativesQueries> logger) {
		logger.LogPublicEndpointContext();
		var markerSets = await closestRelativeSearchController.ListAvailableMarkerSetsAsync();
		logger.LogSuccessfulRequest();
		return markerSets;
	}
	
	[Authorize(Policy = nameof(HasUserViewRightPolicy))]
	public async Task<MarkerSet> GetMarkerSet(string name, [Service] ClosestRelativeSearchController closestRelativeSearchController, [Service] EndpointLogger<ClosestRelativesQueries> logger) {
		logger.LogPublicEndpointContext();
		logger.Logger.LogInformation("Request arguments: name {Name}", name);
		var markerSet = await closestRelativeSearchController.GetMarkerSetAsync(name);
		logger.LogSuccessfulRequest();
		return markerSet;
	}
	
	/// <summary>
	/// Determines all of the haplogroups that the closest relatives search should go through. Returns null if all haplogroups should be considered.
	/// </summary>
	/// <param name="userId">Id of the user requesting relatives search.</param>
	/// <param name="haplogId">The id of the haplogroup the user is searching for.</param>
	/// <param name="isSubtree">Whether the search should include all haplogroups in the phylo subtree of the given haplogroup.</param>
	/// <returns></returns>
	/// <exception cref="WrongHaplogroupBranchException">Thrown when the user's haplogroup is within different branch then the requested <paramref name="haplogId"/>.</exception>
	/// <exception cref="HaplogroupNotInPhyloTreeException">Thrown when the requested <paramref name="haplogId"/> is not within the phylo tree and subtree was selected.</exception>
	private async Task<int[]?> HaploIdsSubtreeManager(int userId, int? haplogId, bool? isSubtree, PhyloTreeController phyloTreeController, CompleteUserFetcher userFetcher)  {

		if(!haplogId.HasValue) {
			return null;
		} 
		var u = await userFetcher.FindCompleteUserByIdAsync(userId);
		var tree = phyloTreeController.GetTreeUserless();
		bool eligable = tree.AreSameBranch(u.GeneticData.Haplogroup.Id, haplogId.Value);
		if(!eligable) {
			throw new WrongHaplogroupBranchException($"Selected haplogroup (id: {haplogId}) is not in the same subbranch as the user's haplogroup (id: {u.GeneticData.Haplogroup.Id})");
		}

		if(!isSubtree.HasValue || !isSubtree.Value) {
			return new int[] { haplogId.Value };
		}

		var start = tree.NodeForHaplogroup(haplogId.Value);
		var root = tree.HaplogroopBranchRoot(start);
		var inSameBranch = tree.FullSubtreeOfNode(root);

		return inSameBranch
			.Select(n => n.HaplogroupId)
			.ToArray();
	}
}