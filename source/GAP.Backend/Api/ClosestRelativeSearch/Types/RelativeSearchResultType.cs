using Users.ClosestRelativeSearch;
using Users.UserAccess.Entities;

namespace Api.ClosestRelativeSearch.Types;

public class RelativeSearchResultType : ObjectType<ClosestRelativeSearchResult<CompleteUser>> {
	private const string RelativeSearchResultTypeName = "RelativeSearchResult";

	protected override void Configure(IObjectTypeDescriptor<ClosestRelativeSearchResult<CompleteUser>> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(RelativeSearchResultTypeName);

		descriptor.Field(closestRelativeSearchResult => closestRelativeSearchResult.UserMarkerSetValues)
			.Type<NonNullType<ListType<IntType>>>();
		descriptor.Field(closestRelativeSearchResult => closestRelativeSearchResult.MarkerSet)
			.Type<NonNullType<MarkerSetType>>();
		descriptor.Field(closestRelativeSearchResult => closestRelativeSearchResult.UserRelativeDistances)
			.Type<NonNullType<ListType<NonNullType<UserRelativeDistanceType>>>>();
	}
}