using Users.ClosestRelativeSearch.MarkerSets;

namespace Api.ClosestRelativeSearch.Types;

public class MarkerSetType : ObjectType<MarkerSet> {
	private const string MarkerSetTypeName = "MarkerSet";

	protected override void Configure(IObjectTypeDescriptor<MarkerSet> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(MarkerSetTypeName);

		descriptor.Field(markerSet => markerSet.Name)
			.Type<NonNullType<StringType>>();
		descriptor.Field(markerSet => markerSet.MarkerCount)
			.Type<NonNullType<IntType>>();
		descriptor.Field(markerSet => markerSet.MaxDistance)
			.Type<NonNullType<IntType>>();
		descriptor.Field(markerSet => markerSet.Markers)
			.Type<NonNullType<ListType<NonNullType<StringType>>>>();
	}
}