using Api.Users.Types.Public;
using Users.ClosestRelativeSearch;
using Users.UserAccess;
using Users.UserAccess.Entities;

namespace Api.ClosestRelativeSearch.Types;

public class UserRelativeDistanceType : ObjectType<UserRelativeDistance<CompleteUser>> {
	private const string UserRelativeDistanceTypeName = "UserRelativeDistance";
	private const string MarkerValuePropertyName = "markerSetValues";

	protected override void Configure(IObjectTypeDescriptor<UserRelativeDistance<CompleteUser>> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(UserRelativeDistanceTypeName);

		descriptor.Field(userRelativeDistance => userRelativeDistance.User)
			.Resolve(ctx => UserMasker.MaskUserWithoutSignedAgreement(user:ctx.Parent<UserRelativeDistance<CompleteUser>>().User))
			.Type<NonNullType<PublicCompleteUserType>>();
		descriptor.Field(userRelativeDistance => userRelativeDistance.Distance.Distance)
			.Type<NonNullType<IntType>>();
		descriptor.Field(userRelativeDistance => userRelativeDistance.Distance.ComparedMarkers)
			.Type<NonNullType<IntType>>();
		descriptor.Field(userRelativeDistance => userRelativeDistance.Distance.MarkerValues)
			.Name(MarkerValuePropertyName)
			.Type<NonNullType<ListType<IntType>>>();
	}
}