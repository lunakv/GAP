﻿using Users.Exceptions;

namespace Api.ClosestRelativeSearch.Types {
	public class WrongHaplogroupBranchException : UserException {
		public WrongHaplogroupBranchException(string message) : base(message) {
		}

		public override string GetRepresentation => nameof(WrongHaplogroupBranchException);

	}
}
