using Laboratory.LabelGeneration;
using Microsoft.AspNetCore.Mvc;

namespace Api.Laboratory.LabelGeneration.Inputs; 

[BindProperties]
public class UserLabelSelectionInput {
	public int AddressLabel { get; set; }
	public int SampleLabel { get; set; }

	public UserLabelSelection ToUserLabelSelection() {
		return new UserLabelSelection(
			addressLabel: AddressLabel,
			sampleLabel: SampleLabel
		);
	}
}