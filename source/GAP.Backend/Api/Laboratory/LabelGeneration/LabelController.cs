using Api.Laboratory.LabelGeneration.Inputs;
using Api.Logging;
using Auth.Policies;
using FluentValidation;
using Laboratory;
using Laboratory.KitManagement.Exceptions;
using Laboratory.LabelGeneration.Exceptions;
using Laboratory.LabelGeneration.LabelLayout;
using Laboratory.LabelGeneration.Labels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration.UserSecrets;
using Users.Exceptions;
using Labels = Laboratory.LabelGeneration;

namespace Api.Laboratory.LabelGeneration;

[Route("laboratory/label")]
[Authorize(policy: nameof(HasLabAccessPolicy))]
public class LabelController : Controller {
	private readonly Labels.LabelController _labelController;
	private readonly EndpointLogger<LabelController> _logger;

	private const string PdfContentType = "application/pdf";

	public LabelController(Labels.LabelController labelController, EndpointLogger<LabelController> logger) {
		_labelController = labelController;
		_logger = logger;
	}

	[HttpGet]
	public async Task<IActionResult> GenerateLabels(List<int> kitIds, UserLabelSelectionInput userLabelSelection, string labelLayout) {
		try {
			_logger.LogAuthEndpointContext();
			_logger.Logger.LogInformation("Request arguments: kitIds {KitIds}, userLabelSelection {UserLabelSelection}, LabelLayout {labelLayout}",
				kitIds, userLabelSelection, labelLayout);
			byte[] labels = await _labelController.GenerateLabelsAsync(
				kitIds: kitIds,
				userLabelSelection: userLabelSelection.ToUserLabelSelection(),
				labelLayout: labelLayout
			);
			var stream = new MemoryStream(labels);
			_logger.LogSuccessfulRequest();
			return File(fileStream: stream, contentType: PdfContentType);
		} catch (UserException ex) when (ex is InvalidLabelSelectionException || ex is KitNotFoundException || ex is LayoutNotFoundException) {
			_logger.Logger.LogWarning(exception: ex, message: ex.Message);
			return BadRequest(ex.Message);
		}
	}
}