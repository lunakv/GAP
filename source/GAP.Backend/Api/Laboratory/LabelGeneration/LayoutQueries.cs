using Api.Logging;
using Auth.Policies;
using HotChocolate.AspNetCore.Authorization;
using Laboratory.LabelGeneration.LabelLayout;

namespace Api.Laboratory.LabelGeneration; 

[ExtendObjectType("Query")]
public class LayoutQueries {
	[Authorize(Policy = nameof(HasLabAccessPolicy))]
	public IList<string> GetAvailableLabelLayouts([Service] ILayoutFetcher layoutFetcher, [Service] EndpointLogger<LayoutQueries> logger) {
		logger.LogAuthEndpointContext();
		IList<string> layoutNames = layoutFetcher.GetLayoutNames();
		logger.LogSuccessfulRequest();
		return layoutNames;
	}
}