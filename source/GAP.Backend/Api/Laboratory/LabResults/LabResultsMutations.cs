using Api.Laboratory.KitManagement.Inputs;
using Api.Logging;
using Auth.Policies;
using HotChocolate.AspNetCore.Authorization;
using Laboratory.KitManagement;
using Laboratory.KitManagement.Exceptions;
using Laboratory.KitManagement.Kits;
using Laboratory.KitManagement.KitStageAdvancement;
using Laboratory.LabResults;

namespace Api.Laboratory.LabResults; 

[ExtendObjectType("Mutation")]
public class LabResultsMutations {
		
	[Authorize(Policy = nameof(HasLabAccessPolicy))]
	public async Task<LabStrFileUploadResult> UploadLabStrFile(IFile file, [Service] LabResultsController labResultsController,
		[Service] EndpointLogger<LabResultsMutations> logger) {
		logger.LogAuthEndpointContext();
		await using Stream stream = file.OpenReadStream();
		LabStrFileUploadResult strFileUploadResult = await labResultsController.UploadLabStrFile(file: stream);
		logger.LogSuccessfulRequest();
		return strFileUploadResult;
	}
}