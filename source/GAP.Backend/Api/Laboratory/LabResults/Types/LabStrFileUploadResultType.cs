using Api.GenData.Types;
using Laboratory.LabResults;

namespace Api.Laboratory.LabResults.Types;

public class LabStrFileUploadResultType : ObjectType<LabStrFileUploadResult> {
	private const string LabStrFileUploadResultTypeName = "LabStrFileUploadResult";

	protected override void Configure(IObjectTypeDescriptor<LabStrFileUploadResult> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(LabStrFileUploadResultTypeName);

		descriptor.Field(uploadResult => uploadResult.OriginalStrDataSources)
			.Type<NonNullType<ListType<NonNullType<OriginalStrDataSourceType>>>>();
		descriptor.Field(uploadResult => uploadResult.KitsAdvanced)
			.Type<NonNullType<BooleanType>>();
	}
}