using Laboratory.SequencerInputGeneration;
using Laboratory.SequencerInputGeneration.Input;

namespace Api.Laboratory.SequencerInputGeneration.Types;

public class SequencerInputType : ObjectType<SequencerInput> {
	private const string SequencerInputTypeName = "SequencerInput";
	
	protected override void Configure(IObjectTypeDescriptor<SequencerInput> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(SequencerInputTypeName);

		descriptor.Field(sequencerInput => sequencerInput.Version)
			.Type<NonNullType<StringType>>();
		descriptor.Field(sequencerInput => sequencerInput.FirstTableRow)
			.Type<NonNullType<FirstTableRowType>>();
		descriptor.Field(sequencerInput => sequencerInput.SecondTableRows)
			.Type<NonNullType<ListType<NonNullType<SecondTableRowType>>>>();
	}
}