using Laboratory.SequencerInputGeneration.Input;

namespace Api.Laboratory.SequencerInputGeneration.Types;

public class SecondTableHeaderType : ObjectType<SecondTableHeader> {
	private readonly string SecondTableHeaderTypeName = "SecondTableHeader";

	protected override void Configure(IObjectTypeDescriptor<SecondTableHeader> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(SecondTableHeaderTypeName);

		descriptor.Field(secondTableRow => secondTableRow.Well)
			.Type<NonNullType<StringType>>();
		descriptor.Field(secondTableRow => secondTableRow.SampleName)
			.Type<NonNullType<StringType>>();
		descriptor.Field(secondTableRow => secondTableRow.Assay)
			.Type<NonNullType<StringType>>();
		descriptor.Field(secondTableRow => secondTableRow.FileNameConvention)
			.Type<NonNullType<StringType>>();
		descriptor.Field(secondTableRow => secondTableRow.ResultsGroup)
			.Type<NonNullType<StringType>>();
		descriptor.Field(secondTableRow => secondTableRow.SampleType)
			.Type<NonNullType<StringType>>();
		descriptor.Field(secondTableRow => secondTableRow.UserDefinedField1)
			.Type<NonNullType<StringType>>();
		descriptor.Field(secondTableRow => secondTableRow.UserDefinedField2)
			.Type<NonNullType<StringType>>();
		descriptor.Field(secondTableRow => secondTableRow.UserDefinedField3)
			.Type<NonNullType<StringType>>();
		descriptor.Field(secondTableRow => secondTableRow.UserDefinedField4)
			.Type<NonNullType<StringType>>();
		descriptor.Field(secondTableRow => secondTableRow.UserDefinedField5)
			.Type<NonNullType<StringType>>();
		descriptor.Field(secondTableRow => secondTableRow.Comments)
			.Type<NonNullType<StringType>>();
	}
}