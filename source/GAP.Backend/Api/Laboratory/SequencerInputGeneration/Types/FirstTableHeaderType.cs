using Laboratory.SequencerInputGeneration.Input;

namespace Api.Laboratory.SequencerInputGeneration.Types;

public class FirstTableHeaderType : ObjectType<FirstTableHeader> {
	private const string FirstTableHeaderTypeName = "FirstTableHeader";

	protected override void Configure(IObjectTypeDescriptor<FirstTableHeader> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(FirstTableHeaderTypeName);

		descriptor.Field(firstTableHeader => firstTableHeader.PlateName)
			.Type<NonNullType<StringType>>();
		descriptor.Field(firstTableHeader => firstTableHeader.ApplicationType)
			.Type<NonNullType<StringType>>();
		descriptor.Field(firstTableHeader => firstTableHeader.CapillaryLength)
			.Type<NonNullType<IntType>>();
		descriptor.Field(firstTableHeader => firstTableHeader.Polymer)
			.Type<NonNullType<StringType>>();
		descriptor.Field(firstTableHeader => firstTableHeader.NumberOfWells)
			.Type<NonNullType<IntType>>();
		descriptor.Field(firstTableHeader => firstTableHeader.OwnerName)
			.Type<NonNullType<StringType>>();
		descriptor.Field(firstTableHeader => firstTableHeader.BarcodeNumber)
			.Type<NonNullType<StringType>>();
		descriptor.Field(firstTableHeader => firstTableHeader.Comments)
			.Type<NonNullType<StringType>>();
	}
}