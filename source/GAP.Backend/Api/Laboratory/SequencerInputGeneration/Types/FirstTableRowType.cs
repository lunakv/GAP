using Laboratory.SequencerInputGeneration.Input;

namespace Api.Laboratory.SequencerInputGeneration.Types;

public class FirstTableRowType : ObjectType<FirstTableRow> {
	private const string FirstTableRowTypeName = "FirstTableRow";

	protected override void Configure(IObjectTypeDescriptor<FirstTableRow> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(FirstTableRowTypeName);

		descriptor.Field(firstTableRow => firstTableRow.PlateName)
			.Type<NonNullType<StringType>>();
		descriptor.Field(firstTableRow => firstTableRow.ApplicationType)
			.Type<NonNullType<StringType>>();
		descriptor.Field(firstTableRow => firstTableRow.CapillaryLength)
			.Type<NonNullType<IntType>>();
		descriptor.Field(firstTableRow => firstTableRow.Polymer)
			.Type<NonNullType<StringType>>();
		descriptor.Field(firstTableRow => firstTableRow.NumberOfWells)
			.Type<NonNullType<IntType>>();
		descriptor.Field(firstTableRow => firstTableRow.OwnerName)
			.Type<NonNullType<StringType>>();
		descriptor.Field(firstTableRow => firstTableRow.BarcodeNumber)
			.Type<NonNullType<StringType>>();
		descriptor.Field(firstTableRow => firstTableRow.Comments)
			.Type<NonNullType<StringType>>();
	}
}