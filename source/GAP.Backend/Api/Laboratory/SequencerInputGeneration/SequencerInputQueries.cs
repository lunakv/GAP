using Auth.Policies;
using HotChocolate.AspNetCore.Authorization;
using Laboratory.SequencerInputGeneration.Input;

using SequencerInputGen = Laboratory.SequencerInputGeneration;

namespace Api.Laboratory.SequencerInputGeneration; 

[ExtendObjectType("Query")]
public class SequencerInputQueries {
	[Authorize(Policy = nameof(HasLabAccessPolicy))]
	public async Task<SequencerInput> PrepareSequencerInput(List<int> kitIds, [Service] SequencerInputGen.SequencerInputController sequencerInputController) {
		return await sequencerInputController.PrepareSequencerInputAsync(kitIds: kitIds);
	}
}