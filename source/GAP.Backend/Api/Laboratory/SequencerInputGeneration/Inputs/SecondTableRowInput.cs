using Laboratory.SequencerInputGeneration.Input;

namespace Api.Laboratory.SequencerInputGeneration.Inputs;

public class SecondTableRowInput {
	public string Well { get; set; }
	public string SampleName { get; set; }
	public string Assay { get; set; }
	public string FileNameConvention { get; set; }
	public string ResultsGroup { get; set; }
	public string SampleType { get; set; }
	public string UserDefinedField1 { get; set; }
	public string UserDefinedField2 { get; set; }
	public string UserDefinedField3 { get; set; }
	public string UserDefinedField4 { get; set; }
	public string UserDefinedField5 { get; set; }
	public string Comments { get; set; }

	public SecondTableRowInput() {
		Well = string.Empty;
		SampleName = string.Empty;
		Assay = string.Empty;
		FileNameConvention = string.Empty;
		ResultsGroup = string.Empty;
		SampleType = string.Empty;
		UserDefinedField1 = string.Empty;
		UserDefinedField2 = string.Empty;
		UserDefinedField3 = string.Empty;
		UserDefinedField4 = string.Empty;
		UserDefinedField5 = string.Empty;
		Comments = string.Empty;
	}

	public SecondTableRow ToSecondTableRow() {
		return new SecondTableRow(
			well: Well,
			sampleName: SampleName,
			assay: Assay,
			fileNameConvention: FileNameConvention,
			resultsGroup: ResultsGroup,
			sampleType: SampleType,
			userDefinedField1: UserDefinedField1,
			userDefinedField2: UserDefinedField2,
			userDefinedField3: UserDefinedField3,
			userDefinedField4: UserDefinedField4,
			userDefinedField5: UserDefinedField5,
			comments: Comments
		);
	}
}