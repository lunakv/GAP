using Laboratory.SequencerInputGeneration.Input;

namespace Api.Laboratory.SequencerInputGeneration.Inputs;

public class FirstTableRowInput {
	public string PlateName { get; set; }
	public string ApplicationType { get; set; }
	public int CapillaryLength { get; set; }
	public string Polymer { get; set; }
	public int NumberOfWells { get; set; }
	public string OwnerName { get; set; }
	public string BarcodeNumber { get; set; }
	public string Comments { get; set; }

	public FirstTableRowInput() {
		PlateName = string.Empty;
		ApplicationType = string.Empty;
		CapillaryLength = 0;
		Polymer = string.Empty;
		NumberOfWells = 0;
		OwnerName = string.Empty;
		BarcodeNumber = string.Empty;
		Comments = string.Empty;
	}

	public FirstTableRow ToFirstTableRow() {
		return new FirstTableRow(
			plateName: PlateName,
			applicationType: ApplicationType,
			capillaryLength: CapillaryLength,
			polymer: Polymer,
			numberOfWells: NumberOfWells,
			ownerName: OwnerName,
			barcodeNumber: BarcodeNumber,
			comments: Comments
		);
	}
}