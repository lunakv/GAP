using Laboratory.SequencerInputGeneration.Input;

namespace Api.Laboratory.SequencerInputGeneration.Inputs; 

public class SequencerInputInput {
	public string Version { get; set; }
	public FirstTableRowInput FirstTableRow { get; set; }
	public List<SecondTableRowInput> SecondTableRows { get; set; }

	public SequencerInputInput() {
		Version = string.Empty;
		FirstTableRow = new FirstTableRowInput();
		SecondTableRows = new List<SecondTableRowInput>();
	}

	public SequencerInput ToSequencerInput() {
		return new SequencerInput(
			version: Version,
			firstTableHeader: new FirstTableHeader(),
			firstTableRow: FirstTableRow.ToFirstTableRow(),
			secondTableHeader: new SecondTableHeader(),
			secondTableRows: SecondTableRows.Select(secondTableRow => secondTableRow.ToSecondTableRow()).ToList()
		);
	}
}