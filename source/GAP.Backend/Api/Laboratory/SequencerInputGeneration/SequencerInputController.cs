using System.Text.Json;
using Api.Laboratory.SequencerInputGeneration.Inputs;
using Api.Logging;
using Auth.Policies;
using Laboratory.SequencerInputGeneration;
using Laboratory.SequencerInputGeneration.Input;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using SequncerInputGeneration = Laboratory.SequencerInputGeneration;

namespace Api.Laboratory.SequencerInputGeneration;

[Route("laboratory/sequencer-input")]
[Authorize(policy: nameof(HasLabAccessPolicy))]
public class SequencerInputController : Controller {
	private readonly SequncerInputGeneration.SequencerInputController _sequencerInputController;
	private readonly EndpointLogger<SequencerInputController> _logger;

	private const string TextContentType = "text/plain";

	public SequencerInputController(SequncerInputGeneration.SequencerInputController sequencerInputController, EndpointLogger<SequencerInputController> logger) {
		_sequencerInputController = sequencerInputController;
		_logger = logger;
	}

	[HttpPost]
	public async Task<IActionResult> GenerateSequencerInput([FromBody] SequencerInputInput sequencerInputInput) {
		_logger.LogAuthEndpointContext();
		_logger.Logger.LogInformation("Requests parameters: {@SequencerInput}", sequencerInputInput);
		SequencerInput sequencerInput = sequencerInputInput.ToSequencerInput();
		Stream generatedSequencerInput = await _sequencerInputController.GenerateSequencerInputAsync(sequencerInput);
		_logger.LogSuccessfulRequest();
		return File(fileStream: generatedSequencerInput, contentType: TextContentType);
	}
}