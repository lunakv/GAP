using Api.Laboratory.KitManagement.Resolvers;
using Api.Users;
using Api.Users.Types;
using Laboratory.KitManagement.Kits;
using Users.UserAccess.Entities.Interfaces;

namespace Api.Laboratory.KitManagement.Types;

public class KitType : ObjectType<Kit<IHasUserId>> {
	private const string KitTypeName = "Kit";

	protected override void Configure(IObjectTypeDescriptor<Kit<IHasUserId>> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(KitTypeName);

		descriptor.Field(kit => kit.Id)
			.Type<NonNullType<IntType>>();
		descriptor.Field(kit => kit.Stage)
			.Type<NonNullType<KitStageType>>();
		descriptor.Field(kit => kit.History)
			.Type<NonNullType<ListType<KitEventType>>>();
		descriptor.Field(kit => kit.User)
			.Type<NonNullType<UserType>>()
			.ResolveWith<KitResolvers>(r => r.GetUserAsync(default!, default!));
		descriptor.Field(kit => kit.Note)
			.Type<NonNullType<StringType>>();
		descriptor.Field(kit => kit.ActionRequired)
			.Type<NonNullType<BooleanType>>();
		descriptor.Field(kit => kit.Active)
			.Type<NonNullType<BooleanType>>();
	}
}