using Laboratory.KitManagement.Events;

namespace Api.Laboratory.KitManagement.Types;

public class KitEventType : ObjectType<KitEvent> {
	private const string KitEventTypeName = "KitEvent";
	private const string KitEventTypePropertyEnumName = "event";

	protected override void Configure(IObjectTypeDescriptor<KitEvent> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(KitEventTypeName);

		descriptor.Field(kitEvent => kitEvent.GetEvent())
			.Name(KitEventTypePropertyEnumName)
			.Type<NonNullType<KitEventEnumType>>();
		descriptor.Field(kitEvent => kitEvent.Date)
			.Type<NonNullType<DateType>>();
		descriptor.Field(kitEvent => kitEvent.Note)
			.Type<NonNullType<StringType>>();
	}
}