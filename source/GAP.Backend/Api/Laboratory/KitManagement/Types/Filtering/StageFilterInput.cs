using HotChocolate.Data.Filters;
using Laboratory.KitManagement.Kits;
using Users.UserAccess.Entities.Interfaces;

namespace Api.Laboratory.KitManagement.Types.Filtering;

public class StageFilterInput : EnumOperationFilterInputType<Kit<IHasUserId>.KitStage> { }