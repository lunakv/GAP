using HotChocolate.Data.Filters;
using Laboratory.KitManagement.Kits;
using Users.UserAccess.Entities.Interfaces;

namespace Api.Laboratory.KitManagement.Types.Filtering;

public class KitFilterInput : FilterInputType<Kit<IHasUserId>> {
	private const string KitFilterInputTypeName = nameof(KitFilterInput);

	protected override void Configure(IFilterInputTypeDescriptor<Kit<IHasUserId>> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(KitFilterInputTypeName);

		descriptor.Field(kit => kit.Id);
		descriptor.Field(kit => kit.Stage)
			.Type<StageFilterInput>();
		descriptor.Field(kit => kit.ActionRequired);
		descriptor.Field(kit => kit.Active);
	}
}