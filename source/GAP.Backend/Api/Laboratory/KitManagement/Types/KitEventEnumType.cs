using Laboratory.KitManagement.Events;

namespace Api.Laboratory.KitManagement.Types;

public class KitEventEnumType : EnumType<KitEvent.Event> {
	private const string KitEventEnumTypeName = "KitEventEnum";

	protected override void Configure(IEnumTypeDescriptor<KitEvent.Event> descriptor) {
		descriptor.Name(KitEventEnumTypeName);
	}
}