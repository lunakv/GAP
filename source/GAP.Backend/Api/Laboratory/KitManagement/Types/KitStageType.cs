using Api.Laboratory.KitManagement.Resolvers;
using HotChocolate.Language;
using Laboratory.KitManagement.Kits;
using Users.UserAccess.Entities.Interfaces;

namespace Api.Laboratory.KitManagement.Types;

public class KitStageType : EnumType<Kit<IHasUserId>.KitStage> {
	private const string KitStageTypeName = "KitStage";

	protected override void Configure(IEnumTypeDescriptor<Kit<IHasUserId>.KitStage> descriptor) {
		descriptor.Name(KitStageTypeName);
	}
}