using Api.Laboratory.KitManagement.Inputs;
using Api.Laboratory.KitManagement.Types;
using Api.Laboratory.KitManagement.Types.Filtering;
using Api.Logging;
using Auth.Policies;
using HotChocolate.AspNetCore.Authorization;
using HotChocolate.Data.Sorting.Expressions;
using Laboratory.KitManagement;
using Laboratory.KitManagement.Kits;
using Users.UserAccess.Entities.Interfaces;

namespace Api.Laboratory.KitManagement;

[ExtendObjectType("Query")]
public class KitQueries {
	[UseOffsetPaging]
	[UseFiltering(filterType: typeof(KitFilterInput))]
	[Authorize(Policy = nameof(HasLabAccessPolicy))]
	public async Task<IEnumerable<Kit<IHasUserId>>> GetKits(KitStorageFilterInput? storageFilter, [Service] KitController kitController, [Service] EndpointLogger<KitQueries> logger) {
		logger.LogAuthEndpointContext();
		logger.Logger.LogInformation("Request parameters: {@StorageFilter}", storageFilter);
		IReadOnlyCollection<Kit<IHasUserId>> kits = await kitController.GetKitsAsync(storageFilter?.ToKitStorageFilter());
		logger.LogSuccessfulRequest();
		return kits;
	}

}
