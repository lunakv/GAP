using Api.Laboratory.KitManagement.Types;
using Api.Users;
using Api.Users.DataLoaders;
using Laboratory.KitManagement.Kits;
using Users.UserAccess;
using Users.UserAccess.Entities;
using Users.UserAccess.Entities.Interfaces;

namespace Api.Laboratory.KitManagement.Resolvers; 

public class KitResolvers {
	public async Task<User> GetUserAsync([Parent] Kit<IHasUserId> kit, UserByIdDataLoader userByIdDataLoader) {
		return await userByIdDataLoader.LoadAsync(kit.User.Id);
	}
}