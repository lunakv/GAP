namespace Api.Laboratory.KitManagement.Inputs; 

public class KitUpdateInput {
	public int KitId { get; set; }
	public string Note { get; set; } = null!;
}