using Laboratory.KitManagement.KitStageAdvancement;

namespace Api.Laboratory.KitManagement.Inputs;

[GraphQLName("KitAction")]
public class KitActionInput {
	public int KitId { get; }
	public string Note { get; }

	public KitActionInput(int kitId, string note) {
		KitId = kitId;
		Note = note;
	}

	public KitAction ToKitAction() {
		return new KitAction(
			kitId: KitId,
			note: Note
		);
	}
}