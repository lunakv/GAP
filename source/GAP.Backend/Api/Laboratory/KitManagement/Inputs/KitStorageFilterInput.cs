using Laboratory.KitManagement.Storage;

namespace Api.Laboratory.KitManagement.Inputs;

[GraphQLName("KitStorageFilter")]
public class KitStorageFilterInput {
	public bool? ActionRequired { get; set; }
	public bool? Active { get; set; }

	public KitStorageFilter ToKitStorageFilter() {
		return new KitStorageFilter(
			actionRequired: ActionRequired,
			active: Active
		);
	}
}