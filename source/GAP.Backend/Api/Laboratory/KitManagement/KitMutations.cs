using Api.Laboratory.KitManagement.Inputs;
using Api.Logging;
using Auth.Policies;
using HotChocolate.AspNetCore.Authorization;
using Laboratory.KitManagement;
using Laboratory.KitManagement.Exceptions;
using Laboratory.KitManagement.Kits;
using Laboratory.KitManagement.KitStageAdvancement;
using Users.UserAccess.Entities.Interfaces;

namespace Api.Laboratory.KitManagement; 

[ExtendObjectType("Mutation")]
public class KitMutations {
	[Authorize(Policy = nameof(HasLabAccessPolicy))]
	public async Task<IReadOnlyCollection<Kit<IHasUserId>>> AdvanceKitsToNextStage(IReadOnlyCollection<KitActionInput> kitActions, [Service] KitController kitController,
		[Service] EndpointLogger<KitMutations> logger) {
		logger.LogAuthEndpointContext();
		logger.Logger.LogInformation("Request arguments: kitActions {KitActions}", kitActions);
		IReadOnlyCollection<Kit<IHasUserId>> kits = await kitController.AdvanceKitsToNextStage(
			kitActions: kitActions.Select(kitAction => kitAction.ToKitAction()).ToList()
		);
		logger.LogSuccessfulRequest();
		return kits;
	}

	[Authorize(Policy = nameof(HasLabAccessPolicy))]
	public async Task<Kit<IHasUserId>> UpdateKit(KitUpdateInput kitUpdateInput, [Service] KitController kitController,
		[Service] EndpointLogger<KitMutations> logger) {
		logger.LogAuthEndpointContext();
		logger.Logger.LogInformation("Request arguments: kitUpdateInput {KitUpdateInput}", kitUpdateInput);
		Kit<IHasUserId> kit = await kitController.UpdateKitAsync(kitId: kitUpdateInput.KitId, note: kitUpdateInput.Note);
		logger.LogSuccessfulRequest();
		return kit;
	}
}