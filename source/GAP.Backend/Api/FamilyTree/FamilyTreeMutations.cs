﻿using Api.FamilyTree.Dtos;
using Api.Logging;
using Api.Users;
using Auth.Policies;
using FamilyTree.Access;
using HotChocolate.AspNetCore.Authorization;

namespace Api.FamilyTree; 

[ExtendObjectType("Mutation")]
public class FamilyTreeMutations {
	
	[Authorize(Policy = nameof(HasUserManageRightPolicy))]
	public async Task<IList<FamilyTreeNodeDto>> UpdateFamilyTree(int userId, int treeId, List<FamilyTreeNodeDto> nodes, [Service] IFamilyTreeSaver treeSaver, [Service] EndpointLogger<FamilyTreeMutations> logger) {
		logger.LogAuthEndpointContext(aboutUserId: userId);
		logger.Logger.LogInformation("Request arguments: userId {UserId} treeId {TreeId}", userId, treeId);

		var converted = nodes
			.Select(n => n.ToNode())
			.ToList();
		var saved = await treeSaver.UpdateTreeAsync(userId, treeId, converted);
		var result = saved
			.Select(n => new FamilyTreeNodeDto(n))
			.ToList();

		logger.LogSuccessfulRequest();
		return result;
	}
	
	[Authorize(Policy = nameof(HasUserManageRightPolicy))]
	public async Task<TreeIdDto> AddFamilyTree(int userId, [Service] IFamilyTreeSaver treeSaver, [Service] EndpointLogger<UserMutations> logger) {
		logger.LogAuthEndpointContext(aboutUserId: userId);
		logger.Logger.LogInformation("Request arguments: userId {UserId}", userId);

		var treeId = await treeSaver.AddNewTreeAsync(userId);

		logger.LogSuccessfulRequest();
		return new TreeIdDto(treeId);
	}

	public record TreeIdDto(int Id);
	
	[Authorize(Policy = nameof(HasUserManageRightPolicy))]
	public async Task<TreeIdDto> RemoveFamilyTree(int userId, int treeId, [Service] IFamilyTreeSaver treeSaver, [Service] EndpointLogger<UserMutations> logger) {
		logger.LogAuthEndpointContext(aboutUserId: userId);
		logger.Logger.LogInformation("Request arguments: userId {UserId} treeId {TreeId}", userId, treeId);

		await treeSaver.RemoveTreeAsync(treeId, userId);

		logger.LogSuccessfulRequest();
		return new TreeIdDto(-1);
	}
}
