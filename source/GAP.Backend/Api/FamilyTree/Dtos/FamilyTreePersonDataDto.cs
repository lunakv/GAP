﻿using FamilyTree;

namespace Api.FamilyTree.Dtos;

[GraphQLName("FamilyTreePersonData")]
public record FamilyTreePersonDataDto(

    string? GivenName,
    string? FamilyName,
    string? BirthDate,
    string? DeathDate,
    string? Avatar,
    Gender? Gender
) {
    public FamilyTreePersonDataDto(FamilyTreePersonData data) 
        :this(data.GivenName,
             data.FamilyName,
             data.BirthDate,
             data.DeathDate,
             data.Avatar,
             data.Gender)
    {}

    [GraphQLIgnore]
    public FamilyTreePersonData ToPersonData() {
        return new FamilyTreePersonData(
            GivenName,
            FamilyName,
            BirthDate,
            DeathDate,
            Avatar,
            Gender);
    }
};

