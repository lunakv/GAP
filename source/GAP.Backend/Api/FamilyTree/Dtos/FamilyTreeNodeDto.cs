﻿using FamilyTree;
using System.Linq;

namespace Api.FamilyTree.Dtos;


[GraphQLName("FamilyTreeNode")]
public record FamilyTreeNodeDto
(
    string Id,
    FamilyTreePersonDataDto PersonData,
    IList<string> Partners,
    IList<string> Children,
    string? FatherId,
    string? MotherId
) {
    public FamilyTreeNodeDto(FamilyTreeNode node) 
        :this(
            node.Id,
            new FamilyTreePersonDataDto(node.PersonData),
            node.Partners,
            node.Children,
            node.FatherId,
            node.MotherId){
    }

    [GraphQLIgnore]
    public FamilyTreeNode ToNode() {
        return new FamilyTreeNode(
            Id,
            PersonData.ToPersonData(),
            Partners,
            Children,
            FatherId,
            MotherId);
    }
};
