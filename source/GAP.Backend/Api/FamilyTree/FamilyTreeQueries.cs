﻿using Api.Logging;
using Auth.Policies;
using HotChocolate.AspNetCore.Authorization;
using Users.UserAccess;
using FamilyTree;
using Api.FamilyTree.Dtos;
using FamilyTree.Access;
using static Api.FamilyTree.FamilyTreeMutations;

namespace Api.FamilyTree; 

[ExtendObjectType("Query")]
public class FamilyTreeQueries {
	
    public FamilyTreeQueries() {}
    
    [Authorize(Policy = nameof(HasUserViewRightPolicy))]
    public async Task<IList<FamilyTreeNodeDto>> GetFamilyTree(int userId, int treeId, [Service] IFamilyTreeFetcher treeFetcher, [Service] EndpointLogger<FamilyTreeQueries> endpointLogger) {
        endpointLogger.LogAuthEndpointContext(aboutUserId: userId);
        endpointLogger.Logger.LogInformation("Request arguments: userid {UserId} treeId {TreeId}", userId, treeId);
        
        var result = await treeFetcher.GetTreeAsync(userId, treeId);
        var converted = result
            .Select(n => new FamilyTreeNodeDto(n))
            .ToList();

        endpointLogger.LogSuccessfulRequest();

        return converted;
    }  

    [Authorize(Policy = nameof(HasUserViewRightPolicy))]
    ///<param name="userId">is required just for the purpose of rights - it's retrieved by reflection</param>
    public async Task<IList<TreeIdDto>> GetFamilyTreesOfUser(int userId, [Service] IFamilyTreeFetcher treeFetcher, [Service] EndpointLogger<FamilyTreeQueries> endpointLogger) {
        endpointLogger.LogAuthEndpointContext(aboutUserId: userId);
        endpointLogger.Logger.LogInformation("Request arguments: userid {UserId}", userId);
        
        var trees = await treeFetcher.GetTreesOfUserAsync(userId);

        endpointLogger.LogSuccessfulRequest();

        return trees.Select(t => new TreeIdDto(t)).ToList();
    }

    [Authorize(Policy = nameof(HasUserViewRightPolicy))]
    public async Task<IList<FamilyTreeNodeDto>> GetFamilyTreeBackup(int userId, int treeId, [Service] IFamilyTreeFetcher treeFetcher, [Service] EndpointLogger<FamilyTreeQueries> endpointLogger) {
        endpointLogger.LogAuthEndpointContext(aboutUserId: userId);
        endpointLogger.Logger.LogInformation("Request arguments: userid {UserId} treeId {TreeId}", userId, treeId);
        
        var result = await treeFetcher.GetBackupAsync(userId, treeId);
        var converted = result
            .Select(n => new FamilyTreeNodeDto(n))
            .ToList();

        endpointLogger.LogSuccessfulRequest();

        return converted;
    }
}
