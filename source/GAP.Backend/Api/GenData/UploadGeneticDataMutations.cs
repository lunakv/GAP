using Api.GenData.Inputs;
using Api.Logging;
using Auth.Policies;
using HotChocolate.AspNetCore.Authorization;
using Users.GenData;
using Users.GenData.Parsers;
using Path = System.IO.Path;

namespace Api.GenData;

[ExtendObjectType("Mutation")]
public class UploadGeneticDataMutations {
	[Authorize(Policy = nameof(HasUserManageRightPolicy))]
	public async Task<GeneticData> AddStrMarkers(int userId, GeneticTestProviderInput geneticTestProvider, IList<StrMarkerInput> strMarkers, IFile file, [Service] GeneticDataUploadController geneticDataUploadController, [Service] EndpointLogger<UploadGeneticDataMutations> logger) {
		logger.LogAuthEndpointContext();
		string fileExtension = Path.GetExtension(file.Name) ?? string.Empty;
		GeneticData geneticData = await geneticDataUploadController.UploadExternalStrFile(
			userId: userId,
			file: file.OpenReadStream(),
			externalStrFileParser: new UnsupportedProviderParser(
				geneticTestProvider: new GeneticTestProvider(geneticTestProvider.Name),
				fileExtension: fileExtension,
				strMarkers: new StrMarkerCollection<string, int>(strMarkers.ToDictionary(m => m.Name, m => m.Value)))
		);
		logger.LogSuccessfulRequest();
		return geneticData;
	}

	[Authorize(Policy = nameof(HasUserManageRightPolicy))]
	public async Task<GeneticData> AddStrMarkersFile(int userId, IFile file, [Service] GeneticDataUploadController geneticDataUploadController, [Service] EndpointLogger<UploadGeneticDataMutations> logger) {
		logger.LogAuthEndpointContext();
		await using Stream stream = file.OpenReadStream();
		var geneticData = await geneticDataUploadController.UploadExternalStrFile(userId: userId, file: stream, externalStrFileParser: new StrParserResolver());

		logger.LogSuccessfulRequest();
		return geneticData;
	}
}