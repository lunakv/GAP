using Api.GenData.Payloads;

namespace Api.GenData.Types;

public class FixConflictPayloadType : ObjectType<FixConflictPayload> {
	protected override void Configure(IObjectTypeDescriptor<FixConflictPayload> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Field(fixConflictPayload => fixConflictPayload.AggregatedStrMarkerValues)
			.Type<NonNullType<ListType<NonNullType<StrMarkerType>>>>();
	}
}