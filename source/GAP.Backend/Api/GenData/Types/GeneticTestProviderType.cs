using Users.GenData.Parsers;

namespace Api.GenData.Types;

public class GeneticTestProviderType : ObjectType<GeneticTestProvider> {
	private const string GeneticTestProviderTypeName = "GeneticTestProvider";

	protected override void Configure(IObjectTypeDescriptor<GeneticTestProvider> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(GeneticTestProviderTypeName);

		descriptor.Field(provider => provider.Name)
			.Type<NonNullType<StringType>>();
	}
}