using Users.GenData;

namespace Api.GenData.Types;

public class StrMarkerType : ObjectType<StrMarkerCollection<string, int>.StrMarker> {
	private const string StrMarkerTypeName = "StrMarker";

	protected override void Configure(IObjectTypeDescriptor<StrMarkerCollection<string, int>.StrMarker> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(StrMarkerTypeName);

		descriptor.Field(marker => marker.Name)
			.Type<NonNullType<StringType>>();
		descriptor.Field(marker => marker.Value)
			.Type<NonNullType<IntType>>();
	}
}