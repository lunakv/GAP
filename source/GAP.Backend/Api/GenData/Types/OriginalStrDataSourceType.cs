using Api.GenData.Resolvers;
using Api.Users.Types;
using Users.GenData;
using Users.GenData.OriginalStrDataSources;

namespace Api.GenData.Types;

public class OriginalStrDataSourceType : ObjectType<OriginalStrDataSource> {
	private const string OriginalStrDataSourceTypeName = "OriginalStrDataSource";
	private const string UserPropertyName = "user";

	protected override void Configure(IObjectTypeDescriptor<OriginalStrDataSource> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(OriginalStrDataSourceTypeName);

		descriptor.Field(source => source.UserId)
			.Name(UserPropertyName)
			.ResolveWith<OriginalStrDataSourceResolvers>(r => r.GetUserAsync(default!, default!))
			.Type<NonNullType<UserType>>();
		descriptor.Field(source => source.TestProvider)
			.Type<NonNullType<GeneticTestProviderType>>();
		descriptor.Field(source => source.StrMarkers)
			.Resolve(ctx => ctx.Parent<OriginalStrDataSource>().StrMarkers.CloneAsOrderedList(MarkerComparer.AreSame))
			.Type<NonNullType<ListType<NonNullType<StrMarkerType>>>>();
		descriptor.Field(source => source.FileName)
			.Type<StringType>();
	}
}