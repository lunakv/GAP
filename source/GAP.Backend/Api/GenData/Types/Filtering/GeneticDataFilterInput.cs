using HotChocolate.Data.Filters;
using Users.GenData;

namespace Api.GenData.Types.Filtering; 

public class GeneticDataFilterInput : FilterInputType<GeneticData> {
	private const string GeneticDataFilterInputTypeName = nameof(GeneticDataFilterInput);

	protected override void Configure(IFilterInputTypeDescriptor<GeneticData> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(GeneticDataFilterInputTypeName);

		descriptor.Field(geneticData => geneticData.Haplogroup)
			.Type<HaplogroupFilterInput>();
	}
}