using HotChocolate.Data.Filters;
using Users.GenData.Haplogroup;

namespace Api.GenData.Types.Filtering; 

public class HaplogroupFilterInput : FilterInputType<Haplogroup> {
	private const string HaplogroupFilterInputTypeName = nameof(HaplogroupFilterInput);

	protected override void Configure(IFilterInputTypeDescriptor<Haplogroup> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(HaplogroupFilterInputTypeName);

		descriptor.Field(haplogroup => haplogroup.Id);
		descriptor.Field(haplogroup => haplogroup.Name);
	}
}