using Users.GenData;

namespace Api.GenData.Types.Public; 

public class PublicGeneticDataType : ObjectType<GeneticData> {
	private const string PublicGeneticDataTypeName = "PublicGeneticData";

	protected override void Configure(IObjectTypeDescriptor<GeneticData> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(PublicGeneticDataTypeName);

		descriptor.Field(geneticData => geneticData.Haplogroup)
			.Resolve(ctx => ctx.Parent<GeneticData>().Haplogroup.Name)
			.Type<StringType>();
	}
}