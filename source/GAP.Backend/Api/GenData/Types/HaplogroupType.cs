using Users.GenData.Haplogroup;

namespace Api.GenData.Types;

public class HaplogroupType : ObjectType<Haplogroup> {
	private const string HaplogroupTypeName = "Haplogroup";

	protected override void Configure(IObjectTypeDescriptor<Haplogroup> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(HaplogroupTypeName);

		descriptor.Field(haplogroup => haplogroup.Id)
			.Type<NonNullType<IntType>>();
		descriptor.Field(haplogroup => haplogroup.Name)
			.Type<NonNullType<StringType>>();
	}
}