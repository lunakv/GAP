using Api.GenData.Resolvers;
using Users.GenData;

namespace Api.GenData.Types;

public class GeneticDataType : ObjectType<GeneticData> {
	private const string GeneticDataTypeName = "GeneticData";
	private const string StrConflictPropertyName = "strConflict";

	protected override void Configure(IObjectTypeDescriptor<GeneticData> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(GeneticDataTypeName);

		descriptor.Field(geneticData => geneticData.StrMarkers)
			.Resolve(ctx => ctx.Parent<GeneticData>().StrMarkers.CloneAsOrderedList(MarkerComparer.AreSame))
			.Type<NonNullType<ListType<NonNullType<StrMarkerType>>>>();
		descriptor.Field(geneticData => geneticData.Haplogroup)
			.Type<NonNullType<HaplogroupType>>();
		descriptor.Field(geneticData => geneticData.OriginalStrDataSources)
			.ResolveWith<GeneticDataResolvers>(r => r.GetOriginalStrDataSources(default!, default!))
			.Type<NonNullType<ListType<NonNullType<OriginalStrDataSourceType>>>>();
		descriptor.Field(StrConflictPropertyName)
			.ResolveWith<GeneticDataResolvers>(r => r.GetStrConflict(default!, default!, default!))
			.Type<StrConflictType>();
	}
}