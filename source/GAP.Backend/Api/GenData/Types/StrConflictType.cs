using Api.GenData.Resolvers;
using Api.Users.Types;
using Users.GenData.Conflict;

namespace Api.GenData.Types;

public class StrConflictType : ObjectType<StrConflict> {
	private const string StrConflictTypeName = "StrConflict";
	private const string UserPropertyName = "user";

	protected override void Configure(IObjectTypeDescriptor<StrConflict> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(StrConflictTypeName);

		descriptor.Field(strConflict => strConflict.UserId)
			.Name(UserPropertyName)
			.ResolveWith<StrConflictResolvers>(r => r.GetUserAsync(default!, default!))
			.Type<NonNullType<UserType>>();
		descriptor.Field(strConflict => strConflict.ConflictingMarkers)
			.Type<NonNullType<ListType<NonNullType<StringType>>>>();
	}
}