using Api.Logging;
using Auth.Policies;
using HotChocolate.AspNetCore.Authorization;
using Users.GenData;
using Users.GenData.Conflict;

namespace Api.GenData;

[ExtendObjectType("Query")]
public class ConflictQueries {
	[Authorize(Policy = nameof(IsAdminPolicy))]
	public async Task<ICollection<StrConflict>> GetStrConflicts(List<int>? userIds, [Service] ConflictController conflictController, [Service] MarkerSorter markerSorter, [Service] EndpointLogger<ConflictQueries> logger) {
		logger.LogAuthEndpointContext();
		await markerSorter.LoadOrderingAsync();
		ICollection<StrConflict> conflicts = (
			await conflictController.GetStrConflictsAsync(userIds: userIds)
		).OrderBy(conflict => conflict.UserId)
		.Select(conflict => new StrConflict(
			userId: conflict.UserId,
			conflictingMarkers: markerSorter.FixMarkerNames(conflict.ConflictingMarkers))
		).ToList();
		logger.LogSuccessfulRequest();
		return conflicts;
	}
}