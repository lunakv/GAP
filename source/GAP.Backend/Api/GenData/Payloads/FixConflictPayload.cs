using Users.GenData;

namespace Api.GenData.Payloads;

public class FixConflictPayload {
	public IList<StrMarkerCollection<string, int>.StrMarker> AggregatedStrMarkerValues { get; }

	public FixConflictPayload(StrMarkerCollection<string, int> aggregatedMarkerValues) {
		AggregatedStrMarkerValues = aggregatedMarkerValues.CloneAsOrderedList(MarkerComparer.AreSame);
	}
}