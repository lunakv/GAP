using Users.GenData.Parsers;

namespace Api.GenData.Inputs; 

public class GeneticTestProviderInput {
	public string Name { get; set; } = null!;

	public GeneticTestProvider ToGeneticTestProvider() {
		return new GeneticTestProvider(name: Name);
	}
}