namespace Api.GenData.Inputs;

public class StrMarkerInput {
	public string Name { get; set; } = null!;
	public int Value { get; set; }
}