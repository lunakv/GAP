using Api.Logging;
using Auth.Identity;
using Auth.Identity.Claims;
using Auth.Policies;
using Auth.Policies.AuthorizationHandlers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Options;
using Users;
using Users.GenData.OriginalStrDataSources;
using Users.GenData.OriginalStrDataSources.Storage;
using Users.GenData.OriginalStrDataSources.Storage.File;
using Users.UserSharing.UserManagementSharing;
using Users.UserSharing.UserManagementSharing.Storage;
using Users.UserSharing.UserViewSharing;
using Users.UserSharing.UserViewSharing.Storage;

namespace Api.GenData;

[Route("file")]
[Authorize]
public class FileServerController : Controller {
	private readonly IOriginalStrFileFetcher _originalStrFileFetcher;
	private readonly IOriginalStrDataSourceFetcher _originalStrDataSourceFetcher;
	private readonly AccessChecker _accessChecker;
	private readonly EndpointLogger<FileServerController> _logger;

	private const string DefaultContentType = "application/octet-stream";

	public FileServerController(IOriginalStrFileFetcher originalStrFileFetcher, IOriginalStrDataSourceFetcher originalStrDataSourceFetcher,
		AccessChecker accessChecker, EndpointLogger<FileServerController> logger) {
		_originalStrFileFetcher = originalStrFileFetcher;
		_originalStrDataSourceFetcher = originalStrDataSourceFetcher;
		_accessChecker = accessChecker;
		_logger = logger;
	}

	[HttpGet("{fileName}")]
	public async Task<IActionResult> ServeOriginalStrFile(string fileName) {
		_logger.LogAuthEndpointContext();
		_logger.Logger.LogInformation("Request arguments: filename {FileName}", fileName);

		if (string.IsNullOrEmpty(fileName) || fileName.IndexOfAny(System.IO.Path.GetInvalidFileNameChars()) >= 0) {
			_logger.Logger.LogWarning("Provided {FileName} for original genetic data file includes invalid chars or is empty.", fileName);
			return BadRequest();
		}

		ICollection<OriginalStrDataSource> sources = await _originalStrDataSourceFetcher.TryGetOriginalStrDataSourceAsync(fileName: fileName);
		
		// Filename contains data of more users, so only admin should have access.
		if (sources.Count > 1) {
			if (!_accessChecker.IsAdmin(HttpContext.User)) {
				return Unauthorized();
			}
		}

		if (sources.Count == 0) {
			return NotFound();
		}

		// Filename represents data of one user, so admin, the user and his viewers and managers have access.
		bool viewAccessProvided = await _accessChecker.HasViewAccessAsync(accessingUser: HttpContext.User, accessedUserId: sources.First().UserId);
		if (!viewAccessProvided) {
			int claimUserId = new ClaimsRetriever().GetClaimUserId(HttpContext.User);
			_logger.Logger.LogWarning("User {ClaimUserId} attempted to download {FileName} which he had no auth for.",
				claimUserId, fileName
			);
			return Unauthorized();
		}

		string filePath = _originalStrFileFetcher.GetFilePath(fileName);

		if (!System.IO.File.Exists(filePath)) {
			_logger.Logger.LogWarning("No original genetic file with {Path} found", filePath);
			return NotFound();
		}

		var contentTypeProvider = new FileExtensionContentTypeProvider();
		bool contentTypeDetermined = contentTypeProvider.TryGetContentType(filePath, out string? contentType);
		if (!contentTypeDetermined || contentType == null) {
			contentType = DefaultContentType;
		}

		var fs = new FileStream(filePath, new FileStreamOptions() {
			Access = FileAccess.Read,
			Mode = FileMode.Open,
			Options = FileOptions.Asynchronous
		});
		_logger.LogSuccessfulRequest();
		return File(fileStream: fs, contentType: contentType);
	}
}