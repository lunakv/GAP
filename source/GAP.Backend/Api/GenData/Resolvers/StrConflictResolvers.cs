using Api.Users;
using Api.Users.DataLoaders;
using Users.GenData.Conflict;
using Users.UserAccess.Entities;

namespace Api.GenData.Resolvers;

public class StrConflictResolvers {
	public async Task<User> GetUserAsync([Parent] StrConflict strConflict, UserByIdDataLoader userByIdDataLoader) {
		return await userByIdDataLoader.LoadAsync(strConflict.UserId);
	}
}