using Api.GenData.DataLoaders;
using Users.GenData;
using Users.GenData.Conflict;
using Users.GenData.OriginalStrDataSources;

namespace Api.GenData.Resolvers;

public class GeneticDataResolvers {
	public async Task<IReadOnlyCollection<OriginalStrDataSource>> GetOriginalStrDataSources([Parent] GeneticData geneticData,
		OriginalStrDataSourceByIdDataLoader originalStrDataSourceByIdDataLoader) {
		return await originalStrDataSourceByIdDataLoader.LoadAsync(geneticData.OriginalStrDataSources);
	}

	public async Task<StrConflict?> GetStrConflict([Parent] GeneticData geneticData,
		OriginalStrDataSourceByIdDataLoader originalStrDataSourceByIdDataLoader,
		NullableStrConflictByUserIdDataLoader nullableStrConflictByUserIdDataLoader) {
		IReadOnlyList<OriginalStrDataSource> originalStrDataSources = await originalStrDataSourceByIdDataLoader.LoadAsync(geneticData.OriginalStrDataSources);

		if (originalStrDataSources.Count > 0) {
			return await nullableStrConflictByUserIdDataLoader.LoadAsync(originalStrDataSources[0].UserId);
		}

		return null;
	}
}