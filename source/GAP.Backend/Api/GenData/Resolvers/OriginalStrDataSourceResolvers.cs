using Api.Users.DataLoaders;
using Users.GenData.OriginalStrDataSources;
using Users.UserAccess.Entities;

namespace Api.GenData.Resolvers; 

public class OriginalStrDataSourceResolvers {
	public async Task<User> GetUserAsync([Parent] OriginalStrDataSource originalStrDataSource, UserByIdDataLoader userByIdDataLoader) {
		return await userByIdDataLoader.LoadAsync(originalStrDataSource.UserId);
	}
}