using Api.GenData.Inputs;
using Api.GenData.Payloads;
using Api.Logging;
using Auth.Policies;
using HotChocolate.AspNetCore.Authorization;
using Users.GenData;
using Users.GenData.Conflict;

namespace Api.GenData;

[ExtendObjectType("Mutation")]
public class ConflictMutations {
	[Authorize(Policy = nameof(IsAdminPolicy))]
	public async Task<FixConflictPayload> FixStrConflict(int userId, List<StrMarkerInput> newAggregatedMarkerValues, [Service] ConflictController conflictController,  [Service] MarkerSorter markerSorter, [Service] EndpointLogger<ConflictMutations> logger) {
		logger.LogAuthEndpointContext();
		logger.Logger.LogInformation("Request arguments: userId {UserId}, new marker values {NewMarkerValues}", userId, newAggregatedMarkerValues);
		await markerSorter.LoadOrderingAsync();
		var updatedMarkers = await conflictController.FixStrConflictAsync(
			userId: userId,
			newAggregatedMarkersValues: MarkerFormatter.FormatMarkers(new StrMarkerCollection<string, int>(newAggregatedMarkerValues.ToDictionary(marker => marker.Name, marker => marker.Value)))
		);
		logger.LogSuccessfulRequest();
		markerSorter.AddOrdering(updatedMarkers);
		return new FixConflictPayload(updatedMarkers);
	}
}