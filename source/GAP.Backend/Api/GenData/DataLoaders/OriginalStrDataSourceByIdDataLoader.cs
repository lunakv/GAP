using Users.GenData;
using Users.GenData.OriginalStrDataSources;
using Users.GenData.OriginalStrDataSources.Storage;

namespace Api.GenData.DataLoaders;

public class OriginalStrDataSourceByIdDataLoader : BatchDataLoader<int, OriginalStrDataSource> {
	private readonly IOriginalStrDataSourceFetcher _originalStrDataSourceFetcher;
	private readonly MarkerSorter _markerSorter;

	public OriginalStrDataSourceByIdDataLoader(IBatchScheduler batchScheduler, IOriginalStrDataSourceFetcher originalStrDataSourceFetcher, MarkerSorter markerSorter, DataLoaderOptions? options = null)
		: base(batchScheduler, options) {
		_originalStrDataSourceFetcher = originalStrDataSourceFetcher;
		_markerSorter = markerSorter;
	}

	protected override async Task<IReadOnlyDictionary<int, OriginalStrDataSource>> LoadBatchAsync(IReadOnlyList<int> keys, CancellationToken cancellationToken) {
		var sources = await _originalStrDataSourceFetcher.GetOriginalStrDataSourcesAsync(sourceIds: keys);
		await _markerSorter.LoadOrderingAsync();
		foreach (var kvp in sources) {
			_markerSorter.AddOrdering(kvp.Value.StrMarkers);
		}
		return new Dictionary<int, OriginalStrDataSource>(sources);
	}
}