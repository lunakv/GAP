using Users.GenData;
using Users.GenData.Conflict;
using Users.GenData.Conflict.Storage;

namespace Api.GenData.DataLoaders;

public class NullableStrConflictByUserIdDataLoader : BatchDataLoader<int, StrConflict?> {
	private readonly IStrConflictFetcher _strConflictFetcher;
	private readonly MarkerSorter _markerSorter;

	public NullableStrConflictByUserIdDataLoader(IBatchScheduler batchScheduler, IStrConflictFetcher strConflictFetcher, MarkerSorter markerSorter, DataLoaderOptions? options = null)
		: base(batchScheduler, options) {
		_strConflictFetcher = strConflictFetcher;
		_markerSorter = markerSorter;
	}

	protected override async Task<IReadOnlyDictionary<int, StrConflict?>> LoadBatchAsync(IReadOnlyList<int> keys, CancellationToken cancellationToken) {
		ICollection<StrConflict> conflicts = await _strConflictFetcher.TryGetConflictsAsync(userIds: keys);
		await _markerSorter.LoadOrderingAsync();
		conflicts = conflicts.Select(conflict => new StrConflict(
				userId: conflict.UserId,
				conflictingMarkers: _markerSorter.FixMarkerNames(conflict.ConflictingMarkers)
			)
		).ToList();
		
		Dictionary<int, StrConflict?> conflictMap = conflicts.ToDictionary<StrConflict, int, StrConflict?>(conflict => conflict.UserId, conflict => conflict);
		foreach (int key in keys) {
			conflictMap.TryAdd(key: key, value: null);
		}

		return conflictMap;
	}
}