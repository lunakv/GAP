using Users.GenData.Conflict;
using Users.GenData.Conflict.Storage;

namespace Api.GenData.DataLoaders; 

public class StrConflictByUserIdDataLoader : BatchDataLoader<int, StrConflict> {
	private readonly IStrConflictFetcher _strConflictFetcher;

	public StrConflictByUserIdDataLoader(IBatchScheduler batchScheduler, IStrConflictFetcher strConflictFetcher, DataLoaderOptions? options = null)
		: base(batchScheduler, options) {
		_strConflictFetcher = strConflictFetcher;
	}
	protected override async Task<IReadOnlyDictionary<int, StrConflict>> LoadBatchAsync(IReadOnlyList<int> keys, CancellationToken cancellationToken) {
		ICollection<StrConflict> conflicts = await _strConflictFetcher.GetConflictsAsync(userIds: keys);
		return conflicts.ToDictionary(conflict => conflict.UserId, conflict => conflict);
	}
}