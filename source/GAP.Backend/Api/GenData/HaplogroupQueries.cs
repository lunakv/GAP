﻿using Api.Logging;
using Users.GenData.Haplogroup;
using Users.GenData.Haplogroup.Storage;
using Users.UserAccess;

namespace Api.GenData;

[ExtendObjectType("Query")]
public class HaplogroupQueries {
	public async Task<IList<Haplogroup>> GetHaplogroups([Service] IHaplogroupFetcher haplogroupFetcher, [Service] EndpointLogger<HaplogroupQueries> logger) {
		logger.LogPublicEndpointContext();
		IList<Haplogroup> haplogroups = await haplogroupFetcher.GetHaplogroupsAsync();
		logger.LogSuccessfulRequest();
		return haplogroups;
	}

	public async Task<Haplogroup> GetHaplogroup(int haplogroupId, [Service] IHaplogroupFetcher haplogroupFetcher, [Service] EndpointLogger<HaplogroupQueries> logger) {
		logger.LogPublicEndpointContext();
		Haplogroup haplogroup = await haplogroupFetcher.FindHaplogroupByIdAsync(haplogroupId);
		logger.LogSuccessfulRequest();
		return haplogroup;
	}
}