﻿using Api.Logging;
using Api.Map.Inputs;
using Map;
using Map.Map;
using Map.ValueObjects;
using PhyloTree;
using RegionUserFilterInput = Api.Map.Inputs.RegionUserFilterInput;

namespace Api.Map;

/// <summary>
/// Query graphql endpoint which defines haplogroup map and administrative regions queries.
/// </summary>
[ExtendObjectType("Query")]
public class MapQueries {
	[UseFiltering]
	public async Task<MapRegionLayer> GetRegionLayerAsync(int layerId, RegionUserFilterInput? userFilterInput, [Service] MapController mapController, [Service] PhyloTreeController treeController,
		[Service] EndpointLogger<MapQueries> logger) {
		logger.LogPublicEndpointContext();
		logger.Logger.LogInformation("Request arguments: layerId {LayerId}, userFilter {@UserFilter}", layerId, userFilterInput);
		var userFilter = new global::Map.ValueObjects.RegionUserFilterInput(
			haplogroupIds: userFilterInput?.HaplogroupIds ?? new List<int>(),
			familyNames: userFilterInput?.FamilyNames ?? new List<string>()
		);
		MapRegionLayer layer = await mapController.GetLayerAsync(layerId, userFilter, treeController);
		logger.LogSuccessfulRequest();
		return layer;
	}

	public async Task<MapRegion> GetRegionDetailAsync(int regionId, RegionUserFilterInput? userFilterInput, [Service] MapController mapController, [Service] PhyloTreeController treeController,
		[Service] EndpointLogger<MapQueries> logger) {
		logger.LogPublicEndpointContext();
		logger.Logger.LogInformation("Request arguments: regionId {RegionId}, userFilter {@UserFilter}", regionId, userFilterInput);
		var userFilter = new global::Map.ValueObjects.RegionUserFilterInput(
			haplogroupIds: userFilterInput?.HaplogroupIds ?? new List<int>(),
			familyNames: userFilterInput?.FamilyNames ?? new List<string>()
		);
		MapRegion region = await mapController.GetRegionDetailAsync(regionId, userFilter, treeController);
		logger.LogSuccessfulRequest();
		return region;
	}

	public async Task<IList<AdministrativeRegion>> GetAdministrativeRegions(AdministrativeRegion.RegionType regionType,
		[Service] MapController mapController, [Service] EndpointLogger<MapQueries> logger) {
		logger.LogPublicEndpointContext();
		logger.Logger.LogInformation("Request arguments: regionType {RegionType}.", regionType);
		IList<AdministrativeRegion> regions = await mapController.GetAdministrativeRegionsAsync(regionType: regionType);
		logger.LogSuccessfulRequest();
		return regions;
	}
}