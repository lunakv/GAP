namespace Api.Map.Inputs;

public class RegionUserFilterInput {
	public IReadOnlyCollection<int>? HaplogroupIds { get; set; }
	public IReadOnlyCollection<string>? FamilyNames { get; set; }
}