using Api.GenData.Types;
using Map.Map;
using Map.ValueObjects;

namespace Api.Map.Types;

public class HaplogroupCountType : ObjectType<HaplogroupCount> {
	private const string HaplogroupCountTypeName = "HaplogroupCount";

	protected override void Configure(IObjectTypeDescriptor<HaplogroupCount> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(HaplogroupCountTypeName);

		descriptor.Field(haplogroupCount => haplogroupCount.Haplogroup)
			.Type<NonNullType<HaplogroupType>>();
		descriptor.Field(haplogroupCount => haplogroupCount.Count)
			.Type<NonNullType<IntType>>();
	}
}