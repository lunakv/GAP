using Map.Map;

namespace Api.Map.Types; 

public class MapRegionLayerType : ObjectType<MapRegionLayer> {
	private const string MapRegionLayerTypeName = "RegionLayer";
	private const string IdPropertyName = "layerId";
	
	protected override void Configure(IObjectTypeDescriptor<MapRegionLayer> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(MapRegionLayerTypeName);

		descriptor.Field(mapRegionLayer => mapRegionLayer.Id)
			.Name(IdPropertyName)
			.Type<NonNullType<IntType>>();
		
		descriptor.Field(mapRegionLayer => mapRegionLayer.Regions)
			.Type<NonNullType<ListType<NonNullType<MapRegionType>>>>();
	}
}
