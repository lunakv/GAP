using Map.Map;
using Map.ValueObjects;

namespace Api.Map.Types;

public class FamilyNameCountType : ObjectType<FamilyNameCount> {
	private const string FamilyNameCountTypeName = "FamilyNameCount";

	protected override void Configure(IObjectTypeDescriptor<FamilyNameCount> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(FamilyNameCountTypeName);

		descriptor.Field(familyNameCount => familyNameCount.FamilyName)
			.Type<NonNullType<StringType>>();

		descriptor.Field(familyNameCount => familyNameCount.Count)
			.Type<NonNullType<IntType>>();
	}
}