using Map.ValueObjects;

namespace Api.Map.Types; 

public class AdministrativeRegionType : ObjectType<AdministrativeRegion> {
	private const string AdministrativeRegionTypeName = "AdministrativeRegion";

	protected override void Configure(IObjectTypeDescriptor<AdministrativeRegion> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(AdministrativeRegionTypeName);

		descriptor.Field(region => region.Id)
			.Type<NonNullType<IntType>>();
		descriptor.Field(region => region.Type)
			.Type<NonNullType<EnumType<AdministrativeRegion.RegionType>>>();
		descriptor.Field(region => region.Name)
			.Type<NonNullType<StringType>>();
		descriptor.Field(region => region.ParentRegionId)
			.Type<IntType>();
	}
}