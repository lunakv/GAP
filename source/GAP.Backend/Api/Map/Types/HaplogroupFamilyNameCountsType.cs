using Api.GenData.Types;
using Map.Map;

namespace Api.Map.Types;

public class HaplogroupFamilyNameCountsType : ObjectType<HaplogroupFamilyNameCounts> {
	private const string HaplogroupFamilyNameCountsTypeName = "HaplogroupFamilyNameCounts";

	protected override void Configure(IObjectTypeDescriptor<HaplogroupFamilyNameCounts> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(HaplogroupFamilyNameCountsTypeName);

		descriptor.Field(haplogroupFamilyNameCounts => haplogroupFamilyNameCounts.Haplogroup)
			.Type<NonNullType<HaplogroupType>>();
		descriptor.Field(haplogroupFamilyNameCounts => haplogroupFamilyNameCounts.FamilyNameCounts)
			.Type<NonNullType<ListType<NonNullType<FamilyNameCountType>>>>();
	}
}