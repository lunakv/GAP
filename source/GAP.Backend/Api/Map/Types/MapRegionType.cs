using Auth.Policies;
using Map.Map;
using Map.ValueObjects;

namespace Api.Map.Types;

public class MapRegionType : ObjectType<MapRegion> {
	private const string MapRegionTypeName = "Region";
	private const string UserCountPropertyName = "userCount";
	private const string HaplogroupFamilyNameCountsPropertyName = "haplogroupFamilyNameCounts";
	private const string HaplogroupCountsPropertyName = "haplogroupUserCounts";

	protected override void Configure(IObjectTypeDescriptor<MapRegion> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(MapRegionTypeName);

		descriptor.Field(mapRegion => mapRegion.Region.AdministrativeRegion.Id)
			.Type<NonNullType<IntType>>();
		descriptor.Field(mapRegion => mapRegion.Region.AdministrativeRegion.Name)
			.Type<NonNullType<StringType>>();
		descriptor.Field(mapRegion => mapRegion.GetUserCount())
			.Name(UserCountPropertyName)
			.Type<NonNullType<IntType>>();
		descriptor.Field(HaplogroupFamilyNameCountsPropertyName)
			.Resolve(ctx => ctx.Parent<MapRegion>().GetHaplogroupFamilyNameCounts().Where(hfnc => hfnc.FamilyNameCounts.Count > 0))
			.Type<NonNullType<ListType<NonNullType<HaplogroupFamilyNameCountsType>>>>()
			.Authorize(policy: nameof(IsUiUserPolicy));
		descriptor.Field(HaplogroupCountsPropertyName)
			.Resolve(ctx => ctx.Parent<MapRegion>().GetHaplogroupCounts().Where(hc => hc.Count > 0))
			.Type<NonNullType<ListType<NonNullType<HaplogroupCountType>>>>();
	}
}