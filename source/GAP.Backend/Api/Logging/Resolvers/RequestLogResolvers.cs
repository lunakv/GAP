using Api.Users.DataLoaders;
using Logging;

namespace Api.Logging.Resolvers;

class RequestLogResolvers {
	public async Task<object?> GetAboutUserAsync([Parent] RequestLog requestLog, UserByIdDataLoader userByIdDataLoader, StafferByIdDataloader stafferByIdDataloader) {
		if (requestLog.AboutUser == null) {
			return null;
		}

		if (requestLog.AboutUser.Staffer) {
			return await stafferByIdDataloader.LoadAsync(key: requestLog.AboutUser.Id);
		} else {
			return await userByIdDataLoader.LoadAsync(key: requestLog.AboutUser.Id);
		}
	}

	public async Task<object> GetPerformedByUserAsync([Parent] RequestLog requestLog, UserByIdDataLoader userByIdDataLoader, StafferByIdDataloader stafferByIdDataloader) {
		if (requestLog.PerformedByUser.Staffer) {
			return await stafferByIdDataloader.LoadAsync(key: requestLog.PerformedByUser.Id);
		} else {
			return await userByIdDataLoader.LoadAsync(key: requestLog.PerformedByUser.Id);
		}
	}
}