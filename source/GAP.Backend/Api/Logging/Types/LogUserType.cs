using Api.Auth.Identity.Types;
using Api.Users.Types;
using Logging;

namespace Api.Logging.Types;

public class LogUserType : UnionType {
	private const string LogUserTypeName = "LogUser";

	protected override void Configure(IUnionTypeDescriptor descriptor) {
		descriptor.Name(LogUserTypeName);

		descriptor.Type<StafferType>();
		descriptor.Type<UserType>();
	}
}