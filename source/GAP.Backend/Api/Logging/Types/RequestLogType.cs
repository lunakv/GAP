using Api.Logging.Resolvers;
using Api.Users;
using Api.Users.DataLoaders;
using Logging;

namespace Api.Logging.Types;

public class RequestLogType : ObjectType<RequestLog> {
	private const string RequestLogTypeName = "RequestLog";

	protected override void Configure(IObjectTypeDescriptor<RequestLog> descriptor) {
		descriptor.BindFieldsExplicitly();

		descriptor.Name(RequestLogTypeName);

		descriptor.Field(requestLog => requestLog.RequestId)
			.Type<NonNullType<StringType>>();
		descriptor.Field(requestLog => requestLog.PerformedByUser)
			.Type<NonNullType<LogUserType>>()
			.ResolveWith<RequestLogResolvers>(r => r.GetPerformedByUserAsync(default!, default!, default!));
		descriptor.Field(requestLog => requestLog.AboutUser)
			.Type<LogUserType>()
			.ResolveWith<RequestLogResolvers>(r => r.GetAboutUserAsync(default!, default!, default!));
		descriptor.Field(requestLog => requestLog.Endpoint)
			.Type<NonNullType<StringType>>();
		descriptor.Field(requestLog => requestLog.Date)
			.Type<NonNullType<DateTimeType>>();
		descriptor.Field(requestLog => requestLog.RequestSuccessful)
			.Type<NonNullType<BooleanType>>();
		descriptor.Field(requestLog => requestLog.Raw)
			.Type<NonNullType<StringType>>();
	}
}