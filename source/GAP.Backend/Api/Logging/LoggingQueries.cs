using Api.Logging.Inputs;
using Auth.Policies;
using HotChocolate.AspNetCore.Authorization;
using Logging;

namespace Api.Logging; 

[ExtendObjectType("Query")]
public class LoggingQueries {
	
	[UseOffsetPaging(IncludeTotalCount = true)]
	[UseSorting]
	[Authorize(Policy = nameof(IsAdminPolicy))]
	public async Task<IList<RequestLog>> GetUserLogs(LogFilterInput? logFilter, bool? includeRawLogs, [Service] LoggingController loggingController, [Service] EndpointLogger<LoggingQueries> logger) {
		logger.LogAuthEndpointContext();
		logger.Logger.LogInformation("Request arguments: {@LogFilter}, {IncludeRawLogs}", logFilter, includeRawLogs);
		logFilter ??= new LogFilterInput() { AboutUserEmail = null, PerformerEmail = null, From = null, To = null };
		IList<RequestLog> logs = await loggingController.GetUserLogsAsync(logFilter: logFilter.ToLogFilter(), includeRawLogs: includeRawLogs ?? false);
		logger.LogSuccessfulRequest();
		return logs;
	}
	
	[UseOffsetPaging(IncludeTotalCount = true)]
	[UseSorting]
	[Authorize(Policy = nameof(IsRootPolicy))]
	public async Task<IList<RequestLog>> GetAnyUserLogs(LogFilterInput? logFilter, bool? includeRawLogs, [Service] LoggingController loggingController, [Service] EndpointLogger<LoggingQueries> logger) {
		logger.LogAuthEndpointContext();
		logger.Logger.LogInformation("Request arguments: {@LogFilter}, {IncludeRawLogs}", logFilter, includeRawLogs);
		logFilter ??= new LogFilterInput() { AboutUserEmail = null, PerformerEmail = null, From = null, To = null };
		IList<RequestLog> logs = await loggingController.GetAnyUserLogsAsync(logFilter: logFilter.ToLogFilter(), includeRawLogs: includeRawLogs ?? false);
		logger.LogSuccessfulRequest();
		return logs;
	}
	
	[Authorize(Policy = nameof(IsAdminPolicy))]
	public async Task<IList<string>> GetLoggedEndpointNames([Service] LoggingController loggingController, [Service] EndpointLogger<LoggingQueries> logger) {
		logger.LogAuthEndpointContext();
		IList<string> endpointNames = await loggingController.GetEndpointNamesAsync();
		logger.LogSuccessfulRequest();
		return endpointNames;
	}
}