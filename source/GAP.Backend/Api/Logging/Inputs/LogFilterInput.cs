using Logging;

namespace Api.Logging.Inputs;

public class LogFilterInput {
	public string? PerformerEmail { get; set; }
	public string? AboutUserEmail { get; set; }
	public DateTime? From { get; set; }
	public DateTime? To { get; set; }
	public string? RequestId { get; set; }
	public List<string>? EndpointNames { get; set; }
	public bool? RequestSuccessful { get; set; }

	public LogFilter ToLogFilter() {
		return new LogFilter(
			performerEmail: PerformerEmail,
			aboutUserEmail: AboutUserEmail,
			from: From,
			to: To,
			requestId: RequestId,
			endpointNames: EndpointNames,
			requestSuccessful: RequestSuccessful
		);
	}
}