using System.Runtime.CompilerServices;
using Api.Exceptions;
using Auth.Identity;
using Auth.Identity.Claims;
using Auth.Policies.AuthorizationHandlers;

namespace Api.Logging;

public class EndpointLogger<TLogCategory> {
	private readonly IHttpContextAccessor _contextAccessor;
	public ILogger<TLogCategory> Logger { get; }

	public EndpointLogger(IHttpContextAccessor contextAccessor, ILogger<TLogCategory> logger) {
		_contextAccessor = contextAccessor;
		Logger = logger;
	}

	public void LogAuthEndpointContext(int? aboutUserId = null, [CallerMemberName] string caller = "caller") {
		if (_contextAccessor.HttpContext == null) {
			throw new HttpContextNotAccessibleException($"HttpContext is not accessible.");
		}

		int userId = new ClaimsRetriever().GetClaimUserId(_contextAccessor.HttpContext.User);
		bool isAdmin = new ClaimsRetriever().IsAdmin(_contextAccessor.HttpContext.User);
		Logger.LogInformation("User {PerformedByUser} (isAdmin: {Admin}) accessed {Endpoint}.", userId, isAdmin, caller);
		Logger.LogInformation("Endpoint accessed about user {AboutUser}", aboutUserId);
	}
	
	public void LogAuthEndpointContextAboutUserIsPerformer([CallerMemberName] string caller = "caller") {
		if (_contextAccessor.HttpContext == null) {
			throw new HttpContextNotAccessibleException($"HttpContext is not accessible.");
		}
		int aboutUserId = new ClaimsRetriever().GetClaimUserId(_contextAccessor.HttpContext.User);
		LogAuthEndpointContext(aboutUserId: aboutUserId, caller: caller);
	}

	public void LogEndpointContext(int performedByUserId, int aboutUserId, [CallerMemberName] string caller = "caller") {
		Logger.LogInformation("User {PerformedByUser} accessed {Endpoint}.", performedByUserId, caller);
		Logger.LogInformation("Endpoint accessed about user {AboutUser}", aboutUserId);
	}

	public void LogPublicEndpointContext([CallerMemberName] string caller = "caller") {
		Logger.LogInformation("User accessed {Endpoint}.", caller);
	}

	public void LogSuccessfulRequest() {
		Logger.LogInformation("Request outcome is success: {RequestSuccessful}.", true);
	}
}