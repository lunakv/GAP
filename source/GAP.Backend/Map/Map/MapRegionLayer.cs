using Map.ValueObjects;
using Users.UserAccess.Entities;

namespace Map.Map;

/// <summary>
/// Layer of <see cref="MapRegion"/>s.
/// </summary>
public class MapRegionLayer {
	public int Id { get; }
	public IList<MapRegion> Regions { get; }

	public MapRegionLayer(int id, IList<MapRegion> regions) {
		Id = id;
		Regions = regions;
	}
}