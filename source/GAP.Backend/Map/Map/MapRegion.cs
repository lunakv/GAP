using Map.ValueObjects;
using Users.GenData.Haplogroup;

namespace Map.Map;

public record FamilyNameCount(string FamilyName, int Count);

public record HaplogroupFamilyNameCounts(Haplogroup Haplogroup, IList<FamilyNameCount> FamilyNameCounts);

public record HaplogroupCount(Haplogroup Haplogroup, int Count);

/// <summary>
/// Region of haplogroup map which is capable to compute haplogroup and family names distributions.
/// </summary>
public class MapRegion {
	public HierarchicalRegion Region { get; }

	private readonly IList<MapUser> _users;
	private readonly IList<Haplogroup> _haplogroups;

	public MapRegion(HierarchicalRegion region, IList<MapUser> users, IList<Haplogroup> haplogroups) {
		Region = region;
		_users = users;
		_haplogroups = haplogroups;
	}

	public IList<HaplogroupCount> GetHaplogroupCounts() {
		IDictionary<Haplogroup, IList<MapUser>> dist = GetHaplogroupDistribution();
		return dist.Select(kvp =>
			new HaplogroupCount(Haplogroup: kvp.Key, Count: kvp.Value.Count)).ToList();
	}

	public IList<HaplogroupFamilyNameCounts> GetHaplogroupFamilyNameCounts() {
		IDictionary<Haplogroup, IList<MapUser>> dist = GetHaplogroupDistribution();

		return dist.Select(kvp => new HaplogroupFamilyNameCounts(
				Haplogroup: kvp.Key,
				FamilyNameCounts: GetFamilyNameCounts(kvp.Value)))
			.ToList();
	}

	public int GetUserCount() {
		return _users.Count;
	}

	public IReadOnlyCollection<MapUser> GetUsers() => _users.ToList();

	private IList<FamilyNameCount> GetFamilyNameCounts(IList<MapUser> users) {
		IDictionary<string, IList<MapUser>> dist = GetFamilyNameDistribution(users);
		return dist.Select(kvp => new FamilyNameCount(FamilyName: kvp.Key, Count: kvp.Value.Count)).ToList();
	}

	private IDictionary<Haplogroup, IList<MapUser>> GetHaplogroupDistribution() {
		Dictionary<Haplogroup, IList<MapUser>> distribution = _haplogroups
			.ToDictionary(h => h, h => (IList<MapUser>)new List<MapUser>());

		foreach (MapUser user in _users) {
			distribution[user.Haplogroup].Add(user);
		}

		return distribution;
	}

	private IDictionary<string, IList<MapUser>> GetFamilyNameDistribution(IList<MapUser> users) {
		Dictionary<string, IList<MapUser>> distribution = users
			.Select(u => u.FamilyName)
			.Distinct()
			.ToDictionary(familyName => familyName, familyName => (IList<MapUser>)new List<MapUser>());

		foreach (MapUser user in users) {
			distribution[user.FamilyName].Add(user);
		}

		return distribution;
	}
}