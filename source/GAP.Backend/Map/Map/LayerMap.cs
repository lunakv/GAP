using System.ComponentModel;
using Map.MapFetcher;
using Map.ValueObjects;
using Users.GenData.Haplogroup;

namespace Map.Map; 

/// <summary>
/// Implementation of <see cref="IMap"/> interface which stores region as list of layers.
/// </summary>
public class LayerMap : IMap {
	private readonly IList<HierachicalRegionLayer> _layers;
	private readonly IList<MapUser> _users;
	private readonly IList<Haplogroup> _haplogroups;

	public LayerMap(IList<HierachicalRegionLayer> layers, IList<MapUser> users, IList<Haplogroup> haplogroups) {
		_layers = layers;
		_users = users;
		_haplogroups = haplogroups;
	}
	
	public MapRegionLayer GetLayer(int layerId) {
		HierachicalRegionLayer? layer = _layers.FirstOrDefault(l => l?.Id == layerId, defaultValue: null);
		if (layer == null) {
			throw new ArgumentNullException($"Layer {layerId} not present in Map.");
		}

		IDictionary<int, IList<MapUser>> regionUsers = GetUsersForLayerRegions(layer); 

		return new MapRegionLayer(
			id: layer.Id,
			regions: layer.Regions.Select(r => new MapRegion(
					region: r,
					users: regionUsers[r.AdministrativeRegion.Id],
					haplogroups: _haplogroups
				)
			).ToList()
		);
	}

	public MapRegion GetRegion(int regionId) {
		HierarchicalRegion? region = _layers.SelectMany(l => l.Regions).FirstOrDefault(r => r?.AdministrativeRegion.Id == regionId, defaultValue: null);
		if (region == null) {
			throw new ArgumentNullException($"Region {regionId} not present in Map.");
		}
		
		return new MapRegion(
			region: region,
			users: GetUsersForRegion(region).ToList(),
			haplogroups: _haplogroups
		);
	}

	private IEnumerable<(MapUser User, int RegionId)> MapUsersToLayerRegions(HierachicalRegionLayer layer) {
		return
			from r in layer.Regions.SelectMany(r =>
				r.Subregions.Select(subregion => new { r.AdministrativeRegion.Id, subregion }))
			join user in _users on r.subregion.Id equals user.RegionId
			select (user, r.Id);
	}

	private Dictionary<int, IList<MapUser>> GetUsersForLayerRegions(HierachicalRegionLayer layer) {
		var usersWithRegionIds = MapUsersToLayerRegions(layer);

		Dictionary<int, IList<MapUser>> regionUsers = layer.Regions.ToDictionary(
			keySelector: r => r.AdministrativeRegion.Id,
			elementSelector: r => (IList<MapUser>)new List<MapUser>()
		);

		foreach (var userWithRegionId in usersWithRegionIds) {
			regionUsers[userWithRegionId.RegionId].Add(userWithRegionId.User);
		}

		return regionUsers;
	}

	private IEnumerable<MapUser> GetUsersForRegion(HierarchicalRegion region) {
		return
			from subregion in region.Subregions
			join user in _users on subregion.Id equals user.RegionId
			select user;
	}
}