namespace Map.Map; 

/// <summary>
/// Interface for representing a haplogroup map filled with regions which can compute haplogroup statistics.
/// </summary>
public interface IMap {
	public MapRegionLayer GetLayer(int layerId);
	public MapRegion GetRegion(int regionId);
}