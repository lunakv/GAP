using Users.Exceptions;

namespace Map.Exceptions; 

public class LayerNotFoundException : UserException {
	public LayerNotFoundException(string message) : base(message) { }
	public override string GetRepresentation => nameof(LayerNotFoundException);
}