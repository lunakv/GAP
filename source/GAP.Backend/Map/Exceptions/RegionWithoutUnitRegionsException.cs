namespace Map.Exceptions;

public class RegionWithoutUnitRegionsException : Exception {
	public RegionWithoutUnitRegionsException(string message) : base(message) { }
}