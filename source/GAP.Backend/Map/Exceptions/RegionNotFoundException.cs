using Users.Exceptions;

namespace Map.Exceptions; 

public class RegionNotFoundException : UserException {
	public RegionNotFoundException(string message) : base(message) { }
	public override string GetRepresentation => nameof(RegionNotFoundException);
}