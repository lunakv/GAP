using Map.ValueObjects;

namespace Map.RegionDataSaver;

public interface IAdministrativeRegionLayerSaver {
	void AddLayers(IList<AdministrativeRegionLayer> layers);
}