using Map.ValueObjects;

namespace Map.RegionDataSaver;

/// <summary>
/// Adds administrative layers to storage but also configures adding an artificial global region layer with one region.
/// </summary>
public class AdministrativeRegionLayerSaver : IAdministrativeRegionLayerSaver {
	private readonly IAdministrativeRegionLayerStorageSaver _administrativeRegionLayerStorageSaver;
	private const string GlobalRegionName = "Global";

	public AdministrativeRegionLayerSaver(IAdministrativeRegionLayerStorageSaver administrativeRegionLayerStorageSaver) {
		_administrativeRegionLayerStorageSaver = administrativeRegionLayerStorageSaver;
	}

	public void AddLayers(IList<AdministrativeRegionLayer> layers) {
		// Create new global region.
		int maxId = layers.SelectMany(l => l.Regions).Select(r => r.Id).Max();
		AdministrativeRegion globalRegion = new AdministrativeRegion(
			id: maxId + 1,
			type: AdministrativeRegion.RegionType.Global,
			name: GlobalRegionName,
			parentRegionId: null
		);

		// Update the top level layer so that parent regions are set to the new global region.
		AdministrativeRegionLayer toplevelLayer = layers[^1];
		layers[^1] = new AdministrativeRegionLayer(
			id: toplevelLayer.Id,
			regions: toplevelLayer.Regions.Select(r => new AdministrativeRegion(
				id: r.Id,
				type: r.Type,
				name: r.Name,
				parentRegionId: globalRegion.Id
			)).ToList()
		);

		// Add the global region along with its layer to layers.
		layers.Add(new AdministrativeRegionLayer(
			id: toplevelLayer.Id + 1,
			regions: new List<AdministrativeRegion> { globalRegion }
		));

		_administrativeRegionLayerStorageSaver.AddLayers(layers: layers);
	}
}