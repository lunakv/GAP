﻿using Map.ValueObjects;

namespace Map.RegionDataSaver;

/// <summary>
/// Presents a interface for saving layers of administrative region for storage gateway to implement.
/// </summary>
public interface IAdministrativeRegionLayerStorageSaver {
    void AddLayers(IList<AdministrativeRegionLayer> layers);
}
