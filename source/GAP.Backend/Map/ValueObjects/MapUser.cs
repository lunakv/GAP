using Users.GenData.Haplogroup;

namespace Map.ValueObjects;

public class MapUser {
	public int Id { get; }
	public string FamilyName { get; }
	public Haplogroup Haplogroup { get; }
	public int RegionId { get; }
	public string Municipality { get; }

	public MapUser(int id, string familyName, Haplogroup haplogroup, int regionId, string municipality) {
		Id = id;
		FamilyName = familyName;
		Haplogroup = haplogroup;
		RegionId = regionId;
		Municipality = municipality;
	}
}