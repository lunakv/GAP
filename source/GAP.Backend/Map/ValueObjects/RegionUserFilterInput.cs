﻿namespace Map.ValueObjects;

public class RegionUserFilterInput {
	public IReadOnlyCollection<int> HaplogroupIds { get; set; }
	public IReadOnlyCollection<string> FamilyNames { get; set; }

	public RegionUserFilterInput(IReadOnlyCollection<int> haplogroupIds, IReadOnlyCollection<string> familyNames) {
		HaplogroupIds = haplogroupIds;
		FamilyNames = familyNames;
	}
}