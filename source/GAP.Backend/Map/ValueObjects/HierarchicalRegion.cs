namespace Map.ValueObjects;

public class HierarchicalRegion {
	public AdministrativeRegion AdministrativeRegion { get; }
	public IList<AdministrativeRegion> Subregions { get; }

	public HierarchicalRegion(AdministrativeRegion region, IList<AdministrativeRegion> subregions) {
		AdministrativeRegion = region;
		Subregions = subregions;
	}
	
}