﻿namespace Map.ValueObjects;

/// <summary>
/// Represents an geographical administrative region. It should be a part of an real region hierarchy. 
/// </summary>
public class AdministrativeRegion {
	public int Id { get; set; }
	public RegionType Type { get; set; }
	public string Name { get; set; }
	public int? ParentRegionId { get; set; }
	
	public enum RegionType { Municipality, District, County, Country, Global }
	
	
	public AdministrativeRegion(int id, RegionType type, string name, int? parentRegionId) {
		Id = id;
		Type = type;
		Name = name;
		ParentRegionId = parentRegionId;
	}
}
