﻿namespace Map.ValueObjects;

public class AdministrativeRegionLayer {

    public int Id { get; }

    public IList<AdministrativeRegion> Regions { get; }

    public AdministrativeRegionLayer(int id, IList<AdministrativeRegion> regions) {
        Id = id;
        Regions = regions;
    }

}
