﻿namespace Map.ValueObjects;

public class HierachicalRegionLayer {
    
    public int Id { get; }
    public IList<HierarchicalRegion> Regions { get; }

    public HierachicalRegionLayer(int id, IList<HierarchicalRegion> regions) {
        Id = id;
        Regions = regions;
    }
    
}
