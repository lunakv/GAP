using Map.Map;
using Users.UserAccess.UserFilter;

namespace Map.MapFetcher; 

/// <summary>
/// Interface for getting haplogroup map.
/// </summary>
public interface IMapFetcher {
	Task<IMap> FetchMapAsync(UserFilter userFilter, IHaplogroupFilter haplogroupFilter);
}