using Map.Map;
using Map.MapFetcher.Storage;
using Map.ValueObjects;
using Users.GenData.Haplogroup;
using Users.GenData.Haplogroup.Storage;
using Users.UserAccess;
using Users.UserAccess.UserFilter;

namespace Map.MapFetcher;

/// <summary>
/// Fetcher for haplogroup map which is optimized to load regions as separate objects, not as layers.
/// </summary>
public class RegionMapFetcher {
	private readonly IRegionFetcher _regionFetcher;
	private readonly MapUserFetcher _userFetcher;
	private readonly IHaplogroupFetcher _haplogroupFetcher;

	public RegionMapFetcher(
		IRegionFetcher regionFetcher,
		MapUserFetcher userFetcher,
		IHaplogroupFetcher haplogroupFetcher) {
		_regionFetcher = regionFetcher;
		_userFetcher = userFetcher;
		_haplogroupFetcher = haplogroupFetcher;
	}

	public Task<IMap> FetchMapAsync(IList<int> regionIds)
		=> FetchMapAsync(regionIds: regionIds, userFilter: new EmptyUserFilter(), haplogroupFilter: new EmptyHaplogroupFilter());

	public async Task<IMap> FetchMapAsync(IList<int> regionIds, UserFilter userFilter, IHaplogroupFilter haplogroupFilter) {
		var fetchRegionsTask = regionIds.Select(regionId => _regionFetcher.GetHierarchicalRegionByIdAsync(regionId));
		var fetchUsersTask = _userFetcher.GetUsersAsync(userFilter: userFilter);
		var fetchHaplogroupsTask = _haplogroupFetcher.GetHaplogroupsAsync(haplogroupFilter: haplogroupFilter);

		IList<MapUser> fetchedUsers = await fetchUsersTask.ConfigureAwait(false);
		IList<Haplogroup> fetchedHaplogroups = await fetchHaplogroupsTask.ConfigureAwait(false);

		var regions = await Task.WhenAll(fetchRegionsTask)
			.ConfigureAwait(false);

		return new LayerMap(
			layers: new List<HierachicalRegionLayer> { new HierachicalRegionLayer(id: 0, regions: regions) },
			users: fetchedUsers,
			haplogroups: fetchedHaplogroups
		);
	}
}