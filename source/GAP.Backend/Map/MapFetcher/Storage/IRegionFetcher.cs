﻿using Map.ValueObjects;

namespace Map.MapFetcher.Storage;

/// <summary>
/// Interface for storage gateway which fetches saved administrative regions (<see cref="AdministrativeRegion"/>).
/// </summary>
public interface IRegionFetcher {
	Task<HierarchicalRegion> GetHierarchicalRegionByIdAsync(int id);
	Task<IList<AdministrativeRegion>> GetParentRegionsAsync(int unitRegionId);
	Task<AdministrativeRegion> GetMunicipalityRegionByName(string municipalityName);
	Task<IList<AdministrativeRegion>> GetAdministrativeRegionsAsync(AdministrativeRegion.RegionType regionType);
}