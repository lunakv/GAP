﻿using Map.ValueObjects;

namespace Map.MapFetcher.Storage;

/// <summary>
/// Interface for getting layers of administrative regions (<see cref="AdministrativeRegion"/>).
/// </summary>
public interface IRegionLayerFetcher {
    Task<IList<int>> GetLayerStructureAsync();

    Task<HierachicalRegionLayer> GetHierarchicalLayerAsync(int layerId);
}
