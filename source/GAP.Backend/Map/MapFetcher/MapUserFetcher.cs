using Map.ValueObjects;
using Users.UserAccess;
using Users.UserAccess.UserFilter;

namespace Map.MapFetcher;

/// <summary>
/// Implements retrieving user which have haplogroup. It server for transformation of users from map user objects so that map does not depend too
/// closely on generic user class.
/// </summary>
public class MapUserFetcher {
	private readonly CompleteUserFetcher _userFetcher;

	public MapUserFetcher(CompleteUserFetcher userFetcher) {
		_userFetcher = userFetcher;
	}

	public async Task<IList<MapUser>> GetUsersAsync(UserFilter userFilter) {
		var filter = new UserWithMunicipalityFilter()
			.Chain(userFilter)
			.Chain(new HaplogroupUserFilter());

		var users = await _userFetcher.GetUsersAsync(filter)
			.ConfigureAwait(false);

		return users.Select(u => new MapUser(id: u.Id, familyName: u.Profile.FamilyName,
				haplogroup: u.GeneticData.Haplogroup, regionId: u.RegionId, municipality: u.Profile.ResidenceAddress.Municipality!))
			.ToList();
	}
}