using Map.Map;
using Map.MapFetcher.Storage;
using Map.ValueObjects;
using Users.GenData.Haplogroup;
using Users.GenData.Haplogroup.Storage;
using Users.UserAccess;
using Users.UserAccess.UserFilter;

namespace Map.MapFetcher; 

/// <summary>
/// Fetcher for haplogroup map which is optimized to load regions as layers.
/// </summary>
public class LayerMapFetcher{
	private readonly IRegionLayerFetcher _hierarchicalRegionLayerFetcher;
	private readonly MapUserFetcher _userFetcher;
	private readonly IHaplogroupFetcher _haplogroupFetcher;

	public LayerMapFetcher(
		IRegionLayerFetcher hierarchicalRegionLayerFetcher,
		MapUserFetcher userFetcher,
		IHaplogroupFetcher haplogroupFetcher) {
		_hierarchicalRegionLayerFetcher = hierarchicalRegionLayerFetcher;
		_userFetcher = userFetcher;
		_haplogroupFetcher = haplogroupFetcher;
	}
	
	public Task<IMap> FetchMapAsync(IList<int> layerIds)
		=> FetchMapAsync(layerIds: layerIds, userFilter: new EmptyUserFilter(), haplogroupFilter: new EmptyHaplogroupFilter());
	
	public async Task<IMap> FetchMapAsync(IList<int> layerIds, UserFilter userFilter, IHaplogroupFilter haplogroupFilter) {
		var layerTasks = layerIds.Select(layerId => _hierarchicalRegionLayerFetcher.GetHierarchicalLayerAsync(layerId: layerId));
		var fetchUsersTask = _userFetcher.GetUsersAsync(userFilter: userFilter);
		var fetchHaplogroupsTask = _haplogroupFetcher.GetHaplogroupsAsync(haplogroupFilter: haplogroupFilter);

		var layers = await Task.WhenAll(layerTasks)
			.ConfigureAwait(false);
		IList<MapUser> users = await fetchUsersTask.ConfigureAwait(false);
		IList<Haplogroup> haplogroups = await fetchHaplogroupsTask.ConfigureAwait(false);

		return new LayerMap(
			layers: layers,
			users: users,
			haplogroups: haplogroups
		);

	}
}