﻿using Map.Map;
using Map.MapFetcher;
using Map.MapFetcher.Storage;
using Map.ValueObjects;
using PhyloTree;
using System.Net.NetworkInformation;
using Users.UserAccess.UserFilter;

namespace Map;

/// <summary>
/// Application Controller for haplogroup map and its administrative regions.
/// </summary>
public class MapController {
	private readonly LayerMapFetcher _layerMapFetcher;
	private readonly RegionMapFetcher _regionMapFetcher;
	private readonly IRegionFetcher _regionFetcher;

	public MapController(LayerMapFetcher layerMapFetcher, RegionMapFetcher regionMapFetcher, IRegionFetcher regionFetcher) {
		_layerMapFetcher = layerMapFetcher;
		_regionMapFetcher = regionMapFetcher;
		_regionFetcher = regionFetcher;
	}

	public async Task<MapRegionLayer> GetLayerAsync(int layerId, RegionUserFilterInput filter, PhyloTreeController phyloTreeController) {
		var (userFilter, haplogroupFilter) = ConvertFilterInputToFilter(filter, phyloTreeController);
		IMap map = await _layerMapFetcher.FetchMapAsync(layerIds: new int[] { layerId }, userFilter: userFilter, haplogroupFilter: haplogroupFilter)
			.ConfigureAwait(false);
		return map.GetLayer(layerId);
	}

	public async Task<MapRegion> GetRegionDetailAsync(int regionId, RegionUserFilterInput filter, PhyloTreeController phyloTreeController) {
		var (userFilter, haplogroupFilter) = ConvertFilterInputToFilter(filter, phyloTreeController);
		IMap map = await _regionMapFetcher.FetchMapAsync(regionIds: new int[] { regionId }, userFilter: userFilter, haplogroupFilter: haplogroupFilter)
			.ConfigureAwait(false);

		return map.GetRegion(regionId);
	}

	public Task<IList<AdministrativeRegion>> GetAdministrativeRegionsAsync(AdministrativeRegion.RegionType regionType) {
		return _regionFetcher.GetAdministrativeRegionsAsync(regionType: regionType);
	}

	private (UserFilter, IHaplogroupFilter) ConvertFilterInputToFilter(RegionUserFilterInput inputFilter, PhyloTreeController treeController, bool useHaplogroupRoots = true) {
		IReadOnlyCollection<int> ids;
		if(useHaplogroupRoots) {
			var tree = treeController.GetTreeUserless();
			ids = inputFilter.HaplogroupIds
				.Select(h => tree.NodeForHaplogroup(h).Id)
				.Select(n => tree.FullSubtreeOfNode(tree.Nodes[n]))
				.SelectMany(x => x)
				.Select(n => n.HaplogroupId)
				.ToList();
		} else {
			ids = inputFilter.HaplogroupIds;
		}
		return (
			new HaplogroupUserFilter(ids).Chain(new FamilyNameUserFilter(inputFilter.FamilyNames)),
			new IdHaplogroupFilter(ids)
		);
	}
}