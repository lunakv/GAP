using DataLoader;
using DataLoader.RealDataLoaders;
using System.Runtime.Serialization;

namespace OldDataParsingTests; 

[Collection(nameof(TestCollection))]
public class ResultingDataTests {
	const string Path = "data_loader_real_users.json";

	[Theory]
	[MemberData(nameof(GetSmokeData))]
	public void ProfileValuesTest(OldDataParseTestSource testSource) {
		var users = GetAllUsers();
		var u = users.FirstOrDefault(u => testSource.ReferenceUser.Profile.Email == u.Profile.Email);
		var refer = testSource.ReferenceUser;
		if(!testSource.UserExists) {
			Assert.Null(u);
			return;
		}
		Assert.NotNull(u);
		Assert.Equal(refer.Profile.GivenName, u.Profile.GivenName);
		Assert.Equal(refer.Profile.FamilyName, u.Profile.FamilyName);
		Assert.Equal(refer.Profile.Email, u.Profile.Email);
		Assert.Equal(refer.Profile.PhoneNumber, u.Profile.PhoneNumber);
		Assert.Equal(refer.Profile.ResidenceAddress.Town, u.Profile.ResidenceAddress.Town);
		Assert.Equal(refer.Profile.ResidenceAddress.Street, u.Profile.ResidenceAddress.Street);
		Assert.Equal(refer.Profile.ResidenceAddress.HouseNumber, u.Profile.ResidenceAddress.HouseNumber);
		if(refer.Profile.ResidenceAddress.ZipCode != null) //zip code may be set by the old data parser
			Assert.Equal(refer.Profile.ResidenceAddress.ZipCode, u.Profile.ResidenceAddress.ZipCode); 
	}

	[Theory]
	[MemberData(nameof(GetSmokeData))]
	public void AncestorValuesTest(OldDataParseTestSource testSource) {
		var users = GetAllUsers();
		var u = users.FirstOrDefault(u => testSource.ReferenceUser.Profile.Email == u.Profile.Email);
		var refer = testSource.ReferenceUser;
		if(!testSource.UserExists) {
			Assert.Null(u);
			return;
		}
		Assert.NotNull(u);
		Assert.Equal(refer.Ancestor.FamilyName, u.Ancestor.FamilyName);
		Assert.Equal(refer.Ancestor.PlaceOfDeath, u.Ancestor.PlaceOfDeath);
		Assert.Equal(refer.Ancestor.DeathDate, u.Ancestor.DeathDate);
	}
	
	[Theory]
	[MemberData(nameof(GetSmokeData))]
	public void GeneticDataValuesTest(OldDataParseTestSource testSource) {
		var users = GetAllUsers();
		var u = users.FirstOrDefault(u => testSource.ReferenceUser.Profile.Email == u.Profile.Email);
		var refer = testSource.ReferenceUser;
		if(!testSource.UserExists) {
			Assert.Null(u);
			return;
		}
		Assert.NotNull(u);
		Assert.Equal(refer.GeneticData.StrMarkers.Count, u.GeneticData.StrMarkers.Count);
		foreach(var k in refer.GeneticData.StrMarkers.Keys) {
			Assert.Contains(k, u.GeneticData.StrMarkers.Keys);
			Assert.Equal(refer.GeneticData.StrMarkers[k], u.GeneticData.StrMarkers[k]);
		}
	}

	private IList<AlreadyUser> GetAllUsers() 
		=> GetAllUsers(Path);

	private IList<AlreadyUser> GetAllUsers(string path) {
		return DataLoader.RealDataLoaders.OldUserDataSource.GetRawUsers(path) 
			?? throw new ArgumentException($"Users returned from data loader are null for path {path}");
	}

	public static IEnumerable<object[]> GetSmokeData() {
		yield return ExcelSmokeData();
		yield return XmlSmokeData1();
	}

	private static object[] ExcelSmokeData() {
		return new object[] {
			new OldDataParseTestSource(
				new AlreadyUser(
					new AlreadyProfile(
						GivenName: "John",
						FamilyName: "Smith",
						Email: "john.s@seznam.cz",
						PhoneNumber: null,
						ResidenceAddress: new AlreadyExpandedAddress(
							Town: "Praha",
							Municipality: null,
							County: null,
							Street: "Na po����",
							HouseNumber: "62/18",
							ZipCode: "12310"
						),
						CorrespondenceAddress: null,
						BirthDate: null
					),
					new AlreadyAncestor(
						address: new Users.UserAccess.Entities.ExpandedAddress(),
						placeOfDeath: "�stecko,Jablonec nad Nisou",
						deathDate: new DateOnly(1711, 1, 1)
					),
					new AlreadyGeneticData(
						new Dictionary<string, int>() {
							{"DYS393",			1	},
							{"DYS390",			2	},
							{"DYS19",			3	},
							{"DYS391",			4	},
							{"DYS385a",			5	},
							{"DYS385b",			6	},
							{"DYS426",			7	},
							{"DYS388",			8	},
							{"DYS439",			9	},
							{"DYS389I",			10	},
							{"DYS392",			11	},
							{"DYS389II",		12	},
							{"DYS458",			13	},
							{"DYS459",			14	},
							{"DYS455",			15	},
							{"DYS454",			16	},
							{"DYS447",			17	},
							{"DYS437",			18	},
							{"DYS448",			19	},
							{"DYS449",			20	},
							{"DYS464",			21	},
							{"DYS460",			22	},
							{"Y-GATA-H4",		23	},
							{"YCAII",			24	},
							{"DYS456",			1	},
							{"DYS607",			2	},
							{"DYS576",			3	},
							{"DYS570",			4	},
							{"CDY",				5	},
							{"DYS442",			6	},
							{"DYS438",			7	},
							{"DYS531",			1	},
							{"DYS578",			2	},
							{"DYF395S1",		3	},
							{"DYS590",			4	},
							{"DYS537",			5	},
							{"DYS641",			6	},
							{"DYS472",			7	},
							{"DYF406S1",		8	},
							{"DYS511",			9	},
							{"DYS425",			10	},
							{"DYS413",			11	},
							{"DYS557",			12	},
							{"DYS594",			13	},
							{"DYS436",			14	},
							{"DYS490",			15	},
							{"DYS534",			16	},
							{"DYS450",			17	},
							{"DYS444",			18	},
							{"DYS481",			19	},
							{"DYS520",			20	},
							{"DYS446",			21	},
							{"DYS617",			22	},
							{"DYS568",			23	},
							{"DYS487",			24	},
							{"DYS572",			25	},
							{"DYS640",			26	},
							{"DYS492",			1	},
							{"DYS565",			2	},
							{"DYS710",			3	},
							{"DYS485",			4	},
							{"DYS632",			5	},
							{"DYS495",			6	},
							{"DYS540",			7	},
							{"DYS714",			8	},
							{"DYS716",			9	},
							{"DYS717",			10	},
							{"DYS505",			11	},
							{"DYS556",			12	},
							{"DYS549",			13	},
							{"DYS589",			14	},
							{"DYS522",			15	},
							{"DYS494",			16	},
							{"DYS533",			17	},
							{"DYS636",			18	},
							{"DYS575",			19	},
							{"DYS638",			20	},
							{"DYS462",			21	},
							{"DYS452",			22	},
							{"DYS445",			23	},
							{"Y-GATA-A10",		24	},
							{"DYS463",			25	},
							{"DYS441",			26	},
							{"Y-GGAAT-1B07",	27	},
							{"DYS525",			28	},
							{"DYS712",			29	},
							{"DYS593",			30	},
							{"DYS650",			1	},
							{"DYS532",			2	},
							{"DYS715",			3	},
							{"DYS504",			4	},
							{"DYS513",			5	},
							{"DYS561",			6	},
							{"DYS552",			7	},
							{"DYS726",			8	},
							{"DYS635",			9	},
							{"DYS587",			10	},
							{"DYS643",			11	},
							{"DYS497",			12	},
							{"DYS510",			13	},
							{"DYS434",			14	},
							{"DYS461",			15	},
							{"DYS435",			16	}
						},
						null!
					)
				)
			)
		};
	}

	private static object[] XmlSmokeData1() {
		return new object[] {
			new OldDataParseTestSource(
				new AlreadyUser(
					new AlreadyProfile(
						GivenName: "Kamil",
						FamilyName: "Fajfka",
						Email: "fajfkak@gmail.com",
						PhoneNumber: "123 456 789",
						ResidenceAddress: new AlreadyExpandedAddress(
							Town: "T�nec nad S�zavou",
							Municipality: null,
							County: null,
							Street: "T�nec nad S�zavou",
							HouseNumber: "3",
							ZipCode: null
						),
						CorrespondenceAddress: null,
						BirthDate: null
					),
					new AlreadyAncestor(
						address: new Users.UserAccess.Entities.ExpandedAddress(),
						familyName: null,
						placeOfDeath: null,
						deathDate: null
					),
					new AlreadyGeneticData(
						new Dictionary<string, int>() {
							{"dys390",			11	},
							{"dys19",			2	},
							{"dys426",			1	},
							{"dys388",			0	},
							{"dys439",			21	},
							{"dys389I",			13	},
							{"dys392",			10	},
							{"dys389II",		18	},
							{"dys458",			15	},
							{"dys459",			0	},
							{"dys447",			1	},
							{"dys437",			12	},
							{"dys448",			21	},
							{"dys449",			0	},
						},
						null!
					)
				),
				userExists: true
			) 
		}; 
	}
}