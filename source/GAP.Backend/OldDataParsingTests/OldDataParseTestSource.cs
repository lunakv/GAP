﻿using DataLoader.RealDataLoaders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace OldDataParsingTests;
public class OldDataParseTestSource : TestUtils.TestSource {
	public AlreadyUser ReferenceUser { get; }
	public bool UserExists { get; }
	public OldDataParseTestSource(AlreadyUser referenceUser,
			bool userExists = true,
			[CallerMemberName] string testName = "No test name provided.") : base(testName) {
		ReferenceUser = referenceUser;
		UserExists = userExists;
	}
}
