namespace Logging; 

public class LogFilter {
	public string? PerformerEmail { get; set; }
	public string? AboutUserEmail { get; set; }
	public DateTime? From { get; set; }
	public DateTime? To { get; set; }
	public string? RequestId { get; set; }
	public IList<string> EndpointNames { get; }
	public bool? RequestSuccessful { get; set; }

	public LogFilter(string? performerEmail, string? aboutUserEmail, DateTime? from, DateTime? to, string? requestId, IList<string>? endpointNames, bool? requestSuccessful) {
		PerformerEmail = performerEmail;
		AboutUserEmail = aboutUserEmail;
		From = from;
		To = to;
		RequestId = requestId;
		EndpointNames = endpointNames ?? new List<string>();
		RequestSuccessful = requestSuccessful;
	}

	public bool NoFiltersSet() {
		return PerformerEmail == null
		       && AboutUserEmail == null
		       && From == null
		       && To == null 
		       && RequestId == null
		       && EndpointNames.Count == 0 
		       && RequestSuccessful == null;
	}
}