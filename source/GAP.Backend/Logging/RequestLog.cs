using Users.UserAccess.Entities;

namespace Logging; 

public class RequestLog {
	public string RequestId { get; }
	public LogUser PerformedByUser { get; }
	public LogUser? AboutUser { get; }
	public string Endpoint { get; }
	public DateTime Date { get; }
	public bool RequestSuccessful { get; }
	public string Raw { get; }
	
	public RequestLog(string requestId, LogUser performedByUser, LogUser? aboutUser, string endpoint, DateTime date, bool requestSuccessful, string raw) {
		RequestId = requestId;
		PerformedByUser = performedByUser;
		AboutUser = aboutUser;
		Endpoint = endpoint;
		Date = date;
		RequestSuccessful = requestSuccessful;
		Raw = raw;
	}
}