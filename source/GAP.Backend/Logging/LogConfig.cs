namespace Logging; 

public class LogConfig {
	public int DefaultDaysRequestTimeLimit { get; set; }

	public LogConfig() {
		DefaultDaysRequestTimeLimit = 0;
	}
}