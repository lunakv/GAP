using Users.Exceptions;

namespace Logging.Exceptions; 

public class AboutUserNotFoundException : UserException {
	public AboutUserNotFoundException(string message) : base(message) { }
	public AboutUserNotFoundException(string message, Exception? innerException) : base(message, innerException) { }
	public override string GetRepresentation => nameof(AboutUserNotFoundException);
}