using Users.Exceptions;

namespace Logging.Exceptions; 

public class PerformerUserNotFoundException : UserException {
	public PerformerUserNotFoundException(string message) : base(message) { }
	public PerformerUserNotFoundException(string message, Exception? innerException) : base(message, innerException) { }
	public override string GetRepresentation => nameof(PerformerUserNotFoundException);
}