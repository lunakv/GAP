using Auth.Identity;
using Auth.Identity.Storage;
using Logging.Exceptions;
using Logging.Storage;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Logging;

/// <summary>
/// Fetches mutation request logs from database and validates their content. Also maps users to ids in the logs. 
/// </summary>
public class LoggingController {
	private readonly ILogFetcher _logFetcher;
	private readonly IIdentityManager _identityManager;
	private readonly ILogger<LoggingController> _logger;
	private readonly LogConfig _logConfig;

	public LoggingController(ILogFetcher logFetcher, IIdentityManager identityManager, ILogger<LoggingController> logger, IOptions<LogConfig> logConfig) {
		_logFetcher = logFetcher;
		_identityManager = identityManager;
		_logger = logger;
		_logConfig = logConfig.Value;
	}

	public async Task<IList<RequestLog>> GetUserLogsAsync(LogFilter logFilter, bool includeRawLogs) {
		StorageLogFilter storageLogFilter = await ConstructStorageLogFilterAsync(logFilter).ConfigureAwait(false);

		IList<StorageRequestLog> logs = await _logFetcher.GetLogsAsync(logFilter: storageLogFilter, includeRawLogs: includeRawLogs).ConfigureAwait(false);

		return (
			await CreateValidRequestLogsAsync(logs: logs).ConfigureAwait(false)
		).Where(log => log.AboutUser is not { Staffer: true })
			.Where(log => !log.PerformedByUser.Staffer)
			.ToList();
	}

	public async Task<IList<RequestLog>> GetAnyUserLogsAsync(LogFilter logFilter, bool includeRawLogs) {
		StorageLogFilter storageLogFilter = await ConstructStorageLogFilterAsync(logFilter).ConfigureAwait(false);

		IList<StorageRequestLog> logs = await _logFetcher.GetLogsAsync(logFilter: storageLogFilter, includeRawLogs: includeRawLogs).ConfigureAwait(false);

		return await CreateValidRequestLogsAsync(logs: logs).ConfigureAwait(false);
	}

	public Task<IList<string>> GetEndpointNamesAsync() {
		return _logFetcher.GetEndpointNamesAsync();
	}

	private async Task<StorageLogFilter> ConstructStorageLogFilterAsync(LogFilter logFilter) {
		if (logFilter.NoFiltersSet()) {
			logFilter.From = DateTime.Now.Subtract(
				new TimeSpan(days: _logConfig.DefaultDaysRequestTimeLimit, hours: 0, minutes: 0, seconds: 0)
			);
		}

		int? performer = await TranslatePerformerEmailToIdAsync(performerEmail: logFilter.PerformerEmail).ConfigureAwait(false);
		int? aboutUser = await TranslateAboutUserEmailToIdAsync(aboutUserEmail: logFilter.AboutUserEmail).ConfigureAwait(false);

		return new StorageLogFilter(
			performerId: performer,
			aboutUser: aboutUser,
			from: logFilter.From,
			to: logFilter.To,
			requestId: logFilter.RequestId,
			endpointNames: logFilter.EndpointNames,
			requestSuccessful: logFilter.RequestSuccessful
		);
	}

	private async Task<int?> TranslatePerformerEmailToIdAsync(string? performerEmail) {
		if (performerEmail == null) {
			return null;
		}

		int? performerId = await _identityManager.GetUserIdAsync(email: performerEmail).ConfigureAwait(false);

		if (performerId != null) {
			return performerId;
		}

		throw new PerformerUserNotFoundException($"Perfomer user with email {performerEmail} not found.");
	}

	private async Task<int?> TranslateAboutUserEmailToIdAsync(string? aboutUserEmail) {
		if (aboutUserEmail == null) {
			return null;
		}

		int? aboutUserId = await _identityManager.GetUserIdAsync(email: aboutUserEmail).ConfigureAwait(false);

		if (aboutUserId != null) {
			return aboutUserId;
		}

		throw new AboutUserNotFoundException($"About user with email {aboutUserEmail} not found.");
	}

	private async Task<IList<RequestLog>> CreateValidRequestLogsAsync(IList<StorageRequestLog> logs) {
		var userIdsInLogs = logs.Select(log => log.AboutUser).Concat(logs.Select(log => log.PerformedByUser)).Where(user => user != null)
			.Cast<int>().Distinct().ToList();
		IDictionary<int, string> emailMapping = await _identityManager.GetEmailsAsync(userIds: userIdsInLogs).ConfigureAwait(false);
		ISet<int> staffIds = (
			await _identityManager.GetStaffAsync().ConfigureAwait(false)
		).Select(admin => admin.Id).ToHashSet();

		List<RequestLog> resultLogs = new();
		foreach (StorageRequestLog storageLog in logs) {
			RequestLog? log = CreateValidLog(log: storageLog, emailMapping: emailMapping, staffIds: staffIds);
			if (log != null) {
				resultLogs.Add(log);
			}
		}

		return resultLogs;
	}

	private RequestLog? CreateValidLog(StorageRequestLog log, IDictionary<int, string> emailMapping, ISet<int> staffIds) {
		if (log.RequestId == null) {
			_logger.LogError("Log with request id: {InvalidLogRequestId} missing RequestId.", log.RequestId);
			return null;
		}

		if (log.PerformedByUser == null) {
			_logger.LogError("Log with request id: {InvalidLogRequestId} missing PerformedByUser.", log.RequestId);
			return null;
		}

		if (log.Endpoint == null) {
			_logger.LogError("Log with request id: {InvalidLogRequestId} missing Endpoint.", log.RequestId);
			return null;
		}

		if (log.Date == null) {
			_logger.LogError("Log with request id: {InvalidLogRequestId} missing Date.", log.RequestId);
			return null;
		}

		if (log.Raw == null) {
			_logger.LogError("Log with request id: {InvalidLogRequestId} missing Raw.", log.RequestId);
			return null;
		}

		if (!emailMapping.ContainsKey(log.PerformedByUser.Value)) {
			_logger.LogError("Log with request id: {InvalidLogRequestId} has invalid PerformedByUser value {PerformedByUser}.",
				log.RequestId, log.PerformedByUser.Value
			);
			return null;
		}

		if (log.AboutUser != null && !emailMapping.ContainsKey(log.AboutUser.Value)) {
			_logger.LogError("Log with request id: {InvalidLogRequestId} has invalid AboutUser value {AboutUser}.",
				log.RequestId, log.AboutUser.Value
			);
			return null;
		}

		return new RequestLog(
			requestId: log.RequestId,
			performedByUser: new LogUser(
				id: log.PerformedByUser.Value,
				email: emailMapping[log.PerformedByUser.Value],
				staffer: staffIds.Contains(log.PerformedByUser.Value)
			),
			aboutUser: log.AboutUser == null
				? null
				: new LogUser(
					id: log.AboutUser.Value,
					email: emailMapping[log.AboutUser.Value],
					staffer: staffIds.Contains(log.AboutUser.Value)
				),
			endpoint: log.Endpoint,
			date: log.Date.Value,
			requestSuccessful: log.RequestSuccessful ?? false,
			raw: log.Raw
		);
	}
}