namespace Logging.Storage; 

public class StorageRequestLog {
	public string? RequestId { get; }
	public int? PerformedByUser { get; }
	public int? AboutUser { get; }
	public string? Endpoint { get; }
	public DateTime? Date { get; }
	public bool? RequestSuccessful { get; }
	public string? Raw { get; }
	
	public StorageRequestLog(string? requestId, int? performedByUser, int? aboutUser, string? endpoint, DateTime? date, bool? requestSuccessful, string? raw) {
		RequestId = requestId;
		PerformedByUser = performedByUser;
		AboutUser = aboutUser;
		Endpoint = endpoint;
		Date = date;
		RequestSuccessful = requestSuccessful;
		Raw = raw;
	}
}