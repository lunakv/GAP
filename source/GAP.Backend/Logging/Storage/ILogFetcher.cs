namespace Logging.Storage; 

public interface ILogFetcher {
	Task<IList<StorageRequestLog>> GetLogsAsync(StorageLogFilter logFilter, bool includeRawLogs);
	Task<IList<string>> GetEndpointNamesAsync();
}