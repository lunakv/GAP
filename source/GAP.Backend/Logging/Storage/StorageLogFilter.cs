namespace Logging.Storage; 

public class StorageLogFilter {
	public int? PerformerId { get; set; }
	public int? AboutUser { get; set; }

	private DateTime? _from;
	public DateTime? From {
		get => _from;
		set => _from = value?.ToUniversalTime();
	}

	private DateTime? _to;

	public DateTime? To {
		get => _to;
		set => _to = value?.ToUniversalTime();
	}
	public string? RequestId { get; set; }
	public IList<string> EndpointNames { get; }
	public bool? RequestSuccessful { get; set; }

	public StorageLogFilter(int? performerId, int? aboutUser, DateTime? from, DateTime? to, string? requestId, IList<string>? endpointNames, bool? requestSuccessful) {
		PerformerId = performerId;
		AboutUser = aboutUser;
		From = from;
		To = to;
		RequestId = requestId;
		EndpointNames = endpointNames ?? new List<string>();
		RequestSuccessful = requestSuccessful;
	}
}