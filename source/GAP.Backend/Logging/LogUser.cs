namespace Logging; 

public class LogUser {
	public int Id { get; }
	public string Email { get; }
	public bool Staffer { get; }
	
	public LogUser(int id, string email, bool staffer) {
		Id = id;
		Email = email;
		Staffer = staffer;
	}
}