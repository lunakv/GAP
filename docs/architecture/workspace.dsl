workspace "GAP System Workspace" "This workspace documents the architecture of the GAP system." {

    model {
        gapSystem = softwareSystem "GAP System" "GAP systems manages user STR data and provides STR testing." {
            clientContainer = container "Client" "Contains web UI app."

            serverContainer = container "Server" "Contains the backend business logic." {
            }

            mapDataFetcherContainer = container "Map Data Fetcher" "Program which fetches and preprocesses administrative regions." 

            dataLoaderContainer = container "Data Loader" "Program to initilize storages and populate them with data."
            emailServerContainer = container "Email Server" "Receives email from within the system and forwards them." "Postfix"
            group "Storage" {
                mapStorageContainer = container "Map Storage" "Stores administrative regions, hierarchy of administrative regions." "PostgreSQL"
                logStorageContainer = container "Log Storage" "Stores logs." "PostgreSQL"
                identityStorageContainer = container "Identity Storage" "Stores all user auth identities." "PostgreSQL"
                userStorageContainer = container "User Storage" "Stores user personal and genetic data." "PostgreSQL"
                phyloTreeStorageContainer = container "Phylogenetic Tree Storage" "Stores phylogenetic tree." "File System"
                familyTreeStorageContainer = container "Family Tree Storage" "Stores user family trees." "PostgreSQL"    
            }
        }

        ftdnaSystem = softwareSystem "FTDNA" "Commercial system which provides marker testing and genealogic features." "Existing System"
        nevgenSystem = softwareSystem "NevGen" "System for predicting haplogroups from STR markers." "Existing System"
        nodcSystem = softwareSystem "Czech National Open Data Catalog" "Stores and presents metadata about open data datasets in Czechia." "Existing System"
        
        root = person "Root" "Super admin who has unrestricted system access."
        admin = person "Admin" "Admin manages users, resolves conflicts, oversees lab tech work."
        labTech = person "Lab Tech" "Lab tech processes new testing requests, sends kits and processes samples."
        unregisteredUser = person "Unregistered User" "User who opens the GAP homepage but is not registered."
        registeredUser = person "Registered User" "Registered user with genetic data in GAP system."
        publicApiConsumer = person "Public API Consumer" "User who consumer genetic data through public API."

        root -> clientContainer "Uses"
        admin -> clientContainer "Uses"
        labTech -> clientContainer "Uses"
        unregisteredUser -> clientContainer "Uses"
        registeredUser -> clientContainer "Uses"
        publicApiConsumer -> serverContainer "Queries"

        serverContainer -> ftdnaSystem "Downloads current haplogroup phylogenetic tree."
        dataLoaderContainer -> ftdnaSystem "Downloads current haplogroup phylogenetic tree."
        serverContainer -> nevgenSystem "Predicts haplgroup based on STR markers.
        mapDataFetcherContainer -> nodcSystem "Downloads ruian administrative region data."
         
        clientContainer -> serverContainer "Sends requests."
        serverContainer -> emailServerContainer "Sends emails."
        dataLoaderContainer -> emailServerContainer "Sends emails."

        serverContainer -> identityStorageContainer "Accessses data"
        serverContainer -> mapStorageContainer "Accesses data"
        serverContainer -> userStorageContainer "Accesses data"
        serverContainer -> phyloTreeStorageContainer "Accesses data"
        serverContainer -> familyTreeStorageContainer "Accesses data"
        serverContainer -> logStorageContainer "Accesses data"

        dataLoaderContainer -> identityStorageContainer "Loads data"
        dataLoaderContainer -> mapStorageContainer "Loads data"
        dataLoaderContainer -> userStorageContainer "Loads data"
        dataLoaderContainer -> phyloTreeStorageContainer "Loads data"
        dataLoaderContainer -> familyTreeStorageContainer "Loads data"
        

    }
    views {
    systemContext gapSystem "Gap-System-View" {
        include *
    }

    container gapSystem "Gap-Container-View" {
        include *
        
    }

    # component serverContainer "Server-Component-View" {
    #     include *
    # }

    styles {
        element "Element" {
            fontSize 32
        }

        relationship "Relationship" {
            fontSize 30
        }

        element "Group" {
            fontSize 30
        }

           element "Existing System" {
                background #999999
            }

    }

        

    theme default
}
