GET {SERVER_ADDRESS}/laboratory/label

Header:
  Authorization:
    Bearer Token

Body:
  Form-data (as bulk edit option from Postman):

	addressLabel:2
	sampleLabel:2
	labelLayout:LabelLayout63X38
	kitIds:4
	kitIds:5
	kitIds:6
	kitIds:7
	kitIds:8
	kitIds:9
	kitIds:11
	kitIds:12
	kitIds:13
	kitIds:14
