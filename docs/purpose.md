# Záměr softwarového projektu

 

**Studijní program**: Softwarové a datové inženýrství

**Typ projektu**: Softwarový projekt (NPRG069)

**Studenti**: Kryštof Hrubý, Matyáš Brabec, Václav Luňák, Cyril Chudáček

**Vedoucí**: doc. RNDr. David Hoksza, Ph.D.

**Konzultanti**:

**Jméno a téma projektu**: Genetika a příjmení (GAP), Genetika

## Overview
Zásadní motivací projektu je modernizace současné webové aplikace genebáze.cz, která je postavena na zastaralých technologiích. Cílem projektu je reimplementovat a zároveň rozšířit některé části pro správu, analýzu a vizualizaci genetických profilů, zejména pro účely genealogických výzkumů. Sytém bude sloužit jak běžným občanům ČR, kteří budou moci nahrávat a spravovat svá genetická data, tak kriminalistickému ústavu ČR (dále jen KÚ), který bude moci nad těmito daty vykonávat analýzy. 

 Dalším důvodem je nedostatečné zabezpečení vysoce citlivých genetických dat v současném systému. Aktuálně je projekt nasazen u soukromé osoby na „osobním“ počítači, bez přímé kontroly vlastníka genetických dat – KÚ. Naším cílem je tento nedostatek napravit a zároveň zajistit, že jsou dodržena všechna pravidla pro ukládání a práci s osobními daty v souladu s platnými zákony o ochraně osobních údajů (např. GDPR).

Projekt se dělí na několik hlavních částí – webové rozhraní, backend a subsystém na správu distribuce genetických testů.

## Webové rozhraní

Webové rozhraní bude sloužit zejména uživatelům aplikace (tj. občanům ČR) k nahrávání, správě a prohlížení vlastních genetických dat. Pro tyto uživatele také bude dostupná mapa distribuce haploskupin, fylogenetický strom a rodokmen. Díky těmto nástrojů bude možnost vyhledat potenciální příbuzné (právě na základě poskytnutých genetických dat) a případně je kontaktovat.

Pro administrátory bude navíc k dispozici rozhraní pro správu systému. Toto rozhraní umožní vytvářet uživatelské účty, prohlížet a editovat data ostatních uživatelů a vytvářet exporty dat pro spolupracující třetí strany.

## Backend 

Hlavní zodpovědností backendu bude nahrávání, ukládání a správa uživatelských dat -- osobních, genetických a genealogických. Zároveň bude poskytovat funkcionalitu vyžadovanou webovým rozhraním, např. predikce haploskupin, hledání příbuzností, generování geografických agregací aj.

K dispozici bude taktéž veřejné API, které bude umožňovat externím spolupracovníkům KÚ přístup ke genetickým datům. Součástí API bude autentikační a autorizační systém určující k jakým datům má daný konzument přístup.

Zvláštní důraz bude kladen na bezpečné uložení a práci s daty. Za tímto účelem bude využito zabezpečené úložiště poskytunté organizací CESNET. Systém též poskytne podporu pro pseudoanonymizaci exportovaných dat.

## Distribuce genetických testů

Jako jeden z hlavních datových zdrojů pro vyvíjený systém by měly být vlastní genetické testu KÚ. KÚ po žádosti uživatele odešle testovací set poštou, načež si uživatel sám odebere vzorky a odešle je zpět na KÚ. Následuje série chemických analýz, na jejímž konci je soubor s výsledky testu a unikátním ID. Cílem je co nejvíce automatizovat celý proces a tak eliminovat možná místa pro lidskou chybu, např. zprostředkováním automatického tisku adres na obálky testovacích sad, generovaním unikátních identifikátorů testů a strojově čitelných čárových kódů apod.

## Přibližný průběh
Odhadovaná délka projektu je standardních 9 měsíců. Projekt bude rozdělen do následujících částí:

1. Komunikace s KÚ, definice funkčních požadavků systému.
2. Tvorba detailní specifikace.
3. Vytvoření základní kostry aplikace.
4. Vývoj hlavních funkcí.
5. Návrh a vývoj případných dodatečných funkcí.
6. Testování a oprava chyb.
7. Finalizace, konečná dokumentace.
