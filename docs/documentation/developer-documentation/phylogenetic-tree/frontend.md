# Frontend

## Integration of D3.js

D3.js (Data-Driven Documents) is a powerful JavaScript library for creating dynamic and interactive data visualizations. There are several reasons why using D3.js is beneficial for phylogenetic tree visualization:

1. **Rich set of data visualization capabilities:** D3.js provides a wide range of tools and functions for handling data and creating visual representations. It offers various chart types, scales, and layouts, making it well-suited for complex tree structures like phylogenetic trees.

2. **Flexibility and customization:** D3.js gives you fine-grained control over the visual elements of your tree. You can easily customize the appearance, behavior, and interactions based on your specific requirements. This flexibility allows you to create a unique and tailored visualization.

3. **Data-driven approach:** D3.js emphasizes the connection between data and visual representation. It enables you to bind your data directly to the visual elements, making it easier to update and manipulate the visualization in response to changes in the underlying data.

However, there are a few considerations to keep in mind:

1. **Learning curve:** D3.js has a steep learning curve, especially for those new to JavaScript or data visualization concepts. It requires a good understanding of JavaScript, SVG (Scalable Vector Graphics), and DOM (Document Object Model) manipulation.

2. **Complexity:** While D3.js offers powerful capabilities, it also comes with a higher level of complexity compared to simpler charting libraries. This complexity can sometimes make it challenging to grasp and implement certain features, especially for less experienced developers.

## Layout of the Tree

In designing the layout for our phylogenetic tree visualization, we initially explored the idea of a dynamic layout using a force-directed graph layout. However, after careful consideration, we decided to use a deterministic hierarchical tree layout for the following reasons:

1. **Improved readability:** The deterministic hierarchical layout provides a clear visual representation of the tree structure. It allows users to easily identify parent-child relationships and understand the overall hierarchy. This is particularly important when dealing with large phylogenetic trees, where a dynamic layout might introduce confusion due to changing node positions.

2. **Consistency:** The deterministic layout ensures that the tree's shape remains consistent across multiple interactions or reloads. This consistency enhances the user experience by providing a stable and predictable representation of the tree. It avoids potential confusion caused by a dynamic layout that reshuffles the nodes on each interaction.

To further enhance the user experience, we decided to implement an interaction mechanism where users can display or hide specific parts of the tree by clicking on nodes. Additionally, right-clicking on a node triggers a context menu with additional actions.

## Deviations from Specification

While implementing the phylogenetic tree visualization, we made a slight deviation from the initial specification regarding the user search functionality. The decision to modify the search functionality was driven by privacy concerns and the need to restrict access to potentially sensitive information.

Originally, the specification included a search feature that allowed users to search for any user in the application. However, after careful revision and discussions with the contractor, we decided that exposing potentially sensitive information about all users to any user could pose privacy risks.

To address this concern, we decided to limit the search functionality to display only users within the same subtree as the currently selected node. This ensures that users can only access information about individuals within their immediate genetic lineage. For more detailed information on the search functionality, please refer to the section on "Closest Relative Search."

## Feature Structure

The key implementation components of our phylogenetic tree visualization are:

1. **PhylogeneticTree.tsx:** This central component orchestrates the various aspects of the visualization. It handles the display of user haplogroup information, haplogroup legend, and initializes the tree visualization using D3.js. It defines actions to be taken based on user input, such as clicking, dragging, and zooming. Additionally, it implements context menu actions.

2. **graph.ts:** This file defines our own manipulations with the tree structure. It includes functions for finding a specific node, determining the root of the tree containing a given node, and other tree-related operations. The main interface defined in this file is the `Node` interface, representing a tree node and the data it holds. Unlike the flat representation used in the family tree feature, the phylogenetic tree utilizes node pointers, making tree manipulation more straightforward.

Regarding the styling of the tree, you might find it surprising that all the tree settings are contained in a single typescript file. The reason behind this choice is related to maintainability and the limitations of CSS styling for SVG elements. Since not all SVG properties can be styled using CSS, consolidating the tree's styling settings in one file makes it easier to manage and maintain the visual aspects of the tree visualization. This approach ensures that all the relevant styling information is centralized and can be modified more efficiently.
