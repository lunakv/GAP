# Overview

This part of the application focuses on displaying and managing phylogenetic tree that represent evolutionary relationships based on mutations in the human Y chromosome (Y-DNA haplotree) and mitochondrial DNA (mtDNA haplotree). These trees consist of nodes corresponding to different haplogroups, with new branches created as new mutations occur over generations.

To generate the trees, genetic and genealogical data are required. However, instead of creating our own tree, we utilize the one provided by FTDNA, which claims to be the largest in the world. We adopt FTDNA's naming conventions for the haplogroups to ensure consistency. As new data points are added to the system, the tree needs to be periodically updated. Additionally, an administrator can manually request updates to the tree.

Assigning nodes to users is a crucial aspect of the application. Each user is assigned a relevant node in the tree based on their haplogroup. To determine the user's haplogroup, the application employs the NevGen predictor. The NevGen model is continuously improved with new data, and therefore, it also requires periodic updates.

Overall, the React frontend application provides an interface to visualize and manage phylogenetic trees representing Y-DNA and mtDNA haplotrees. It leverages the extensive FTDNA tree, periodically updates the tree with new data, and assigns nodes to users using the NevGen predictor, which is also updated regularly.
