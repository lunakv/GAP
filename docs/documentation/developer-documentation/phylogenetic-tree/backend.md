# Backend

The phylogenetic tree on the backend side of the application consists of three main stages, each with different data structures and supported actions.

The data flow and workflow of the phylogenetic tree can be described well with the following diagram:
![Phylogenetic Tree Workflow Diagram](../images/PhyloTreeWorkFlow.drawio.png)

1. **First stage:** This stage occurs when the tree is downloaded. At this point, each tree node has no fields connected to users. It only contains fields that form the tree structure (Children, ParentId), fields that define the node (Name, Id), and a field to map users to this node - variants. These nodes are referred to as [PhyloVariantedNode](../../api/PhyloTree.DataClasses.PhyloVariantedNode.html). Since the node represents a haplogroup, every user with a haplogroup (e.g., [CompleteUser](../../api/Users.UserAccess.Entities.CompleteUser.html)) can be mapped to it. However, nodes can be represented by multiple different haplogroup names, hence the variants (possible names) are necessary for user mapping.

2. **Second stage:** In this stage, the tree is pruned. The downloaded tree includes approximately 60,000 nodes, and displaying it as a whole on the frontend would be confusing, especially considering that most of the tree nodes would be empty. Pruning is implemented to keep only the relevant haplogroups in the tree. During pruning, all users are mapped to a node (if possible), and then nodes with empty subtrees are discarded. The final nodes, called [PhyloQuantifiedNode](../../api/PhyloTree.DataClasses.PhyloQuantifiedNode.html), no longer include variants since no further user mapping is necessary. However, they contain the IDs of users corresponding to the given nodes, as well as the internal ID of the represented haplogroup. The top of the tree consists of so-called *dead* haplogroups, which are haplogroups that no longer exist in current human beings. To determine if given nodes correspond to the same subtree below these *dead* haplogroups, a boolean is stored, which is only true at the top of those subtrees. The pruned tree is then stored in a singleton instance of the [PhyloTreeController](../../api/PhyloTree.PhyloTreeController.html) class in memory for quick access. To ensure availability of such queries at all times, the initial pruning occurs immediately after the API application starts and is initiated within the `ConfigureTree` method in the `Api.ServiceConfig` file.

3. **Third stage:** When a query for any tree component arises, the third stage of tree representation takes place. This stage is brief and not shown in the picture. Users are inserted into the pruned tree from memory, replacing their IDs with the corresponding nodes. The resulting tree is then returned as the query result in the form of a list of nodes.

The internal [PhylogeneticTree](../../api/PhyloTree.DataClasses.PhylogeneticTree.html) class stores the nodes as a dictionary, enabling faster pruning process with constant time for node access. It also stores the list of roots (since the downloaded tree is actually a forest) and additional information that defines the tree (ID, creation time).

To support extensibility, the individual node classes are not hardcoded into the algorithms. Instead, they derive from three different interfaces required in the respective algorithms. The corresponding inheritances are depicted in the following diagram:
![Phylogenetic Tree Classes Diagram](../images/PhyloTreeClasses.drawio.png)

Another feature is the `TreeBuilder`, which is a nested class within [PhylogeneticTree](../../api/PhyloTree.DataClasses.PhylogeneticTree.html). It facilitates the easier creation of trees and is also visible in the diagram.

The project also includes extension methods for [PhylogeneticTree](../../api/PhyloTree.DataClasses.PhylogeneticTree.html). Although not necessary since the methods could be directly in the class itself, the tree is a data class. Algorithms should be performed on it, but the class itself should not be cluttered with algorithms. Therefore, an extension method is implemented to go through the tree and determine if two nodes are in the same sub-branch.
