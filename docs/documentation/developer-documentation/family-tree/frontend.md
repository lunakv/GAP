# Frontend

## Introduction

As mentioned in overview the Family Tree feature allows users to visualize and navigate family relationships using an interactive chart. This documentation provides an overview of the frontend implementation of the feature, focusing on the integration of the [family-chart](https://github.com/donatso/family-chart) library and the file structure of the feature.

## Integration of family-chart Library

The `family-chart` library was chosen as the visualization component for the Family Tree feature due to its availability as a free option. While it has some limitations, such as lacking TypeScript support and minimal documentation, it was the best available choice considering our project's requirements.

### Why family-chart?

1. **Free and Available**: The family-chart library is the only free library that met our needs for visualizing family trees (as far as we know). It provided a starting point for our implementation without incurring any additional costs as it is build on top of D3.js.

2. **Visualization Capabilities**: Despite its limitations, the family-chart library excels in visualizing family trees. It offers a visually appealing and interactive chart representation, enabling users to explore and understand complex family relationships more easily.

### Limitations of family-chart

1. **Limited Functionality**: The family-chart library focuses solely on visualization, meaning it lacks built-in support for manipulating family tree data. We had to implement our own functionality for adding, removing, and modifying individuals within the family tree.

2. **No TypeScript Support**: Unfortunately, the library does not provide TypeScript definitions, which may introduce some challenges when integrating it into a TypeScript-based project. We recommend exercising caution and thorough testing when working with the library in a TypeScript environment.

3. **Minimal Documentation**: The family-chart library lacks comprehensive documentation, which has led to some guesswork when exploring its functionality and capabilities. We encourage developers to thoroughly test and experiment with the library to fully understand its behavior.

4. **Limited Customization**: The family-chart library, despite being the only free option available for visualizing family trees, presents limitations in terms of customization and extensibility. Due to the lack of comprehensive documentation and limited functionality, modifying the behavior and appearance of the library can be challenging. For instance, it may not be possible to customize the interactivity of the chart or apply custom styles specifically for unknown people in the family tree. While it is theoretically feasible to make adjustments, the lack of clear instructions or guidance in the documentation makes it difficult to implement such changes effectively. As a result, our ability to fully customize and tailor the family chart to our specific requirements is significantly hindered.

## Feature Structure

Let's start with the core logic of the family tree feature. As mentioned before, the `family-chart` library does not have built-in support for tree manipulations. Therefore, we had to implement all the necessary manipulations ourselves, such as removing a node from the tree or editing the relations of an existing node. You can find all of these manipulation functions in the `tree-manipulations.ts` file. Additionally, it's important to note that the library operates using a flat representation of the tree. This means that the references between nodes are represented by IDs instead of direct pointers to the actual objects.

Now let's briefly explore the models used throughout the feature and the relevant components that utilize them. The primary object in the family tree feature is the `FamilyTreeNode`, which adheres to the requirements of the `family-chart` library. It is recommended not to modify this interface to ensure compatibility and functionality with the library. Another important interface is `FamilyNodeEditLogic`, which is used by the `EditFamilyNodeModal.tsx` component. This component implements the edit family node modal and provides a user-friendly interface for modifying specific details of individual family nodes. It not only allows editing existing family tree nodes but also facilitates the addition of new nodes. This component receives an instance of the `FamilyNodeEditLogic` interface, which determines various aspects such as disabled fields and their respective contents.

The central component is `FamilyTree.tsx`. It serves as the primary implementation of the Family Tree feature, handling the rendering logic and interactions required for visualizing the family tree chart. It also utilizes supporting components like the `ContextMenu` component and orchestrates interactions between various components. For example, it provides the appropriate instance of `FamilyNodeEditLogic` to the `EditFamilyNodeModal.tsx` component, ensuring smooth functionality and data flow between the components.

Lastly, it's worth mentioning that there is a layer of abstraction built on top of data fetching from the server (implemented in `family-tree-provider.ts`). This layer effectively hides the technical intricacies of data fetching and acts as a bridge, ensuring alignment between the entities used in the frontend and those on the server. It shields the frontend application from low-level implementation details.
