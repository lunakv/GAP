# Overview

The logging feature in the React frontend application serves as a means to track and monitor important actions and events within the system. This overview provides an understanding of how logging is implemented, including the access control mechanism for different user roles. The focus is on logging relevant mutations while ensuring data privacy and maintaining access restrictions based on user roles.

## Introduction

Logging plays a crucial role in capturing and documenting important actions and events within the application. It enables administrators and root users to monitor system activities, track changes, and identify potential issues. The logging feature is designed to provide transparency and accountability while maintaining data privacy and access control.

## Access Control for Logs

- **User**: No access

- **Lab tech**: No access
  
- **Admin**: Admins have limited access to logs and can only view logs related to user-related actions. This includes logs pertaining to user management operations, such as granting or revoking permissions for other users.
  
- **Root**: Root users have the highest level of access and can view all logs within the application, including those related to admins. This allows for comprehensive monitoring and analysis of all system activities.

## Logging Relevant Mutations

To maintain clarity and relevancy, the logging feature focuses on capturing relevant mutations within the application. Mutations refer to significant changes or actions that impact the system's data or functionality e.g. user adds user manager on *Manage Permission* page.

## Data in Logs Table

In the logs table, each log entry provides valuable information about a specific system action or event. The table includes the following columns:

1. **Request ID**: A unique identifier associated with each logged action, facilitating easy reference and tracking.

2. **Request Performer**: Indicates the user or role responsible for initiating the action, providing visibility into who performed the logged operation.

3. **Affected User**: Specifies the user who is directly impacted by the logged action, providing insight into the individuals involved.

4. **Endpoint Name**: Describes the name or identifier of the endpoint related to the logged action, allowing for easy identification of the specific functionality or feature involved.

5. **Timestamp**: Displays the date and time at which the action was logged, providing a chronological order of events and facilitating monitoring and analysis.

6. **Status**: Indicates the status or outcome of the logged action, such as success or failure, aiding in identifying any potential issues or anomalies.

7. **Detail**: Offers additional details or contextual information about the logged action, providing a deeper understanding of the event and aiding in troubleshooting or investigation if necessary.


Additionally, the logs table offers the capability to filter entries based on each column. This filtering functionality allows users to narrow down the log entries based on specific criteria. For example, users can filter by Request Performer to view logs performed by a particular user or filter by Timestamp to focus on logs within a specific time range. This filtering capability enhances the usability and efficiency of the logs table, enabling users to quickly locate and analyze relevant log entries based on their specific requirements.

## Data Privacy and Security

The logging feature takes data privacy and security into consideration. The access control mechanism ensures that only authorized users can view specific log entries based on their roles. Additionally, the focus on logging relevant mutations helps maintain data privacy by excluding logs that contain sensitive or unnecessary information, such as user A viewing user B's resource.

## Conclusion

The logging feature in the React frontend application provides transparency, accountability, and monitoring capabilities within the system. By implementing access control based on user roles, the application ensures that logs are accessible to the appropriate users. Logging relevant mutations enhances the usefulness of the logs while maintaining data privacy. This feature empowers administrators and root users to track important actions and events, enabling efficient system management and analysis.