# Backend

Access to logs is implemented in `Api/Logging`, `Logging` and `PostgreSqlStorage/LoggingGateway`.

There are three GraphQL queries for accessing mutation logs: `userLogs`, `anyUserLogs` and `loggedEndpointNames` in [LoggingQueries](../../api/Api.Logging.LoggingQueries.html).

Query `loggedEndpointNames` returns a list of endpoint names which the logs are available for. Queries `userLogs` and `anyUserLogs` return a list of logs where each log contains loggin information about one client request. The difference is that `anyUserLogs` which are accessible only by root contain request logs of both GAP staffers and normal users and `userLogs` which are accessible also by admins contain only logs of standard users.

The main bulk of logic is implemented in application controller [LoggingController](../../api/Logging.LoggingController.html). It uses [ILogFetcher](../../api/Logging.Storage.ILogFetcher.html) interface for getting logs from storage. The logs include information about who performed the request and sometimes who it was performed about (e.g. user changes his own data.) Then it checks if the logs have correctly filled fields and logs error if not. [IIdentityManager](../../api/Auth.Identity.Storage.IIdentityManager.html) from `Identity` component to get the user identities which provide more information than just id. If also checks that all users mentioned in logs exist.

The database layout is generated from `appsettings.json` serilog configuration. The configuration that Serilog should log to Console and two PostgreSQL tables - `DebugLogs` and `MutationLogs`. Console and `DebugLogs` log outputs contain all log collected during execution. `MutationLogs` table contains only logs of mutation endpoints. Logs from this table can be fetched using [JsonLogFetcher](../../api/PostgreSqlStorage.LoggingGateway.JsonLogFetcher.html) which implements `ILogFetcher` interface. 

One request log fetcher using `ILogFetcher` does not correspond to one row in the table but rather to multiple rows which log different things. `JsonLogFetcher` needs to correctly group and merge all logs by their request ids.