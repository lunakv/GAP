# Backend

Closest relatives search on backend are only queries, since no user can change any data in this section, only view them. The queries are provided by [ClosestRelativesQueries](../../api/Api.ClosestRelativeSearch.ClosestRelativesQueries.html). There are two types of them. One are on marker sets - the frontend may ask for available marker sets and also for a given marker set by name. The other and much more complex query is on searching the the users and finding the closest ones. Both of these queries are resolved by the [ClosestRelativeSearchController](../../api/Users.ClosestRelativeSearch.ClosestRelativeSearchController.html) from the `Users` project. 

The easier MarkerSet quries are done via the [MarkerSetFetcher](../../api/Users.ClosestRelativeSearch.MarkerSets.MarkerSetFetcher.html) class, which fetches them from local files from a directory.

Lets uncover the slightly more complicated query type. The controller fetches all users from the database (see [users](../user-sharing/backend.md)) and using the [ClosestRelativeFinder](../../api/Users.ClosestRelativeSearch.ClosestRelativeFinder.html) with selected marker set. What has not been described yet is, that the user can specify a filter to search the users through . It may be a name, a haplogroup or a subtree of a given node in the [phylogenetic tree](../phylogenetic-tree/overview.md) (which corresponds to related haplogroups). 

So the [ClosestRelativeFinder](../../api/Users.ClosestRelativeSearch.ClosestRelativeFinder.html) can also uses be provided with a filter. If no filter is specified, some default genetic distance filter is applied. It computes which users satisfy the filter and orders them based on the genetic distance. This is then returned.


This diagram resembles the data flow within a user searching query:
![Diagram](../images/ClosestRelativesWorkflow.drawio.png)