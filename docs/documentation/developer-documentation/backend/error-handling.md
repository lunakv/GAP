# Error Handling

This chapter describes error api and how errors are handled on backend.

The [GraphQL specification](https://graphql.org/learn/validation/) specifies that if a mutation or query validation fails, the result contains a `errors` field which contains a list of detected errors. We use this concept to report custom errors that were cause by user (or they are caused frontend bugs). Backend automatically fills optional `code` field with different value for each use case on an error object (in `errors`) it creates and sends to frontend.

Therefore, errors are not part of schemas and instead reuse this GraphQL error mechanism from GraphQL specification.

Support on backend is implemented by [LogErrorFilter](../../api/Api.ErrorHandling.LogErrorFilter.html) which is registered to GraphQL middleware. I has method `OnError(IError error)` which is called when GraphQL middleware detects that an error has happened. When an excetion is thrown and is not caught in endpoint resolver or any class it uses, it escapes to the middleware which converts it to an error preserving the stack trace and message of the thrown exception.

`LogErrorFilter` needs to detect whether the error it gets as parameter is an exception that was caused by client. This kind of exception is represented by [UserException](../../api/Users.Exceptions.UserException.html). Any exception that inherits from this exception is expected to represent a user error and is caught by `LogErrorFilter` and propageted to frontend. The easiest solution for code generation would be to set the code as exception names. However, that makes frontend error handling dependent on the names of the exception in the whole backend.

Instead there is a mapping file which maps exception to codes that are used on frontend. The path to this file is specified in `appsettings.json` in `ErrorConfig__ExceptionToCodeMappingFile` field. The default file can be found in `../Configuration/Errors/ExceptionToCodeMapping.json` relative from the appsetting file. It is just one object whose field represent the mapping (key=exception, value=code). 

Also, `UserException` has abstract method `GetRepresentation` which returns its string representation (usually the exception name) which is mapped to the code. This is useful for cases when creating a custom exception for each usecase is exhaustive (e.g. business validation of all user input data).


`LogErrorFilter` also logs the errors representing any thrown exception from user code.