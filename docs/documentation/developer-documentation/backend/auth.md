# Authentication & Authorization


## Authentication

Authentication is done using [JWT](https://jwt.io/introduction) tokens using [Identity](https://learn.microsoft.com/en-us/aspnet/core/security/authentication/identity?view=aspnetcore-6.0&tabs=visual-studio) library. There is a [ITokenCreator](../../api/Auth.Token.ITokenCreator.html) interface which specifies api for creating security tokens. [JwtTokenCreator](../../api/Auth.Token.JwtTokenCreator.html) is the used implementation which uses `Identity` library to generate JWT tokens.

Two important configuration parameters are `AuthConfig__TokenExpiration` and `AuthConfig__SymmetricKey` in `appsettings.json`. Token expiration sets the default login token expiration time span. Symmetric key is the key used for signing and validating JWT tokens.

## Authorization

Authorization is done using [ASP NET Core Authorization](https://learn.microsoft.com/en-us/aspnet/core/security/authorization/introduction?view=aspnetcore-6.0) support and [Hot Chocolate Authorization](https://chillicream.com/docs/hotchocolate/v12/security/authorization) integration to ASP NET Core. 

Authorization middleware in ASP NET Core provides role and policy based authorization. Roles are created for every type of user in our application and saved in JWT login tokens. Policies are used to secure endpoints. Note that some policies (e.g. `IsAdminPolicy`) represent only a role requirement.

Custom requirements are added to policies which must be satisfied for a policy to be satisfied.

There are a lot of requirements and policies and their are documented in reference api documention; therefore, we only include links to them in api documentation.

Roles are `User`, `Admin`, `Root`, `LabTech`, `PublicApiConsumer`. They map to the types of users working with our application presented in [detail](../overview.md).

Requirements are [HasSignedAgreementRequirement](../../api/Auth.Policies.AuthorizationRequirements.HasSignedAgreementRequirement.html), [IsAuthUserThemselfRequirement](../../api/Auth.Policies.AuthorizationRequirements.IsAuthUserThemselfRequirement.html), [IsUserThemselfOrAdminRequirement](../../api/Auth.Policies.AuthorizationRequirements.IsUserThemselfOrAdminRequirement.html), [UserManagerRequirement](../../api/Auth.Policies.AuthorizationRequirements.UserManagerRequirement.html), [UserViewerRequirement](../../api/Auth.Policies.AuthorizationRequirements.UserViewerRequirement.html).

Requirements are added to policies in [PolicyRegistrar](../../api/Auth.Policies.PolicyRegistrar.html) which is called from `ServiceConfig` class which adds them to the authorization middleware.

There are following policies: [HasUserManageRightPolicy](../../api/Auth.Policies.HasUserManageRightPolicy.html), [HasUserViewRightPolicy](../../api/Auth.Policies.HasUserViewRightPolicy.html), [IsRootPolicy](../../api/Auth.Policies.IsRootPolicy.html), [IsAdminPolicy](../../api/Auth.Policies.IsAdminPolicy.html), [IsUiUserPolicy](../../api/Auth.Policies.IsUiUserPolicy.html), [HasLabAccessPolicy](../../api/Auth.Policies.HasLabAccessPolicy.html), [HasPublicApiAccessPolicy](../../api/Auth.Policies.HasPublicApiAccessPolicy.html), [IsUserThemselfOrAdminPolicy](../../api/Auth.Policies.IsUserThemselfOrAdminPolicy.html), [IsUserThemselfWithoutSignedAdministrationAgreementPolicy](../../api/Auth.Policies.IsUserThemselfWithoutSignedAdministrationAgreementPolicy.html), [IsAuthUserThemselfPolicy](../../api/Auth.Policies.IsAuthUserThemselfPolicy.html), [IsUserThemselfPolicy](../../api/Auth.Policies.IsUserThemselfPolicy.html).

Requirements are satisfied by [authorization service and authorization handlers](https://learn.microsoft.com/en-us/aspnet/core/security/authorization/policies?view=aspnetcore-6.0). We implement our own custom authorization handler which implement [IAuthorizationHandler](https://learn.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.authorization.iauthorizationhandler?view=aspnetcore-6.0) interfaces. Our authorization handlers are called by the authorization middleware. The only thing that we need to do is to register them to the server DI as services implmenting `IAuthorizationHandler`.

The handlers typically use claims stored in login JWT token to get the identity of the user accessing the server. Since the handlers are in DI, they can also use any service in DI. For more information, look at the handlers in code or api documentation. For convenience, we provide link to the api documentation: [AdminHandler](../../api/Auth.Policies.AuthorizationHandlers.AdminHandler.html), [LabTechHandler](../../api/Auth.Policies.AuthorizationHandlers.LabTechHandler.html), [RootHandler](../../api/Auth.Policies.AuthorizationHandlers.RootHandler.html), [UserHasSignedAdministrationAgreementHandler](../../api/Auth.Policies.AuthorizationHandlers.UserHasSignedAdministrationAgreementHandler.html), [UserManagerHandler](../../api/Auth.Policies.AuthorizationHandlers.UserManagerHandler.html), [UserThemselfHandler](../../api/Auth.Policies.AuthorizationHandlers.UserThemselfHandler.html), [UserViewerHandler](../../api/Auth.Policies.AuthorizationHandlers.UserViewerHandler.html).