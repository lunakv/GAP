# Server Configuration

Server configuration is done in class `Service Config` and `appsettings.json` in the root folder of project `Api`.

## Service Config

`Service Config` is responsible for building web application. It registers services to dependency injection service with their lifetime. That includes adding database contexts for connecting to databases, logging framework, identity storage, our own interfaces with implementations, authentication and authorization.  

It also makes configuration in `appsettings.json` accessible in the rest of the framework.


## App Settings

App settings contain connection strings, authorization settings and more. The file has comments for each option and we do not want to copy it here since then it may diverge from the one in use down the way.