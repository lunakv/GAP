# Legacy data
As the project is improving an already existing system of data collection and storing, there are already existent data that need to be parsed and included in the created database. Unfortunately, the previous data are not unified and there are various sources with various data structures.
All of those data contain previous records regarding personal (name) and contact (email, telephone, address) information as well as information about the user's oldest known ancestor (name, year, place).
Those information are parsed from 5 different files. This number is not final since not all of the data sources have been provided from the client yet, but the ones that were are used and the users they store are accessible in the database.
This whole parsing process is done within the `OldKuUserDataManager` project.

The address format within the source files is uncertain, but one of the goals of the application is to provide map overview of users within their respective regions. So the data parsing project also downloads information about every town and about all municipality regions in Czechia from CUZK (Český úřad zeměměřičský a katastrální). Then all users have their region predicted given the name of the town they live in and if those are ambiguous, even street names are taken into account. The region prediction is done within [RegionSetter](../../api/OldKuUserDataManager.RegionSetting.RegionSetter.html) and the data preparation is ruled by [RegionParsingController](../../api/OldKuUserDataManager.RegionSetting.RegionParsingController.html).

Most of the previous data contain information about the users' haplogroups, but it was settled with the contractor that those should be predicted within the project to ensure their validity. Haplogroup prediction is used for this purpose (explained [here](./haplogroup-prediction.md)).

The haplogroup prediction of the input users is a process lasting about 3 hours and so the data is stored with both region and haplogroup prediction in it as a local file. This file can then be processed to create data suppliable to `DataLoader` by using the `--save-app-users y` arguments within the configuration file.

If there are any changes that need to be done within the input files, that prediction would be lost when parsing the data again. For this purpose the there is `UsersMatcher` class that matches users and selects the better data out of both instances. This way, predictions remain even when input files or parsing algorithms change.

## Running the software

The OldKuUserDataManager.Program is the entry point of the parsing software. It accepts one parameter in form of `-c path/to/configuration/file`. Such configuration file then may include many possible arguments. Example of such a file is `/docs/inputDataExamples/oldDataConfig.cfg`. The most important of the available commands are:
- file arguments - those are arguments pointing to the files with given parts of user data
- `--proceed y` - when used, no parsing is done, only predictions
- `--save-app-users y` - saves the current list of users as a file suppliable for DataLoader
- `--predict-count-haplogroups` (and `--predict-count-regions`) - how many haplogroups (regions) to predict
- `--print-predict-count y` - prints a statistic about how many users have haplogroup and regions predicted

## Data Flow

Within this module, there is a diverse range of data that needs to be transferred, parsed, and modified. The following diagram attempts to illustrate the overall process:
![Data Flow Diagram](../images/LegacyDataFlow.drawio.png)

It is challenging to depict all possible scenarios as the data flow can vary significantly depending on the arguments provided, and there are numerous possibilities. However, the diagram above represents the complete process, with arguments mostly removing certain parts of it. There is one important exception worth mentioning, though. The diagram does not depict the flow of data when saving it through the `DataLoader`. To provide a clearer understanding of the data flow during that process, the following diagram specifically illustrates that part:
![To DataLoader Diagram](../images/LegacyDataToDataLoader.drawio.png)

However there is not much logic during this process. Most of it happens within the [WrongUserRemover](../../api/OldKuUserDataManager.WrongUserRemover.html) class. Every user is tested for various properties there and only if he meets all of the criteria, he will be pased to the [FinalDataDistributor](../../api/OldKuUserDataManager.FinalDataDistributor.html) class to get saved.
