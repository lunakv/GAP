# Design Concepts
In this chapter, we discuss the base design concepts which different features follow. This chapter is dependent on the Overview which presents the components of our application. The first topic is Feature Implementation which describes activity diagram os serving a standard request. The rest to the discussed topics then explain design choices presented in the diagram. 

## Feature Implementation

In the Overview, we discussed the component view of the server. There was `API` component which was responsible for publishing server endpoints, `Feature/Business Logic` components responsible for feature bussines logic implementation and `Storage Gateway` components which connected to `Storage`. Now we take a look at the proccess of serving requests more closely. It is illustrated on the activity diagram below:

![ Feature Implementation ](../images/feature-aktivity.drawio.png)

When request comes to the server, it goes through ASP NET Core authentication and authorization middleware where we implemnted our own authorization logic. If authentication and authorized, the request gets to the endpoint. This endpoint is either a graphql query or mutation resolver or HTTP controller from ASP NET Core MVC.

The request parameters are parsed to the endpoint data transfer inputs. This part implicitly validates correct types of the input argument or fails. Server DI provides services for serving the request. That is [EndpointLogger](../../api/Api.Logging.EndpointLogger-1.html) class which provides functionality for logging endpoint request data. It also logs who sent the request and if the request is about another user, it logs the user it is about. These logs can be then retrieved by admin users.

The other service is an application controller (e.g. `KitManagementController` class for `Kit Management` in `Laboratory`). There is a one application controller for each small set of logically connected endpoints and each endpoint has a method to call which does the work the endpoint represents.

The controller is a business logic class and receives business classes as arguments. Before calling the endpoint corresponding method on the controller, the input classes must be trasformed to business classes (entities).

The controller calls any business logic classes necessary to implement the feature. This includes fetching or storing data using `Storage Gateway` components. Result is returned to endpoint resolver. If anything fails, exception is thrown and handled in [LogErrorFilter](../../api/Api.ErrorHandling.LogErrorFilter.html) from `API` component. 

Endpoint returns the received data from from controller. Again, we do not leak public entities to server api. Raw HTTP endpoints only return files so the following applies to GraphQL queries and mutations.

One way to not leak public entities is to create a new class (or more interconnected classed) for each entity and manually create them from entities and return them. We call these objects data transfer objects in `API` and they have suffix `Dto`. The somewhat obvious advantage of this approach is simplicity.  Moreover, `Hot Chocolate` GraphQL library can provide client with options for filtering and sorting the results based on any field in the schema. This works only on classes and simple objects. The disadvantage is that it requires a lot of boilerplate code for each entity to return in the server api. Additionaly, there is no way to provide lazy inner resolvers (e.g. object stores user Id and we could use inner resolver to lazily get the whole user if it is requested). All data must be loaded no matter which fields are requested by client.

The other way is to define GraphQL types ourselves using [object type builders](https://chillicream.com/docs/hotchocolate/v12/defining-a-schema/object-types)(object types - code first variant). Object types let us specify what properties should be public, if there are new properties which the entity does not have, how to resolve properties, what is the type of properties, field resolver authorization and more. In this case, the query or mutation resolver simply returns the entity object. `Hot Chocolate` then uses defined object types for returned entities to create GraphQL types based on out specification in the object type. These types are usually in `Types` directories which are in the same directories and mutations and queries. 

We use both approaches.

This applies to the vast majority of the features.

## Endpoint Logging

We mention that the endpoint logging is done manually in each endpoint resolver. That raises the question of why not do it automatically in [ASP NET Core middleware](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/middleware/?view=aspnetcore-6.0) or using `Hot Chocolate` middleware for all the endpoint at one place once.

One reason is that not every endpoint's input data should be logged. For example endpoints where passwords are sent as arguments. The information about which user called the endpoint needs to be retrieved from authentication token which only exists on endpoints for authenticated users. Moreover, if a endpoint operation is about a user (retrieves user data of user 1 for example), retrieving information which user it is requires delving into GraphQL or HTTP request parameters. 

## Business Entities and API Input Objects

Business entity classes are not used as endpoint inputs for two reasons. First is close coupling between api and business logic. Changing public class properties would change api. While there are attributes that set certain properties to be ignored when constructing api types provided to the outside world, the entity would be dependent on the web api library which the attribute comes from. If it was necessary to change the api, the corresponding business entity would have to be changed as well which is not very good. The other reason is that the web api library which creates the input objects typically needs to somehow create the objects which can require class properties to have for example public set properties.


## Storage Access

Most of the business logic components need to access storage. For example, `Genetic Data` component needs to store STR markers. It also needs to merge these markers from multiple sources and detect merge conflicts. If this component from the business logic can access the database directly and save str data then it gains another responsibility and that is to fetch and save data. It needs to know the implementation details of how to connect to the database, how the database layout is structured and what part of it to update.

Moreover, since the business logic is in the same component, it is kind of dependent on the exact database schema and used database technology and library. Even ORM access to a database still requires ORM classes to be programmed in certain way. Therefore, we decided that database access should be a responsibility of another component which we call `Gateway`. Then our business logic component can get rid itself off of the database access responsibility but it still depends on a gateway component which implements access to one database implementation. 

That is why we decided that business logic component would define an interface of how it wants to fetch and store data. The component is then not dependent on any gateway and if we for example need to switch from relational to graph databases, it is only necessary to implement the corresponding gateway and no business logic code needs to be changed.

In code, it looks the following way:

![ Gateway Concept ](../images/gateway.drawio.png)

There is interface `IFetcher` for fetching data from storage and interface `ISaver` for saving data. A gateway component then has classes which implement these interfaces. While those interfaces could be merged into one, we choose to have them separate to have the option to create proxies. Proxies can be used for improving performance by replicating the data accross multiple databases or caching the data. Or there might be access control proxy or logging proxy. There is no other way to access the stored data other than using these interfaces. 

With this pattern we can test components without the need of a database since it is only necessary to provide an in memory storage gateway or even a fake implementation gateway. 

The disadvantage of this approach is that if we want to filter the data by a parameter efficiently it needs to be part of the interfaces which can lead to methods with a lot of parameters and a lot of code that just forwards these parameters. The other disadvantage is that if there is need to use atomically multiple storages, it cannot be done easily without Two Phase Commit which is then required to be supported by the storages. Therefore, the scope of these interfaces in difficult to set and too small scopes may cause problems down the way.

### Database ORM Models 

Business Logic components such as `Users` define entity objects which they work with. These related objects are part of the fetcher and saver storage interfaces. We mentioned in Technologies that that we use ORM library EF Core to access the database. These entity objects are not used as ORM database models to generate tables since then they would need to follow the rules for ORM objects. Instead there is usually a model mirroring corresponding entity which represents table rows and is used to communicate with database.

## Graph API

We try to have the api be a graph of objects and use it in GraphQL way. For example querying user is defined by [UserType](../../api/Api.Users.Types.UserType.html) which include as a property [`GeneticDataType](../../api/Api.GenData.Types.GeneticDataType.html) which in turn provides properties that the original `GeneticData` class does not have - conflicts and full load of original data sources from the list of their ids in resolver. 

Then is it possible view one request to show user all his genetic data along with any conflicting STR marker values.

Then any endpoint which fetches this exact user type can also access the whole graph of users. Naturally, sometimes we explicitly provide new types for user for security concerns (e.g. when user share access rights). 