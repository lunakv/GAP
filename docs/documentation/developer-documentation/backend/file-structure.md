# File Structure

`Server`, `Data Loader` and `Old KU User Data Manager` containers are implemented in `{repo}/source/GAP.Backend` directory. All are added to one solution `GAP.Backend.sln`. 

`Server` executable is implemented in `Api` project, `Data Loader` in `DataLoader` project and `Old KU User Dat aManager` in `OldKUUserDataManager` project. The rest of the projects implement the business logic features and storage gateway as library projects. Name of the project roughly corresponds to what it implements. Note that `PostgreSqlProject` contains more storage gateways to make database management and maintainment easier.

Projects with suffix `Tests` provide tests. The folder `Configuration` contains default files, predefined files or aforemetioned exception to code mapping file.

