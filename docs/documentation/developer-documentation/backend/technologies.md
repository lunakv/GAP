# Technologies

This chapter discusses the main technologies (libraries, frameworks, databases, ...) we use on backend. Infrastructure technologies are no included and instead discussed in separately in [Infrastructure](../infrastructure/overview.md). 

## ASP.NET Core

The backend is implemented in `C#` ([.NET 6](https://learn.microsoft.com/en-us/dotnet/core/whats-new/dotnet-6)) and uses [ASP.NET Core](https://learn.microsoft.com/en-us/aspnet/core/?view=aspnetcore-6.0) framework to create the backend api server. ASP.NET Core is the standard `C#` framework for building web applications. It is a quite large framework which provides many features any web application typically needs to use. The framework has quite good reference documention and contains a lot of tutorials on using it to build a web app and explanations of the framework's concepts. It also provides a way for libraries to be seamlessly integrated into the framework and a lot of libraries contain extension methods on the framework's service builder to achieve that integration. 

The reasons above led us to use the framework to build our api server.

To support the largeness of the framework, it even supports writing frontend code in `C#` using [Razor](https://learn.microsoft.com/en-us/aspnet/core/razor-pages/?view=aspnetcore-6.0&tabs=visual-studio) or [Blazor](https://learn.microsoft.com/en-us/aspnet/core/blazor/?view=aspnetcore-6.0). It also supports [MVC](https://learn.microsoft.com/en-us/aspnet/core/mvc/overview?view=aspnetcore-6.0) pattern.

We list the main ASP.NET Core features that our server uses. 

The framework supports the dependency injection ([DI](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-6.0)) pattern which is used to fill endpoint controllers with services to server requests. Services can be registered to DI with different lifetimes (transient, scoped, singleton).

The framework takes server [configuration](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-6.0) from `appsettings.json` and provides support for multiple running [environments](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/environments?view=aspnetcore-6.0). Objects in the file can be again registered to DI and retrieved with any service.

The framework supports the dotnet [logging interface](https://learn.microsoft.com/en-us/aspnet/core/fundamentals/logging/?view=aspnetcore-6.0) which again seamlessly works with DI. The interface supports structured logging and many logging libraries do implemented the interface and can be plugged in the system without changing any logging code aside configuration.

The framework provides [authentication](https://learn.microsoft.com/en-us/aspnet/core/security/authentication/?view=aspnetcore-6.0) and [authorization](https://learn.microsoft.com/en-us/aspnet/core/security/authorization/introduction?view=aspnetcore-6.0) along with [Identity](https://learn.microsoft.com/en-us/aspnet/core/security/authentication/identity?view=aspnetcore-6.0&tabs=visual-studio) which is a framework for managing users, passwords, roles, claims, ...

## Storage

Our system stores data as file in filesystem or in database. Since the app is an information system with a relatively low number of user, we wanted to use a relational database with ACID properties. We also store a STR markers whose storing in relational tables is quite cumbersome if not put into the database as string or binary data which limits the flexibility and readability of the storage. Therefore, we also wanted our database to have a document model for storing genetic data. 

We chose [PostgreSQL](https://www.postgresql.org/) since it contains both relation and document models and has a provider for the storage library that we preferred to use.

## Storage Access

`C#` has a great ORM library for accessing database: [Entity Framework Core](https://learn.microsoft.com/en-us/ef/core/). The library mainly connects to relationship databases and there are many database providers for various database include [PostgreSQL EF Core provider](https://www.npgsql.org/efcore/). The library supports schema generations based on `C#` classes (models), migrations when these models change. EF Core implements stardard [LINQ](https://learn.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/linq/) interface which can be used to fetch, filter, join, transform data using the standard LINQ syntax with the advantage of the whole process being translated to run within the database (if possible - some of the LINQ operations are too general and complex to be run within db, e.g. [Group Join](https://learn.microsoft.com/en-us/ef/core/querying/complex-query-operators#groupjoin)). 

EF Core also provides options for full lazy or eager loading of foreign key linked models in another tables. There is also an option for a fine grained control where the user always specifies what additional models to load.

On top of that EF Core has great documentation and integrates well with ASP.NET Core framework. For example, tt can use the Logging interface internally to log requests to database. 

## GraphQL 

The bulk of our api is in [GraphQL](https://graphql.org/). There are two main libraries for graphql: [graphql-dotnet](https://github.com/graphql-dotnet/graphql-dotnet), [Hot Chocolate](https://chillicream.com/docs/hotchocolate/v12). We chose Hot Chocolate because of the quality of their documentation and there are more technologies from [Chilli Cream](https://github.com/ChilliCream/graphql-platform) which integrate well with Hot Chocolate. There is especially a nice graphql client [Banana Cake Pop](https://chillicream.com/docs/bananacakepop/v2) which provides great graphql api browsing experience. Hot Chocolate also includes guides at how to efficiently implement graphql resolvers using [data loaders](https://github.com/graphql/dataloader) in their library.

Hot Chocolate contains its own dependency injection implementation for registering input or output graphql types. It is possible to split mutation and query schema into multiple classes for separation of concerns. There is a support for paging, generic filtering and sorting support for endpoint output data which can be overridden and customized for our needs.

Hot Chocolate seamlessly integrages into ASP NET Core. It contains extension methods to map the graphql server into ASP NET Core server and can tap into ASP NET Core service dependency injection manager to provide out query and mutation endpoints with application controllers.

## Logging

It was mentioned in the ASP NET Core section that we use the [standard microsoft extension dotnet logginng interface](https://learn.microsoft.com/en-us/dotnet/core/extensions/logging?tabs=command-line). It does not contain a logging provider to database but there are many logging frameworks ([NLog](https://nlog-project.org/), [Serilog](https://serilog.net/)) which implement the intefaces and contain log sinks to many database technologies. Both of these frameworks can be well integrated to ASP NET Core and its config in `appsettings.json`. Our application uses Serilog for logging into PostgreSQL database and Console. There are sinks for logging to e.g. [Grafana](https://grafana.com/) but we chose not to do so to make the server maintainabality simpler.

## Validation

Validation of input based on business logic requirements for personal information is done using [Fluent Validation](https://github.com/FluentValidation/FluentValidation) library. 

## Tests

Out units tests are powered by [xUnit](https://xunit.net/) and [Fluent Assertions](https://fluentassertions.com/).

## Other Technologies

There are more libraries that we use for generating barcodes, pdfs, csv, fake data or sending emails or reading format such as excel. These are usually used in just one feature; therefore, they will be discussed with the feature.
