# Overview

In the future, this application is about to be managed by only around one user; therefore, the application must be simple to maintain. So we opted to go with one main monolithic runtime component architecture instead of roling out a more complex architecture like microservices. The business logic of the backend of our application is implemented in `Server` container which serves graphql and HTTP requests. Below, there is Contaner view of GAP system:  

![ GAP Backend Container View ](../images/structurizr-82509-Gap-Container-View.png)

Users mainly connect to frontend web UI app represented by `Client` container which sends request to `Server`. `Server` also provides public API. `Server` uses `Email Server` to send emails to users and accesses stored persistent data from `Gap Storage`.

`Data Loader` is an executable program whose main responsibility is to initialize all used storages and fill them with mock or real data. Real data typically include Phylogenetic tree, administrative region data for the countries where the application is deployed and old user personal and genetic data. Phylogenetic tree is downloaded from `FTDNA` system and saved in a supported format which `Server` can work with. `Map Data Fetcher` is employed to download the administrative data (from `Czech National Open Data Catalog` system in Czechia), simplify and save as a JSON file which can be provided as an input to `Data Loader`. Old user data were managed manually in various formats (usually Excel spreadsheets) by multiple people. `Old KU User Data Manager` can parse such input and again provide a JSON file to give to `Data Loader`. Mock data are generated directly in `Data Loader` and there is no need to use any external data to run the application with mock data.

## Server
Somewhat simplified `Server` component view is below. Note that relationships in this diagram do not necessarily mean code dependencies but rather workflow dependencies.

![ GAP Server Component View ](../images/structurizr-82509-Server-Component-View.png)

`Client` exclusively communicates with `API` component.

`API` component is mainly responsible for providing api GraphQL and HTTP endpoints, translating data transfer objects sent from `Client` to business logic objects which are then forwarded to the linked components. Any data returned from such linked components is then translated back to data transfer objects. GraphQL typically provides lazy field resolvers which are also part of `API`. It uses `Auth` component for securing the endpoints with policy based authorization. It also logs any request information.

`API` has quite a lot of responsibilities; therefore, it is split to more components and these are merge to one so that the diagram is readable. There is a small component for each of the linked business logic component which performs the aforementioned for the given domain the business component is responsible for. Some business components such as `Users` also represent multiple components so there smaller components have each their own api component. We discuss this in later per feature chapters. `API` contains also a small component for error handling which configures error codes for frontend.

Each component linked from `API` provides business logic for a certain domain. Sometimes there are multiple responsibilities and such components are further split up. Most of there component have their own associated storage which they access using a component with the same name with suffix `Storage Gateway`. These gateway components implement communication with given storage technology be it a database or only a file.

`Auth` component defines authentication and authorization scheme. It provides policies which can be applied to server endpoints and handlers which provide resolution whether given user is authorized or not.

`Logging` component provides logs of all change actions (mutations) that users perform in our application. It accesses log database through `Logging Storage Gateway`.

`Identity` component consist of subcomponents: `Registration`, `Login`, `Identity Manager`. `Registration` and `Login` do what they names suggest. `Identity Manager` provides user management in terms of creating account, claims, tokens, email confirmation and more. It uses `Email` component to send emails mainly for email confirmation. `Email` component communicates with `Email Server` using SMTP to send emails to outside world. `Identity` accesses roles and other objects from `Auth` to create tokens to pass authentication and authorization.

`Laboratory` component provides features for the lab of Criminalistic Institute so theu are able to conveniently process samples from registered users. It mainly includes tracking which kits were sent to users and received which samples were analysed or not. It also capable of generating labels for printer so that some of the lab work can be automated or an input to the sample sequencer of the lab. It has subcomponents: `Kit Management`, `Label Generation`, `Lab Results`, `Sequencer Input Generation`. Likewise other components, it accesses its storage using `Laboratory Storage Gateway`.

`Map` manages administrative regions of countries where this app is deployed and their hierarchy. It also provides `Haplogroup Map` component which is capable of computing value distributions upon the regions. Again, `Map Storage Gateway` component provides access to administrative region storage.

`Users` component provides fuctionality for working with user personal and genetic data. It has the following subcomponents: `User Access`, `User Sharing`, `Genetic Data`, `Haplogroup Prediction`, `Genetic Data Parsers`. `User Access` provides functionality for accessing, filtering and masking user data. `User Sharing` component lets users share their data with other users by giving them read and edit rights to own accounts. `Genetic Data` handles uploading STR markers from new sources, finding STR marker conflicts and their resoution. `Haplogroup Prediction` uses `NevGen` system for predicting haplogroups from user data. `Parsers` can parse STR results from external test provides such as FTDNA. `Users` accesses storage using `Users Storage Gateway`. The gateway is split into `User Access Gateway` and `Genetic Data Gateway` subcomponents.

`Closest Relative Search` component finds the closest relatives based on STR markers.

`Family Tree` component provides basic support for storing and fetching family trees.

`Phylogenetic Tree` component provides phylogenetic tree for the users of our system. It provides support for downloading a fylogenetic tree from FTDNA and pruning it based on haplogroups of users in our system so that the initially big tree is smaller and localized. It uses `Phylogenetic Tree Storage Gateway` to store all versions of the tree.

Most of the components are dependent on `Users` component since they usually require either users' STR data, users' haplogroups or their personal information.

## Storage

The previous diagram presented `Gap Storage` container which stores all data. That was to provide a simple view of the whole system but, in reality, there can be multiple runtime storages. The following picture presents them. Note that even though these are modeled as components in C4 model, they can still be runtime components.

![ GAP Storage Component View ](../images/structurizr-82509-Gap-Storage-View.png)

Note that while both `Server` and `Data Loader` have access to the storage, it does not mean that each implements their own storage access. As was seen in previous chapter `Data Loader` uses `Server` components for storage initiliazation and data loading. Note that the relationships are there to point out which storages `Server` and `Data Loader` access and what operation they perform on them, not actual direct code dependency.

`Identity Storage` stores data about all user accounts for managing user identity. This storage stores data for authentication, authorization, registration, login as well as user identity info such as emails and hashed passwords. The other storage which stores user data is `User Storage`. `User Storage` stores business logic personal (name, address, ancestor, ...) and genetic (STR markers, haplogroup, ...) data.

You may ask that if both store user data, why not use the same storage and reduce the complexity? While both storages store user data, they do so for different purpose and are managed differently. `User Storage` is completely managed by our application and we decide the layout. On the other hand, `Identity Storage` has layout based on the identity library that we use for managing auth. We feel that adding personal and genetic data to `Identity Storage` leads to coupling to certain identity library and results to inflexible database layout. Moreover, if in future we decide that another identity library or even standalone external identity service such as Auth0 would better suit our needs, we can delete `Identity Storage` and implement identity interface which our application defines without comproming our business logic user data in `User Storage`. 

`Log Storage` stores structurized server logging information.

`Laboratory Storage` contains any data relevant to lab management of GAP project. This almost exclusively means tracking information about if any lab tests are currently requested, what lab tests have been sent to users and have been received back and which collected genetic samples have been analysed in sequences and which not.

`Map storage` stores administrative regions and their hierarchy structure for the countries which the application supports (only Czechia at the moment). `Phylogenetic Tree Storage` stores original phylogenetic tree and its any created transformed variants. `Family Tree Storage` stores family trees for users.

### Storage Runtime Elements

While these storages can be all own runtime elements, storages that use the same storage technology and are completely managed by our application are for simplicity put in one database. That means `User Storage`, `Laboratory Storage`, `Map Storage` and `Family Tree Storage` are in the same PostgreSQL database. The access api to the data is defined for each storage independently to other storages and their data so this does not couple all data together. `Identity Storage` has its own PostgreSQL database for the aforementioned reasons. `Log Storage` schema is partially generated by logging framework; therefore, it has its own PostgreSQL database as well. 

These databases can be operated by all by one PostgreSQL server or each database can have its own. Our configuration only requires connection strings to the databases. 

`Data Loader` is responsible for initializing all the storages and populates `Identity Storage`, `User Storage`, `Laboratory Storage`, `Map Storage`, `Phylogenetic Tree Storage`, `Family Tree Storage`.




