# Development Process
This page describes the process and tools that we used while developing the application.

## Issue Tracking
We use YouTrack (a cloud-based web application created by JetBrains) as our issue tracker. All issues and features related to the application are tracked through a dedicated YouTrack workspace. An issue is only planned and available to work on if it's created in YouTrack. Each issue is assigned to the subsystem it is relevant for at the moment of its creation, and a developer assigns the issue to themselves once they start work on it.

Throughout the lifecycle of each issue its status is continually updated, though for our purposes we use a more simplified model than what YouTrack is able to offer. Each issue starts in the "Open" state, and becomes "In Progress" once work on it has started. Oncedevelopment is done and a merge request is createtd, it moves into "Waiting for Review", where it waits until the feature is merged, thereby becoming "Fixed".

There were exploratory efforts made to further organize our issue tracking by utilizing the built-in kanban boards (named "Agile Boards" in YouTrack), dividing issues into sprints, and assigning differing priorities to them, but those were met with middling success in our workflow.

## Communication
Most of our online communication both within the team and with outside stake holders took place through Slack. We also arrange periodic status progress meetings through Discord.

## Git Workflow
### Branch Structure
The project uses Git for source control, with the MFF GitLab instance being used as the "source of truth" central repository. There are two branches with designated special meaning, `main` and `production`.

The `main` branch represents the current state of development. All features are based on the `main` branch, and all are merged back into it. Care is taken that the branch is always in a usable state, i.e. the site is able to start and all tests pass on the head of `main` at any given time. Direct pushes to this branch are not allowed, all additions must come in the form of a merge request.

The `production` branch reflects the current state of the application as deployed in production. This branch is always behind or at parity with `main`, and updates to it are made solely by merge requests from the `main` branch. No commits can be pushed directly to this branch.

*(Note: A merge request may report that `production` is some number of commits ahead of `main`. This is purely a consequence of the fact that each merge into `production` creates an additional merge commit that is not present in `main`. It does not represent the actual logical state of the branches.)*

All other branches are feature branches. These branches are created from `main`, used to develop a single feature, and then merged back into `main` when done. Feature branches are deleted once merged, and their commits are not squashed as part of the merge. There are no requirements on how to resolve merge conflicts. Namely, we don't require a strictly linear history. Both rebasing and merging are accepted strategies for dealing with merge conflicts. 

Sometimes it can be warranted for feature branches to contain sub-branches. This typically happens in situations where a frontend developer and a backend developer wish to work together on a larger feature without disrupting each other and/or the sanctity of `main`. All such sub-branches are merged into their respective feature branch before that branch is merged into `main`.

#### Naming Conventions
Each feature branch is named in the format `GAP-XXX_feature_description`, where `GAP-XXX` is the YouTrack ID of the issue this branch corresponds to, followed by a short description of that issue. The underscores in this format can be equivalently substituted with hyphens.

Each commit made into a feature branch also has a commit message that starts with the prefix `GAP-XXX:`, substituting the corresponding issue ID. This convention aims to allow developers to easily identify what the scope of each branch is, as well as simplifying the determination of which feature caused a change while looking through the Git history.

### Merge Requests
When a feature is finished, a merge request back into `main` is created and the changes are submitted for code review. The developer who created a merge request designates a reviewer for it in GitLab. These reviews are then primarily performed through the built-in GitLab changelog viewer. It is imperative that no merge request is merged without at least one approving review. This ensures that no obvious oversights were made by the developer, that all agreed-upon conventions are being properly followed, and most importantly that each line of code is seen by more than one set of eyes, which increases overall visibility into the system. 

The changelog viewer allows for notes to be made on lines where the reviewer wishes to point out a flaw, suggest an alternative approach, or inquire about the decisions behind a particular implementation detail. This opens up a channel of discussion between the reviewer and the creator. A merge request cannot be merged until all outstanding discussions are resolved.

Due to this emphasis on collaborative development, we generally discourage developers from merging their own merge requests. It is customary for the reviewer to do so instead, once they are satisfied with the state of the implementation.

Aside from manual review, each merge request is also set up to run an automatic CI pipeline. This pipeline tests that the project as a whole builds correctly, that all tests pass for it and that all agreed upon code style conventions are followed. It's not necessary for every commit in the feature branch to pass the pipeline, but the feature as a whole cannot be merged until all pipelines pass.

Since we're operating under the express assumption that `main` is always in a correct state, as an optimization measure the pipelines for the frontend and backend portions of the repository are separate and each is run only when a code change is detected in their respective parts of the repository. This assumption also allows us to wholly omit running these pipelines on merge requests from `main` to `production`. 

### Single Feature Development Lifecycle
Combining all the above, let's describe the full lifecycle of implementing a single feature in the application, from conception to deployment.

1. An issue is created in YouTrack, describing the proposed feature.
2. A developer creates a feature branch from `main`, using the generated issue ID. They assign themself to the issue and change its status to "In Progress".
3. The developer works inside their feature branch, commiting changes as they progress. 
4. Once finished, the developer creates a merge request back into `main`. 
  - If there are merge conflicts between the feature branch and `main`, the developer resolves them.
  - If there's a failing integration pipeline, the developer examines the cause of the failure and fixes it.
5. The developer changes the issue state to "Waiting for Review" and assigns a reviewer to the merge request.
6. The reviewer reviews the request, opening discussions wherever they deem warranted.
7. The developer updates the branch in response to the review if necessary.
8. Once satisfied, the reviewer approves the merge request, merges it into `main`, and marks the correspondign issue as "Fixed".
9. After some time, the current state of `main` is merged into `production`, from where it is deployed onto the production server.

