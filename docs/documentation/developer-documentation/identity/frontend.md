# Frontend

## Determining Identity
The identity of a user is established through a combination of a numerial user ID and a JWT token the client obtains after a successful login. This ID and token are stored in the browser's local storage. The API client is configured to implicitly inject the JWT token from local storage into any outgoing request.

The JWT tokens have a set expiration date, and the client parses them so that when that expiration date is near, a notification and a countdown can be shown to the user, giving them the opportunity to log out and back in as convenient.

The service as a whole distinguishes between several different types of users, and the frontend as a whole must do likewise. A laboratory technician can view different parts of the site than a regular user, who can view different parts than an administrator. As such, the type of the currently logged-in user is also stored in local storage and used to inform these interface differences.

The only exception from the user type list of the API is that of the public API user. Such an user is never expected to connect throught the webapp in the first place, resorting to interacting with the API directly. As such, this type of user is not at all present or considered in any frontend code.

## Type-Based Application Views
Since different kinds of users have access to different parts of the site, we have to reflect that in what links we make available to each type of user. This is mostly accomplished by customizing the contents of the side menu.

Each side menu is declared as a list of menu items. Each menu item has a label and a list of paths it corresponds to (this is a list so that e.g. both `/` and `/map` are correctly mapped to the link for the map. It also has a `visibility` field, which enumerates what types of users are allowed to see the page behind that item. When the menu is rendered, each item is checked agains the type of the current user, and only those that match are shown. This gives access of each page only to the persons who have it.

For pages that are visible to several types of users but have to display different content based on that type, utility functions are exported from the `src/utils/api-client.ts` file allowing those pages to make that determination.

## Masking
Some users have the ability to directly manage accounts of other users. Administrators have that right for everyone by default, and users can designate other users who manage their accounts. To enable this feature, the client uses a technique we call masking.

Whenever a user wants to access the data of another user, they mask themselves as that user. When a mask is created, the stored user ID and user type of the client are replaced with those of the target (the originals are safely stowed away sideways). Only the JWT token remains that of the real user. Then, when sending any requests to the API, this substitution means the site basically behaves the exact same as it would if the target user was logged in directly. The API ensures through the real user's JWT that all actions are properly authorized. Then, once the user wishes to become unmasked, the original values are simply restored.

This approach comes with several advantages:
- Frontend components don't have to differentiate between real users and managers
- The client as a whole doesn't require any wide-reaching interference to support masking
- The mechanism is handled almost fully by the client, with the API being responsible only for ensuring proper authorization
- It is fairly simple to implement, without need for any sophisticated synchronization techniques

Of course, we want the user to be able to detect that they are masking as another user, so that they don't accidentally interact with the site in lieu of someone else. The site detects when masking is employed and displays a big colorful banner near the top of the screen. The banner shows the name of the target user and allows unmasking.

## Registration
User registration is handled as a series of sequential forms sharing a single `data` object. All filled in values are set onto this object. At the end of this sequence, the data is parsed into a representation accepted by the API and submitted.
