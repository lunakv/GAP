# Backups

As any project entrusted with managing the data of its users, we have a strong imperative to try and prevent any permanent data loss. To this end, the project needs a backup strategy.

We use the S3-compatible object storage cluster operated by CESNET. The data is sent daily into a single storage bucket. This bucket has object versioning enabled, archiving all previous versions of backed up documents to prevent permanent deletion.

Stored to the bucket are all user-uploaded files, all project logs, and a consistent dump of the full production database. It has been considered that creating snapshots of the database dumps may be storage intensive, as it's almost a guarantee that some changes will have been made between snapshots, causing the entire dump to be re-uploaded. We were assured by the backup provider that this won't be an issue for our expected database volumes.

There is currently no rentention policy specified on any object versions, meaning they all remain in the bucket indefinitely. Once we have a more accurate sense of how much data we are going to store, we will reconsider the possibility of configuring some retention scheme in consultation with the backup provider.

We use [restic](https://restic.readthedocs.io/en/latest/index.html) for the actual backup software. This S3-compatible option was recommended to us by CESNET and it supports native encryption, automatically creating and restoring from snapshots, and in general all the basic features one could expect out of a backup software. We are currently collaborating with CESNET to extend their knowledgebase with instructions on how to use restic in their storage system.
