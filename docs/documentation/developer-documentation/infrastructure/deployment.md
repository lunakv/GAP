# Deployment
This page describes the specifics of how the application is deployed to production.

## Deployment Structure
The application consists of several Docker containers. We chose to utilize containerization because it allows for an efficient and automated bundling and deployment system that is basically independent on the specifics of the hardware on which it runs. Docker was an obvious choice of container runtime, given its large market share and our familiarity with the technology. Other runtimes such as podman were considered, but ultimately rejected, as it was decided they didn't provide enough benefit to overweigh the aforementioned advantages of Docker.

### Containers
The site is served through an NGINX reverse proxy. This proxy takes the form of a container based on the [linuxserver/swag](https://github.com/linuxserver/docker-swag) image. This image is built to easily facilitate the configuration of NGINX and adds support for automatic fail2ban firewall rules and automated creation and renewal of TLS certificates through Let's Encrypt. Said certificate is created to support all utilized subdomains.

As the frontend client is created as a single-page application, it doesn't require any server runtime support and its build artifacts consist purely of static files. As a consequence, it doesn't require any additional software and we can serve this static content directly with the NGINX proxy server.

Since the frontend uses client-side routing, it is important that any route not mapped to the API or an existing file returns the site's index. This makes sure that when a client loads the site on a page that isn't the root, it still downloads correctly and the client is responsible for routing to the correct page.

The API server takes form of a Docker container that runs a compiled .NET binary. This container isn't exposed to the host network or the outside world directly. Instead, all content to it is passed through the reverse proxy. The GraphQL endpoint is mapped to `/api`, with other endpoints such as `/laboratory/*` being passed through as-is.

Also containerized is the database, which runs as a single PostgreSQL instance. This instance contains all databases used by the project. The container itself isn't connected to the host network, the outside world, or the reverse proxy. Instead, it utilizes a separate internal network through which it communicates with the API server. These two containers are the only entities on this network.

Lastly, there is the `mapdata` container. Rather than being part of the production runtime, this is a transient container that is run on each deployment to download and parsed the topographical data used in the project. Once available, this data is bundled as part of the static frontend site files.

## Orchestration
The containers are arranged declaratively through a single docker compose file, located at `infrastructure/docker-compose.yml`. This file describes each running container, assigning it startup commands, network ports, data volumes, and environment variables. The ecosystem of docker-compose was another reason for us to utilize Docker as our runtime. While a more complex deployment strategy could definitely be employed, such as a kubernetes cluster with autoscaling distributed orchestration, we came to the conclusion that for our purposes (a handful of containers running on a single machine) the complexity of such a deployment would massively overshadow any percieved increase in flexibility or future-proofness. In this regard, docker-compose proved powerful enough to suit all our needs while being straightforward enough to make setup fairly painless.

Whenever a change is pushed to the `production` branch, an automated CD pipeline runs. This pipeline builds all projects in the repository, bundles them into containers, and then deploys those containers on the production server over a remote connection. To prevent overload on the job runners and oversaturation of the network connection, this containerization isn't done on the CD runner locally. Instead, it connects to the production docker host through an SSH tunnel and sends commands directly to it, allowing us to perform the actual work on the production host while being automatically orchestrated from a remote location andwithout having to keep a copy of the source code.

There are two parts necessary for the production system that aren't handled through this deployment procedure: the database data and the phylogenetic tree file. These were omitted from the process as they only need to be inserted once initially to bootstrap the deployment. Once the tree file is added, it can be refreshed at any point with a simple API call. Similarly, once the databases are populated with user data, it's hardly needed to automatically re-deploy the database (and even smaller tasks such as automated migrations are generally not trivial).


To maximize flexibility, the application is almost entirely configurable through environment variables. General configuration options and overrides are defined through the compose file and `*.env` files inside `infrastructure/`, with some defaults for the API server declared in `source/GAP.Backend/Api/appsettings.json`. Sensitive configuration variables (passwords, keys, connection strings, etc.) are stored encrypted using the GitLab CI/CD Variables feature, where only repository maintainers can access them, and are injected into pipeline jobs only as needed.


