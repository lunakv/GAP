# Overview
The purpose of this section is to describe the specifics of how the system is actually deployed, decisions made affecting this deployment, and resources specific to the production server.

## Resource Providers
The production server is located at <https://dnaportal.cz>. This server is run on a virtual machine provided by [CESNET](https://www.cesnet.cz). This machine is part of the MUNI internal network. The object storage used for backups is also provided by CESNET.
