# Email

The site needs to send and receive email. Sending email is an absolute necessity for features such as confirming user registration and resetting passwords. While receiving emails is admittedly less of a priority, it was seen as desirable by us and the contractor to have the possibility of communicating with mail addresses dedicated to the project. Rather than using an existing off-the-shelf solution, we decided to build our own email server.

## Technology Stack
The site uses Postfix as an SMTP server for sending and receiving mail. It was a natural choice, as it is open-source, widely used (reportedly the [second most common](http://www.securityspace.com/s_survey/data/man.202302/mxsurvey.html) MTA worldwide), and with a large ecosystem of supporting tools and information available.

SASL authentication and mailbox management is done with Dovecot. This popular IMAP/SMTP server boasts easy integration with Postfix and is commonly paired with it.

Instead of the traditional approach of relying on actual system user accounts, we use virtual domains and virtual users. The tables enumerating these are stored in a dedicated MySQL database.

As a security measure, Postfix is configured to use Spamassassin for verifying incoming email, rejecting any mail that doesn't pass spam validation. Because we want to be able to reject suspect messages directly by terminating the incoming connection, the use of Spamassassin (run in the form of its `spamd` daemon) is facilitated by a simple proxy program called [Postprox](http://www.ivarch.com/programs/postprox.shtml), which runs the spam check while the connection is still active and passes messages through to the rest of Posfix's SMTP server after they are validated. We do this because if we checked the message only after the whole transmission was finished and the message queued, we would either have to silently discard suspected spam, which would drop false positives without either the sender or receiver knowing, or we'd have to send explicit bounce message, which potentially introduces unwanted backscatter.

Finally, outgoing messages are signed with a DKIM certificate using OpenDKIM.

The configuration files for these services are not publicly available, as some may contain sensitive information.

![Email Architecture Diagram](../images/structurizr-82509-Gap-Mailserver-View.png)

## Networking
All services are available on their standard ports. That means ports 25 and 587 for SMTP, 110 and 995 for POP3, and 143 and 993 for IMAP.

Outgoing mail is not sent directly to the destination mail server. Instead, an MTA located at `relay.muni.cz` is used as an outbound SMTP relay. This is done for two reasons. First, we are assured that this relay has excellent sending reputation, mitigating the risk of our mail ending up in spam folders of recipients. Second, it seems as though the network blocks outbound connections to port 25 (presumably in order to prevent spam originating there), so the only way to actually reach the destination MTA is to go through the network-designated relay.

## Processes
This section outlines the internals of how mail is handled in the system. For specifics on how to interact with the mail server as a user, please see the relevant sections of the User documentation.

### Sending Email
A user sends mail by connecting to Postfix and starting a TLS encrypted session. The user authenticates with their login credentials. These are verified by Postfix using the Dovecot plugin, and Dovecot in turn checks the password hash against its respective database entry. Once authenticated, the user sends their mail through a regular SMTP session.

Once the message is queued, its contents are signed by the DKIM service and the mail is routed to the relay. Note that for DKIM verification to work correctly, the `From:` and `Date:` headers must be present on every outgoing message. In particular, DKIM is configured to oversign the `From:` header in the verification hash to combat sender spoofing. All tested email clients set these headers automatically by default.

If a message cannot be delivered, an error explaining the details is delivered to the sender's inbox.

### Receiving Email
The sender connects to Postfix and initiates a SMTP session. This connection is accepted only if the recipient is a valid account inside our domain, which is checked using the MySQL table of virtual users. Once initiated, the communication is passed through Postprox running as a before-queue content filter. This filter executes a script that verifies the mail with spamassassin, immediately rejects it if it's suspected as spam, and re-injects it back into Postfix otherwise. Once received, the message is stored in the appropriate user's inbox.

### Reading Received Email
The client initiates a standard TLS encrypted connection through either POP3 or IMAP. This connection is handled by Dovecot, which connects to MySQL for the purposes of authentication. 


