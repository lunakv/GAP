# Security

Since we're dealing with potentially very sensitive customer data, it is absolutely paramount that necessary security precautions are taken. This page provides an overview of some of the practices employed in this project.

For the purposes of this page, it should be taken as given that any and all passwords mentioned are of sufficient complexity and length.

## Client-Server Communication
### Network Traffic Encryption
All communication with the server is encrypted. While port 80 is open on the inbound reverse proxy, it serves only to redirect to the HTTPS version of the site. The proxy is configured to not use outdated or vulnerable cyphers or encryption schemes. The TLS certificate for all used subdomains is signed by Let's Encrypt and automatically renewed before reaching expiration. HSTS is employed to mitigate the threat of protocol downgrade attacks. This combination of factors results in the site achieving an A grade in the [Qualsys SSL report](https://www.ssllabs.com/ssltest/analyze.html?d=dnaportal.cz&hideResults=on).

### Authentication and Endpoint Security
The API uses role-based access control for monitoring proper authorization, and all endpoints are carefully designed to only allow for authorized requests. In GraphQL endpoints where only some data can be made public, the verification can even be made on a field by field bases to check that no data leaks through a GraphQL request.

Authentication is done using JWT tokens with set expiration times. 

## Data Storage
The server runs inside virtual machine provided by CESNET on the MUNI network. This infrastructure provides default at-rest data encryption.

## Infrastructure Security
### Machine Access
A firewall is enabled on the machine that rejects all incoming connections by default. Only services provided by the server have their ports explicitly open to the world. In addition to this local firewall, a network-wide firewall is also enabled on the gateway to the machine's internal network. Any opening of new ports must be coordinated with an outside network operator with authorization to modify this outside firewall.

The production machine is accessible through SSH running on port 22 by default. (Should we want the port changed for the purposes of obfuscation, we must keep in mind that it must be opened on the network-wide firewall in addition to the local firewall.) The SSH server is configured to accept only key-based authentication, meaning both plaintext and interactive modes are completely disabled. 

The main administrative account with `sudo` privileges requires additional password authentication to execute any `sudo` commands. Access to this account is limited to the bare minimum, meaning only designated systems administrators possess SSH keys accepted by the server, not developers. It is highly encouraged that any private key used to log in to the system is password-protected.

The automated deployment is also done through SSH key-based authentication, though using a wholly separate user account. This account has permission to give commands to the Docker daemon (as is necessary), but doesn't possess any other roles or privileges on the machine.

### Database Access
The production database is configured to only be accessible from the API server. The API server communicates using a dedicated account that only has access to the production databases. The administrator `postgres` account is not used and a separate password is explicitly set for it to prevent unauthorized access.

### Credentials
All secrets used by the production deployment are stored encrypted inside the GitLab CI/CD Variables storage of the repository. There, only maintainers of the repository have access to them. To avoid leakage, these variables are configured to only be available in jobs running on the `production` branch, and the deployment jobs are carefully written to not leak any of them through the job logs.

## Email Security
### Traffic Encryption
All traffic to any part of the mail server is required to be encrypted, either through a regular TLS connection or with the `STARTTLS` command. The only exception to this is inbound mail on port 25, which cannot require TLS encryption in accordance with RFC 2487. On this port, unencrypted traffic is allowed, but logging in is not permitted outside of an encrypted session.

Given this requirement for encrypting the whole connection, we see it as sufficient that simple plain text authentication schemes are used.

### Accounts and File Access
Each process inside the mail stack has a separate user account with as few privileges as possible. Each sensitive file is restricted to be viewable only by the user/group for which it is necessary to view that file. Where possible, the Postfix subprocesses are run in a chroot jail.

The `rustic` binary has been granted the additional capability to list and read all files on the system regardless of permissions. This was done to allow for scanning of protected directories without the need to add the rustic system user into the docker group. This extended capability doesn't extend to modifying any files, and the binary is restricted to only be executable by `root` and the `rustic` group.

### Remote Backups
All backups are encrypted using a strong password before being stored into the backup S3 bucket. The bucket has object versioning enabled, so that files are recoverable even if someone tries to delete them from the remote storage.

### Database Configuration
The MySQL database storing user credentials has been hardened in the following ways:
- The root account has a dedicated password.
- The root account is not accessible from outside the local host.
- Anonymous-user accounts were disabled. 
- The default test database was deleted.

In addition, the database user through which the rest of the system connects has read-only permissions on the database, and a separate user exists for the purposes of administrating it. Both are accessible only through the local host. 

Passwords for email accounts are stored as a salted hash using 5000 rounds of SHA-512.

## Software Upgrades
It is important to keep all used software up-to-date with the most recent security patches. The `unattended-upgrades` package is configured on the server to automatically download and apply security upgrades for all packages installed on the server.

The libraries used in the API server project (and to a lesser extent the .NET runtime itself) should be monitored for known security vulnerabilities that can be exploited in our use-case, but no automated system for this has been set up. 

Since all frontend code is compiled into static files served as-is to the client, any reported vulnerabilities in our frontend dependencies are practically immaterial for us.
