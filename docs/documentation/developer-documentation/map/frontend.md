# Frontend

The map uses D3.js for visualizing the distribution. The benefits and properties of this library were already covered in the [previous section](../phylogenetic-tree/frontend.md), so they need not be repeated. One thing that is worth considering however is how we managed to integrate D3 into our React site.

## D3 Integration

D3.js clashes somewhat with React. This is because D3 works by manipulating elements in the DOM, but React doesn't like to give you access to it. Instead, it exposes a virtual DOM and only internally translates its changes into updates of the real DOM when necessary. This poses a problem.

Luckily, React includes an escape hatch in the form of the `useRef` hook. This hook creates a reference that you can pass to a component, and the value of that reference attaches the component to a specific DOM element. By utilizing this hook, we can create our own `useD3` hook that lets us use D3 to interact with components defined through React.

### The useD3 hook
The first parameter of this hook is a rendering function. The hook creates a reference using `useRef`, then makes a D3 selection on that reference and passes that selection to the rendering function.

The second parameter is a list of dependencies. D3 visualizations can be fairly computationally intensive, and we don't want to run them on every render. That's why we specify a list of objects, and the rendering function is run only when one of those objects changes. For example, in the map this list includes (unsurprisingly) the object representing region data and the object representing the map data.

The third parameter is an arbitrary object. We sometimes want the render function to utilize values and functions from outside contexts, such as the setter functions of `useState` hooks. This is easily done through closures when the function is created in the same scope that the variable is in, but it becomes unwieldy to have long D3 scripts in a component file. For this reason, we pass this third parameter, which is simply used as a collection of any auxiliary values the render function might need.

## Rendering Process
The following describes the workings of the map's render function.

The map loads the parsed TopoJSON file and injects it into a D3.js script. This script parses said file, combines it with the user data we receive from the API, then turns it into an SVG image. 

The regions are colored using a D3 ordinal color scale and change color based on their count. Clicking a region expands an extended table where users can see specific a per-haplogroup breakdown of how much they're represented both within that region and overall.

The render script is re-run whenever a change is made to the visualization, i.e. whenever the haplogroup selection, family name selection, or region specificity change.

Each region is linked to its highest encompassing region. This allows us to cleanly display borders of those highest regions on hover and correctly show the name of them in the hover tooltip.

## User Differentiation
The map has to work slightly differently for registered users and non-registered users. The 
