# Backend

Map on backend is implemented in `Api/Map`, `Map`, `PostgreSQLStorage/MapGateway`, `PostgreSQLStorageTests/MapGateway` directories.

## Haplogroup Map

Haplogroup map endpoint presents two main queries: `regionLayer`, `regionDetail` in [MapQueries](../../api/Api.Map.MapQueries.html).

The following diagram presents the business logic class diagram for map. The greyed out rectangles mean being defined and implemented in different feature..

![ Map Class Diagram ](../images/map-class.drawio.png)

[MapController](../../api/Map.MapController.html) provides a method with similar name for both queries. It contains [LayerMapFetcher](../../api/Map.MapFetcher.LayerMapFetcher.html) and [RegionMapFetcher](../../api/Map.MapFetcher.RegionMapFetcher.html) which are capable of creating for given regions or layers a haplogroup map implementing [IMap](../../api/Map.Map.IMap.html) interface. Both fetchers access the map storage holding administrative region hierarchy for the map using [IRegionFetcher](../../api/Map.MapFetcher.Storage.IRegionFetcher.html) or [IRegionLayerFetcher](../../api/Map.MapFetcher.Storage.IRegionLayerFetcher.html).

Haplogroup map provides statistics based on which users and haplogroups live in each region; therefore, both fetchers use [MapUserFetcher](../../api/Map.MapFetcher.MapUserFetcher.html) to get users and [IHaplogroupFetcher](../../api/Users.GenData.Haplogroup.Storage.IHaplogroupFetcher.html) to get all haplogroups. `MapUserFetcher` is a class which uses [CompleteUserFetcher](../../api/Users.UserAccess.CompleteUserFetcher.html) class to get users which have haplogroups. 

`CompleteUserFetcher` fetches [CompleteUser](../../api/Users.UserAccess.Entities.CompleteUser.html) list. If this list was used the entiry map would be dependent on `CompleteUser`; therefore, `MapUserFetcher` creates a new class `MapUser` which represents a user in a haplogroup map.

`IMap` interface provides its layers in form of [MapRegionLayer](../../api/Map.Map.MapRegionLayer.html) and both can provide their regions as [MapRegion](../../api/Map.Map.MapRegion.html). `MapRegion` class can compute the required haplogroup distrubution, family name distribution and haplogroup family name distribution for the region it represents. 

Either layer (for `regionLayer`) or a region (for `regionDetail`) are provided to GraphQL api. There is a [MapRegionType](../../api/Api.Map.Types.MapRegionType.html) objects which defines the fields row map region type in GraphQL.

## Administrative Regions

It is also possible to retrieve the administrative regions used in for haplogroup map separetely - `administrativeRegions` query in `MapQueries`.

## Administrative Region Storage

The hierarchy of administrative regions and the regions themselves are save in two tables. Table `Regions` contains all administrative region and table `UnitRegionMappings` contains mapping between the smallest administrative regions and the regions that geographically contain them.

[IRegionFetcher](../../api/Map.MapFetcher.Storage.IRegionFetcher.html) and [IRegionLayerFetcher](../../api/Map.MapFetcher.Storage.IRegionLayerFetcher.html) are the intefaces which define how administrative regions should be fetched from database. They are implemented by [RegionFetcher](../../api/PostgreSqlStorage.MapGateway.Accessors.RegionFetcher.html) and [RegionLayerFetcher](../../api/PostgreSqlStorage.MapGateway.Accessors.RegionLayerFetcher.html). These fetchers use [EF Core](https://learn.microsoft.com/en-us/ef/core/) and [PostgreSQL EF Core provider](https://www.npgsql.org/efcore/) to access PostgreSQL database.

Administrative layers can be added via [IAdministrativeRegionLayerStorageSaver](../../api/PostgreSqlStorage.MapGateway.Accessors.AdministrativeRegionLayerStorageSaver.html) and [AdministrativeRegionLayerSaver](../../api/Map.RegionDataSaver.AdministrativeRegionLayerSaver.html) which on top of calling `IAdministrativeRegionLayerStorageSaver` to add layer to storage adds one layer with global region which contains all regions.

