# Generating Map Data
In order to display a map containing the haplogroup distribution, we first have to obtain the necessary topological data describing the country. Luckily, such data is readily available through the National Open Data Catalog.

We have created a separate `mapdata` project as part of our repository that's responsible for downloading up-to-date map data and transforming it into a format that's usable for us.

## Downloading Map Data
The data is published [in the catalog](https://data.gov.cz/dataset?iri=https%3A%2F%2Fdata.gov.cz%2Fzdroj%2Fdatov%C3%A9-sady%2F00025712%2Fe0fe186c71d535aeb8effc5e212364ae) as part of the RUIAN project. It consists of a collection of shapefiles describing the topology of each region, as well as metadata describing said region (name, subdivisions, ...)

The data is organized into layers, where each layer represents a particular kind of administrative region. There are layers for towns, municipalities (obce s rozšířenou působností, counties (okresy), regions (kraje), and several others. The latter three layers are the ones we're interested in.

## Parsing Map Data
The data as downloaded is stored in the form of ESRI shapefile files. Any text in them is encoded using the Windows-1250 encoding. Since shapefiles are not a particularly friendly format to work with, and the base data includes a lot of extraneous information we don't need, our first step is to transform the data we need into a different format. We chose (UTF-8 encoded) TopoJSON for this purpose.

TopoJSON is an extension of GeoJSON. Whereas GeoJSON is a JSON-like format describing the representation of geographical features, TopoJSON builds upon it by also considering topology. This addition tends to lead to reduction in file size, as geometries no longer need to be represented discretely. Another benefit of this format is its excellent integration with d3js.

To complete this transformation, we first adjust the encoding and map projection of the shapefiles using the GDAL translation library. Then we transform these modified shapefiles into GeoJSON files and add some additional properties to each region in them (name, ID, and ID of enclosing region). These files are then finally compiled into a single TopoJSON file.

As an optimization measure, the topology of this file is slightly simplified as the last step of this process.

## Mapdata Project Structure
Our map data generator script utilizes a combination of regular system packages and binaries that are only available as part of an NPM package. This results in a slightly unusual structure for this particular project.

Nominally, it is a NodeJS project with regular NodeJS dependencies. However, there isn't a single line of actual JavaScript written in the project. Instead, the `package.json` declares a single script, `fetch`, which runs a shell script. The reason this shell script has to be run indirectly through a package manager is because this inserts the JS dependencies into the script's `PATH`, allowing us to run NPM package binaries inside it.

There's also a prepared Dockerfile which builds the project into a container with all system dependencies installed.
