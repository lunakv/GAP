# Frontend

This documentation provides an overview of the implementation details of the Frontend Application Behavior as Managed User feature. This feature allows users to seamlessly switch to the perspective of a managed user, providing an immersive experience within the application. The implementation involves storing logged user data in local storage, switching user information, and maintaining token-based authentication for successful server requests.

## Implementation Overview

The Frontend Application Behavior as Managed User feature is designed to enhance collaboration and facilitate efficient data management. It simplifies the process of exploring the application from the perspective of a managed user by utilizing local storage and token-based authentication. The following steps outline the implementation:

**Storing Logged User Data**: When a user logs into the application, their user data, such as name and ID, is stored in the local storage. This information serves as a reference point for switching to the perspective of a managed user.

**Switching User Information**: When a user wishes to use the application as a managed user, the frontend retrieves the managed user's data, including their name and ID, and replaces the current user data in the local storage. This switch allows the application to behave as if the managed user were the active user.

**Token-Based Authentication**: Despite the switch in user information, the token-based authentication system remains unchanged. The existing JSON Web Token (JWT) associated with the active user is still used for making server requests. This ensures that the requests made as the managed user have the necessary permissions to perform user management operations.

**Server Requests**: When the frontend sends requests to the server, the token associated with the active user (previously the logged user) is included. These requests, bound by the active user's token, are authorized by the backend to perform user management operations on behalf of the managed user. This allows for a seamless experience when manipulating data as the managed user.

## Data Privacy and Security Considerations

The implementation of the Frontend Application Behavior as Managed User feature upholds data privacy and security. The token-based authentication system ensures that only authorized users can access and manage user data. The switch in user information within the local storage allows for a smooth transition to the managed user's perspective without compromising the overall security of the application.

## Conclusion

The implementation of the Frontend Application Behavior as Managed User feature leverages local storage and token-based authentication to provide a seamless experience for users exploring the application as managed users. By storing logged user data and switching user information, users can easily switch perspectives and perform user management operations while maintaining the necessary security measures. The integration of token-based authentication ensures that server requests made as the managed user are authorized and bound to succeed.
