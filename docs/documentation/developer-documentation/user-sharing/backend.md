# Backend

Every user is saved within the database and can be loaded or changed on demand. User changes are always (except initial database population) triggered by a user from the frontend. It doesn't have to be the user in question, as any given user can allow other users to change his data. This process from the backend point of view will be described below. Let's first look at user structure and workflow to understand what the matter.

As mentioned, any changes to user data happen from frontend and so through mutations. [UserMutations](../../api/Api.Users.UserMutations.html) follow this proceeding workflow pattern:
![User Mutations Diagram](../images/UserMutate.drawio.png)

The mutation triggers a cascade reaction within the software. The [UserMutations](../../api/Api.Users.UserMutations.html) class calls the [Users.UserController](../../api/Users.UserAccess.UserController.html), which fetches the respective user using the [PostgreSqlStorage.UserFetcher](../../api/PostgreSqlStorage.UsersGateway.UserAccess.Accessors.UserFetcher.html) class. It then changes the desired field of it and calls the [PostgreSqlStorage.UserSaver](../../api/PostgreSqlStorage.UsersGateway.UserAccess.Accessors.UserSaver.html) to propagate them into the database.

User access may very well occur as a request from the frontend as well, and the [Api.UserQueries](../../api/Api.Users.UserQueries.html) class is ready for it. But user data is needed at many points of the application. For example, the [closest relatives search](../closest-relative-search/overview.md), [phylogenetic tree](../phylogenetic-tree/overview.md), [map](../map/overview.md), or even the above-mentioned user mutations load the user from the database first. The application also includes public API, which only returns respective users as a set of their markers, haplogroups, municipalities and publicIds. These publicIds are separate from the internal ids with no relation.

## Data Structure

When considering the data view, the following diagram describes the structure of the [User](../../api/Users.UserAccess.Entities.User.html) class and its fields:
![User Classes Diagram](../images/UserClasses.drawio.png)

Another important class is [CompleteUser](../../api/Users.UserAccess.Entities.CompleteUser.html), which only differs from [User](../../api/Users.UserAccess.Entities.User.html) by the nonnullable [GeneticData](../../api/Users.GenData.GeneticData.html) field. From the database point of view, there is only one class for all the fields, which is [PostgreSqlStorage.User](../../api/PostgreSqlStorage.UsersGateway.UserAccess.Models.User.html). The only exception are haplogroups, which are stored in the database separately. Also, the genetic markers are not stored as respective strings and integers but as one compact json file.

## User Sharing

Now, when we understand the user structure and data access process, we can dive into the sharing rights. From the point of view of extensibility, the sharing rights are divided into view and manage rights. Backend differ those two very strictly and users can only change others' data with manage right and view it with view right. However the client does not require this feature yet and so frontend is not reflecting it. 
On backend, there are the [Api.SharingQueries](../../api/Api.UserSharing.UserSharingQueries.html) and [Api.SharingMutations](../../api/Api.UserSharing.UserSharingMutations.html) those give the endpoint to change/view these rights. Those use the [UserViewController](../../api/Users.UserSharing.UserViewSharing.UserViewController.html) (respectively [UserManagementController](../../api/Users.UserSharing.UserManagementSharing.UserManagementController.html)) to add, remove or view some rights. These controllers then uses respective RightFetchers and RightSavers to uccess and modify the database.