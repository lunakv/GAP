# Overview

The application provides users with an interface to access and analyze their own genetic data. The application supports multiple sources of genetic data, including KU labs and FTDNA.

One of the key components of the application is the "Aggregated markers" pseudo file. This "file" contains merged marker data from different sources. In cases where there are conflicts in the data from different sources, the conflicts are resolved by a KU worker (this process is explained in a separate section). The Aggregated markers file provides a comprehensive overview of the user's genetic markers, combining data from various sources.

The application allows users to compare data from different sources and identify any variations or differences in marker values. This feature enables users to gain insights into how their genetic data differs across different data sources. By visualizing and analyzing these variations, users can better understand their genetic profile.

Additionally, the application supports the downloading of genetic data in FTDNA format. Users can export their genetic data in this specific format for further analysis or sharing with other platforms or individuals.

Furthermore, the application supports the uploading of genetic data files. Users can upload their own genetic data files, allowing them to conveniently store and manage their genetic information within the application. Additionally, users can access and view the genetic data of other users who have granted them access. If granted access, users can even upload genetic data files on behalf of other users, streamlining the data management process.

In summary, the application provides users with a user-friendly interface to explore, compare, and analyze their genetic data. The application supports multiple data sources, enables the comparison of marker values, facilitates the downloading and uploading of genetic data files, and allows users to access and manage genetic data from other users with appropriate permissions.

## Conflict Resolution

The feature utilizes the KU worker to resolve conflicts in genetic data merging. Conflicts arise when different data sources provide varying values for the same marker. The KU worker's role is to analyze these conflicts and determine the most accurate marker values based on predefined rules.

The KU worker is equipped with an interface, presenting a table containing markers for evaluation. This table allows the worker to easily review the conflicting marker values and make informed decisions. If necessary, the worker can override any marker value with their own judgment to ensure accuracy.

Once the conflicts are resolved, the resolved marker values are incorporated into the Aggregated markers "file", providing users with a comprehensive and accurate representation of their genetic data.

The resolution process is visually indicated to users through a small red exclamation mark icon. Once the conflict has been resolved the the exclamation mark is no longer shown.