# Backend

Endpoints for genetic data are in `Api/GenData`. The business logic in `Users/GenData` and storage gateway in `PostgreSqlStorage/UsersGateway/GenData`.

The main endpoints for genetic data are uploading files from external STR marker test providers and integrating them to our system. Uploading new STR marker source when some STR marker source(s) are already present can trigger conflict - values on the same markers differ between two or more original str data sources.

## Haplogroup Prediction

We use [NevGen](https://nevgen.org/) for predicting haplogroups from STR markers. There is a [IHaplogroupPredictor](../../api/Users.HaplogroupPrediction.IHaplogroupPredictor.html) interface for services for predicting haplogroups based on STR markers. There are two implementations:
[NevgenHaplogroupPredictor](../../api/Users.HaplogroupPrediction.NevgenHaplogroupPredictor.html) and [RandomHaplogroupPredictor](../../api/Users.HaplogroupPrediction.RandomHaplogroupPredictor.html).

The random predictor predicts random haplogroups which exist in the database. It is fast good to use for mock data or when testing something else and using NevGen is slow.

`NevgenHaplogroupPredictor` as mentioned is slow. It connects to NevGen internal api. It is important to note that nevgen.org does not provide an intentional public API as of the time of this project. However, communication with the owner of nevgen.org has taken place, and with their consent, the underlying PHP code is utilized to determine the haplogroups of users. The PHP code is written in Serbian, making navigation and understanding of the code within the predictor class potentially challenging. However, the owner of nevgen.org has expressed interest in creating a public API, which would make this part of the GAP software a good candidate for future renovation.

It is worth mentioning that since every haplogroup prediction requires an HTTP request, the predictions are generally slow within the application. That is not a problem in most cases, since those predictions are rare. The only problematic part being prediction of haplogroups of Users signed up before this software creation ([detail](../backend/legacy-data.md)).

There is an option for switching between the user implementation in `appsettings.json`. 

## Parsers

Implemented in `Users/GenData/Parsers`.

Currently, the only file format we support is CSV file format from [FTDNA](https://www.familytreedna.com/). The parser is implemented in [FtdnaStrParser](../../api/Users.GenData.Parsers.FtdnaStrParser.html). There is [IExternalStrFileParser](../../api/Users.GenData.Parsers.IExternalStrFileParser.html) interface for parsing STR data from external sources. Some test provides provide STR data in pdf formats ([Genomac](https://www.genomac.cz/)) so if the provider is not supported it is possible to upload data manually. Therefore, there is [UnsupportedProviderParser](../../api/Users.GenData.Parsers.UnsupportedProviderParser.html) which takes as an argument genetic data and a file which it does not parse at all.

There is also [StrParserResolver](../../api/Users.GenData.Parsers.StrParserResolver.html) which contains all supported provides and tries to parse a input file at least with one of them. This is useful for endpoints for uploading data with supported provides because there needs to be only one instead of having one endpoint for each test provider (and parser).

Note that there is also STR parser for lab genetic data but that is implemented in [Laboratory](../laboratory/backend.md).

## Original STR Data Sources

When a new STR file is uploaded, it must be saved to filesystem for the user and admin to view later. Admin can for example later check the validicity of the file. Also we want to preserve the information of the test provider who provided the test. Class for this is called [OriginalStrDataSource](../../api/Users.GenData.OriginalStrDataSources.OriginalStrDataSource.html) and it preserves the original uploaded data with metadata and filename of where the input file is stored.

## STR File Upload

The endpoint for STR file upload is [UploadGeneticDataMutations](../../api/Api.GenData.UploadGeneticDataMutations.html). It publishes `addStrMarkers` mutation for uploading data with unsupported provider (STR markers are part of the mutation input). Mutation for uploading STR data with supported provider is `AddStrMarkersFile`.

Both endpoints call [GeneticDataUploadController](../../api/Users.GenData.GeneticDataUploadController.hmtl) with correct `IExternalStrFileParser` parser implementation. The controller performs basic parameter validation for the existence of user and that the file can be parsed. It uses [GeneticDataUploader](../../api/Users.GenData.GeneticDataUploader.html) to save the original STR file to file system and to correctly upload the data to our system. Several places need to be updated, a conflict can arise and new haplogroup needs to be predicted. The new aggergated STR of all sources are part of [User](../../api/Users.UserAccess.Entities.User.html) along with the new haplogroup.

Note that there can be only one STR conflict for each user. If new data are uploaded when conflicts exists, it is updated.

Various genetic data need to be stored separately. There is [IOriginalStrDataSourceSaver](../../api/Users.GenData.OriginalStrDataSources.Storage.IOriginalStrDataSourceSaver.html) for storing original STR data sources, [IUserSaver](../../api/Users.UserAccess.Storage.IUserSaver.html) for updating user genetic data and [IStrConflictSaver](../../api/Users.GenData.Conflict.Storage.IStrConflictSaver.html) for saving conflicts if any occured.

There is [ISaverFactory](../../api/Users.ISaverFactory.html) interface for creating storage gateway savers for genetic data and users is such a way that they can be used in one transation. There is such implementation if all such data are stored in the same PostgreSQL database [SaverFactory](../../api/PostgreSqlStorage.UsersGateway.SaverFactory.html) which for these exact data makes sense. Other unrelared storage apis are not included in the interface.

## Storage

Aggregated STR data and haplogroup are stored with user in database. Conflicts and original str sources have their own tables. 


## Conflicts 

There is a query for getting the conflicts [ConflictQueries](../../api/Api.GenData.ConflictQueries.html) `strConflicts` and a mutation for fixing them [ConflictMutations](../../api/) `fixStrConflict`.

The main logic is done in [ConflictController](../../api/Users.GenData.Conflict.ConflictController.html). [IStrConflictSaver](../../api/Users.GenData.Conflict.Storage.IStrConflictSaver.html) and [IStrConflictFetcher](../../api/Users.GenData.Conflict.Storage.IStrConflictFetcher.html) are used to access storage with conflicts.