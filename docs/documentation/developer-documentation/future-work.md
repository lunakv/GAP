# Future Work

Rarely can a piece of software be called truly complete. There are always improvements, fixes, additions and extensions to be made. In this page, we'd like to consider the ways in which the project could develop in the future.

It is important to note that for KU, our contractor, this was treated as a vanguard project in their effort to modernize genetic genealogy. It is our understanding that they have great plans for this area, and we hope this project will serve as a good basis to build upon, though we (and perhaps even they) cannot fully anticipate the direction those efforts will take.

With that being said, we'd like to outline a few areas where we see the possibility for future work on the project.

### More Built-In Analysis Features
If the aim of the project is to facilitate data analysis, new ways of analyzing that data are always an area of interest for future development. We always provide an escape hatch of sorts in the form of exporting the data in a suitable format, allowing users to work with it directly, but if the contractor decides a new analytics feature is important enough, it could be integrated directly into the site.

### Adding Support for External Genetic Data Providers
We currently support importing Y-DNA test results directly from FTDNA. This company was chosen as it is a dominant player in the space of analyzing Y-DNA, and because it allows results to be exported in machine-readable formats. Data from all other providers has to be added manually and verified by a qualified lab technician.

Future work could add importers for other services that provide these tests, alleviating some of the work from actual personnel.

### Family Tree Integration
We use an in-house implementation of a family tree that the users can fill in. The site could possibly be extended to allow transferring the family tree data of users to and from other services, such as WikiTree or MyHeritage.

### More Login Options
There were brief discussions during development about adding support for OAuth-based authentication, most likely using the [MojeID](https://www.mojeid.cz/) provider for login instead of simple password logins. Another possible addition to the login workflow is the addition of multi-factor authentication.

### Responsive Design
As was previously noted, we (in consultation with the contractor) made a conscious decision to develop the site's layout with the anticipation of being viewed on desktop screen sizes. A possible are of improvement would be to re-imagine the layout in a way that would make it more responsive and make the site more available on mobile devices as a result.

