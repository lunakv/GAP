# Backend

`Laboratory` has four main business areas: `Kit Management`, `Label Generation`, `Lab Results` and `Sequencer Input Generation`.

## Kit Management

Endpoints located in `Api/Laboratory/KitManagement/`, business logic in `Laboratory/KitManagement/` and storage gateway in `PostresqlStorage/LaboratoryGateway/KitManagement`.

Application controller is [KitController](../../api/Laboratory.KitManagement.KitController.html). The main entity is [Kit](../../api/Laboratory.KitManagement.Kits.Kit-1.html) which has four descendants with each representing different stage of the kit. Kits can be advanced by [KitStageAdvancer](../../api/Laboratory.KitManagement.KitStageAdvancement.KitStageAdvancer-1.html). 

Kits can be fetched and advanced to next stages but the important features the other three.

## Label Generation

Endpoints located in `Api/Laboratory/LabelGeneration/`, business logic in `Laboratory/LabelGeneration/`.

The following diagram shows the class diagram for label generation.

![Label Generation Class Diagram](../images/label-generation.drawio.png)

The goal is to generate a pdf file with labels for printer with given layout with user residence adresses or kit identifiction with barcode. [LabelDocument](../../api/Laboratory.LabelGeneration.LabelDocument.html) is that class that handles generating pdfs based on given layout. It uses library [QuestPDF](https://www.questpdf.com/) which provides great support for creating pdf reports and is open source. The input to the document if list of [ILabel](../../api/Laboratory.LabelGeneration.Labels.ILabel.html) which represents a label to be put in one place in pdf layout. The version for address is [AddressLabel](../../api/Laboratory.LabelGeneration.Labels.AddressLabel.html) and the one with kit identification and barcode is [KitIdentificationLabel](../../api/Laboratory.LabelGeneration.Labels.KitIdentificationLabel.html). These labels can be created by [LabelFactory](../../api/Laboratory.LabelGeneration.Labels.LabelFactory.html). It uses [BarcodeGenerator](../../api/Laboratory.LabelGeneration.Barcodes.BarcodeGenerator.html) to generate barcode of the type chosen in documentation and passes it to `KitIdentificationLabel`. It users [ZXing.Net](https://github.com/micjahn/ZXing.Net) library for the barcode generation. It has little documentation but is the one of the few libraries that support barcode generating on all platforms (is not dependent on [System.Drawing](https://learn.microsoft.com/en-us/dotnet/api/system.drawing?view=net-6.0)).

## Lab Results

Endpoints located in `Api/Laboratory/LabResults/`, business logic in `Laboratory/LabResults/`.

It contains a parser [LabResults](../../api/Laboratory.LabResults.LabStrFileParser.html) for lab STR file format. [LabResultsController](../../api/Laboratory.LabResults.LabResultsController.html) uses [GeneticDataUploader](../../api/Users.GenData.GeneticDataUploader.html) to correctly upload STR data of all users. This is done transactinally.

## Sequencer Input

Creates inputs for sequencers. The main logic is done using [CsvSequencerInputGenerator](../../api/Laboratory.SequencerInputGeneration.CsvSequencerInputGenerator.html)