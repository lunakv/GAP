# Developer Documentation

This is the developer documentation for the GAP project. Its purpose is help developers familiarize themselves with the codebase and document the decisions made while writing it. It is structured as follows.

First, some introductory pages describe the overall system and how to work with it. Then, for each major feature of the site, a section is added describing it from the backend and the frontend. Following those is a section on the infrastructure and how the site is actually deployed. And finally it ends with some thoughts on the possible future directions of the project.

The developer documentation assumes familiarity with the basic functionality of the site. Readers are expected to have an understanding at least at the level provided by having read the [user documentation](../user-documentation/overview.html).