# Architecture
The architecture of our solution is described according to the C4 model. The picture below shows the system view.

![ GAP System View ](images/structurizr-82509-Gap-System-View.png)

The GAP system is used by Unregistered Users, Registered Users, GAP managers (Root, Admin, Lab Tech) and Public API Consumers. 
- Unregistered Users can open the GAP homepage, read about and join the GAP project.
- Registered Users are users who already joined GAP project and either applied for STR tests or uploaded their STR data from external test providers.
- Root, Admin and Lab Tech are managers of the system with different privileges. Lab tech can access any laboratory related part. Admin and Root users have access to user personal and genetic data, manage any genetic conflicts. Root additionally can create new manager accounts and can monitor user actions within GAP system. 
- Public API Consumer has access to server public API which provides pseudonymized genetic data.

GAP system communicates with FTDNA system, NevGen system and Czech National Open Data Catalog. 

FTDNA is a commercial system which provides STR and SNP marker testing and a lot of visualization features based on the genetic data. Our system uses FTDNA public api to download phylogenetic tree of all known STR haplogroups. 

Nevgen is a system for predicting haplogroups from STR markers. Our system calls Nevgen api whenever any new STR markers are uploaded to get haplogroup predictions.

Czech National Open Data Catalog stores and provides metadata about open datasets in Czechia. Our system uses it to download the administration regions.
