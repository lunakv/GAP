# Localization

From the beginning, we aimed for the site to support multiple languages. Not only is that a benefit to the few users of the site who don't speak Czech (or just feel more comfortable using English), it also forces us into the good practice of separating our resource strings from the rest of the code. This way, adding a new language or adjusting the wording of a component is as straightforward as finding the entry inside the resource files and changing it.

We use [i18next](https://www.i18next.com/) (as well as its React plugin) for the actual translation management. The translations are stored as a series of JSON files containing keys and values for each string resource. Each language has its own directory with identical structure and keys in each file. As of now, Czech and English are the two supported languages.
