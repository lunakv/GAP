# Overview
The frontend project is a single-page application written in React. This page outlines the basics of how the frontend project is structured and a brief summary of how it works. Following pages expand on this overview with more detailed explanations.


## File Structure
The project was created using `create-react-app`, so it uses the standard basic structure of CRA apps. The `src` directory contains most of the source code, and `public` stores static content and localization files.

The project follows a standard directory structure, with each major feature having its own dedicated directory. For example, features like Map, Phylogenetic Tree, and Family Tree have their own separate directories, promoting modularity and organization. Additionally, there is a shared documents folder for components, utilities, and other resources that are commonly used across different features.

Within `src`, the most notable directory is `features`, which contains one subdirectory for each main feature of the site, alongside an `xShared` directory for components used in multiple features.

### Feature Structure
The feature directories don't have any fixed rigid structure, but they tend to be a flat collection of files pertaining to that feature. It is typical, though not required, that declarations of API queries used in the feature are placed into a `queries.ts` file, and any types that aren't mirrors of an API type are placed into `types.ts`. The exception to this are prop types, which are placed right alongside the component they're describing.

## Site Hierarchy
The site's main layout consists of four parts. A top bar, a side menu, and the main content area, and a footer. The main area is populated by the features of the site. The is declared in the `src/features/AppSkeleton` directory.

Since most of this layout is mostly independent on the actual feature being displayed, we have a `withMainLayout` HOC that accepts a component and renders that component inside the main area of this layout.

The site is fully assembled inside `App.tsx`. There, routes are mapped to pages and served to the router, and all context providers are created.

It should be noted that this layout is not designed as being responsive. We are acutely aware of this. When the issue of responsiveness was discussed in meetings with the contractor, the team was explicitly told that viewing a site on mobile screens is not a required or even wanted feature. After this direction was given, we decided that given the position of our main stakeholder it is better to spend the time and effort on other parts of the site. We leave the issue of redesigning the layout to be responsive as an area of future improvement. The team tried to make the components incidentally mobile-friendly where possible to ease this future conversion.


## Server Communication

The client communicates with the GraphQL backend using the [Apollo Client](https://www.apollographql.com/docs/react/) library. We create a singleton object of this client at startup which configures the proper connection, and then all requests are routed using that client. Most calls don't rely on the client directly, instead utilizing the built-in context and hooks support from Apollo.

### Error Handling
The API calls can result in errors. The API client is configured so that when any error occurs, it is logged and a small, visible, dismissible popup is displayed on top of the screen. The backend has a specific error code mapped for each kind of exception. These error codes are sent as part of the errors themselves, and frontend uses this mapping to display the proper message for every failed request. This absolves most of the rest of the project of any responsibility for error handling This absolves most of the rest of the project of any responsibility for dealing with errors.

### Client-Side Routing

The client-side routing is done locally using [React Router](https://reactrouter.com). To ensure proper CORS validation and similarity between development and production environments, a Webpack proxy is set up to run on deployment, automatically routing all API calls to the actual standalone API server.

When referencing URL paths, we want to ensure that we are not simply passing string constants around, as that would be difficult to maintain. To that end, all paths used by the router are declared in `src/route-definitions.ts`, and anything referencing a path uses one of those routes to do so.

Note that the routes aren't simple strings, but objects with a `path` key. This was done to allow for easier future expansion, should we want to add additional attributes to those routes.