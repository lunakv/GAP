# Správa pracovníků

Jakožto **root** máte možnost vytvářet a mazat speciální spravující účty. Mezi které patří:

- **Root**: má přístup ke všem funkcionalitám aplikace
- **Administrátor**: má přístup k většině administrátorské funkcionality, krom několika výjimek. Jednou z nich je právě *správa pracovníků*. ostatní jsou zdokumentovány v příslušných sekcích
- **Laboratorní pracovník**: zodpovídá za analýzu genetických kitů (viz sekce [Vzorky](../lab-worker/genetic-samples.md))

Na obrázku je vidět formulář na přidání nového pracovníka. Zde je důležité upozornit, že email tohoto pracovníka nesmí být evidován nikde v aplikaci, tj. nesmí již být pracovníkem a ani nesmí být asociován s uživatelem aplikace.

![formulář a tabulka](../images/staff-management-landing.png)

Po přidání se zobrazí hláška a je automaticky obnovena tabulka s administrátory.

![uspěšné přidání](../images/staff-management-success-add.png)