# Přehled

V této části se dozvíte, jak aplikaci používat. Rozlišujeme několik různých typů uživatelů a každý z nich má definovanou množinu funkcí, které může provádět. V následujících kapitolách jsou všechny podrobně popsány.
