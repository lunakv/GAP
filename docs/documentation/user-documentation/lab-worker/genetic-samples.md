# Vzorky

Zodpovědností laboratorního technika v systému je zpracování vzorků s genetickými daty, které si uživatelé nechali skrz projekt zaslat. Pro vzorky musí být vytištěny adresy a identifikující čárové kódy, musí být odeslány žadateli, přijaty a zanalyzovány v laboratoři. Výsledky této analýzy se pak projeví v genetických datech odpovídajícího uživatele.

Na stránce si technik může zobrazit stav všech vzorků, které byly v rámci projektu zaslány. Rozlišujeme čtyři různé stavy vzorků:
1. Neodeslaný vzorek. V tomto stavu je vytvořen po registraci vzorek uživatele, který zažádal o zaslání testovací sady.
2. Odeslaný vzorek. Do tohoto stavu se vzorek dostává poté, co se testovací sada odešle z laboratoře žadateli.
3. Přijatý vzorek. Tento vzorek dorazil od uživatele zpět do laboratoře, ale ještě neprošel analýzou.
4. Analyzovaný vzorek. Tento vzorek už vyprodukoval žádaná genetická data, která byla nahrána do systému. 

![vzorky k odeslání](../images/kits-to-send.png)

Na vrchu stránky je výběr, pomocí kterého si vybíráte stav vzorků, který vás zajímá. Na stránce pak vidíte stránkovanou tabulku vzorků v tomto stavu. Pozor: číslo vzorku se může lišit od ID uživatele, jemuž vzorek patří! Jsou to dvě naprosto nesouvisející hodnoty.

Pro každý stav je také zobrazena lišta s akcemi, které se dají provádět na vzorcích v tomto stavu. Kliknutím na toto tlačítko se provede daná akce pro všechny aktuálně vybrané vzorky. Jednou z těchto akcí je vždy přesunutí do následujícího stavu.

![vzorky k analýze](../images/kits-to-analyze.png)

Výběr se provádí pomocí zaškrtávacího políčka v řádku žádaného vzorku. Zaškrtnutím políčka v hlavičce vyberete celou stránku. Je nutno podotknout, že vybírat lze vždy jen v rámci jedné stránky tabulky, a přechodem na jinou stránku se dosavadní výběr zruší. Z tohoto důvodu je také nastavena velikost stránky na 21 (jelikož se vejde 21 štítků na A4), kromě vzorků čekajících na analýzu, jichž je na stránku 96 (velikost jednoho plata do sekvenátoru).

Kliknutím na tlačítko s lupou otevřete okno s detailními informacemi o vzorku. Zde vidíte jméno, adresu a stav vzorku, a navíc dedikované pole s poznámkami, které může technik o vzorku chtít zaznamenat. V tomto okně také můžete přenést vybraný vzorek do následujícího stavu, případně přejít na profil jeho vlastníka.

![detail](../images/kits-info.png)

V případě že chcete vyhledat jeden konkrétní vzorek (např. protože právě dorazil poštou), na vrcholu stránky je tlačítko pro jejich hledání. Hledání probíhá pomocí čísla kitu, které je mj. zaneseno v čárových kódech posílaných s každou testovací sadou. Při úspěšném nalezení vzorku se otevře okno s jeho detailem.

![hledání](../images/kits-search.png)

Vedle tlačítka pro vyhledávání je tlačítko na nahrání analyzovaných dat. Pomocí tohoto tlačítka můžete do systému nahrát výsledek genetické analýzy. Ten bude systémem automaticky zpracován a příslušné změny budou provedeny na odpovídajících vzorcích a uživatelích.

