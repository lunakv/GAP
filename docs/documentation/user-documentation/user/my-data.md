# Moje data

Aplikace umožňuje spravovat vaše genetická data a popřípadě i data uživatelů jenž Vám dali přístup (o přístupech více v sekci [Spravovat práva](./rights-management.md)).

## Rozložení stránky 

Jak můžete vidět na obrázku níže stránka se skládá z několika různých komponent. V následujících sekcích si pojďme podrobněji probrat jejich využití.

![zobrazení vlastních dat](../images/data_view_my_data.png)

### Zdroje dat

Jak již bylo vysvětleno v [Registraci](./registration.md), tak Vaše genetické data mohou pocházet z několika zdrojů. Všechny najdete právě v tomto slouci, spolu se speciálním pseudo zdrojem "*Agregované Markery*".

Tento pseudo zdroj je vygenerován agregací všech ostatních dostupných datových zdrojů. Pokud nastane konflikt hodnot jednoho markeru z různých datových zdrojů, tak je tento problém rozhodnut pracovníkem Kriminalistického ústavu. Tyto zrevidované hodnoty se potom zobrazují v pseudo zdroji "*Agregované Markery*". Pokud se pracovník zatím nevěnoval vašim datům, bude u konfliktní hodnoty zobrazen vykřičníkem s vysvětlujícím komentářem.

### Tabulka s markery

Asi nejpodstatnější částí je tabulka s hodnotami vašich markerů. Na prvním řádku buňky je vyznačeno jméno markery na druhém potom získané hodnoty. Pokud je hodnot u jednoho markeru více, tak přejetím myší nad hodnotu markeru zjistíte z jakového datového zdroje pochází. (Více hodnot může nastat právě pokud jste zaškrtli více datových zdrojů.)

Na obrázku níže vidíte situaci, kdy je vybráno více zdrojů dat.

![více zdrojů dat je vybráno](../images/data_view_selected_more_sources.png)

### Haploskupina

Další nedílnou součástí stránky je informace o Vaší haploskupině (pokud již máte nějakou určenou). Tato informace se nachází nad tabulkou a je vypočtena z hodnot v pseudo zdroji "*Agregované Markery*".

### Nahrávání a stahování dat

Pokud jste tak neučinili při registraci, tak je zde možné nahrát genetická data z jiných laboratoří (více v sekci [Registrace](./registration.md)).

Krom toho je možné každý z datových zdrojů stáhnout ve formátu `.csv` od [FTDNA](https://www.familytreedna.com/) (tj. první řádek jsou názvy markerů a druhý hodnoty v příslušném pořadí).

### Správa dat jiných uživatelů

Pokud máte přístup k datům jiného uživatele (více v sekci [Spravovat práva](./rights-management.md)), tak v záložkách v horní části uvidíte jejich příslušná jména. Po kliknutí na záložku uvidíte data uživatele stejným způsobem jak bylo popsáno výše.

Jak je zmíněno v sekci [Spravovat práva](./rights-management.md), tak rozlišujeme dva módy správy jiného uživatel - editační, nahlížecí. Který z nich máte u jakého uživatele je naznačeno ikonou vedle jména - oko ~ nahlížení, tužka ~ editace. V případě nahlížecího přístupu nemáte povoleno nahrávat soubory.

Na obrázku níže lze vidět náhled uživatele, ke kterému je pouze nahlížecí přístup. 

![data jiného uživatele](../images/data_view_other_user.png)
