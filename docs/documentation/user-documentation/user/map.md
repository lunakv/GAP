# Mapa

Po přihlášení vidíte stránku s mapou republiky. V mapě je zobrazená distribuce uživatelů projektu - čím více uživatelů, tím tmavěji zabarvený region. Přejetím myší na region můžete zobrazit o který region se jedná a v jakém kraji se nachází.

![mapa po příhlášení](../images/users_map.png)

Při kliknutí na region se zobrazí okno s detailními informacemi pro tento region. Pro každou haploskupinu můžete vidět, kolik lidí s touto haploskupinou se v regionu vyskytuje. Pro porovnání vidíte také, kolik uživatelů s haploskupinou se nachází v celém projektu.

Tato informace je pak dále podrozdělena podle příjmení. Pro každou haploskupinu vidíme všechna příjmení, která se s danou haploskupinou vyskytují v daném regionu, a jejich počet v regionu a v republice.

Těchto oken můžete mít otevřeno více zároveň. Držením a posunem myši můžete přesouvat okna v rámci stránky.

![dialog detailu regionu](../images/users_map_dialog.png)

Mapa nabízí několik možností filtrování. Ve výběru z pravého horního rohu můžete vybrat, na které haploskupiny se zobrazení omezí. Lze vybrat více než jednu haploskupinu, a v takovém případě se zobrazí informace o uživatelích majících alespoň jednu z těchto haploskupin.

Mapu můžete filtrovat také podle příjmení. Ve výběru si zvolíte jedno či více příjmení ze seznamu nalezených v projektu, a zobrazení se upraví tak, aby se brali v potaz pouze uživatelé s tímto příjmením.

Tyto dva filtry mohou fungovat současně. Pokud například zvolíte haploskupiny H1 a H2 a příjmení Novák, v mapě se zobrazí jen všichni Nováci, kteří mají jednu z těchto haploskupin.

![mapa s filtrem](../images/users_map_filter.png)

V mapě si též můžete vybrat úroveň detailu, se kterou se zobrazují regiony. Máte na výběr mezi rozložením podle obcí s rozšířenou působnosí, okresů, nebo krajů.
