# Správa práv

Tato část aplikace Vám umožňuje dát přístup k Vašim datům jinému uživateli a zároveň Vám dává možnost zjistit ke komu máte jaká práva. Tuto stránku naleznete poněkud netradičně po rozbalení pravého horního menu.

## Práva a interface

V aplikaci rozlišujeme dvě možnosti přístupu:

- **editační**: lze upravovat veškerá uživatelova data.
- **nahlížecí**: lze pouze prohlížet uživatelova data bez možnosti změny.

Dvě horní tabulky Vám ukazují jací uživatelé mají nahlížecí (resp. editační) přístup k vašemu účtu. U obou tabulek je možnost jak přidání dalšího správcovského uživatele, tak odebrání přístupu nějakému z uživatelů.

Dvě spodní potom ukazují k jakým uživatelům máte naopak přístup Vy.

![tabulky s právy](../images/rights_management.png)

## Maskování uživatelů

Správce uživatele má úplné pravomoce nad spravovaným uživatelem, neboli může za něj provádět veškeré operace, které by byl schopen provádět uživatel sám. Přístup, kterým toto v systému zajišťujeme, nazýváme maskování. 

![maskovaní za jiného uživatele](../images/user-search-masked-as-user.png)

Když maskujete jiného uživatele, procházíte web v jejich zastoupení. Jinými slovy, můžete se tvářit, jako kdyby se dočasně místo Vás přihlásil na web daný uživatel. Když jste takto "pod maskou" jiného uživatele, můžete procházet jeho osobní údaje, jeho nejbližší příbuzné, jeho genetická data, zkrátka vše, čeho je schopen uživatel sám. 

Zatímco procházíte web tímto způsobem, upozorňuje vás o této skutečnosti velký banner na vrchu obrazovky. Tento banner obsahuje jméno uživatele, jehož identitu dočasně zaujímáte, a odkaz zpět na Váš profil, kterým můžete maskování ukončit.

