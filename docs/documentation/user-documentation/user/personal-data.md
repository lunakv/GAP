# Osobní údaje

V sekci **Osobní údaje** lze měnit data, které u Vás systém eviduje. Jedná se vesměs o všechny reálie uvedené při [registraci](./registration.md).

Na rozdíl od většiny funkcionalit aplikace, naleznete tuto po rozbalení hamburgerového menu v pravé horní části stránky. Poté se zobrazí stránka s rozdílným bočním menu, kde již můžete upravovat své údaje (viz obrázky níže).

![osobní údaje](../images/personal_data.png)
![data předka](../images/personal_data_ancesor.png)
![přihlašovací údaje](../images/personal_data_login_data.png)