# Registrace

Na hlavní stránce projektu GAP je mimo jiné možnost registrace (viz obrázek níže). V této sekci se s tímto procesem podrobně seznámíme.

![úvodní stránka](../images/landing_page.png)

## Přihlašovací údaje

Nejprve budete vyzvání k zadání Vašeho emailu a hesla. Prosím mějte na paměti, že email musí být funkční, protože k dokončení procesu registrace je zapotřebí jeho potvrzení. Do Vaší schránky přijde email s odkazem.

![krok 1](../images/registration-1.png)

## Osobní údaje

Dalším krokem je vyplněním Vašich osobních údajů, jako je jméno, datum narození a adresa.

![krok 2](../images/registration-2.png)

## Předek

Do zapojení do projektu je potřeba poskytnout také údaje o vašem nejstarším známém předku. Je velmi důležité, aby se jednalo o muže, protože pouze muži mají chromozom Y, na kterém je predikce haploskupin založena.

Byď není nutné vyplňovat všechny údaje, prosím vyplňte všechny Vám známé reálie.

![krok 3](../images/registration-3.png)

## Genetický test


K přístupu ke všem datům poskytovaným projektem GAP je třeba poskytnout Vaše genetické data jedním z následujících způsobů:

- **Přihlášením se o bezplatný test laboratoří Kriminalistického Ústavu**: V tomto případě bude na Vaší adresu zaslán genetický kit s návodem na použití. Ten posléze odešlete na přiloženou adresu. Laboratoře analyzují poskytnutý vzorek a výsledek posléze budete moci prohlížet přímo v aplikaci (více v sekci [Moje data](./my-data.md)).

![krok 4](../images/registration-4.png)

- **Nahráním existujících genetických dat**: Pokud jste máte genetický test od jiných firem, je možné ho do aplikace nahrát (je možné nahrávat i po registraci - více v sekci [Moje data](./my-data.md)). V současné době aplikace podporuje automatické zpracovaní pouze dat ve formátu FTDNA (více informací [zde](https://www.familytreedna.com/)), nicméně je možné nahrát i jiná data, která budou ručně zpracována pracovníkem Kriminalistického Ústavu.

![krok 5](../images/registration-5.png)

## Informovaný souhlas

K přístupu k datům projektu je potřeba přečíst a potvrdit informovaný souhlas.

![krok 6](../images/registration-6.png)

## Shrnutí

V posledním kroku prosím zkontrolujte všechna Vámi poskytnuta data. Po potvrzení budete přesměrováni do samotné aplikace.

![krok 7](../images/registration-7.png)