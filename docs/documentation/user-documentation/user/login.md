# Přihlášení

Na hlavní stránce je krom jiného možnost přihlášení.

![[úvodní obrazovka]](../images/landing_page.png)

Po zadání emailu a hesla budete přesměrováni do samotné aplikace. V případě zapomenutého hesla je možnost jeho obnovi. Po zadaní Vašeho emailu Vám přijdou instrukce do vaší schránky.

![zobrazený rodokmen](../images/login.png)
