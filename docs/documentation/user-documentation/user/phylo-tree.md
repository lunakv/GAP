# Fylogenetický strom

Jedním ze hlavních funkcionalit aplikace je fylogenetický strom (více informací o něm [zde](https://cs.wikipedia.org/wiki/Fylogenetick%C3%BD_strom)). V následující sekci se dozvíte jak s ním efektivně zacházet.

## Rozložení stránky

Jak je vidět na obrázku níže stránka se skládá z několika částí. V centrální části je samotný strom, který je možno libovolně posouvat, přibližovat či oddalovat. V horní části se potom nachází legenda všech podstromů stromu. A v neposlední řadě informace o vaší haploskupině - ta byla určena na Vámi poskytnutých datech. Tento informační panel může být v několika stavech:

- **Nemáte určenou haploskupinu**: tento stav může nastat například z důvodu, že stále nebyl zpracován Váš genetický vzorek (o který jste mohli zažádat během [registrace](./registration.md)), nebo stále probíhá výpočet vaší haploskupiny v naší aplikaci (tento proces zabírá netriviální čas a tedy skupina nemusí být určena dříve něz i několik minut po nahrání genetických dat).

- **Máte určenou haploskupinu**:
  - *vaše haploskupina **je** ve stromě*: v takovém případě je příslušný vrchol stromu zvýrazněn pulsujícím kruhem, jak je vyznačeno na informačním panelu.
  - *vaše haploskupina **není** ve stromě*: vzhledem k tomu, že náš prediktor nevgen může predikovat jinou skupinu, než je v dostupném fylogenetickém stromě, muže se stát, že v něm Váš vrchol nenaleznete. V takovém případě stále můžete strom prohlížet a je možné, že při některém z periodických obnov stromu, se Vaše haploskupina ve stromě objeví.  

![fylogenetický strom](../images/phylo-tree-landing.png)

## Grafické odlišnosti vrcholů stromu

Pro lepší orientaci ve stromě lze pozorovat několik následujících grafických pomůcek: 

- **barva vrcholu**: barevně jsou odlišeny různé podskupiny (podstromy) stromu.
- **dvojitý kruh**: takto je označen vrchol v němž se nacházejí další uživatelé.
- **pulsující dvojitý kruh**: jak již bylo zmíněno výše, tímto je identifikována Vaše haplokupina. 
- **tmavé lemování kruhu**: takovýto vrchol má schované potomky, které lze kliknutím zobrazit.  
- **pulsující kruh v kořenu**: ten slouží k zvýraznění takzvaného genetického Adama, neboli nejstarší známé haploskupiny, která je otcem všech haploskupin.

## Pokročilé interakce se stromem

Aby se Vám strom lépe prohlížel, je možnost klikáním levého tlačítka myši na vrcholy části stromu sbalovat, či naopak rozbalovat. Kliknutím na vrchol pravým tlačítkem myši potom zobrazíte kontextové menu s dalšími akcemi (viz obrázek níže). Pojďme si všechny postupně projít:

- **informace o vrcholu**: Zobrazí všechny dostupné informace o vrcholu včetně všech uživatelů s danou haploskupinou (viz obrázek níže).
 ![informace o vrcholu stromu](../images/phylo-tree-node-info.png)

- **rozbal celý podstrom** resp. **sbal celý podstrom**: rekurzivně rozbalí (resp. sbalí) celý podstrom příslušného vrcholu
- **vyhledat nejbližší příbuzné**: otevře novou záložku s vyhledáváním nejbližších příbuzných (více v sekci [Nejbližší příbuzní](./relatives-search.md))

![kontextové menu](../images/phylo-tree-contextmenu.png)

## Omezení vyhledávání ostatních uživatelů

Všechny akce týkající se vyhledávání uživatelů zmíněných výše nelze provádět mimo Váš podstrom. Jedná se o zobrazení uživatelů v haploskupině a vyhledávání nejbližších příbuzných. Na obrázcích níže můžete vidět tato omezení v praxi.

![kontextové menu (v jiném podstromu)](../images/phylo-tree-not-my-sybtree-contextmenu.png)
![informace o vrcholu stromu (v jiném podstromu)](../images/phylo-tree-not-my-sybtree-node-info.png)
