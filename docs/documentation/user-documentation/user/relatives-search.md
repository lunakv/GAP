# Nejbližší příbuzní

V tomto okně můžeme vidět seznam našich nejbližších příbuzných. Nejbližší příbuzné identifikujeme jako uživatele, kteří s námi mají co nejvíce společných hodnot STR markerů. Čím méně rozdílných hodnot má daná osoba, tím více ji počítáme jako blízkého příbuzného.

![hlavní tabulka](../images/relatives.png)

Na vrchu stránky vidíte seznam sad markerů. Tyto sady určují, na jakých markerech se s Vámi budou uživatelé porovnávat. Čím více markerů je v sadě, tím může být porovnání přesnější, ale zároveň také roste pravděpodobnost že se jich bude velké množství odlišovat.

Pod tímto výběrem máte konfigurační panel. Zde si můžete zvolit, že chcete omezit zobrazení jenom na nejbližší příbuzné se stejným příjmením jako máte vy sami. Zároveň si můžete vybrat konkrétní haploskupiny, ve kterých chcete nejbližší příbuzné vyhledávat. Upozorňujeme, že můžete vybírat jen haploskupiny ze stejné větve fylogenetického stromu, ve které se sami nacházíte. Pro všechny ostatní skončí dotaz chybou.

Pokud zvolíte možnost "Hledat v podstromu", příbuzní se nebudou hledat jen ve vybraných haploskupinách, ale i ve všech jejich potomcích. Ve výchozím stavu (kde se hledá skrz všechny haploskupiny) nemá tato možnost žádný efekt.

Pod konfigurací následuje seznam nalezených příbuzných. U každého příbuzného je vidět jméno a příjmení uživatele, jeho haploskupina, počet testovaných markerů (neboli kolik markerů z vybrané sady u uživatele známe), genetická vzdálenost (kolik markerů ze sady je jiných než u Vás), a jméno nejstaršího známého předka (pokud je vyplněné).

Kliknutím na tlačítko Detail můžete zobrazit další podrobnosti o uživateli.

![detail uživatele](../images/relatives_user_detail.png)

Kliknutím na tlačítko Porovnání otevřete okno s detailním srovnáním vašich STR markerů v dané sadě.

![porovnání markerů](../images/relatives_compare.png)
