# Externí aplikace

V bočním menu nalezne odkazy do aplikací třetích stran. Jedná se o aplikace: 

- [Nevgen]() prediktor haploskupin v sytému GAP
- [SNP Tracker](https://www.nevgen.org/) aplikace sledující migrace haploskupin v evropě

![hlavní layout s bočním menu](../images/users_map.png)
