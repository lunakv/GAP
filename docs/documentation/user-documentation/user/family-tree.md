# Rodokmen

Další službou, kterou aplikace je nabízí, je manipulace s rodokmeny. Jedná se o poměrně standardní interface, tj. potomci jsou vertikálně níže než rodiče etc. Triviální rodokmen lze nahlédnout na obrázku níže.

![zobrazený rodokmen](../images/family_tree.png)

# Editace

Kliknutím pravým tlačítkem myši na libovolného člověka v rodokmenu lze zobrazit kontextové menu s dostupnými akcemi nad daným člověkem.

![zobrazený rodokmen](../images/family_tree_contextmenu.png)

- **odstraň**: pokud to topologická situace rodokmenu umožňuje (tj. nedošlo by odstraněním člověka k rozpadu rodokmenu na více částí), tak je dána možnost člověka odstranit.
- **přidej nový vrchol**: přidá vybraného příbuzného k danému člověku. Toto je provedeno za pomocí formuláře kde vyplníte potřebné údaje. (Formulář k nahlédnutí níže)
- **Upravit**: otevře stejný formulář pro úpravu reálií daného člověka.

![zobrazený rodokmen](../images/family_tree_edit_form.png)

> [!WARNING]
> Během některých akcí mohou být některá pole editace deaktivována.

> [!WARNING]
> Pokud se pokusíte upravit člověka takovým způsobem, že by došlo k nekonzistenci rodokmenu, tak bude zobrazena chybová hláška a editace nebude provedena. (Například pokud by jste způsobili rozpad rodokmenu na více částí.)

## Ukládání stromu

Strom je ukládám automaticky po každé editaci, není tedy nutné manuálně tuto akci inicializovat.
