# Úvod

Vítejte v uživatelském návodu projektu GAP (Genetika a příjmení). Tato sekce vám poskytne veškeré informace, které potřebujete k optimálnímu využívání aplikace.
Aplikace přináší řadu funkcí, které vám umožní získat genetický test od laboratoří kriminalistického ústavu, spravovat vaše genetické a genealogické údaje, prozkoumávat fylogenetický strom a získat přístup k rozložení haploskupin po celé České republice.
