# Vyhledávání uživatelů

Administrátor má schopnost vyhledávat mezi všemi uživateli v systému. Uživatele je schopný filtrovat podle příjmení a podle haploskupiny.

![tabulka](../images/user-search-table.png)

Pokud si chce administrátor blíže prohlížet data některého uživatele, může kliknout na příslušné tlačítko lupy, čímž začne *maskovat* daného uživatele. Více informací o maskování viz [Správa práv](../user/rights-management.md) (administrátor má pravomoce správce pro všechny běžné uživatele). Po ukončení maskování se vrátíte zpět na stránku vyhledávání.