# Konflikty v genetických datech

Uživatelům je umožněno mít genetická data z různých datových zdrojů, např. z laboratoří Kriminalistického Ústavu nebo [FTDNA](https://www.familytreedna.com/). To nevyhnutelně vede ke konfliktům, které je potřeba manuálně řešit pracovníkem Kriminalistického Ústavu. Za tímto účelem byl v aplikaci vytvořen interface.

Na prvním obrázku vidíte situaci bezprostředně po otevření v případě, že existuje alespoň jeden uživatel. Na druhém je potom případ, kdy nejsou žádné konflikty k řešení. 

![úvodní stránka](../images/gen-conflicts-landing.png)
![úvodní stránka](../images/gen-conflicts-no-conflicts.png)

## Work flow

Jako administrátor nejprve vyberte uživatele jehož konflikty chcete řešit. V horní části se pro tyto účely nachází select input se všemi uživateli, kde se identifikovala konfliktní data.

Následně se vám zobrazí tabulka se všemi konfliktními hodnotami markerů. V případě potřeby si můžete zobrazit i markery, kde nebyl identifikován konflikt (pomocí checkboxu nad tabulkou). Posléze můžete upravovat i tyto hodnoty.

Následně lze začít proces napravování konfliktů. Ve sloupečcích se nachází každý jeden datový zdroj. Pomocí checkboxů lze určovat výsledné hodnoty, ty se objeví v posledním sloupci v editačním poli. V případě potřeby lze zvolit svojí vlastní hodnotu markeru a zcela ignorovat datové zdroje.

Částečně vyplněnou tabulku vidíte zde:

![průběh řešení](../images/gen-conflicts-filling.png)

Dokud nevyřešíte všechny konflikty daného uživatele nelze pokračovat v odeslání na server. V momentě kdy je vše vyplněno, tak se aktivuje tlačítko *Uložit hodnoty markerů* (viz obrázek níže).

![vyřešeno](../images/gen-conflicts-filled.png)

Po kliknutí se objeví hláška o úspěšném odesláním dat na server.

![úspěšně odesláno](../images/gen-conflicts-success-resolved.png)
