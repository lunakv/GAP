# Logy

Namísto procházení logů přímo v databázi, aplikace definuje uživatelsky přívětivý interface.

![tabulka](../images/logs-table.png)

Jedná se o filtrovatelnou tabulku, kde každý řádek reprezentuje jedno **editační** volání na server. Jsou zaznamenávány následující položky: 

- unikátní **ID** logu
- uživatele jenž dotaz **provedl** 
- uživatel jenž byl dotazem **ovlivněn**
- **jméno** endpointu/akce která se stala
- **čas** akce
- **status** tj. jestli dotaz selhal nebo prošel

V případě, že by informace v tabulce nebyly dostačující, tak je možné si rozkliknout detail logu, kde naleznete podrobnější přehled logu:

![detail](../images/logs-detail.png)

## Přístup

Pokud jste **root**, tak máte přístup k logům všech uživatelů, včetně těch administrátorských. Pokud jste **administrátor**, tak můžete nahlížet pouze logy řadových uživatelů aplikace.
