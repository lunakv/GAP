# GAP 3.2.2023

## Ucast
 - Ucastnili se: 
     - M. Brabec, C.Chudacek, E.Nekardova

## Fylogeneticky strom
Hlavnim tematem meetingu bylo dostat feedback na zatim implementovane casti fylogenetickeho stromu a domluvit se na jeho dalsich castech



### Diskutovane hotove casti

- Staticky layout stromu
- Graficky styl vrcholu
  - Kruhy s textem
- Zakaldni interakce se stromem
  - Rozbalovani a sbalovani vrcholu
  - zoom a drag
- vizualni rozliseni rozbalitelnych vrcholu

### Dalsi casti k implementaci

- obarveni haplogrup
- inicialni rozbaleni stomu
  - viditelne koreny haplogrup
  - zobrazeni okoli uzivatele
- zvyrazneni vrcholu uzivatele
- prolik k zobrazeni nejblizsich pribuznych na kazdem vrcholu stromu
- zobrazeni vzorku uzivatelu na kazdem vrcholu
- pokrocile interace 
  - sbalit a rozbalit cely podstrom
- vizualni rozliseni prazdnych vrcholu
- zobrazovat jmeno a rok na vrcholu

### 

