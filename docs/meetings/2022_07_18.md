# Dev meeting
- 18. 07. 2022
- Ucastnili se: 
    -  K. Hruby, M. Brabec, V. Lunak

# Co jsme probirali
- Domluveni na dalsim postupu
  - Studium frameworku/ knihoven - React, ASP.NET, GraphQL
  - Dodelavani High-level Architektury
  - Vytvoreni struktury projektu - Solution, Project Structure
  - Rozdeleni backendu na API, Business Logic, Infrastructure casti
    - API obsahuje graphQL knihovnu a tvori api endpoint. Zaroven vola BL pro zpracovani pozadavku.
    - BL neni zavisla na zadnem projektu a poskytuje Abstrakce pro pristup k datum, ktere implementuje Infrastructure cast.
    - Infrastructure obsahuje implementaci komunikace s db.
  - Zacatek implementace PoC API, BL, Infrastructure
- Ukoly
  - Vse nahore zmineno - K. Hruby, M. Brabec, V. Lunak
  - Maly PoC pro API, Infrastructure, BL uz s GraphQL a PostgreSQL - K. Hruby
  - Vytvoreni fake dat - K. Hruby
  - Napsani kapitoly o Architekture - V. Lunak